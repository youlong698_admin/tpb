# tpb

####  **项目介绍** 
云企采电子采购平台”：是河南云企采信息科技有限公司在多家大、中、小型企业采购需求的分析与实际应用的基础上，结合企业采购流程优化再造理念开发的一体化电子招标采购平台。平台采用先进的采购管理经验和科学的采购管理方法，为客户提供从采购项目立项及审批，计划安排、采购过程（询比价/竞价/招投标）、采购结果、合同管理、订单管理、资金计划、供应商管理、物料管理等全套采购流程的多种功能。适宜多采购类型、多采购方式、多采购层级、多采购品种，提供规范化流程、实时操作、智能化引导和方便快捷的采购交易平台。并运用大数据分析等先进技术为企业提供采购信息分析和采购过程监控等延伸服务。http://www.yunqicai.cn

####  **软件架构** 

软件架构说明
- 核心框架：Spring 4.1.6
- 视图框架：Struts 2.5.13
- 持久层框架：Hibernate 3.3
- 日志管理：Log4j 2.8.2
- JS框架：Jquery 1.8.3
- UI框架: Ace-Admin

####  **开发环境** 

建议开发者使用以下环境，可以避免版本带来的问题
- IDE: MyEclipse8.5+
- DB: Oracle10g+
- JDK: JDK1.6+
- WEB: Tomcat6.0+


####  **运行环境** 

- WEB服务器：Tomcat6.0+
- 数据库服务器：Oracle10g+
- JAVA平台: JRE1.6+
- 操作系统：Windows、Linux等


#### 系统截图
 **采购方** 

![首页图片](https://images.gitee.com/uploads/images/2018/1031/172245_cbf5ee07_557782.png "yunqicai_index.png")
![图形化操作流程](https://images.gitee.com/uploads/images/2018/1031/172320_865d1739_557782.png "火狐截图_2018-10-14T07-22-07.984Z.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1031/172355_99a18e90_557782.png "火狐截图_2018-10-14T09-33-59.481Z.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1031/172416_c9731c18_557782.png "火狐截图_2018-10-14T07-24-40.560Z.png")

 **供应商** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/101838_76b66da3_557782.png "火狐截图_2018-11-16T07-06-17.865Z.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/101854_64ed2a50_557782.png "火狐截图_2018-11-16T07-11-03.159Z.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/101906_b91cc46d_557782.png "火狐截图_2018-11-16T07-19-11.842Z.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/101920_055fa425_557782.png "火狐截图_2018-11-16T07-20-23.906Z.png")

 **移动端** 

![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/101947_c7d2d58a_557782.jpeg "微信图片_20181116151613.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/102006_99650d35_557782.jpeg "微信图片_20181116151525.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/102019_d8c06f07_557782.jpeg "微信图片_20181116151603.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/102026_8da970c0_557782.jpeg "微信图片_20181116151607.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1123/102036_f070aedf_557782.jpeg "微信图片_20181116151544.jpg")

