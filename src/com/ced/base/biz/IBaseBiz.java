package com.ced.base.biz;

import java.io.Serializable;
import java.util.List;


import com.ced.base.dao.IBaseDao;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

public interface IBaseBiz {

	
	abstract void setBaseDao(IBaseDao dao);	

	/**
	 * 保存对象
	 * 
	 * @param o
	 *            对象
	 * @throws BaseException
	 */
	abstract void saveObject(Object o) throws BaseException;

	/**
	 * 更新对象
	 * 
	 * @param o
	 *            对象
	 * @throws BaseException
	 */
	abstract void updateObject(Object o) throws BaseException;
	
	/**
	 * 修改或是更新数据
	 */
	abstract void saveOrUpdateObject(Object o) throws BaseException;

	/**
	 * 删除对象
	 */
	abstract void removeObject(Object o) throws BaseException;

	/**
	 * 删除对象
	 * 
	 * @param clazz
	 *            对象Class
	 * @param id
	 *            对象ID
	 * @throws BaseException 
	 * @throws BaseException
	 */
	abstract void removeObject(Class clazz, Serializable id) throws BaseException;

	/**
	 * 批量删除对象
	 * 
	 * @param list
	 *            对象ID数组
	 * @throws BaseException
	 */
	abstract void removeBatchObject(Class clazz, Serializable[] id)
			throws BaseException;
	/**
	 * 统计数据条数
	 * hql sample: select count(*) from ObjectName where 1=1 
	 * @param hql 查询语句
	 */
	abstract int countObjects( String hql )throws BaseException;
	/**
	 * 统计数据条数
	 * hql sample: select sum()  min() max() from ObjectName where 1=1 
	 * @param hql 查询语句
	 */
	public Object sumOrMinOrMaxObjects( String hql,Class clazz)throws BaseException;
	/**
	 * 查询单个对象（对象ID）
	 * 
	 * @param clazz
	 *            对象Class
	 * @param id
	 *            对象ID
	 * @return Object 对象
	 * @throws BaseException
	 */
	abstract Object getObject(Class clazz, Serializable id)
			throws BaseException;

	/**
	 * 查询所有对象
	 * 
	 * @param clazz
	 *            对象Class
	 * @return List 对象列表
	 * @throws BaseException
	 */
	abstract List getObjects(Class clazz) throws BaseException;
/**
	 * 封装HQL查询
	 * @param rollPage 翻页
	 * @param hql 
	 * @return
	 */
	abstract List getObjects(RollPage rollPage, String hql)
			throws BaseException;
	/**
	 * HQL查询 
	 * @param hql 
	 * @return
	 */
	abstract List getObjects(String hql) throws BaseException;
	
	/**
	 * 获得Sequence的下一个值
	 * @return
	 * @throws BaseException 
	 */
	public String getSequenceNextValue(String seqName) throws BaseException;
	
	/**
	 * 移除Session中的对象
	 * 
	 * @param obj
	 *            对象obj
	 * @throws BaseException
	 */
	abstract void removeSessionObject(Object obj) throws BaseException;
	

	/**
	 * 执行返回泛型集合的SQL语句
	 * 
	 * @param cls
	 *            泛型类型
	 * @param sql
	 *            查询SQL语句
	 * @return 泛型集合
	 * @throws BaseException
	 */
	public  List<Object> getObjectsList(Class clazz, String sql)throws BaseException;
	/**
	 * 执行返回泛型集合的SQL语句
	 * 
	 * @param cls
	 *            泛型类型
	 * @param rollPage
	 *           分页参数
	 * @param sql
	 *            查询SQL语句
	 * @return 泛型集合
	 * @throws BaseException
	 */
	public  List<Object> getObjectsListRollPage(Class clazz,RollPage rollPage, String sql)throws BaseException;
	/**
	 * 自定义执行sql
	 * @param sql 
	 * @return
	 */
	public int updateJdbcSql(String sql) throws BaseException;
	/**
	 * 未经封装的SQL查询
	 * 
	 * @param rollPage
	 *            翻页
	 * @param sql
	 *            SQL语句
	 * @param column
	 *            列名
	 * @return List 结果集
	 * @throws BaseException 
	 */
	abstract List getObjects(String sql, int columnCount) throws BaseException;
	/**
	 * 统计数据条数
	 * hql sample: select sum()  min() max() from table where 1=1 
	 * @param hql 查询语句
	 */
	abstract Object sumOrMinOrMaxJdbcSql( String sql,Class clazz)throws BaseException;
}