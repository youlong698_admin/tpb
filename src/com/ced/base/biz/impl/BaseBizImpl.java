package com.ced.base.biz.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



import com.ced.base.biz.IBaseBiz;
import com.ced.base.dao.IBaseDao;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

public class BaseBizImpl implements IBaseBiz  {

	protected final Logger log = LogManager.getLogger(this.getClass());
	

	protected IBaseDao baseDao = null;


	public void setBaseDao(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

	
	/**
	 * 保存对象
	 * 
	 * @param o
	 *            对象
	 * @throws BaseException
	 */
	public void saveObject(Object o) throws BaseException {
		baseDao.saveObject(o);
	}
	
	/**
	 * 更新对象
	 * 
	 * @param o
	 *            对象
	 * @throws BaseException
	 */
	public void updateObject(Object o) throws BaseException {
		baseDao.updateObject(o);
	}
	
	/**
	 * 修改或是更新数据
	 */
	public void saveOrUpdateObject(Object o)throws BaseException {
		baseDao.saveOrUpdateObject(o);
	}
	
	/**
	 * 删除对象
	 */
	public void removeObject( Object o) throws BaseException {
		baseDao.removeObject(o) ;
	}
	
	/**
	 * 删除对象
	 * 
	 * @param clazz
	 *            对象Class
	 * @param id
	 *            对象ID
	 * @throws BaseException
	 */
	public void removeObject(Class clazz, Serializable id) throws BaseException {
		baseDao.removeObject(clazz, id);
	}

	
	/**
	 * 批量删除对象
	 * 
	 * @param list
	 *            对象ID数组
	 * @throws BaseException
	 */
	public void removeBatchObject(Class clazz, Serializable[] id)
			throws BaseException {
		baseDao.removeBatchObject(clazz, id);
	}
	/**
	 * 统计数据条数
	 * hql sample: select count(*) from ObjectName where 1=1 
	 * @param hql 查询语句
	 */
	public int countObjects( String hql )throws BaseException {
		return baseDao.count( hql ) ;
	}
	/**
	 * 统计数据条数
	 * hql sample: select sum()  min() max() from ObjectName where 1=1 
	 * @param hql 查询语句
	 */
	public Object sumOrMinOrMaxObjects( String hql,Class clazz )throws BaseException {
		return baseDao.sumOrMinOrMax( hql, clazz ) ;
	}
	/**
	 * 查询单个对象（对象ID）
	 * 
	 * @param clazz
	 *            对象Class
	 * @param id
	 *            对象ID
	 * @return Object 对象
	 * @throws BaseException
	 */
	public Object getObject(Class clazz, Serializable id) throws BaseException {
		return baseDao.getObject(clazz, id);
	}

	/**
	 * 查询所有对象
	 * 
	 * @param clazz
	 *            对象Class
	 * @return List 对象列表
	 * @throws BaseException
	 */
	public List getObjects(Class clazz) throws BaseException {
		return baseDao.getObjects(clazz);
	}/**
	 * 封装HQL查询
	 * @param rollPage 翻页
	 * @param hql 
	 * @return
	 */
	public List getObjects(RollPage rollPage, String hql) throws BaseException {
		return baseDao.getObjects(rollPage, hql ) ;
	}
	/**
	 * HQL查询 
	 * @param hql 
	 * @return
	 */
	public List getObjects(String hql)  throws BaseException {
		return baseDao.getObjects(hql) ;
	}
	/**
	 * 移除Session中的对象
	 * 
	 * @param obj
	 *            对象obj
	 * @throws BaseException
	 */
	public void removeSessionObject(Object obj) throws BaseException {
		baseDao.removeSessionObject(obj) ;
	}
	/**
	 * 获得Sequence的下一个值
	 * @author luguanglei 2016-08-25
	 * @return
	 * @throws BaseException 
	 */
	public String getSequenceNextValue(String seqName) throws BaseException {  
	    return baseDao.getSequenceNextValue(seqName);  
	} 
	/**
	 * 执行返回泛型集合的SQL语句
	 * 
	 * @param cls
	 *            泛型类型
	 * @param sql
	 *            查询SQL语句
	 * @return 泛型集合
	 * @throws BaseException
	 */
	public List<Object> getObjectsList(Class clazz, String sql)
			throws BaseException {		
		return baseDao.getObjectsList(clazz, sql);
	}
	/**
	 * 执行返回泛型集合的SQL语句
	 * 
	 * @param cls
	 *            泛型类型
	 * @param rollPage
	 *           分页参数
	 * @param sql
	 *            查询SQL语句
	 * @return 泛型集合
	 * @throws BaseException
	 */
	public List<Object> getObjectsListRollPage(Class clazz,RollPage rollPage, String sql)
			throws BaseException {		
		return baseDao.getObjectsListRollPage(clazz,rollPage, sql);
	}
	/**
	 * 自定义执行sql
	 * @param sql 
	 * @return
	 */
	public int updateJdbcSql(String sql) throws BaseException {
		return baseDao.updateJdbcSql(sql) ;
	}
	/**
	 * 未经封装的SQL查询
	 * 
	 * @param rollPage
	 *            翻页
	 * @param sql
	 *            SQL语句
	 * @param column
	 *            列名
	 * @return List 结果集
	 * @throws BaseException 
	 */
	public List getObjects(String sql, int columnCount) throws BaseException {
		return baseDao.getObjects(sql, columnCount) ;
	}
	/**
	 * 统计数据条数
	 * hql sample: select sum()  min() max() from table where 1=1 
	 * @param hql 查询语句
	 */
	public Object sumOrMinOrMaxJdbcSql( String sql,Class clazz )throws BaseException {
		return baseDao.sumOrMinOrMaxJdbcSql( sql, clazz ) ;
	}

}
