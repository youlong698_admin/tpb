package com.ced.base.exception;

public class ExceptionRescoure {

	public static final String LOGIN_EXCEPTION = "登陆错误";

	public static final String HIBERNATE_QUERY_EXCEPTION = "查询数据错误";	

	public static final String HIBERNATE_SAVE_EXCEPTION = "保存数据错误";

	public static final String HIBERNATE_UPDATE_EXCEPTION = "更新数据错误";

	public static final String HIBERNATE_DELETE_EXCEPTION = "删除数据错误";
	
	public static final String HIBERNATE_BATCH_SAVE_EXCEPTION = "批量保存数据错误";

	public static final String HIBERNATE_BATCH_UPDATE_EXCEPTION = "批量更新数据错误";

	public static final String HIBERNATE_BATCH_DELETE_EXCEPTION = "批量删除数据错误";
	
	public static final String HIBERNATE_NOT_FIND = "查询的数据不存在";
		
	public static final String HIBERNATE_OBJECT_EXCEPTION = "泛型类型转换错误";;
	
	public static final String HIBERNATE_CLOSE_EXCEPTION = "数据库连接关闭错误";
	
	public static final String JDBC_UPDATE_EXCEPTION = "JDBC更新数据错误";
	
}
