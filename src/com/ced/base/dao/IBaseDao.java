package com.ced.base.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import org.hibernate.Session;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

public interface IBaseDao {
	/**
	 * 获取Connection
	 */
	public Connection getConnection() throws BaseException ;
	/**
	 * 获取HibernateSession
	 */
	public Session getTrSession() throws BaseException;

	/**
	 * 保存数据
	 */
	public void saveObject(Object o) throws BaseException ;
	
	/**
	 * 更新数据
	 */
	public void updateObject(Object o) throws BaseException ;
	
	/**
	 * 更新数据
	 */
	public void updateObjectByHql(String hql) throws BaseException;
	
	/**
	 * 修改或是更新数据
	 */
	public void saveOrUpdateObject(Object o)throws BaseException ;
	
	/**
	 * 删除数据
	 */
	public void removeObject( Object o) throws BaseException ;
	
	/**
	 * 删除数据
	 */
	public void removeObject(Class clazz, Serializable id) throws BaseException ;

	/**
	 * 批量删除数据
	 */
	public void removeBatchObject(Class clazz, Serializable[] id) throws BaseException ;

	/**
	 * 数据查询（根据ID查询）
	 */
	public Object getObject(Class clazz, Serializable id) throws BaseException ;

	/**
	 * 数据查询，查询所有数据
	 */
	public List getObjects(Class clazz)throws BaseException ;
	/**
	 * 统计数据条数
	 * 
	 * @param hql
	 * @return
	 * @throws BaseException
	 */
	public int count(String hql) throws BaseException ;
	/**
	 * 计算之和 sum 最小值min 最大值max
	 * 
	 * @param hql
	 * @return
	 * @throws BaseException
	 */
	public Object sumOrMinOrMax(String hql,Class clazz) throws BaseException ;
	
	/**
	 * 封装HQL查询
	 * @param rollPage 翻页
	 * @param hql 
	 * @return
	 */
	public List getObjects(RollPage rollPage, String hql) throws BaseException ;

	/**
	 * HQL查询 
	 * @param hql 
	 * @return
	 */
	public List getObjects(String hql)  throws BaseException ;
	
	/**
	 * 移除Session中的对象
	 * 
	 * @param obj
	 *            对象obj
	 * @throws BaseException
	 */
	public void removeSessionObject(Object obj) throws BaseException ;
	/**
	 * 执行返回泛型集合的SQL语句
	 * 
	 * @param cls
	 *            泛型类型
	 * @param sql
	 *            查询SQL语句
	 * @return 泛型集合
	 * @throws BaseException
	 */
	public <T> List<T> getObjectsList(Class<T> cls, String sql)throws BaseException;
	/**
	 * 执行返回泛型集合的SQL语句
	 * 
	 * @param cls
	 *            泛型类型
	 * @param sql
	 *            查询SQL语句
	 * @return 泛型集合
	 * @throws BaseException
	 */
	public <T> List<T> getObjectsListRollPage(Class<T> cls,RollPage rollPage, String sql)throws BaseException;
	/**
	 * 自定义执行sql
	 * @param sql 
	 * @return
	 */
	public int updateJdbcSql(String sql) throws BaseException;
	
	/**
	 * 取得SEQUENCE的下一个值
	 * @param seqName  SEQUENCE名称 
	 * @throws BaseException 
	 */
	public String getSequenceNextValue(String seqName ) throws BaseException ;
	/**
	 * 未经封装的SQL查询
	 * 
	 * @param rollPage
	 *            翻页
	 * @param sql
	 *            SQL语句
	 * @param column
	 *            列名
	 * @return List 结果集
	 * @throws BaseException 
	 */
	public List getObjects(String sql, int columnCount) throws BaseException;
	/**
	 * 计算之和 sum 最小值min 最大值max
	 * 
	 * @param sql
	 * @return
	 * @throws BaseException
	 */
	public Object sumOrMinOrMaxJdbcSql(String sql,Class clazz) throws BaseException ;
}