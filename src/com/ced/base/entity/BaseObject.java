package com.ced.base.entity;

import java.io.File;
import java.io.Serializable;

import com.ced.sip.common.entity.SessionInfo;

/**
 * Base class for Model objects. Child objects should implement toString(),
 * equals() and hashCode();
 */
public abstract class BaseObject implements Serializable {

	// 当前登录用户信息
    private SessionInfo sessionInfo ;
    
   
    // 附件下载url地址
    private String attachmentUrl ;
    
    
    /***单文件上传时候所用****/
    // 默认上传文件名
	private File []   uploadFiles ;
	
	private String [] uploadFilesContentType ;
	
	private String [] uploadFilesFileName ;

	 /***多文件上传时候所用****/
    // 附件删除ID
    private String attIds ;
    
    // 上传附件生成的uuid
	private String uuIdData ;

    // 附件名
	private String   fileNameData ;

    // 附件格式
	private String   fileTypeData ;
	
	private String  attIdData;
	
	public File[] getUploadFiles() {
		return uploadFiles;
	}

	public void setUploadFiles(File[] uploadFiles) {
		this.uploadFiles = uploadFiles;
	}

	public String[] getUploadFilesContentType() {
		return uploadFilesContentType;
	}

	public void setUploadFilesContentType(String[] uploadFilesContentType) {
		this.uploadFilesContentType = uploadFilesContentType;
	}

	public String[] getUploadFilesFileName() {
		return uploadFilesFileName;
	}

	public void setUploadFilesFileName(String[] uploadFilesFileName) {
		this.uploadFilesFileName = uploadFilesFileName;
	}

	public SessionInfo getSessionInfo() {
		return sessionInfo;
	}

	public void setSessionInfo(SessionInfo sessionInfo) {
		this.sessionInfo = sessionInfo;
	}

	public String getAttachmentUrl() {
		return attachmentUrl;
	}

	public void setAttachmentUrl(String attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}

	public String getAttIds() {
		return attIds;
	}

	public void setAttIds(String attIds) {
		this.attIds = attIds;
	}

	public String getUuIdData() {
		return uuIdData;
	}

	public void setUuIdData(String uuIdData) {
		this.uuIdData = uuIdData;
	}

	public String getFileNameData() {
		return fileNameData;
	}

	public void setFileNameData(String fileNameData) {
		this.fileNameData = fileNameData;
	}

	public String getFileTypeData() {
		return fileTypeData;
	}

	public void setFileTypeData(String fileTypeData) {
		this.fileTypeData = fileTypeData;
	}

	public String getAttIdData() {
		return attIdData;
	}

	public void setAttIdData(String attIdData) {
		this.attIdData = attIdData;
	}
	
}
