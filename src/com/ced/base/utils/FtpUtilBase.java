package com.ced.base.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;


public class FtpUtilBase{

	private FTPClient  ftpClient = null;

	private static final Log logger = LogFactory.getLog(FtpUtilBase.class);
	 //ftp服务器地址
    public String hostname = GlobalSettingBase.getAttachdmentPlacedFtpAddress();
    //ftp服务器端口号默认为21
    public Integer port =  Integer.parseInt( GlobalSettingBase.getAttachdmentPlacedFtpAddressPort() ) ;
    //ftp登录账号
    public String username = GlobalSettingBase.getFileServerFtpUser();
    //ftp登录密码
    public String password = GlobalSettingBase.getFileServerFtpPassword();
	/**
	 * 连接文件服务器
	 */
	public void initFtpClient() {
		try {
			ftpClient = new FTPClient();
            ftpClient.setControlEncoding("utf-8");
            ftpClient.connect(GlobalSettingBase.getAttachdmentPlacedFtpAddress(), Integer.parseInt( GlobalSettingBase.getAttachdmentPlacedFtpAddressPort() )); //连接ftp服务器
            ftpClient.enterLocalActiveMode();    
            ftpClient.login(username, password); //登录ftp服务器
            int replyCode = ftpClient.getReplyCode(); //是否成功登录服务器
            if(!FTPReply.isPositiveCompletion(replyCode)){
               Thread.sleep(10);
             }
                			
			logger.info("login success");
		} catch (Exception ex) {
			logger.error(ex);
		}
	}
	 
	/**
     * 上传文件
     * @param pathname ftp服务保存地址
     * @param inputStream 
     *  @param originfilename 待上传文件的名称（绝对地址） * 
     * @return
     */
     public boolean uploadFile( String pathname, InputStream inputStream,String originfilename){
    	 boolean flag = false;
         try{
        	 logger.info("开始上传文件"+originfilename);
             initFtpClient();
             ftpClient.setFileType(ftpClient.BINARY_FILE_TYPE);
             CreateDirecroty(pathname);
             ftpClient.makeDirectory(pathname);
             ftpClient.changeWorkingDirectory(pathname);
             ftpClient.storeFile(originfilename, inputStream);
             inputStream.close();
             ftpClient.logout();
             flag = true;
             logger.info("上传文件成功");
         }catch (Exception e) {
        	 logger.error("上传文件失败"+e);
             e.printStackTrace();
         }finally{
             if(ftpClient.isConnected()){ 
                 try{
                     ftpClient.disconnect();
                 }catch(IOException e){
                     e.printStackTrace();
                 }
             } 
             if(null != inputStream){
                 try {
                     inputStream.close();
                 } catch (IOException e) {
                     e.printStackTrace();
                 } 
             } 
         }
         return true;
     }
     //改变目录路径
      public boolean changeWorkingDirectory(String directory) {
             boolean flag = true;
             try {
                 flag = ftpClient.changeWorkingDirectory(directory);
                 if (flag) {
                	 logger.info("进入文件夹" + directory + " 成功！");

                 } else {
                	 logger.info("进入文件夹" + directory + " 失败！开始创建文件夹");
                 }
             } catch (IOException ioe) {
                 ioe.printStackTrace();
             }
             return flag;
         }

     //创建多层目录文件，如果有ftp服务器已存在该文件，则不创建，如果无，则创建
     public boolean CreateDirecroty(String remote) throws IOException {
         boolean success = true;
         String directory = remote + "/";
         // 如果远程目录不存在，则递归创建远程服务器目录
         if (!directory.equalsIgnoreCase("/") && !changeWorkingDirectory(new String(directory))) {
             int start = 0;
             int end = 0;
             if (directory.startsWith("/")) {
                 start = 1;
             } else {
                 start = 0;
             }
             end = directory.indexOf("/", start);
             String path = "";
             String paths = "";
             while (true) {
                 String subDirectory = new String(remote.substring(start, end).getBytes("GBK"), "iso-8859-1");
                 path = path + "/" + subDirectory;
                 if (!existFile(path)) {
                     if (makeDirectory(subDirectory)) {
                         changeWorkingDirectory(subDirectory);
                     } else {
                         logger.info("创建目录[" + subDirectory + "]失败");
                         changeWorkingDirectory(subDirectory);
                     }
                 } else {
                     changeWorkingDirectory(subDirectory);
                 }

                 paths = paths + "/" + subDirectory;
                 start = end + 1;
                 end = directory.indexOf("/", start);
                 // 检查所有目录是否创建完毕
                 if (end <= start) {
                     break;
                 }
             }
         }
         return success;
     }

   //判断ftp服务器文件是否存在    
     public boolean existFile(String path) throws IOException {
             boolean flag = false;
             FTPFile[] ftpFileArr = ftpClient.listFiles(path);
             if (ftpFileArr.length > 0) {
                 flag = true;
             }
             return flag;
         }
     //创建目录
     public boolean makeDirectory(String dir) {
         boolean flag = true;
         try {
             flag = ftpClient.makeDirectory(dir);
             if (flag) {
            	 logger.info("创建文件夹" + dir + " 成功！");

             } else {
            	 logger.info("创建文件夹" + dir + " 失败！");
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
         return flag;
     }
     
     /** * 下载文件 * 
     * @param pathname FTP服务器文件目录 * 
     * @param filename 文件名称 * 
	 * @param saveAsFileName 下载时指定的文件名
     * @param response
     * @return */
     public  boolean downloadFile(String pathname, String filename,String saveAsFileName, HttpServletResponse response){ 
         boolean flag = false; 
         OutputStream os=null;
         try { 
             initFtpClient();
			 response.setContentType("application/x-msdownload");
             //切换FTP目录 
             ftpClient.changeWorkingDirectory(pathname); 
             FTPFile[] ftpFiles = ftpClient.listFiles(); 
             for(FTPFile file : ftpFiles){ 
                 if(filename.equalsIgnoreCase(file.getName())){ 
                	 response.addHeader("Content-Disposition",
     						"attachment; filename=\""
     								+ StringUtil.getISO(saveAsFileName) + "\"");

     				response.setHeader("Accept-ranges", "bytes");
     				//response.setHeader( "Set-Cookie", "name=value; HttpOnly"); 

     				os = ((HttpServletResponse) response)
     						.getOutputStream();
     				InputStream is = ftpClient.retrieveFileStream(file.getName());

     				byte[] bytes = new byte[1024];
     				int c;
     				while ((c = is.read(bytes)) != -1) {
     					os.write(bytes, 0, c);
     					// fos.write(bytes, 0, c);
     				}
     				is.close();
     				os.close();
                 } 
             } 
             ftpClient.logout(); 
             flag = true; 
         } catch (Exception e) {
             e.printStackTrace(); 
         } finally{ 
             if(ftpClient.isConnected()){ 
                 try{
                     ftpClient.disconnect();
                 }catch(IOException e){
                     e.printStackTrace();
                 }
             } 
             if(null != os){
                 try {
                     os.close();
                 } catch (IOException e) {
                     e.printStackTrace();
                 } 
             } 
         } 
         return flag; 
     }
     
     /** * 删除文件 * 
     * @param pathname FTP服务器保存目录 * 
     * @param filename 要删除的文件名称 * 
     * @return */ 
     public boolean deleteFile(String pathname, String filename){ 
         boolean flag = false; 
         try { 
        	 logger.info("开始删除文件");
             initFtpClient();
             //切换FTP目录 
             ftpClient.changeWorkingDirectory(pathname); 
             ftpClient.dele(filename); 
             ftpClient.logout();
             flag = true; 
             logger.info("删除文件成功");
         } catch (Exception e) { 
        	 logger.info("删除文件失败");
             e.printStackTrace(); 
         } finally {
             if(ftpClient.isConnected()){ 
                 try{
                     ftpClient.disconnect();
                 }catch(IOException e){
                     e.printStackTrace();
                 }
             } 
         }
         return flag; 
     }
	
}
