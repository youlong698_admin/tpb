package com.ced.base.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author 代长伟
 */
public class GlobalSettingBase {

	/**
	 * Logger for this class
	 */
	private static final Log log = LogFactory.getLog(GlobalSettingBase.class);

	private static Properties globalSetting = null;

	// 附件默认上传目录
	private static String default_attachment_upload_location = "/TEMP-GENERATOR/ATTACHMENT/";

	static {
		InputStream is = GlobalSettingBase.class.getClassLoader()
				.getResourceAsStream("globalSetting.properties");
		Properties pro = new Properties();

		try {
			pro.load(is);
		} catch (IOException e) {
			log.error("load file globalSetting.properties error", e);
		}
		
		globalSetting = pro;
	}

	/**
	 * 获取附件上传相对路径
	 * @return
	 */
	public static String getAttachmentUploadLoaction() {
		String location = globalSetting
				.getProperty("attachment_upload_location");
		if (StringUtil.isBlank(location)) {
			location = default_attachment_upload_location;
		}
		return location;
	}
	/**
	 * get globalSetting.properties
	 * @param key
	 * @return
	 */
	public static String getGlobalSettingProperty(String key) {
		log.debug("Fetching property [" + key + "="
				+ globalSetting.getProperty(key) + "]");
		return globalSetting.getProperty(key);
	}

	/**
	 * nas 服务器地址，用于存放附件
	 * <p>采用FTP协议</p>
	 * @return
	 */
	public static String getAttachdmentPlacedFtpAddress(){
		return getGlobalSettingProperty("attachment_placed_ftp_address");
	}

	/**
	 * nas 服务器地址端口，用于存放附件
	 * <p>采用FTP协议</p>
	 * @author Ted 2010-05-27 
	 * @return
	 */
	public static String getAttachdmentPlacedFtpAddressPort(){
		return getGlobalSettingProperty("attachment_placed_ftp_address_port") ;
	}
	
	/**
	 * 文件服务器的访问用户名( FTP )
	 * <p>采用FTP协议</p>
	 * @return
	 */
	public static String getFileServerFtpUser(){
		return getGlobalSettingProperty("fileserver_ftp_user");
	}
	/**
	 * 文件路径
	 * @return
	 */
	public static String getFilePath(){
		return getGlobalSettingProperty("file_path");
	}
	/**
	 * 文件服务器的访问用户名的密码( FTP )
	 * <p>采用FTP协议</p>
	 * @return
	 */
	public static String getFileServerFtpPassword(){
		return getGlobalSettingProperty("fileserver_ftp_password");
	}
	/**
	 * 邮箱smtpServer
	 * @return
	 */
	public static String getSmtpServer(){
		return getGlobalSettingProperty("smtpServer");
	}
	/**
	 * 邮箱端口smtpPort
	 * @return
	 */
	public static String getSmtpPort(){
		return getGlobalSettingProperty("smtpPort");
	}
	/**
	 * 邮箱发送者用户名emailFromUserName
	 * @return
	 */
	public static String getEmailFromUserName(){
		return getGlobalSettingProperty("emailFromUserName");
	}
	/**
	 * 邮箱发送者用户名emailFromUserPassword
	 * @return
	 */
	public static String getEmailFromUserPassword(){
		return getGlobalSettingProperty("emailFromUserPassword");
	}

	public static Properties getGlobalSetting() {
		return globalSetting;
	}
	 
}
