package com.ced.base.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryTask;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;


import com.ced.base.entity.BaseAttachment;
import com.ced.base.entity.BaseObject;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.EmailUtil;
import com.ced.base.utils.RollPage;
import com.ced.base.utils.StringUtil;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.smsMessage.SmsMessage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.system.biz.ISysEmailSmsLogBiz;
import com.ced.sip.system.biz.ISysMessageLogBiz;
import com.ced.sip.system.entity.SysEmailSmsLog;
import com.ced.sip.system.entity.SysMessageLog;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.workflow.base.entity.WFHistoryTask;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
import com.opensymphony.xwork2.ActionSupport;

public abstract class BaseAction extends ActionSupport {
	   
	private static final long serialVersionUID = 3525445612504421307L;

	protected transient final Logger log = LogManager.getLogger(this.getClass());

	protected static final String CONTENT_TYPE = "text/html; charset=utf-8";
	protected static final String SUCCESS		= "success";
	protected static final String INDEX			= "index";
	protected static final String VIEW 			= "view";
	protected static final String ADD_INIT		= "addInit";
	protected static final String ADD 			= "add";
	protected static final String MODIFY_INIT 	= "modifyInit";
	protected static final String MODIFY 		= "modify";
	protected static final String DELETE 		= "delete";
	protected static final String DETAIL 		= "detail";
	protected static final String SUBMIT 		= "submit";
	protected static final String TREE			= "tree";
	protected static final String OPEN_NEW		= "openNew";
	
	
	EmailUtil util = new EmailUtil();

	SmsMessage smsMessage=new SmsMessage();
	// 用户邮件短信日志 
	@Autowired
	private ISysEmailSmsLogBiz iSysEmailSmsLogBiz;
	//用户消息提醒
	@Autowired
	private ISysMessageLogBiz iSysMessageLogBiz;
	
	// 用户邮件短信日志
	private SysEmailSmsLog sysEmailSmsLog;
	
	//用户消息提醒
	private SysMessageLog  sysMessageLog;
	
	private List listValue ;
	
	private RollPage rollPage  ;
	
	private String page = "0";
	
	//datatbales
	private  String draw;
	//数据起始位置
	private String startIndex="0";
	  //数据长度
	private String pageSize="10";
	//获取客户端需要那一列排序
	private String orderColumn;
	 //获取排序方式 默认为asc
	private String orderDir="desc";
	
	private String returnJson = "";
	
	private HashMap<String, Object> jsonMap = new HashMap<String, Object>();
	
	@Autowired
	private SnakerEngineFacets facets;
	/**
	 * 
	 * @param bo 附件对象实例
	 * @param billId 单据ID 
	 * @param attachmentCode 附件编码   多文件上传
	 * @param writer 编制人
	 * @return
	 */
	protected BaseAttachment setUploadFile( BaseObject bo, Long billId, String attachmentCode, String writer ) {
		BaseAttachment att = new BaseAttachment() ;
		att.setUuIdData( bo.getUuIdData() ) ;
		att.setFileNameData(bo.getFileNameData() ) ;
		att.setAttIdData( bo.getAttIdData() ) ;
		att.setFileTypeData( bo.getFileTypeData() ) ;
		att.setAttachmentType( StringUtil.getClassName( bo.getClass() ) ) ;
		att.setAttachmentTypeId( billId )  ;
		att.setAttachmentField( attachmentCode ) ;
		att.setWriter(writer) ;
		att.setWriteDate( new Date() ) ;
		
		return att ;
	}
	
	/**
	 * 
	 * @param bo 附件对象实例
	 * @param billId 单据ID
	 * @param attachmentCode 附件编码  单文件上传
	 * @param writer 编制人
	 * @return
	 */
	protected BaseAttachment setUploadFileOne( BaseObject bo, Long billId, String attachmentCode, String writer ) {
		BaseAttachment att = new BaseAttachment() ;
		att.setUploadFiles( bo.getUploadFiles() ) ;
		att.setUploadFilesFileName( bo.getUploadFilesFileName() ) ;
		att.setUploadFilesContentType( bo.getUploadFilesContentType() ) ;
		att.setAttachmentType( StringUtil.getClassName( bo.getClass() ) ) ;
		att.setAttachmentTypeId( billId )  ;
		att.setAttachmentField( attachmentCode ) ;
		att.setWriter(writer) ;
		att.setWriteDate( new Date() ) ;
		
		return att ;
	}
	
	
	
	/**
	 * Convenience method to get the request
	 * 
	 * @return current request
	 */
	protected HttpServletRequest getRequest() {
		return ServletActionContext.getRequest();
	}

	/**
	 * Convenience method to get the response
	 * 
	 * @return current response
	 */
	protected HttpServletResponse getResponse() {
		return ServletActionContext.getResponse();
	}

	/**
	 * Convenience method to get the session
	 */
	protected HttpSession getSession() {
		return getRequest().getSession();
	}
	
	/**
	 * print log message
	 * 
	 * @param e
	 */
	public void log(String message, Exception e) {
		log.error(message, e);
		e.printStackTrace() ;
	}

	public void setListValue(List listValue) {
		this.listValue = listValue;
	}

	public List getListValue() {
		return listValue;
	}
	public RollPage getRollPage() {
		if( rollPage == null ) {
			rollPage = new RollPage(0) ; 
		}
		
		if( startIndex != null && startIndex.length()>0  ) {
			rollPage.setPageCur( Integer.parseInt( startIndex )/ Integer.parseInt( pageSize ) ) ;
		}else {
			rollPage.setPageCur( 0 ) ;
		}
		
		if( pageSize != null && pageSize.length()>0 ) {
			rollPage.setPagePer( Integer.parseInt( pageSize ) ) ;
		}else {
			rollPage.setPagePer( 10 ) ;
		}		
		return  rollPage ;
	}
	public RollPage getRollPageDataTables() {
		if( rollPage == null ) {
			rollPage = new RollPage(0) ; 
		}
		
		if( startIndex != null && startIndex.length()>0  ) {
			rollPage.setPageCur( Integer.parseInt( startIndex )/ Integer.parseInt( pageSize ) ) ;
		}else {
			rollPage.setPageCur( 0 ) ;
		}
		
		if( pageSize != null && pageSize.length()>0 ) {
			rollPage.setPagePer( Integer.parseInt( pageSize ) ) ;
		}else {
			rollPage.setPagePer( 10 ) ;
		}
		if(!StringUtil.isBlank(orderColumn)){
			rollPage.setOrderColumn(orderColumn);
			rollPage.setOrderDir(orderDir);
		}
		return  rollPage ;
	}
	public RollPage getRollPageDataTablesIndex() {
		if( rollPage == null ) {
			rollPage = new RollPage(0) ; 
		}
		
		if( page != null && page.length()>0  ) {
			rollPage.setPageCur( Integer.parseInt( startIndex )/ Integer.parseInt( pageSize ) ) ;
		}else {
			rollPage.setPageCur( 0 ) ;
		}
		
		
		rollPage.setPagePer( 5 ) ;
		
		if(!StringUtil.isBlank(orderColumn)){
			rollPage.setOrderColumn(orderColumn);
			rollPage.setOrderDir(orderDir);
		}
		return  rollPage ;
	}
	/**
	 * 获取页面dataTable使用的json字符串
	 * @param president
	 * @throws Exception
	 * @author luguanglei
	 */
	public void getPagejsonDataTables(List president) throws Exception {
		JsonConfig jsonConfig = new JsonConfig(); 
		//转换json解析Date类型时的输出格式
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonValueProcessor(){

			public Object processArrayValue(Object arg0, JsonConfig arg1) {
				// TODO Auto-generated method stub
				return null;
			}

			public Object processObjectValue(String arg0, Object arg1,JsonConfig arg2){
				 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
				 String date = "";
				 if(arg1 != null){
					 date = sdf.format(((Date)arg1));    
				 }
				 return date; 
			}});
		JSONObject json = new JSONObject();
		if(president==null){
			jsonMap.put("pageData", "");
			jsonMap.put("total", this.getRollPageDataTables().getRowCount());
			jsonMap.put("draw", "1");
			json.putAll(jsonMap, jsonConfig);
		}else{
			jsonMap.put("pageData", president);
			jsonMap.put("total", this.getRollPageDataTables().getRowCount());
			jsonMap.put("draw", draw);
			json.putAll(jsonMap, jsonConfig);
		}
		
		returnJson = json.toString();
		PrintWriter writer = getResponse().getWriter();  
        writer.print(returnJson);  
        writer.flush();  
        writer.close(); 
	}
	public void getPagejsonDataTablesByLongDate(List president) throws Exception {
		JsonConfig jsonConfig = new JsonConfig(); 
		//转换json解析Date类型时的输出格式
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonValueProcessor(){

			public Object processArrayValue(Object arg0, JsonConfig arg1) {
				// TODO Auto-generated method stub
				return null;
			}

			public Object processObjectValue(String arg0, Object arg1,JsonConfig arg2){
				 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
				 String date = "";
				 if(arg1 != null){
					 date = sdf.format(((Date)arg1));    
				 }
				 if(date.contains(" 00:00:00")) date=date.replace(" 00:00:00", "");
				 return date; 
			}});

		JSONObject json = new JSONObject();
		if(president==null){
			jsonMap.put("pageData", "");
			jsonMap.put("total", this.getRollPageDataTables().getRowCount());
			jsonMap.put("draw", "1");
			json.putAll(jsonMap, jsonConfig);
		}else{
			jsonMap.put("pageData", president);
			jsonMap.put("total", this.getRollPageDataTables().getRowCount());
			jsonMap.put("draw", draw);
			json.putAll(jsonMap, jsonConfig);
		}
		
		returnJson = json.toString();
		
		PrintWriter writer = getResponse().getWriter();  
        writer.print(returnJson);  
        writer.flush();  
        writer.close(); 
	}
	/**
	 * @title 发送邮件通用
	 * @return
	 * @throws BaseException 
	 */
	public int saveSendMailToUsers(String userEmail,String subject,String content,String userNameCn) throws BaseException {
		int status = 1;
		sysEmailSmsLog=new SysEmailSmsLog();
		try{
			content+="<br>平台网址：http://www.yunqicai.cn";
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			if("0".equals(systemConfiguration.getMailFalg())){
				//log.info("标题："+subject);
				//log.info("内容："+content);
				sysEmailSmsLog.setAddress(userEmail);
				sysEmailSmsLog.setComId(comId);
				sysEmailSmsLog.setSendDate(DateUtil.getCurrentDateTime());
				sysEmailSmsLog.setSendName(userNameCn);
				sysEmailSmsLog.setTitle(subject);
				sysEmailSmsLog.setContent(content);
				sysEmailSmsLog.setType(1L);
				
				util.sendHtmlMail(userEmail, subject, content);
				
				status=0;
				sysEmailSmsLog.setStatus(status);
				this.iSysEmailSmsLogBiz.saveSysEmailSmsLog(sysEmailSmsLog);
			}
		} catch (Exception e) {
			log("发送邮件通用功能错误！", e);
			status=1;
			sysEmailSmsLog.setDescription(e.getMessage());
			this.iSysEmailSmsLogBiz.saveSysEmailSmsLog(sysEmailSmsLog);
		}
		return status;
	}
	/**
	 * @title 发送邮件通用 供应商发送专用
	 * @return
	 * @throws BaseException 
	 */
	public int saveSendMailToUsersSupplier(String userEmail,String subject,String content,String userNameCn) throws BaseException{
		int status = 1;
		sysEmailSmsLog=new SysEmailSmsLog();
		try{
			content+="<br>平台网址：http://www.yunqicai.cn";
				//log.info("标题："+subject);
				//log.info("内容："+content);
				sysEmailSmsLog.setAddress(userEmail);
				sysEmailSmsLog.setComId(0L);
				sysEmailSmsLog.setSendDate(DateUtil.getCurrentDateTime());
				sysEmailSmsLog.setSendName(userNameCn);
				sysEmailSmsLog.setTitle(subject);
				sysEmailSmsLog.setContent(content);
				sysEmailSmsLog.setType(1L);
			
				util.sendHtmlMail(userEmail, subject, content);
				
				status=0;
				sysEmailSmsLog.setStatus(status);
				this.iSysEmailSmsLogBiz.saveSysEmailSmsLog(sysEmailSmsLog);
				
		} catch (Exception e) {
			log("发送邮件通用功能错误！", e);
			status=1;
			sysEmailSmsLog.setDescription(e.getMessage());
			this.iSysEmailSmsLogBiz.saveSysEmailSmsLog(sysEmailSmsLog);
		}
		return status;
	}
	/**
	 * title 消息提醒通用
	 * @param senduserName
	 * @param sendName
	 * @param receiveUserName
	 * @param receiveName
	 * @param type
	 * @param content
	 * @return
	 */
	public boolean saveSysMessage(String sendNameCn,String sendName,String receiveNameCn,String receiveName,int type,String content,Long comId) {
		boolean flag = true;
		try{
			sysMessageLog=new SysMessageLog();
			sysMessageLog.setComId(comId);
			sysMessageLog.setReceivePersonCn(receiveNameCn);
			sysMessageLog.setReceivePerson(receiveName);
			sysMessageLog.setSendPersonCn(sendNameCn);
			sysMessageLog.setSendPerson(sendName);
			sysMessageLog.setMessageType("0");
			sysMessageLog.setStatus("0");
			sysMessageLog.setMessageContent(content);
			sysMessageLog.setSendDate(DateUtil.getCurrentDateTime());
			sysMessageLog.setType(type);
			this.iSysMessageLogBiz.saveSysMessageLog(sysMessageLog);
		    flag=true;
		} catch (Exception e) {
			log("消息提醒通用错误！", e);
		}
		return flag;
	}
	/**
	 * @title 发送短信通用
	 * @return
	 * @throws BaseException 
	 */
	public int saveSmsMessageToUsers(String mobiles,String content,String userNameCn) {
		int status = 1;
		String description="";
		URL url = null;
		String UserName="qicaiyida";
		String Password="1q2w3e4r!@";
		try{
			if (!StringUtil.isBlank(mobiles)) {
				content = content.replace("【", "");
				content = content.replace("】", "");
				content += "【电子采购平台】";
				Long comId = UserRightInfoUtil.getComId(getRequest());
				SystemConfiguration systemConfiguration = BaseDataInfosUtil
						.convertSystemConfiguration(comId);
				if ("0".equals(systemConfiguration.getMessageFalg())) {
					// log.info("手机号："+mobiles);
					// log.info("内容："+content);
					String strUrl = "http://api.sms1086.com/api/Send.aspx?username="
							+ URLEncoder.encode(UserName, "GB2312")
							+ "&password="
							+ java.net.URLEncoder.encode(Password, "GB2312")
							+ "&mobiles="
							+ java.net.URLEncoder.encode(mobiles, "GB2312")
							+ "&content="
							+ java.net.URLEncoder.encode(content, "GB2312");
					url = new URL(strUrl);
					URLConnection UConn = url.openConnection();
					BufferedReader breader = new BufferedReader(
							new InputStreamReader(UConn.getInputStream()));
					String str = breader.readLine();
					while (str != null) {
						str = URLDecoder.decode(str, "GB2312");
						String[] strs = str.split("&");
						if (strs[0].replace("result=", "").trim().equals("0")) {
							status = 1;
							str = "恭喜，短信发送成功。";
						} else {
							status = 1;
							str = "短信发送失败。失败原因："
									+ strs[1].replace("description=", "");
							description = strs[1].replace("description=", "");
						}
						str = breader.readLine();
					}
					if (mobiles.contains(",")) {
						String[] m_str = mobiles.split(",");
						for (String m : m_str) {
							sysEmailSmsLog = new SysEmailSmsLog();
							sysEmailSmsLog.setAddress(m);
							sysEmailSmsLog.setComId(comId);
							sysEmailSmsLog.setSendDate(DateUtil
									.getCurrentDateTime());
							sysEmailSmsLog.setSendName(userNameCn);
							sysEmailSmsLog.setTitle(content);
							sysEmailSmsLog.setContent(content);
							sysEmailSmsLog.setType(0L);
							sysEmailSmsLog.setStatus(status);
							sysEmailSmsLog.setDescription(description);
							this.iSysEmailSmsLogBiz
									.saveSysEmailSmsLog(sysEmailSmsLog);
						}
					} else {
						sysEmailSmsLog = new SysEmailSmsLog();
						sysEmailSmsLog.setAddress(mobiles);
						sysEmailSmsLog.setComId(comId);
						sysEmailSmsLog.setSendDate(DateUtil
								.getCurrentDateTime());
						sysEmailSmsLog.setSendName(userNameCn);
						sysEmailSmsLog.setTitle(content);
						sysEmailSmsLog.setContent(content);
						sysEmailSmsLog.setType(0L);
						sysEmailSmsLog.setStatus(status);
						sysEmailSmsLog.setDescription(description);
						this.iSysEmailSmsLogBiz
								.saveSysEmailSmsLog(sysEmailSmsLog);
					}
				}
			}
		} catch (Exception e) {
			log("发送短信通用功能错误！", e);
		}
		return status;
	}
	/**
	 * @title 发送短信通用  供应商发送短信用
	 * @return
	 * @throws BaseException 
	 */
	public int saveSmsMessageToUsersSupplier(String mobiles,String content,String userNameCn) {
		int status = 1;
		String description="";
		URL url = null;
		String UserName="qicaiyida";
		String Password="1q2w3e4r!@";
		try{

			content = content.replace("【", "");
			content = content.replace("】", "");
			content += "【电子采购平台】";
			String strUrl = "http://api.sms1086.com/api/Send.aspx?username="
					+ URLEncoder.encode(UserName, "GB2312") + "&password="
					+ java.net.URLEncoder.encode(Password, "GB2312")
					+ "&mobiles="
					+ java.net.URLEncoder.encode(mobiles, "GB2312")
					+ "&content="
					+ java.net.URLEncoder.encode(content, "GB2312");
			url = new URL(strUrl);
			URLConnection UConn = url.openConnection();
			BufferedReader breader = new BufferedReader(new InputStreamReader(
					UConn.getInputStream()));
			String str = breader.readLine();
			while (str != null) {
				str = URLDecoder.decode(str, "GB2312");
				String[] strs = str.split("&");
				if (strs[0].replace("result=", "").trim().equals("0")) {
					status = 1;
					str = "恭喜，短信发送成功。";
				} else {
					status = 1;
					str = "短信发送失败。失败原因：" + strs[1].replace("description=", "");
					description = strs[1].replace("description=", "");
				}
				str = breader.readLine();
			}
			if (mobiles.contains(",")) {
				String[] m_str = mobiles.split(",");
				for (String m : m_str) {
					sysEmailSmsLog = new SysEmailSmsLog();
					sysEmailSmsLog.setAddress(m);
					sysEmailSmsLog.setComId(0L);
					sysEmailSmsLog.setSendDate(DateUtil.getCurrentDateTime());
					sysEmailSmsLog.setSendName(userNameCn);
					sysEmailSmsLog.setTitle(content);
					sysEmailSmsLog.setContent(content);
					sysEmailSmsLog.setType(0L);
					sysEmailSmsLog.setStatus(status);
					sysEmailSmsLog.setDescription(description);
					this.iSysEmailSmsLogBiz.saveSysEmailSmsLog(sysEmailSmsLog);
				}
			} else {
				sysEmailSmsLog = new SysEmailSmsLog();
				sysEmailSmsLog.setAddress(mobiles);
				sysEmailSmsLog.setComId(0L);
				sysEmailSmsLog.setSendDate(DateUtil.getCurrentDateTime());
				sysEmailSmsLog.setSendName(userNameCn);
				sysEmailSmsLog.setTitle(content);
				sysEmailSmsLog.setContent(content);
				sysEmailSmsLog.setType(0L);
				sysEmailSmsLog.setStatus(status);
				sysEmailSmsLog.setDescription(description);
				this.iSysEmailSmsLogBiz.saveSysEmailSmsLog(sysEmailSmsLog);
			}
		} catch (Exception e) {
			log("发送短信通用功能错误！", e);
		}
		return status;
	}
	/**
	 * @title 查询审批记录
	 * @return
	 * @throws BaseException 
	 */
	public List<WFHistoryTask> getHistoryTaskInfo(String orderId) {
		
		List<WFHistoryTask> wfhytList = new ArrayList<WFHistoryTask>();
		try{
			HistoryTask hyTask=null;
			WFHistoryTask wfhyt=null;
			QueryFilter qfilter = new QueryFilter();
			qfilter.setOrderId(orderId);
			qfilter.setOrder("asc");
			qfilter.setOrderBy("finish_Time");
			List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(qfilter);
			for(Iterator iter = hytList.iterator();iter.hasNext();){
				hyTask = (HistoryTask)iter.next();
				wfhyt = new WFHistoryTask();
				BeanUtils.copyProperties(wfhyt, hyTask);
				wfhyt.setPerformType(hyTask.getVariableMap().get("result") == null ? "0" :hyTask.getVariableMap().get("result").toString());
				wfhyt.setVariable(hyTask.getVariableMap().get("signContent") == null ? "" :hyTask.getVariableMap().get("signContent").toString());
				wfhyt.setOperatorCn(BaseDataInfosUtil.convertLoginNameToChnName(wfhyt.getOperator()));
				wfhytList.add(wfhyt);
				
			}
		} catch (Exception e) {
			log("查询审批记录出错！", e);
		}
		return wfhytList;
		
	}
	/**
	 * @title 获取任务流程处理人（英文）
	 * @return
	 * @throws BaseException 
	 */
	public Map<String,Object> getTaskActors(List<Order> orderList) {
		String actorsEn = "";
		String actorsCn = "";
		Map<String,Object> map=null;
		try{
			if(orderList!=null&&orderList.size()>0){
				Order od = orderList.get(0);
				List<Task> taskList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(od.getId()));
				if(taskList!=null&&taskList.size()>0){
					Task task = taskList.get(0);
					String[] taskActors = facets.getEngine().query().getTaskActorsByTaskId(task.getId());
					if(taskActors!=null){
						map=new HashMap<String, Object>();
						for(int i=0;i<taskActors.length;i++){
							if(i==taskActors.length-1){
								actorsEn += taskActors[i];
								actorsCn += BaseDataInfosUtil.convertLoginNameToChnName(taskActors[i]);
							}else{
							    actorsEn += taskActors[i]+",";
							    actorsCn += BaseDataInfosUtil.convertLoginNameToChnName(taskActors[i])+",";
							}
						}
						map.put("actorsEn", actorsEn);
						map.put("actorsCn", actorsCn);
					}
				}
			}
		} catch (Exception e) {
			log("获取任务流程处理人信息错误！", e);
		}
		return map;
	}
	
	//ajax初始化（通用）
	public void ajaxHeadInit() throws IOException {
		this.getRequest().setCharacterEncoding("UTF-8");
		this.getResponse().setCharacterEncoding("UTF-8");
		this.getResponse().setContentType("text/xml");
		this.getResponse().setHeader("pragma", "no-cache");
		this.getResponse().setHeader("cache-control", "no-cache");
		//this.getResponse().setHeader( "Set-Cookie", "name=value; HttpOnly"); 
	}

	protected ServletContext getServletContext() {
		return ServletActionContext.getServletContext() ;
	}
	
	public void setRollPage(RollPage rollPage) {
		this.rollPage = rollPage;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getPage() {
		return page;
	}
	/**
	 * 解析附件删除ID
	 * @param atts 附件删除ID
	 * @return
	 */
	public String[] parseAttachIds( String atts ){
		String [] rValues = null ;
		if( atts != null && atts.length()>0 ) {
			rValues =  atts.split(",") ;
		}
		return rValues ;
	}
	public SnakerEngineFacets getFacets() {
		return facets;
	}

	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}

	public String getDraw() {
		return draw;
	}

	public void setDraw(String draw) {
		this.draw = draw;
	}

	public String getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(String startIndex) {
		this.startIndex = startIndex;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderColumn() {
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public String getOrderDir() {
		return orderDir;
	}

	public void setOrderDir(String orderDir) {
		this.orderDir = orderDir;
	}

	
	
	
}
