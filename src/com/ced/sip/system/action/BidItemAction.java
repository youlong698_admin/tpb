package com.ced.sip.system.action;

import java.io.PrintWriter;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.system.biz.IBidItemBiz;
import com.ced.sip.system.entity.BidItem;

public class BidItemAction extends BaseAction {
	// 评标项 服务类
	private IBidItemBiz iBidItemBiz;
	// 评标项实例
	private BidItem bidItem ;
	String biId;
	/**
	 * 查看评标项信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidItem() throws BaseException {
		
		try{
          this.getRequest().setAttribute("param", bidItem.getItemType());
		} catch (Exception e) {
			log.error("查看评标项信息列表错误！", e);
			throw new BaseException("查看评标项信息列表错误！", e);
		}
		
		return VIEW ;
		
	}
	/**
	 * 查看评标项信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findBidItem() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(bidItem==null)
			{
				bidItem=new BidItem();
			}
			String itemName=this.getRequest().getParameter("itemName");
			bidItem.setComId(comId);
			bidItem.setItemName(itemName);
			// 采购项目 列表信息
			this.setListValue(this.iBidItemBiz.getBidItemList(this.getRollPageDataTables(), bidItem));
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看采购项目信息列表错误！", e);
			throw new BaseException("查看采购项目信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 保存评标项信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidItemInit() throws BaseException {
		try{
			
		} catch (Exception e) {
			log("保存评标项信息初始化错误！", e);
			throw new BaseException("保存评标项信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	/**
	 * 查看明细初始化
	 * @return
	 * @throws BaseException 
	 */
	public String openNew() throws BaseException {
		try{
			bidItem=this.iBidItemBiz.getBidItem( bidItem.getBiId() );
		} catch (Exception e) {
			log("修改评标项信息初始化错误！", e);
			throw new BaseException("修改评标项信息初始化错误！", e);
		}
		return OPEN_NEW;
		
	}
	/**
	 * 保存评标项信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidItem() throws BaseException {
		
		try{
			// 保存BidItem信息
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(bidItem != null){
				String str = bidItem.getItemDetail().replace(" ", "");
				bidItem.setItemDetail(str);
				bidItem.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
				bidItem.setWriteDate(DateUtil.getCurrentDateTime());
				bidItem.setComId(comId);			
				this.iBidItemBiz.saveBidItem( bidItem );
				this.getRequest().setAttribute("message","新增成功!");
			}
		} catch (Exception e) {
			log("保存评标项信息错误！", e);
			throw new BaseException("保存评标项信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改评标项信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidItemInit() throws BaseException {
		
		try{
			// 取得BidItem信息
			bidItem=this.iBidItemBiz.getBidItem( bidItem.getBiId() );
		} catch (Exception e) {
			log("修改评标项信息初始化错误！", e);
			throw new BaseException("修改评标项信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改评标项信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidItem() throws BaseException {
		
		try{
			// 修改BidItem信息
			this.iBidItemBiz.updateBidItem( bidItem );
			this.getRequest().setAttribute("message","修改成功!");
			this.getRequest().setAttribute("operModule", "修改评标项信息");
		} catch (Exception e) {
			log("修改评标项信息错误！", e);
			throw new BaseException("修改评标项信息错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 删除评标项信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteBidItem() throws BaseException {
		try{
			PrintWriter out = this.getResponse().getWriter();
			String ids =this.getRequest().getParameter("ids");
			if(ids!=null){
				String[] idArr = ids.split(",");
				for(int i=0;i<idArr.length;i++){
					Long id = Long.parseLong(idArr[i]);
					bidItem=this.iBidItemBiz.getBidItem(id);
					bidItem.setIsUsable("1");
					this.iBidItemBiz.updateBidItem(bidItem);
				}
			}
			
			String message="删除成功";
  			out.print(message);
			
			// 删除BidItem信息
  			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除评标项信息");
		} catch (Exception e) {
			log("删除评标项信息错误！", e);
			throw new BaseException("删除评标项信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看评标项明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidItemDetail() throws BaseException {
		
		try{
			// 取BidItem信息
			bidItem=this.iBidItemBiz.getBidItem( bidItem.getBiId() );
		} catch (Exception e) {
			log("查看评标项明细信息错误！", e);
			throw new BaseException("查看评标项明细信息错误！", e);
		}
		return DETAIL;
		
	}
	
	/**
	 * 提交评标项信息
	 * @return
	 * @throws BaseException 
	 */
	public String submitBidItem() throws BaseException {
		
		try{
//			this.iBidItemBiz.submitSubjects( subjects  );
		} catch (Exception e) {
			log("提交评标项信息错误！", e);
			throw new BaseException("提交评标项信息错误！", e);
		}
		return SUBMIT;
		
	}

   /**
    * 查看评委评标 评分项目明细
    * @return
	* @throws BaseException 
    */
	public String findBidItemDetail() throws BaseException {
		try{
			biId = this.getRequest().getParameter("itemId");
			
			if(bidItem == null){
				bidItem = new BidItem();
			}
			bidItem.setBiId( Long.parseLong(biId));
			bidItem=this.iBidItemBiz.getBidItem( bidItem.getBiId() );
		} catch (Exception e) {
			log("查看评委评标 评分项目明细错误！", e);
			throw new BaseException("查看评委评标 评分项目明细错误！", e);
		}
		return "detail";
		
	}
	
	public IBidItemBiz getiBidItemBiz() {
		return iBidItemBiz;
	}
	public void setiBidItemBiz(IBidItemBiz iBidItemBiz) {
		this.iBidItemBiz = iBidItemBiz;
	}
	
	public BidItem getBidItem() {
		return bidItem;
	}

	public void setBidItem(BidItem bidItem) {
		this.bidItem = bidItem;
	}
	public String getBidId() {
		return biId;
	}
	public void setBidId(String biId) {
		this.biId = biId;
	}
	
}
