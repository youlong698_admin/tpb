package com.ced.sip.system.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.CodeGetByConditionsUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.HanziZhuanhuan;
import com.ced.sip.common.utils.StringUtil;

import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.entity.Dictionary;
import com.ced.sip.system.entity.MaterialKind;
import com.ced.sip.system.entity.MaterialList;

public class MaterialAction extends BaseAction {

	private IMaterialBiz iMaterialBiz;
	
	private MaterialKind materialKind;
	private MaterialList materialList;
	private String iden ;
	private String ids ;
	private String message ;
	private String ss;


	private File file;
	//导入上传模块完成alert的内容
	private String contentImport;
	//导入上传模板完成后判断是否成功
	private boolean boolImport;
	
	/**
	 * 物料库管理首页
	 * @return
	 */
	public String viewMaterialInfoIndex(){
		
		return INDEX;
	}
	
	
	/**
	 * 查看采购类别信息初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMaterialKindInitTree() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log("查看采购类别信息初始树列表错误！", e);
			throw new BaseException("查看采购类别信息初始树列表错误！", e);
		}
		return TREE ;
	}
	
	/**
	 * 查看采购类别信息树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void viewMaterialKindTree() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			materialKind =new MaterialKind();
			String mkId = this.getRequest().getParameter("mkId");
			String id=this.getRequest().getParameter("id");
			if(StringUtil.isNotBlank(mkId))
			{
				if(StringUtil.isBlank(id)) id=mkId;
			}else
			{
				if(StringUtil.isBlank(id)) id="0";
			}
			materialKind.setParentId(new Long(id));
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) materialKind.setComId(comId);
			List<MaterialKind> mkList = iMaterialBiz.getMaterialKindList( materialKind );
			
			JSONArray jsonArray = new JSONArray();
			for (MaterialKind maKind:mkList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", maKind.getMkId());
				subJsonObject.element("pid", maKind.getParentId());
				subJsonObject.element("name",maKind.getMkName());
				
				if("0".equals(maKind.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);
					//subJsonObject.element("iconOpen",);
					//subJsonObject.element("iconClose","<i class='icon-folder-close'></i>");
					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
		} catch (Exception e) {
			log("查看采购类别信息初始树列表错误！", e);
			throw new BaseException("查看采购类别信息初始树列表错误！", e);
		}
	}
	
	/**
	 * 修改采购类别初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateMaterialKindInit() throws BaseException {
		
		try{
			
			materialKind = iMaterialBiz.getMaterialKind( materialKind.getMkId() ) ;
			
		} catch (Exception e) {
			log("修改采购类别初始化错误！", e);
			throw new BaseException("修改采购类别初始化错误！", e);
		}
		
		return MODIFY_INIT ;
	}
	
	/**
	 * 修改采购类别信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateMaterialKind() throws BaseException {
		try{
			
			iMaterialBiz.updateMaterialKind( materialKind ) ;
			
			materialKind.setOperType("update") ;
			
			message = " 修改成功! " ;
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改采购类别信息信息");
		} catch (Exception e) {
			log("修改采购类别信息错误！", e);
			throw new BaseException("修改采购类别信息错误！", e);
		}
		return MODIFY;
	}
	
	/**
	 * 保存采购类别信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveMaterialKindInit() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());

			String str = "";
			int strLen =0;
			if(materialKind!=null){
				materialKind = iMaterialBiz.getMaterialKind( materialKind.getMkId() ) ;
				if(materialKind.getMkCode().length()==2){
					 str = materialKind.getMkCode().substring(0,2);
				}
				else if(materialKind.getMkCode().length()==4){
					 str = materialKind.getMkCode().substring(0,4);
				}
			}else{
				materialKind=new MaterialKind();
				str="";
				materialKind.setMkCode("");
			}
			strLen = materialKind.getMkCode().length()+2;
		    List matKindList = this.iMaterialBiz.getMaterialCode(str, Integer.toString(strLen));
		    	Object obj =  matKindList.get(0);
		    	if(!StringUtil.isNotBlank(obj)){
		    		materialKind.setMaterCode(str+CodeGetByConditionsUtil.getSerialFormatObject("00").format(1).toString());
		    	}
		    	if(StringUtil.isNotBlank(obj)){
		    		String sCode = obj.toString();
			    	String sNum = sCode.substring(sCode.length()-2, sCode.length());
			    	materialKind.setMaterCode(str+CodeGetByConditionsUtil.getSerialFormatObject("00").format(Long.parseLong(sNum)+1).toString());
		    	}
			this.getRequest().setAttribute("comId", comId);
		} catch (Exception e) {
			log("保存采购类别信息初始化错误！", e);
			throw new BaseException("保存采购类别信息初始化错误！", e);
		}
		return ADD_INIT;
	}
	
	
	/**
	 * 删除采购类别信息
	 * @return
	 * @throws BaseException 
	 */
	public String delMaterialKind() throws BaseException {
		try{
			materialKind = iMaterialBiz.getMaterialKind( materialKind.getMkId() ) ;
			materialKind.setIsUsable(TableStatus.COMMON_STATUS_INVALID);
			iMaterialBiz.updateMaterialKind(materialKind ) ;
			materialKind.setOperType("delete") ;
			
			message = " 删除成功! " ;
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除采购类别信息信息");
			PrintWriter out = this.getResponse().getWriter();
			out.println(message);
		} catch (Exception e) {
			log("删除采购类别信息错误！", e);
			throw new BaseException("删除采购类别信息错误！", e);
		}
		
		return null;
		
	}

	/**
	 * 保存采购类别信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveMaterialKind() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			MaterialKind parentdt=null;
			// 更新父节点中是否有子节点为存在 
			if(materialKind.getParentId()==null){
				parentdt=new MaterialKind();
			    parentdt.setIsHaveChild("0");
			    parentdt.setLevels(0);
			    parentdt.setSelflevCode("");
			    materialKind.setParentId(0L);
			}else{			
				parentdt= iMaterialBiz.getMaterialKind( materialKind.getParentId() ) ;
			    parentdt.setIsHaveChild("0");
			    iMaterialBiz.updateMaterialKind( parentdt ) ;
			}
			
			materialKind.setLevels( parentdt.getLevels() + 1 ) ;
			materialKind.setComId(comId);
			materialKind.setWriteDate(DateUtil.getCurrentDateTime() ) ;
			materialKind.setWriter( UserRightInfoUtil.getUserName( this.getRequest()) ) ;
			
			// 保存采购类别表
			iMaterialBiz.saveMaterialKind( materialKind ) ;
			
			materialKind.setSelflevCode( parentdt.getSelflevCode() + "," + materialKind.getMkId().toString() ) ;
			
			// 更新级联码
			iMaterialBiz.updateMaterialKind( materialKind ) ;
			
			materialKind.setOperType("add") ;
			
            this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "保存采购类别信息");
			
		} catch (Exception e) {
			log("保存采购类别信息错误！", e);
			throw new BaseException("保存采购类别信息错误！", e);
		}
		
		return ADD;
		
	}
	
	/**
	 * 查看物料信息列表（默认显示所有）
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMaterialAllList() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看物料信息列表（默认显示所有）错误！", e);
			throw new BaseException("查看物料信息列表（默认显示所有）错误！", e);
		}
		return "viewAll" ;
	}
	
	public String findMaterialAllList() throws BaseException {
		
		try{
			if(materialList==null)
			{
				materialList=new MaterialList();
			}
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String materialName=this.getRequest().getParameter("materialName");
			String mkKindId=this.getRequest().getParameter("mkKindId");
			materialList.setMaterialName(materialName);
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) materialList.setComId(comId);
			if(StringUtil.isNotBlank(mkKindId))materialList.setMkKindId(new Long(mkKindId));
			
			//String iden=this.getRequest().getParameter("iden");
			this.setListValue( this.iMaterialBiz.getMaterialListList(getRollPageDataTables(), materialList));
			
			this.getPagejsonDataTables(this.getListValue());
			
		} catch (Exception e) {
			log.error("查看物料信息列表（默认显示所有）错误！", e);
			throw new BaseException("查看物料信息列表（默认显示所有）错误！", e);
		}
		return "viewAll" ;
	}
	
	/**
	 * 查看物料信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMaterialList() throws BaseException {
		
		try{
			String mkKindId=this.getRequest().getParameter("mkKindId");
			
			this.getRequest().setAttribute("mkKindId", mkKindId);
			
		} catch (Exception e) {
			log.error("查看物料信息列表错误！", e);
			throw new BaseException("查看物料信息列表错误！", e);
		}
		
		return VIEW ;
		
	}
	/**
	 * 查看物料信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String uptateTre() throws BaseException {
		
		try{
			if (StringUtil.isNotBlank(ss)) {
				materialKind.setMkId(Long.valueOf(ss));
				materialKind = iMaterialBiz.getMaterialKind(materialKind.getMkId());
			}
		} catch (Exception e) {
			log.error("查看物料信息列表错误！", e);
			throw new BaseException("查看物料信息列表错误！", e);
		}
		
		return "updateKind" ;
		
	}
	/**
	 * 查看物料信息列表 Ushine
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findAllMaterialList() throws BaseException {
		
		try{

			Long comId=UserRightInfoUtil.getComId(getRequest());
			
			String mkId=this.getRequest().getParameter("mkId");
			String materialName=this.getRequest().getParameter("materialName");
			if(materialList==null){
				materialList = new MaterialList();
			}
				if(StringUtil.isNotBlank(mkId)){
					materialKind = iMaterialBiz.getMaterialKind(materialKind.getMkId());		
					materialList.setMkKindId(new Long(mkId));
				}

			materialList.setMaterialName(materialName);
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) materialList.setComId(comId);
			this.setListValue( iMaterialBiz.getMaterialListList(getRollPageDataTables(), materialList));
			
			this.getPagejsonDataTables(this.getListValue());
			
		} catch (Exception e) {
			log.error("查看物料信息列表错误！", e);
			throw new BaseException("查看物料信息列表错误！", e);
		}
		
		return VIEW ;
		
	}
	
	/**
	 * 导出物料库EXCEL
	 * Ushine 2016-11-15
	 * @return
	 * @throws BaseException
	 */
	public void exportMaterialList() throws BaseException {
		try{
			  List<String> titleList = new ArrayList<String>();
			 titleList.add("物料分类");
			 titleList.add("编码");
			 titleList.add("名称");
			 titleList.add("规格型号");
			 titleList.add("计量单位");
			 titleList.add("最新采购单价");
			 Long comId=UserRightInfoUtil.getComId(getRequest());
			 materialList=new MaterialList();
			 materialList.setComId(comId);
			 List list = iMaterialBiz.getMaterialListList(materialList); // 获取数据集合,非分页
				
				
				
				List<Object[]> objList = new ArrayList<Object[]>();
				for (int i = 0; i < list.size(); i++) {
					MaterialList materialList = (MaterialList) list.get(i);
					if (StringUtil.isNotBlank(materialList)) {
						Object[] obj = new Object[] {
							materialList.getMkKindName(),	
							materialList.getMaterialCode(),
							materialList.getMaterialName(),
							materialList.getMaterialType(),
							materialList.getUnit(),
							StringUtil.formatDouble(materialList.getPrice())
						};
						objList.add(obj);
					}
				}
				// 输出的excel文件名
				String file = "物料库信息.xls";
				String targetfile = this.getServletContext().getRealPath(file);
                
				List<Map<String,Object>> excelList=new ArrayList<Map<String,Object>>();
				Map<String,Object> map=new HashMap<String,Object>();
				map.put("worksheet", "物料库信息");
				map.put("titleList", titleList);
				map.put("valueList", objList);
				excelList.add(map);
				// 调用JXL方法
				new ExcelUtil().expCommonExcel(targetfile,excelList);
				
				this.getResponse().setContentType("application/octet-stream; charset=utf-8");
				this.getResponse().setHeader("Content-Disposition", "attachment; filename="+new String(file.getBytes("gbk"),"iso-8859-1") );
				File files = new File(targetfile);
				FileInputStream is = new FileInputStream(files);
				OutputStream os = this.getResponse().getOutputStream();
				byte[] buff = new byte[1024];
				int readCount = 0;
				readCount = is.read(buff);
				while (readCount != -1) {
					os.write(buff, 0, readCount);
					readCount = is.read(buff);
				}
				if (is != null) {
					is.close();
				}
				if (os != null) {
					os.close();
				}
		} catch (Exception e) {
			log.error("导出物料库信息列表错误！", e);
			throw new BaseException("导出物料库信息列表错误！", e);
		}
		
	}
	/**
	 * 新增物料信息初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveMaterialInit() throws BaseException {
		
		try{
		 Long comId=UserRightInfoUtil.getComId(getRequest());
		 String str = "";
		 materialKind = iMaterialBiz.getMaterialKind(materialKind.getMkId());
		 //if(materialKind.getMkCode().length()==6){
		//	 str = materialKind.getMkCode().substring(0,6);
		 //}
		 str=materialKind.getMkCode();
		 int codeLen = materialKind.getMkCode().length()+4;
		 List materList = this.iMaterialBiz.getMaterialList(str, Integer.toString(codeLen));
		 Object obj = materList.get(0);
		 if(materialList ==null){
			 materialList = new MaterialList();
		 }
		 if(!StringUtil.isNotBlank(obj)){
			 materialList.setMaterCode(str+CodeGetByConditionsUtil.getSerialFormatObject("0000").format(1).toString());
	    	}
		 if(StringUtil.isNotBlank(obj)){
			 String sCode = obj.toString();
			 String sNum = sCode.substring(sCode.length()-2, sCode.length());
			 materialList.setMaterCode(str+CodeGetByConditionsUtil.getSerialFormatObject("0000").format(Long.parseLong(sNum)+1).toString());
		 	}
		 this.getRequest().setAttribute("comId", comId);
		 List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1711);
		 this.getRequest().setAttribute("dictionaryList", dictionaryList);
		} catch (Exception e) {
			log.error("新增物料信息初始化错误！", e);
			throw new BaseException("新增物料信息初始化错误！", e);
		}
		
		return "listAddInit" ;
		
	}
	
	/**
	 * 新增物料信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveMaterial() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			materialKind = iMaterialBiz.getMaterialKind(materialList.getMkKindId());
			materialList.setComId(comId);
			materialList.setMnemonicCode(HanziZhuanhuan.getBeginCharacter(materialList.getIsUsable()));
			iMaterialBiz.saveMaterialList(materialList);
			
			this.getRequest().setAttribute("message", "保存成功");
		    this.getRequest().setAttribute("operModule", "保存物料信息");
		} catch (Exception e) {
			log.error("新增物料信息错误！", e);
			throw new BaseException("新增物料信息错误！", e);
		}
		
		return "viewAll" ;
		
	}
	
	/**
	 * 修改物料信息初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateMaterialInit() throws BaseException {		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			materialList = iMaterialBiz.getMaterialList(materialList.getMaterialId());
			materialKind = iMaterialBiz.getMaterialKind(materialList.getMkKindId());
			this.getRequest().setAttribute("comId", comId);
			//获取计量单位
			 List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1711);
			 this.getRequest().setAttribute("dictionaryList", dictionaryList);
		} catch (Exception e) {
			log.error("修改物料信息初始化错误！", e);
			throw new BaseException("修改物料信息初始化错误！", e);
		}
		
		return "listUpdateInit" ;
		
	}

	/**
	 * 修改物料信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateMaterial() throws BaseException {
		
		try{
			materialKind = iMaterialBiz.getMaterialKind(materialList.getMkKindId());
			materialList.setMnemonicCode(HanziZhuanhuan.getBeginCharacter(materialList.getIsUsable()));
			iMaterialBiz.updateMaterialList(materialList);
			
			this.getRequest().setAttribute("message", "修改成功");
		    this.getRequest().setAttribute("operModule", "修改物料信息");
		} catch (Exception e) {
			log.error("修改物料信息错误！", e);
			throw new BaseException("修改物料信息错误！", e);
		}
		
		return "viewAll" ;
		
	}
	
	/**
	 * 删除物料信息
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 */
	public String deleteMaterial() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			String idss[] = ids.split(",");
			// 删除物料信息  伪删
			
			for(int i=0;i<idss.length;i++){
				if(!(idss[i].equals(" "))&&idss[i]!=null){
					materialList = iMaterialBiz.getMaterialList(new Long(idss[i]));
					materialList.setIsUsable(TableStatus.COMMON_STATUS_INVALID);
					iMaterialBiz.deleteMaterialList(materialList);
				}
			}
			
			String message="删除成功";
			out.println(message);
			this.getRequest().setAttribute("message", message);
		    this.getRequest().setAttribute("operModule", "删除物料信息");
		} catch (Exception e) {
			log("删除物料信息错误！", e);
			out.println("删除失败");
			throw new BaseException("删除物料信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看物料信息明细
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMaterialDetail() throws BaseException {
		
		try{
			materialList = iMaterialBiz.getMaterialList(materialList.getMaterialId());
			materialKind = iMaterialBiz.getMaterialKind(materialList.getMkKindId());
			this.getRequest().setAttribute("param", this.getRequest().getParameter("param"));
	
		} catch (Exception e) {
			log.error("查看物料信息明细错误！", e);
			throw new BaseException("查看物料信息明细错误！", e);
		}
		
		return "listDetail" ;
		
	}
	/**
	 * 物料数据信息Excel导入
	 * @return
	 * @throws BaseException 
	 */
	public String uploadImpMaterialList() throws BaseException {
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			//保存物料导入数据
			ExcelUtil eu = new ExcelUtil();
			String[][] materialListExcel = eu.readExcel(file.getPath());
			List materialListList = getMaterialExcel(materialListExcel);
			if(boolImport){
				for(int i=0;i<materialListList.size();i++){
					//保存物料数据
					materialList = (MaterialList) materialListList.get(i);
					materialList.setIsUsable("0");
					materialList.setComId(comId);
					this.iMaterialBiz.saveMaterialList(materialList);
				}
				contentImport = "成功";
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("contentImport", contentImport);
			this.getRequest().setAttribute("operModule", "导入物料数据信息");
			this.getRequest().setAttribute("message", contentImport);
			out.print(jsonObject.toString());
		} catch (Exception e) {			
			contentImport="导入失败！（模板信息有误，请检查模板信息无误后再导入）";
			log("导入物料数据信息错误！", e);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("contentImport", contentImport);
			out.print(jsonObject.toString());
		}	
		return null;
		
	}
	
	/**
	 * 评分项信息Excel导入
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialExcel(String[][] materialExcel) throws Exception{
		List materialListList = new ArrayList();
		boolImport = true;
		contentImport = "";
		MaterialList maList=null;
		Set set=new HashSet();
		String materilKindName = "",materilKindNameTmp="",materilCode = "",materilName = "",materilType = "",unit = "",remark="";
		for(int i=3;i<materialExcel.length;i++){
			if(StringUtil.isNotBlank(materialExcel[i][0])){
				materilKindName = StringUtil.convertNullToBlankAndTrim(materialExcel[i][0]);//*
				materilCode = StringUtil.convertNullToBlankAndTrim(materialExcel[i][1]);//*
				materilName = StringUtil.convertNullToBlankAndTrim(materialExcel[i][2]);//*
				materilType = StringUtil.convertNullToBlankAndTrim(materialExcel[i][3]);//*
				unit = StringUtil.convertNullToBlankAndTrim(materialExcel[i][4]);//*
				remark = StringUtil.convertNullToBlankAndTrim(materialExcel[i][5]);//*	
				
				materialList=new MaterialList();
				//校验编码
				if(StringUtil.isNotBlank(materilKindName)){
					if(!materilKindNameTmp.equals(materilKindName)){
						//查找 materilKind是否存在
						materialKind=new MaterialKind();
						materialKind.setMkName(materilKindName);
						materialKind=this.iMaterialBiz.getMaterialKindByMaterialKind(materialKind);
						materilKindNameTmp=materilKindName;
					}
					if(StringUtil.isNotBlank(materialKind.getMkId())){
						materialList.setMkKindId(materialKind.getMkId());
						materialList.setMkKindName(materialKind.getMkName());
					}else{
						boolImport = false;
						contentImport = "导入失败！第"+(i+1)+"行的物料类别“"+materilKindName+"”在系统中不存在，请核查";
					}
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，物料类别不能为空";
				}
				//校验编码
				if(StringUtil.isNotBlank(materilCode)){
					maList=this.iMaterialBiz.getMaterialListByCode(materilCode);
					if(maList.getMaterialId()!=null){
						boolImport = false;
						contentImport = "导入失败！第"+(i+1)+"行的编码“"+materilCode+"”系统中已经存在，请核查";	
					}else{
						if(set.add(materilCode)){
					       materialList.setMaterialCode(materilCode);
						}else{
							boolImport = false;
							contentImport = "导入失败！第"+(i+1)+"行的编码“"+materilCode+"”Excel出现了两次，请核查";	
						}
					}
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，编码不能为空";
				}
				//校验名称
				if(StringUtil.isNotBlank(materilName)){
					materialList.setMaterialName(materilName);
					materialList.setMnemonicCode(HanziZhuanhuan.getBeginCharacter(materilName));
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，名称不能为空";
				}
				materialList.setMaterialType(materilType);
				//计量单位
				if(StringUtil.isNotBlank(unit)){
					materialList.setUnit(unit);
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，计量单位不能为空";
				}
				materialList.setRemark(remark);
				materialListList.add(materialList);
			}
		}
		return materialListList;
	}
	public String getIden() {
		return iden;
	}

	public void setIden(String iden) {
		this.iden = iden;
	}


	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}


	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}


	public MaterialKind getMaterialKind() {
		return materialKind;
	}


	public void setMaterialKind(MaterialKind materialKind) {
		this.materialKind = materialKind;
	}


	public MaterialList getMaterialList() {
		return materialList;
	}


	public void setMaterialList(MaterialList materialList) {
		this.materialList = materialList;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


	public String getIds() {
		return ids;
	}


	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getSs() {
		return ss;
	}


	public void setSs(String ss) {
		this.ss = ss;
	}

	public File getFile() {
		return file;
	}


	public void setFile(File file) {
		this.file = file;
	}


	public String getContentImport() {
		return contentImport;
	}


	public void setContentImport(String contentImport) {
		this.contentImport = contentImport;
	}
	
}
