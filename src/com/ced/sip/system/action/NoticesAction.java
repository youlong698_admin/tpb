package com.ced.sip.system.action;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.biz.INoticesBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Notices;
import com.ced.sip.system.entity.Users;

@SuppressWarnings("serial")
public class NoticesAction extends BaseAction {
	private INoticesBiz iNoticesBiz;
	private IDepartmentsBiz iDepartmentsBiz;
	private IAttachmentBiz iAttachmentBiz;
	private IUsersBiz iUsersBiz;
	
	private Notices notices;
	private String id;
	private Users ur;
	private Departments dept;

	/**
	 * 查看公告信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewNotices() throws BaseException {

		try {
			
		} catch (Exception e) {
			log.error("查看公告信息列表错误！", e);
			throw new BaseException("查看公告信息列表错误！", e);
		}

		return VIEW;

	}

	/**
	 * 查看公告信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String findNotices() throws BaseException {

		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(notices==null)
			{
				notices=new Notices();
			}
			String publishDate=this.getRequest().getParameter("publishDate");
			
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
			  
			if(StringUtil.isNotBlank(publishDate))
			{
				notices.setPublishDate(sdf.parse(publishDate));
			}
			String title=this.getRequest().getParameter("title");
			notices.setTitle(title);
			String keyword=this.getRequest().getParameter("keyword");
			notices.setKeyword(keyword);
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) notices.setComId(comId);
			List<Notices> noticesList=new ArrayList<Notices>();
			List<Object[]> president = this.iNoticesBiz.getNoticesList(this.getRollPageDataTables(), notices); 
			for(Object[] obj:president){
				notices=(Notices)obj[0];
				notices.setCompName((String)obj[1]);
				noticesList.add(notices);
			}
			this.getPagejsonDataTables(noticesList);
		} catch (Exception e) {
			log.error("查看公告信息列表错误！", e);
			throw new BaseException("查看公告信息列表错误！", e);
		}

		return null;

	}
	
	/**
	 * 保存公告信息初始化
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String saveNoticesInit() throws BaseException {
		try {
			ur=(Users)this.iUsersBiz.getUsersList(UserRightInfoUtil.getSessionInfo(this.getRequest()).getUsers()).get(0);
			dept=this.iDepartmentsBiz.getDepartments(ur.getDepId());
		} catch (Exception e) {
			log("保存公告信息初始化错误！", e);
			throw new BaseException("保存公告信息初始化错误！", e);
		}
		return ADD_INIT;

	}

	/**
	 * 保存公告信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String saveNotices() throws BaseException {

		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			notices.setComId(comId);
			notices.setPublisher(UserRightInfoUtil.getChineseName(this.getRequest()));
			this.iNoticesBiz.saveNotices(notices);
			//保存附件		
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(notices,
					notices.getNoticeId(), AttachmentStatus.ATTACHMENT_CODE_701,
					UserRightInfoUtil.getUserName(this.getRequest())));
			
			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "保存公告信息");
		} catch (Exception e) {
			log("保存公告信息错误！", e);
			throw new BaseException("保存公告信息错误！", e);
		}

		return ADD_INIT;

	}

	/**
	 * 修改公告信息初始化
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateNoticesInit() throws BaseException {

		try {
			 notices = this.iNoticesBiz.getNotices(notices.getNoticeId());
				
			//获取附件
			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( notices.getNoticeId(), AttachmentStatus.ATTACHMENT_CODE_701 ) );
			notices.setUuIdData((String)map.get("uuIdData"));
			notices.setFileNameData((String)map.get("fileNameData"));
			notices.setFileTypeData((String)map.get("fileTypeData"));
			notices.setAttIdData((String)map.get("attIdData"));
			notices.setAttIds((String)map.get("attIds"));
		} catch (Exception e) {
			log("修改公告信息初始化错误！", e);
			throw new BaseException("修改公告信息初始化错误！", e);
		}
		return MODIFY_INIT;

	}
	/**
	 * 修改公告信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateNotices() throws BaseException {

		try {
			this.iNoticesBiz.updateNotices(notices);
			
			//保存附件
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(notices,notices.getNoticeId(), AttachmentStatus.ATTACHMENT_CODE_701,UserRightInfoUtil.getUserName(this.getRequest())));
		
			// 删除附件
			iAttachmentBiz.deleteAttachments( parseAttachIds( notices.getAttIds() ) ) ;
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改公告信息");
		
		} catch (Exception e) {
			log("修改公告信息错误！", e);
			throw new BaseException("修改公告信息错误！", e);
		}
		return updateNoticesInit();
	}

	/**
	 * 删除公告信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String deleteNotices() throws BaseException {
		try {
			PrintWriter out = this.getResponse().getWriter();
			String id = this.getRequest().getParameter("id");
			String[] st = id.split(",");
		
			for (int i = 0; i < st.length; i++) {
				if (st[i] != null) {
					notices = this.iNoticesBiz.getNotices(new Long(st[i]));
					notices.setIsUsable("1");
					this.iNoticesBiz.updateNotices(notices);
				//	this.iNoticesBiz.deleteNotices(notices);
				}
			}
			String message="删除成功";
			
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除公告信息");
  			out.println(message);  
		} catch (Exception e) {
			log("删除公告信息错误！", e);
			throw new BaseException("删除公告信息错误！", e);
		}
		return null;

	}

	/**
	 * 查看公告明细信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewNoticesDetail() throws BaseException {

		try {
			notices = this.iNoticesBiz.getNotices(notices.getNoticeId());
			notices.setAttachmentUrl(iAttachmentBiz.getAttachmentPageUrl(
					iAttachmentBiz.getAttachmentList(new Attachment(notices
							.getNoticeId(), AttachmentStatus.ATTACHMENT_CODE_701)),
					"0", this.getRequest()));
		} catch (Exception e) {
			log("查看公告明细信息错误！", e);
			throw new BaseException("查看公告明细信息错误！", e);
		}
		return DETAIL;

	}
	

	public Notices getNotices() {
		return notices;
	}

	public void setNotices(Notices notices) {
		this.notices = notices;
	}

	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}

	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public IDepartmentsBiz getiDepartmentsBiz() {
		return iDepartmentsBiz;
	}

	public void setiDepartmentsBiz(IDepartmentsBiz iDepartmentsBiz) {
		this.iDepartmentsBiz = iDepartmentsBiz;
	}

	public Users getUr() {
		return ur;
	}

	public void setUr(Users ur) {
		this.ur = ur;
	}

	public Departments getDept() {
		return dept;
	}

	public void setDept(Departments dept) {
		this.dept = dept;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public INoticesBiz getiNoticesBiz() {
		return iNoticesBiz;
	}

	public void setiNoticesBiz(INoticesBiz iNoticesBiz) {
		this.iNoticesBiz = iNoticesBiz;
	}

}
