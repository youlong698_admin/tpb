package com.ced.sip.system.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.GlobalSettingBase;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.system.biz.ISysCompanyBiz;
import com.ced.sip.system.biz.ISystemConfigurationBiz;
import com.ced.sip.system.entity.SysCompany;
import com.ced.sip.system.entity.SystemConfiguration;
/** 
 * 类名称：SystemConfigurationAction
 * 创建人：luguanglei 
 * 创建时间：2017-03-30
 */
public class SystemConfigurationAction extends BaseAction {
    //上传文件存放路径   
    private final static String UPLOADDIR = "upload/sysCompany/logo/";
	// 系统配置表 
	private ISystemConfigurationBiz iSystemConfigurationBiz;
	//采购单位表
	private ISysCompanyBiz iSysCompanyBiz;
	
	// 系统配置表
	private SystemConfiguration systemConfiguration;
	//采购单位
	private SysCompany sysCompany;
	
	private File file;
	
	private String fileContentType;   
	
	private String fileFileName; 
	
	/**
	 * 修改系统配置表信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateSystemConfigurationInit() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			systemConfiguration=this.iSystemConfigurationBiz.getSystemConfiguration(comId);
			systemConfiguration.setComId(comId);
			sysCompany=this.iSysCompanyBiz.getSysCompany(comId);
			Map industryOwnedMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1707);
			Map managementModelMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1708);
			this.getRequest().setAttribute("industryOwnedMap", industryOwnedMap);
			this.getRequest().setAttribute("managementModelMap", managementModelMap);
			
		} catch (Exception e) {
			log("修改系统配置表信息初始化错误！", e);
			throw new BaseException("修改系统配置表信息初始化错误！", e);
		}
		return "systemConfiguration";
		
	}
	
	/**
	 * 修改系统配置表信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateSystemConfiguration() throws BaseException {
		
		try{
            String logo="",fileType="";  
            if(file!=null){
				InputStream in = new FileInputStream(file);   
	            String dir = GlobalSettingBase.getFilePath()+UPLOADDIR; 
	            
	            int index = fileFileName.lastIndexOf(".");
	    		if (index > -1) {
	    			fileType =fileFileName.substring(index);
	    		}
	    		fileType=fileType.toLowerCase().substring(1);
				String cjrq = new SimpleDateFormat("yyyyMMddHHmmss")
						.format(new Date());
				String fjTpName = cjrq + "." + fileType;// 文件的名字
	            File uploadFile = new File(dir, fjTpName);   
	            logo=UPLOADDIR+fjTpName;
	            OutputStream out = new FileOutputStream(uploadFile);   
	            byte[] buffer = new byte[1024 * 1024];   
	            int length;   
	            while ((length = in.read(buffer)) > 0) {   
	                out.write(buffer, 0, length);   
	            }   
	            in.close();   
	            out.close();   
	            sysCompany.setLogo(logo);
            }
			if(systemConfiguration.getScId()==null){
				if(systemConfiguration.getBidpurchaseresult00Workflow()==null)systemConfiguration.setBidpurchaseresult00Workflow("0");
				if(systemConfiguration.getBidpurchaseresult01Workflow()==null)systemConfiguration.setBidpurchaseresult01Workflow("0");
				if(systemConfiguration.getBidpurchaseresult02Workflow()==null)systemConfiguration.setBidpurchaseresult02Workflow("0");
				if(systemConfiguration.getRequiredcollect00Workflow()==null) systemConfiguration.setRequiredcollect00Workflow("0");
				if(systemConfiguration.getRequiredcollect01Workflow()==null) systemConfiguration.setRequiredcollect01Workflow("0");
				if(systemConfiguration.getRequiredcollect02Workflow()==null) systemConfiguration.setRequiredcollect02Workflow("0");
				if(systemConfiguration.getRequiredMaterialWorkflow()==null) systemConfiguration.setRequiredMaterialWorkflow("0");
				if(systemConfiguration.getContractWorkflow()==null) systemConfiguration.setContractWorkflow("0");
				if(systemConfiguration.getOrderWorkflow()==null) systemConfiguration.setOrderWorkflow("0");
				if(systemConfiguration.getCapitalPlanWorkflow()==null) systemConfiguration.setCapitalPlanWorkflow("0");
				if(systemConfiguration.getMailFalg()==null) systemConfiguration.setMailFalg("0");
				if(systemConfiguration.getMessageFalg()==null) systemConfiguration.setMessageFalg("0");
				if(systemConfiguration.getRequiredcollect02Workflow()==null) systemConfiguration.setRequiredcollect02Workflow("0");
				this.iSystemConfigurationBiz.saveSystemConfiguration(systemConfiguration);
				BaseDataInfosUtil.updateSystemConfigurationCache("save",systemConfiguration);
			}else{
			    this.iSystemConfigurationBiz.updateSystemConfiguration(systemConfiguration);
				BaseDataInfosUtil.updateSystemConfigurationCache("update",systemConfiguration);	
			}
			
			this.iSysCompanyBiz.updateSysCompany(sysCompany);
			Map industryOwnedMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1707);
			Map managementModelMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1708);
			this.getRequest().setAttribute("industryOwnedMap", industryOwnedMap);
			this.getRequest().setAttribute("managementModelMap", managementModelMap);
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改系统配置表");
		} catch (Exception e) {
			log("修改系统配置表信息错误！", e);
			throw new BaseException("修改系统配置表信息错误！", e);
		}
		return "systemConfigurationSuccess";
		
	}

	public SystemConfiguration getSystemConfiguration() {
		return systemConfiguration;
	}

	public void setSystemConfiguration(SystemConfiguration systemConfiguration) {
		this.systemConfiguration = systemConfiguration;
	}

	public ISystemConfigurationBiz getiSystemConfigurationBiz() {
		return iSystemConfigurationBiz;
	}

	public void setiSystemConfigurationBiz(
			ISystemConfigurationBiz iSystemConfigurationBiz) {
		this.iSystemConfigurationBiz = iSystemConfigurationBiz;
	}

	public ISysCompanyBiz getiSysCompanyBiz() {
		return iSysCompanyBiz;
	}

	public void setiSysCompanyBiz(ISysCompanyBiz iSysCompanyBiz) {
		this.iSysCompanyBiz = iSysCompanyBiz;
	}

	public SysCompany getSysCompany() {
		return sysCompany;
	}

	public void setSysCompany(SysCompany sysCompany) {
		this.sysCompany = sysCompany;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	
}
