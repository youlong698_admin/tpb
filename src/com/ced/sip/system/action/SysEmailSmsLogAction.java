package com.ced.sip.system.action;

import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.system.biz.ISysEmailSmsLogBiz;
import com.ced.sip.system.entity.SysEmailSmsLog;
/** 
 * 类名称：SysEmailSmsLogAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-10
 */
public class SysEmailSmsLogAction extends BaseAction {

	// 用户邮件短信日志 
	private ISysEmailSmsLogBiz iSysEmailSmsLogBiz;
	
	// 用户邮件短信日志
	private SysEmailSmsLog sysEmailSmsLog;
	
	/**
	 * 查看用户邮件短信日志信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSysEmailSmsLog() throws BaseException {
		
		try{
		
		} catch (Exception e) {
			log.error("查看用户邮件短信日志信息列表错误！", e);
			throw new BaseException("查看用户邮件短信日志信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看用户邮件短信日志信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findSysEmailSmsLog() throws BaseException {
		
		try{
			List list=this.iSysEmailSmsLogBiz.getSysEmailSmsLogList(this.getRollPageDataTables(), sysEmailSmsLog);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看用户邮件短信日志信息列表错误！", e);
			throw new BaseException("查看用户邮件短信日志信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 查看用户邮件短信日志明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSysEmailSmsLogDetail() throws BaseException {
		
		try{
			sysEmailSmsLog=this.iSysEmailSmsLogBiz.getSysEmailSmsLog(sysEmailSmsLog.getSeslId() );
		} catch (Exception e) {
			log("查看用户邮件短信日志明细信息错误！", e);
			throw new BaseException("查看用户邮件短信日志明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public ISysEmailSmsLogBiz getiSysEmailSmsLogBiz() {
		return iSysEmailSmsLogBiz;
	}

	public void setiSysEmailSmsLogBiz(ISysEmailSmsLogBiz iSysEmailSmsLogBiz) {
		this.iSysEmailSmsLogBiz = iSysEmailSmsLogBiz;
	}

	public SysEmailSmsLog getSysEmailSmsLog() {
		return sysEmailSmsLog;
	}

	public void setSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) {
		this.sysEmailSmsLog = sysEmailSmsLog;
	}
	
}
