package com.ced.sip.system.action;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IQualityCategoryBiz;
import com.ced.sip.system.entity.QualityCategory;
/** 
 * 类名称：QualityCategoryAction
 * 创建人：luguanglei 
 * 创建时间：2017-03-07
 */
public class QualityCategoryAction extends BaseAction {

	// 资质文件类别 
	private IQualityCategoryBiz iQualityCategoryBiz;
	
	// 资质文件类别
	private QualityCategory qualityCategory;
	
	/**
	 * 查看资质文件类别信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewQualityCategory() throws BaseException {
		
		try{
		
		} catch (Exception e) {
			log.error("查看资质文件类别信息列表错误！", e);
			throw new BaseException("查看资质文件类别信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看资质文件类别信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findQualityCategory() throws BaseException {
		
		try{
			String qualityName=this.getRequest().getParameter("qualityName");
			qualityCategory=new QualityCategory();
			qualityCategory.setQualityName(qualityName);
			List list=this.iQualityCategoryBiz.getQualityCategoryList(this.getRollPageDataTables(), qualityCategory);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看资质文件类别信息列表错误！", e);
			throw new BaseException("查看资质文件类别信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存资质文件类别信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveQualityCategoryInit() throws BaseException {
		try{
		
		} catch (Exception e) {
			log("保存资质文件类别信息初始化错误！", e);
			throw new BaseException("保存资质文件类别信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存资质文件类别信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveQualityCategory() throws BaseException {
		
		try{
			this.iQualityCategoryBiz.saveQualityCategory(qualityCategory);			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增资质文件类别");
		} catch (Exception e) {
			log("保存资质文件类别信息错误！", e);
			throw new BaseException("保存资质文件类别信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改资质文件类别信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateQualityCategoryInit() throws BaseException {
		
		try{
			qualityCategory=this.iQualityCategoryBiz.getQualityCategory(qualityCategory.getQcId() );
		} catch (Exception e) {
			log("修改资质文件类别信息初始化错误！", e);
			throw new BaseException("修改资质文件类别信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改资质文件类别信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateQualityCategory() throws BaseException {
		
		try{
			this.iQualityCategoryBiz.updateQualityCategory(qualityCategory);			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改资质文件类别");
		} catch (Exception e) {
			log("修改资质文件类别信息错误！", e);
			throw new BaseException("修改资质文件类别信息错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 删除资质文件类别信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteQualityCategory() throws BaseException {
		try{
		    QualityCategory qualityCategory=new QualityCategory();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				qualityCategory=this.iQualityCategoryBiz.getQualityCategory(new Long(str[i]));
	 				this.iQualityCategoryBiz.deleteQualityCategory(qualityCategory);
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除资质文件类别");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除资质文件类别信息错误！", e);
			throw new BaseException("删除资质文件类别信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看资质文件类别明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewQualityCategoryDetail() throws BaseException {
		
		try{
			qualityCategory=this.iQualityCategoryBiz.getQualityCategory(qualityCategory.getQcId() );
		} catch (Exception e) {
			log("查看资质文件类别明细信息错误！", e);
			throw new BaseException("查看资质文件类别明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public IQualityCategoryBiz getiQualityCategoryBiz() {
		return iQualityCategoryBiz;
	}

	public void setiQualityCategoryBiz(IQualityCategoryBiz iQualityCategoryBiz) {
		this.iQualityCategoryBiz = iQualityCategoryBiz;
	}

	public QualityCategory getQualityCategory() {
		return qualityCategory;
	}

	public void setQualityCategory(QualityCategory qualityCategory) {
		this.qualityCategory = qualityCategory;
	}
	
}
