package com.ced.sip.system.action;


import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Departments;
/**
 * @author ted
 * 
 */
public class DepartmentsAction extends BaseAction {
	//部门
	private IDepartmentsBiz iDepartmentsBiz;
	//用户
	private IUsersBiz iUsersBiz;
	
	
	private Departments departments;
	private String tree ;
	private String message ;
	
	/**
	 * 查看部门信息首页列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDepartmentsIndex() {
		
		return INDEX ;
	}
	
	/**
	 * 查看部门信息表初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDepartmentsInitTree() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}
		return TREE ;
	}
	
	/**
	 * 查看部门信息表树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void viewDepartmentsTree() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			//获取部门所以信息
			departments=new Departments();
			String id=this.getRequest().getParameter("id");
			if(StringUtil.isNotBlank(id)) departments.setParentDeptId(Long.parseLong(id));
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) departments.setComId(comId);
			List<Departments> deptList =this.iDepartmentsBiz.getDepartmentsList(departments);
			
			JSONArray jsonArray = new JSONArray();
			for (Departments dept:deptList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", dept.getDepId());
				subJsonObject.element("pid", dept.getParentDeptId());
				subJsonObject.element("name",dept.getDeptName());
				
				if("0".equals(dept.getIsHaveChild()+""))
				{
					subJsonObject.element("isParent", true);
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}
	}
	
	/**
	 * 修改部门信息初始化updateDepartments
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateDepartmentsInit() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(departments==null){
				//获取部门所以信息
				departments=new Departments();
				if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) departments.setComId(comId);
				List<Departments> deptList =this.iDepartmentsBiz.getDepartmentsList(departments);
				if(deptList.size()>0) departments=deptList.get(0);
			}else{
			    departments= iDepartmentsBiz.getDepartments( departments.getDepId() ) ;
			}
			Departments departments_tmp=new Departments();
			departments_tmp.setIsPurchaseDept(TableStatus.COMMON_IS);
			departments_tmp.setComId(comId);
			List<Departments> list_purchase=iDepartmentsBiz.getDepartmentsList(departments_tmp);
			this.getRequest().setAttribute("list_purchase", list_purchase);
		} catch (Exception e) {
			log("修改部门信息初始化错误！", e);
			throw new BaseException("修改部门信息初始化错误！", e);
		}
		
		return MODIFY_INIT ;
	}
	
	/**
	 * 修改部门信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateDepartments() throws BaseException {
		try{
			//if(departments.getIsPurchaseDept().equals(TableStatus.COMMON_IS)) departments.setPurchaseDeptId(null);
			iDepartmentsBiz.updateDepartments( departments ) ;
			departments.setOperType("update") ;
			
			BaseDataInfosUtil.updateDepmengtCache("update", departments);
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改部门信息");
			
			PrintWriter out = this.getResponse().getWriter();
			
			out.println("修改成功");
		} catch (Exception e) {
			log("修改部门信息信息错误！", e);
			throw new BaseException("修改部门信息信息错误！", e);
		}
		return MODIFY;
	}
	
	/**
	 * 保存部门信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveDepartmentsInit() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			departments = iDepartmentsBiz.getDepartments( departments.getDepId());
			Departments departments_tmp=new Departments();
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) departments_tmp.setComId(comId);
			departments_tmp.setIsPurchaseDept(TableStatus.COMMON_IS);
			List<Departments> list_purchase=iDepartmentsBiz.getDepartmentsList(departments_tmp);
			this.getRequest().setAttribute("list_purchase", list_purchase);
			this.getRequest().setAttribute("comId", comId);
		} catch (Exception e) {
			log("保存部门信息信息初始化错误！", e);
			throw new BaseException("保存部门信息信息初始化错误！", e);
		}
		return ADD_INIT;
	}
	
	/**
	 * 保存部门信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveDepartments() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			
			// 更新父节点中是否有子节点为存在  Ted 2009-12-03 
			Departments parentdt= iDepartmentsBiz.getDepartments( departments.getParentDeptId() ) ;
			parentdt.setIsHaveChild("0");
			
			iDepartmentsBiz.updateDepartments( parentdt ) ;
			
			departments.setSelflevName( parentdt.getSelflevName() + "," + departments.getDeptName().toString() ) ;
			
			departments.setDeptLevel( parentdt.getDeptLevel() + 1 ) ;
			
			departments.setWriteDate(DateUtil.getCurrentDateTime() ) ;
			departments.setWriter(UserRightInfoUtil.getUserName(this.getRequest())) ;
			departments.setComId(comId);
			
			if(departments.getIsPurchaseDept().equals(TableStatus.COMMON_IS)) departments.setPurchaseDeptId(null);
			// 保存部门表
			iDepartmentsBiz.saveDepartments( departments ) ;
			
			if(departments.getOwnPurchaseDeptId().equals(0L)){
				departments.setOwnPurchaseDeptId(departments.getDepId());
			}
			
			departments.setSelflevCode( parentdt.getSelflevCode() + "," + departments.getDepId().toString() ) ;
			
			// 更新级联码
			iDepartmentsBiz.updateDepartments( departments ) ;
			
			departments.setOperType("add") ;
			message = " 新增成功! " ;
			BaseDataInfosUtil.updateDepmengtCache("save", departments);
			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "新增部门信息");
			
		} catch (Exception e) {
			log("保存部门信息信息错误！", e);
			throw new BaseException("保存部门信息信息错误！", e);
		}
		
		return ADD;
		
	}
	
	/**
	 * 删除部门信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String delDepartments() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			departments = iDepartmentsBiz.getDepartments(  departments.getDepId() ) ;
			
			// 更新部门表-本级和它所有子节点为不可用
			iDepartmentsBiz.updateDepartmentsBySelflevCode( departments ) ;
			
			Departments dept = new Departments() ;
			dept.setParentDeptId( departments.getParentDeptId() ) ;
			//  判断父节点下面是否存在子节点
			int dpCount = iDepartmentsBiz.getDepartmentsCount( dept ) ;
			if( dpCount == 0 ) {
				Departments  parentDept= iDepartmentsBiz.getDepartments(  departments.getParentDeptId() );
				parentDept.setIsHaveChild("1");
				iDepartmentsBiz.updateDepartments( parentDept );
			}
			departments.setOperType("delete") ;
			//departments.setIsUsable("1");
			//iDepartmentsBiz.deleteDepartments(departments.getDepId());
						
			message = " 删除成功! " ;
			BaseDataInfosUtil.updateDepmengtCache("update", departments);
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("comId", comId);
			this.getRequest().setAttribute("operModule", "删除部门信息");
		} catch (Exception e) {
			log("删除部门信息信息错误！", e);
			throw new BaseException("删除部门信息信息错误！", e);
		}
		
		return null;
		//793
	}

	public IDepartmentsBiz getiDepartmentsBiz() {
		return iDepartmentsBiz;
	}

	public void setiDepartmentsBiz(IDepartmentsBiz iDepartmentsBiz) {
		this.iDepartmentsBiz = iDepartmentsBiz;
	}

	public Departments getDepartments() {
		return departments;
	}

	public void setDepartments(Departments departments) {
		this.departments = departments;
	}

	public IUsersBiz getIUsersBiz() {
		return iUsersBiz;
	}

	public void setIUsersBiz(IUsersBiz usersBiz) {
		iUsersBiz = usersBiz;
	}

	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
