package com.ced.sip.system.action;


import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.biz.IOrgRightsBiz;
import com.ced.sip.system.biz.IOrganizatinosBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.OrgRights;
import com.ced.sip.system.entity.Organizatinos;
import com.ced.sip.system.entity.Users;
/**
 * @author ted
 * 
 */
public class OrganizatinosAction extends BaseAction {
	private IOrganizatinosBiz iOrganizatinosBiz;
	private Organizatinos organizatinos;
	private IUsersBiz iUsersBiz;
	private Users users;
	private String tree ;
	private String message ;
	
	private IDepartmentsBiz iDepartmentsBiz;
	private Departments departments;
	private IOrgRightsBiz iOrgRightsBiz;
	private OrgRights orgRights;
	
	/**
	 * 查看群组信息首页列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrganizatinosIndex() {
		
		return INDEX ;
	}
	
	/**
	 * 查看群组信息表初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrganizatinosInitTree() throws BaseException {
		
		try{
		} catch (Exception e) {
			log("查看群组信息表初始树列表错误！", e);
			throw new BaseException("查看群组信息表初始树列表错误！", e);
		}
		return TREE ;
	}
	
	/**
	 * 查看群组信息表树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void viewOrganizatinosTree() throws BaseException {
		
		try{		
			Long comId=UserRightInfoUtil.getComId(getRequest());

			Organizatinos organizatinos = new Organizatinos() ;
			String id = this.getRequest().getParameter("id");
			if(StringUtil.isNotBlank(id))  organizatinos.setParentOrgId(new Long(id) ) ;
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) organizatinos.setComId(comId);
			List<Organizatinos> orgList = iOrganizatinosBiz.getOrganizatinosList( organizatinos  );
			JSONArray jsonArray = new JSONArray();
			for (Organizatinos org:orgList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", org.getOrgId());
				subJsonObject.element("pid",org.getParentOrgId());
				subJsonObject.element("name",org.getOrgName());
				
				if("0".equals(org.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);
					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close();  
	        
		} catch (Exception e) {
			log("查看群组信息表初始树列表错误！", e);
			throw new BaseException("查看群组信息表初始树列表错误！", e);
		}
	}
	
	
	/**
	 * 群组中权限部门树
	 * @return
	 * @throws BaseException
	 */
	public String viewOrganizatinos_InitTree() throws BaseException {
		
		try{
			String deptId=this.getRequest().getParameter("deptId");
			String orgId=this.getRequest().getParameter("orgId");
	        
			this.getRequest().setAttribute("deptId", deptId);
	        this.getRequest().setAttribute("orgId", orgId);
		} catch (Exception e) {
			log("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}
		return "deptTree";
	}
	public String viewOrganizatinos_Tree() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String deptIds = this.getRequest().getParameter("deptId");
			if(deptIds!=null && deptIds!=""&& deptIds!=",")
			{
				deptIds=deptIds.substring(1,deptIds.lastIndexOf(","));
			}
			String[] str=deptIds.split(",");
			
			Departments departments = new Departments() ;
			departments.setComId(comId);
			List<Departments> deptList = iDepartmentsBiz.getDepartmentsList( departments  );
			JSONArray jsonArray = new JSONArray();
			for (Departments dept:deptList) {
				JSONObject subJsonObject = new JSONObject();
				
				String a=dept.getDepId().toString();
				if(a.equals("1"))
				{
					subJsonObject.element("open", true);
				}
				subJsonObject.element("id", dept.getDepId());
				subJsonObject.element("pid", dept.getParentDeptId());
				subJsonObject.element("name",dept.getDeptName());
				subJsonObject.element("isChild",dept.getIsHaveChild());  //是否是父节点
				
				
				for(int i=0;i<str.length;i++)
				{
					if(a.equals(str[i])) //判断是否已经选中
					{
						subJsonObject.element("checked",true);
					}
				}
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}
		return null;
	}
	
	/**
	 * 修改群组信息初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateOrganizatinosInit() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(organizatinos==null){
				organizatinos = new Organizatinos() ;
				if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) organizatinos.setComId(comId);
				List<Organizatinos> orgList = iOrganizatinosBiz.getOrganizatinosList( organizatinos  );
				if(orgList.size()>0)organizatinos=orgList.get(0);
			}else{
			   organizatinos= iOrganizatinosBiz.getOrganizatinos( organizatinos.getOrgId() ) ;
			}
			OrgRights orgRig=new OrgRights();
			orgRig.setOrgId(organizatinos.getOrgId());
			List<OrgRights> orgriList=this.iOrgRightsBiz.getOrgRightsList(orgRig);
			String deptIds="";
			for(OrgRights og:orgriList)
			{
				departments=this.iDepartmentsBiz.getDepartments(og.getDepId());
				deptIds+=departments.getDepId()+":"+departments.getDeptName()+",";
			}
			if(!"".equals(deptIds))
			{
				this.getRequest().setAttribute("returnValues", deptIds.substring(0,deptIds.lastIndexOf(",")));
			}
			
		} catch (Exception e) {
			log("修改群组信息初始化错误！", e);
			throw new BaseException("修改群组信息初始化错误！", e);
		}
		
		return MODIFY_INIT ;
	}
	
	/**
	 * 修改群组信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateOrganizatinos() throws BaseException {
		try{
			String deptIds=this.getRequest().getParameter("deptId");
			
			if(",".equals(deptIds)){deptIds="";}
			
			if(deptIds!=null && deptIds!="")
			{
				deptIds=deptIds.substring(1,deptIds.lastIndexOf(","));
				String[] str=deptIds.split(",");
				iOrgRightsBiz.deleteOrgRights(organizatinos.getOrgId().toString());
				
				for(int i=0;i<str.length;i++)
				{
					OrgRights orgRights=new OrgRights();
					orgRights.setDepId(new Long(str[i]));
					orgRights.setOrgId(organizatinos.getOrgId());
					orgRights.setComId(organizatinos.getComId());
					//给群组添加部门权限
					iOrgRightsBiz.saveOrgRights(orgRights);
				}
			}
			
			iOrganizatinosBiz.updateOrganizatinos( organizatinos ) ;
			organizatinos.setOperType("update") ;
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改群组信息");
			
			PrintWriter out = this.getResponse().getWriter();
			out.println("修改成功");
		} catch (Exception e) {
			log("修改群组信息信息错误！", e);
			throw new BaseException("修改群组信息信息错误！", e);
		}
		return MODIFY;
	}
	
	/**
	 * 保存群组信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveOrganizatinosInit() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			organizatinos = iOrganizatinosBiz.getOrganizatinos( organizatinos.getOrgId() ) ;
			this.getRequest().setAttribute("comId", comId);
		} catch (Exception e) {
			log("保存群组信息信息初始化错误！", e);
			throw new BaseException("保存群组信息信息初始化错误！", e);
		}
		return ADD_INIT;
	}
	
	/**
	 * 保存群组信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveOrganizatinos() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String deptIds=this.getRequest().getParameter("deptId");
			if(",".equals(deptIds)){deptIds="";}
			
			// 更新父节点中是否有子节点为存在  Ted 2009-12-03 
			Organizatinos parentdt= iOrganizatinosBiz.getOrganizatinos( organizatinos.getParentOrgId() ) ;
			parentdt.setIsHaveChild("0");
			
			iOrganizatinosBiz.updateOrganizatinos( parentdt ) ;
			
			organizatinos.setSelflevName( parentdt.getSelflevName() + "," + organizatinos.getOrgName().toString() ) ;
			
			organizatinos.setOrgLevel( parentdt.getOrgLevel() + 1 ) ;
			
			organizatinos.setWriteDate(DateUtil.getCurrentDateTime() ) ;
			organizatinos.setWriter(UserRightInfoUtil.getUserName(getRequest())) ;
			organizatinos.setComId(comId);
			// 保存群组表
			iOrganizatinosBiz.saveOrganizatinos( organizatinos ) ;
			List<Organizatinos> orgList=iOrganizatinosBiz.getOrganizatinosList(organizatinos);
			if(orgList.size()==1)
			{
				Long oId=null;
				for(Organizatinos organ:orgList)
				{
					oId=organ.getOrgId();
				}
				if(deptIds!=null && deptIds!="")
				{
					deptIds=deptIds.substring(1,deptIds.lastIndexOf(","));
					String[] str=deptIds.split(",");
					
					for(int i=0;i<str.length;i++)
					{
						OrgRights orgRights=new OrgRights();
						orgRights.setDepId(new Long(str[i]));
						orgRights.setOrgId(oId);
						orgRights.setComId(comId);
						//给群组添加部门权限
						iOrgRightsBiz.saveOrgRights(orgRights);
					}
				}
			}
			
			organizatinos.setSelflevCode( parentdt.getSelflevCode() + "," + organizatinos.getOrgId().toString() ) ;
			
			// 更新级联码
			iOrganizatinosBiz.updateOrganizatinos( organizatinos ) ;
			
			organizatinos.setOperType("add") ;
			
			this.getRequest().setAttribute("message", "新增成功!");
			this.getRequest().setAttribute("operModule", "新增群组信息");
			
		} catch (Exception e) {
			log("保存群组信息信息错误！", e);
			throw new BaseException("保存群组信息信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 删除群组信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String delOrganizatinos() throws BaseException {
		try{
			
			organizatinos = iOrganizatinosBiz.getOrganizatinos(  organizatinos.getOrgId() ) ;
			
			// 更新群组表-本级和它所有子节点为不可用
			iOrganizatinosBiz.updateOrganizatinosBySelflevCode( organizatinos ) ;
			
			Organizatinos org = new Organizatinos() ;
			org.setParentOrgId( organizatinos.getParentOrgId() ) ;
			//  判断父节点下面是否存在子节点
			int dpCount = iOrganizatinosBiz.getOrganizatinosCount( org ) ;
			if( dpCount == 0 ) {
				Organizatinos  parentDept= iOrganizatinosBiz.getOrganizatinos(  organizatinos.getParentOrgId() );
				parentDept.setIsHaveChild("1");
				iOrganizatinosBiz.updateOrganizatinos( parentDept );
			}
			
			organizatinos.setOperType("delete") ;
			
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除群组信息");
			
		} catch (Exception e) {
			log("删除群组信息信息错误！", e);
			throw new BaseException("删除群组信息信息错误！", e);
		}
		
		return null;
		//793
	}

	public IOrganizatinosBiz getIOrganizatinosBiz() {
		return iOrganizatinosBiz;
	}

	public void setIOrganizatinosBiz(IOrganizatinosBiz organizatinosBiz) {
		iOrganizatinosBiz = organizatinosBiz;
	}

	public Organizatinos getOrganizatinos() {
		return organizatinos;
	}

	public void setOrganizatinos(Organizatinos organizatinos) {
		this.organizatinos = organizatinos;
	}

	public IUsersBiz getIUsersBiz() {
		return iUsersBiz;
	}

	public void setIUsersBiz(IUsersBiz usersBiz) {
		iUsersBiz = usersBiz;
	}

	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

	public IDepartmentsBiz getiDepartmentsBiz() {
		return iDepartmentsBiz;
	}

	public void setiDepartmentsBiz(IDepartmentsBiz iDepartmentsBiz) {
		this.iDepartmentsBiz = iDepartmentsBiz;
	}

	public Departments getDepartments() {
		return departments;
	}

	public void setDepartments(Departments departments) {
		this.departments = departments;
	}

	public IOrgRightsBiz getiOrgRightsBiz() {
		return iOrgRightsBiz;
	}

	public void setiOrgRightsBiz(IOrgRightsBiz iOrgRightsBiz) {
		this.iOrgRightsBiz = iOrgRightsBiz;
	}

	public OrgRights getOrgRights() {
		return orgRights;
	}

	public void setOrgRights(OrgRights orgRights) {
		this.orgRights = orgRights;
	}

	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}

	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	
	
}
