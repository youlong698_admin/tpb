package com.ced.sip.system.action;

import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.CodeGetByConditionsUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IMajorBiz;
import com.ced.sip.system.entity.Major;
/** 
 * 类名称：MajorAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class MajorAction extends BaseAction {

	// 专业信息 
	private IMajorBiz iMajorBiz;
	
	// 专业信息
	private Major major;
	/**
	 * 专业管理首页
	 * @return
	 */
	public String viewMajorInfoIndex(){
		
		return INDEX;
	}
	
	
	/**
	 * 查看专业信息初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMajorInitTree() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log("查看专业信息初始树列表错误！", e);
			throw new BaseException("查看专业信息初始树列表错误！", e);
		}
		return TREE ;
	}
	
	/**
	 * 查看专业信息树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void viewMajorTree() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			major =new Major();
			String mkId = this.getRequest().getParameter("mkId");
			String id=this.getRequest().getParameter("id");
			if(StringUtil.isNotBlank(mkId))
			{
				if(StringUtil.isBlank(id)) id=mkId;
			}else
			{
				if(StringUtil.isBlank(id)) id="0";
			}
			major.setParentId(new Long(id));
			major.setComId(comId);
			List<Major> mkList = iMajorBiz.getMajorList( major );
			
			JSONArray jsonArray = new JSONArray();
			for (Major maKind:mkList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", maKind.getMjId());
				subJsonObject.element("pid", maKind.getParentId());
				subJsonObject.element("name",maKind.getMjName());
				
				if("0".equals(maKind.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
		} catch (Exception e) {
			log("查看专业信息初始树列表错误！", e);
			throw new BaseException("查看专业信息初始树列表错误！", e);
		}
	}
	
	/**
	 * 修改专业初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateMajorInit() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(major==null){
				major = new Major() ;
				if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) major.setComId(comId);
				List<Major> orgList = iMajorBiz.getMajorList( major  );
				if(orgList.size()>0)major=orgList.get(0);
			}else{
				major = iMajorBiz.getMajor( major.getMjId() ) ;
			}
			
			
		} catch (Exception e) {
			log("修改专业初始化错误！", e);
			throw new BaseException("修改专业初始化错误！", e);
		}
		
		return MODIFY_INIT ;
	}
	
	/**
	 * 修改专业信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateMajor() throws BaseException {
		try{
			
			iMajorBiz.updateMajor( major ) ;

			major.setOperType("update") ;
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改专业信息信息");
		} catch (Exception e) {
			log("修改专业信息错误！", e);
			throw new BaseException("修改专业信息错误！", e);
		}
		return MODIFY;
	}
	
	/**
	 * 保存专业信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveMajorInit() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String str = "";
			int strLen =0;
			if(major!=null){
				major = iMajorBiz.getMajor( major.getMjId() ) ;
				if(major.getMjCode().length()==2){
					 str = major.getMjCode().substring(0,2);
				}
				else if(major.getMjCode().length()==4){
					 str = major.getMjCode().substring(0,4);
				}
			}else{
				major=new Major();
				str="";
				major.setMjCode("");
			}
			strLen = major.getMjCode().length()+2;
		    List matKindList = this.iMajorBiz.getMajorCode(str, Integer.toString(strLen),comId);
		    	Object obj =  matKindList.get(0);
		    	if(!StringUtil.isNotBlank(obj)){
		    		major.setMajorCode(str+CodeGetByConditionsUtil.getSerialFormatObject("00").format(1).toString());
		    	}
		    	if(StringUtil.isNotBlank(obj)){
		    		String sCode = obj.toString();
			    	String sNum = sCode.substring(sCode.length()-2, sCode.length());
			    	major.setMajorCode(str+CodeGetByConditionsUtil.getSerialFormatObject("00").format(Long.parseLong(sNum)+1).toString());
		    	}
			this.getRequest().setAttribute("comId", comId);
		} catch (Exception e) {
			log("保存专业信息初始化错误！", e);
			throw new BaseException("保存专业信息初始化错误！", e);
		}
		return ADD_INIT;
	}
	
	
	/**
	 * 删除专业信息
	 * @return
	 * @throws BaseException 
	 */
	public String delMajor() throws BaseException {
		try{
			major = iMajorBiz.getMajor( major.getMjId() ) ;
			major.setIsUsable(TableStatus.COMMON_STATUS_INVALID);
			iMajorBiz.updateMajor(major ) ;
			
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除专业信息信息");
			
		} catch (Exception e) {
			log("删除专业信息错误！", e);
			throw new BaseException("删除专业信息错误！", e);
		}
		
		return null;
		
	}

	/**
	 * 保存专业信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveMajor() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			Major parentdt=null;
			// 更新父节点中是否有子节点为存在 
			if(major.getParentId()==null){
				parentdt=new Major();
			    parentdt.setIsHaveChild("0");
			    parentdt.setLevels(0);
			    parentdt.setSelflevCode("");
			    major.setParentId(0L);
			}else{			
				parentdt= iMajorBiz.getMajor( major.getParentId() ) ;
			    parentdt.setIsHaveChild("0");
			    parentdt.setComId(comId);
			    iMajorBiz.updateMajor( parentdt ) ;
			}

		    major.setComId(comId);
			major.setLevels( parentdt.getLevels() + 1 ) ;
			
			major.setWriteDate(DateUtil.getCurrentDateTime() ) ;
			major.setWriter( UserRightInfoUtil.getUserName( this.getRequest()) ) ;
			
			// 保存专业表
			iMajorBiz.saveMajor( major ) ;
			
			major.setSelflevCode( parentdt.getSelflevCode() + "," + major.getMjId().toString() ) ;
			
			// 更新级联码
			iMajorBiz.updateMajor( major ) ;			

			major.setOperType("add") ;
			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "保存专业信息");
			
		} catch (Exception e) {
			log("保存专业信息错误！", e);
			throw new BaseException("保存专业信息错误！", e);
		}
		
		return ADD;
		
	}

	public IMajorBiz getiMajorBiz() {
		return iMajorBiz;
	}

	public void setiMajorBiz(IMajorBiz iMajorBiz) {
		this.iMajorBiz = iMajorBiz;
	}

	public Major getMajor() {
		return major;
	}

	public void setMajor(Major major) {
		this.major = major;
	}
	
}
