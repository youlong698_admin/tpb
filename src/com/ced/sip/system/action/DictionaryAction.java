package com.ced.sip.system.action;

import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IDictionaryBiz;
import com.ced.sip.system.entity.Dictionary;

public class DictionaryAction extends BaseAction {
	//数据字典
	private IDictionaryBiz iDictionaryBiz  ;
	
	private Dictionary dict ;
	private Dictionary parentDict ;
	private String tree ;
	private String message ;
	
	//字典类型
	private String dictType;
	/**
	 * 查看字典信息首页列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewDictIndex() {
		
		return INDEX;
	}

	/**
	 * 查看字典信息表初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDictInitTree() throws BaseException {
		
		try{
			/*Long id;
			String name = "";
			
			// 初始父ID为零
			Dictionary dict = new Dictionary() ;
			dict.setParentDictId( new Long(0) ) ;
			List deptList = iTSysDictBiz.getTSysDictList( dict );
			
			StringBuffer script = new StringBuffer();
			script.append("var tree = new WebFXTree(\"字典类型树\");\n");
			Iterator it=	deptList.iterator();
			
			while (it.hasNext()) {
				Dictionary	sdict=(Dictionary)it.next();
				id = sdict.getDictId();
				name = StringEscapeUtils.escapeXml(sdict.getDictName().trim());
				
				// 判断是否有子节点
				if ("0".equals( sdict.getIsHaveChild() )) {
					script.append("tree.add( rti = new WebFXLoadTreeItem(\"" + name + "\", \"viewDictTree_dict.action?dict.parentDictId=" + id + "\",\"\"));\n");
					
				} else {
					script.append("tree.add( new WebFXTreeItem(\"" + name + "\",\"javascript:folderview('" + id + "')\"));\n");
				}
			}
			
			script.append("document.write(tree);\n");
			script.append("tree.expand();\n"); 

			tree = script.toString() ;
			*/
		} catch (Exception e) {
			log("查看字典信息表初始树列表错误！", e);
			throw new BaseException("查看字典信息表初始树列表错误！", e);
		}
		
		return TREE ;
	}
	
	/**
	 * 查看字典信息表树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDictTree() throws BaseException {
		
		try{
			String id = this.getRequest().getParameter("id");
			if(StringUtil.isBlank(id)) id="0";
			dict=new Dictionary();
			dict.setParentDictId(Long.parseLong(id));
			List<Dictionary> dictList = iDictionaryBiz.getTSysDictList( dict );
			
			JSONArray jsonArray = new JSONArray();
			for (Dictionary dic:dictList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", dic.getDictId());
				subJsonObject.element("pid", dic.getParentDictId());
				subJsonObject.element("name",dic.getDictName());
				
				if("0".equals(dic.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);
					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
		} catch (Exception e) {
			log("查看字典信息表初始树列表错误！", e);
			throw new BaseException("查看字典信息表初始树列表错误！", e);
		}
		
		return null ;
	}
	
	/**
	 * 查看字典信息初始化列表
	 */
       public String viewTSysDict() throws BaseException {
		
		try{
		} catch (Exception e) {
			log.error("查看字典信息列表错误！", e);
			throw new BaseException("查看字典信息列表错误！", e);
		}
		
		return VIEW ;
		
	}
	/**
	 * 查看字典信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findviewTSysDict() throws BaseException {
		
		try{
			List<Dictionary> parentDict = this.iDictionaryBiz.getTSysDictList(this.getRollPageDataTables(), dict) ;
			this.getPagejsonDataTables(parentDict);
			//parentDict = this.iTSysDictBiz.getTSysDict( dict.getParentDictId() ) ;
			//this.setListValue( this.iTSysDictBiz.getTSysDictList( dict ) ) ;
		} catch (Exception e) {
			log.error("查看字典信息列表错误！", e);
			throw new BaseException("查看字典信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存字典信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveTSysDictInit() throws BaseException {
		try{
			parentDict = this.iDictionaryBiz.getTSysDict( dict.getParentDictId() ) ;
		} catch (Exception e) {
			log("保存字典信息初始化错误！", e);
			throw new BaseException("保存字典信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存字典信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveTSysDict() throws BaseException {
		
		try{
			dict.setWriter( UserRightInfoUtil.getUserName( this.getRequest() ) ) ;
			dict.setWriteDate( DateUtil.getCurrentDateTime() ) ;
			
			this.iDictionaryBiz.saveTSysDict( dict );
			
			BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_03 ) ;
			/*if("1770".equals(dict.getParentDictCode()))
			{
				BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_04 ) ;
			}*/
			this.getRequest().setAttribute("message", "新增成功");
			
			//BaseDataInfosUtil.updateDepmengtCache("add", departments);
		} catch (Exception e) {
			log("保存字典信息错误！", e);
			throw new BaseException("保存字典信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改字典信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateTSysDictInit() throws BaseException {
		
		try{
			dict=this.iDictionaryBiz.getTSysDict( dict.getDictId() );
			parentDict = this.iDictionaryBiz.getTSysDict( dict.getParentDictId() ) ;
		} catch (Exception e) {
			log("修改字典信息初始化错误！", e);
			throw new BaseException("修改字典信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改字典信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateTSysDict() throws BaseException {
		
		try{
			dict.setWriter( UserRightInfoUtil.getUserName( this.getRequest() ) ) ;
			dict.setWriteDate( DateUtil.getCurrentDateTime() ) ;
			
			this.iDictionaryBiz.updateTSysDict( dict );
			this.getRequest().setAttribute("message", "修改成功");
//			BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_03 ) ;
			
			/*if("1770".equals(dict.getParentDictCode()))
			{
				BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_04 ) ;
			}*/
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改字典信息");
		} catch (Exception e) {
			log("修改字典信息错误！", e);
			throw new BaseException("修改字典信息错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 删除字典信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteTSysDict() throws BaseException {
		try{
			String dictId=this.getRequest().getParameter("dictId");
			String[] str=dictId.split(",");
			for(int i=0;i<str.length;i++){
			dict=this.iDictionaryBiz.getTSysDict( new Long(str[i]) );
			dict.setIsUsable( TableStatus.COMMON_STATUS_INVALID ) ;
			this.iDictionaryBiz.updateTSysDict( dict );
			}
//			BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_03 ) ;
			/*if("1770".equals(dict.getParentDictCode()))
			{
				BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_04 ) ;
			}*/
			PrintWriter out = this.getResponse().getWriter();
			String message="删除成功";
			this.getRequest().setAttribute("message", message);
			this.getRequest().setAttribute("operModule", "删除字典信息");
			out.println(message);
			
		} catch (Exception e) {
			log("删除字典信息错误！", e);
			throw new BaseException("删除字典信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看字典明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewTSysDictDetail() throws BaseException {
		
		try{
			dict=this.iDictionaryBiz.getTSysDict( dict.getDictId() );
		} catch (Exception e) {
			log("查看字典明细信息错误！", e);
			throw new BaseException("查看字典明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public Dictionary getParentDict() {
		return parentDict;
	}

	public void setParentDict(Dictionary parentDict) {
		this.parentDict = parentDict;
	}

	public Dictionary getDict() {
		return dict;
	}

	public void setDict(Dictionary dict) {
		this.dict = dict;
	}

	public IDictionaryBiz getiDictionaryBiz() {
		return iDictionaryBiz;
	}

	public void setiDictionaryBiz(IDictionaryBiz iDictionaryBiz) {
		this.iDictionaryBiz = iDictionaryBiz;
	}

	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}	
	
}
