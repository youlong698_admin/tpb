package com.ced.sip.system.action;

import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.PasswordUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ICategoryBuyerBiz;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.biz.IOrganizatinosBiz;
import com.ced.sip.system.biz.IOrgUserBiz;
import com.ced.sip.system.biz.IPurchaseDepartBuyerBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.CategoryBuyer;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Dictionary;
import com.ced.sip.system.entity.OrgUser;
import com.ced.sip.system.entity.Organizatinos;
import com.ced.sip.system.entity.PurchaseDepartBuyer;
import com.ced.sip.system.entity.Users;

public class UsersAction extends BaseAction {
	// 部门服务类
	private IDepartmentsBiz iDepartmentsBiz;
	// 用户服务类
	private IUsersBiz iUsersBiz;
	// 群组信息表服务类
	private IOrganizatinosBiz iOrganizatinosBiz;
	// 群组用户表服务类
	private IOrgUserBiz iOrgUserBiz;	
	//采购人采购部门服务类
	private IPurchaseDepartBuyerBiz iPurchaseDepartBuyerBiz;
	//采购员所管理的采购类别
	private ICategoryBuyerBiz iCategoryBuyerBiz;

	// 部门实体类
	private Departments departments;
	// 用户实体类
	private Users users;
	// 组织机构与群组区分参数
	private String orgType;
	// 群组信息表实体类
	private Organizatinos organizatinos;
	// 群组用户表实体类
	private OrgUser orguser;
	//采购员所管理的采购类别
	private CategoryBuyer categoryBuyer;
	//采购员所管理的采购部门
	private PurchaseDepartBuyer purchaseDepartBuyer;
	/**
	 * 查看部门信息首页列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewDepartmentsIndex() {

		return INDEX;
	}

	/**
	 * 查看部门信息表初始树列表 getTree
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewDepartmentsInitTree() throws BaseException {

		try {

		} catch (Exception e) {
			log.error("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}

		return TREE;
	}

	/**
	 * 查看部门信息表树列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewDepartmentsTree() throws BaseException {

		try {
			// 获取部门所以信息
			Long comId = UserRightInfoUtil.getComId(getRequest());
			// 获取部门所以信息
			departments = new Departments();
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest()))
				departments.setComId(comId);
			List<Departments> deptList = this.iDepartmentsBiz
					.getDepartmentsList(departments);

			JSONArray jsonArray = new JSONArray();
			for (Departments dept : deptList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", dept.getDepId());
				subJsonObject.element("pid", dept.getParentDeptId());
				subJsonObject.element("name", dept.getDeptName());

				if ("1".equals(dept.getDepId() + "")) {
					subJsonObject.element("open", true);
				}

				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();
			writer.print(jsonArray);
			writer.flush();
			writer.close();

		} catch (Exception e) {
			log.error("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}

		return null;
	}

	/**
	 * 查看所有用户信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewAllusers() throws BaseException {

		try {
		} catch (Exception e) {
			log.error("查看所有用户信息列表！", e);
			throw new BaseException("查看所有用户信息列表！", e);
		}
		return "viewAll";
	}

	/**
	 * 查看用户信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewUsers() throws BaseException {

		try {

		} catch (Exception e) {
			log.error("查看用户信息列表错误！", e);
			throw new BaseException("查看用户信息列表错误！", e);
		}

		return VIEW;

	}

	/**
	 * 查看用户信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String findUsers() throws BaseException {
		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			if (users == null) {
				users = new Users();
			}
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest()))
				users.setComId(comId);
			String userChinesename = this.getRequest().getParameter(
					"userChinesename");
			users.setUserChinesename(userChinesename);
			String deptId = this.getRequest().getParameter("departId");
			if (deptId != null && !"".equals(deptId)) {
				users.setDepId(new Long(deptId));
			}
			if (StringUtil.isNotBlank(userChinesename)) {
				users.setDepId(null);
			}
			String isUsable = this.getRequest().getParameter("isUsable");
			users.setIsUsable(isUsable);

			List<Users> president = this.iUsersBiz.getUsersList(this
					.getRollPageDataTables(), users);
			for (Users users : president) {
				users.setDepName(BaseDataInfosUtil.convertDeptIdToName(users
						.getDepId()));
			}
			this.getPagejsonDataTables(president);
		} catch (Exception e) {
			log.error("查看用户信息列表错误！", e);
			throw new BaseException("查看用户信息列表错误！", e);
		}

		return null;

	}

	/**
	 * 新增用户信息初始化
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String saveUsersInit() throws BaseException {

		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			departments = iDepartmentsBiz.getDepartments(users.getDepId());
			List<Departments> deptList=this.iDepartmentsBiz.getPurchaseDepartmentsListByDeptId(users.getDepId());
			this.getRequest().setAttribute("comId", comId);
			this.getRequest().setAttribute("deptList", deptList);
			
			//获取岗位
			 List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1717);
			 this.getRequest().setAttribute("dictionaryList", dictionaryList);
			
		} catch (Exception e) {
			log.error("新增用户信息初始化错误！", e);
			throw new BaseException("新增用户信息初始化错误！", e);
		}

		return ADD_INIT;

	}

	/**
	 * 新增用户信息
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String saveUsers() throws BaseException {

		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			departments = iDepartmentsBiz
					.getDepartments(departments.getDepId());
			users.setWriteDate(DateUtil.getCurrentDateTime());
			users.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
			users.setIsUsable(TableStatus.COMMON_STATUS_VALID);
			users.setSelflevCode(departments.getSelflevCode());
			users
					.setUserPassword(PasswordUtil.encrypt(users
							.getUserPassword()));
			users.setComId(comId);
			iUsersBiz.saveUsers(users);
			BaseDataInfosUtil.updateUserCache("save", users);
			
			//处理所管理的采购类别
			String materialKindIds=this.getRequest().getParameter("materialKindIds");
			if(StringUtil.isNotBlank(materialKindIds)){
				this.iCategoryBuyerBiz.saveCategoryBuyerByMaterialKindIds(materialKindIds, users.getUserId(), comId);
			}
			//处理所管理的采购组织
			String[] deptPurchaseId=this.getRequest().getParameterValues("deptPurchaseId");
			if(StringUtil.isNotBlank(deptPurchaseId)){
				this.iPurchaseDepartBuyerBiz.savePurchaseDepartBuyerByDeptPurchaseId(deptPurchaseId, users.getUserId(), comId);
			}
		} catch (Exception e) {
			log.error("新增用户信息错误！", e);
			throw new BaseException("新增用户信息错误！", e);
		}

		return ADD;

	}

	/**
	 * 修改用户信息初始化
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateUsersInit() throws BaseException {

		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			users = this.iUsersBiz.getUsers(users.getUserId());
			departments.setDepId(users.getDepId());
			departments = this.iDepartmentsBiz.getDepartments(departments
					.getDepId());
			this.getRequest().setAttribute("comId", comId);
			
			String materialKindIds="",maerialKindNames="";
			categoryBuyer=new CategoryBuyer();
			categoryBuyer.setBuyerId(users.getUserId());
			List<CategoryBuyer> categoryBuyerList=this.iCategoryBuyerBiz.getCategoryBuyerList(categoryBuyer);
			for(CategoryBuyer categoryBuyer:categoryBuyerList){
				materialKindIds+=","+categoryBuyer.getMkThreeId();
				maerialKindNames+=","+categoryBuyer.getMkThreeName();
			}
			if(StringUtil.isNotBlank(materialKindIds)){
				materialKindIds=materialKindIds.substring(1);
				maerialKindNames=maerialKindNames.substring(1);
			}

			this.getRequest().setAttribute("materialKindIds", materialKindIds);
			this.getRequest().setAttribute("maerialKindNames", maerialKindNames);
			
			String purchaseDepartBuyerIds=",";
			purchaseDepartBuyer=new PurchaseDepartBuyer();
			purchaseDepartBuyer.setBuyerId(users.getUserId());
			List<PurchaseDepartBuyer> purchaseDepartBuyerList=this.iPurchaseDepartBuyerBiz.getPurchaseDepartBuyerList(purchaseDepartBuyer);
			for(PurchaseDepartBuyer purchaseDepartBuyer:purchaseDepartBuyerList){
				purchaseDepartBuyerIds+=purchaseDepartBuyer.getDeptId()+",";
			}
			this.getRequest().setAttribute("purchaseDepartBuyerIds", purchaseDepartBuyerIds);

			List<Departments> deptList=this.iDepartmentsBiz.getPurchaseDepartmentsListByDeptId(users.getDepId());
			this.getRequest().setAttribute("deptList", deptList);
			
			//获取岗位
			List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1717);
			 this.getRequest().setAttribute("dictionaryList", dictionaryList);
		} catch (Exception e) {
			log.error("修改用户信息初始化错误！", e);
			throw new BaseException("修改用户信息初始化错误！", e);
		}
		return MODIFY_INIT;

	}

	/**
	 * 验证用户信息登陆名是否存在
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String VerificationUsers(String userName) throws BaseException {
		String us = "1";
		try {
			users = new Users();
			users.setUserName(userName.trim());
			// System.out.println(userName);
			List<Users> usList = this.iUsersBiz.getUsersList(users);
			if (usList.size() > 0) {
				us = "1";
			} else {
				us = "0";
			}
		} catch (Exception e) {
			log.error("验证用户信息错误！", e);
			throw new BaseException("验证用户信息错误！", e);
		}
		return us;

	}

	/**
	 * 修改用户信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateUsers() throws BaseException {

		try {
			departments = iDepartmentsBiz
					.getDepartments(departments.getDepId());
			users.setWriteDate(DateUtil.getCurrentDateTime());
			users.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
			users.setSelflevCode(departments.getSelflevCode());
			this.iUsersBiz.updateUsers(users);
			BaseDataInfosUtil.updateUserCache("update", users);
			Long comId=users.getComId();
			
			this.iCategoryBuyerBiz.deleteCategoryBuyerByBuyerId(users.getUserId());
			this.iPurchaseDepartBuyerBiz.deletePurchaseDepartBuyerByBuyerId(users.getUserId());
			
			//处理所管理的采购类别
			String materialKindIds=this.getRequest().getParameter("materialKindIds");
			if(StringUtil.isNotBlank(materialKindIds)){
				this.iCategoryBuyerBiz.saveCategoryBuyerByMaterialKindIds(materialKindIds, users.getUserId(), comId);
			}
			//处理所管理的采购组织
			String[] deptPurchaseId=this.getRequest().getParameterValues("deptPurchaseId");
			if(StringUtil.isNotBlank(deptPurchaseId)){
				this.iPurchaseDepartBuyerBiz.savePurchaseDepartBuyerByDeptPurchaseId(deptPurchaseId, users.getUserId(), comId);
			}
			
		} catch (Exception e) {
			log.error("修改用户信息错误！", e);
			throw new BaseException("修改用户信息错误！", e);
		}
		return MODIFY;

	}

	/**
	 * 用户信息明细
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewUsersDetail() throws BaseException {

		try {
			users = this.iUsersBiz.getUsers(users.getUserId());
			departments.setDepId(departments.getDepId());
			if (StringUtil.isNotBlank(users.getDepId())) {
				users.setDepIdCn(BaseDataInfosUtil.convertDeptIdToName(users
						.getDepId()));
			}
		} catch (Exception e) {
			log.error("修改用户信息初始化错误！", e);
			throw new BaseException("修改用户信息初始化错误！", e);
		}
		return DETAIL;

	}

	/**
	 * 重置用户密码信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String resetUsers() throws BaseException {
		try {
			String userId = this.getRequest().getParameter("userId");

			users = iUsersBiz.getUsers(new Long(userId));
			users.setUserPassword(PasswordUtil.encrypt("123456"));
			iUsersBiz.updateUsers(users);

		} catch (Exception e) {
			log.error("重置用户密码信息错误！", e);
			throw new BaseException("重置用户密码信息错误！", e);
		}
		return "modify";

	}

	/**
	 * 删除用户信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String deleteUsers() throws BaseException {
		try {
			PrintWriter out = this.getResponse().getWriter();
			String id = this.getRequest().getParameter("id");
			String[] str = id.split(",");
			for (int i = 0; i < str.length; i++) {
				if (str[i] != null) {
					users = this.iUsersBiz.getUsers(new Long(str[i]));
					users.setIsUsable("1");
					this.iUsersBiz.updateUsers(users);
					BaseDataInfosUtil.updateUserCache("update", users);
					// this.iUsersBiz.deleteUsers(users);
				}
			}
			String message = "删除成功";
			out.println(message);
		} catch (Exception e) {
			log("删除用户信息错误！", e);
			throw new BaseException("删除用户信息错误！", e);
		}
		return null;
	}

	/************************************ 群组管理开始 *****************************************************/

	/**
	 * 查看群组信息表初始树列表 getTree
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewOrganizatinosInitTree() throws BaseException {

		try {

		} catch (Exception e) {
			log("查看群组信息表初始树列表错误！", e);
			throw new BaseException("查看群组信息表初始树列表错误！", e);
		}
		return "tree_org";
	}

	/**
	 * 查看群组信息表树列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public void viewOrganizatinosTree() throws BaseException {

		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			String id = this.getRequest().getParameter("id");
			if (StringUtil.isBlank(id))
				id = "0";
			Organizatinos organizatinos = new Organizatinos();
			organizatinos.setParentOrgId(new Long(id));
			organizatinos.setComId(comId);

			List<Organizatinos> orgList = iOrganizatinosBiz
					.getOrganizatinosList(organizatinos);

			JSONArray jsonArray = new JSONArray();
			for (Organizatinos org : orgList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", org.getOrgId());
				subJsonObject.element("pid", org.getParentOrgId());
				subJsonObject.element("name", org.getOrgName());

				if ("0".equals(org.getIsHaveChild())) {
					subJsonObject.element("isParent", true);

				}

				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();
			// System.out.println(jsonArray);
			writer.print(jsonArray);
			writer.flush();
			writer.close();

		} catch (Exception e) {
			log("查看群组信息表初始树列表错误！", e);
			throw new BaseException("查看群组信息表初始树列表错误！", e);
		}
	}

	/**
	 * 查看群组信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewOrganizatinos() throws BaseException {
		try {
			// organizatinos.getOrgId();
			String orgSelect = this.getRequest().getParameter("orgSelect");
			this.getRequest().setAttribute("orgSelect", orgSelect);

		} catch (Exception e) {
			log.error("查看群组信息列表错误！", e);
			throw new BaseException("查看群组信息列表错误！", e);
		}
		return "viewOrg";
	}

	/**
	 * 查看群组信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String findOrganizatinos() throws BaseException {
		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			if (orguser == null) {
				orguser = new OrgUser();
			}
			orguser.setComId(comId);
			String userNameCn = this.getRequest().getParameter("userNameCn");
			orguser.setUserNameCn(userNameCn);
			if (StringUtil.isNotBlank(userNameCn)) {
				orguser.setOrgId(null);
			}

			List<OrgUser> president = this.iOrgUserBiz.getOrgusersList(this
					.getRollPageDataTables(), orguser);
			for (int i = 0; i < president.size(); i++) {
				orguser = president.get(i);
				orguser.setDeptName(BaseDataInfosUtil
						.convertDeptIdToName(orguser.getDepId()));
			}
			this.getPagejsonDataTables(president);

		} catch (Exception e) {
			log.error("查看群组信息列表错误！", e);
			throw new BaseException("查看群组信息列表错误！", e);
		}
		return null;
	}

	/**
	 * 新增群组成员信息初始化
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String saveOrganizatinosInit() throws BaseException {

		try {
			organizatinos = this.iOrganizatinosBiz
					.getOrganizatinos(organizatinos.getOrgId());
		} catch (Exception e) {
			log.error("新增群组成员信息初始化错误！", e);
			throw new BaseException("新增群组成员信息初始化错误！", e);
		}
		return "add_initOrg";
	}

	/**
	 * 新增群组成员信息
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String saveOrganizatinos() throws BaseException {

		try {
			Long comId = UserRightInfoUtil.getComId(getRequest());
			String orgNames = "";
			String ids = "";
			organizatinos = this.iOrganizatinosBiz.getOrganizatinos(orguser
					.getOrgId());
			orgNames = orguser.getOrgUserIds();
			String[] orgNameSplit = orgNames.split(",");
			for (int i = 0; i < orgNameSplit.length; i++) {
				OrgUser orguse = new OrgUser();
				// String[] orgSp= orgNameSplit[i].split(":");
				ids = orgNameSplit[i];
				Users users = this.iUsersBiz.getUsers(Long.parseLong(ids));
				// names = orgSp[1];
				orguse.setUserName(users.getUserName());
				orguse.setUserNameCn(users.getUserChinesename());
				orguse.setDepId(users.getDepId());
				orguse.setIsUsable("0");
				orguse.setOrgId(organizatinos.getOrgId());
				orguse.setOrgCode(organizatinos.getOrgCode());
				orguse.setOrgName(organizatinos.getOrgName());
				orguse.setComId(comId);
				this.iOrgUserBiz.saveOrguser(orguse);
			}
		} catch (Exception e) {
			log.error("新增群组成员信息错误！", e);
			throw new BaseException("新增群组成员信息错误！", e);
		}
		return "add_org";
	}

	/**
	 * 删除群组成员信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String deleteOrganizatinos() throws BaseException {
		try {
			String id = this.getRequest().getParameter("ids");
			String[] str = id.split(",");
			for (int i = 0; i < str.length; i++) {
				if (str[i] != null) {
					orguser = this.iOrgUserBiz.getOrguser(new Long(str[i]));
					orguser.setIsUsable("1");
					// this.IOrguserBiz.updateOrguser(orguser);
					this.iOrgUserBiz.deleteOrguser(orguser);
				}
			}

			PrintWriter out = this.getResponse().getWriter();
			String message = "删除成功";
			out.println(message);

		} catch (Exception e) {
			log("删除群组成员信息信息错误！", e);
			throw new BaseException("删除群组成员信息信息错误！", e);
		}
		return null;
	}

	/************************************ 群组管理结束 ******************************************************/

	/**
	 * 触屏版 个人信息页面
	 * @return
	 * @throws BaseException
	 */
	public String userMobile() throws BaseException{
		try {
			users = UserRightInfoUtil.getUsers(this.getRequest());
			users.setDepName(BaseDataInfosUtil.convertDeptIdToName(users
					.getDepId()));
		} catch (Exception e) {
			log("触屏版 个人信息页面错误！", e);
			throw new BaseException("触屏版 个人信息页面错误！", e);
		}
		return "userMobile";
	}
	public IDepartmentsBiz getiDepartmentsBiz() {
		return iDepartmentsBiz;
	}

	public void setiDepartmentsBiz(IDepartmentsBiz iDepartmentsBiz) {
		this.iDepartmentsBiz = iDepartmentsBiz;
	}

	public IUsersBiz getIUsersBiz() {
		return iUsersBiz;
	}

	public void setIUsersBiz(IUsersBiz usersBiz) {
		iUsersBiz = usersBiz;
	}

	public Departments getDepartments() {
		return departments;
	}

	public void setDepartments(Departments departments) {
		this.departments = departments;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}

	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public Organizatinos getOrganizatinos() {
		return organizatinos;
	}

	public void setOrganizatinos(Organizatinos organizatinos) {
		this.organizatinos = organizatinos;
	}

	public IOrganizatinosBiz getiOrganizatinosBiz() {
		return iOrganizatinosBiz;
	}

	public void setiOrganizatinosBiz(IOrganizatinosBiz iOrganizatinosBiz) {
		this.iOrganizatinosBiz = iOrganizatinosBiz;
	}

	public OrgUser getOrguser() {
		return orguser;
	}

	public void setOrguser(OrgUser orguser) {
		this.orguser = orguser;
	}

	public IOrgUserBiz getiOrgUserBiz() {
		return iOrgUserBiz;
	}

	public void setiOrgUserBiz(IOrgUserBiz iOrgUserBiz) {
		this.iOrgUserBiz = iOrgUserBiz;
	}

	public IPurchaseDepartBuyerBiz getiPurchaseDepartBuyerBiz() {
		return iPurchaseDepartBuyerBiz;
	}

	public void setiPurchaseDepartBuyerBiz(
			IPurchaseDepartBuyerBiz iPurchaseDepartBuyerBiz) {
		this.iPurchaseDepartBuyerBiz = iPurchaseDepartBuyerBiz;
	}

	public ICategoryBuyerBiz getiCategoryBuyerBiz() {
		return iCategoryBuyerBiz;
	}

	public void setiCategoryBuyerBiz(ICategoryBuyerBiz iCategoryBuyerBiz) {
		this.iCategoryBuyerBiz = iCategoryBuyerBiz;
	}

}
