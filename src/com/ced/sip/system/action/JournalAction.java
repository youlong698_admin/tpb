package com.ced.sip.system.action;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.system.biz.ISysErrorLogBiz;
import com.ced.sip.system.biz.ISysInterfaceLogBiz;
import com.ced.sip.system.biz.ISysUserOperateLogBiz;
import com.ced.sip.system.biz.ISysUserInoutLogBiz;
import com.ced.sip.system.entity.SysErrorLog;
import com.ced.sip.system.entity.SysInterfaceLog;
import com.ced.sip.system.entity.SysUserInoutLog;
import com.ced.sip.system.entity.SysUserOperateLog;

public class JournalAction extends BaseAction{
	//用户登录日志
   private ISysUserInoutLogBiz iSysUserInoutLogBiz ;
   //用户操作日志
   private ISysUserOperateLogBiz iSysUserOperateLogBiz;
   	//接口日志
   private ISysInterfaceLogBiz iSysInterfaceLogBiz;
   //错误日志
   private ISysErrorLogBiz iSysErrorLogBiz;
   
   private SysUserInoutLog sysUserInoutLog;
   private SysUserOperateLog sysUserOperateLog;
   private SysInterfaceLog sysInterfaceLog;
   private SysErrorLog sysErrorLog;
   
   /**
    * 查看登陆日志管理列表
    * @return
    * @throws BaseException 
    * @Action
    */
   public String viewRegisterJournal() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看登陆日志列表错误！", e);
			throw new BaseException("查看登陆日志列表错误！", e);
		}
		
		return VIEW ;
		
	}
   /**
    * 查看登陆日志管理列表
    * @return
    * @throws BaseException 
    * @Action
    */
    public String findRegisterJournal() throws BaseException {
    	try{
    	String userChineseName=this.getRequest().getParameter("userChineseName");
    	String ipAddress=this.getRequest().getParameter("ipAddress");
    	String writeDateStare=this.getRequest().getParameter("writeDateStare");
    	String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
    	
    	if(sysUserInoutLog==null){
    		sysUserInoutLog=new SysUserInoutLog();
    	}
    	sysUserInoutLog.setUserChineseName(userChineseName);
    	sysUserInoutLog.setIpAddress(ipAddress);
    	sysUserInoutLog.setLoginDate(DateUtil.StringToDate(writeDateStare, "yyyy-MM-dd"));
    	sysUserInoutLog.setLogoutDate(DateUtil.StringToDate(writeDateEnd, "yyyy-MM-dd"));
    	
    	this.setListValue(this.iSysUserInoutLogBiz.getSysUserInoutLogList(this.getRollPageDataTables(), sysUserInoutLog));
    	this.getPagejsonDataTablesByLongDate(this.getListValue());
    	}catch(Exception e){
    		log.error("查看登陆日志管理列表错误！", e);
			throw new BaseException("查看登陆日志管理列表错误！", e);
    	}
    	return null;
    }
    
    /**
     * 查看用户操作日志管理列表
     * @return
     * @throws BaseException 
     * @Action
     */
    public String viewUserOperateLog() throws BaseException {
 		
 		try{
 			
 		} catch (Exception e) {
 			log.error("查看用户操作日志列表错误！", e);
 			throw new BaseException("查看用户操作日志列表错误！", e);
 		}
 		
 		return "viewOperate";
 		
 	}
    /**
     * 查看用户操作日志管理列表
     * @return
     * @throws BaseException 
     * @Action
     */
     public String findUserOperateLog() throws BaseException {
     	try{
     		
     		String ipAddress=this.getRequest().getParameter("ipAddress");
        	String writer=this.getRequest().getParameter("writer");
        	String method=this.getRequest().getParameter("method");
        	
        	
     	if(sysUserOperateLog==null){
     		sysUserOperateLog=new SysUserOperateLog();
     	}
     	sysUserOperateLog.setIpAddress(ipAddress);
     	sysUserOperateLog.setWriter(writer);
     	sysUserOperateLog.setMethod(method);
     	
     	this.setListValue(this.iSysUserOperateLogBiz.getSysUserOperateLogList(this.getRollPageDataTables(), sysUserOperateLog ));
     	
     	//System.out.println(tSysUserOperateLog.getOperDate());
     	this.getPagejsonDataTablesByLongDate(this.getListValue());
     	
     	}catch(Exception e){
     		log.error("查看用户操作日志列表错误！", e);
 			throw new BaseException("查看用户操作日志列表错误！", e);
     	}
     	return null;
     }
     /**
      * 查看用户操作日志详情
      * @return
      * @throws BaseException 
      * @Action
      */
     public String viewUserOperateLogDetail() throws BaseException {
  		
  		try{
  			String id=this.getRequest().getParameter("uilId");
 			sysUserOperateLog=this.iSysUserOperateLogBiz.getSysUserOperateLog(new Long(id));
 			
 			this.getRequest().setAttribute("userOperateLog", sysUserOperateLog);
  		} catch (Exception e) {
  			log.error("查看用户操作日志列表错误！", e);
  			throw new BaseException("查看用户操作日志列表错误！", e);
  		}
  		
  		return "detailOperate";
  	}
     
     /**
      * 查看接口日志管理列表
      * @return
      * @throws BaseException 
      * @Action
      */
     public String viewInterfaceLog() throws BaseException {
  		
  		try{
  			
  		} catch (Exception e) {
  			log.error("查看接口日志管理列表错误！", e);
  			throw new BaseException("查看接口日志管理列表错误！", e);
  		}
  		
  		return "viewInterface" ;
  		
  	}
     /**
      * 查看接口日志管理列表
      * @return
      * @throws BaseException 
      * @Action
      */
      public String findInterfaceLog() throws BaseException {
      	try{
      		
      		String systemName=this.getRequest().getParameter("systemName");
      		String interfaceName=this.getRequest().getParameter("interfaceName");
      		String writer=this.getRequest().getParameter("writer");
        	String writeDateStare=this.getRequest().getParameter("writeDateStare");
        	String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
        	
      	if(sysInterfaceLog==null){
      		sysInterfaceLog=new SysInterfaceLog();
      	}
      	sysInterfaceLog.setSystemName(systemName);
      	sysInterfaceLog.setInterfaceName(interfaceName);
      	sysInterfaceLog.setWriter(writer);
      	
      	this.setListValue(this.iSysInterfaceLogBiz.getSysInterfaceLogList(this.getRollPageDataTables(), sysInterfaceLog,writeDateStare,writeDateEnd));
      	this.getPagejsonDataTablesByLongDate(this.getListValue());
      	}catch(Exception e){
      		log.error("查看接口日志管理列表错误！", e);
  			throw new BaseException("查看接口日志管理列表错误！", e);
      	}
      	return null;
      }
      
      /**
       * 查看接口日志详情
       * @return
       * @throws BaseException 
       * @Action
       */
      public String viewInterfaceLogDetail() throws BaseException {
   		
   		try{
   			String id=this.getRequest().getParameter("silId");
   			sysInterfaceLog=this.iSysInterfaceLogBiz.getSysInterfaceLog(new Long(id));
   			this.getRequest().setAttribute("interfaceLog", sysInterfaceLog);
   		} catch (Exception e) {
   			log.error("查看接口日志管理列表错误！", e);
   			throw new BaseException("查看接口日志管理列表错误！", e);
   		}
   		
   		return "detailInterface" ;
   		
   	}
      
      /**
       * 查看错误日志管理列表
       * @return
       * @throws BaseException 
       * @Action
       */
      public String viewErrorLog() throws BaseException {
   		
   		try{
   			
   		} catch (Exception e) {
   			log.error("查看错误日志管理列表错误！", e);
   			throw new BaseException("查看错误日志管理列表错误！", e);
   		}
   		
   		return "viewError" ;
   		
   	}
      /**
       * 查看错误日志管理列表
       * @return
       * @throws BaseException 
       * @Action
       */
       public String findErrorLog() throws BaseException {
       	try{
       		
       	String hashcode=this.getRequest().getParameter("hashcode");
       	String ipAddress=this.getRequest().getParameter("ipAddress");
        String writer=this.getRequest().getParameter("writer");
         //	String writeDateStare=this.getRequest().getParameter("writeDateStare");
         //	String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
         	
       	if(sysErrorLog==null){
       		sysErrorLog=new SysErrorLog();
       	}
       	sysErrorLog.setHashcode(hashcode);
       	sysErrorLog.setIpAddress(ipAddress);
       	sysErrorLog.setWriter(writer);
       	
       	this.setListValue(this.iSysErrorLogBiz.getSysErrorLogList(this.getRollPageDataTables(), sysErrorLog));
      	this.getPagejsonDataTablesByLongDate(this.getListValue());
       	}catch(Exception e){
       		log.error("查看错误日志管理列表错误！", e);
   			throw new BaseException("查看错误日志管理列表错误！", e);
       	}
       	return null;
       }
       
       /**
        * 查看错误日志详情
        * @return
        * @throws BaseException 
        * @Action
        */
       public String viewErrorLogDetail() throws BaseException {
    		
    		try{
    			String id=this.getRequest().getParameter("elId");
    			sysErrorLog=this.iSysErrorLogBiz.getSysErrorLog(new Long(id));
    			this.getRequest().setAttribute("errorLog", sysErrorLog);
    		} catch (Exception e) {
    			log.error("查看错误日志管理列表错误！", e);
    			throw new BaseException("查看错误日志管理列表错误！", e);
    		}
    		
    		return "detailError" ;
    		
    	}
	public ISysUserInoutLogBiz getiSysUserInoutLogBiz() {
		return iSysUserInoutLogBiz;
	}
	public void setiSysUserInoutLogBiz(ISysUserInoutLogBiz iSysUserInoutLogBiz) {
		this.iSysUserInoutLogBiz = iSysUserInoutLogBiz;
	}
	public SysUserInoutLog getSysUserInoutLog() {
		return sysUserInoutLog;
	}
	public void setSysUserInoutLog(SysUserInoutLog sysUserInoutLog) {
		this.sysUserInoutLog = sysUserInoutLog;
	}
	public ISysUserOperateLogBiz getiSysUserOperateLogBiz() {
		return iSysUserOperateLogBiz;
	}
	public void setiSysUserOperateLogBiz(ISysUserOperateLogBiz iSysUserOperateLogBiz) {
		this.iSysUserOperateLogBiz = iSysUserOperateLogBiz;
	}
	public SysUserOperateLog getSysUserOperateLog() {
		return sysUserOperateLog;
	}
	public void setSysUserOperateLog(SysUserOperateLog sysUserOperateLog) {
		this.sysUserOperateLog = sysUserOperateLog;
	}
	public ISysInterfaceLogBiz getiSysInterfaceLogBiz() {
		return iSysInterfaceLogBiz;
	}
	public void setiSysInterfaceLogBiz(ISysInterfaceLogBiz iSysInterfaceLogBiz) {
		this.iSysInterfaceLogBiz = iSysInterfaceLogBiz;
	}
	public SysInterfaceLog getSysInterfaceLog() {
		return sysInterfaceLog;
	}
	public void setSysInterfaceLog(SysInterfaceLog sysInterfaceLog) {
		this.sysInterfaceLog = sysInterfaceLog;
	}
	public ISysErrorLogBiz getiSysErrorLogBiz() {
		return iSysErrorLogBiz;
	}
	public void setiSysErrorLogBiz(ISysErrorLogBiz iSysErrorLogBiz) {
		this.iSysErrorLogBiz = iSysErrorLogBiz;
	}
	public SysErrorLog getSysErrorLog() {
		return sysErrorLog;
	}
	public void setSysErrorLog(SysErrorLog sysErrorLog) {
		this.sysErrorLog = sysErrorLog;
	}
   
   
	
}
