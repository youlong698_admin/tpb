package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysUserInoutLog;

public interface ISysUserInoutLogBiz {

	/**
	 * 根据主键获得登录日志表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysUserInoutLog getSysUserInoutLog(Long id) throws BaseException;

	/**
	 * 添加登录日志信息
	 * @param TSysUserInoutLog 登录日志表实例
	 * @throws BaseException 
	 */
	abstract void saveSysUserInoutLog(SysUserInoutLog TSysUserInoutLog)
			throws BaseException;

	/**
	 * 更新登录日志表实例
	 * @param TSysUserInoutLog 登录日志表实例
	 * @throws BaseException 
	 */
	abstract void updateSysUserInoutLog(SysUserInoutLog TSysUserInoutLog)
			throws BaseException;

	/**
	 * 删除登录日志表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysUserInoutLog(String id) throws BaseException;

	/**
	 * 删除登录日志表实例
	 * @param TSysUserInoutLog 登录日志表实例
	 * @throws BaseException 
	 */
	abstract void deleteSysUserInoutLog(SysUserInoutLog SysUserInoutLog)
			throws BaseException;

	/**
	 * 记录用户退出日志
	 * @param sessionPath 
	 * @throws BaseException 
	 */
	public void userLogoutLog( String sessionPath )  throws BaseException ;
	/**
	 * 获得所有系统登陆日志表数据集
	 * @param rollPage 分页对象
	 * @param tSysUserInoutLog 查询参数对象
	 * @author luguanglei  2016-02-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSysUserInoutLogList(RollPage rollPage,
			SysUserInoutLog SysUserInoutLog) throws BaseException;

}