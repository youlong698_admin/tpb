package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.OrgRights;
import com.ced.sip.system.entity.OrgUser;
import com.ced.sip.system.entity.Users;

public interface  IOrgRightsBiz {
	/**
	 * 根据主键获得群组权限表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract OrgRights getOrgRights(Long id) throws BaseException;
	
	/**
	 * 查询群组权限信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组权限表实例
	 * @throws BaseException 
	 */
	abstract int getOrgRightsInt(OrgRights orgRights) throws BaseException;
	
	/**
	 * 添加群组权限信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组权限表实例
	 * @throws BaseException 
	 */
	abstract void saveOrgRights(OrgRights orgRights) throws BaseException;
	/**
	 * 修改群组权限信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组权限表实例
	 * @throws BaseException 
	 */
	abstract void updateOrgRights(OrgRights orgRights) throws BaseException;
	
	/**
	 * 删除群组权限表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrgRights(String id) throws BaseException;

	/**
	 * 删除群组权限表实例
	 * @author xcb 2016-10-13
	 * @param orguser 群组权限表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrgRights(OrgRights orgRights) throws BaseException;

	/**
	 * 删除群组权限表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrgRights(String[] id) throws BaseException;
	
	/**
	 * 获得所有群组权限表数据集
	 * @author xcb 2016-10-13
	 * @param orguser 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<OrgRights> getOrgRightsList(OrgRights orgRights) throws BaseException;

	/**
	 * 获得所有群组权限表数据集
	 * @author xcb 2016-10-13
	 * @param orguser 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<OrgRights> getOrgRightsList( RollPage rollPage,OrgRights orgRights) throws BaseException;

	
}
