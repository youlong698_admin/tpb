package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.MaterialKind;
import com.ced.sip.system.entity.MaterialList;

public interface IMaterialBiz {
	/**
	 * 根据主键获得采购类别表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract MaterialKind getMaterialKind(Long id) throws BaseException;

	/**
	 * 添加采购类别信息
	 * @param materialKind 采购类别表实例
	 * @throws BaseException 
	 */
	abstract void saveMaterialKind(MaterialKind materialKind) throws BaseException;

	/**
	 * 更新采购类别表实例
	 * @param materialKind 采购类别表实例
	 * @throws BaseException 
	 */
	abstract void updateMaterialKind(MaterialKind materialKind) throws BaseException;

	/**
	 * 删除采购类别表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteMaterialKind(String id) throws BaseException;

	/**
	 * 删除采购类别表实例
	 * @param materialKind 采购类别表实例
	 * @throws BaseException 
	 */
	abstract void deleteMaterialKind(MaterialKind materialKind) throws BaseException;

	/**
	 * 删除采购类别表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteMaterialKinds(String[] id) throws BaseException;
	
	/**
	 * 获得所有采购类别表数据集
	 * @param materialKind 查询参数对象
	 * @author luguanglei 2015-01-23
	 * @return
	 * @throws BaseException 
	 */
	public List<MaterialKind> getMaterialKindWithTreeList(MaterialKind materialKind ) throws BaseException;
	/**
	 * 获得所有采购类别表数据集
	 * @param comId 
	 * @author luguanglei 2015-01-23
	 * @return
	 * @throws BaseException 
	 */
	public List getAllMaterialListForTree(Long comId) throws BaseException;
	
	/**
	 * 获得所有采购类别表数据集
	 * @param materialKind 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMaterialKindList(MaterialKind materialKind ) throws BaseException ;
	/**
	 *  获得所有采购类别表数据集
	 * materialKind 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract MaterialKind getMaterialKindByMaterialKind(MaterialKind materialKind) throws BaseException ;
	
	/**
	 * 获得采购价格三级联动数据集
	 * @param materialKind 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMatList(MaterialKind materialKind ) throws BaseException ;
	/**
	 * 获得所有采购类别表数据集
	 * @param rollPage 分页对象
	 * @param materialKind 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMaterialKindList(RollPage rollPage, MaterialKind materialKind)
			throws BaseException;
	
	
	/**
	 * 根据主键获得物料信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract MaterialList getMaterialList(Long id) throws BaseException;

	/**
	 * 添加物料信息信息
	 * @param materialList 物料信息表实例
	 * @throws BaseException 
	 */
	abstract void saveMaterialList(MaterialList materialList) throws BaseException;

	/**
	 * 更新物料信息表实例
	 * @param materialList 物料信息表实例
	 * @throws BaseException 
	 */
	abstract void updateMaterialList(MaterialList materialList) throws BaseException;

	/**
	 * 删除物料信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteMaterialList(String id) throws BaseException;

	/**
	 * 删除物料信息表实例
	 * @param materialList 物料信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteMaterialList(MaterialList materialList) throws BaseException;

	/**
	 * 删除物料信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteMaterialLists(String[] id) throws BaseException;
	
	/**
	 * 获得所有物料信息表数据集
	 * @param materialList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMaterialListList(MaterialList materialList ) throws BaseException ;
	
	/**
	 * 根据编码获得物料信息表数据集
	 * @param materialCode 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract MaterialList getMaterialListByCode(String materialCode ) throws BaseException ;
	
	/**
	 * 获得所有物料信息表数据集
	 * @param rollPage 分页对象
	 * @param materialList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMaterialListList(RollPage rollPage, MaterialList materialList)
			throws BaseException;
	/**
	 * 获得指定物料信息表数据集
	 * @param materialKind 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMaterialCode(String materialType,String mkCodeLength ) throws BaseException ;
	abstract List getMaterialList(String materialType,String mkCodeLength ) throws BaseException ;
	/**
	 * 获取采购类别中文名
	 * Ushine 2016-11-17
	 * @param materialKind
	 * @return
	 * @throws BaseException
	 */
	abstract List getMkName(String mkCode) throws BaseException;
	
	/**
	 * 获得指定物料信息表数据集
	 * @param mkCodeLength 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMaterialCodeChart(String mkCodeLength ) throws BaseException ;
	
}
