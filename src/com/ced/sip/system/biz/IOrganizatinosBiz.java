package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.Organizatinos;
import com.ced.sip.system.entity.Users;

public interface IOrganizatinosBiz {

	/**
	 * 根据主键获得群组表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Organizatinos getOrganizatinos(Long id) throws BaseException;

	/**
	 * 添加群组表信息
	 * @param Organizatinos 群组表实例
	 * @throws BaseException 
	 */
	abstract void saveOrganizatinos(Organizatinos organizatinos) throws BaseException;

	/**
	 * 更新群组表实例
	 * @param organizatinos 群组表实例
	 * @throws BaseException 
	 */
	abstract void updateOrganizatinos(Organizatinos organizatinos)
			throws BaseException;

	/**
	 * 更新群组表实例-修改本级和本级所有子节点为不可用
	 * @param organizatinos 群组表实例
	 * @throws BaseException 
	 */
	abstract void updateOrganizatinosBySelflevCode(  Organizatinos organizatinos  )
		throws BaseException;
	
	/**
	 * 删除群组表实例
	 * @param id 主键
	 * @throws BaseException 
	 */
	abstract void deleteOrganizatinos(Long id) throws BaseException;
		
	/**
	 * 删除群组表实例
	 * @param organizatinos 群组表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrganizatinos(Organizatinos organizatinos)
			throws BaseException;

	/**
	 * 删除群组表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteDemos(String[] id) throws BaseException;

	/**
	 * 获得所有群组数量
	 * @param organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	public int getOrganizatinosCount( Organizatinos organizatinos ) throws BaseException ;
	
	/**
	 * 获得所有群组数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Organizatinos> getOrganizatinosList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有群组数据集
	 * @param organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Organizatinos> getOrganizatinosList(Organizatinos organizatinos)
			throws BaseException;

	/**
	 * 获得所有群组数据集
	 * @param rollPage 分页对象
	 * @param organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Organizatinos> getOrganizatinosList(RollPage rollPage,
			Organizatinos organizatinos) throws BaseException;
	

	/**
	 * 获得所有群组数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<Organizatinos> getTreeOrganizatinosList() throws BaseException ;
	/**
	 * 获得所有人员表数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<Users> getUsersList( RollPage rollPage) throws BaseException ;
	/**
	 * 获得根据群组编号数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<Organizatinos> getOrganizatinosCodeList(Organizatinos organizatinos ) throws BaseException ;
	/**
	 * 获得上级群组编号数据集，如果有下级群组，上级群组不允许删除
	 * @return
	 * @throws BaseException 
	 */
	public List<Organizatinos> getOrganizatinosupDepIdList(Organizatinos organizatinos ) throws BaseException;
	
	/**
	 * 获得所有群组人员视图
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAllOrgRoot() throws BaseException ;
}