package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysCompany;
/** 
 * 类名称：ISysCompanyBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-24
 */
public interface ISysCompanyBiz {

	/**
	 * 根据主键获得采购单位表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysCompany getSysCompany(Long id) throws BaseException;

	/**
	 * 添加采购单位信息
	 * @param sysCompany 采购单位表实例
	 * @throws BaseException 
	 */
	abstract void saveSysCompany(SysCompany sysCompany) throws BaseException;

	/**
	 * 更新采购单位表实例
	 * @param sysCompany 采购单位表实例
	 * @throws BaseException 
	 */
	abstract void updateSysCompany(SysCompany sysCompany) throws BaseException;

	/**
	 * 删除采购单位表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysCompany(String id) throws BaseException;

	/**
	 * 删除采购单位表实例
	 * @param sysCompany 采购单位表实例
	 * @throws BaseException 
	 */
	abstract void deleteSysCompany(SysCompany sysCompany) throws BaseException;

	/**
	 * 删除采购单位表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysCompanys(String[] id) throws BaseException;

	/**
	 * 获得采购单位表数据集
	 * sysCompany 采购单位表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract SysCompany getSysCompanyBySysCompany(SysCompany sysCompany) throws BaseException ;
	
	/**
	 * 获得所有采购单位表数据集
	 * @param sysCompany 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysCompanyList(SysCompany sysCompany) throws BaseException ;
	
	/**
	 * 获得所有采购单位表数据集
	 * @param rollPage 分页对象
	 * @param sysCompany 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysCompanyList(RollPage rollPage, SysCompany sysCompany)
			throws BaseException;

}