package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.CategoryBuyer;
/** 
 * 类名称：ICategoryBuyerBiz
 * 创建人：luguanglei 
 * 创建时间：2017-03-19
 */
public interface ICategoryBuyerBiz {

	/**
	 * 根据主键获得表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract CategoryBuyer getCategoryBuyer(Long id) throws BaseException;

	/**
	 * 添加信息
	 * @param categoryBuyer 表实例
	 * @throws BaseException 
	 */
	abstract void saveCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException;

	/**
	 * 更新表实例
	 * @param categoryBuyer 表实例
	 * @throws BaseException 
	 */
	abstract void updateCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException;

	/**
	 * 删除表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteCategoryBuyer(String id) throws BaseException;

	/**
	 * 删除表实例
	 * @param categoryBuyer 表实例
	 * @throws BaseException 
	 */
	abstract void deleteCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException;

	/**
	 * 删除表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteCategoryBuyers(String[] id) throws BaseException;

	/**
	 * 获得所有表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCategoryBuyerList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有表数据集
	 * @param categoryBuyer 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCategoryBuyerList(CategoryBuyer categoryBuyer) throws BaseException ;
	
	/**
	 * 获得所有表数据集
	 * @param rollPage 分页对象
	 * @param categoryBuyer 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCategoryBuyerList(RollPage rollPage, CategoryBuyer categoryBuyer)
			throws BaseException;

	/**
	 * 依据用户id删除表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteCategoryBuyerByBuyerId(Long buyerId) throws BaseException;
	
	/**
	 * 批量保存采购员所管理的采购类别
	 * @param materialKindIds
	 * @param userId
	 * @param comId
	 * @throws BaseException
	 */
	abstract void saveCategoryBuyerByMaterialKindIds(String materialKindIds,Long userId,Long comId) throws BaseException;
}