package com.ced.sip.system.biz;

import java.sql.Timestamp;
import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.OrgUser;
import com.ced.sip.system.entity.Users;

public interface IUsersBiz {

	/**
	 * 根据主键获得用户表实例
	 * @author zwp 2017-10-19
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Users getUsers(Long id) throws BaseException;

	/**
	 * 添加用户信息
	 * @author zwp 2017-10-19
	 * @param user 用户表实例
	 * @throws BaseException 
	 */
	abstract void saveUsers(Users user) throws BaseException;

	/**
	 * 更新用户表实例
	 * @author zwp 2017-10-19
	 * @param user 用户表实例
	 * @throws BaseException 
	 */
	abstract void updateUsers(Users user) throws BaseException;

	/**
	 * 删除用户表实例
	 * @author zwp 2017-10-19
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteUsers(String id) throws BaseException;

	/**
	 * 删除用户表实例
	 * @author zwp 2017-10-19
	 * @param user 用户表实例
	 * @throws BaseException 
	 */
	abstract void deleteUsers(Users user) throws BaseException;

	/**
	 * 删除用户表实例
	 * @author zwp 2017-10-19
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteUserss(String[] id) throws BaseException;
	
	/**
	 * 删除用户表实例
	 * @author zwp 2017-10-19
	 * @param list 对象数组
	 * @throws BaseException 
	 */
	abstract void deleteUserss(List list) throws BaseException;

	/**
	 * 获得所有用户表数据集
	 * @author zwp 2017-10-19
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersList(RollPage rollPage) throws BaseException;

	/**
	 * 获得所有用户表数据集
	 * @author zwp 2017-10-19
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersList(Users user) throws BaseException;
	/**
	 * 获得所有用户表数据集
	 * @author zwp 2017-10-19
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersList(RollPage rollPage, Users user)
			throws BaseException;

	/**
	 * 获得所有用户表数据集
	 * @author zwp 2017-10-19
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersList(RollPage rollPage, Users user, Departments departments)
			throws BaseException;
	
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return departments 部门
	 * @author zwp 2017-10-19
	 * @throws BaseException 
	 */
	abstract List getUsersList( Users user, Departments departments) throws BaseException;
	
	
	/**
	 * 集合数量用户表实例
	 * @author zwp 2017-10-19
	 * @param projectId
	 * @throws BaseException
	 */
	public int countUsers(Users users) throws BaseException;
	/**
	 * 获得所有用户表数据
	 * @author zwp 2017-10-19
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersList() throws BaseException;

	/**
	 * 获取用户集合
	 * @param user 用户实体类
	 * @param cert 资质实体类
	 * @author zwp
	 */
	abstract List getUserList( String name
			) throws BaseException;
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @author 谢磊
	 * @throws BaseException 
	 */
	abstract List getUsersListRight( RollPage rollPage, Users user ) throws BaseException;
	
	/**
	 * 集合数量用户表实例
	 * @author 谢磊 2017-11-9
	 * @param users 用户
	 * @throws BaseException
	 */
	abstract int countDeptUsers(Users users) throws BaseException;
	
	/**
	 * 根据登录名获得用户表数据  
	 * @author fyh
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersListByUserName( String userName) throws BaseException;
	/**
	 * 根据群组信息获取群组下面的人员信息
	 * @param ur
	 * @return
	 * @throws BaseException
	 */	
	abstract List getUsersListByOrgUser(OrgUser ur) throws BaseException;
	/**
	 * 获得所有用户表数据集  岗位并不是空的
	 * @author zwp 2017-10-19
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getUsersListByNotStation(Users user) throws BaseException;
}