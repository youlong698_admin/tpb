package com.ced.sip.system.biz;

import com.ced.base.exception.BaseException;
import com.ced.sip.system.entity.SystemConfiguration;
/** 
 * 类名称：ISystemConfigurationBiz
 * 创建人：luguanglei 
 * 创建时间：2017-03-30
 */
public interface ISystemConfigurationBiz {

	/**
	 * 根据主键获得系统配置表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SystemConfiguration getSystemConfiguration(Long comId) throws BaseException;

	/**
	 * 更新系统配置表表实例
	 * @param systemConfiguration 系统配置表表实例
	 * @throws BaseException 
	 */
	abstract void updateSystemConfiguration(SystemConfiguration systemConfiguration) throws BaseException;

	/**
	 * 保存系统配置表表实例
	 * @param systemConfiguration 系统配置表表实例
	 * @throws BaseException 
	 */
	abstract void saveSystemConfiguration(SystemConfiguration systemConfiguration) throws BaseException;


}