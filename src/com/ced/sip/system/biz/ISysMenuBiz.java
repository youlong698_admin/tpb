package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysMenu;

public interface ISysMenuBiz {
	
	/**
	 * 根据主键获得菜单表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysMenu getSysMenu(Long id) throws BaseException;
	
	public SysMenu getSysMenuById(String id) throws BaseException ;
	
	/**
	 * 根据名称获得菜单表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public SysMenu getSysMenuByName(String menuName) throws BaseException;

	/**
	 * 添加菜单表信息
	 * @param Departments 菜单表实例
	 * @throws BaseException 
	 */
	public void saveSysMenu(SysMenu sysMenu) throws BaseException;
	/**
	 * 更新菜单表实例
	 * @param departments 菜单表实例
	 * @throws BaseException 
	 */
	public void updateSysMenu(SysMenu sysMenu)
			throws BaseException;

	/**
	 * 删除菜单表实例
	 * @param id 主键
	 * @throws BaseException 
	 */
	public void deleteSysMenu(Long id) throws BaseException;
		
	/**
	 * 删除菜单表实例
	 * @param departments 菜单表实例
	 * @throws BaseException 
	 */
	public void deleteSysMenu(SysMenu sysMenu)
			throws BaseException;

	/**
	 * 删除菜单表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysMenu(String[] id) throws BaseException;

	/**
	 * 获得所有菜单数量
	 * @param departments 菜单对象
	 * @return
	 * @throws BaseException 
	 */
	public int getSysMenuCount( SysMenu sysMenu ) throws BaseException ;
	
	/**
	 * 获得所有菜单数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List<SysMenu> getSysMenuList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有菜单数据集
	 * @param departments 菜单对象
	 * @return
	 * @throws BaseException 
	 */
	public List<SysMenu> getSysMenuList(SysMenu sysMenu)
			throws BaseException;

	/**
	 * 获得所有菜单数据集
	 * @param rollPage 分页对象
	 * @param departments 菜单对象
	 * @return
	 * @throws BaseException 
	 */
	public List<SysMenu> getSysMenuList(RollPage rollPage,
			SysMenu sysMenu) throws BaseException;
	
	/**
	 * 获得所有菜单数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<SysMenu> getTreeSysMenuListByUserId(String userId) throws BaseException ;
	
	/**
	 * 根据父级编号查询自己菜单数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<SysMenu> getFuSysMenuList(String pMenuId) throws BaseException ;
	/**
	 * 获得所有菜单数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<SysMenu> getTreeSysMenuListForSupplier() throws BaseException ;
}
