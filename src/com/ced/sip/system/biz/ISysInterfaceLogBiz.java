package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysInterfaceLog;

public interface ISysInterfaceLogBiz {

	/**
	 * 根据主键获得用户接口日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysInterfaceLog getSysInterfaceLog(Long id) throws BaseException;

	/**
	 * 添加角色权限信息
	 * @param demo 用户接口日志实例
	 * @throws BaseException 
	 */
	abstract void saveSysInterfaceLog(SysInterfaceLog SysInterfaceLog) throws BaseException;

	/**
	 * 更新用户接口日志实例
	 * @param tTSysInterfaceLogRightInfo 用户接口日志实例
	 * @throws BaseException 
	 */
	abstract void updateSysInterfaceLog(SysInterfaceLog SysInterfaceLog) throws BaseException;

	/**
	 * 删除用户接口日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysInterfaceLog(String id) throws BaseException;

	/**
	 * 删除用户接口日志实例
	 * @param tTSysInterfaceLogRightInfo 用户接口日志实例
	 * @throws BaseException 
	 */
	abstract void deleteSysInterfaceLog(SysInterfaceLog SysInterfaceLog) throws BaseException;

	/**
	 * 删除用户接口日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysInterfaceLog(String[] id) throws BaseException;

	/**
	 * 获得所有用户接口日志数据集
	 * @param tTSysInterfaceLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysInterfaceLogList(SysInterfaceLog SysInterfaceLog ) throws BaseException ;
	
	/**
	 * 获得所有用户接口日志数据集
	 * @param rollPage 分页对象
	 * @param tTSysInterfaceLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysInterfaceLogList(RollPage rollPage, SysInterfaceLog SysInterfaceLog,String writeDateStare,String writeDateEnd)
			throws BaseException;
	
}
