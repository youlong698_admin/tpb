package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysEmailSmsLog;
/** 
 * 类名称：ISysEmailSmsLogBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-10
 */
public interface ISysEmailSmsLogBiz {

	/**
	 * 根据主键获得用户邮件短信日志表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysEmailSmsLog getSysEmailSmsLog(Long id) throws BaseException;

	/**
	 * 添加用户邮件短信日志信息
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @throws BaseException 
	 */
	abstract void saveSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException;

	/**
	 * 更新用户邮件短信日志表实例
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @throws BaseException 
	 */
	abstract void updateSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException;

	/**
	 * 删除用户邮件短信日志表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysEmailSmsLog(String id) throws BaseException;

	/**
	 * 删除用户邮件短信日志表实例
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @throws BaseException 
	 */
	abstract void deleteSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException;

	/**
	 * 删除用户邮件短信日志表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysEmailSmsLogs(String[] id) throws BaseException;

	/**
	 * 获得用户邮件短信日志表数据集
	 * sysEmailSmsLog 用户邮件短信日志表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract SysEmailSmsLog getSysEmailSmsLogBySysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException ;
	
	/**
	 * 获得所有用户邮件短信日志表数据集
	 * @param sysEmailSmsLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysEmailSmsLogList(SysEmailSmsLog sysEmailSmsLog) throws BaseException ;
	
	/**
	 * 获得所有用户邮件短信日志表数据集
	 * @param rollPage 分页对象
	 * @param sysEmailSmsLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysEmailSmsLogList(RollPage rollPage, SysEmailSmsLog sysEmailSmsLog)
			throws BaseException;

}