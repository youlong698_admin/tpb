package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysUserOperateLog;

public interface ISysUserOperateLogBiz {

	/**
	 * 根据主键获得用户操作日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysUserOperateLog getSysUserOperateLog(Long id) throws BaseException;

	/**
	 * 添加角色权限信息
	 * @param demo 用户操作日志实例
	 * @throws BaseException 
	 */
	abstract void saveSysUserOperateLog(SysUserOperateLog SysUserOperateLog) throws BaseException;

	/**
	 * 更新用户操作日志实例
	 * @param tTSysUserOperateLogRightInfo 用户操作日志实例
	 * @throws BaseException 
	 */
	abstract void updateSysUserOperateLog(SysUserOperateLog SysUserOperateLog) throws BaseException;

	/**
	 * 删除用户操作日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysUserOperateLog(String id) throws BaseException;

	/**
	 * 删除用户操作日志实例
	 * @param tTSysUserOperateLogRightInfo 用户操作日志实例
	 * @throws BaseException 
	 */
	abstract void deleteSysUserOperateLog(SysUserOperateLog SysUserOperateLog) throws BaseException;

	/**
	 * 删除用户操作日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysUserOperateLog(String[] id) throws BaseException;

	/**
	 * 获得所有用户操作日志数据集
	 * @param tTSysUserOperateLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysUserOperateLogList(SysUserOperateLog tSysUserOperateLog ) throws BaseException ;
	
	/**
	 * 获得所有用户操作日志数据集
	 * @param rollPage 分页对象
	 * @param tTSysUserOperateLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysUserOperateLogList(RollPage rollPage, SysUserOperateLog SysUserOperateLog)
			throws BaseException;
	
}
