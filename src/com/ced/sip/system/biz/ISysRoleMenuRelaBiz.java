package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysRoleMenuRela;

public interface ISysRoleMenuRelaBiz {

	/**
	 * 根据主键获得角色权限表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysRoleMenuRela getSysRoleMenuRela(Long id) throws BaseException;

	/**
	 * 添加角色权限信息
	 * @param demo 角色权限表实例
	 * @throws BaseException 
	 */
	abstract void saveSysRoleMenuRela(SysRoleMenuRela sysRoleMenuRela) throws BaseException;

	/**
	 * 更新角色权限表实例
	 * @param tSysRoleRightInfo 角色权限表实例
	 * @throws BaseException 
	 */
	abstract void updateSysRoleMenuRela(SysRoleMenuRela sysRoleMenuRela) throws BaseException;

	/**
	 * 删除角色权限表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysRoleMenuRela(String id) throws BaseException;

	/**
	 * 删除角色权限表实例
	 * @param tSysRoleRightInfo 角色权限表实例
	 * @throws BaseException 
	 */
	abstract void deleteSysRoleMenuRela(SysRoleMenuRela sysRoleMenuRela) throws BaseException;

	/**
	 * 删除角色权限表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysRoleMenuRela(String[] id) throws BaseException;

	/**
	 * 获得所有角色权限表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysRoleMenuRelaList( RollPage rollPage  ) throws BaseException ;
	
	/**
	 * 获得所有角色权限表数据集
	 * @param tSysRoleRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysRoleMenuRelaList(  SysRoleMenuRela sysRoleMenuRela ) throws BaseException ;
	
	/**
	 * 获得所有角色权限表数据集
	 * @param rollPage 分页对象
	 * @param tSysRoleRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysRoleMenuRelaList(RollPage rollPage, SysRoleMenuRela sysRoleMenuRela)
			throws BaseException;
}
