package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.Major;
/** 
 * 类名称：IMajorBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public interface IMajorBiz {

	/**
	 * 根据主键获得专业信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Major getMajor(Long id) throws BaseException;

	/**
	 * 添加专业信息信息
	 * @param major 专业信息表实例
	 * @throws BaseException 
	 */
	abstract void saveMajor(Major major) throws BaseException;

	/**
	 * 更新专业信息表实例
	 * @param major 专业信息表实例
	 * @throws BaseException 
	 */
	abstract void updateMajor(Major major) throws BaseException;

	/**
	 * 删除专业信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteMajor(String id) throws BaseException;

	/**
	 * 删除专业信息表实例
	 * @param major 专业信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteMajor(Major major) throws BaseException;

	/**
	 * 删除专业信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteMajors(String[] id) throws BaseException;

	/**
	 * 获得专业信息表数据集
	 * major 专业信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract Major getMajorByMajor(Major major) throws BaseException ;
	
	/**
	 * 获得所有专业信息表数据集
	 * @param major 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMajorList(Major major) throws BaseException ;
	
	/**
	 * 获得所有专业信息表数据集
	 * @param rollPage 分页对象
	 * @param major 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMajorList(RollPage rollPage, Major major)
			throws BaseException;
	/**
	 * 获得指定专业信息表数据集
	 * @param mjCode 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract List getMajorCode(  String mjCode,String mjCodeLength,Long comId) throws BaseException ;
	

}