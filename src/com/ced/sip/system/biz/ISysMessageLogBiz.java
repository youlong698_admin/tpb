package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysMessageLog;

public interface ISysMessageLogBiz {

	/**
	 * 根据主键获得用户消息日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysMessageLog getSysMessageLog(Long id) throws BaseException;

	/**
	 * 添加角色权限信息
	 * @param demo 用户消息日志实例
	 * @throws BaseException 
	 */
	abstract void saveSysMessageLog(SysMessageLog sysMessageLog) throws BaseException;

	/**
	 * 更新用户消息日志实例
	 * @param tTSysMessageLogRightInfo 用户消息日志实例
	 * @throws BaseException 
	 */
	abstract void updateSysMessageLog(SysMessageLog sysMessageLog) throws BaseException;

	/**
	 * 删除用户消息日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysMessageLog(String id) throws BaseException;

	/**
	 * 删除用户消息日志实例
	 * @param TSysMessageLogRightInfo 用户消息日志实例
	 * @throws BaseException 
	 */
	abstract void deleteSysMessageLog(SysMessageLog SysMessageLog) throws BaseException;

	/**
	 * 删除用户消息日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysMessageLog(String[] id) throws BaseException;

	/**
	 * 获得所有用户消息日志数据集
	 * @param TSysMessageLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysMessageLogList(SysMessageLog sysMessageLog ) throws BaseException ;
	
	/**
	 * 获得所有用户消息日志数据集
	 * @param rollPage 分页对象
	 * @param SysMessageLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysMessageLogList(RollPage rollPage, SysMessageLog sysMessageLog)
			throws BaseException;
	/**
	 * 获得所有用户消息日志总数数据集
	 * @param 
	 * @param Notices 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int getSysMessageLogCount( SysMessageLog sysMessageLog)
			throws BaseException;
}
