package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.INoticesBiz;
import com.ced.sip.system.entity.Notices;

public class NoticesBizImpl extends BaseBizImpl implements INoticesBiz  {
	
	/**
	 * 根据主键获得公告信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public Notices getNotices(Long id) throws BaseException {
		return (Notices)this.getObject(Notices.class, id);
	}
	
	/**
	 * 添加公告信息信息
	 * @param notices 公告信息表实例
	 * @throws BaseException 
	 */
	public void saveNotices(Notices notices) throws BaseException{
		this.saveObject( notices ) ;
	}
	
	/**
	 * 更新公告信息表实例
	 * @param notices 公告信息表实例
	 * @throws BaseException 
	 */
	public void updateNotices(Notices notices) throws BaseException{
		this.updateObject( notices ) ;
	}
	
	/**
	 * 删除公告信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteNotices(String id) throws BaseException {
		this.removeObject( this.getNotices( new Long(id) ) ) ;
	}
	
	/**
	 * 删除公告信息表实例
	 * @param notices 公告信息表实例
	 * @throws BaseException 
	 */
	public void deleteNotices(Notices notices) throws BaseException {
		this.removeObject( notices ) ;
	}
	
	/**
	 * 删除公告信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteNoticess(String[] id) throws BaseException {
		this.removeBatchObject(Notices.class, id) ;
	}
	
	/**
	 * 获得所有公告信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getNoticesList( RollPage rollPage ,int index,String type ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Notices de where 1 = 1 " );
//		//判断公告发布的类型 type  1.通知2.公告3.发文4.其他
//		if ( ! StringUtil.isBlank( type ) ) {
//			hql.append( " and de.type = '" );
//			hql.append( type.trim() );
//			hql.append( "'" );
//		}
		//有效的
		hql.append( " and de.isUsable = '0'" );
		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有公告信息表数据集
	 * @param notices 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getNoticesList(  Notices notices ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Notices de where 1 = 1 " );
		
		hql.append(" and isUsable = '0' order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有公告信息表数据集
	 * @param rollPage 分页对象
	 * @param notices 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getNoticesList( RollPage rollPage, Notices notices ) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,sc.compName from Notices de,SysCompany sc where de.comId=sc.scId " );
		if(StringUtil.isNotBlank(notices)){
			
			if(StringUtil.isNotBlank(notices.getTitle())){
				hql.append(" and de.title like '%").append(notices.getTitle()).append("%'");
			}
			if(StringUtil.isNotBlank(notices.getKeyword())){
				hql.append(" and de.keyword like '%").append(notices.getKeyword()).append("%'");
			}
			if(StringUtil.isNotBlank(notices.getPublishDate())){
				hql.append(" and de.publishDate = ").append("to_date('").append(DateUtil.getDefaultDateFormat(notices.getPublishDate())).append("','yyyy-mm-dd')");
			}
			if (StringUtil.isNotBlank(notices.getComId())) {
				hql.append(" and de.comId = ").append(notices.getComId()).append("");
			}
		}
		hql.append(" and de.isUsable = '0'");
		if(StringUtil.isNotBlank(rollPage.getOrderColumn()))
		    hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		else 
			hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	public int getNoticesCount(Notices notices) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.noticeId) from Notices de where 1 = 1 " );
        if(StringUtil.isNotBlank(notices)){
			
			if(StringUtil.isNotBlank(notices.getTitle())){
				hql.append(" and de.title like '%").append(notices.getTitle()).append("%'");
			}
			if(StringUtil.isNotBlank(notices.getKeyword())){
				hql.append(" and de.keyword like '%").append(notices.getKeyword()).append("%'");
			}
			if(StringUtil.isNotBlank(notices.getPublishDate())){
				hql.append(" and de.publishDate = ").append("to_date('").append(DateUtil.getDefaultDateFormat(notices.getPublishDate())).append("','yyyy-mm-dd')");
			}
			if (StringUtil.isNotBlank(notices.getComId())) {
				hql.append(" and de.comId = ").append(notices.getComId()).append("");
			}
		}
		return this.countObjects(hql.toString());
	}
	
}
