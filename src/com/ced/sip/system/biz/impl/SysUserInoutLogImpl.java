package com.ced.sip.system.biz.impl;

import java.util.Date;
import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.biz.ISysUserInoutLogBiz;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.entity.SysUserInoutLog;
public class SysUserInoutLogImpl extends BaseBizImpl implements ISysUserInoutLogBiz   {
	
	/**
	 * 根据主键获得登录日志表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public SysUserInoutLog getSysUserInoutLog(Long id) throws BaseException {
		return (SysUserInoutLog)this.getObject(SysUserInoutLog.class, id);
	}
	
	/**
	 * 添加登录日志信息
	 * @param TSysUserInoutLog 登录日志表实例
	 * @throws BaseException 
	 */
	public void saveSysUserInoutLog(SysUserInoutLog SysUserInoutLog) throws BaseException{
		this.saveObject(SysUserInoutLog ) ;
	}
	
	/**
	 * 更新登录日志表实例
	 * @param TSysUserInoutLog 登录日志表实例
	 * @throws BaseException 
	 */
	public void updateSysUserInoutLog(SysUserInoutLog SysUserInoutLog) throws BaseException{
		this.updateObject(SysUserInoutLog ) ;
	}
	
	/**
	 * 删除登录日志表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysUserInoutLog(String id) throws BaseException {
		this.removeObject( this.getSysUserInoutLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除登录日志表实例
	 * @param TSysUserInoutLog 登录日志表实例
	 * @throws BaseException 
	 */
	public void deleteSysUserInoutLog(SysUserInoutLog SysUserInoutLog) throws BaseException {
		this.removeObject(SysUserInoutLog ) ;
	}
	
	/**
	 * 记录用户退出日志
	 * @param sessionPath 
	 * @throws BaseException 
	 */
	public void userLogoutLog( String sessionPath )  throws BaseException {
		
		StringBuffer hql = new StringBuffer(" From SysUserInoutLog log where 1=1 and log.sessionPath = '" + sessionPath + "' " );
		List gList = this.getObjects( hql.toString() );
			
		if( gList != null && gList.size()>0 ) {
			SysUserInoutLog userLog = (SysUserInoutLog) gList.get( 0 ) ;
			userLog.setLogoutDate( new Date() ) ;
			userLog.setLineStatus( TableStatus.COMMON_STATUS_INVALID ) ;
			
			this.updateSysUserInoutLog( userLog ) ;
		}
			
	}
	/**
	 * 获得所有系统登陆日志表数据集
	 * @param rollPage 分页对象
	 * @param tSysUserInoutLog 查询参数对象
	 * @author luguanglei  2016-02-28
	 * @return
	 * @throws BaseException 
	 */
	public  List getSysUserInoutLogList(RollPage rollPage,SysUserInoutLog SysUserInoutLog) throws BaseException{
		StringBuffer hql = new StringBuffer(" from SysUserInoutLog de where 1 = 1 " );
		
		if(StringUtil.isNotBlank(SysUserInoutLog))
		{
			if(StringUtil.isNotBlank(SysUserInoutLog.getUserChineseName()))
			{
				hql.append(" and de.userChineseName like '%").append(SysUserInoutLog.getUserChineseName()).append("%'");
			}
			if(StringUtil.isNotBlank(SysUserInoutLog.getIpAddress()))
			{
				hql.append(" and de.ipAddress ='").append(SysUserInoutLog.getIpAddress()).append("'");
			}
			if(StringUtil.isNotBlank(SysUserInoutLog.getLoginDate())){
				hql.append(" and trunc( de.loginDate,'dd')>= to_date('").append( DateUtil.getDefaultDateFormat(SysUserInoutLog.getLoginDate())).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(SysUserInoutLog.getLogoutDate())){
				hql.append(" and trunc( de.loginDate,'dd')<= to_date('").append( DateUtil.getDefaultDateFormat(SysUserInoutLog.getLogoutDate())).append("','yyyy-MM-dd')");
			}
		}

		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
