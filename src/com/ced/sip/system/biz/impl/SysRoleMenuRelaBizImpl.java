package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysRoleMenuRelaBiz;
import com.ced.sip.system.entity.SysRoleMenuRela;
import com.ced.sip.system.entity.SysUserRoleRela;

public class SysRoleMenuRelaBizImpl extends BaseBizImpl implements ISysRoleMenuRelaBiz {

	public SysRoleMenuRela getSysRoleMenuRela(Long id) throws BaseException {
		// TODO Auto-generated method stub
		return (SysRoleMenuRela)this.getObject(SysRoleMenuRela.class, id);
	}

	public void saveSysRoleMenuRela(SysRoleMenuRela sysRoleMenuRela)
			throws BaseException {
		// TODO Auto-generated method stub
		this.saveObject(sysRoleMenuRela);
	}

	public void updateSysRoleMenuRela(SysRoleMenuRela sysRoleMenuRela)
			throws BaseException {
		// TODO Auto-generated method stub
		this.updateObject(sysRoleMenuRela) ;
	}

	public void deleteSysRoleMenuRela(String id) throws BaseException {
		// TODO Auto-generated method stub
		this.removeObject( this.getSysRoleMenuRela(new Long(id)) ) ;
	}

	public void deleteSysRoleMenuRela(SysRoleMenuRela sysRoleMenuRela)
			throws BaseException {
		// TODO Auto-generated method stub
		this.removeObject( sysRoleMenuRela ) ;
	}

	public void deleteSysRoleMenuRela(String[] id) throws BaseException {
		// TODO Auto-generated method stub
		this.removeBatchObject(SysRoleMenuRela.class, id) ;
	}

	public List getSysRoleMenuRelaList(RollPage rollPage) throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from SysRoleMenuRela de where 1 = 1 " );
		
		return this.getObjects( rollPage,hql.toString() );
	}

	public List getSysRoleMenuRelaList(SysRoleMenuRela sysRoleMenuRela)
			throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from SysRoleMenuRela de where 1 = 1 " );
		if(sysRoleMenuRela != null){
			if(StringUtil.isNotBlank(sysRoleMenuRela.getRoleId())){
				hql.append(" and de.roleId='"+sysRoleMenuRela.getRoleId()+"'");
			}
		}
		return this.getObjects(hql.toString() );
	}

	public List getSysRoleMenuRelaList(RollPage rollPage,
			SysRoleMenuRela sysRoleMenuRela) throws BaseException {
		// TODO Auto-generated method stub
		return null;
	}

}
