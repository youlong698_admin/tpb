package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysCompanyBiz;
import com.ced.sip.system.entity.SysCompany;
/** 
 * 类名称：SysCompanyBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-24
 */
public class SysCompanyBizImpl extends BaseBizImpl implements ISysCompanyBiz  {
	
	/**
	 * 根据主键获得采购单位表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-24
	 * @return
	 * @throws BaseException 
	 */
	public SysCompany getSysCompany(Long id) throws BaseException {
		if(id.equals(0L)) return  new SysCompany();
		else return (SysCompany)this.getObject(SysCompany.class, id);
	}
	
	/**
	 * 获得采购单位表实例
	 * @param sysCompany 采购单位表实例
	 * @author luguanglei 2017-05-24
	 * @return
	 * @throws BaseException 
	 */
	public SysCompany getSysCompany(SysCompany sysCompany) throws BaseException {
		return (SysCompany)this.getObject(SysCompany.class, sysCompany.getScId() );
	}
	
	/**
	 * 添加采购单位信息
	 * @param sysCompany 采购单位表实例
	 * @author luguanglei 2017-05-24
	 * @throws BaseException 
	 */
	public void saveSysCompany(SysCompany sysCompany) throws BaseException{
		this.saveObject( sysCompany ) ;
	}
	
	/**
	 * 更新采购单位表实例
	 * @param sysCompany 采购单位表实例
	 * @author luguanglei 2017-05-24
	 * @throws BaseException 
	 */
	public void updateSysCompany(SysCompany sysCompany) throws BaseException{
		this.updateObject( sysCompany ) ;
	}
	
	/**
	 * 删除采购单位表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-24
	 * @throws BaseException 
	 */
	public void deleteSysCompany(String id) throws BaseException {
		this.removeObject( this.getSysCompany( new Long(id) ) ) ;
	}
	
	/**
	 * 删除采购单位表实例
	 * @param sysCompany 采购单位表实例
	 * @author luguanglei 2017-05-24
	 * @throws BaseException 
	 */
	public void deleteSysCompany(SysCompany sysCompany) throws BaseException {
		this.removeObject( sysCompany ) ;
	}
	
	/**
	 * 删除采购单位表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-24
	 * @throws BaseException 
	 */
	public void deleteSysCompanys(String[] id) throws BaseException {
		this.removeBatchObject(SysCompany.class, id) ;
	}
	
	/**
	 * 获得采购单位表数据集
	 * sysCompany 采购单位表实例
	 * @author luguanglei 2017-05-24
	 * @return
	 * @throws BaseException 
	 */
	public SysCompany getSysCompanyBySysCompany(SysCompany sysCompany) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysCompany de where 1 = 1 " );
		if(StringUtil.isNotBlank(sysCompany) ){
			if(StringUtil.isNotBlank(sysCompany.getCompName())){
				hql.append(" and de.compName like '%").append(sysCompany.getCompName()).append("%'");
			}
		}
		hql.append(" order by de.scId desc ");
		List list = this.getObjects( hql.toString() );
		sysCompany = new SysCompany();
		if(list!=null&&list.size()>0){
			sysCompany = (SysCompany)list.get(0);
		}
		return sysCompany;
	}
	
	/**
	 * 获得所有采购单位表数据集
	 * @param sysCompany 查询参数对象
	 * @author luguanglei 2017-05-24
	 * @return
	 * @throws BaseException 
	 */
	public List getSysCompanyList(SysCompany sysCompany) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysCompany de where 1 = 1 " );
		if(StringUtil.isNotBlank(sysCompany) ){
			if(StringUtil.isNotBlank(sysCompany.getCompName())){
				hql.append(" and de.compName like '%").append(sysCompany.getCompName()).append("%'");
			}
		}
		hql.append(" order by de.scId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有采购单位表数据集
	 * @param rollPage 分页对象
	 * @param sysCompany 查询参数对象
	 * @author luguanglei 2017-05-24
	 * @return
	 * @throws BaseException 
	 */
	public List getSysCompanyList(RollPage rollPage, SysCompany sysCompany) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysCompany de where 1 = 1 " );
		if(StringUtil.isNotBlank(sysCompany) ){
			if(StringUtil.isNotBlank(sysCompany.getCompName())){
				hql.append(" and de.compName like '%").append(sysCompany.getCompName()).append("%'");
			}
			if(StringUtil.isNotBlank(sysCompany.getLogo())){
				hql.append(" and de.logo is not null ");
			}
		}
		hql.append(" order by de.scId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
