package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IMajorBiz;
import com.ced.sip.system.entity.Major;
/** 
 * 类名称：MajorBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class MajorBizImpl extends BaseBizImpl implements IMajorBiz  {
	
	/**
	 * 根据主键获得专业信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public Major getMajor(Long id) throws BaseException {
		return (Major)this.getObject(Major.class, id);
	}
	
	/**
	 * 获得专业信息表实例
	 * @param major 专业信息表实例
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public Major getMajor(Major major) throws BaseException {
		return (Major)this.getObject(Major.class, major.getMjId() );
	}
	
	/**
	 * 添加专业信息信息
	 * @param major 专业信息表实例
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void saveMajor(Major major) throws BaseException{
		this.saveObject( major ) ;
	}
	
	/**
	 * 更新专业信息表实例
	 * @param major 专业信息表实例
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void updateMajor(Major major) throws BaseException{
		this.updateObject( major ) ;
	}
	
	/**
	 * 删除专业信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void deleteMajor(String id) throws BaseException {
		this.removeObject( this.getMajor( new Long(id) ) ) ;
	}
	
	/**
	 * 删除专业信息表实例
	 * @param major 专业信息表实例
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void deleteMajor(Major major) throws BaseException {
		this.removeObject( major ) ;
	}
	
	/**
	 * 删除专业信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void deleteMajors(String[] id) throws BaseException {
		this.removeBatchObject(Major.class, id) ;
	}
	
	/**
	 * 获得专业信息表数据集
	 * major 专业信息表实例
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public Major getMajorByMajor(Major major) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Major de where 1 = 1 " );

		hql.append(" order by de.mjId desc ");
		List list = this.getObjects( hql.toString() );
		major = new Major();
		if(list!=null&&list.size()>0){
			major = (Major)list.get(0);
		}
		return major;
	}
	
	/**
	 * 获得所有专业信息表数据集
	 * @param major 查询参数对象
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public List getMajorList(Major major) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Major de where 1 = 1 " );
		if(major != null){
			if(!StringUtil.isBlank(major.getParentId())){
				hql.append(" and de.parentId = ").append(major.getParentId());
			}
			if (StringUtil.isNotBlank(major.getMjId())) {
				hql.append(" and de.mjId = '").append(major.getMjId()).append("'");
			}
			if (StringUtil.isNotBlank(major.getComId())) {
				hql.append(" and de.comId = ").append(major.getComId()).append("");
			}
		}
		hql.append(" and de.isUsable='0' ");
		hql.append(" order by de.displaySort ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有专业信息表数据集
	 * @param rollPage 分页对象
	 * @param major 查询参数对象
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public List getMajorList(RollPage rollPage, Major major) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Major de where 1 = 1 " );

		hql.append(" order by de.mjId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得指定物料信息表数据集
	 * @param mjCode 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getMajorCode(String mjCode,String mjCodeLength,Long comId) throws BaseException {
		StringBuffer hql = new StringBuffer(" select max(de.mjCode) from Major de where 1 = 1 " );
		    hql.append(" and de.comId = ").append(comId).append("");
			hql.append(" and de.mjCode like '%").append(mjCode).append("%'");
			hql.append(" and length(de.mjCode) = ").append(mjCodeLength);
			return this.getObjects(hql.toString() );
		}
	
}
