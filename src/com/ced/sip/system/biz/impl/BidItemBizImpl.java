package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.entity.TenderBidItem;
import com.ced.sip.system.biz.IBidItemBiz;
import com.ced.sip.system.entity.BidItem;

public class BidItemBizImpl extends BaseBizImpl implements IBidItemBiz  {
	
	/**
	 * 根据主键获得评标项表实例
	 * @param id 主键
	 * @author zzn 2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public BidItem getBidItem(Long id) throws BaseException {
		return (BidItem)this.getObject(BidItem.class, id);
	}
	
	/**
	 * 获得评标项表实例
	 * @param bidItem 评标项表实例
	 * @author zzn 2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public BidItem getBidItem( BidItem bidItem ) throws BaseException {
		return (BidItem)this.getObject(BidItem.class, bidItem.getBiId() );
	}
	
	/**
	 * 添加评标项信息
	 * @param bidItem 评标项表实例
	 * @author zzn 2016-08-28
	 * @throws BaseException 
	 */
	public void saveBidItem(BidItem bidItem) throws BaseException{
		this.saveObject( bidItem ) ;
	}
	
	/**
	 * 更新评标项表实例
	 * @param bidItem 评标项表实例
	 * @author zzn 2016-08-28
	 * @throws BaseException 
	 */
	public void updateBidItem(BidItem bidItem) throws BaseException{
		this.updateObject( bidItem ) ;
	}
	
	/**
	 * 删除评标项表实例
	 * @param id 主键数组
	 * @author zzn 2016-08-28
	 * @throws BaseException 
	 */
	public void deleteBidItem(String id) throws BaseException {
		this.removeObject( this.getBidItem( new Long(id) ) ) ;
	}
	
	/**
	 * 删除评标项表实例
	 * @param bidItem 评标项表实例
	 * @author zzn 2016-08-28
	 * @throws BaseException 
	 */
	public void deleteBidItem(BidItem bidItem) throws BaseException {
		this.removeObject( bidItem ) ;
	}
	
	/**
	 * 删除评标项表实例
	 * @param id 主键数组
	 * @author zzn 2016-08-28
	 * @throws BaseException 
	 */
	public void deleteBidItems(String[] id) throws BaseException {
		this.removeBatchObject(BidItem.class, id) ;
	}
	
	/**
	 * 获得所有评标项表数据集
	 * @param rollPage 分页对象
	 * @author zzn 2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public List getBidItemList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidItem de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有评标项表数据集
	 * @param BidItem 查询参数对象
	 * @author zzn 2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public List getBidItemList(  BidItem bidItem ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidItem de where 1 = 1 " );
		if(bidItem != null){
			if(StringUtil.isNotBlank(bidItem.getItemType())){
				hql.append(" and de.itemType ='").append(bidItem.getItemType());
			}
		}
		hql.append(" and de.isUsable = '0' ");
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有评标项表数据集
	 * @param rollPage 分页对象
	 * @param bidItem 查询参数对象
	 * @author zzn 2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public List getBidItemList( RollPage rollPage, BidItem bidItem ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidItem de where 1 = 1 " );
		if (StringUtil.isNotBlank(bidItem)) {
			if (StringUtil.isNotBlank(bidItem.getItemName())) {
				hql.append(" and de.itemName like '%").append(bidItem.getItemName()).append("%'");
			}
			if (StringUtil.isNotBlank(bidItem.getItemType())) {
				hql.append(" and de.itemType = '").append(bidItem.getItemType()).append("'");
			}
			if (StringUtil.isNotBlank(bidItem.getComId())) {
				hql.append(" and de.comId = ").append(bidItem.getComId()).append("");
			}
		}
		hql.append(" and de.isUsable='0'");

		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 获得未存在评标项数据集
	 * @param rollPage 分页对象
	 * @param bidItem 评标项表
	 * @param bidItemSet 评标项设置表
	 */
	public List getBidItemList( RollPage rollPage, BidItem bidItem, TenderBidItem tenderBidItem) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidItem bi where 1 = 1 " );
		if (StringUtil.isNotBlank(bidItem)) {
			if (StringUtil.isNotBlank(bidItem.getItemType())) {
				hql.append(" and bi.itemType = '").append(bidItem.getItemType()).append("'");
				hql.append(" and bi.biId not in (select distinct tbi.itemId from TenderBidItem tbi where tbi.rcId = ")
				.append(tenderBidItem.getRcId()).append("").append(")");
			}
			if (StringUtil.isNotBlank(bidItem.getItemName())) {
				hql.append(" and bi.itemName like '%").append(bidItem.getItemName()).append("%'");
			}
			if (StringUtil.isNotBlank(bidItem.getComId())) {
				hql.append(" and bi.comId = ").append(bidItem.getComId()).append("");
			}
		}
		hql.append(" and bi.isUsable='0'");
		return this.getObjects(rollPage, hql.toString() );
	}
}
