package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IWebInfoBiz;
import com.ced.sip.system.entity.WebInfo;
/** 
 * 类名称：WebInfoBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class WebInfoBizImpl extends BaseBizImpl implements IWebInfoBiz  {
	
	/**
	 * 根据主键获得网站信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public WebInfo getWebInfo(Long id) throws BaseException {
		return (WebInfo)this.getObject(WebInfo.class, id);
	}
	
	/**
	 * 获得网站信息表实例
	 * @param webInfo 网站信息表实例
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public WebInfo getWebInfo(WebInfo webInfo) throws BaseException {
		return (WebInfo)this.getObject(WebInfo.class, webInfo.getWiId() );
	}
	
	/**
	 * 添加网站信息信息
	 * @param webInfo 网站信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void saveWebInfo(WebInfo webInfo) throws BaseException{
		this.saveObject( webInfo ) ;
	}
	
	/**
	 * 更新网站信息表实例
	 * @param webInfo 网站信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void updateWebInfo(WebInfo webInfo) throws BaseException{
		this.updateObject( webInfo ) ;
	}
	
	/**
	 * 删除网站信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteWebInfo(String id) throws BaseException {
		this.removeObject( this.getWebInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除网站信息表实例
	 * @param webInfo 网站信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteWebInfo(WebInfo webInfo) throws BaseException {
		this.removeObject( webInfo ) ;
	}
	
	/**
	 * 删除网站信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteWebInfos(String[] id) throws BaseException {
		this.removeBatchObject(WebInfo.class, id) ;
	}
	
	/**
	 * 获得网站信息表数据集
	 * webInfo 网站信息表实例
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public WebInfo getWebInfoByWebInfo(WebInfo webInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WebInfo de where 1 = 1 " );
		{
			if(StringUtil.isNotBlank(webInfo.getWmId()))
			{
				hql.append(" and de.wmId=").append(webInfo.getWmId()).append("");
			}
			
		}
		hql.append(" order by de.wiId desc ");
		List list = this.getObjects( hql.toString() );
		webInfo = new WebInfo();
		if(list!=null&&list.size()>0){
			webInfo = (WebInfo)list.get(0);
		}
		return webInfo;
	}
	
	/**
	 * 获得所有网站信息表数据集
	 * @param webInfo 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getWebInfoList(WebInfo webInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WebInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(webInfo))
		{
			if(StringUtil.isNotBlank(webInfo.getWmId()))
			{
				hql.append(" and de.wmId=").append(webInfo.getWmId()).append("");
			}
			
		}
		hql.append(" order by de.wiId");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有网站信息表数据集
	 * @param rollPage 分页对象
	 * @param webInfo 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getWebInfoList(RollPage rollPage, WebInfo webInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WebInfo de where 1 = 1 " );
		{
			if(StringUtil.isNotBlank(webInfo.getWmId()))
			{
				hql.append(" and de.wmId=").append(webInfo.getWmId()).append("");
			}
			if(StringUtil.isNotBlank(webInfo.getTitle())){
				hql.append(" and de.title like '%").append(webInfo.getTitle()).append("%'");
			}
			if(StringUtil.isNotBlank(webInfo.getPublishDate())){
				hql.append(" and de.publishDate = ").append("to_date('").append(DateUtil.getDefaultDateFormat(webInfo.getPublishDate())).append("','yyyy-mm-dd')");
			}	
			if(webInfo.getIsIndex()==2){
				hql.append(" and de.isIndex="+webInfo.getIsIndex());
			}
		}
		if(StringUtil.isNotBlank(rollPage.getOrderColumn())) hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 获得所有网站信息表数据集   网站信息
	 * @param rollPage 分页对象
	 * @param webInfo 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getWebInfoListWeb(RollPage rollPage, WebInfo webInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WebInfo de where 1 = 1 and de.status=2 " );
		{
			if(StringUtil.isNotBlank(webInfo.getWmId()))
			{
				hql.append(" and de.wmId=").append(webInfo.getWmId()).append("");
			}
			if(StringUtil.isNotBlank(webInfo.getContent())){
				hql.append(" and (de.title like '%").append(webInfo.getContent()).append("%' or de.content like '%").append(webInfo.getContent()).append("%')");
			}
			if(StringUtil.isNotBlank(webInfo.getPublishDate())){
				hql.append(" and de.publishDate = ").append("to_date('").append(DateUtil.getDefaultDateFormat(webInfo.getPublishDate())).append("','yyyy-mm-dd')");
			}	
			if(webInfo.getIsIndex()==2){
				hql.append(" and de.isIndex="+webInfo.getIsIndex());
			}
		}
		hql.append(" order by de.publishDate desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有网站信息表数据集   网站信息 重要通知公告
	 * @param rollPage 分页对象
	 * @param webInfo 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getWebInfoListWebNotice(RollPage rollPage, WebInfo webInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WebInfo de where 1 = 1 and de.status=2 and  de.wmId=33" );
		{
			if(StringUtil.isNotBlank(webInfo.getWmId()))
			{
				hql.append(" and de.wmId=").append(webInfo.getWmId()).append("");
			}
			if(StringUtil.isNotBlank(webInfo.getTitle())){
				hql.append(" and de.title like '%").append(webInfo.getTitle()).append("%'");
			}
			if(StringUtil.isNotBlank(webInfo.getPublishDate())){
				hql.append(" and de.publishDate = ").append("to_date('").append(DateUtil.getDefaultDateFormat(webInfo.getPublishDate())).append("','yyyy-mm-dd')");
			}	
			if(webInfo.getIsIndex()==2){
				hql.append(" and de.isIndex="+webInfo.getIsIndex());
			}
		}
		hql.append(" order by de.publishDate desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
}
