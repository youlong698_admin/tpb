package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IOrganizatinosBiz;
import com.ced.sip.system.entity.Organizatinos;
import com.ced.sip.system.entity.Users;

public class OrganizatinosBizImpl extends BaseBizImpl implements IOrganizatinosBiz   {

	/**
	 * 根据主键获得群组表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public Organizatinos getOrganizatinos(Long id) throws BaseException {
		return (Organizatinos)this.getObject(Organizatinos.class, id);
	}
	
	/**
	 * 添加群组表信息
	 * @param Organizatinos 群组表实例
	 * @throws BaseException 
	 */
	public void saveOrganizatinos(Organizatinos organizatinos) throws BaseException{
		this.saveObject( organizatinos ) ;
	}
	
	/**
	 * 更新群组表实例
	 * @param organizatinos 群组表实例
	 * @throws BaseException 
	 */
	public void updateOrganizatinos(Organizatinos organizatinos ) throws BaseException{
		this.updateObject( organizatinos ) ;
	}
	
	/**
	 * 更新群组表实例-修改本级和本级所有子节点为不可用
	 * @param organizatinos 群组表实例
	 * @throws BaseException 
	 */
	public void updateOrganizatinosBySelflevCode(  Organizatinos organizatinos  )throws BaseException{
		StringBuffer sbHql = new StringBuffer( " from  Organizatinos org where  1=1 " );
		
		if( organizatinos != null && organizatinos.getOrgId() != null ) {
			sbHql.append( " and org.selflevCode like '%," ).append( organizatinos.getOrgId() ).append(",%'") ;
			sbHql.append( " or org.orgId = " ).append( organizatinos.getOrgId() );
			
			List orgList= getObjects( sbHql.toString() );
			
			if(orgList!=null){
				for(int i=0;i<orgList.size();i++){
					Organizatinos  org = (Organizatinos) orgList.get(i);
					org.setIsUsable("1");
					
					this.updateOrganizatinos( org ) ;
				}
			}
		}
	}
	
	/**
	 * 删除群组表实例
	 * @param id 主键
	 * @throws BaseException 
	 */
	public void deleteOrganizatinos(Long id) throws BaseException {
		this.removeObject( this.getOrganizatinos( new Long(id) ) ) ;
	}
	
	/**
	 * 删除群组表实例
	 * @param organizatinos 群组表实例
	 * @throws BaseException 
	 */
	public void deleteOrganizatinos(Organizatinos organizatinos) throws BaseException {
		this.removeObject( organizatinos ) ;
	}
	
	/**
	 * 删除群组表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteDemos(String[] id) throws BaseException {
		this.removeBatchObject(Organizatinos.class, id) ;
	} 
	
	/**
	 * 获得所有群组数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List<Organizatinos> getOrganizatinosList( RollPage rollPage ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Organizatinos org where 1 = 1 " );
		hql.append(" and org.isUsable = '0' ") ;
		hql.append(" order by org.remark asc ") ;
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有群组数据集
	 * @Cacheable(cacheName="departCache") 
	 * @param organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	
	public List<Organizatinos> getOrganizatinosList( Organizatinos organizatinos ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Organizatinos org where 1 = 1 " );

		if( organizatinos != null ) {
			// 父类ID
			if( organizatinos.getParentOrgId() != null ) {
				hql.append(" and org.parentOrgId = " ).append( organizatinos.getParentOrgId() ) ;
			}
			// 群组编码
			if( organizatinos.getOrgCode() != null && organizatinos.getOrgCode().length()>0 ) {
				hql.append(" and org.orgCode = '" ).append( organizatinos.getOrgCode() ).append("' ") ;
			}
			if( organizatinos.getOrgName() != null && organizatinos.getOrgName().length()>0 ) {
				hql.append(" and org.orgName = '" ).append( organizatinos.getOrgName() ).append("' ") ;
			}
			if (StringUtil.isNotBlank(organizatinos.getComId())) {
				hql.append(" and org.comId = ").append(organizatinos.getComId()).append("");
			}
		}
		hql.append(" and org.isUsable = '0' ") ;
		hql.append(" order by org.orgOrder asc ") ;
		
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有群组数据集
	 * @param rollPage 分页对象
	 * @param organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	public List<Organizatinos> getOrganizatinosList( RollPage rollPage, Organizatinos organizatinos ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Organizatinos org where 1 = 1 " );

		if( organizatinos != null ) {
			// 父类ID
			if( organizatinos.getParentOrgId() != null ) {
				hql.append(" and org.parentOrgId = " ).append( organizatinos.getParentOrgId() ) ;
			}
			// 群组编码
			if( organizatinos.getOrgCode() != null && organizatinos.getOrgCode().length()>0 ) {
				hql.append(" and org.orgCode = '" ).append( organizatinos.getOrgCode() ).append("' ") ;
			}
		}
		
		hql.append(" and org.isUsable = '0' ") ;
		
		hql.append(" order by org.orgOrder asc ") ;
		
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有群组数量
	 * @param organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	public int getOrganizatinosCount( Organizatinos organizatinos ) throws BaseException {
		
		int gValue = 0 ;
		StringBuffer hql = new StringBuffer(" select count(*) from Organizatinos org where 1 = 1 " );

		if( organizatinos != null ) {
			// 父类ID
			if( organizatinos.getParentOrgId() != null ) {
				hql.append(" and org.parentOrgId = " ).append( organizatinos.getParentOrgId() ) ;
			}
			// 群组编码
			if( organizatinos.getOrgCode() != null && organizatinos.getOrgCode().length()>0 ) {
				hql.append(" and org.orgCode = '" ).append( organizatinos.getOrgCode() ).append("' ") ;
			}
		}
		
		hql.append(" and org.isUsable = '0' ") ;
		
		return this.countObjects( hql.toString() ) ;
	}
	
	/**
	 * 获得所有群组数据集
	 * @return
	 * @throws BaseException 
	 */
	public List<Organizatinos> getTreeOrganizatinosList() throws BaseException {
		List<Organizatinos> list=	this.getObjects(Organizatinos.class);
		
		return list;    
	}
	
	/**
	 * 获得所有群组数据集
	 * @param rollPage 分页对象
	 * @param Organizatinos 群组对象
	 * @return
	 * @throws BaseException 
	 */
	public List<Users> getUsersList( RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users  where 1 = 1 " );
			return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得根据群组编号数据集
	 * @return
	 * @throws BaseException 
	 */
	public List<Organizatinos> getOrganizatinosCodeList(Organizatinos organizatinos ) throws BaseException {
		StringBuffer hql = new StringBuffer("from Organizatinos where orgCode='"+organizatinos.getOrgCode().trim()+"'");
		List<Organizatinos> list=	this.getObjects(hql.toString());
		
		return list;
	}
	
	/**
	 * 获得上级群组编号数据集，如果有下级群组，上级群组不允许删除
	 * @return
	 * @throws BaseException 
	 */
	public List<Organizatinos> getOrganizatinosupDepIdList(Organizatinos organizatinos ) throws BaseException {
		StringBuffer hql = new StringBuffer("from Organizatinos where updepId='"+organizatinos.getOrgCode()+"'");
		List<Organizatinos> list=	this.getObjects(hql.toString());
		
		return list;
	}
	/**
	 * 获得所有群组人员视图
	 * @return
	 * @throws BaseException 
	 */
	public List getAllOrgRoot() throws BaseException {
		StringBuffer hql=new StringBuffer(" from Organizatinos d where d.orgLevel = 1 ");
		return this.getObjects(hql.toString());
	}
}
