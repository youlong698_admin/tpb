package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IOrgRightsBiz;
import com.ced.sip.system.entity.OrgRights;

public class OrgRightsBizImpl extends BaseBizImpl implements IOrgRightsBiz {
	/**
	 * 根据主键获得群组权限表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public OrgRights getOrgRights(Long id) throws BaseException {
		return (OrgRights)this.getObject(OrgRights.class, id);
		
	}

	/**
	 * 查询群组权限表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public int getOrgRightsInt(OrgRights orgRights) throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from OrgRights de where 1 = 1 " );
		if(StringUtil.isNotBlank(orgRights)){
			hql.append(" and de.orgId = '").append(orgRights.getOrgId()).append("'");
			hql.append(" and de.depId = '").append(orgRights.getDepId()).append("'");
		}
		
		List s=this.getObjects( hql.toString() );
		return s.size();
	}

	/**
	 * 添加群组权限信息
	 * @author xcb 2016-10-13
	 * @param user 群组权限表实例
	 * @throws BaseException 
	 */
	public void saveOrgRights(OrgRights orgRights) throws BaseException {
		this.saveObject(orgRights);
		
	}
	/**
	 * 修改群组权限信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组权限表实例
	 * @throws BaseException 
	 */
	public void updateOrgRights(OrgRights orgRights) throws BaseException {
		this.updateObject(orgRights);
		
	}
	/**
	 * 删除群组权限表实例
	 * @author xcb 2016-10-13
	 * @param id 群组id
	 * @throws BaseException 
	 */
	public void deleteOrgRights(String id) throws BaseException {
		
		StringBuffer hql = new StringBuffer(" from OrgRights de where 1 = 1");
		
		if(StringUtil.isNotBlank(id)){
			hql.append(" and de.orgId = '").append(id).append("'");
		}
		
		hql.append(" order by de.orgRightId desc ") ;
		List<OrgRights> orgList=this.getObjects(hql.toString() );
		
		for(OrgRights og:orgList)
		{
			this.removeObject( og ) ;
		}
		
		
	}
	/**
	 * 删除群组权限表实例
	 * @author xcb 2016-10-13
	 * @param orguser 群组权限表实例
	 * @throws BaseException 
	 */
	public void deleteOrgRights(OrgRights orgRights) throws BaseException {
		this.removeObject( orgRights ) ;
		
	}

	/**
	 * 删除群组权限表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteOrgRights(String[] id) throws BaseException {
		this.removeBatchObject(OrgRights.class, id) ;
		
	}
	
	/**
	 * 获得所有群组权限表数据集
	 * @author xcb 2016-10-13
	 * @param orguser 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List<OrgRights> getOrgRightsList(OrgRights orgRights) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrgRights de where 1 = 1");
		if(StringUtil.isNotBlank(orgRights)){
			if(StringUtil.isNotBlank(orgRights.getOrgId())){
				hql.append(" and de.orgId = '").append(orgRights.getOrgId()).append("'");
			}
			if(StringUtil.isNotBlank(orgRights.getDepId())){
				hql.append(" and de.depId = '").append(orgRights.getDepId()).append("'");
			}
		}
		
		hql.append(" order by de.orgRightId desc ") ;
		return this.getObjects(hql.toString() );
	}
	
	public List<OrgRights> getOrgRightsList( RollPage rollPage,OrgRights orgRights) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrgRights de where 1 = 1");
		if(StringUtil.isNotBlank(orgRights)){
			
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}


	
}
