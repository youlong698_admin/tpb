package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.entity.MaterialKind;
import com.ced.sip.system.entity.MaterialList;

public class MaterialBizImpl extends BaseBizImpl implements IMaterialBiz{
	/**
	 * 根据主键获得采购类别表实例
	 * @param id 主键
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public MaterialKind getMaterialKind(Long id) throws BaseException {
		return (MaterialKind)this.getObject(MaterialKind.class, id);
	}
	
	/**
	 * 获得采购类别表实例
	 * @param materialKind 采购类别表实例
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public MaterialKind getMaterialKind( MaterialKind materialKind ) throws BaseException {
		return (MaterialKind)this.getObject(MaterialKind.class, materialKind.getMkId() );
	}
	
	/**
	 * 添加采购类别信息
	 * @param materialKind 采购类别表实例
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void saveMaterialKind(MaterialKind materialKind) throws BaseException{
		this.saveObject( materialKind ) ;
	}
	
	/**
	 * 更新采购类别表实例
	 * @param materialKind 采购类别表实例
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void updateMaterialKind(MaterialKind materialKind) throws BaseException{
		this.updateObject( materialKind ) ;
	}
	
	/**
	 * 删除采购类别表实例
	 * @param id 主键数组
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void deleteMaterialKind(String id) throws BaseException {
		this.removeObject( this.getMaterialKind( new Long(id) ) ) ;
	}
	
	/**
	 * 删除采购类别表实例
	 * @param materialKind 采购类别表实例
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void deleteMaterialKind(MaterialKind materialKind) throws BaseException {
		this.removeObject( materialKind ) ;
	}
	
	/**
	 * 删除采购类别表实例
	 * @param id 主键数组
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void deleteMaterialKinds(String[] id) throws BaseException {
		this.removeBatchObject(MaterialKind.class, id) ;
	}
	
	/**
	 * 获得所有采购类别表数据集
	 * @param materialKind 查询参数对象
	 * @author luguanglei 2015-01-23
	 * @return
	 * @throws BaseException 
	 */
	public List<MaterialKind> getMaterialKindWithTreeList(  MaterialKind materialKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialKind de where 1 = 1 " );
		if(materialKind != null){
			if(!StringUtil.isBlank(materialKind.getParentId())){
				hql.append(" and de.parentId = ").append(materialKind.getParentId());
			}
			
			if (StringUtil.isNotBlank(materialKind.getMkId())) {
				hql.append(" and de.mkId = '").append(materialKind.getMkId()).append("'");
			}
			if (StringUtil.isNotBlank(materialKind.getComId())) {
				hql.append(" and de.comId = ").append(materialKind.getComId()).append("");
			}
		}
		hql.append(" order by de.displaySort ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有采购类别表数据集
	 * @param materialKind 查询参数对象
	 * @author luguanglei 2015-01-23
	 * @return
	 * @throws BaseException 
	 */
	public List getAllMaterialListForTree(Long comId) throws BaseException {
		StringBuffer hql = new StringBuffer("SELECT * FROM material_kind where com_id="+comId+" START WITH parent_id = 0 CONNECT BY PRIOR mk_id = parent_id ORDER SIBLINGS BY display_sort ");
		return this.getObjectsList(MaterialKind.class, hql.toString());
	}
	/**
	 * 获得所有采购类别表数据集
	 * @param materialKind 查询参数对象
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialKindList(  MaterialKind materialKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialKind de where 1 = 1 " );
		if(materialKind != null){
			if(!StringUtil.isBlank(materialKind.getParentId())){
				hql.append(" and de.parentId = ").append(materialKind.getParentId());
			}
			
			if (StringUtil.isNotBlank(materialKind.getMkId())) {
				hql.append(" and de.mkId = '").append(materialKind.getMkId()).append("'");
			}
			if (StringUtil.isNotBlank(materialKind.getComId())) {
				hql.append(" and de.comId = ").append(materialKind.getComId()).append("");
			}
		}
		hql.append(" order by de.displaySort ");
		return this.getObjects( hql.toString() );
	}
	/**
	 *  获得所有采购类别表数据集
	 * materialKind 查询参数对象
	 * @return 2016-8-10
	 * @throws BaseException 
	 */
	public MaterialKind getMaterialKindByMaterialKind(MaterialKind materialKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialKind de where 1 = 1 " );
		if(materialKind != null){
			if(!StringUtil.isBlank(materialKind.getParentId())){
				hql.append(" and de.parentId = ").append(materialKind.getParentId());
			}
			if (StringUtil.isNotBlank(materialKind.getMkId())) {
				hql.append(" and de.mkId = '").append(materialKind.getMkId()).append("'");
			}
			if (StringUtil.isNotBlank(materialKind.getMkName())) {
				hql.append(" and de.mkName = '").append(materialKind.getMkName()).append("'");
			}
			if (StringUtil.isNotBlank(materialKind.getComId())) {
				hql.append(" and de.comId = ").append(materialKind.getComId()).append("");
			}
		}
		List list = this.getObjects( hql.toString() );
		materialKind = new MaterialKind();
		if(list!=null&&list.size()>0){
			materialKind = (MaterialKind)list.get(0);
		}
		return materialKind;
	}
	/**
	 * 获得采购价格查询三级联动集
	 * @param materialKind 查询参数对象
	 * @author zhangzhaoning 2016-10-17
	 * @return
	 * @throws BaseException 
	 */
	public List getMatList(  MaterialKind materialKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialKind de  where 1=1" );
		hql.append(" and de.parentId =").append(materialKind.getMkId());
		hql.append(" order by de.id");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有采购类别表数据集
	 * @param rollPage 分页对象
	 * @param materialKind 查询参数对象
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialKindList( RollPage rollPage, MaterialKind materialKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialKind de where 1 = 1 " );
		if(StringUtil.isNotBlank(materialKind)){
			if(StringUtil.isNotBlank(materialKind.getLevels())){
				hql.append(" and de.levels ='").append(materialKind.getLevels()).append("'");
			}
			if(StringUtil.isNotBlank(materialKind.getMkCode())){
				hql.append(" and de.mkCode like '%").append(materialKind.getMkCode()).append("%'");
			}
			if(StringUtil.isNotBlank(materialKind.getMkName())){
				hql.append(" and de.mkName like '%").append(materialKind.getMkName()).append("%'");
			}
			if (StringUtil.isNotBlank(materialKind.getComId())) {
				hql.append(" and de.comId = ").append(materialKind.getComId()).append("");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 根据主键获得物料信息表实例
	 * @param id 主键
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public MaterialList getMaterialList(Long id) throws BaseException {
		return (MaterialList)this.getObject(MaterialList.class, id);
	}
	
	/**
	 * 获得物料信息表实例
	 * @param materialList 物料信息表实例
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public MaterialList getMaterialList( MaterialList materialList ) throws BaseException {
		return (MaterialList)this.getObject(MaterialList.class, materialList.getMaterialId() );
	}
	
	/**
	 * 添加物料信息信息
	 * @param materialList 物料信息表实例
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void saveMaterialList(MaterialList materialList) throws BaseException{
		this.saveObject( materialList ) ;
	}
	
	/**
	 * 更新物料信息表实例
	 * @param materialList 物料信息表实例
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void updateMaterialList(MaterialList materialList) throws BaseException{
		this.updateObject( materialList ) ;
	}
	
	/**
	 * 删除物料信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void deleteMaterialList(String id) throws BaseException {
		this.removeObject( this.getMaterialList( new Long(id) ) ) ;
	}
	
	/**
	 * 删除物料信息表实例
	 * @param materialList 物料信息表实例
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void deleteMaterialList(MaterialList materialList) throws BaseException {
		this.removeObject( materialList ) ;
	}
	
	/**
	 * 删除物料信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2016-08-27
	 * @throws BaseException 
	 */
	public void deleteMaterialLists(String[] id) throws BaseException {
		this.removeBatchObject(MaterialList.class, id) ;
	}
	
	/**
	 * 获得所有物料信息表数据集
	 * @param materialList 查询参数对象
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialListList(  MaterialList materialList ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialList de where 1 = 1 " );
		if(materialList != null){
			if(!StringUtil.isBlank(materialList.getMaterialCode())){
				hql.append(" and de.materialCode like '%").append(materialList.getMaterialCode()).append("%'");
			}
			if(!StringUtil.isBlank(materialList.getMkKindId())){
				hql.append(" and de.mkKindId =").append(materialList.getMkKindId());
			}
			if(StringUtil.isNotBlank(materialList.getMaterialName())){
				hql.append(" and de.materialName like'%").append(materialList.getMaterialName()).append("%'");
			}
			if(StringUtil.isNotBlank(materialList.getMaterialId())){
				hql.append(" and de.materialId like'%").append(materialList.getMaterialId()).append("%'");
			}
			if (StringUtil.isNotBlank(materialList.getComId())) {
				hql.append(" and de.comId = ").append(materialList.getComId()).append("");
			}
		}
		hql.append(" and de.isUsable ='0'");
		hql.append(" order by de.materialCode "); //Ushine,2016-11-15
		return this.getObjects(hql.toString() );
	}
	
	/**
	 * 获得所有物料信息表数据集
	 * @param rollPage 分页对象
	 * @param materialList 查询参数对象
	 * @author luguanglei 2016-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialListList( RollPage rollPage, MaterialList materialList ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialList de where 1 = 1 " );
		if(materialList != null){
			if(!StringUtil.isBlank(materialList.getMaterialCode())){
				hql.append(" and de.materialCode like '%").append(materialList.getMaterialCode()).append("%'");
			}
			if(!StringUtil.isBlank(materialList.getMkKindId())){
				hql.append(" and de.mkKindId =").append(materialList.getMkKindId());
			}
			if(StringUtil.isNotBlank(materialList.getMaterialName())){
				hql.append(" and (de.materialName like'%").append(materialList.getMaterialName()).append("%' or de.mnemonicCode like '%").append(materialList.getMaterialName().toUpperCase()).append("%')");
			}
			if(StringUtil.isNotBlank(materialList.getMaterialId())){
				hql.append(" and de.materialId like'%").append(materialList.getMaterialId()).append("%'");
			}
			if(StringUtil.isNotBlank(materialList.getComId())){
				hql.append(" and de.comId = ").append(materialList.getComId());
			}
		}
		hql.append(" and de.isUsable ='0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}

	public MaterialList getMaterialListByCode(String materialCode) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialList de where 1 = 1 " );
		hql.append(" and de.materialCode ='").append(materialCode).append("'");
		
		List list = this.getObjects(hql.toString());
		if(list.size()>0){
			return (MaterialList) list.get(0);
		}else{
			return new MaterialList();
		}
		
	}
	/**
	 * 获得指定物料信息表数据集
	 * @param materialKind 查询参数对象
	 * @author xuchengbin 2016-10-22
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialCode(String materialType,String mkCodeLength) throws BaseException {
		StringBuffer hql = new StringBuffer(" select max(de.mkCode) from MaterialKind de where 1 = 1 " );
			hql.append(" and de.mkCode like '%").append(materialType).append("%'");
			hql.append(" and length(de.mkCode) = ").append(mkCodeLength);
			return this.getObjects(hql.toString() );
		}
	/**
	 * 获得指定物料信息表数据集
	 * @param materialList 查询参数对象
	 * @author xuchengbin 2016-10-22
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialList(String materialType,String mkCodeLength) throws BaseException {
		StringBuffer hql = new StringBuffer(" select max(de.materialCode) from MaterialList de where 1 = 1 " );
			hql.append(" and de.materialCode like '%").append(materialType).append("%'");
			hql.append(" and length(de.materialCode) = ").append(mkCodeLength);
			return this.getObjects(hql.toString() );
		}

	
	/**
	 * 获取物料采购类别中文名
	 * @author Ushine,2016-11-17
	 */
	public List getMkName(String mkCode) throws BaseException {
		StringBuffer hql = new StringBuffer(" select mk.mkName from MaterialKind mk where 1 = 1 ");
			if(StringUtil.isNotBlank(mkCode)){
				hql.append(" and mk.mkCode = '").append(mkCode).append("'");
			}
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得指定物料信息表数据集
	 * @param mkCodeLength 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getMaterialCodeChart(String mkCodeLength) throws BaseException {
		StringBuffer hql = new StringBuffer(" from MaterialKind de where 1 = 1 " );
			hql.append(" and length(de.mkCode) = ").append(mkCodeLength);
			return this.getObjects(hql.toString() );
		}	
}
