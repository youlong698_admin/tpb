package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IOrgUserBiz;
import com.ced.sip.system.entity.OrgUser;

public class OrgUserBizImpl extends BaseBizImpl implements IOrgUserBiz {
	/**
	 * 根据主键获得群组用户表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public OrgUser getOrguser(Long id) throws BaseException {
		return (OrgUser)this.getObject(OrgUser.class, id);
		
	}
	
	/**
	 * 添加群组用户信息
	 * @author xcb 2016-10-13
	 * @param user 群组用户表实例
	 * @throws BaseException 
	 */
	public void saveOrguser(OrgUser orguser) throws BaseException {
		this.saveObject(orguser);
		
	}
	/**
	 * 修改群组用户信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组用户表实例
	 * @throws BaseException 
	 */
	public void updateOrguser(OrgUser orguser) throws BaseException {
		this.updateObject(orguser);
		
	}
	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteOrguser(String id) throws BaseException {
		this.removeObject( this.getOrguser( new Long(id) ) ) ;
		
	}
	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param orguser 群组用户表实例
	 * @throws BaseException 
	 */
	public void deleteOrguser(OrgUser orguser) throws BaseException {
		this.removeObject( orguser ) ;
		
	}

	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteOrgusers(String[] id) throws BaseException {
		this.removeBatchObject(OrgUser.class, id) ;
		
	}
	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteOrgusers(List list) throws BaseException {
		if(list!=null && list.size()!=0){
			for(int i=0;i<list.size();i++){
				deleteOrguser((OrgUser)list.get(i));
			}
		}
		
	}
	/**
	 * 获得所有群组用户表数据集
	 * @author xcb 2016-10-13
	 * @param orguser 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List<OrgUser> getOrgusersList(OrgUser orguser) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrgUser de where 1 = 1");
		if(StringUtil.isNotBlank(orguser)){
			if(StringUtil.isNotBlank(orguser.getOrgId())){
				hql.append(" and de.orgId = '").append(orguser.getOrgId()).append("'");
			}
			if(StringUtil.isNotBlank(orguser.getUserName())){
				hql.append(" and de.userName like '%").append(orguser.getUserName()).append("%'");
			}
			if(StringUtil.isNotBlank(orguser.getDepId())){
				hql.append(" and de.depId = '").append(orguser.getDepId()).append("'");
			}
			if(StringUtil.isNotBlank(orguser.getOrgCode())){
				hql.append(" and de.orgCode = '").append(orguser.getOrgCode()).append("'");
			}
		}
		hql.append(" and de.isUsable = '0' ") ;
		hql.append(" order by de.orgId desc ") ;
		return this.getObjects(hql.toString() );
	}
	
	public List<OrgUser> getOrgusersList( RollPage rollPage,OrgUser orguser) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrgUser de where 1 = 1");
		if(StringUtil.isNotBlank(orguser)){
			if(StringUtil.isNotBlank(orguser.getOrgId())){
				hql.append(" and de.orgId = '").append(orguser.getOrgId()).append("'");
			}
			if(StringUtil.isNotBlank(orguser.getUserNameCn())){
				hql.append(" and (de.userName like '%").append(orguser.getUserNameCn()).append("%'").append(" or de.userNameCn like '%").append(orguser.getUserNameCn()).append("%')");;
			}
//			if(StringUtil.isNotBlank(orguser.getUserNameCn())){
//				hql.append(" and de.userNameCn like '%").append(orguser.getUserNameCn()).append("%'");
//			}
			if(StringUtil.isNotBlank(orguser.getDepId())){
				hql.append(" and de.depId = '").append(orguser.getDepId()).append("'");
			}
			if(StringUtil.isNotBlank(orguser.getOrgCode())){
				hql.append(" and de.orgCode = '").append(orguser.getOrgCode()).append("'");
			}
			if (StringUtil.isNotBlank(orguser.getComId())) {
				hql.append(" and de.comId = ").append(orguser.getComId()).append("");
			}
		}
		hql.append(" and de.isUsable = '0' ") ;
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	
}
