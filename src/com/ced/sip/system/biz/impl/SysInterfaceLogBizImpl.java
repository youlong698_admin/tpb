package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysInterfaceLogBiz;
import com.ced.sip.system.entity.SysInterfaceLog;

public class SysInterfaceLogBizImpl extends BaseBizImpl implements ISysInterfaceLogBiz  {
	
	/**
	 * 根据主键获得用户接口日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public SysInterfaceLog getSysInterfaceLog(Long id) throws BaseException {
		return (SysInterfaceLog)this.getObject(SysInterfaceLog.class, id);
	}

	/**
	 * 添加角色信息
	 * @param tSysInterfaceLog 用户接口日志实例
	 * @throws BaseException 
	 */
	public void saveSysInterfaceLog(SysInterfaceLog SysInterfaceLog) throws BaseException{
		this.saveObject(SysInterfaceLog ) ;
	}
	
	/**
	 * 更新用户接口日志实例
	 * @param tSysInterfaceLog 用户接口日志实例
	 * @throws BaseException 
	 */
	public void updateSysInterfaceLog(SysInterfaceLog SysInterfaceLog) throws BaseException{
		this.updateObject(SysInterfaceLog ) ;
	}
	
	/**
	 * 删除用户接口日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysInterfaceLog(String id) throws BaseException {
		this.removeObject( this.getSysInterfaceLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除用户接口日志实例
	 * @param tSysInterfaceLog 用户接口日志实例
	 * @throws BaseException 
	 */
	public void deleteSysInterfaceLog(SysInterfaceLog SysInterfaceLog) throws BaseException {
		this.removeObject(SysInterfaceLog ) ;
	}
	
	/**
	 * 删除用户接口日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysInterfaceLog(String[] id) throws BaseException {
		this.removeBatchObject(SysInterfaceLog.class, id) ;
	}	
	
	/**
	 * 获得所有用户接口日志数据集
	 * @param tSysInterfaceLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getSysInterfaceLogList(SysInterfaceLog SysInterfaceLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from SysInterfaceLog de  where 1=1 " );
		if(SysInterfaceLog != null){
			
		}
		hql.append(" order by de.silId desc");
		
		return this.getObjects( hql.toString() );
	}


	
	public List getSysInterfaceLogList(RollPage rollPage,
			SysInterfaceLog SysInterfaceLog,String writeDateStare,String writeDateEnd) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from SysInterfaceLog de  where 1=1 " );
		
		if(StringUtil.isNotBlank(SysInterfaceLog))
		{
			if(StringUtil.isNotBlank(SysInterfaceLog.getSystemName()))
			{
				hql.append(" and de.systemName like '%").append(SysInterfaceLog.getSystemName()).append("%'");
			}
			if(StringUtil.isNotBlank(SysInterfaceLog.getInterfaceName()))
			{
				hql.append(" and de.interfaceName like '%").append(SysInterfaceLog.getInterfaceName()).append("%'");
			}
			if(StringUtil.isNotBlank(SysInterfaceLog.getWriter()))
			{
				hql.append(" and de.writer ='").append(SysInterfaceLog.getWriter()).append("'");
			}
			if(StringUtil.isNotBlank(writeDateStare)){
				hql.append(" and trunc(de.writeDate,'dd')>= to_date('").append(writeDateStare).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(writeDateEnd)){
				hql.append(" and trunc(de.writeDate,'dd')<= to_date('").append(writeDateEnd).append("','yyyy-MM-dd')");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	
	


	
	
}
