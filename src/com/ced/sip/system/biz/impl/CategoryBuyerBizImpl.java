package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ICategoryBuyerBiz;
import com.ced.sip.system.entity.CategoryBuyer;
/** 
 * 类名称：CategoryBuyerBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-03-19
 */
public class CategoryBuyerBizImpl extends BaseBizImpl implements ICategoryBuyerBiz  {
	
	/**
	 * 根据主键获得表实例
	 * @param id 主键
	 * @author luguanglei 2017-03-19
	 * @return
	 * @throws BaseException 
	 */
	public CategoryBuyer getCategoryBuyer(Long id) throws BaseException {
		return (CategoryBuyer)this.getObject(CategoryBuyer.class, id);
	}
	
	/**
	 * 获得表实例
	 * @param categoryBuyer 表实例
	 * @author luguanglei 2017-03-19
	 * @return
	 * @throws BaseException 
	 */
	public CategoryBuyer getCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException {
		return (CategoryBuyer)this.getObject(CategoryBuyer.class, categoryBuyer.getCbId() );
	}
	
	/**
	 * 添加信息
	 * @param categoryBuyer 表实例
	 * @author luguanglei 2017-03-19
	 * @throws BaseException 
	 */
	public void saveCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException{
		this.saveObject( categoryBuyer ) ;
	}
	
	/**
	 * 更新表实例
	 * @param categoryBuyer 表实例
	 * @author luguanglei 2017-03-19
	 * @throws BaseException 
	 */
	public void updateCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException{
		this.updateObject( categoryBuyer ) ;
	}
	
	/**
	 * 删除表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-03-19
	 * @throws BaseException 
	 */
	public void deleteCategoryBuyer(String id) throws BaseException {
		this.removeObject( this.getCategoryBuyer( new Long(id) ) ) ;
	}
	
	/**
	 * 删除表实例
	 * @param categoryBuyer 表实例
	 * @author luguanglei 2017-03-19
	 * @throws BaseException 
	 */
	public void deleteCategoryBuyer(CategoryBuyer categoryBuyer) throws BaseException {
		this.removeObject( categoryBuyer ) ;
	}
	
	/**
	 * 删除表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-03-19
	 * @throws BaseException 
	 */
	public void deleteCategoryBuyers(String[] id) throws BaseException {
		this.removeBatchObject(CategoryBuyer.class, id) ;
	}
	
	/**
	 * 获得所有表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-03-19
	 * @return
	 * @throws BaseException 
	 */
	public List getCategoryBuyerList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CategoryBuyer de where 1 = 1 " );

		hql.append(" order by de.cbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有表数据集
	 * @param categoryBuyer 查询参数对象
	 * @author luguanglei 2017-03-19
	 * @return
	 * @throws BaseException 
	 */
	public List getCategoryBuyerList(CategoryBuyer categoryBuyer) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CategoryBuyer de where 1 = 1 " );
		if(StringUtil.isNotBlank(categoryBuyer) ){
			if(StringUtil.isNotBlank(categoryBuyer.getBuyerId())){
				hql.append(" and buyerId = ").append(categoryBuyer.getBuyerId());
			}
		}
		hql.append(" order by de.cbId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有表数据集
	 * @param rollPage 分页对象
	 * @param categoryBuyer 查询参数对象
	 * @author luguanglei 2017-03-19
	 * @return
	 * @throws BaseException 
	 */
	public List getCategoryBuyerList(RollPage rollPage, CategoryBuyer categoryBuyer) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CategoryBuyer de where 1 = 1 " );

		hql.append(" order by de.cbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 依据用户id删除表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteCategoryBuyerByBuyerId(Long buyerId)
			throws BaseException {
		String sql="delete from category_buyer where buyer_id="+buyerId;
		this.updateJdbcSql(sql);		
	}

	/**
	 * 批量保存采购员所管理的采购类别
	 * @param materialKindIds
	 * @param userId
	 * @param comId
	 * @throws BaseException
	 */
	public void saveCategoryBuyerByMaterialKindIds(String materialKindIds,
			Long userId, Long comId) throws BaseException {
		StringBuffer sql=new StringBuffer("begin ");
		String[] str=materialKindIds.split(",");
		for(int i=0;i<str.length;i++){
			sql.append(" insert into category_buyer(cb_id,mk_three_id,buyer_id,is_usable,mk_three_code,mk_three_name,com_id) select seq_category_buyer_id.nextval,"+str[i]+","+userId+",0,mk_code,mk_name,"+comId+" from material_kind where mk_id="+str[i]+";");
		}
		sql.append("end;");
		log.info(sql.toString());
		this.updateJdbcSql(sql.toString());
	}
	
}
