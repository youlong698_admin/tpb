package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysUserRoleRelaBiz;
import com.ced.sip.system.entity.SysUserRoleRela;

public class SysUserRoleRelaBizImpl extends BaseBizImpl implements ISysUserRoleRelaBiz {

	public SysUserRoleRela getSysUserRoleRela(Long id) throws BaseException {
		// TODO Auto-generated method stub
		return (SysUserRoleRela)this.getObject(SysUserRoleRela.class, id);
	}

	public void saveSysUserRoleRela(SysUserRoleRela sysUserRoleRela)
			throws BaseException {
		// TODO Auto-generated method stub
		this.saveObject(sysUserRoleRela);
	}

	public void updateSysUserRoleRela(SysUserRoleRela sysUserRoleRela)
			throws BaseException {
		// TODO Auto-generated method stub
		this.updateObject(sysUserRoleRela) ;
	}

	public void deleteSysUserRoleRela(String id) throws BaseException {
		// TODO Auto-generated method stub
		this.removeObject( this.getSysUserRoleRela(new Long(id)) ) ;
	}

	public void deleteSysUserRoleRela(SysUserRoleRela sysUserRoleRela)
			throws BaseException {
		// TODO Auto-generated method stub
		this.removeObject( sysUserRoleRela ) ;
	}

	public void deleteSysUserRoleRela(String[] id) throws BaseException {
		// TODO Auto-generated method stub
		this.removeBatchObject(SysUserRoleRela.class, id) ;
	}

	public List getSysUserRoleRelaList(RollPage rollPage) throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from SysUserRoleRela de where 1 = 1 " );
		
		return this.getObjects( rollPage,hql.toString() );
	}

	public List getSysUserRoleRelaList(SysUserRoleRela sysUserRoleRela)
			throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from SysUserRoleRela de where 1 = 1 " );
		if(sysUserRoleRela != null){
			if(StringUtil.isNotBlank(sysUserRoleRela.getUserId())){
				hql.append(" and de.userId='"+sysUserRoleRela.getUserId()+"'");
			}
			
			if(StringUtil.isNotBlank(sysUserRoleRela.getRoleId())){
				hql.append(" and de.roleId='"+sysUserRoleRela.getRoleId()+"'");
			}
		}
		return this.getObjects(hql.toString() );
	}

	public List getSysUserRoleRelaList(RollPage rollPage,
			SysUserRoleRela sysUserRoleRela) throws BaseException {
		// TODO Auto-generated method stub
		return null;
	}

}
