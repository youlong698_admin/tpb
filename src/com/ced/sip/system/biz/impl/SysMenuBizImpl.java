package com.ced.sip.system.biz.impl;

import java.util.ArrayList;
import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysMenuBiz;
import com.ced.sip.system.entity.SysMenu;

public class SysMenuBizImpl extends BaseBizImpl implements ISysMenuBiz {

	public SysMenu getSysMenu(Long id) throws BaseException {
		return (SysMenu)this.getObject(SysMenu.class, id);
	}

	public SysMenu getSysMenuByName(String menuName) throws BaseException {
		return null;
	}

	public void saveSysMenu(SysMenu sysMenu) throws BaseException {
			this.saveObject(sysMenu);
	}

	public void updateSysMenu(SysMenu sysMenu) throws BaseException {
		this.updateObject(sysMenu) ;
	}

	public void deleteSysMenu(String id) throws BaseException {
		
		this.removeObject( this.getSysMenu(new Long(id)) ) ;
	}

	public void deleteSysMenu(SysMenu sysMenu) throws BaseException {
		
		this.removeObject( sysMenu ) ;
	}

	public void deleteSysMenu(String[] id) throws BaseException {
		
		this.removeBatchObject(SysMenu.class, id) ;
	}

	public int getSysMenuCount(SysMenu sysMenu) throws BaseException {
		
		return 0;
	}

	public List<SysMenu> getSysMenuList(RollPage rollPage) throws BaseException {
		
		StringBuffer hql = new StringBuffer(" from SysMenu de where 1 = 1 " );
		
		return this.getObjects( rollPage,hql.toString() );
	}
	
	public SysMenu getSysMenuById(String id) throws BaseException {
		
		StringBuffer hql = new StringBuffer(" from SysMenu de where 1 = 1 and menuCode="+id );
		SysMenu menu = null;
		List lst = this.getObjects(hql.toString() );
		if(lst != null && lst.size() > 0){
			menu = (SysMenu) lst.get(0);
		}
		return menu;
	}

	public List<SysMenu> getSysMenuList(SysMenu sysMenu) throws BaseException {
		
		StringBuffer hql = new StringBuffer(" from SysMenu de where 1 = 1 " );
		if(sysMenu != null){
			if(StringUtil.isNotBlank(sysMenu.getIsActive())){
				hql.append(" and de.isActive='"+sysMenu.getIsActive()+"'");
			}
			if(StringUtil.isNotBlank(sysMenu.getPmenuCode())){
				hql.append(" and de.pmenuCode='"+sysMenu.getPmenuCode()+"'");
			}
		}
		hql.append(" ORDER BY to_number(de.pmenuCode) ,to_number(de.sortNo) ");
		return this.getObjects(hql.toString() );
	}

	public List<SysMenu> getSysMenuList(RollPage rollPage, SysMenu sysMenu)
			throws BaseException {
		
		return null;
	}

	public List<SysMenu> getTreeSysMenuListByUserId(String userId) throws BaseException {
		List<SysMenu> list=new ArrayList<SysMenu>();
         if(!userId.equals("")){
			StringBuffer hql = new StringBuffer("FROM SysMenu sm WHERE sm.menuId IN " +
					"(SELECT srmr.menuId FROM SysRoleMenuRela srmr,SysUserRoleRela surr WHERE surr.roleId=srmr.roleId and surr.userId =" +userId+") " +
					" ORDER BY to_number(sm.pmenuCode) ,to_number(sm.sortNo)");
			list=this.getObjects(hql.toString() );
		}
		
		return list;
	}
	public void deleteSysMenu(Long id) throws BaseException {
		
		
	}
	public List<SysMenu> getFuSysMenuList(String pMenuId) throws BaseException {
		
		StringBuffer hql = new StringBuffer("FROM SysMenu sm WHERE sm.pmenuCode ='"+pMenuId+"'" +
		"ORDER BY to_number(sm.sortNo)");
		return this.getObjects(hql.toString() );
	}
	public List<SysMenu> getTreeSysMenuListForSupplier() throws BaseException {
		 List<SysMenu> list=new ArrayList<SysMenu>();
        StringBuffer hql = new StringBuffer("FROM SysMenu sm WHERE sm.type=2 ORDER BY to_number(sm.pmenuCode) ,to_number(sm.sortNo)");
		 list=this.getObjects(hql.toString() );
		
		return list;
	}


}
