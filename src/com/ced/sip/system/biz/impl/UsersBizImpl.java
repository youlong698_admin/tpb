package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.OrgUser;
import com.ced.sip.system.entity.Users;

public class UsersBizImpl extends BaseBizImpl implements IUsersBiz  {
	
	/**
	 * 根据主键获得用户表实例
	 * @param id 主键
	 * @return
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public Users getUsers(Long id) throws BaseException {
		return (Users)this.getObject(Users.class, id);
	}
	
	/**
	 * 添加用户信息
	 * @author zwp 2012-10-19
	 * @param user 用户表实例
	 * @throws BaseException 
	 */
	public void saveUsers(Users user) throws BaseException{
		this.saveObject( user ) ;
	}
	
	/**
	 * 更新用户表实例
	 * @author zwp 2012-10-19
	 * @param user 用户表实例
	 * @throws BaseException 
	 */
	public void updateUsers(Users user) throws BaseException{
		this.updateObject( user ) ;
	}
	
	/**
	 * 删除用户表实例
	 * @author zwp 2012-10-19
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteUsers(String id) throws BaseException {
		this.removeObject( this.getUsers( new Long(id) ) ) ;
	}
	
	/**
	 * 删除用户表实例
	 * @author zwp 2012-10-19
	 * @param user 用户表实例
	 * @throws BaseException 
	 */
	public void deleteUsers(Users user) throws BaseException {
		this.removeObject( user ) ;
	}
	
	/**
	 * 删除用户表实例
	 * @author zwp 2012-10-19
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteUserss(String[] id) throws BaseException {
		this.removeBatchObject(Users.class, id) ;
	}
	
	/**
	 * 删除用户表实例
	 * @author zwp 2012-10-19
	 * @param list 对象数组
	 * @throws BaseException 
	 */
	public void deleteUserss(List list) throws BaseException {
		Users users=null;
		if(list!=null && list.size()!=0){
			for(int i=0;i<list.size();i++){
				users=(Users)list.get(i);	
				deleteUsers(users);
			}
		}
	}
	
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public List getUsersList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users de where 1 = 1 " );
		hql.append(" and isUsable = '0' ");
		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有用户表数据集
	 * @param user 查询参数对象
	 * @return
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public List getUsersList(  Users user ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users usr where 1 = 1 " );
		if(StringUtil.isNotBlank(user) ){
			if(StringUtil.isNotBlank(user.getDepId())){
				hql.append(" and usr.depId ='").append( user.getDepId() ).append("'");
			}
			if( ! StringUtil.isBlank( user.getUserName() )  ){
				hql.append(" and usr.userName ='").append( user.getUserName() ).append("'");
			}			
			if( ! StringUtil.isBlank( user.getUserPassword() )  ){
				hql.append(" and usr.userPassword ='").append( user.getUserPassword() ).append("'");
			}
			if( ! StringUtil.isBlank( user.getUserChinesename() )  ){
				hql.append(" and usr.userChinesename ='").append( user.getUserChinesename() ).append("'");
			}
			if(StringUtil.isNotBlank(user.getStation())){
				hql.append(" and usr.station like '%,").append(user.getStation()).append(",%'");
			}
			if(StringUtil.isNotBlank(user.getComId())){
				hql.append(" and usr.comId ='").append( user.getComId() ).append("'");
			}
		}
		hql.append(" and usr.isUsable = '0' ");
		hql.append(" order by usr.userId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @author
	 * @throws BaseException 
	 */
	public List getUsersList( RollPage rollPage, Users user ) throws BaseException {
		List list=null;
		StringBuffer hql = new StringBuffer("from Users de where 1=1 ");
	       if(!StringUtil.isBlank(user)){
				if(!StringUtil.isBlank(user.getDepId())){
					hql.append(" and de.depId = ").append(user.getDepId());
				}
				if(!StringUtil.isBlank(user.getUserChinesename())){
					hql.append(" and (de.userChinesename like '%").append(user.getUserChinesename().trim()).append("%' or de.userName like '%").append(user.getUserChinesename().trim()).append("%')");
                 }
				
				if(!StringUtil.isBlank(user.getRank())){
					hql.append(" and de.rank like '%").append(user.getRank()).append("%'");
				}
				if(!StringUtil.isBlank(user.getStation())){
					hql.append(" and de.station like '%").append(user.getStation()).append("%'");
				}
				if(!StringUtil.isBlank(user.getIsUsable())){
					hql.append(" and de.isUsable = '").append(user.getIsUsable()).append("'");
				}
				if (StringUtil.isNotBlank(user.getComId())) {
					hql.append(" and de.comId = ").append(user.getComId()).append("");
				}
            }
			hql.append(" order by de.userId asc ");
			list=this.getObjects(rollPage, hql.toString() );
	    
		return list;
	}
	
	
	
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @author 谢磊
	 * @throws BaseException 
	 */
	public List getUsersListRight( RollPage rollPage, Users user ) throws BaseException {
		List list=null;
		StringBuffer hql = new StringBuffer(" from Users de  where 1=1 and de.isUsable ='0' " );
			if(StringUtil.isNotBlank(user)){
				if(StringUtil.isNotBlank(user.getDepId())){
					hql.append(" and de.depId = ").append(user.getDepId());
				}
				if(StringUtil.isNotBlank(user.getUserName())){
					if(user.getUserName().equals("0")){
						hql.append( " and (de.userName is not null  or de.userName <>  '')  and de.userName <> 'admin' ");
					}else{
						
						hql.append(" and de.userName = ' ").append(user.getUserName()).append("'");
					}
				}
				if(!StringUtil.isBlank(user.getUserChinesename())){
					hql.append(" and (de.userChinesename like '%").append(user.getUserChinesename().trim()).append("%' or de.userName like '%").append(user.getUserChinesename().trim()).append("%')");
					}
				if(!StringUtil.isBlank(user.getRank())){
					hql.append(" and de.rank like '%").append(user.getRank()).append("%'");
				}
				if(!StringUtil.isBlank(user.getStation())){
					hql.append(" and de.station like '%").append(user.getStation()).append("%'");
				}
				if (StringUtil.isNotBlank(user.getComId())) {
					hql.append(" and de.comId = ").append(user.getComId()).append("");
				}
				
			}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		
		list=this.getObjects(rollPage, hql.toString() );
	    
		return list;
	}
	
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return departments 部门
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public List getUsersList(RollPage rollPage, Users user, Departments departments) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users as u where 1 = 1 " );
		hql.append(" and u.isUsable='").append(TableStatus.COMMON_STATUS_VALID).append("' ");
		  //查询
		if(StringUtil.isNotBlank(user)){
			if(StringUtil.isNotBlank(user.getUserChinesename())){
				hql.append(" and u.userChinesename like '%").append(user.getUserChinesename().trim()).append("%' ");
			}
		}
		  if(departments!=null){
			  if(departments.getDepId()!=null){
				  hql.append(" and u.depId=").append(departments.getDepId()).append(" ");
			  }
		  }
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有用户表数据集
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return departments 部门
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public List getUsersList( Users user, Departments departments) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users as u where 1 = 1 " );
		hql.append(" and u.isUsable='").append(TableStatus.COMMON_STATUS_VALID).append("' ");
		  //查询
		if(StringUtil.isNotBlank(user)){
			if(StringUtil.isNotBlank(user.getUserChinesename())){
				hql.append(" and u.userChinesename like '%").append(user.getUserChinesename().trim()).append("%' ");
				user.setUserChinesename(user.getUserChinesename());
			}
		}
		  if(departments!=null){
			  if(departments.getDepId()!=null){
				  hql.append(" and u.depId=").append(departments.getDepId()).append(" ");
			  }
		  }
		//System.out.println(hql);
		return this.getObjects( hql.toString() );
	}	
	/**
	 * 集合数量用户表实例
	 * @author zwp 2012-10-19
	 * @param users 用户
	 * @throws BaseException
	 */
	public int countUsers(Users users) throws BaseException{
		StringBuffer hql = new StringBuffer(" select count(*) from Users as u where 1=1");
		if(users!=null){
			if(users.getDepId()!=null){
			hql.append(" or u.selflevCode like '%,").append(users.getDepId()).append(",%') ");
			}
		}
//		hql.append(" and u.status='01' ");
		hql.append(" and u.isUsable='").append(TableStatus.COMMON_STATUS_VALID).append("' ");
		hql.append(" order by u.depId desc");
		//System.out.println(hql);
		return this.baseDao.count(hql.toString());
	    
	}
	
	/**
	 * 集合数量用户表实例
	 * @author 谢磊 2012-11-9
	 * @param users 用户
	 * @throws BaseException
	 */
	public int countDeptUsers(Users users) throws BaseException{
		StringBuffer hql = new StringBuffer(" select count(*) from Users u  where 1=1 ");
		if(StringUtil.isNotBlank(users)){
			if(users.getDepId()!=null){
		    hql.append("  and u.depId = '").append(users.getDepId()).append("'");		
			}
			if(StringUtil.isNotBlank(users.getUserName())){
				if(users.getUserName().equals("0")){
					hql.append( "and ( u.userName is not null or u.userName != '' )  and u.userName <> 'admin' ");
				}else{
					
					hql.append(" and u.userName = ' ").append(users.getUserName()).append("'");
				}
			}
			
		}
//		hql.append(" and u.status='01' ");
		hql.append(" and u.isUsable = 0");
		//System.out.println(hql);
		return this.countObjects(hql.toString());
	    
	}
	
	/**
	 * 获得所有用户表数据
	 * @return
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public List getUsersList() throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users de where 1 = 1 and de.isUsable = '0' " );
		
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获取用户集合
	 * @param name 用户名
	 * @author zwp
	 */
	public List getUserList(String name
			) throws BaseException {
	   StringBuffer hql = new StringBuffer("from Users de where 1 = 1" );
        if(StringUtil.isNotBlank(name)){
        	hql.append(" and de.userChinesename='").append(name).append("' ");
        }	    
	    hql.append(" and de.isUsable='0' ");
		hql.append(" order by de.id desc ");
		//System.out.println(hql);
		return this.getObjects( hql.toString() );
				
	}
	/**
	 * 根据登录名获得用户表数据  
	 * @author fyh
	 * @param rollPage 分页对象
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getUsersListByUserName( String userName) throws BaseException{
		StringBuffer hql = new StringBuffer("from Users de where 1 = 1" );
    	if(userName!=null&&userName.length()>0)
    		hql.append(" and de.userName='").append(userName).append("' ");
		hql.append(" and de.isUsable='0' ");
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	public List getUsersListByOrgUser(OrgUser ur) throws BaseException {
		StringBuffer hql = new StringBuffer("select distinct usr from Users usr,OrgUser de  where usr.userName=de.userName " );
		if(StringUtil.isNotBlank(ur) ){
			if( ! StringUtil.isBlank( ur.getOrgCode() )  ){
				hql.append(" and de.orgCode ='").append( ur.getOrgCode() ).append("'");
			}
		}
		
		hql.append(" and usr.isUsable = '0' order by usr.userId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有用户表数据集   岗位并不是空的
	 * @param user 查询参数对象
	 * @return
	 * @author zwp 2012-10-19
	 * @throws BaseException 
	 */
	public List getUsersListByNotStation(  Users user ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users usr where usr.station is not null " );
		if(StringUtil.isNotBlank(user) ){
			
			if(StringUtil.isNotBlank(user.getComId())){
				hql.append(" and usr.comId ='").append( user.getComId() ).append("'");
			}
		}
		hql.append(" and usr.isUsable = '0' order by usr.userId desc ");
		return this.getObjects( hql.toString() );
	}
} 
