package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysErrorLogBiz;
import com.ced.sip.system.entity.SysErrorLog;

public class SysErrorLogBizImpl extends BaseBizImpl implements ISysErrorLogBiz  {
	
	/**
	 * 根据主键获得错误日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public SysErrorLog getSysErrorLog(Long id) throws BaseException {
		return (SysErrorLog)this.getObject(SysErrorLog.class, id);
	}

	/**
	 * 添加角色信息
	 * @param tSysMessageLog 错误日志实例
	 * @throws BaseException 
	 */
	public void saveSysErrorLog(SysErrorLog SysErrorLog) throws BaseException{
		this.saveObject(SysErrorLog ) ;
	}
	
	/**
	 * 更新错误日志实例
	 * @param tSysErrorLog 错误日志实例
	 * @throws BaseException 
	 */
	public void updateSysErrorLog(SysErrorLog SysErrorLog) throws BaseException{
		this.updateObject(SysErrorLog ) ;
	}
	
	/**
	 * 删除错误日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysErrorLog(String id) throws BaseException {
		this.removeObject( this.getSysErrorLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除错误日志实例
	 * @param tSysErrorLog 错误日志实例
	 * @throws BaseException 
	 */
	public void deleteSysErrorLog(SysErrorLog tSysErrorLog) throws BaseException {
		this.removeObject( tSysErrorLog ) ;
	}
	
	/**
	 * 删除错误日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysErrorLog(String[] id) throws BaseException {
		this.removeBatchObject(SysErrorLog.class, id) ;
	}	
	
	/**
	 * 获得所有错误日志数据集
	 * @param tSysErrorLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getSysErrorLogList(SysErrorLog SysErrorLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysErrorLog de  where 1=1 " );
		if(SysErrorLog != null){
			
		}
		hql.append(" order by de.elId desc");
		
		return this.getObjects( hql.toString() );
	}


	
	public List getSysErrorLogList(RollPage rollPage,
			SysErrorLog SysErrorLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysErrorLog de  where 1=1 " );
		if(StringUtil.isNotBlank(SysErrorLog))
		{
			if(StringUtil.isNotBlank(SysErrorLog.getHashcode()))
			{
				hql.append(" and de.hashcode like '%").append(SysErrorLog.getHashcode()).append("%'");
			}
			if(StringUtil.isNotBlank(SysErrorLog.getWriter()))
			{
				hql.append(" and de.writer ='").append(SysErrorLog.getWriter()).append("'");
			}
			if(StringUtil.isNotBlank(SysErrorLog.getIpAddress()))
			{
				hql.append(" and de.ipAddress ='").append(SysErrorLog.getIpAddress()).append("'");
			}
		}
		hql.append(" order by de.elId desc");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	
	


	
	
}
