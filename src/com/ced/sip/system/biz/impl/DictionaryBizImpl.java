package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.TableStatus;
import com.ced.sip.system.biz.IDictionaryBiz;
import com.ced.sip.system.entity.Dictionary;

public class DictionaryBizImpl extends BaseBizImpl implements IDictionaryBiz  {
	
	/**
	 * 根据主键获得字典信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public Dictionary getTSysDict(Long id) throws BaseException {
		return (Dictionary)this.getObject(Dictionary.class, id);
	}
	
	/**
	 * 获得字典信息表实例
	 * @param tSysDict 字典信息表实例
	 * @return
	 * @throws BaseException 
	 */
	public Dictionary getTSysDict( Dictionary tSysDict ) throws BaseException {
		return (Dictionary)this.getObject(Dictionary.class, tSysDict.getDictId() );
	}
	
	/**
	 * 添加字典信息信息
	 * @param tSysDict 字典信息表实例
	 * @throws BaseException 
	 */
	public void saveTSysDict(Dictionary tSysDict) throws BaseException{
		this.saveObject( tSysDict ) ;
	}
	
	/**
	 * 更新字典信息表实例
	 * @param tSysDict 字典信息表实例
	 * @throws BaseException 
	 */
	public void updateTSysDict(Dictionary tSysDict) throws BaseException{
		this.updateObject( tSysDict ) ;
	}
	
	/**
	 * 删除字典信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteTSysDict(String id) throws BaseException {
		this.removeObject( this.getTSysDict( new Long(id) ) ) ;
	}
	
	/**
	 * 删除字典信息表实例
	 * @param tSysDict 字典信息表实例
	 * @throws BaseException 
	 */
	public void deleteTSysDict(Dictionary tSysDict) throws BaseException {
		this.removeObject( tSysDict ) ;
	}
	
	/**
	 * 删除字典信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteTSysDicts(String[] id) throws BaseException {
		this.removeBatchObject(Dictionary.class, id) ;
	}
	
	/**
	 * 获得所有字典信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getTSysDictList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Dictionary de where 1 = 1 " );
		
		hql.append( " and de.isUsable = '" ).append( TableStatus.COMMON_STATUS_VALID ).append("' ") ;
		
		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有字典信息表数据集
	 * @param tSysDict 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getTSysDictList(  Dictionary tSysDict ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Dictionary de where 1 = 1 " );
		if( tSysDict != null ) {
			// 父类ID
			if( tSysDict.getParentDictId() != null ) {
				hql.append(" and de.parentDictId = " ).append( tSysDict.getParentDictId() ) ;
			}
			if( tSysDict.getParentDictCode() != null ) {
				hql.append(" and de.parentDictCode = " ).append( tSysDict.getParentDictCode() ) ;
			}
		}
		
		hql.append( " and de.isUsable = '" ).append( TableStatus.COMMON_STATUS_VALID ).append("' ") ;
		
		hql.append(" order by de.dictOrder asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有字典信息表数据集
	 * @param rollPage 分页对象
	 * @param tSysDict 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getTSysDictList( RollPage rollPage, Dictionary tSysDict ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Dictionary de where 1 = 1 " );

		if( tSysDict != null ) {
			// 父类ID
			if( tSysDict.getParentDictId() != null ) {
				hql.append(" and de.parentDictId = " ).append( tSysDict.getParentDictId() ) ;
			}
			if( tSysDict.getDictName()!= null ) {
				hql.append(" and de.dictName like '%").append( tSysDict.getDictName() ).append("%'") ;
			}
		}
		
		hql.append( " and de.isUsable = '" ).append( TableStatus.COMMON_STATUS_VALID ).append("' ") ;
		
        if(rollPage.getOrderColumn()!=null){
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
        }
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
