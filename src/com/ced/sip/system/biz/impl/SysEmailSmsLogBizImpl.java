package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.biz.ISysEmailSmsLogBiz;
import com.ced.sip.system.entity.SysEmailSmsLog;
/** 
 * 类名称：SysEmailSmsLogBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-10
 */
public class SysEmailSmsLogBizImpl extends BaseBizImpl implements ISysEmailSmsLogBiz  {
	
	/**
	 * 根据主键获得用户邮件短信日志表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-10
	 * @return
	 * @throws BaseException 
	 */
	public SysEmailSmsLog getSysEmailSmsLog(Long id) throws BaseException {
		return (SysEmailSmsLog)this.getObject(SysEmailSmsLog.class, id);
	}
	
	/**
	 * 获得用户邮件短信日志表实例
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @author luguanglei 2017-08-10
	 * @return
	 * @throws BaseException 
	 */
	public SysEmailSmsLog getSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException {
		return (SysEmailSmsLog)this.getObject(SysEmailSmsLog.class, sysEmailSmsLog.getSeslId() );
	}
	
	/**
	 * 添加用户邮件短信日志信息
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @author luguanglei 2017-08-10
	 * @throws BaseException 
	 */
	public void saveSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException{
		this.saveObject( sysEmailSmsLog ) ;
	}
	
	/**
	 * 更新用户邮件短信日志表实例
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @author luguanglei 2017-08-10
	 * @throws BaseException 
	 */
	public void updateSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException{
		this.updateObject( sysEmailSmsLog ) ;
	}
	
	/**
	 * 删除用户邮件短信日志表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-10
	 * @throws BaseException 
	 */
	public void deleteSysEmailSmsLog(String id) throws BaseException {
		this.removeObject( this.getSysEmailSmsLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除用户邮件短信日志表实例
	 * @param sysEmailSmsLog 用户邮件短信日志表实例
	 * @author luguanglei 2017-08-10
	 * @throws BaseException 
	 */
	public void deleteSysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException {
		this.removeObject( sysEmailSmsLog ) ;
	}
	
	/**
	 * 删除用户邮件短信日志表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-10
	 * @throws BaseException 
	 */
	public void deleteSysEmailSmsLogs(String[] id) throws BaseException {
		this.removeBatchObject(SysEmailSmsLog.class, id) ;
	}
	
	/**
	 * 获得用户邮件短信日志表数据集
	 * sysEmailSmsLog 用户邮件短信日志表实例
	 * @author luguanglei 2017-08-10
	 * @return
	 * @throws BaseException 
	 */
	public SysEmailSmsLog getSysEmailSmsLogBySysEmailSmsLog(SysEmailSmsLog sysEmailSmsLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysEmailSmsLog de where 1 = 1 " );

		hql.append(" order by de.seslId desc ");
		List list = this.getObjects( hql.toString() );
		sysEmailSmsLog = new SysEmailSmsLog();
		if(list!=null&&list.size()>0){
			sysEmailSmsLog = (SysEmailSmsLog)list.get(0);
		}
		return sysEmailSmsLog;
	}
	
	/**
	 * 获得所有用户邮件短信日志表数据集
	 * @param sysEmailSmsLog 查询参数对象
	 * @author luguanglei 2017-08-10
	 * @return
	 * @throws BaseException 
	 */
	public List getSysEmailSmsLogList(SysEmailSmsLog sysEmailSmsLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysEmailSmsLog de where 1 = 1 " );

		hql.append(" order by de.seslId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有用户邮件短信日志表数据集
	 * @param rollPage 分页对象
	 * @param sysEmailSmsLog 查询参数对象
	 * @author luguanglei 2017-08-10
	 * @return
	 * @throws BaseException 
	 */
	public List getSysEmailSmsLogList(RollPage rollPage, SysEmailSmsLog sysEmailSmsLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SysEmailSmsLog de where 1 = 1 " );

		hql.append(" order by de.seslId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
