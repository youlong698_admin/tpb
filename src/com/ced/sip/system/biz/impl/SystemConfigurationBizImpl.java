package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.sip.system.biz.ISystemConfigurationBiz;
import com.ced.sip.system.entity.SystemConfiguration;
/** 
 * 类名称：SystemConfigurationBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-03-30
 */
public class SystemConfigurationBizImpl extends BaseBizImpl implements ISystemConfigurationBiz  {
	
	/**
	 * 根据主键获得系统配置表表实例
	 * @param id 主键
	 * @author luguanglei 2017-03-30
	 * @return
	 * @throws BaseException 
	 */
	public SystemConfiguration getSystemConfiguration(Long comId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SystemConfiguration de where de.comId="+comId );
		List list=this.getObjects(hql.toString());
		SystemConfiguration systemConfiguration=new SystemConfiguration();
		if(list.size()>0) systemConfiguration=(SystemConfiguration)list.get(0);
		return systemConfiguration;
	}
	
	/**
	 * 更新系统配置表表实例
	 * @param systemConfiguration 系统配置表表实例
	 * @author luguanglei 2017-03-30
	 * @throws BaseException 
	 */
	public void updateSystemConfiguration(SystemConfiguration systemConfiguration) throws BaseException{
		this.updateObject( systemConfiguration ) ;
	}

	/**
	 * 保存系统配置表表实例
	 * @param systemConfiguration 系统配置表表实例
	 * @throws BaseException 
	 */
	public void saveSystemConfiguration(SystemConfiguration systemConfiguration)
			throws BaseException {
		this.saveObject(systemConfiguration);
		
	}
	
}
