package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysRoleBiz;
import com.ced.sip.system.entity.SysRole;

public class SysRoleBizImpl extends BaseBizImpl implements ISysRoleBiz {

	public SysRole getSysRole(Long id) throws BaseException {
		// TODO Auto-generated method stub
		return (SysRole)this.getObject(SysRole.class, id);
	}

	public void saveSysRole(SysRole sysRole) throws BaseException {
		// TODO Auto-generated method stub
		this.saveObject(sysRole);
	}

	public void updateSysRole(SysRole sysRole) throws BaseException {
		// TODO Auto-generated method stub
		this.updateObject(sysRole) ;
	}

	public void deleteSysRole(String id) throws BaseException {
		// TODO Auto-generated method stub
		this.removeObject( this.getSysRole(new Long(id)) ) ;
	}

	public void deleteSysRole(SysRole sysRole) throws BaseException {
		// TODO Auto-generated method stub
		this.removeObject( sysRole ) ;
	}

	public void deleteSysRole(String[] id) throws BaseException {
		// TODO Auto-generated method stub
		this.removeBatchObject(SysRole.class, id) ;
	}

	public List getSysRoleList(RollPage rollPage) throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from SysRole de where 1 = 1 " );
		
		return this.getObjects( rollPage,hql.toString() );
	}

	public List getSysRoleList(SysRole sysRole) throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from SysRole de where 1 = 1 " );
		hql.append(" order by de.roleNo desc ");
		return this.getObjects(hql.toString());
	}

	public List getSysRoleList(RollPage rollPage, SysRole sysRole)
			throws BaseException {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select de,sc.compName from SysRole de,SysCompany sc where de.comId=sc.scId " );
		if(sysRole!=null){
			if(!StringUtil.isBlank(sysRole.getRoleName())){
				hql.append(" and de.roleName like '%").append(sysRole.getRoleName()).append("%'");
			}
			if (StringUtil.isNotBlank(sysRole.getComId())) {
				hql.append(" and de.comId = ").append(sysRole.getComId()).append("");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects( rollPage,hql.toString() );
	}

}
