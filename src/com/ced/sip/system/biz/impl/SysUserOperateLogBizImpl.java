package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysUserOperateLogBiz;
import com.ced.sip.system.entity.SysUserOperateLog;

public class SysUserOperateLogBizImpl extends BaseBizImpl implements ISysUserOperateLogBiz  {
	
	/**
	 * 根据主键获得用户操作日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public SysUserOperateLog getSysUserOperateLog(Long id) throws BaseException {
		return (SysUserOperateLog)this.getObject(SysUserOperateLog.class, id);
	}

	/**
	 * 添加角色信息
	 * @param tSysUserOperateLog 用户操作日志实例
	 * @throws BaseException 
	 */
	public void saveSysUserOperateLog(SysUserOperateLog SysUserOperateLog) throws BaseException{
		this.saveObject(SysUserOperateLog ) ;
	}
	
	/**
	 * 更新用户操作日志实例
	 * @param tSysUserOperateLog 用户操作日志实例
	 * @throws BaseException 
	 */
	public void updateSysUserOperateLog(SysUserOperateLog SysUserOperateLog) throws BaseException{
		this.updateObject(SysUserOperateLog ) ;
	}
	
	/**
	 * 删除用户操作日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysUserOperateLog(String id) throws BaseException {
		this.removeObject( this.getSysUserOperateLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除用户操作日志实例
	 * @param tSysUserOperateLog 用户操作日志实例
	 * @throws BaseException 
	 */
	public void deleteSysUserOperateLog(SysUserOperateLog SysUserOperateLog) throws BaseException {
		this.removeObject(SysUserOperateLog ) ;
	}
	
	/**
	 * 删除用户操作日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysUserOperateLog(String[] id) throws BaseException {
		this.removeBatchObject(SysUserOperateLog.class, id) ;
	}	
	
	/**
	 * 获得所有用户操作日志数据集
	 * @param tSysUserOperateLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getSysUserOperateLogList(SysUserOperateLog SysUserOperateLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from SysUserOperateLog de  where 1=1 " );
		if(SysUserOperateLog != null){
			
		}
		hql.append(" order by de.uilId desc");
		
		return this.getObjects( hql.toString() );
	}


	
	public List getSysUserOperateLogList(RollPage rollPage,
			SysUserOperateLog SysUserOperateLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from SysUserOperateLog de  where 1=1 " );
		
		if(StringUtil.isNotBlank(SysUserOperateLog))
		{
			if(StringUtil.isNotBlank(SysUserOperateLog.getMethod()))
			{
				hql.append(" and de.method like '%").append(SysUserOperateLog.getMethod()).append("%'");
			}
			if(StringUtil.isNotBlank(SysUserOperateLog.getWriter()))
			{
				hql.append(" and de.writer ='").append(SysUserOperateLog.getWriter()).append("'");
			}
			if(StringUtil.isNotBlank(SysUserOperateLog.getIpAddress())){
				hql.append(" and de.ipAddress ='").append(SysUserOperateLog.getIpAddress()).append("'");
			}
			
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	
	


	
	
}
