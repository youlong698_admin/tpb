package com.ced.sip.system.biz.impl;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Users;

public class DepartmentsBizImpl extends BaseBizImpl implements IDepartmentsBiz   {

	/**
	 * 根据主键获得部门表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public Departments getDepartments(Long id) throws BaseException {
		return (Departments)this.getObject(Departments.class, id);
	}
	
	/**
	 * 添加部门表信息
	 * @param Departments 部门表实例
	 * @throws BaseException 
	 */
	public void saveDepartments(Departments departments) throws BaseException{
		this.saveObject( departments ) ;
	}
	
	/**
	 * 更新部门表实例
	 * @param departments 部门表实例
	 * @throws BaseException 
	 */
	public void updateDepartments(Departments departments ) throws BaseException{
		this.updateObject( departments ) ;
	}
	
	/**
	 * 更新部门表实例-修改本级和本级所有子节点为不可用
	 * @param departments 部门表实例
	 * @throws BaseException 
	 */
	public void updateDepartmentsBySelflevCode(  Departments departments  )throws BaseException{
		StringBuffer sbHql = new StringBuffer( " from  Departments dept where  1=1 " );
		
		if( departments != null && departments.getDepId() != null ) {
			sbHql.append( " and (dept.selflevCode like '%," ).append( departments.getDepId() ).append(",%'") ;
			sbHql.append( " or dept.depId = " ).append( departments.getDepId()+" )" );
			
			List deptList= getObjects( sbHql.toString() );
			
			if(deptList!=null){
				for(int i=0;i<deptList.size();i++){
					Departments  dept = (Departments) deptList.get(i);
					dept.setIsUsable("1");
					
					String  sql="update users set is_usable='1' where dep_id="+dept.getDepId();
					this.updateJdbcSql(sql);
					
					this.updateDepartments( dept ) ;
				}
			}
		}
	}
	
	/**
	 * 删除部门表实例
	 * @param id 主键
	 * @throws BaseException 
	 */
	public void deleteDepartments(Long id) throws BaseException {
		this.removeObject( this.getDepartments( new Long(id) ) ) ;
	}
	
	/**
	 * 删除部门表实例
	 * @param departments 部门表实例
	 * @throws BaseException 
	 */
	public void deleteDepartments(Departments departments) throws BaseException {
		this.removeObject( departments ) ;
	}
	/**
	 * 删除部门表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteDemos(String[] id) throws BaseException {
		this.removeBatchObject(Departments.class, id) ;
	} 
	
	/**
	 * 获得某些指定部门数据集
	 * @Cacheable(cacheName="departCache") 
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	
	public List<Departments> getSomeDepartmentsList( String deptNames ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Departments dept where 1 = 1 " );

		if( deptNames!=null ) {
			hql.append(" and dept.deptName in(" ).append( deptNames ).append(")") ;
		}
		//hql.append(" and dept.isUsable = '0' ") ;
		hql.append(" order by dept.deptOrder asc ") ;
		
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得用户表数据集
	 * @return
	 * @throws BaseException 
	 */
	
	public List<Users> getUsersList() throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users usr where 1 = 1 " );
		
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有部门数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List<Departments> getDepartmentsList( RollPage rollPage ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Departments dept where 1 = 1 " );
		hql.append(" and dept.isUsable = '0' ") ;
		hql.append(" order by dept.remark asc ") ;
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有部门数据集
	 * @Cacheable(cacheName="departCache") 
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	
	public List<Departments> getDepartmentsList( Departments departments ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Departments dept where 1 = 1 and dept.isUsable='0' " );

		if( departments != null ) {
			if( departments.getDepId() != null ) {
				hql.append(" and dept.depId = " ).append( departments.getDepId()) ;
			}
			// 父类ID
			if( departments.getParentDeptId() != null ) {
				hql.append(" and dept.parentDeptId = " ).append( departments.getParentDeptId() ) ;
			}
			// 部门编码
			if( departments.getDeptCode() != null && departments.getDeptCode().length()>0 ) {
				hql.append(" and dept.deptCode = '" ).append( departments.getDeptCode() ).append("' ") ;
			}
			if( departments.getDeptName() != null && departments.getDeptName().length()>0 ) {
				hql.append(" and dept.deptName = '" ).append( departments.getDeptName() ).append("' ") ;
			}
			if( departments.getIsPurchaseDept() != null) {
				hql.append(" and dept.isPurchaseDept = '" ).append( departments.getIsPurchaseDept() ).append("' ") ;
			}
			if (StringUtil.isNotBlank(departments.getComId())) {
				hql.append(" and dept.comId = ").append(departments.getComId()).append("");
			}
		}
		hql.append(" order by dept.deptOrder asc ") ;
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有部门数据集
	 * @param rollPage 分页对象
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	public List<Departments> getDepartmentsList( RollPage rollPage, Departments departments ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Departments dept where 1 = 1 " );

		if( departments != null ) {
			// 父类ID
			if( departments.getParentDeptId() != null ) {
				hql.append(" and dept.parentDeptId = " ).append( departments.getParentDeptId() ) ;
			}
			// 部门编码
			if( departments.getDeptCode() != null && departments.getDeptCode().length()>0 ) {
				hql.append(" and dept.depCode = '" ).append( departments.getDeptCode() ).append("' ") ;
			}
			if (StringUtil.isNotBlank(departments.getComId())) {
				hql.append(" and dept.comId = ").append(departments.getComId()).append("");
			}
		}
		
		//hql.append(" and dept.isUsable = '0' ") ;
		
		hql.append(" order by dept.deptOrder asc ") ;
		
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有部门数量
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	public int getDepartmentsCount( Departments departments ) throws BaseException {
		StringBuffer hql = new StringBuffer(" select count(*) from Departments dept where 1 = 1 " );

		if( departments != null ) {
			// 父类ID
			if( departments.getParentDeptId() != null ) {
				hql.append(" and dept.parentDeptId = " ).append( departments.getParentDeptId() ) ;
			}
			// 部门编码
			if( departments.getDeptCode() != null && departments.getDeptCode().length()>0 ) {
				hql.append(" and dept.deptCode = '" ).append( departments.getDeptCode() ).append("' ") ;
			}

			if (StringUtil.isNotBlank(departments.getComId())) {
				hql.append(" and dept.comId = ").append(departments.getComId()).append("");
			}
		}
		
		hql.append(" and dept.isUsable = '0' ") ;
		
		return this.countObjects( hql.toString() ) ;
	}
	
	/**
	 * 获得所有部门数据集
	 * @return
	 * @throws BaseException 
	 */
	public List<Departments> getTreeDepartmentsList() throws BaseException {
		List<Departments> list=	this.getObjects(Departments.class);
		
		return list;    
	}
	
	/**
	 * 获得所有部门数据集
	 * @param rollPage 分页对象
	 * @param Departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	public List<Users> getUsersList( RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Users  where 1 = 1 " );
			return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得根据部门编号数据集
	 * @return
	 * @throws BaseException 
	 */
	public List<Departments> getDepartmentsCodeList(Departments departments ) throws BaseException {
		StringBuffer hql = new StringBuffer("from Departments where depCode='"+departments.getDeptCode().trim()+"'");
		List<Departments> list=	this.getObjects(hql.toString());
		
		return list;
	}
	
	/**
	 * 获得上级部门编号数据集，如果有下级部门，上级部门不允许删除
	 * @return
	 * @throws BaseException 
	 */
	public List<Departments> getDepartmentsupDepIdList(Departments departments ) throws BaseException {
		StringBuffer hql = new StringBuffer("from Departments where updepId='"+departments.getDeptCode()+"'");
		List<Departments> list=	this.getObjects(hql.toString());
		
		return list;
	}
	/**
	 * 获得所有部门人员视图
	 * @return
	 * @throws BaseException 
	 */
	public List getAllDeptRoot() throws BaseException {
		StringBuffer hql=new StringBuffer(" from Departments d where d.deptLevel = 1 ");
		return this.getObjects(hql.toString());
	}

	public Departments getDepartmentsByName(String deptName)
			throws BaseException {
		Departments dept = null;
		StringBuffer hql = new StringBuffer("from Departments where deptName='"+deptName+"'");
		List<Departments> list=	this.getObjects(hql.toString());
		if(list.size()>0){
			dept = list.get(0);
		}
		return dept;
	}

	
	public List getDepIdsByUserId(String userName) throws BaseException {
		String sql="select t.dep_id from departments t,org_user ou,org_rights ors where t.dep_id=ors.dep_id and ors.org_id=ou.org_id and ou.user_name='"+userName+"'";
		return this.getObjectsList(Departments.class, sql);
	}

	/**
	 * 根据当前登录人查询table显示的数据权限
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 */
	public List getDepartsToUserId(Long userId) throws BaseException {
		// TODO Auto-generated method stub
		String sql="select t.dep_id from departments t where t.comp_leader_id='"+userId+"' union select t.dep_id from departments t where t.dep_leader_id='"+userId+"'";
		return this.getObjectsList(Departments.class, sql);
	}
	
	/**
	 * 根据当前登录人查询table显示部门领导
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 */
	public List getDepToUserId(Long userId) throws BaseException {
		// TODO Auto-generated method stub
		String sql="select t.dep_id from departments t where t.dep_leader_id='"+userId+"'";
		return this.getObjectsList(Departments.class, sql);
	}

	public List getSignUsersList() throws BaseException {
        StringBuffer hql = new StringBuffer();
		List<Departments> list=new ArrayList<Departments>();
		hql.append( "select new Departments(t.depLeaderId,t.depLeaderCn)  from Departments t ");
		list=this.getObjects( hql.toString() );
		
		return list;
	}
	

	public List getComSignUsersList() throws BaseException {
        StringBuffer hql = new StringBuffer();
		List<Departments> list=new ArrayList<Departments>();
		hql.append( "select new Departments(t.compLeaderId,t.compLeaderCn)  from Departments t ");
		list=this.getObjects( hql.toString() );
		
		return list;
	}
	/**
	 * 获得所有该部门下权限采购组的数据集
	 * @param deptId 部门ID
	 * @return
	 * @throws BaseException 
	 */	
	public List<Departments> getPurchaseDepartmentsListByDeptId(Long deptId) throws BaseException {
		StringBuffer hql = new StringBuffer("from Departments dept where exists (select depId from Departments k where k.purchaseDeptId="+deptId+" and dept.depId=k.depId) ") ;
		return this.getObjects( hql.toString());
	}

	/**
	 * 根据层次级别码和层次级别获取组织机构对象
	 * @param selflevCode
	 * @param deptLevel
	 * @return
	 * @throws BaseException
	 */
	public Departments getDepartmentsBySelflevCode(String selflevCode, Long deptLevel)
			throws BaseException {
		Departments dept=null;
		StringBuffer hql = new StringBuffer("from Departments dept where '"+selflevCode+"' like dept.selflevCode||'%' and dept.deptLevel="+deptLevel) ;
		List<Departments> list=	this.getObjects(hql.toString());
		if(list.size()>0){
			dept = list.get(0);
		}
		return dept;
	}
}
