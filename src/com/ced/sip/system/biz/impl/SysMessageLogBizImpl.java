package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.ISysMessageLogBiz;
import com.ced.sip.system.entity.SysMessageLog;

public class SysMessageLogBizImpl extends BaseBizImpl implements ISysMessageLogBiz  {
	
	/**
	 * 根据主键获得用户消息日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public SysMessageLog getSysMessageLog(Long id) throws BaseException {
		return (SysMessageLog)this.getObject(SysMessageLog.class, id);
	}

	/**
	 * 添加角色信息
	 * @param tSysMessageLog 用户消息日志实例
	 * @throws BaseException 
	 */
	public void saveSysMessageLog(SysMessageLog SysMessageLog) throws BaseException{
		this.saveObject(SysMessageLog ) ;
	}
	
	/**
	 * 更新用户消息日志实例
	 * @param tSysMessageLog 用户消息日志实例
	 * @throws BaseException 
	 */
	public void updateSysMessageLog(SysMessageLog SysMessageLog) throws BaseException{
		this.updateObject(SysMessageLog ) ;
	}
	
	/**
	 * 删除用户消息日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysMessageLog(String id) throws BaseException {
		this.removeObject( this.getSysMessageLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除用户消息日志实例
	 * @param tSysMessageLog 用户消息日志实例
	 * @throws BaseException 
	 */
	public void deleteSysMessageLog(SysMessageLog SysMessageLog) throws BaseException {
		this.removeObject(SysMessageLog ) ;
	}
	
	/**
	 * 删除用户消息日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteSysMessageLog(String[] id) throws BaseException {
		this.removeBatchObject(SysMessageLog.class, id) ;
	}	
	
	/**
	 * 获得所有用户消息日志数据集
	 * @param tSysMessageLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getSysMessageLogList(SysMessageLog SysMessageLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from SysMessageLog de  where 1=1 " );
		if(SysMessageLog != null){
			if(StringUtil.isNotBlank(SysMessageLog.getReceivePerson())){
				hql.append(" and de.receivePerson = '").append(SysMessageLog.getReceivePerson()).append("'");
			}
			if(StringUtil.isNotBlank(SysMessageLog.getSendPerson())){
				hql.append(" and de.sendPerson = '").append(SysMessageLog.getSendPerson()).append("'");
			}
			if(StringUtil.isNotBlank(SysMessageLog.getSendPersonCn())){
				hql.append(" and de.sendPersonCn = '").append(SysMessageLog.getSendPersonCn()).append("'");
			}
			if(StringUtil.isNotBlank(SysMessageLog.getMessageType())){
				hql.append(" and de.messageType = '").append(SysMessageLog.getMessageType()).append("'");
			}
			if(StringUtil.isNotBlank(SysMessageLog.getMessageContent())){
				hql.append(" and de.messageContent like '%").append(SysMessageLog.getMessageContent()).append("%'");
			}
			if(StringUtil.isNotBlank(SysMessageLog.getStatus())){
				hql.append(" and de.status = '").append(SysMessageLog.getStatus()).append("'");
			}
		}
		hql.append(" order by de.status,de.sendDate desc");
		
		return this.getObjects( hql.toString() );
	}


	
	public List getSysMessageLogList(RollPage rollPage,
			SysMessageLog SysMessageLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from SysMessageLog de  where 1=1 " );
		if(SysMessageLog != null){
			if(SysMessageLog != null){
				if(StringUtil.isNotBlank(SysMessageLog.getReceivePerson())){
					hql.append(" and de.receivePerson = '").append(SysMessageLog.getReceivePerson()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getSendPerson())){
					hql.append(" and de.sendPerson = '").append(SysMessageLog.getSendPerson()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getSendPersonCn())){
					hql.append(" and de.sendPersonCn = '").append(SysMessageLog.getSendPersonCn()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getMessageType())){
					hql.append(" and de.messageType = '").append(SysMessageLog.getMessageType()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getMessageContent())){
					hql.append(" and de.messageContent like '%").append(SysMessageLog.getMessageContent()).append("%'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getStatus())){
					hql.append(" and de.status = '").append(SysMessageLog.getStatus()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getType())){
					hql.append(" and de.type = '").append(SysMessageLog.getType()).append("'");
				}
			}
		}
		hql.append(" order by de.status,de.sendDate desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	
	public int getSysMessageLogCount(SysMessageLog SysMessageLog)
			throws BaseException {
		StringBuffer hql = new StringBuffer(" select count(de.smlId)  from SysMessageLog de  where 1=1 " );
		if(SysMessageLog != null){
			if(SysMessageLog != null){
				if(StringUtil.isNotBlank(SysMessageLog.getReceivePerson())){
					hql.append(" and de.receivePerson = '").append(SysMessageLog.getReceivePerson()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getSendPerson())){
					hql.append(" and de.sendPerson = '").append(SysMessageLog.getSendPerson()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getSendPersonCn())){
					hql.append(" and de.sendPersonCn = '").append(SysMessageLog.getSendPersonCn()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getMessageType())){
					hql.append(" and de.messageType = '").append(SysMessageLog.getMessageType()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getMessageContent())){
					hql.append(" and de.messageContent like '%").append(SysMessageLog.getMessageContent()).append("%'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getStatus())){
					hql.append(" and de.status = '").append(SysMessageLog.getStatus()).append("'");
				}
				if(StringUtil.isNotBlank(SysMessageLog.getType())){
					hql.append(" and de.type = '").append(SysMessageLog.getType()).append("'");
				}
			}
		}
		return this.countObjects(hql.toString());
	}
	
}
