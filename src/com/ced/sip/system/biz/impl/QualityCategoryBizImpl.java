package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IQualityCategoryBiz;
import com.ced.sip.system.entity.QualityCategory;
/** 
 * 类名称：QualityCategoryBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-03-07
 */
public class QualityCategoryBizImpl extends BaseBizImpl implements IQualityCategoryBiz  {
	
	/**
	 * 根据主键获得资质文件类别表实例
	 * @param id 主键
	 * @author luguanglei 2017-03-07
	 * @return
	 * @throws BaseException 
	 */
	public QualityCategory getQualityCategory(Long id) throws BaseException {
		return (QualityCategory)this.getObject(QualityCategory.class, id);
	}
	
	/**
	 * 获得资质文件类别表实例
	 * @param qualityCategory 资质文件类别表实例
	 * @author luguanglei 2017-03-07
	 * @return
	 * @throws BaseException 
	 */
	public QualityCategory getQualityCategory(QualityCategory qualityCategory) throws BaseException {
		return (QualityCategory)this.getObject(QualityCategory.class, qualityCategory.getQcId() );
	}
	
	/**
	 * 添加资质文件类别信息
	 * @param qualityCategory 资质文件类别表实例
	 * @author luguanglei 2017-03-07
	 * @throws BaseException 
	 */
	public void saveQualityCategory(QualityCategory qualityCategory) throws BaseException{
		this.saveObject( qualityCategory ) ;
	}
	
	/**
	 * 更新资质文件类别表实例
	 * @param qualityCategory 资质文件类别表实例
	 * @author luguanglei 2017-03-07
	 * @throws BaseException 
	 */
	public void updateQualityCategory(QualityCategory qualityCategory) throws BaseException{
		this.updateObject( qualityCategory ) ;
	}
	
	/**
	 * 删除资质文件类别表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-03-07
	 * @throws BaseException 
	 */
	public void deleteQualityCategory(String id) throws BaseException {
		this.removeObject( this.getQualityCategory( new Long(id) ) ) ;
	}
	
	/**
	 * 删除资质文件类别表实例
	 * @param qualityCategory 资质文件类别表实例
	 * @author luguanglei 2017-03-07
	 * @throws BaseException 
	 */
	public void deleteQualityCategory(QualityCategory qualityCategory) throws BaseException {
		this.removeObject( qualityCategory ) ;
	}
	
	/**
	 * 删除资质文件类别表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-03-07
	 * @throws BaseException 
	 */
	public void deleteQualityCategorys(String[] id) throws BaseException {
		this.removeBatchObject(QualityCategory.class, id) ;
	}
	
	/**
	 * 获得所有资质文件类别表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-03-07
	 * @return
	 * @throws BaseException 
	 */
	public List getQualityCategoryList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from QualityCategory de where 1 = 1 " );

		hql.append(" order by de.qcId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有资质文件类别表数据集
	 * @param qualityCategory 查询参数对象
	 * @author luguanglei 2017-03-07
	 * @return
	 * @throws BaseException 
	 */
	public List getQualityCategoryList(QualityCategory qualityCategory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from QualityCategory de where 1 = 1 " );
		if(StringUtil.isNotBlank(qualityCategory) ){
			if(StringUtil.isNotBlank(qualityCategory.getQualityName())){
				hql.append(" and de.qualityName like '%").append( qualityCategory.getQualityName()).append("%'");
			}
		}
		hql.append(" order by de.qcId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有资质文件类别表数据集
	 * @param rollPage 分页对象
	 * @param qualityCategory 查询参数对象
	 * @author luguanglei 2017-03-07
	 * @return
	 * @throws BaseException 
	 */
	public List getQualityCategoryList(RollPage rollPage, QualityCategory qualityCategory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from QualityCategory de where 1 = 1 " );
		if(StringUtil.isNotBlank(qualityCategory) ){
			if(StringUtil.isNotBlank(qualityCategory.getQualityName())){
				hql.append(" and de.qualityName like '%").append( qualityCategory.getQualityName()).append("%'");
			}
		}
		hql.append(" order by de.qcId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
