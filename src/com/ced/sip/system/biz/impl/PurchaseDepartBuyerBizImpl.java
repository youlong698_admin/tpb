package com.ced.sip.system.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.biz.IPurchaseDepartBuyerBiz;
import com.ced.sip.system.entity.PurchaseDepartBuyer;
/** 
 * 类名称：PurchaseDepartBuyerBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-09-14
 */
public class PurchaseDepartBuyerBizImpl extends BaseBizImpl implements IPurchaseDepartBuyerBiz  {
	
	/**
	 * 根据主键获得采购人采购部门表实例
	 * @param id 主键
	 * @author luguanglei 2017-09-14
	 * @return
	 * @throws BaseException 
	 */
	public PurchaseDepartBuyer getPurchaseDepartBuyer(Long id) throws BaseException {
		return (PurchaseDepartBuyer)this.getObject(PurchaseDepartBuyer.class, id);
	}
	
	/**
	 * 获得采购人采购部门表实例
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @author luguanglei 2017-09-14
	 * @return
	 * @throws BaseException 
	 */
	public PurchaseDepartBuyer getPurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException {
		return (PurchaseDepartBuyer)this.getObject(PurchaseDepartBuyer.class, purchaseDepartBuyer.getPdbId() );
	}
	
	/**
	 * 添加采购人采购部门信息
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @author luguanglei 2017-09-14
	 * @throws BaseException 
	 */
	public void savePurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException{
		this.saveObject( purchaseDepartBuyer ) ;
	}
	
	/**
	 * 更新采购人采购部门表实例
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @author luguanglei 2017-09-14
	 * @throws BaseException 
	 */
	public void updatePurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException{
		this.updateObject( purchaseDepartBuyer ) ;
	}
	
	/**
	 * 删除采购人采购部门表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-09-14
	 * @throws BaseException 
	 */
	public void deletePurchaseDepartBuyer(String id) throws BaseException {
		this.removeObject( this.getPurchaseDepartBuyer( new Long(id) ) ) ;
	}
	
	/**
	 * 删除采购人采购部门表实例
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @author luguanglei 2017-09-14
	 * @throws BaseException 
	 */
	public void deletePurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException {
		this.removeObject( purchaseDepartBuyer ) ;
	}
	
	/**
	 * 删除采购人采购部门表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-09-14
	 * @throws BaseException 
	 */
	public void deletePurchaseDepartBuyers(String[] id) throws BaseException {
		this.removeBatchObject(PurchaseDepartBuyer.class, id) ;
	}
	
	/**
	 * 获得采购人采购部门表数据集
	 * purchaseDepartBuyer 采购人采购部门表实例
	 * @author luguanglei 2017-09-14
	 * @return
	 * @throws BaseException 
	 */
	public PurchaseDepartBuyer getPurchaseDepartBuyerByPurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseDepartBuyer de where 1 = 1 " );

		hql.append(" order by de.pdbId desc ");
		List list = this.getObjects( hql.toString() );
		purchaseDepartBuyer = new PurchaseDepartBuyer();
		if(list!=null&&list.size()>0){
			purchaseDepartBuyer = (PurchaseDepartBuyer)list.get(0);
		}
		return purchaseDepartBuyer;
	}
	
	/**
	 * 获得所有采购人采购部门表数据集
	 * @param purchaseDepartBuyer 查询参数对象
	 * @author luguanglei 2017-09-14
	 * @return
	 * @throws BaseException 
	 */
	public List getPurchaseDepartBuyerList(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseDepartBuyer de where 1 = 1 " );
		if(StringUtil.isNotBlank(purchaseDepartBuyer) ){
			if(StringUtil.isNotBlank(purchaseDepartBuyer.getBuyerId())){
				hql.append(" and buyerId = ").append( purchaseDepartBuyer.getBuyerId());
			}
		}
		hql.append(" order by de.pdbId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有采购人采购部门表数据集
	 * @param rollPage 分页对象
	 * @param purchaseDepartBuyer 查询参数对象
	 * @author luguanglei 2017-09-14
	 * @return
	 * @throws BaseException 
	 */
	public List getPurchaseDepartBuyerList(RollPage rollPage, PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseDepartBuyer de where 1 = 1 " );

		hql.append(" order by de.pdbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 获得所有采购人采购部门表数据集
	 * @param rollPage 分页对象
	 * @param purchaseDepartBuyer 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public void savePurchaseDepartBuyerByDeptPurchaseId(String[] deptPurchaseId,
			Long userId, Long comId) throws BaseException {
		StringBuffer sql=new StringBuffer("begin ");
		for(int i=0;i<deptPurchaseId.length;i++){
			sql.append(" insert into purchase_depart_buyer(pdb_id,dept_id,buyer_id,is_usable,dept_name,com_id) select seq_purchase_depart_buyer_id.nextval,"+deptPurchaseId[i]+","+userId+",0,dept_name,"+comId+" from departments where dep_id="+deptPurchaseId[i]+";");
		}
		sql.append("end;");
		//log.info(sql.toString());
		this.updateJdbcSql(sql.toString());
		
	}
	/**
	 * 依据用户id删除表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deletePurchaseDepartBuyerByBuyerId(Long buyerId)
			throws BaseException {
		String sql="delete from purchase_depart_buyer where buyer_id="+buyerId;
		this.updateJdbcSql(sql);		
	}
	
}
