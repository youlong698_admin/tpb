package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.PurchaseDepartBuyer;
/** 
 * 类名称：IPurchaseDepartBuyerBiz
 * 创建人：luguanglei 
 * 创建时间：2017-09-14
 */
public interface IPurchaseDepartBuyerBiz {

	/**
	 * 根据主键获得采购人采购部门表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract PurchaseDepartBuyer getPurchaseDepartBuyer(Long id) throws BaseException;

	/**
	 * 添加采购人采购部门信息
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @throws BaseException 
	 */
	abstract void savePurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException;

	/**
	 * 更新采购人采购部门表实例
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @throws BaseException 
	 */
	abstract void updatePurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException;

	/**
	 * 删除采购人采购部门表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deletePurchaseDepartBuyer(String id) throws BaseException;

	/**
	 * 删除采购人采购部门表实例
	 * @param purchaseDepartBuyer 采购人采购部门表实例
	 * @throws BaseException 
	 */
	abstract void deletePurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException;

	/**
	 * 删除采购人采购部门表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deletePurchaseDepartBuyers(String[] id) throws BaseException;

	/**
	 * 获得采购人采购部门表数据集
	 * purchaseDepartBuyer 采购人采购部门表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract PurchaseDepartBuyer getPurchaseDepartBuyerByPurchaseDepartBuyer(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException ;
	
	/**
	 * 获得所有采购人采购部门表数据集
	 * @param purchaseDepartBuyer 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getPurchaseDepartBuyerList(PurchaseDepartBuyer purchaseDepartBuyer) throws BaseException ;
	
	/**
	 * 获得所有采购人采购部门表数据集
	 * @param rollPage 分页对象
	 * @param purchaseDepartBuyer 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getPurchaseDepartBuyerList(RollPage rollPage, PurchaseDepartBuyer purchaseDepartBuyer)
			throws BaseException;

	/**
	 * 依据用户id删除表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deletePurchaseDepartBuyerByBuyerId(Long buyerId) throws BaseException;
	
	/**
	 * 批量保存采购员所管理的采购组织
	 * @param deptPurchaseId
	 * @param userId
	 * @param comId
	 * @throws BaseException
	 */
	abstract void savePurchaseDepartBuyerByDeptPurchaseId(String[] deptPurchaseId,Long userId,Long comId) throws BaseException;
}