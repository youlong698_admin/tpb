package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.Dictionary;

public interface IDictionaryBiz {

	/**
	 * 根据主键获得字典信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Dictionary getTSysDict(Long id) throws BaseException;

	/**
	 * 获得字典信息表实例
	 * @param tSysDict 字典信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract Dictionary getTSysDict(Dictionary tSysDict) throws BaseException;

	/**
	 * 添加字典信息信息
	 * @param tSysDict 字典信息表实例
	 * @throws BaseException 
	 */
	abstract void saveTSysDict(Dictionary tSysDict) throws BaseException;

	/**
	 * 更新字典信息表实例
	 * @param tSysDict 字典信息表实例
	 * @throws BaseException 
	 */
	abstract void updateTSysDict(Dictionary tSysDict) throws BaseException;

	/**
	 * 删除字典信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTSysDict(String id) throws BaseException;

	/**
	 * 删除字典信息表实例
	 * @param tSysDict 字典信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteTSysDict(Dictionary tSysDict) throws BaseException;

	/**
	 * 删除字典信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTSysDicts(String[] id) throws BaseException;

	/**
	 * 获得所有字典信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTSysDictList(RollPage rollPage) throws BaseException;

	/**
	 * 获得所有字典信息表数据集
	 * @param tSysDict 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTSysDictList(Dictionary tSysDict) throws BaseException;

	/**
	 * 获得所有字典信息表数据集
	 * @param rollPage 分页对象
	 * @param tSysDict 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTSysDictList(RollPage rollPage, Dictionary tSysDict)
			throws BaseException;

}