package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysRole;

public interface ISysRoleBiz {

	/**
	 * 根据主键获得角色权限表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysRole getSysRole(Long id) throws BaseException;

	/**
	 * 添加角色权限信息
	 * @param demo 角色权限表实例
	 * @throws BaseException 
	 */
	abstract void saveSysRole(SysRole sysRole) throws BaseException;

	/**
	 * 更新角色权限表实例
	 * @param tSysRoleRightInfo 角色权限表实例
	 * @throws BaseException 
	 */
	abstract void updateSysRole(SysRole sysRole) throws BaseException;

	/**
	 * 删除角色权限表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysRole(String id) throws BaseException;

	/**
	 * 删除角色权限表实例
	 * @param tSysRoleRightInfo 角色权限表实例
	 * @throws BaseException 
	 */
	abstract void deleteSysRole(SysRole sysRole) throws BaseException;

	/**
	 * 删除角色权限表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysRole(String[] id) throws BaseException;

	/**
	 * 获得所有角色权限表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysRoleList( RollPage rollPage  ) throws BaseException ;
	
	/**
	 * 获得所有角色权限表数据集
	 * @param tSysRoleRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysRoleList(  SysRole sysRole ) throws BaseException ;
	
	/**
	 * 获得所有角色权限表数据集
	 * @param rollPage 分页对象
	 * @param tSysRoleRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysRoleList(RollPage rollPage, SysRole sysRole)
			throws BaseException;
	
}
