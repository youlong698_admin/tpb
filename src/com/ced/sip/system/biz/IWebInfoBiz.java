package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.WebInfo;
/** 
 * 类名称：IWebInfoBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public interface IWebInfoBiz {

	/**
	 * 根据主键获得网站信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract WebInfo getWebInfo(Long id) throws BaseException;

	/**
	 * 添加网站信息信息
	 * @param webInfo 网站信息表实例
	 * @throws BaseException 
	 */
	abstract void saveWebInfo(WebInfo webInfo) throws BaseException;

	/**
	 * 更新网站信息表实例
	 * @param webInfo 网站信息表实例
	 * @throws BaseException 
	 */
	abstract void updateWebInfo(WebInfo webInfo) throws BaseException;

	/**
	 * 删除网站信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteWebInfo(String id) throws BaseException;

	/**
	 * 删除网站信息表实例
	 * @param webInfo 网站信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteWebInfo(WebInfo webInfo) throws BaseException;

	/**
	 * 删除网站信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteWebInfos(String[] id) throws BaseException;

	/**
	 * 获得网站信息表数据集
	 * webInfo 网站信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract WebInfo getWebInfoByWebInfo(WebInfo webInfo) throws BaseException ;
	
	/**
	 * 获得所有网站信息表数据集
	 * @param webInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWebInfoList(WebInfo webInfo) throws BaseException ;
	
	/**
	 * 获得所有网站信息表数据集
	 * @param rollPage 分页对象
	 * @param webInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWebInfoList(RollPage rollPage, WebInfo webInfo)
			throws BaseException;
	/**
	 * 获得所有网站信息表数据集   web网站
	 * @param rollPage 分页对象
	 * @param webInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWebInfoListWeb(RollPage rollPage, WebInfo webInfo)
			throws BaseException;

	/**
	 * 获得所有网站信息表数据集  重要通知公告
	 * @param rollPage 分页对象
	 * @param webInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWebInfoListWebNotice(RollPage rollPage, WebInfo webInfo)
			throws BaseException;

}