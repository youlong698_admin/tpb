package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidItem;
import com.ced.sip.system.entity.BidItem;

public interface IBidItemBiz {

	/**
	 * 根据主键获得评标项表实例
	 * @param id 主键
	 * @author  2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract BidItem getBidItem(Long id) throws BaseException;

	/**
	 * 获得评标项表实例
	 * @param bidItem 评标项表实例
	 * @author  2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract BidItem getBidItem(BidItem bidItem) throws BaseException;

	/**
	 * 添加评标项信息
	 * @param bidItem 评标项表实例
	 * @author  2016-08-28
	 * @throws BaseException 
	 */
	public abstract void saveBidItem(BidItem bidItem) throws BaseException;

	/**
	 * 更新评标项表实例
	 * @param bidItem 评标项表实例
	 * @author  2016-08-28
	 * @throws BaseException 
	 */
	public abstract void updateBidItem(BidItem bidItem) throws BaseException;

	/**
	 * 删除评标项表实例
	 * @param id 主键数组
	 * @author  2016-08-28
	 * @throws BaseException 
	 */
	public abstract void deleteBidItem(String id) throws BaseException;

	/**
	 * 删除评标项表实例
	 * @param bidItem 评标项表实例
	 * @author  2016-08-28
	 * @throws BaseException 
	 */
	public abstract void deleteBidItem(BidItem bidItem) throws BaseException;

	/**
	 * 删除评标项表实例
	 * @param id 主键数组
	 * @author  2016-08-28
	 * @throws BaseException 
	 */
	public abstract void deleteBidItems(String[] id) throws BaseException;

	/**
	 * 获得所有评标项表数据集
	 * @param rollPage 分页对象
	 * @author  2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getBidItemList(RollPage rollPage) throws BaseException;

	/**
	 * 获得所有评标项表数据集
	 * @param BidItem 查询参数对象
	 * @author zzn 2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getBidItemList(BidItem BidItem) throws BaseException;

	/**
	 * 获得所有评标项表数据集
	 * @param rollPage 分页对象
	 * @param bidItem 查询参数对象
	 * @author  2016-08-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getBidItemList(RollPage rollPage, BidItem bidItem)
			throws BaseException;
	/**
	 * 获得所有评标项表数据集
	 * @param rollPage 分页对象
	 * @param bidItem 查询参数对象
	 * @author  2014-08-28
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getBidItemList(RollPage rollPage, BidItem bidItem,TenderBidItem tenderBidItem)
			throws BaseException;

}