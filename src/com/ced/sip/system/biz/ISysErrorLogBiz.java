package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.SysErrorLog;

public interface ISysErrorLogBiz {

	/**
	 * 根据主键获得错误日志实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SysErrorLog getSysErrorLog(Long id) throws BaseException;

	/**
	 * 添加角色权限信息
	 * @param demo 错误日志实例
	 * @throws BaseException 
	 */
	abstract void saveSysErrorLog(SysErrorLog SysErrorLog) throws BaseException;

	/**
	 * 更新错误日志实例
	 * @param tTSysErrorLogRightInfo 错误日志实例
	 * @throws BaseException 
	 */
	abstract void updateSysErrorLog(SysErrorLog SysErrorLog) throws BaseException;

	/**
	 * 删除错误日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysErrorLog(String id) throws BaseException;

	/**
	 * 删除错误日志实例
	 * @param tTSysErrorLogRightInfo 错误日志实例
	 * @throws BaseException 
	 */
	abstract void deleteSysErrorLog(SysErrorLog SysErrorLog) throws BaseException;

	/**
	 * 删除错误日志实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSysErrorLog(String[] id) throws BaseException;

	/**
	 * 获得所有错误日志数据集
	 * @param tTSysErrorLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysErrorLogList(  SysErrorLog SysErrorLog ) throws BaseException ;
	
	/**
	 * 获得所有错误日志数据集
	 * @param rollPage 分页对象
	 * @param tTSysErrorLogRightInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSysErrorLogList(RollPage rollPage, SysErrorLog SysErrorLog)
			throws BaseException;
	
}
