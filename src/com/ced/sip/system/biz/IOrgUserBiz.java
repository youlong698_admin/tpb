package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.OrgUser;
import com.ced.sip.system.entity.Users;

public interface  IOrgUserBiz {
	/**
	 * 根据主键获得群组用户表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract OrgUser getOrguser(Long id) throws BaseException;
	/**
	 * 添加群组用户信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组用户表实例
	 * @throws BaseException 
	 */
	abstract void saveOrguser(OrgUser orguser) throws BaseException;
	/**
	 * 修改群组用户信息
	 * @author xcb 2016-10-13
	 * @param orguser 群组用户表实例
	 * @throws BaseException 
	 */
	abstract void updateOrguser(OrgUser orguser) throws BaseException;
	
	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrguser(String id) throws BaseException;

	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param orguser 群组用户表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrguser(OrgUser orguser) throws BaseException;

	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrgusers(String[] id) throws BaseException;
	/**
	 * 删除群组用户表实例
	 * @author xcb 2016-10-13
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrgusers(List list) throws BaseException;
	/**
	 * 获得所有群组用户表数据集
	 * @author xcb 2016-10-13
	 * @param orguser 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<OrgUser> getOrgusersList(OrgUser orguser) throws BaseException;

	/**
	 * 获得所有群组用户表数据集
	 * @author xcb 2016-10-13
	 * @param orguser 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<OrgUser> getOrgusersList( RollPage rollPage,OrgUser orguser) throws BaseException;

	
}
