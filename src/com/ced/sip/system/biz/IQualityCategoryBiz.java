package com.ced.sip.system.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.QualityCategory;
/** 
 * 类名称：IQualityCategoryBiz
 * 创建人：luguanglei 
 * 创建时间：2017-03-07
 */
public interface IQualityCategoryBiz {

	/**
	 * 根据主键获得资质文件类别表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract QualityCategory getQualityCategory(Long id) throws BaseException;

	/**
	 * 添加资质文件类别信息
	 * @param qualityCategory 资质文件类别表实例
	 * @throws BaseException 
	 */
	abstract void saveQualityCategory(QualityCategory qualityCategory) throws BaseException;

	/**
	 * 更新资质文件类别表实例
	 * @param qualityCategory 资质文件类别表实例
	 * @throws BaseException 
	 */
	abstract void updateQualityCategory(QualityCategory qualityCategory) throws BaseException;

	/**
	 * 删除资质文件类别表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteQualityCategory(String id) throws BaseException;

	/**
	 * 删除资质文件类别表实例
	 * @param qualityCategory 资质文件类别表实例
	 * @throws BaseException 
	 */
	abstract void deleteQualityCategory(QualityCategory qualityCategory) throws BaseException;

	/**
	 * 删除资质文件类别表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteQualityCategorys(String[] id) throws BaseException;

	/**
	 * 获得所有资质文件类别表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getQualityCategoryList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有资质文件类别表数据集
	 * @param qualityCategory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getQualityCategoryList(QualityCategory qualityCategory) throws BaseException ;
	
	/**
	 * 获得所有资质文件类别表数据集
	 * @param rollPage 分页对象
	 * @param qualityCategory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getQualityCategoryList(RollPage rollPage, QualityCategory qualityCategory)
			throws BaseException;

}