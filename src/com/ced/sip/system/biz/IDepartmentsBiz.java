package com.ced.sip.system.biz;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;


import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Users;

public interface IDepartmentsBiz {

	/**
	 * 根据主键获得部门表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Departments getDepartments(Long id) throws BaseException;
	
	/**
	 * 根据名称获得部门表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Departments getDepartmentsByName(String deptName) throws BaseException;

	/**
	 * 添加部门表信息
	 * @param Departments 部门表实例
	 * @throws BaseException 
	 */
	abstract void saveDepartments(Departments departments) throws BaseException;

	/**
	 * 更新部门表实例
	 * @param departments 部门表实例
	 * @throws BaseException 
	 */
	abstract void updateDepartments(Departments departments)
			throws BaseException;

	/**
	 * 更新部门表实例-修改本级和本级所有子节点为不可用
	 * @param departments 部门表实例
	 * @throws BaseException 
	 */
	abstract void updateDepartmentsBySelflevCode(  Departments departments  )
		throws BaseException;
	
	/**
	 * 删除部门表实例
	 * @param id 主键
	 * @throws BaseException 
	 */
	abstract void deleteDepartments(Long id) throws BaseException;
		
	/**
	 * 删除部门表实例
	 * @param departments 部门表实例
	 * @throws BaseException 
	 */
	abstract void deleteDepartments(Departments departments)
			throws BaseException;

	/**
	 * 删除部门表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteDemos(String[] id) throws BaseException;

	/**
	 * 获得所有部门数量
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	public int getDepartmentsCount( Departments departments ) throws BaseException ;
	
	/**
	 * 获得某些指定部门数据集
	 * @Cacheable(cacheName="departCache") 
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	
	public List<Departments> getSomeDepartmentsList( String deptNames ) throws BaseException;
	
	/**
	 * 获得所有部门数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Departments> getDepartmentsList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有部门数据集
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Departments> getDepartmentsList(Departments departments)
			throws BaseException;

	/**
	 * 获得所有部门数据集
	 * @param rollPage 分页对象
	 * @param departments 部门对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Departments> getDepartmentsList(RollPage rollPage,
			Departments departments) throws BaseException;
	

	/**
	 * 获得所有部门数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<Departments> getTreeDepartmentsList() throws BaseException ;
	/**
	 * 获得所有人员表数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<Users> getUsersList( RollPage rollPage) throws BaseException ;
	/**
	 * 获得根据部门编号数据集
	 * @param 
	 * @param 
	 * @return List对象
	 * @throws BaseException 
	 */
	public List<Departments> getDepartmentsCodeList(Departments departments ) throws BaseException ;
	/**
	 * 获得上级部门编号数据集，如果有下级部门，上级部门不允许删除
	 * @return
	 * @throws BaseException 
	 */
	public List<Departments> getDepartmentsupDepIdList(Departments departments ) throws BaseException;
	
	/**
	 * 获得所有部门人员视图
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAllDeptRoot() throws BaseException ;
	

	/**
	 * 根据当前登录人查询群组权限
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 */
	abstract List getDepIdsByUserId(String userName) throws BaseException;
	
	
	/**
	 * 根据当前登录人查询table显示的数据权限
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 */
	abstract List getDepartsToUserId(Long userId) throws BaseException;
	/**
	 * 加签获得部门经理数据集
	 * @author lgl 2016-8-17
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSignUsersList() throws BaseException;

	/**
	 * 加签获得分管领导数据集
	 * @author lgl 2016-8-17
	 * @param user 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getComSignUsersList() throws BaseException;
	
	/**
	 *  根据当前登录人查询table显示部门领导
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 */
	abstract List getDepToUserId(Long userId) throws BaseException;
	/**
	 * 获得所有该部门下权限采购组的数据集
	 * @param deptId 部门ID
	 * @return
	 * @throws BaseException 
	 */
	abstract List<Departments> getPurchaseDepartmentsListByDeptId(Long deptId)
			throws BaseException;
	/**
	 * 根据层次级别码和层次级别获取组织机构对象
	 * @param selflevCode
	 * @param deptLevel
	 * @return
	 * @throws BaseException
	 */
	abstract Departments getDepartmentsBySelflevCode(String selflevCode,Long deptLevel)
			throws BaseException;
	
}