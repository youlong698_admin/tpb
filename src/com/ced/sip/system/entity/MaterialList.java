package com.ced.sip.system.entity;

/**
 * MaterialList entity. @author MyEclipse Persistence Tools
 */

public class MaterialList implements java.io.Serializable {

	// Fields

	private Long materialId;
	private String mkKindName;
	private Long mkKindId;
	private String materialCode;
	private String mnemonicCode;
	private String materialName;
	private String materialType;
	private String unit;
	private Double price;
	private String remark;
	private String remark1;
	private String remark2;
	private String remark3;
	private String isUsable;
	//临时存放物资编码
	private String materCode;
	private Long comId;
	// Constructors

	/** default constructor */
	public MaterialList() {
	}

	// Property accessors

	public Long getMaterialId() {
		return this.materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public String getMaterialCode() {
		return this.materialCode;
	}

	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}

	public String getMaterialName() {
		return this.materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getMaterialType() {
		return this.materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark1() {
		return this.remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return this.remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return this.remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public String getIsUsable() {
		return isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getMaterCode() {
		return materCode;
	}

	public void setMaterCode(String materCode) {
		this.materCode = materCode;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	public String getMkKindName() {
		return mkKindName;
	}

	public void setMkKindName(String mkKindName) {
		this.mkKindName = mkKindName;
	}

	public Long getMkKindId() {
		return mkKindId;
	}

	public void setMkKindId(Long mkKindId) {
		this.mkKindId = mkKindId;
	}

	public String getMnemonicCode() {
		return mnemonicCode;
	}

	public void setMnemonicCode(String mnemonicCode) {
		this.mnemonicCode = mnemonicCode;
	}

}