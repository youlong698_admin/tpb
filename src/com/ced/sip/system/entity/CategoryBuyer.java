package com.ced.sip.system.entity;

/** 
 * 类名称：CategoryBuyer
 * 创建人：luguanglei 
 * 创建时间：2017-03-19
 */
public class CategoryBuyer implements java.io.Serializable {

	// 属性信息
	private Long cbId;     //CB_ID
	private Long mkThreeId;     //小类ID
	private String mkThreeCode;     //小类Code
	private String mkThreeName;     //小类Name
	private Long buyerId;     //采购人ID
	private String isUsable;	 //是否有效
	private String remark;	 //备注
	private Long comId;
	
	
	public CategoryBuyer() {
		super();
	}
	  
	public Long getCbId() {
		return cbId;
	}

	public void setCbId(Long cbId) {
		this.cbId = cbId;
	}

	public Long getMkThreeId(){
	   return  mkThreeId;
	} 
	public void setMkThreeId(Long mkThreeId) {
	   this.mkThreeId = mkThreeId;
    }     
	public Long getBuyerId(){
	   return  buyerId;
	} 
	public void setBuyerId(Long buyerId) {
	   this.buyerId = buyerId;
    }     
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getMkThreeCode() {
		return mkThreeCode;
	}
	public void setMkThreeCode(String mkThreeCode) {
		this.mkThreeCode = mkThreeCode;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	public String getMkThreeName() {
		return mkThreeName;
	}

	public void setMkThreeName(String mkThreeName) {
		this.mkThreeName = mkThreeName;
	}
	
}