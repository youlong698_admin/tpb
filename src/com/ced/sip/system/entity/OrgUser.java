package com.ced.sip.system.entity;

/**
 * OrgUser entity. @author MyEclipse Persistence Tools
 */

public class OrgUser implements java.io.Serializable {

	// Fields

	private Long ouId;
	private Long orgId;
	private String orgCode;
	private String orgName;
	private String userNameCn;
	private String userName;
	private Long depId;
	private String isUsable;
	private String orgUserIds;
	private String deptName;
	private String userLoginName;
	private String orgToDepNames;
	private Long comId;
	// Constructors

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}

	/** default constructor */
	public OrgUser() {
	}

	/** full constructor */
	public OrgUser(Long orgId, String orgCode, String orgName, String userNameCn,
			String userName, Long depId, String isUsable, String orgToDepNames) {
		this.orgId = orgId;
		this.orgCode = orgCode;
		this.orgName = orgName;
		this.userNameCn = userNameCn;
		this.userName = userName;
		this.depId = depId;
		this.isUsable = isUsable;
		this.orgToDepNames = orgToDepNames;
	}

	// Property accessors

	public Long getOuId() {
		return this.ouId;
	}

	public void setOuId(Long ouId) {
		this.ouId = ouId;
	}

	public Long getOrgId() {
		return this.orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgCode() {
		return this.orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return this.orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getUserNameCn() {
		return this.userNameCn;
	}

	public void setUserNameCn(String userNameCn) {
		this.userNameCn = userNameCn;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getDepId() {
		return this.depId;
	}

	public void setDepId(Long depId) {
		this.depId = depId;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getOrgUserIds() {
		return orgUserIds;
	}

	public void setOrgUserIds(String orgUserIds) {
		this.orgUserIds = orgUserIds;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getOrgToDepNames() {
		return orgToDepNames;
	}

	public void setOrgToDepNames(String orgToDepNames) {
		this.orgToDepNames = orgToDepNames;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}
	
}