package com.ced.sip.system.entity;

import java.io.Serializable;
import java.util.Date;

public class SysUserRoleRela implements Serializable {
	private static final long serialVersionUID = 1L;
    
    public Long relaId; //关系标识
    public Long roleId; //角色标识
    public Long userId; //操作员标识
    public String writer;     //创建人员
    public Date writeDate; //创建时间
    public Long getRelaId() {
        return relaId;
    }
    public void setRelaId(Long relaId) {
        this.relaId = relaId;
    }
    public Long getRoleId() {
        return roleId;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public Date getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}
    
}
