package com.ced.sip.system.entity;

/** 
 * 类名称：PurchaseDepartBuyer
 * 创建人：luguanglei 
 * 创建时间：2017-09-14
 */
public class PurchaseDepartBuyer implements java.io.Serializable {

	// 属性信息
	private Long pdbId;     //CBID
	private Long deptId;     //小类ID
	private Long buyerId;     //采购人ID
	private String isUsable;	 //是否有效
	private String remark;	 //备注
	private String deptName;	 //小类CODE
	private Long comId;     //
	
	
	public PurchaseDepartBuyer() {
		super();
	}   
	public Long getPdbId() {
		return pdbId;
	}
	public void setPdbId(Long pdbId) {
		this.pdbId = pdbId;
	}
	public Long getDeptId(){
	   return  deptId;
	} 
	public void setDeptId(Long deptId) {
	   this.deptId = deptId;
    }     
	public Long getBuyerId(){
	   return  buyerId;
	} 
	public void setBuyerId(Long buyerId) {
	   this.buyerId = buyerId;
    }     
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getDeptName(){
	   return  deptName;
	} 
	public void setDeptName(String deptName) {
	   this.deptName = deptName;
    }
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }     
}