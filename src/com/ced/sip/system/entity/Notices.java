package com.ced.sip.system.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/**
 * Notices entity. @author MyEclipse Persistence Tools
 */

public class Notices extends BaseObject implements java.io.Serializable {

	// Fields

	private Long noticeId;
	private String title;
	private String content;
	private String keyword;
	private String publisher;
	private Date publishDate;
	private String remark;
	private String isUsable;
	private int type;
	private Long comId;
	private String compName;

	// Constructors

	/** default constructor */
	public Notices() {
	}

	/** full constructor */
	public Notices(String title, String content, String keyword,
			String publisher, Date publishDate,
			String remark,String isUsable) {
		this.title = title;
		this.content = content;
		this.keyword = keyword;
		this.publisher = publisher;
		this.publishDate = publishDate;
		this.remark = remark;
		this.isUsable = isUsable;
	}

	// Property accessors

	public Long getNoticeId() {
		return this.noticeId;
	}

	public void setNoticeId(Long noticeId) {
		this.noticeId = noticeId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getKeyword() {
		return this.keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}


	public String getPublisher() {
		return this.publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Date getPublishDate() {
		return this.publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsUsable() {
		return isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}
	
}