package com.ced.sip.system.entity;

/**
 * OrgRights entity. @author MyEclipse Persistence Tools
 */

public class OrgRights implements java.io.Serializable {

	// Fields

	private Long orgRightId;
	private Long orgId;
	private Long depId;
	private Long comId;

	// Constructors

	/** default constructor */
	public OrgRights() {
	}

	/** minimal constructor */
	public OrgRights(Long orgRightId, Long depId) {
		this.orgRightId = orgRightId;
		this.depId = depId;
	}

	/** full constructor */
	public OrgRights(Long orgRightId, Long orgId, Long depId) {
		this.orgRightId = orgRightId;
		this.orgId = orgId;
		this.depId = depId;
	}

	// Property accessors

	public Long getOrgRightId() {
		return this.orgRightId;
	}

	public void setOrgRightId(Long orgRightId) {
		this.orgRightId = orgRightId;
	}

	public Long getOrgId() {
		return this.orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getDepId() {
		return this.depId;
	}

	public void setDepId(Long depId) {
		this.depId = depId;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

}