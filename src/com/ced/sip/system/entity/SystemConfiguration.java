package com.ced.sip.system.entity;

/** 
 * 类名称：SystemConfiguration
 * 创建人：luguanglei 
 * 创建时间：2017-03-30
 */
public class SystemConfiguration implements java.io.Serializable {
	private String requiredMaterialWorkflow;	 //需求计划审批是否开启
	private String mailFalg;	 //邮件是否开启
	private String messageFalg;	 //短信是否开启
	private Long scId;     //主键
	private String requiredMaterialPrefix; //计划编号前缀
	private String requiredCollectPrefix;//采购立项编号前缀
	private String systemCurrentDept;//当前使用单位
	private String requiredcollect00Workflow;	 //招标立项项目审批是否开启
	private String requiredcollect01Workflow;	 //询价立项项目审批是否开启
	private String requiredcollect02Workflow;	 //竞价立项项目审批是否开启
	private String bidpurchaseresult00Workflow;	 //招标结果项目审批是否开启
	private String bidpurchaseresult01Workflow;	 //询价结果项目审批是否开启
	private String bidpurchaseresult02Workflow;	 //竞价结果项目审批是否开启
	private String contractWorkflow;	 //合同审批是否开启
	private String orderWorkflow;	 //订单审批是否开启
	private String capitalPlanWorkflow;	 //资金计划审批是否开启
	private String contractPrefix;//合同编号前缀
	private String orderPrefix;//订单编号前缀
	private String capitalPlanPrefix;//资金计划编号前缀

	private Long comId;
	
	
	public SystemConfiguration() {
		super();
	}
	public String getRequiredMaterialWorkflow(){
	   return  requiredMaterialWorkflow;
	} 
	public void setRequiredMaterialWorkflow(String requiredMaterialWorkflow) {
	   this.requiredMaterialWorkflow = requiredMaterialWorkflow;
    }
	public String getMailFalg(){
	   return  mailFalg;
	} 
	public void setMailFalg(String mailFalg) {
	   this.mailFalg = mailFalg;
    }
	public String getMessageFalg() {
		return messageFalg;
	}
	public void setMessageFalg(String messageFalg) {
		this.messageFalg = messageFalg;
	}
	public Long getScId(){
	   return  scId;
	} 
	public void setScId(Long scId) {
	   this.scId = scId;
    }
	public String getRequiredMaterialPrefix() {
		return requiredMaterialPrefix;
	}
	public void setRequiredMaterialPrefix(String requiredMaterialPrefix) {
		this.requiredMaterialPrefix = requiredMaterialPrefix;
	}
	public String getRequiredCollectPrefix() {
		return requiredCollectPrefix;
	}
	public void setRequiredCollectPrefix(String requiredCollectPrefix) {
		this.requiredCollectPrefix = requiredCollectPrefix;
	}

	public String getSystemCurrentDept() {
		return systemCurrentDept;
	}

	public void setSystemCurrentDept(String systemCurrentDept) {
		this.systemCurrentDept = systemCurrentDept;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public String getRequiredcollect00Workflow() {
		return requiredcollect00Workflow;
	}
	public void setRequiredcollect00Workflow(String requiredcollect00Workflow) {
		this.requiredcollect00Workflow = requiredcollect00Workflow;
	}
	public String getRequiredcollect01Workflow() {
		return requiredcollect01Workflow;
	}
	public void setRequiredcollect01Workflow(String requiredcollect01Workflow) {
		this.requiredcollect01Workflow = requiredcollect01Workflow;
	}
	public String getRequiredcollect02Workflow() {
		return requiredcollect02Workflow;
	}
	public void setRequiredcollect02Workflow(String requiredcollect02Workflow) {
		this.requiredcollect02Workflow = requiredcollect02Workflow;
	}
	public String getBidpurchaseresult00Workflow() {
		return bidpurchaseresult00Workflow;
	}
	public void setBidpurchaseresult00Workflow(String bidpurchaseresult00Workflow) {
		this.bidpurchaseresult00Workflow = bidpurchaseresult00Workflow;
	}
	public String getBidpurchaseresult01Workflow() {
		return bidpurchaseresult01Workflow;
	}
	public void setBidpurchaseresult01Workflow(String bidpurchaseresult01Workflow) {
		this.bidpurchaseresult01Workflow = bidpurchaseresult01Workflow;
	}
	public String getBidpurchaseresult02Workflow() {
		return bidpurchaseresult02Workflow;
	}
	public void setBidpurchaseresult02Workflow(String bidpurchaseresult02Workflow) {
		this.bidpurchaseresult02Workflow = bidpurchaseresult02Workflow;
	}
	public String getContractWorkflow() {
		return contractWorkflow;
	}
	public void setContractWorkflow(String contractWorkflow) {
		this.contractWorkflow = contractWorkflow;
	}
	public String getOrderWorkflow() {
		return orderWorkflow;
	}
	public void setOrderWorkflow(String orderWorkflow) {
		this.orderWorkflow = orderWorkflow;
	}
	public String getContractPrefix() {
		return contractPrefix;
	}
	public void setContractPrefix(String contractPrefix) {
		this.contractPrefix = contractPrefix;
	}
	public String getOrderPrefix() {
		return orderPrefix;
	}
	public void setOrderPrefix(String orderPrefix) {
		this.orderPrefix = orderPrefix;
	}
	public String getCapitalPlanPrefix() {
		return capitalPlanPrefix;
	}
	public void setCapitalPlanPrefix(String capitalPlanPrefix) {
		this.capitalPlanPrefix = capitalPlanPrefix;
	}
	public String getCapitalPlanWorkflow() {
		return capitalPlanWorkflow;
	}
	public void setCapitalPlanWorkflow(String capitalPlanWorkflow) {
		this.capitalPlanWorkflow = capitalPlanWorkflow;
	}   	
}