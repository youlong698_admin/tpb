package com.ced.sip.system.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：WebInfo
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class WebInfo extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long wiId;     //主键
	private Long wmId;     //栏目ID
	private String title;	 //标题
	private String content;	 //内容
	private String contentShort;	 //内容
	private String publisher;	 //发布人
	private Date publishDate;    //发布日期
	private String remark;	 //备注
	private int status;	 //状态
	private Long writer;     //编制人
	private Date writeDate;    //编制时间
	private String logo;
	private int isIndex;
	private String keyword;
	private int readCount; //阅读量
	
	private String wmName;
	
	
	public WebInfo() {
		super();
	}
	
	public Long getWiId(){
	   return  wiId;
	} 
	public void setWiId(Long wiId) {
	   this.wiId = wiId;
    }     
	public Long getWmId(){
	   return  wmId;
	} 
	public void setWmId(Long wmId) {
	   this.wmId = wmId;
    }     
	public String getTitle(){
	   return  title;
	} 
	public void setTitle(String title) {
	   this.title = title;
    }
	public String getContent(){
	   return  content;
	} 
	public void setContent(String content) {
	   this.content = content;
    }
	public String getPublisher(){
	   return  publisher;
	} 
	public void setPublisher(String publisher) {
	   this.publisher = publisher;
    }
	public Date getPublishDate(){
	   return  publishDate;
	} 
	public void setPublishDate(Date publishDate) {
	   this.publishDate = publishDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public int getStatus(){
	   return  status;
	} 
	public void setStatus(int status) {
	   this.status = status;
    }
	public Long getWriter(){
	   return  writer;
	} 
	public void setWriter(Long writer) {
	   this.writer = writer;
    }     
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }

	public String getWmName() {
		return wmName;
	}

	public void setWmName(String wmName) {
		this.wmName = wmName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public int getIsIndex() {
		return isIndex;
	}

	public void setIsIndex(int isIndex) {
		this.isIndex = isIndex;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getReadCount() {
		return readCount;
	}

	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}
	public String getContentShort() {
		return contentShort;
	}

	public void setContentShort(String contentShort) {
		this.contentShort = contentShort;
	}	    
}