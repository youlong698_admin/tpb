package com.ced.sip.system.entity;

import java.io.Serializable;
import java.util.Date;

public class SysRole implements Serializable {

private static final long serialVersionUID = 1L;
    
private Long roleId;     //角色标识
    private String roleNo;     //角色编码
    private String roleName;   //角色名称
    private String writer;     //创建人员
    private Date writeDate; //创建时间
    private String isActive;   //可用标识
    private String roleDesc;   //角色描述
    private String compName; //采购单位名称
	private Long comId;
    
    public Long getRoleId() {
        return roleId;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public String getRoleNo() {
        return roleNo;
    }
    public void setRoleNo(String roleNo) {
        this.roleNo = roleNo;
    }
    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public Date getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}
	public String getIsActive() {
        return isActive;
    }
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
    public String getRoleDesc() {
        return roleDesc;
    }
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}  
}
