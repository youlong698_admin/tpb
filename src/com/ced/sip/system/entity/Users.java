package com.ced.sip.system.entity;

import java.util.Date;

/**
 * Users entity. @author MyEclipse Persistence Tools
 */

public class Users implements java.io.Serializable {

	// Fields

	private Long userId;
	private String userName;
	private String userPassword;
	private String userChinesename;
	private Long depId;
	private String depName;//部门名称
	private String station;
	private String depIdCn;//部门中文
	private String rank;
	private String email;//邮箱
	private String writer;
	private Date writeDate;
	private String selflevCode;
	private String isUsable;
	private String phoneNum;
	private String telNum;
	private String remark;
	private Long comId;

	
	
	
	//临时变量 
    private String oldPassWord;
    private String newPassWord;
    private String confirmPassWord;
    private String deptName;
    
    private String j_username;
    private String j_password;
    
    private String userIds;
    private int userType;//用户方式  0 普通用户    1中层领导    2高层领导
	// Constructors

	/** default constructor */
	public Users() {
	}

	/** minimal constructor */
	public Users(String userName, String userPassword, String userChinesename) {
		this.userName = userName;
		this.userPassword = userPassword;
		this.userChinesename = userChinesename;
	}
	public Users(String userName, String userChinesename) {
		this.userName = userName;
		this.userChinesename = userChinesename;
	}
	/** full constructor */
	public Users(String userName, String userPassword, String userChinesename,
			Long depId, String station, String rank,
			String email, String writer,
			Date writeDate, String selflevCode, String isUsable,
			String phoneNum, String remark) {
		this.userName = userName;
		this.userPassword = userPassword;
		this.userChinesename = userChinesename;
		this.depId = depId;
		this.station = station;
		this.rank = rank;
		this.email = email;
		this.writer = writer;
		this.writeDate = writeDate;
		this.selflevCode = selflevCode;
		this.isUsable = isUsable;
		this.phoneNum = phoneNum;
		this.remark = remark;
	}

	// Property accessors

	public Long getUserId() {
		return this.userId;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserChinesename() {
		return this.userChinesename;
	}

	public void setUserChinesename(String userChinesename) {
		this.userChinesename = userChinesename;
	}
	public Long getDepId() {
		return this.depId;
	}

	public void setDepId(Long depId) {
		this.depId = depId;
	}

	public String getStation() {
		return this.station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getRank() {
		return this.rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getSelflevCode() {
		return this.selflevCode;
	}

	public void setSelflevCode(String selflevCode) {
		this.selflevCode = selflevCode;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOldPassWord() {
		return oldPassWord;
	}

	public void setOldPassWord(String oldPassWord) {
		this.oldPassWord = oldPassWord;
	}

	public String getNewPassWord() {
		return newPassWord;
	}

	public void setNewPassWord(String newPassWord) {
		this.newPassWord = newPassWord;
	}

	public String getConfirmPassWord() {
		return confirmPassWord;
	}

	public void setConfirmPassWord(String confirmPassWord) {
		this.confirmPassWord = confirmPassWord;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getJ_username() {
		return j_username;
	}

	public void setJ_username(String jUsername) {
		j_username = jUsername;
	}

	public String getJ_password() {
		return j_password;
	}

	public void setJ_password(String jPassword) {
		j_password = jPassword;
	}

	public String getTelNum() {
		return telNum;
	}

	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}
	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getDepIdCn() {
		return depIdCn;
	}

	public void setDepIdCn(String depIdCn) {
		this.depIdCn = depIdCn;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

}