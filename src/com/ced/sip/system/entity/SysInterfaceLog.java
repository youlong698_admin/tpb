package com.ced.sip.system.entity;

import java.util.Date;

/**
 * TSysInterfaceLog entity. @author MyEclipse Persistence Tools
 */

public class SysInterfaceLog implements java.io.Serializable {

	// Fields

	private Long silId;
	private String systemName;
	private String interfaceName;
	private String sendContent;
	private String receiveContent;
	private String interfaceDate;
	private String writerEn;
	private String writer;
	private Date writeDate;
	private int type;
	private Long comId;

	// Constructors

	/** default constructor */
	public SysInterfaceLog() {
	}

	/** minimal constructor */
	public SysInterfaceLog(Long silId) {
		this.silId = silId;
	}

	/** full constructor */
	public SysInterfaceLog(Long silId, String systemName,
			String interfaceName, String sendContent, String receiveContent,
			String interfaceDate, String writerEn, String writer, Date writeDate) {
		this.silId = silId;
		this.systemName = systemName;
		this.interfaceName = interfaceName;
		this.sendContent = sendContent;
		this.receiveContent = receiveContent;
		this.interfaceDate = interfaceDate;
		this.writerEn = writerEn;
		this.writer = writer;
		this.writeDate = writeDate;
	}

	// Property accessors

	public Long getSilId() {
		return this.silId;
	}

	public void setSilId(Long silId) {
		this.silId = silId;
	}

	public String getSystemName() {
		return this.systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getInterfaceName() {
		return this.interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getSendContent() {
		return this.sendContent;
	}

	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}

	public String getReceiveContent() {
		return this.receiveContent;
	}

	public void setReceiveContent(String receiveContent) {
		this.receiveContent = receiveContent;
	}

	public String getInterfaceDate() {
		return this.interfaceDate;
	}

	public void setInterfaceDate(String interfaceDate) {
		this.interfaceDate = interfaceDate;
	}

	public String getWriterEn() {
		return this.writerEn;
	}

	public void setWriterEn(String writerEn) {
		this.writerEn = writerEn;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}
    
}