package com.ced.sip.system.entity;

import java.util.Date;

/** 
 * 类名称：SysCompany
 * 创建人：luguanglei 
 * 创建时间：2017-05-24
 */
public class SysCompany implements java.io.Serializable {

	// 属性信息
	private Long scId;	//主键ID
	private String compName;	 //采购单位名称
	private String shortName;	 //采购单位简称
	private String logo;	 //LOGO
	private String status;	 //状态
	private String contact;	 //联系人
	private String mobilePhone;	 //联系人手机
	private Date createTime;    //创建时间
	private String demoData;	 //是否是体验账户
	private String compPhone;	 //公司电话
	private String compFax;	 //公司传真
	private String loginName;	 //用户名
	private String password;	 //密码
	private String industryOwned; //所属行业
	private String managementModel;//经营模式
	private String province;//省
	private String city;//市
	private String companyProfile;//企业简介
	private String companyAddress;//企业地址
	private String companyWebsite;//企业网站
	private String registerFunds;
	private Date createDate;
	private String orgCode;
	private String registrationType;//注册代码证类型
	private String scopeBusiness;//经营范围
	private Date costDueTime;
	private int rechargeTimes;
	
	
	public SysCompany() {
		super();
	}
	
	public Long getScId(){
	   return  scId;
	} 
	public void setScId(Long scId) {
	   this.scId = scId;
    }	
	public String getCompName(){
	   return  compName;
	} 
	public void setCompName(String compName) {
	   this.compName = compName;
    }
	public String getShortName(){
	   return  shortName;
	} 
	public void setShortName(String shortName) {
	   this.shortName = shortName;
    }
	public String getLogo(){
	   return  logo;
	} 
	public void setLogo(String logo) {
	   this.logo = logo;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public String getContact(){
	   return  contact;
	} 
	public void setContact(String contact) {
	   this.contact = contact;
    }
	public String getMobilePhone(){
	   return  mobilePhone;
	} 
	public void setMobilePhone(String mobilePhone) {
	   this.mobilePhone = mobilePhone;
    }
	public Date getCreateTime(){
	   return  createTime;
	} 
	public void setCreateTime(Date createTime) {
	   this.createTime = createTime;
    }	    
	public String getDemoData(){
	   return  demoData;
	} 
	public void setDemoData(String demoData) {
	   this.demoData = demoData;
    }
	public String getCompPhone(){
	   return  compPhone;
	} 
	public void setCompPhone(String compPhone) {
	   this.compPhone = compPhone;
    }
	public String getCompFax(){
	   return  compFax;
	} 
	public void setCompFax(String compFax) {
	   this.compFax = compFax;
    }
	public String getLoginName(){
	   return  loginName;
	} 
	public void setLoginName(String loginName) {
	   this.loginName = loginName;
    }
	public String getPassword(){
	   return  password;
	} 
	public void setPassword(String password) {
	   this.password = password;
    }

	public String getIndustryOwned() {
		return industryOwned;
	}

	public void setIndustryOwned(String industryOwned) {
		this.industryOwned = industryOwned;
	}

	public String getManagementModel() {
		return managementModel;
	}

	public void setManagementModel(String managementModel) {
		this.managementModel = managementModel;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getRegisterFunds() {
		return registerFunds;
	}

	public void setRegisterFunds(String registerFunds) {
		this.registerFunds = registerFunds;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getRegistrationType() {
		return registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public String getScopeBusiness() {
		return scopeBusiness;
	}

	public void setScopeBusiness(String scopeBusiness) {
		this.scopeBusiness = scopeBusiness;
	}

	public Date getCostDueTime() {
		return costDueTime;
	}

	public void setCostDueTime(Date costDueTime) {
		this.costDueTime = costDueTime;
	}

	public int getRechargeTimes() {
		return rechargeTimes;
	}

	public void setRechargeTimes(int rechargeTimes) {
		this.rechargeTimes = rechargeTimes;
	}
	
}