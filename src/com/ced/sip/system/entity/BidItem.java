package com.ced.sip.system.entity;

import java.util.Date;

/**
 * BidItem entity. @author MyEclipse Persistence Tools
 */

public class BidItem implements java.io.Serializable {

	// Fields

	private Long biId;
	private String itemName;
	private String itemDetail;
	private Double points;
	private Double maxPoints;
	private Double minPoints;
	private String itemType;
	private String writer;
	private Date writeDate;
	private String remark;
	private String isUsable;
	private Long comId;

	// Constructors

	/** default constructor */
	public BidItem() {
	}

	/** full constructor */
	public BidItem(String itemName, String itemDetail, Double points,
			Double maxPoints, Double minPoints, String itemType, String writer,
			Date writeDate, String remark, String isUsable) {
		this.itemName = itemName;
		this.itemDetail = itemDetail;
		this.points = points;
		this.maxPoints = maxPoints;
		this.minPoints = minPoints;
		this.itemType = itemType;
		this.writer = writer;
		this.writeDate = writeDate;
		this.remark = remark;
		this.isUsable = isUsable;
	}

	// Property accessors

	public Long getBiId() {
		return this.biId;
	}

	public void setBiId(Long biId) {
		this.biId = biId;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDetail() {
		return this.itemDetail;
	}

	public void setItemDetail(String itemDetail) {
		this.itemDetail = itemDetail;
	}

	public Double getPoints() {
		return this.points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	public Double getMaxPoints() {
		return this.maxPoints;
	}

	public void setMaxPoints(Double maxPoints) {
		this.maxPoints = maxPoints;
	}

	public Double getMinPoints() {
		return this.minPoints;
	}

	public void setMinPoints(Double minPoints) {
		this.minPoints = minPoints;
	}

	public String getItemType() {
		return this.itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

}