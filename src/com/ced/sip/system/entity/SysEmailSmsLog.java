package com.ced.sip.system.entity;

import java.util.Date;

/** 
 * 类名称：SysEmailSmsLog
 * 创建人：luguanglei 
 * 创建时间：2017-08-10
 */
public class SysEmailSmsLog implements java.io.Serializable {

	// 属性信息
	private Long seslId;	//主键
	private Long type;	//类型 0 短信 1邮件
	private String address;	 //地址
	private String title;	 //标题
	private String content;	 //内容
	private Date sendDate;    //发送时间
	private String sendName;	 //发送人
	private Long comId;     //COM_ID
	private int status;//状态
	private String description;//错误描述
	
	
	public SysEmailSmsLog() {
		super();
	}
	
	public Long getSeslId(){
	   return  seslId;
	} 
	public void setSeslId(Long seslId) {
	   this.seslId = seslId;
    }	
	public Long getType(){
	   return  type;
	} 
	public void setType(Long type) {
	   this.type = type;
    }	
	public String getAddress(){
	   return  address;
	} 
	public void setAddress(String address) {
	   this.address = address;
    }
	public String getTitle(){
	   return  title;
	} 
	public void setTitle(String title) {
	   this.title = title;
    }
	public Date getSendDate(){
	   return  sendDate;
	} 
	public void setSendDate(Date sendDate) {
	   this.sendDate = sendDate;
    }	    
	public String getSendName(){
	   return  sendName;
	} 
	public void setSendName(String sendName) {
	   this.sendName = sendName;
    }
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}     
}