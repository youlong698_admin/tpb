package com.ced.sip.system.entity;

/** 
 * 类名称：QualityCategory
 * 创建人：luguanglei 
 * 创建时间：2017-03-07
 */
public class QualityCategory implements java.io.Serializable {

	// 属性信息
	private Long qcId;     //QCID
	private String code;	 //编码
	private String qualityName;	 //资质名称
	private String isUsable;	 //是否有效
	private String remark;	 //备注
	private Long comId;
	
	
	public QualityCategory() {
		super();
	}
	
	public Long getQcId(){
	   return  qcId;
	} 
	public void setQcId(Long qcId) {
	   this.qcId = qcId;
    }     
	public String getCode(){
	   return  code;
	} 
	public void setCode(String code) {
	   this.code = code;
    }
	public String getQualityName(){
	   return  qualityName;
	} 
	public void setQualityName(String qualityName) {
	   this.qualityName = qualityName;
    }
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}
	
}