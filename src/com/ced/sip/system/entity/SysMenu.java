package com.ced.sip.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.ced.sip.common.model.AuthorityParameter;

public class SysMenu extends AuthorityParameter implements Serializable {

private static final long serialVersionUID = 1L;
	
	private Long menuId;   		//本实体记录的唯一标识
	private String menuCode; 		//菜单编码
	private String menuName;		//菜单名称
	private String pmenuCode;		//直接上级菜单的菜单标识
	private String pMenuName;       //直接上级菜单的菜单名称
	private String menuFolderFlag;	//是否菜单夹，是：菜单夹；否：菜单项
	private String sortNo;			//在同级中的排列顺序的序号，用自然数标识，如，1、2、3
	private String menuImg;			//菜单图标
	private String menuLevel;		//菜单级别
	private String pathCode;		//菜单路径
	private String isActive;		//是否可用标识 0：无效 1：有效
	private String writer;			//菜单添加人员
	private Date writeDate;			//菜单添加时间
	private String remark;			//菜单的备注
	private int type;
	
	public Long getMenuId() {
		return menuId;
	}
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getPmenuCode() {
		return pmenuCode;
	}
	public void setPmenuCode(String pmenuCode) {
		this.pmenuCode = pmenuCode;
	}
	public String getpMenuName() {
		return pMenuName;
	}
	public void setpMenuName(String pMenuName) {
		this.pMenuName = pMenuName;
	}
	public String getMenuFolderFlag() {
		return menuFolderFlag;
	}
	public void setMenuFolderFlag(String menuFolderFlag) {
		this.menuFolderFlag = menuFolderFlag;
	}
	public String getSortNo() {
		return sortNo;
	}
	public void setSortNo(String sortNo) {
		this.sortNo = sortNo;
	}
	public String getMenuLevel() {
		return menuLevel;
	}
	public void setMenuLevel(String menuLevel) {
		this.menuLevel = menuLevel;
	}
	public String getPathCode() {
		return pathCode;
	}
	public void setPathCode(String pathCode) {
		this.pathCode = pathCode;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public Date getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getMenuImg() {
		return menuImg;
	}
	public void setMenuImg(String menuImg) {
		this.menuImg = menuImg;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
