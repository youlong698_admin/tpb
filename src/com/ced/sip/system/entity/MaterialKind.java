package com.ced.sip.system.entity;

import java.util.Date;

/**
 * MaterialKind entity. @author MyEclipse Persistence Tools
 */

public class MaterialKind implements java.io.Serializable {

	// Fields

	private Long mkId;
	private String mkCode;
	private String mkName;
	private Integer displaySort;
	private Long parentId;
	private String isHaveChild;
	private Integer childCount;
	private Integer materialCount;
	private String selflevCode;
	private Integer levels;
	private String writer;
	private Date writeDate;
	private String isUsable;
	private String remark;
	private String materCode;
	private Long comId;
	

	
	private String operType;
	// Constructors

	/** default constructor */
	public MaterialKind() {
	}

	/** full constructor */
	public MaterialKind(String mkCode, String mkName, Integer displaySort,
			Long parentId, String isHaveChild, Integer childCount,
			Integer materialCount, String selflevCode, Integer levels,
			String writer, Date writeDate, String isUsable, String remark) {
		this.mkCode = mkCode;
		this.mkName = mkName;
		this.displaySort = displaySort;
		this.parentId = parentId;
		this.isHaveChild = isHaveChild;
		this.childCount = childCount;
		this.materialCount = materialCount;
		this.selflevCode = selflevCode;
		this.levels = levels;
		this.writer = writer;
		this.writeDate = writeDate;
		this.isUsable = isUsable;
		this.remark = remark;
	}

	// Property accessors

	public Long getMkId() {
		return this.mkId;
	}

	public void setMkId(Long mkId) {
		this.mkId = mkId;
	}

	public String getMkCode() {
		return this.mkCode;
	}

	public void setMkCode(String mkCode) {
		this.mkCode = mkCode;
	}

	public String getMkName() {
		return this.mkName;
	}

	public void setMkName(String mkName) {
		this.mkName = mkName;
	}

	public Integer getDisplaySort() {
		return this.displaySort;
	}

	public void setDisplaySort(Integer displaySort) {
		this.displaySort = displaySort;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getIsHaveChild() {
		return this.isHaveChild;
	}

	public void setIsHaveChild(String isHaveChild) {
		this.isHaveChild = isHaveChild;
	}

	public Integer getChildCount() {
		return this.childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getMaterialCount() {
		return this.materialCount;
	}

	public void setMaterialCount(Integer materialCount) {
		this.materialCount = materialCount;
	}

	public String getSelflevCode() {
		return this.selflevCode;
	}

	public void setSelflevCode(String selflevCode) {
		this.selflevCode = selflevCode;
	}

	public Integer getLevels() {
		return this.levels;
	}

	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public String getMaterCode() {
		return materCode;
	}

	public void setMaterCode(String materCode) {
		this.materCode = materCode;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	

}