package com.ced.sip.system.entity;

import java.util.Date;

/**
 * TSysMessageLog entity. @author MyEclipse Persistence Tools
 */

public class SysMessageLog implements java.io.Serializable {

	// Fields

	private Long smlId;
	private String messageContent;
	private Date sendDate;
	private String receivePersonCn;
	private String receivePerson; //接收人
	private Date lookDate;
	private String status;
	private String messageType;
	private String sendPerson; //发送人
	private String sendPersonCn;
	private int type;
	private Long comId;
	// Constructors

	/** default constructor */
	public SysMessageLog() {
	}

	/** minimal constructor */
	public SysMessageLog(Long smlId) {
		this.smlId = smlId;
	}

	/** full constructor */
	public SysMessageLog(Long smlId, String messageContent, Date sendDate,
			String receivePersonCn, String receivePerson, Date lookDate,
			String status, String messageType) {
		this.smlId = smlId;
		this.messageContent = messageContent;
		this.sendDate = sendDate;
		this.receivePersonCn = receivePersonCn;
		this.receivePerson = receivePerson;
		this.lookDate = lookDate;
		this.status = status;
		this.messageType = messageType;
	}

	// Property accessors

	public Long getSmlId() {
		return this.smlId;
	}

	public void setSmlId(Long smlId) {
		this.smlId = smlId;
	}

	public String getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Date getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getReceivePersonCn() {
		return this.receivePersonCn;
	}

	public void setReceivePersonCn(String receivePersonCn) {
		this.receivePersonCn = receivePersonCn;
	}

	public String getReceivePerson() {
		return this.receivePerson;
	}

	public void setReceivePerson(String receivePerson) {
		this.receivePerson = receivePerson;
	}

	public Date getLookDate() {
		return this.lookDate;
	}

	public void setLookDate(Date lookDate) {
		this.lookDate = lookDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessageType() {
		return this.messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getSendPerson() {
		return sendPerson;
	}

	public void setSendPerson(String sendPerson) {
		this.sendPerson = sendPerson;
	}

	public String getSendPersonCn() {
		return sendPersonCn;
	}

	public void setSendPersonCn(String sendPersonCn) {
		this.sendPersonCn = sendPersonCn;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	
}