package com.ced.sip.system.entity;

import java.util.Date;

/**
 * Departments entity. @author MyEclipse Persistence Tools
 */

public class Departments implements java.io.Serializable {

	// Fields

	private Long depId;
	private String deptCode;
	private String deptName;
	private String deptShortname;
	private String deptDesc;
	private Long deptOrder;
	private Long parentDeptId;
	private String isHaveChild;
	private String selflevCode;
	private String selflevName;
	private String writer;
	private Date writeDate;
	private Long depLeaderId;
	private String depLeaderCn;
	private Long compLeaderId;
	private String compLeaderCn;
	private String remark;
	private String isUsable;
	private Long deptLevel;
	private String isPurchaseDept;
	private Long purchaseDeptId; //集采采购组织
	private Long ownPurchaseDeptId; //自采采购组织
	private Long comId;

	
	
	// 操作信息
	private String operType ;
	
	// 
	private String ouName ;
	
	// Constructors

	/** default constructor */
	public Departments() {
	}
	/** full constructor */
	public Departments( Long depLeaderId,String depLeaderCn) {
		this.depLeaderId = depLeaderId;
		this.depLeaderCn = depLeaderCn;
	}
	/** full constructor */
	public Departments(String deptCode, String deptName, String deptShortname,
			String deptDesc, Long deptOrder,Long parentDeptId, String isHaveChild, 
			String selflevCode,String selflevName,
			String writer, Date writeDate, Long depLeaderId,
			String depLeaderCn, Long compLeaderId, String compLeaderCn,
			String remark, String isUsable, Long deptLevel) {
		this.deptCode = deptCode;
		this.deptName = deptName;
		this.deptShortname = deptShortname;
		this.deptDesc = deptDesc;
		this.deptOrder = deptOrder;
		this.parentDeptId = parentDeptId;
		this.isHaveChild = isHaveChild;
		this.selflevCode = selflevCode;
		this.selflevName = selflevName;
		this.writer = writer;
		this.writeDate = writeDate;
		this.depLeaderId = depLeaderId;
		this.depLeaderCn = depLeaderCn;
		this.compLeaderId = compLeaderId;
		this.compLeaderCn = compLeaderCn;
		this.remark = remark;
		this.isUsable = isUsable;
		this.deptLevel=deptLevel;
	}
	// Property accessors

	public Long getDepId() {
		return this.depId;
	}

	public void setDepId(Long depId) {
		this.depId = depId;
	}

	public String getDeptCode() {
		return this.deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return this.deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptShortname() {
		return this.deptShortname;
	}

	public void setDeptShortname(String deptShortname) {
		this.deptShortname = deptShortname;
	}

	public String getDeptDesc() {
		return this.deptDesc;
	}

	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}

	public Long getDeptOrder() {
		return this.deptOrder;
	}

	public void setDeptOrder(Long deptOrder) {
		this.deptOrder = deptOrder;
	}
	public Long getParentDeptId() {
		return this.parentDeptId;
	}

	public void setParentDeptId(Long parentDeptId) {
		this.parentDeptId = parentDeptId;
	}

	public String getIsHaveChild() {
		return this.isHaveChild;
	}

	public void setIsHaveChild(String isHaveChild) {
		this.isHaveChild = isHaveChild;
	}

	public String getSelflevCode() {
		return this.selflevCode;
	}

	public void setSelflevCode(String selflevCode) {
		this.selflevCode = selflevCode;
	}

	public String getSelflevName() {
		return this.selflevName;
	}

	public void setSelflevName(String selflevName) {
		this.selflevName = selflevName;
	}


	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getDepLeaderCn() {
		return this.depLeaderCn;
	}

	public void setDepLeaderCn(String depLeaderCn) {
		this.depLeaderCn = depLeaderCn;
	}
	

	public Long getDepLeaderId() {
		return depLeaderId;
	}

	public void setDepLeaderId(Long depLeaderId) {
		this.depLeaderId = depLeaderId;
	}

	public Long getCompLeaderId() {
		return compLeaderId;
	}

	public void setCompLeaderId(Long compLeaderId) {
		this.compLeaderId = compLeaderId;
	}

	public String getCompLeaderCn() {
		return this.compLeaderCn;
	}

	public void setCompLeaderCn(String compLeaderCn) {
		this.compLeaderCn = compLeaderCn;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public String getOuName() {
		return ouName;
	}

	public void setOuName(String ouName) {
		this.ouName = ouName;
	}

	public Long getDeptLevel() {
		return deptLevel;
	}

	public void setDeptLevel(Long deptLevel) {
		this.deptLevel = deptLevel;
	}

	public String getIsPurchaseDept() {
		return isPurchaseDept;
	}

	public void setIsPurchaseDept(String isPurchaseDept) {
		this.isPurchaseDept = isPurchaseDept;
	}

	public Long getPurchaseDeptId() {
		return purchaseDeptId;
	}

	public void setPurchaseDeptId(Long purchaseDeptId) {
		this.purchaseDeptId = purchaseDeptId;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public Long getOwnPurchaseDeptId() {
		return ownPurchaseDeptId;
	}
	public void setOwnPurchaseDeptId(Long ownPurchaseDeptId) {
		this.ownPurchaseDeptId = ownPurchaseDeptId;
	}

}