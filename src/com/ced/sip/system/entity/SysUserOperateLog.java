package com.ced.sip.system.entity;

import java.util.Date;

/**
 * TSysUserOperateLog entity. @author MyEclipse Persistence Tools
 */

public class SysUserOperateLog implements java.io.Serializable {

	// Fields

	private Long uilId;
	private Long uolId;
	private String action;
	private String method;
	private String param;
	private String result;
	private Date operDate;
	private String ipAddress;
	private String writerEn;
	private String writer;
	private int type;
	private Long comId;

	// Constructors

	/** default constructor */
	public SysUserOperateLog() {
	}

	/** minimal constructor */
	public SysUserOperateLog(Long uilId) {
		this.uilId = uilId;
	}

	/** full constructor */
	public SysUserOperateLog(Long uilId, Long uolId, String action,String method,
			String param, String result, Date operDate, String ipAddress,
			String writerEn, String writer) {
		this.uilId = uilId;
		this.uolId = uolId;
		this.action = action;
		this.method = method;
		this.param = param;
		this.result = result;
		this.operDate = operDate;
		this.ipAddress = ipAddress;
		this.writerEn = writerEn;
		this.writer = writer;
	}

	// Property accessors

	public Long getUilId() {
		return this.uilId;
	}

	public void setUilId(Long uilId) {
		this.uilId = uilId;
	}

	public Long getUolId() {
		return this.uolId;
	}

	public void setUolId(Long uolId) {
		this.uolId = uolId;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getOperDate() {
		return this.operDate;
	}

	public void setOperDate(Date operDate) {
		this.operDate = operDate;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getWriterEn() {
		return this.writerEn;
	}

	public void setWriterEn(String writerEn) {
		this.writerEn = writerEn;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

}