package com.ced.sip.system.entity;

import java.util.Date;

/**
 * Dictionary entity. @author MyEclipse Persistence Tools
 */

public class Dictionary implements java.io.Serializable {

	// Fields

	private Long dictId;
	private String dictCode;
	private String dictName;
	private Integer dictOrder;
	private String dictType;
	private Long parentDictId;
	private String parentDictCode;
	private String isHaveChild;
	private String selflevCode;
	private Long dictLevel;
	private String remark;
	private String isUsable;
	private String writer;
	private Date writeDate;

	// Constructors

	/** default constructor */
	public Dictionary() {
	}

	/** full constructor */
	public Dictionary(String dictCode, String dictName, Integer dictOrder,
			String dictType, Long parentDictId, String isHaveChild,
			String selflevCode, Long dictLevel, String remark, String isUsable,
			String writer, Date writeDate) {
		this.dictCode = dictCode;
		this.dictName = dictName;
		this.dictOrder = dictOrder;
		this.dictType = dictType;
		this.parentDictId = parentDictId;
		this.isHaveChild = isHaveChild;
		this.selflevCode = selflevCode;
		this.dictLevel = dictLevel;
		this.remark = remark;
		this.isUsable = isUsable;
		this.writer = writer;
		this.writeDate = writeDate;
	}

	// Property accessors

	public Long getDictId() {
		return this.dictId;
	}

	public void setDictId(Long dictId) {
		this.dictId = dictId;
	}

	public String getDictCode() {
		return this.dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}

	public String getDictName() {
		return this.dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public Integer getDictOrder() {
		return this.dictOrder;
	}

	public void setDictOrder(Integer dictOrder) {
		this.dictOrder = dictOrder;
	}

	public String getDictType() {
		return this.dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}

	public Long getParentDictId() {
		return this.parentDictId;
	}

	public void setParentDictId(Long parentDictId) {
		this.parentDictId = parentDictId;
	}

	public String getIsHaveChild() {
		return this.isHaveChild;
	}

	public void setIsHaveChild(String isHaveChild) {
		this.isHaveChild = isHaveChild;
	}

	public String getSelflevCode() {
		return this.selflevCode;
	}

	public void setSelflevCode(String selflevCode) {
		this.selflevCode = selflevCode;
	}

	public Long getDictLevel() {
		return this.dictLevel;
	}

	public void setDictLevel(Long dictLevel) {
		this.dictLevel = dictLevel;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getParentDictCode() {
		return parentDictCode;
	}

	public void setParentDictCode(String parentDictCode) {
		this.parentDictCode = parentDictCode;
	}

}