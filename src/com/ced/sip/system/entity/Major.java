package com.ced.sip.system.entity;

import java.util.Date;

/** 
 * 类名称：Major
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class Major implements java.io.Serializable {

	// 属性信息
	private Long mjId;     //主键
	private String mjCode;	 //专业编码
	private String mjName;	 //专业名称
	private Long displaySort;     //显示序号
	private Long parentId;     //上级节点主键
	private String isHaveChild;	 //是否有子节点
	private String selflevCode;	 //级别码
	private Integer levels;     //层数
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String isUsable;	 //是否有效
	private String remark;	 //备注
	private Long comId;     //COM_ID

	private String majorCode;
	// 操作信息
	private String operType ;
	

	
	public Major() {
		super();
	}
	
	public Long getMjId(){
	   return  mjId;
	} 
	public void setMjId(Long mjId) {
	   this.mjId = mjId;
    }     
	public String getMjCode(){
	   return  mjCode;
	} 
	public void setMjCode(String mjCode) {
	   this.mjCode = mjCode;
    }
	public String getMjName(){
	   return  mjName;
	} 
	public void setMjName(String mjName) {
	   this.mjName = mjName;
    }
	public Long getDisplaySort(){
	   return  displaySort;
	} 
	public void setDisplaySort(Long displaySort) {
	   this.displaySort = displaySort;
    }     
	public Long getParentId(){
	   return  parentId;
	} 
	public void setParentId(Long parentId) {
	   this.parentId = parentId;
    }     
	public String getIsHaveChild(){
	   return  isHaveChild;
	} 
	public void setIsHaveChild(String isHaveChild) {
	   this.isHaveChild = isHaveChild;
    }
	public String getSelflevCode(){
	   return  selflevCode;
	} 
	public void setSelflevCode(String selflevCode) {
	   this.selflevCode = selflevCode;
    }
	public Integer getLevels(){
	   return  levels;
	} 
	public void setLevels(Integer levels) {
	   this.levels = levels;
    }     
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }

	public String getMajorCode() {
		return majorCode;
	}

	public void setMajorCode(String majorCode) {
		this.majorCode = majorCode;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}  
	
}