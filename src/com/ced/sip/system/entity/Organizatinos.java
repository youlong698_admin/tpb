package com.ced.sip.system.entity;

import java.util.Date;

/**
 * Organizatinos entity. @author MyEclipse Persistence Tools
 */

public class Organizatinos implements java.io.Serializable {

	// Fields

	private Long orgId;
	private String orgCode;
	private String orgName;
	private String orgShortname;
	private String orgDesc;
	private Long orgOrder;
	private Long parentOrgId;
	private String isHaveChild;
	private String selflevCode;
	private String selflevName;
	private Long orgLevel;
	private String writer;
	private Date writeDate;
	private String remark;
	private String isUsable;
	private String isSystem;
	private Long comId;


	// 操作信息
	private String operType ;
	
	/** default constructor */
	public Organizatinos() {
	}

	/** full constructor */
	public Organizatinos(String orgCode, String orgName, String orgShortname,
			String orgDesc, Long orgOrder, Long parentOrgId,
			String isHaveChild, String selflevCode, String selflevName,
			Long orgLevel,String writer,
			Date writeDate, String remark,
			String isUsable,String isSystem) {
		this.orgCode = orgCode;
		this.orgName = orgName;
		this.orgShortname = orgShortname;
		this.orgDesc = orgDesc;
		this.orgOrder = orgOrder;
		this.parentOrgId = parentOrgId;
		this.isHaveChild = isHaveChild;
		this.selflevCode = selflevCode;
		this.selflevName = selflevName;
		this.orgLevel = orgLevel;
		this.writer = writer;
		this.writeDate = writeDate;
		this.remark = remark;
		this.isUsable = isUsable;
		this.isSystem=isSystem;
	}

	// Property accessors

	public Long getOrgId() {
		return this.orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgCode() {
		return this.orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return this.orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgShortname() {
		return this.orgShortname;
	}

	public void setOrgShortname(String orgShortname) {
		this.orgShortname = orgShortname;
	}

	public String getOrgDesc() {
		return this.orgDesc;
	}

	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}

	public Long getOrgOrder() {
		return this.orgOrder;
	}

	public void setOrgOrder(Long orgOrder) {
		this.orgOrder = orgOrder;
	}


	public Long getParentOrgId() {
		return this.parentOrgId;
	}

	public void setParentOrgId(Long parentOrgId) {
		this.parentOrgId = parentOrgId;
	}

	public String getIsHaveChild() {
		return this.isHaveChild;
	}

	public void setIsHaveChild(String isHaveChild) {
		this.isHaveChild = isHaveChild;
	}

	public String getSelflevCode() {
		return this.selflevCode;
	}

	public void setSelflevCode(String selflevCode) {
		this.selflevCode = selflevCode;
	}

	public String getSelflevName() {
		return this.selflevName;
	}

	public void setSelflevName(String selflevName) {
		this.selflevName = selflevName;
	}

	public Long getOrgLevel() {
		return this.orgLevel;
	}

	public void setOrgLevel(Long orgLevel) {
		this.orgLevel = orgLevel;
	}
	
	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(String isSystem) {
		this.isSystem = isSystem;
	}

	public String getOperType() {
		return operType;
	}

	public void setOperType(String operType) {
		this.operType = operType;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}
	
}