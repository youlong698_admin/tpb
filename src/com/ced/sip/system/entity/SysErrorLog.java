package com.ced.sip.system.entity;

import java.util.Date;

/**
 * TSysErrorLog entity. @author MyEclipse Persistence Tools
 */

public class SysErrorLog implements java.io.Serializable {

	// Fields

	private Long elId;
	private String hashcode;
	private String writerEn;
	private String writer;
	private String ipAddress;
	private String errorUrl;
	private Date errorDate;
	private String errorInfo;
	private int type;
	private Long comId;

	// Constructors

	/** default constructor */
	public SysErrorLog() {
	}

	/** minimal constructor */
	public SysErrorLog(Long elId) {
		this.elId = elId;
	}

	/** full constructor */
	public SysErrorLog(Long elId, String hashcode, String writerEn,
			String writer, String ipAddress, String errorUrl, Date errorDate,
			String errorInfo) {
		this.elId = elId;
		this.hashcode = hashcode;
		this.writerEn = writerEn;
		this.writer = writer;
		this.ipAddress = ipAddress;
		this.errorUrl = errorUrl;
		this.errorDate = errorDate;
		this.errorInfo = errorInfo;
	}

	// Property accessors

	public Long getElId() {
		return this.elId;
	}

	public void setElId(Long elId) {
		this.elId = elId;
	}

	public String getHashcode() {
		return this.hashcode;
	}

	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}

	public String getWriterEn() {
		return this.writerEn;
	}

	public void setWriterEn(String writerEn) {
		this.writerEn = writerEn;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getErrorUrl() {
		return this.errorUrl;
	}

	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}

	public Date getErrorDate() {
		return this.errorDate;
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate;
	}

	public String getErrorInfo() {
		return this.errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

}