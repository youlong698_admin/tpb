package com.ced.sip.order.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：OrderInfo
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public class OrderInfo extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long oiId;     //主键
	private int type;     //类型
	private Long baCiId;     //项目合同ID
	private String projectContractName;	 //项目合同名称
	private String orderCode;	 //订单编号
	private String orderName;	 //订单名称
	private String orderPersonNameA;	 //甲方名称
	private Long orderUndertakerId; //收货联系人ID
	private String orderUndertaker;	 //收货联系人
	private String orderTelA;	 //收货电话
	private String orderFaxA;	 //收货传真
	private String orderAddressA;	 //收货地址
	private Long supplierId;     //供应商ID
	private String orderNameB;	 //供应商名称
	private String orderPersonB;	 //供应商联系人
	private String orderTelB;	 //供应商电话
	private String orderFaxB;	 //供应商传真
	private String orderAddressB;	 //供应商地址
	private Double orderMoney;	//订单金额
	private Date orderDate;    //订单日期
	private String conMoneyType;	 //货币类型
	private Date requirementArrivalDate;    //要求到货日期
	private String orderSignAddress;	 //到货地址
	private String conRemark;	 //备注
	private String runStatus;	 //运行状态
	private String statusCn;	 //状态中文
	private String isUsable;	 //是否有效
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Long comId;     //
	private Long deptId;     //部门ID
	private String ifSendFlag;	 //是否全部发货
	private String ifReceivedFlag;	 //是否全部收货
	private String bidCode;//项目合同编号
	private Date auditDate;
	private Double contractAmount;//订单物资总数量
	private Double realInputAmount;//订单物资实际收货数量
	private Double realSendAmount;//订单物资实际发货数量
	private Date lastGoodArriveDate;//最后收货日期
	private String orderMobileA;	 //甲方联系人手机
	private String orderMobileB;	 //乙方联系人手机
	private Long floatCode;
	
	
	private String orderId;//流程实例标示
	private String processId;//流程定义标示
	private String processName;//流程定义名称
	private String orderState;//流程状态
	private String orderStateName;//流程状态名称
	private String instanceUrl;//实例启动页面
	
	public OrderInfo() {
		super();
	}
	
	public Long getOiId(){
	   return  oiId;
	} 
	public void setOiId(Long oiId) {
	   this.oiId = oiId;
    }  
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Long getBaCiId() {
		return baCiId;
	}
	public void setBaCiId(Long baCiId) {
		this.baCiId = baCiId;
	}
	public String getProjectContractName() {
		return projectContractName;
	}

	public void setProjectContractName(String projectContractName) {
		this.projectContractName = projectContractName;
	}

	public String getOrderCode(){
	   return  orderCode;
	} 
	public void setOrderCode(String orderCode) {
	   this.orderCode = orderCode;
    }
	public String getOrderName(){
	   return  orderName;
	} 
	public void setOrderName(String orderName) {
	   this.orderName = orderName;
    }
	public String getOrderPersonNameA(){
	   return  orderPersonNameA;
	} 
	public void setOrderPersonNameA(String orderPersonNameA) {
	   this.orderPersonNameA = orderPersonNameA;
    }
	public String getOrderUndertaker(){
	   return  orderUndertaker;
	} 
	public void setOrderUndertaker(String orderUndertaker) {
	   this.orderUndertaker = orderUndertaker;
    }
	public String getOrderTelA(){
	   return  orderTelA;
	} 
	public void setOrderTelA(String orderTelA) {
	   this.orderTelA = orderTelA;
    }
	public String getOrderFaxA(){
	   return  orderFaxA;
	} 
	public void setOrderFaxA(String orderFaxA) {
	   this.orderFaxA = orderFaxA;
    }
	public String getOrderAddressA(){
	   return  orderAddressA;
	} 
	public void setOrderAddressA(String orderAddressA) {
	   this.orderAddressA = orderAddressA;
    }
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getOrderNameB(){
	   return  orderNameB;
	} 
	public void setOrderNameB(String orderNameB) {
	   this.orderNameB = orderNameB;
    }
	public String getOrderPersonB(){
	   return  orderPersonB;
	} 
	public void setOrderPersonB(String orderPersonB) {
	   this.orderPersonB = orderPersonB;
    }
	public String getOrderTelB(){
	   return  orderTelB;
	} 
	public void setOrderTelB(String orderTelB) {
	   this.orderTelB = orderTelB;
    }
	public String getOrderFaxB(){
	   return  orderFaxB;
	} 
	public void setOrderFaxB(String orderFaxB) {
	   this.orderFaxB = orderFaxB;
    }
	public String getOrderAddressB(){
	   return  orderAddressB;
	} 
	public void setOrderAddressB(String orderAddressB) {
	   this.orderAddressB = orderAddressB;
    }
	public Double getOrderMoney(){
	   return  orderMoney;
	} 
	public void setOrderMoney(Double orderMoney) {
	   this.orderMoney = orderMoney;
    }	
	public Date getOrderDate(){
	   return  orderDate;
	} 
	public void setOrderDate(Date orderDate) {
	   this.orderDate = orderDate;
    }	    
	public String getConMoneyType(){
	   return  conMoneyType;
	} 
	public void setConMoneyType(String conMoneyType) {
	   this.conMoneyType = conMoneyType;
    }
	public Date getRequirementArrivalDate(){
	   return  requirementArrivalDate;
	} 
	public void setRequirementArrivalDate(Date requirementArrivalDate) {
	   this.requirementArrivalDate = requirementArrivalDate;
    }	    
	public String getOrderSignAddress(){
	   return  orderSignAddress;
	} 
	public void setOrderSignAddress(String orderSignAddress) {
	   this.orderSignAddress = orderSignAddress;
    }
	public String getConRemark(){
	   return  conRemark;
	} 
	public void setConRemark(String conRemark) {
	   this.conRemark = conRemark;
    }
	public String getRunStatus(){
	   return  runStatus;
	} 
	public void setRunStatus(String runStatus) {
	   this.runStatus = runStatus;
    }
	public String getStatusCn(){
	   return  statusCn;
	} 
	public void setStatusCn(String statusCn) {
	   this.statusCn = statusCn;
    }
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }     
	public Long getDeptId(){
	   return  deptId;
	} 
	public void setDeptId(Long deptId) {
	   this.deptId = deptId;
    }     
	public String getIfSendFlag(){
	   return  ifSendFlag;
	} 
	public void setIfSendFlag(String ifSendFlag) {
	   this.ifSendFlag = ifSendFlag;
    }
	public String getIfReceivedFlag(){
	   return  ifReceivedFlag;
	} 
	public void setIfReceivedFlag(String ifReceivedFlag) {
	   this.ifReceivedFlag = ifReceivedFlag;
    }

	public String getBidCode() {
		return bidCode;
	}

	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getOrderStateName() {
		return orderStateName;
	}

	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public Double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(Double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public Double getRealInputAmount() {
		return realInputAmount;
	}

	public void setRealInputAmount(Double realInputAmount) {
		this.realInputAmount = realInputAmount;
	}

	public Double getRealSendAmount() {
		return realSendAmount;
	}

	public void setRealSendAmount(Double realSendAmount) {
		this.realSendAmount = realSendAmount;
	}

	public Date getLastGoodArriveDate() {
		return lastGoodArriveDate;
	}

	public void setLastGoodArriveDate(Date lastGoodArriveDate) {
		this.lastGoodArriveDate = lastGoodArriveDate;
	}

	public String getOrderMobileA() {
		return orderMobileA;
	}

	public void setOrderMobileA(String orderMobileA) {
		this.orderMobileA = orderMobileA;
	}

	public String getOrderMobileB() {
		return orderMobileB;
	}

	public void setOrderMobileB(String orderMobileB) {
		this.orderMobileB = orderMobileB;
	}

	public Long getFloatCode() {
		return floatCode;
	}

	public void setFloatCode(Long floatCode) {
		this.floatCode = floatCode;
	}

	public Long getOrderUndertakerId() {
		return orderUndertakerId;
	}

	public void setOrderUndertakerId(Long orderUndertakerId) {
		this.orderUndertakerId = orderUndertakerId;
	}
}