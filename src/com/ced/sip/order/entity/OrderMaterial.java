package com.ced.sip.order.entity;

import java.util.Date;

/** 
 * 类名称：OrderMaterial
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class OrderMaterial implements java.io.Serializable {

	// 属性信息
	private Long omId;     //主键
	private Long oiId;     //合同主键
	private String materialCode;	 //物资编码
	private String materialName;	 //物资名称
	private String materialType;	 //物资型号
	private String trademark;	 //品牌
	private Double price;	//单价
	private String unit;	 //单位
	private Double amount;	//数量
	private Date arrivalDate;    //到货日期
	private Double waitInputAmount;	//可收货的数量初始值为物资数量
	private Double realInputAmount;	//实际收货的数量
	private Double waitSendAmount;	//可发货的数量初始值为物资数量
	private Double realSendAmount;	//实际发货的数量
	private String isUsable;	 //是否有效
	private String ifSendFlag;	 //是否全部发货
	private String ifReceivedFlag;	 //是否全部收货
	private String lastSendDate;	 //最后发货日期
	private String remark;	 //备注
	private Long tableKey;//table主键
	
	
	public OrderMaterial() {
		super();
	}
	
	public Long getOmId(){
	   return  omId;
	} 
	public void setOmId(Long omId) {
	   this.omId = omId;
    }     
	public Long getOiId(){
	   return  oiId;
	} 
	public void setOiId(Long oiId) {
	   this.oiId = oiId;
    }     
	public String getMaterialCode(){
	   return  materialCode;
	} 
	public void setMaterialCode(String materialCode) {
	   this.materialCode = materialCode;
    }
	public String getMaterialName(){
	   return  materialName;
	} 
	public void setMaterialName(String materialName) {
	   this.materialName = materialName;
    }
	public String getMaterialType(){
	   return  materialType;
	} 
	public void setMaterialType(String materialType) {
	   this.materialType = materialType;
    }
	public String getTrademark(){
	   return  trademark;
	} 
	public void setTrademark(String trademark) {
	   this.trademark = trademark;
    }
	public Double getPrice(){
	   return  price;
	} 
	public void setPrice(Double price) {
	   this.price = price;
    }	
	public String getUnit(){
	   return  unit;
	} 
	public void setUnit(String unit) {
	   this.unit = unit;
    }
	public Double getAmount(){
	   return  amount;
	} 
	public void setAmount(Double amount) {
	   this.amount = amount;
    }	
	public Date getArrivalDate(){
	   return  arrivalDate;
	} 
	public void setArrivalDate(Date arrivalDate) {
	   this.arrivalDate = arrivalDate;
    }	    
	public Double getWaitInputAmount(){
	   return  waitInputAmount;
	} 
	public void setWaitInputAmount(Double waitInputAmount) {
	   this.waitInputAmount = waitInputAmount;
    }	
	public Double getRealInputAmount(){
	   return  realInputAmount;
	} 
	public void setRealInputAmount(Double realInputAmount) {
	   this.realInputAmount = realInputAmount;
    }	
	public Double getWaitSendAmount(){
	   return  waitSendAmount;
	} 
	public void setWaitSendAmount(Double waitSendAmount) {
	   this.waitSendAmount = waitSendAmount;
    }	
	public Double getRealSendAmount(){
	   return  realSendAmount;
	} 
	public void setRealSendAmount(Double realSendAmount) {
	   this.realSendAmount = realSendAmount;
    }	
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getIfSendFlag(){
	   return  ifSendFlag;
	} 
	public void setIfSendFlag(String ifSendFlag) {
	   this.ifSendFlag = ifSendFlag;
    }
	public String getIfReceivedFlag(){
	   return  ifReceivedFlag;
	} 
	public void setIfReceivedFlag(String ifReceivedFlag) {
	   this.ifReceivedFlag = ifReceivedFlag;
    }
	public String getLastSendDate(){
	   return  lastSendDate;
	} 
	public void setLastSendDate(String lastSendDate) {
	   this.lastSendDate = lastSendDate;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public Long getTableKey() {
		return tableKey;
	}
	public void setTableKey(Long tableKey) {
		this.tableKey = tableKey;
	}
}