package com.ced.sip.order.entity;

import java.util.Date;

/** 
 * 类名称：OrderConfirm
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public class OrderConfirm implements java.io.Serializable {

	// 属性信息
	private Long ocId;     //主键
	private Long oiId;     //订单主键
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String confirmRemark;	 //确认说明
	private String status;	 //状态
	private Date confirmDate;    //确认日期
	
	
	public OrderConfirm() {
		super();
	}
	
	public Long getOcId(){
	   return  ocId;
	} 
	public void setOcId(Long ocId) {
	   this.ocId = ocId;
    }     
	public Long getOiId(){
	   return  oiId;
	} 
	public void setOiId(Long oiId) {
	   this.oiId = oiId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getConfirmRemark(){
	   return  confirmRemark;
	} 
	public void setConfirmRemark(String confirmRemark) {
	   this.confirmRemark = confirmRemark;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public Date getConfirmDate(){
	   return  confirmDate;
	} 
	public void setConfirmDate(Date confirmDate) {
	   this.confirmDate = confirmDate;
    }	    
}