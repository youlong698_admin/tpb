package com.ced.sip.order.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：OrderTermination
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public class OrderTermination  extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long otId;     //主键
	private Long oiId;     //订单ID
	private String orderCode;	 //订单编号
	private String orderName;	 //订单名称
	private String terminationReason;	 //终止原因
	private Date submitDate;    //提交日期
	private String status;	 //状态
	private String statusCn;	 //状态中文
	private Long writerId;     //填报人ID
	private String writer;	 //填报人
	private Date writeDete;    //填报日期

	//临时
	private String writerCn;
	
	public OrderTermination() {
		super();
	}
	
	public Long getOtId(){
	   return  otId;
	} 
	public void setOtId(Long otId) {
	   this.otId = otId;
    }     
	public Long getOiId(){
	   return  oiId;
	} 
	public void setOiId(Long oiId) {
	   this.oiId = oiId;
    }     
	public String getOrderCode(){
	   return  orderCode;
	} 
	public void setOrderCode(String orderCode) {
	   this.orderCode = orderCode;
    }
	public String getOrderName(){
	   return  orderName;
	} 
	public void setOrderName(String orderName) {
	   this.orderName = orderName;
    }
	public String getTerminationReason(){
	   return  terminationReason;
	} 
	public void setTerminationReason(String terminationReason) {
	   this.terminationReason = terminationReason;
    }
	public Date getSubmitDate(){
	   return  submitDate;
	} 
	public void setSubmitDate(Date submitDate) {
	   this.submitDate = submitDate;
    }	    
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public String getStatusCn(){
	   return  statusCn;
	} 
	public void setStatusCn(String statusCn) {
	   this.statusCn = statusCn;
    }
	public Long getWriterId(){
	   return  writerId;
	} 
	public void setWriterId(Long writerId) {
	   this.writerId = writerId;
    }     
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDete(){
	   return  writeDete;
	} 
	public void setWriteDete(Date writeDete) {
	   this.writeDete = writeDete;
    }

	public String getWriterCn() {
		return writerCn;
	}

	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}	    
}