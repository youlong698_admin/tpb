package com.ced.sip.order.util;

public class OrderStatus {
 
	//	-------------------------------------- 订单运行状态  -----------------------------------------
	/** 订单运行状态:已起草 "00" */
	public static final String ORDER_RUN_STATUS_01="01";
	
	/** 订单运行状态:已起草*/
	public static final String ORDER_RUN_STATUS_01_Text = "已起草";
	
	/** 订单运行状态:审批通过 "0" */
	public static final String ORDER_RUN_STATUS_00="0";
	
	/** 订单运行状态:审批通过*/
	public static final String ORDER_RUN_STATUS_00_Text = "审批通过";
	
	/** 订单运行状态:审批通过 "1" */
	public static final String ORDER_RUN_STATUS_1="1";
	
	/** 订单运行状态:审批通过*/
	public static final String ORDER_RUN_STATUS_1_Text = "审批中";
	
	/** 订单运行状态:审批退回 "3" */
	public static final String ORDER_RUN_STATUS_03="3";
	
	/** 订单运行状态:审批退回*/
	public static final String ORDER_RUN_STATUS_03_Text = "审批退回";
	
	/** 订单运行状态:已发送 "4" */
	public static final String ORDER_RUN_STATUS_04="4";
	
	/** 订单运行状态:已发送*/
	public static final String ORDER_RUN_STATUS_04_Text = "已发送至供应商";
	
	/** 订单运行状态:已确认 "5" */
	public static final String ORDER_RUN_STATUS_05="5";
	
	/** 订单运行状态:已确认*/
	public static final String ORDER_RUN_STATUS_05_Text = "供应商已确认";
	
	/** 订单运行状态:已确认 "6" */
	public static final String ORDER_RUN_STATUS_06="6";
	
	/** 订单运行状态:已确认*/
	public static final String ORDER_RUN_STATUS_06_Text = "供应商退回";
	
	/** 订单运行状态:已发货 "7" */
	public static final String ORDER_RUN_STATUS_07="7";
	
	/** 订单运行状态:已发货 */
	public static final String ORDER_RUN_STATUS_07_Text = "已发货";
	
	/** 订单运行状态:收货完成 "8" */
	public static final String ORDER_RUN_STATUS_08="8";
	
	/** 订单运行状态:收货完成 */
	public static final String ORDER_RUN_STATUS_08_Text = "收货完成";
	
	
	/** 订单运行状态:已中止 "9" */
	public static final String ORDER_RUN_STATUS_09="9";
	
	/** 订单运行状态:已中止*/
	public static final String ORDER_RUN_STATUS_09_Text = "已终止";
	
}
