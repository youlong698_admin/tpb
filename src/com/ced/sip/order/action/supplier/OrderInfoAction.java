package com.ced.sip.order.action.supplier;

import java.util.Date;
import java.util.List;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.order.biz.IOrderConfirmBiz;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.order.biz.IOrderMaterialBiz;
import com.ced.sip.bill.biz.ISendBillBiz;
import com.ced.sip.bill.biz.ISendBillDetailBiz;
import com.ced.sip.order.entity.OrderConfirm;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.order.entity.OrderMaterial;
import com.ced.sip.bill.entity.SendBill;
import com.ced.sip.bill.entity.SendBillDetail;
import com.ced.sip.order.util.OrderStatus;
/** 
 * 类名称：OrderInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public class OrderInfoAction extends BaseAction {

	// 订单信息 
	private IOrderInfoBiz iOrderInfoBiz;
	//订单明细信息
	private IOrderMaterialBiz iOrderMaterialBiz;
	//订单确认信息
	private IOrderConfirmBiz iOrderConfirmBiz;
	//发货信息
	private ISendBillBiz iSendBillBiz;
	//发货物资明细
	private ISendBillDetailBiz iSendBillDetailBiz;
	//附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// 订单信息
	private OrderInfo orderInfo;
	
	private OrderMaterial orderMaterial;	

	private OrderConfirm orderConfirm;

	private List<OrderMaterial> omList;
	
	private SendBill sendBill;
	
	private SendBillDetail sendBillDetail;
	
	private List<SendBillDetail> sbdList;
	
	
	/**
	 * 查看订单确认信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrderInfoApplication() throws BaseException {
		String initPage="order_application";
		try{
			String from=this.getRequest().getParameter("from");
			String orderName=this.getRequest().getParameter("orderName");
			 //触屏版使用
	        if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		    this.getRequest().setAttribute("orderName", orderName);
		} catch (Exception e) {
			log.error("查看订单确认信息信息列表错误！", e);
			throw new BaseException("查看订单确认信息信息列表错误！", e);
		}	
			
		return initPage;
				
	}

	/**
	 * 查看我的订单信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrderInfoMy() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看我的订单信息列表错误！", e);
			throw new BaseException("查看我的订单信息列表错误！", e);
		}	
			
		return "order_my" ;
				
	}
	/**
	 * 订单发货管理
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSendOrderInfo() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("订单发货管理列表错误！", e);
			throw new BaseException("订单发货管理列表错误！", e);
		}	
			
		return "order_send" ;
				
	}
	/**
	 * 查看订单确认信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findOrderInfoApplication() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			int sign=Integer.parseInt(this.getRequest().getParameter("sgin"));
			String orderPersonNameA=this.getRequest().getParameter("orderPersonNameA");
			String orderCode=this.getRequest().getParameter("orderCode");
			String orderName=this.getRequest().getParameter("orderName");
			orderInfo=new OrderInfo();
			orderInfo.setOrderPersonNameA(orderPersonNameA);
			orderInfo.setOrderCode(orderCode);
			orderInfo.setOrderName(orderName);
			orderInfo.setSupplierId(supplierId);
			List list=this.iOrderInfoBiz.getOrderInfoListBySupplier(this.getRollPageDataTables(), orderInfo,sign);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看订单确认信息列表错误！", e);
			throw new BaseException("查看订单确认信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 查看订单信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewOrderInfoDetail() throws BaseException {
		
		try{
			orderInfo=this.iOrderInfoBiz.getOrderInfo(orderInfo.getOiId() );
			
			orderMaterial=new OrderMaterial();
			orderMaterial.setOiId(orderInfo.getOiId());
			omList=this.iOrderMaterialBiz.getOrderMaterialList(orderMaterial);
			
			orderConfirm=new OrderConfirm();
			orderConfirm.setOiId(orderInfo.getOiId());
			List confirmList=this.iOrderConfirmBiz.getOrderConfirmList(orderConfirm);
			this.getRequest().setAttribute("confirmList", confirmList);
		} catch (Exception e) {
			log("查看订单信息明细信息错误！", e);
			throw new BaseException("查看订单信息明细信息错误！", e);
		}
		return "order_detail";
		
	}
	/**
	 * 供应商进行订单确认初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveConfirmOrderInfoInit() throws BaseException {		
		String initPage="order_confirm";
		try{
			String from=this.getRequest().getParameter("from");
			orderInfo=this.iOrderInfoBiz.getOrderInfo(orderInfo.getOiId() );
			
			orderMaterial=new OrderMaterial();
			orderMaterial.setOiId(orderInfo.getOiId());
			omList=this.iOrderMaterialBiz.getOrderMaterialList(orderMaterial);
			
			orderConfirm=new OrderConfirm();
			orderConfirm.setOiId(orderInfo.getOiId());
			List confirmList=this.iOrderConfirmBiz.getOrderConfirmList(orderConfirm);
			this.getRequest().setAttribute("confirmList", confirmList);
			 //触屏版使用
	        if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("发送订单至供应商确认初始化页面错误！", e);
			throw new BaseException("发送订单至供应商确认初始化页面错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 供应商进行订单确认操作
	 * @return
	 * @throws BaseException 
	 */
	public String saveConfirmOrderInfo() throws BaseException {		
		String initPage="success";
		try{
		   String from=this.getRequest().getParameter("from");
		   
           Long oiId=Long.parseLong(this.getRequest().getParameter("oiId"));
           String supplierName=UserRightInfoUtil.getSupplierName(this.getRequest());
           String status=this.getRequest().getParameter("status");
           String confirmRemark=this.getRequest().getParameter("confirmRemark");
           Long supplierId=Long.parseLong(this.getRequest().getParameter("supplierId"));
           orderInfo=this.iOrderInfoBiz.getOrderInfo(oiId);
           
           orderConfirm=new OrderConfirm();
           orderConfirm.setOiId(oiId);
           orderConfirm.setConfirmDate(DateUtil.getCurrentDateTime());
           orderConfirm.setConfirmRemark(confirmRemark);
           orderConfirm.setSupplierId(supplierId);
           orderConfirm.setSupplierName(supplierName);
           if(status.equals("1")){
        	   orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_05);
        	   orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_05_Text);
               orderConfirm.setStatus("无异议,确认");
           }else{
        	   orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_06);
        	   orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_06_Text);
               orderConfirm.setStatus("有异议,驳回");
           }
           this.iOrderConfirmBiz.saveOrderConfirm(orderConfirm);
    	   this.iOrderInfoBiz.updateOrderInfo(orderInfo);
    	   
    	   String content=""+supplierName+"对订单已经确认，订单名称为"+orderInfo.getOrderName()+"，订单金额为"+orderInfo.getOrderMoney()+"，确认结果为"+orderConfirm.getStatus();
		   this.saveSmsMessageToUsers(orderInfo.getOrderMobileA(), content, supplierName);
    	   
           this.getRequest().setAttribute("message", "确认成功");
		   this.getRequest().setAttribute("operModule", "供应商确认订单信息");
		   //触屏版使用
	       if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("发送订单至供应商确认操作错误！", e);
			throw new BaseException("发送订单至供应商确认操作错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 供应商发货初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveSendOrderInfoInit() throws BaseException {		
		try{
            orderInfo=this.iOrderInfoBiz.getOrderInfo(orderInfo.getOiId() );
			
			orderMaterial=new OrderMaterial();
			orderMaterial.setOiId(orderInfo.getOiId());
			omList=this.iOrderMaterialBiz.getOrderMaterialList(orderMaterial);
			
			this.getRequest().setAttribute("today", DateUtil.getDefaultDateFormat(new Date()));
		} catch (Exception e) {
			log("供应商发货初始化页面错误！", e);
			throw new BaseException("供应商发货初始化页面错误！", e);
		}
		return "addOrderSendInit";		
	}
	/**
	 * 供应商发货信息保存
	 * @return
	 * @throws BaseException 
	 */
	public String saveSendOrderInfo() throws BaseException {		
		try{
			String writer=UserRightInfoUtil.getSupplierLoginName(this.getRequest());			
            //编制人
			sendBill.setWriter(writer);	 	   
	 	    //编制日期 
			sendBill.setWriteDate(DateUtil.getCurrentDateTime());	
			sendBill.setStatus(TableStatus.COMMON_0);
			sendBill.setIsInput(TableStatus.COMMON_INT_1);
			this.iSendBillBiz.saveSendBill(sendBill);
			
			//保存附件		
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(sendBill,
					sendBill.getSbId(), AttachmentStatus.ATTACHMENT_CODE_1201,
					writer));
			
			Double realSendAmount=0.00;
			// 保存合同物资明细数据
			if (sbdList != null) {
				for (int i = 0; i < sbdList.size(); i++) {
					sendBillDetail = sbdList.get(i);
					if (sendBillDetail != null
							&& StringUtil.isNotBlank(sendBillDetail.getMaterialCode().trim())) {
						sendBillDetail.setSbId(sendBill.getSbId());
						iSendBillDetailBiz.saveSendBillDetail(sendBillDetail);					
						//更新订单明细表中的可发货数量，实际发货数量
						iOrderMaterialBiz.updateSendAmount(sendBillDetail.getSendAmount(), sendBillDetail.getOmId());
						realSendAmount+=sendBillDetail.getSendAmount();
					}
				}
			}
			sendBill.setSendAmount(realSendAmount);
			this.iSendBillBiz.updateSendBill(sendBill);
						
			orderInfo=this.iOrderInfoBiz.getOrderInfo(sendBill.getOiId());
			orderInfo.setRealSendAmount(orderInfo.getRealSendAmount()+realSendAmount);
			orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_07);
			orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_07_Text);
			this.iOrderInfoBiz.updateOrderInfo(orderInfo);
			
			this.getRequest().setAttribute("message", "发货成功");
			this.getRequest().setAttribute("operModule", "供应商发货成功");
		} catch (Exception e) {
			log("供应商发货保存错误！", e);
			throw new BaseException("供应商发货保存错误！", e);
		}
		return "sendBillView";		
	}
	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public IOrderMaterialBiz getiOrderMaterialBiz() {
		return iOrderMaterialBiz;
	}

	public void setiOrderMaterialBiz(IOrderMaterialBiz iOrderMaterialBiz) {
		this.iOrderMaterialBiz = iOrderMaterialBiz;
	}

	public List<OrderMaterial> getOmList() {
		return omList;
	}

	public void setOmList(List<OrderMaterial> omList) {
		this.omList = omList;
	}

	public IOrderConfirmBiz getiOrderConfirmBiz() {
		return iOrderConfirmBiz;
	}

	public void setiOrderConfirmBiz(IOrderConfirmBiz iOrderConfirmBiz) {
		this.iOrderConfirmBiz = iOrderConfirmBiz;
	}

	public ISendBillBiz getiSendBillBiz() {
		return iSendBillBiz;
	}

	public void setiSendBillBiz(ISendBillBiz iSendBillBiz) {
		this.iSendBillBiz = iSendBillBiz;
	}

	public ISendBillDetailBiz getiSendBillDetailBiz() {
		return iSendBillDetailBiz;
	}

	public void setiSendBillDetailBiz(ISendBillDetailBiz iSendBillDetailBiz) {
		this.iSendBillDetailBiz = iSendBillDetailBiz;
	}

	public SendBill getSendBill() {
		return sendBill;
	}

	public void setSendBill(SendBill sendBill) {
		this.sendBill = sendBill;
	}

	public List<SendBillDetail> getSbdList() {
		return sbdList;
	}

	public void setSbdList(List<SendBillDetail> sbdList) {
		this.sbdList = sbdList;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	
}
