package com.ced.sip.order.action.epp;

import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.order.biz.IOrderTerminationBiz;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.order.entity.OrderTermination;
import com.ced.sip.order.util.OrderStatus;
/** 
 * 类名称：OrderTerminationAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public class OrderTerminationAction extends BaseAction {

	// 采购异常表 
	private IOrderTerminationBiz iOrderTerminationBiz;
	
	private IOrderInfoBiz iOrderInfoBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	
	// 采购异常表
	private OrderTermination orderTermination;
	
	private OrderInfo orderInfo;
	
	
	
	/**
	 * 订单终止初始化页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveOrderTerminationInit() throws BaseException {
		
		try{
			orderInfo=iOrderInfoBiz.getOrderInfo(orderInfo.getOiId());
			
			orderTermination=new OrderTermination();
			orderTermination.setOiId(orderInfo.getOiId());
			List<OrderTermination> ctList=iOrderTerminationBiz.getOrderTerminationList(orderTermination);
			if(ctList.size()>0)
			{
				orderTermination=ctList.get(0);
				//获取附件
				Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( orderTermination.getOtId(), AttachmentStatus.ATTACHMENT_CODE_1001 ) );
				orderTermination.setUuIdData((String)map.get("uuIdData"));
				orderTermination.setFileNameData((String)map.get("fileNameData"));
				orderTermination.setFileTypeData((String)map.get("fileTypeData"));
				orderTermination.setAttIdData((String)map.get("attIdData"));
				orderTermination.setAttIds((String)map.get("attIds"));
			}else
			{
				orderTermination.setWriterId(UserRightInfoUtil.getUserId(this.getRequest()));
				orderTermination.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
				orderTermination.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(UserRightInfoUtil.getUserName(this.getRequest())));
				orderTermination.setWriteDete(DateUtil.getCurrentDateTime());
			}
			this.getRequest().setAttribute("abnomalList",TableStatusMap.bidAbnomalMap);
			
		} catch (Exception e) {
			log.error("查看订单业务终止初始化页面错误！", e);
			throw new BaseException("查看订单业务终止初始化页面错误！", e);
		}
		
		return "saveOrderTerm";
		
	}
	
	/**
	 * 保存订单业务终止信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveOrderTermination() throws BaseException {
		
		try{
			if(StringUtil.isNotBlank(orderTermination.getOtId()))
			{
				// 删除附件
				iAttachmentBiz.deleteAttachments( parseAttachIds( orderTermination.getAttIds() ) ) ;
				//保存附件			
				iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( orderTermination, orderTermination.getOtId(), AttachmentStatus.ATTACHMENT_CODE_1101, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
				
				String ciId=this.getRequest().getParameter("ciId");
				orderInfo=iOrderInfoBiz.getOrderInfo(new Long(ciId));
				orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_09);
				orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_09_Text);
				iOrderInfoBiz.updateOrderInfo(orderInfo);
				orderTermination.setOiId(orderInfo.getOiId());
				iOrderTerminationBiz.updateOrderTermination(orderTermination);
			}else
			{
				String oiId=this.getRequest().getParameter("oiId");
				orderInfo=iOrderInfoBiz.getOrderInfo(new Long(oiId));
				orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_09);
				orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_09_Text);
				iOrderInfoBiz.updateOrderInfo(orderInfo);
				
				orderTermination.setSubmitDate(DateUtil.getCurrentDateTime());
				orderTermination.setOiId(orderInfo.getOiId());
				iOrderTerminationBiz.saveOrderTermination(orderTermination);
				
				//保存附件			
				iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( orderTermination, orderTermination.getOtId(), AttachmentStatus.ATTACHMENT_CODE_1101, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
				
				
				this.getRequest().setAttribute("message", orderInfo.getOrderCode()+"订单终止成功");
				this.getRequest().setAttribute("operModule", "保存订单业务终止信息");
			}
			
		} catch (Exception e) {
			log.error("保存订单业务终止信息错误！", e);
			throw new BaseException("保存订单业务终止信息错误！", e);
		}
		return "success";
		
	}
	
	/**
	 * 查看订单业务终止信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrderTerminationDetail() throws BaseException {
		
		try{
			orderTermination=new OrderTermination();
			orderTermination.setOiId(orderInfo.getOiId());
			List<OrderTermination> ctList=iOrderTerminationBiz.getOrderTerminationList(orderTermination);
			
			orderTermination=ctList.get(0);
			orderTermination.setWriterCn(BaseDataInfosUtil.convertUserIdToChnName(orderTermination.getWriterId()));
			String attachment = iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment(orderTermination.getOtId(), AttachmentStatus.ATTACHMENT_CODE_1101 ) ) , "0", this.getRequest() );
		
			this.getRequest().setAttribute("attachment", attachment);
			
		} catch (Exception e) {
			log.error("查看订单业务终止信息错误！", e);
			throw new BaseException("查看订单业务终止信息错误！", e);
		}
		return "orderTermDetail";
		
	}

	public IOrderTerminationBiz getiOrderTerminationBiz() {
		return iOrderTerminationBiz;
	}

	public void setiOrderTerminationBiz(
			IOrderTerminationBiz iOrderTerminationBiz) {
		this.iOrderTerminationBiz = iOrderTerminationBiz;
	}

	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public OrderTermination getOrderTermination() {
		return orderTermination;
	}

	public void setOrderTermination(OrderTermination orderTermination) {
		this.orderTermination = orderTermination;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}
}
