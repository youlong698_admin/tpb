package com.ced.sip.order.action.epp;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractClassBiz;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.biz.IContractMaterialBiz;
import com.ced.sip.contract.entity.ContractClass;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.contract.entity.ContractMaterial;
import com.ced.sip.contract.util.ContractStatus;
import com.ced.sip.order.biz.IOrderConfirmBiz;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.order.biz.IOrderMaterialBiz;
import com.ced.sip.order.entity.OrderConfirm;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.order.entity.OrderMaterial;
import com.ced.sip.order.util.OrderStatus;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IBidAwardDetailBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAward;
import com.ced.sip.purchase.base.entity.BidAwardDetail;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.biz.ISysCompanyBiz;
import com.ced.sip.system.entity.Dictionary;
import com.ced.sip.system.entity.SysCompany;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.system.entity.Users;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
/** 
 * 类名称：OrderInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class OrderInfoAction extends BaseAction {

	@Autowired
	private SnakerEngineFacets facets;
	// 订单信息 
	private IOrderInfoBiz iOrderInfoBiz;
	//订单明细信息
	private IOrderMaterialBiz iOrderMaterialBiz;
	//合同字典信息
	private IContractClassBiz iContractClassBiz;
	//供应商服务类
	private ISupplierInfoBiz iSupplierInfoBiz;
	//采购单位服务类
	private ISysCompanyBiz iSysCompanyBiz;
	//项目信息
	private IRequiredCollectBiz iRequiredCollectBiz;
	//授标信息
	private IBidAwardBiz iBidAwardBiz;
	//授标明细信息
	private IBidAwardDetailBiz iBidAwardDetailBiz;
	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	//合同明细信息
	private IContractMaterialBiz iContractMaterialBiz;
	//合同确认信息
	private IOrderConfirmBiz iOrderConfirmBiz;
	
	
	// 订单信息
	private OrderInfo orderInfo;
	
	private OrderMaterial orderMaterial;

	private List<OrderMaterial> omList;
	
	private ContractClass contractClass;
	
    private SupplierInfo supplierInfo;
    
    private SysCompany sysCompany;
	// 授标
	private BidAward bidAward;
	
	private RequiredCollect requiredCollect;
	
    private RequiredCollectDetail requiredCollectDetail;
	// 合同信息
	private ContractInfo contractInfo;
	
	private ContractMaterial contractMaterial;
	
	private OrderConfirm orderConfirm;
	
	/**
	 * 查看订单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrderInfo() throws BaseException {
		String initPage="view";
		try{
			String from = getRequest().getParameter("from");
			String orderName=this.getRequest().getParameter("orderName");
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			this.getRequest().setAttribute("orderWorkflow", systemConfiguration.getOrderWorkflow());
			this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.OD_WorkFlow_Type);
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		    this.getRequest().setAttribute("orderName", orderName);
		} catch (Exception e) {
			log.error("查看订单信息信息列表错误！", e);
			throw new BaseException("查看订单信息信息列表错误！", e);
		}	
			
		return initPage ;
				
	}
	
	/**
	 * 查看订单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findOrderInfo() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			String orderNameB=this.getRequest().getParameter("orderNameB");
			String orderCode=this.getRequest().getParameter("orderCode");
			String orderName=this.getRequest().getParameter("orderName");
			orderInfo=new OrderInfo();
			orderInfo.setOrderNameB(orderNameB);
			orderInfo.setOrderCode(orderCode);
			orderInfo.setOrderName(orderName);
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			List list=this.iOrderInfoBiz.getOrderInfoList(this.getRollPageDataTables(), orderInfo,sqlStr);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					orderInfo = (OrderInfo) list.get(i);
					if(StringUtil.convertNullToBlank(systemConfiguration.getOrderWorkflow()).equals("0")){
						  setProcessLisOrder(orderInfo, WorkFlowStatus.Order_Work_Item,comId);
						}else{
							orderInfo.setOrderState(orderInfo.getRunStatus());
						}
				}
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看订单信息信息列表错误！", e);
			throw new BaseException("查看订单信息信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 设置计划流程相关列表
	 */
	private void setProcessLisOrder(OrderInfo orderInfo, String processName,Long comId){
		//step1：首先获取流程定义实体
		org.snaker.engine.entity.Process process = facets.getEngine().process().getProcessByName(processName,comId);
		//step2：1、判断流程定义是否存在，2、遍历集合添加流程实例
		if(process != null){
				String newOrderNo=WorkFlowStatus.OD_WorkFlow_Type+orderInfo.getOiId().toString();
				//step3:判断流程实例是否运行
				QueryFilter filter = new QueryFilter();
               filter.setOrderNo(newOrderNo);
				filter.setProcessId(process.getId());
				List<HistoryOrder> holist = facets.getEngine().query().getHistoryOrders(filter);
				if(holist.size() > 0){
					//step5:获取正在运行的流程实例
					filter.setOrderNo(newOrderNo);
					filter.setProcessId(process.getId());
					List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
					if(orderlist.size() > 0){
						Order order = orderlist.get((orderlist.size()-1));
						if(order != null){
							List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
							if(tasklist.size() > 0){
								//设置当前流程名称
								orderInfo.setProcessName(tasklist.get(0).getDisplayName());
							}
							orderInfo.setOrderId(order.getId());//设置实例标示
						}
					}
					HistoryOrder ho = holist.get(0);
					orderInfo.setOrderState(ho.getOrderState().toString());
					orderInfo.setOrderStateName(WorkFlowStatusMap.getWorkflowOrderStatus(ho.getOrderState().toString()));
				}else{
					orderInfo.setOrderStateName(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01_TEXT);
				}
				orderInfo.setProcessId(process.getId());//设置流程标示
				orderInfo.setInstanceUrl(process.getInstanceUrl());//设置流程实例URL
			
		}
	}
	/**
	 * 保存订单信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveOrderInfoInit() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
		    
		    orderInfo=new OrderInfo();
		    
		    //货币类型
		    contractClass=new ContractClass();
			contractClass.setCategory(ContractStatus.COMMON_MONEY_TYPE);
		    List list=this.iContractClassBiz.getContractClassList(contractClass);
		    this.getRequest().setAttribute("moneyTypeList", list);
		
		    List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1711);
			this.getRequest().setAttribute("dictionaryList", dictionaryList);
		    
		    sysCompany=this.iSysCompanyBiz.getSysCompany(comId);
		    orderInfo.setOrderPersonNameA(sysCompany.getCompName());
		    orderInfo.setOrderAddressA(sysCompany.getCompanyAddress());
		    orderInfo.setOrderTelA(sysCompany.getCompPhone());
		    orderInfo.setOrderFaxA(sysCompany.getCompFax());
		    
            this.getRequest().setAttribute("comId", comId);
            
            Users users=UserRightInfoUtil.getUsers(this.getRequest());
            String userChineseName=users.getUserChinesename();
            this.getRequest().setAttribute("userChineseName", userChineseName);
            
            Long deptId=users.getDepId();
            this.getRequest().setAttribute("deptId", deptId);
            
            String orderMobileA=users.getPhoneNum();
            this.getRequest().setAttribute("orderMobileA", orderMobileA);
            
            int type=Integer.parseInt(this.getRequest().getParameter("type"));
            //1为采购项目 2为合同信息
            if(type==1){
            	String rcId=this.getRequest().getParameter("rcId");
                Long baId=Long.parseLong(this.getRequest().getParameter("baId"));
		    	
		    	requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(rcId));
		    	orderInfo.setProjectContractName(requiredCollect.getBuyRemark());
		    	orderInfo.setBidCode(requiredCollect.getBidCode());
		    	
		    	orderInfo.setBaCiId(baId);
			    
		    	bidAward=this.iBidAwardBiz.getBidAward(baId);
		    	orderInfo.setOrderMoney(bidAward.getBidPrice());
		    	
		    	List<BidAwardDetail> badList=new ArrayList<BidAwardDetail>();
		    	BidAwardDetail bidAwardDetail=new BidAwardDetail();
				bidAwardDetail.setBaId(bidAward.getBaId());
				List<Object[]> objectList=this.iBidAwardDetailBiz.getBidAwardDetailListRequiredCollectDetail(bidAwardDetail);
				for(Object[] object:objectList){
					bidAwardDetail=(BidAwardDetail)object[0];
					requiredCollectDetail=(RequiredCollectDetail)object[1];
					bidAwardDetail.setRequiredCollectDetail(requiredCollectDetail);
					badList.add(bidAwardDetail);
				}
                this.getRequest().setAttribute("badList", badList);
				
				supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(bidAward.getSupplierId());
                orderInfo.setOrderNameB(supplierInfo.getSupplierName());
                orderInfo.setSupplierId(supplierInfo.getSupplierId());
                orderInfo.setOrderAddressB(supplierInfo.getSupplierAddress());
                orderInfo.setOrderPersonB(supplierInfo.getContactPerson());
                orderInfo.setOrderTelB(supplierInfo.getSupplierPhone());
                orderInfo.setOrderFaxB(supplierInfo.getSupplierFax());
                orderInfo.setOrderMobileB(supplierInfo.getMobilePhone());
                
            }else if(type==2){
            	String ciId=this.getRequest().getParameter("ciId");
            	contractInfo=this.iContractInfoBiz.getContractInfo(Long.parseLong(ciId));
            	orderInfo.setProjectContractName(contractInfo.getContractName());
            	orderInfo.setBidCode(contractInfo.getContractCode());
		    	orderInfo.setBaCiId(contractInfo.getCiId());
		    	orderInfo.setOrderMoney(contractInfo.getContractMoney());
            	orderInfo.setOrderNameB(contractInfo.getContractNameB());
                orderInfo.setSupplierId(contractInfo.getSupplierId());
                orderInfo.setOrderAddressB(contractInfo.getContractAddressB());
                orderInfo.setOrderPersonB(contractInfo.getContractPersonB());
                orderInfo.setOrderTelB(contractInfo.getContractTelB());
                orderInfo.setOrderFaxB(contractInfo.getContractFaxB());
                orderInfo.setOrderPersonNameA(contractInfo.getContractPersonNameA());
    		    orderInfo.setOrderAddressA(contractInfo.getContractAddressA());
    		    orderInfo.setOrderTelA(contractInfo.getContractTelA());
    		    orderInfo.setOrderFaxA(contractInfo.getContractFaxA());
    		    orderInfo.setOrderMobileB(contractInfo.getContractMobileB());
    		    
    		    contractMaterial=new ContractMaterial();
    		    contractMaterial.setCiId(contractInfo.getCiId());
    		    List cmList=this.iContractMaterialBiz.getContractMaterialList(contractMaterial);
                this.getRequest().setAttribute("cmList", cmList);
            }
            this.getRequest().setAttribute("type", type);
            
		} catch (Exception e) {
			log("保存订单信息信息初始化错误！", e);
			throw new BaseException("保存订单信息信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存订单信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveOrderInfo() throws BaseException {
		
		try{
			String writer=UserRightInfoUtil.getUserName(this.getRequest());
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            String rmCodePrefix = systemConfiguration.getOrderPrefix()+"-"+sdf.format(new Date());
			int floatCode = this.iOrderInfoBiz
					.getMaxFloatCodeByOrderCodePrefix(rmCodePrefix,comId);
			orderInfo.setOrderCode(systemConfiguration.getOrderPrefix()+"-"+sdf.format(new Date())
					+"-"+ (new DecimalFormat("0000").format(floatCode + 1)));
			orderInfo.setFloatCode((long) floatCode + 1);
			//编制人
            orderInfo.setWriter(writer);
	 	   
	 	    //编制日期 
            orderInfo.setWriteDate(DateUtil.getCurrentDateTime());
			 
	 	    //订单运行状态
            orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_01);
	 	    
	 	    //订单运行状态中文
            orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_01_Text);
            
			this.iOrderInfoBiz.saveOrderInfo(orderInfo);	
			Double contractAmount=0.00;
			// 保存合同物资明细数据
			String param=" ( 1=2 ",table="";
			if(orderInfo.getType()==1) table="t.bad_id=";
			else if(orderInfo.getType()==2) table="t.cm_id=";
			if (omList != null) {
				for (int i = 0; i < omList.size(); i++) {
					orderMaterial = omList.get(i);
					if (orderMaterial != null
							&& StringUtil.isNotBlank(orderMaterial.getMaterialCode().trim())) {
						orderMaterial.setOiId(orderInfo.getOiId());
						orderMaterial.setWaitInputAmount(orderMaterial.getAmount());
						orderMaterial.setRealInputAmount(0.00);
						orderMaterial.setWaitSendAmount(orderMaterial.getAmount());
						orderMaterial.setRealSendAmount(0.00);
						iOrderMaterialBiz.saveOrderMaterial(orderMaterial);
						contractAmount+=orderMaterial.getAmount();
						
						//组合授标或者合同明细sql条件
						param+=" or "+table+orderMaterial.getTableKey();
					}
				}
				param+=")";
			}
			orderInfo.setContractAmount(contractAmount);
			orderInfo.setRealInputAmount(0.00);
			orderInfo.setRealSendAmount(0.00);
			this.iOrderInfoBiz.updateOrderInfo(orderInfo);
			
			//如果是项目中生成的订单，需要更新授标表的是否生成订单
			if(orderInfo.getType()==1){
				this.iBidAwardBiz.updateBidAwardByOrder(param,orderInfo.getBaCiId());
			}else if(orderInfo.getType()==2){
				this.iContractInfoBiz.updateContractInfoByOrder(param,orderInfo.getBaCiId());
			}		
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增订单信息");
		} catch (Exception e) {
			log("保存订单信息信息错误！", e);
			throw new BaseException("保存订单信息信息错误！", e);
		}
		
		return "orderSuccess";
		
	}
	
	/**
	 * 修改订单信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateOrderInfoInit() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			//货币类型
			contractClass=new ContractClass();
			contractClass.setCategory(ContractStatus.COMMON_MONEY_TYPE);
		    List list=this.iContractClassBiz.getContractClassList(contractClass);
		    this.getRequest().setAttribute("moneyTypeList", list);
		    
		    List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1711);
			 this.getRequest().setAttribute("dictionaryList", dictionaryList);
		    
			orderInfo=this.iOrderInfoBiz.getOrderInfo(orderInfo.getOiId() );
			
			orderMaterial=new OrderMaterial();
			orderMaterial.setOiId(orderInfo.getOiId());
			omList=this.iOrderMaterialBiz.getOrderMaterialList(orderMaterial);
			
		} catch (Exception e) {
			log("修改订单信息信息初始化错误！", e);
			throw new BaseException("修改订单信息信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改订单信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateOrderInfo() throws BaseException {
		
		try{
			this.iOrderInfoBiz.updateOrderInfo(orderInfo);			
			Double contractAmount=0.00;
			if(orderInfo.getBaCiId()!=null){
				// 保存合同物资明细数据
				if (omList != null) {
					for (int i = 0; i < omList.size(); i++) {
						orderMaterial = omList.get(i);
						if (orderMaterial != null
								&& StringUtil.isNotBlank(orderMaterial.getMaterialCode().trim())) {
							orderMaterial.setWaitInputAmount(orderMaterial.getAmount());
							orderMaterial.setRealInputAmount(0.00);
							orderMaterial.setWaitSendAmount(orderMaterial.getAmount());
							orderMaterial.setRealSendAmount(0.00);
							iOrderMaterialBiz.updateOrderMaterial(orderMaterial);
						}
					}
				}
			}else{
				iOrderMaterialBiz.deleteOrderMaterialByCiId(orderInfo.getOiId());
				
				// 保存合同物资明细数据
				if (omList != null) {
					for (int i = 0; i < omList.size(); i++) {
						orderMaterial = omList.get(i);
						if (orderMaterial != null
								&& StringUtil.isNotBlank(orderMaterial.getMaterialCode().trim())) {
							orderMaterial.setOiId(orderInfo.getOiId());
							orderMaterial.setWaitInputAmount(orderMaterial.getAmount());
							orderMaterial.setRealInputAmount(0.00);
							orderMaterial.setWaitSendAmount(orderMaterial.getAmount());
							orderMaterial.setRealSendAmount(0.00);
							iOrderMaterialBiz.saveOrderMaterial(orderMaterial);
							
							contractAmount+=orderMaterial.getAmount();
						}
					}
				}
				orderInfo.setContractAmount(contractAmount);
				orderInfo.setRealInputAmount(0.00);
				orderInfo.setRealSendAmount(0.00);
				this.iOrderInfoBiz.updateOrderInfo(orderInfo);
			}
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改订单信息");
		} catch (Exception e) {
			log("修改订单信息信息错误！", e);
			throw new BaseException("修改订单信息信息错误！", e);
		}
		return "orderSuccess";
		
	}
	
	/**
	 * 删除订单信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteOrderInfo() throws BaseException {
		try{
		    OrderInfo orderInfo=new OrderInfo();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				orderInfo=this.iOrderInfoBiz.getOrderInfo(new Long(str[i]));
	 				this.iOrderInfoBiz.deleteOrderInfo(orderInfo);
	 				//如果是项目中生成的订单，需要更新授标表的是否生成订单
	 				if(orderInfo.getType()==1){
	 					bidAward=this.iBidAwardBiz.getBidAward(orderInfo.getBaCiId());
	 					bidAward.setIsOrder(1);
	 					this.iBidAwardBiz.updateBidAward(bidAward);
	 				}else if(orderInfo.getType()==2){
	 					contractInfo=this.iContractInfoBiz.getContractInfo(orderInfo.getBaCiId());
	 					contractInfo.setIsOrder(1);
	 					this.iContractInfoBiz.updateContractInfo(contractInfo);
	 				}	
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除订单信息");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除订单信息信息错误！", e);
			throw new BaseException("删除订单信息信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看订单信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewOrderInfoDetail() throws BaseException {
		String initPage="detail";
		try{
			String from = getRequest().getParameter("from");
			orderInfo=this.iOrderInfoBiz.getOrderInfo(orderInfo.getOiId() );
			
			orderMaterial=new OrderMaterial();
			orderMaterial.setOiId(orderInfo.getOiId());
			omList=this.iOrderMaterialBiz.getOrderMaterialList(orderMaterial);
			
			orderConfirm=new OrderConfirm();
			orderConfirm.setOiId(orderInfo.getOiId());
			List confirmList=this.iOrderConfirmBiz.getOrderConfirmList(orderConfirm);
			this.getRequest().setAttribute("confirmList", confirmList);
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("查看订单信息明细信息错误！", e);
			throw new BaseException("查看订单信息明细信息错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 订单不需要审批的时候提交订单信息
	 * @return
	 * @throws BaseException
	 */
    public String updateProcessOrderInfo() throws BaseException{
    	
    	try {
			String ids = this.getRequest().getParameter("ids");
			// 页面所选汇总明细信息
			String[] idss = ids.split(",");
			for (int i = 0; i < idss.length; i++) {
				if (idss[i] != null) {
					orderInfo= this.iOrderInfoBiz
							.getOrderInfo(new Long(idss[i]));
					orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_00);
					orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_00_Text);
					orderInfo.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iOrderInfoBiz.updateOrderInfo(orderInfo);
				}
			}
			PrintWriter out = this.getResponse().getWriter();
			String message = "提交成功";
			this.getRequest().setAttribute("message", "提交成功");
			this.getRequest().setAttribute("operModule", "提交订单成功");
			out.print(message);
		} catch (Exception e) {
			log("订单不需要审批的时候提交订单信息错误！", e);
			throw new BaseException("订单不需要审批的时候提交订单信息错误！", e);
		}
		return null;
    }
	/**
	 * 发送订单至供应商确认
	 * @return
	 * @throws BaseException 
	 */
	public String saveSendOrderInfo() throws BaseException {
		
		try{
		    OrderInfo orderInfo=new OrderInfo();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				orderInfo=this.iOrderInfoBiz.getOrderInfo(new Long(str[i]));
	 				orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_04);
	 				orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_04_Text);
	 				this.iOrderInfoBiz.updateOrderInfo(orderInfo);
	 				
	 				String content="待办提示：您有一个订单需要确认，订单名称为"+orderInfo.getOrderName()+"，订单金额为"+orderInfo.getOrderMoney();
	 				this.saveSmsMessageToUsers(orderInfo.getOrderMobileB(), content, orderInfo.getOrderUndertaker());
 				}
 			}
 			this.getRequest().setAttribute("message", "发送订单至供应商确认成功");
			this.getRequest().setAttribute("operModule", "发送订单至供应商确认订单信息");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("发送订单至供应商确认成功");
		} catch (Exception e) {
			log("发送订单至供应商确认错误！", e);
			throw new BaseException("发送订单至供应商确认错误！", e);
		}
		return null;
		
	}
	/**货单签收管理
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewInputOrderInfo() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("货单签收管理列表错误！", e);
			throw new BaseException("货单签收管理列表错误！", e);
		}	
			
		return "order_input" ;
				
	}
	/**
	 * 货单签收管理列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findInputBillOrderInfo() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String orderNameB=this.getRequest().getParameter("orderNameB");
			String orderCode=this.getRequest().getParameter("orderCode");
			String orderName=this.getRequest().getParameter("orderName");
			orderInfo=new OrderInfo();
			orderInfo.setOrderNameB(orderNameB);
			orderInfo.setOrderCode(orderCode);
			orderInfo.setOrderName(orderName);
			orderInfo.setComId(comId);
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = " and  de.orderUndertakerId="+UserRightInfoUtil.getUserId(this.getRequest());
			}
			List list=this.iOrderInfoBiz.getInputBillOrderInfoList(this.getRollPageDataTables(), orderInfo,sqlStr);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看货单签收信息列表错误！", e);
			throw new BaseException("查看货单签收信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 订单监督页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewOrderInfoSupervise() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			this.getRequest().setAttribute("orderWorkflow", systemConfiguration.getOrderWorkflow());
			this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.OD_WorkFlow_Type);
		} catch (Exception e) {
			log.error("订单监督页面错误！", e);
			throw new BaseException("订单监督页面错误！", e);
		}	
			
		return "orderInfoSupervise" ;
				
	}
	
	/**
	 * 查看订单监督信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findOrderInfoSupervise() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			String orderNameB=this.getRequest().getParameter("orderNameB");
			String orderCode=this.getRequest().getParameter("orderCode");
			String orderName=this.getRequest().getParameter("orderName");
			orderInfo=new OrderInfo();
			orderInfo.setOrderNameB(orderNameB);
			orderInfo.setOrderCode(orderCode);
			orderInfo.setOrderName(orderName);
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			List list=this.iOrderInfoBiz.getOrderInfoSuperviseList(this.getRollPageDataTables(), orderInfo,sqlStr);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					orderInfo = (OrderInfo) list.get(i);
					if(StringUtil.convertNullToBlank(systemConfiguration.getOrderWorkflow()).equals("0")){
						  setProcessLisOrder(orderInfo, WorkFlowStatus.Order_Work_Item,comId);
						}else{
							orderInfo.setOrderState(orderInfo.getRunStatus());
						}
				}
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看订单监督信息列表错误！", e);
			throw new BaseException("查看订单监督信息列表错误！", e);
		}
		
		return null ;
		
	}
	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public IOrderMaterialBiz getiOrderMaterialBiz() {
		return iOrderMaterialBiz;
	}

	public void setiOrderMaterialBiz(IOrderMaterialBiz iOrderMaterialBiz) {
		this.iOrderMaterialBiz = iOrderMaterialBiz;
	}

	public List<OrderMaterial> getOmList() {
		return omList;
	}

	public void setOmList(List<OrderMaterial> omList) {
		this.omList = omList;
	}

	public IContractClassBiz getiContractClassBiz() {
		return iContractClassBiz;
	}

	public void setiContractClassBiz(IContractClassBiz iContractClassBiz) {
		this.iContractClassBiz = iContractClassBiz;
	}

	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public ISysCompanyBiz getiSysCompanyBiz() {
		return iSysCompanyBiz;
	}

	public void setiSysCompanyBiz(ISysCompanyBiz iSysCompanyBiz) {
		this.iSysCompanyBiz = iSysCompanyBiz;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}

	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}

	public IBidAwardDetailBiz getiBidAwardDetailBiz() {
		return iBidAwardDetailBiz;
	}

	public void setiBidAwardDetailBiz(IBidAwardDetailBiz iBidAwardDetailBiz) {
		this.iBidAwardDetailBiz = iBidAwardDetailBiz;
	}

	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}

	public IContractMaterialBiz getiContractMaterialBiz() {
		return iContractMaterialBiz;
	}

	public void setiContractMaterialBiz(IContractMaterialBiz iContractMaterialBiz) {
		this.iContractMaterialBiz = iContractMaterialBiz;
	}

	public IOrderConfirmBiz getiOrderConfirmBiz() {
		return iOrderConfirmBiz;
	}

	public void setiOrderConfirmBiz(IOrderConfirmBiz iOrderConfirmBiz) {
		this.iOrderConfirmBiz = iOrderConfirmBiz;
	}	
}
