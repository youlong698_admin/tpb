package com.ced.sip.order.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.order.biz.IOrderTerminationBiz;
import com.ced.sip.order.entity.OrderTermination;
/** 
 * 类名称：OrderTerminationBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public class OrderTerminationBizImpl extends BaseBizImpl implements IOrderTerminationBiz  {
	
	/**
	 * 根据主键获得订单终止表表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public OrderTermination getOrderTermination(Long id) throws BaseException {
		return (OrderTermination)this.getObject(OrderTermination.class, id);
	}
	
	/**
	 * 获得订单终止表表实例
	 * @param orderTermination 订单终止表表实例
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public OrderTermination getOrderTermination(OrderTermination orderTermination) throws BaseException {
		return (OrderTermination)this.getObject(OrderTermination.class, orderTermination.getOtId() );
	}
	
	/**
	 * 添加订单终止表信息
	 * @param orderTermination 订单终止表表实例
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void saveOrderTermination(OrderTermination orderTermination) throws BaseException{
		this.saveObject( orderTermination ) ;
	}
	
	/**
	 * 更新订单终止表表实例
	 * @param orderTermination 订单终止表表实例
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void updateOrderTermination(OrderTermination orderTermination) throws BaseException{
		this.updateObject( orderTermination ) ;
	}
	
	/**
	 * 删除订单终止表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void deleteOrderTermination(String id) throws BaseException {
		this.removeObject( this.getOrderTermination( new Long(id) ) ) ;
	}
	
	/**
	 * 删除订单终止表表实例
	 * @param orderTermination 订单终止表表实例
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void deleteOrderTermination(OrderTermination orderTermination) throws BaseException {
		this.removeObject( orderTermination ) ;
	}
	
	/**
	 * 删除订单终止表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void deleteOrderTerminations(String[] id) throws BaseException {
		this.removeBatchObject(OrderTermination.class, id) ;
	}
	
	/**
	 * 获得订单终止表表数据集
	 * orderTermination 订单终止表表实例
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public OrderTermination getOrderTerminationByOrderTermination(OrderTermination orderTermination) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderTermination de where 1 = 1 " );

		hql.append(" order by de.otId desc ");
		List list = this.getObjects( hql.toString() );
		orderTermination = new OrderTermination();
		if(list!=null&&list.size()>0){
			orderTermination = (OrderTermination)list.get(0);
		}
		return orderTermination;
	}
	
	/**
	 * 获得所有订单终止表表数据集
	 * @param orderTermination 查询参数对象
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderTerminationList(OrderTermination orderTermination) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderTermination de where 1 = 1 " );
		if(orderTermination != null){
			if (StringUtil.isNotBlank(orderTermination.getOiId())) {
				hql.append(" and de.oiId = ").append(orderTermination.getOiId());
			}
			if (StringUtil.isNotBlank(orderTermination.getOtId())) {
				hql.append(" and de.otId = ").append(orderTermination.getOtId());
			}
		}
		hql.append(" order by de.otId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有订单终止表表数据集
	 * @param rollPage 分页对象
	 * @param orderTermination 查询参数对象
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderTerminationList(RollPage rollPage, OrderTermination orderTermination) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderTermination de where 1 = 1 " );
		if(orderTermination != null){
			if (StringUtil.isNotBlank(orderTermination.getOiId())) {
				hql.append(" and de.oiId = ").append(orderTermination.getOiId());
			}
			if (StringUtil.isNotBlank(orderTermination.getOtId())) {
				hql.append(" and de.otId = ").append(orderTermination.getOtId());
			}
		}
		hql.append(" order by de.otId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
