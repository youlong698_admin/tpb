package com.ced.sip.order.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.order.biz.IOrderMaterialBiz;
import com.ced.sip.order.entity.OrderMaterial;
/** 
 * 类名称：OrderMaterialBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class OrderMaterialBizImpl extends BaseBizImpl implements IOrderMaterialBiz  {
	
	/**
	 * 根据主键获得订单物资信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public OrderMaterial getOrderMaterial(Long id) throws BaseException {
		return (OrderMaterial)this.getObject(OrderMaterial.class, id);
	}
	
	/**
	 * 获得订单物资信息表实例
	 * @param orderMaterial 订单物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public OrderMaterial getOrderMaterial(OrderMaterial orderMaterial) throws BaseException {
		return (OrderMaterial)this.getObject(OrderMaterial.class, orderMaterial.getOmId() );
	}
	
	/**
	 * 添加订单物资信息信息
	 * @param orderMaterial 订单物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void saveOrderMaterial(OrderMaterial orderMaterial) throws BaseException{
		this.saveObject( orderMaterial ) ;
	}
	
	/**
	 * 更新订单物资信息表实例
	 * @param orderMaterial 订单物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void updateOrderMaterial(OrderMaterial orderMaterial) throws BaseException{
		this.updateObject( orderMaterial ) ;
	}
	
	/**
	 * 删除订单物资信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteOrderMaterial(String id) throws BaseException {
		this.removeObject( this.getOrderMaterial( new Long(id) ) ) ;
	}
	
	/**
	 * 删除订单物资信息表实例
	 * @param orderMaterial 订单物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteOrderMaterial(OrderMaterial orderMaterial) throws BaseException {
		this.removeObject( orderMaterial ) ;
	}
	
	/**
	 * 删除订单物资信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteOrderMaterials(String[] id) throws BaseException {
		this.removeBatchObject(OrderMaterial.class, id) ;
	}
	
	/**
	 * 获得订单物资信息表数据集
	 * orderMaterial 订单物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public OrderMaterial getOrderMaterialByOrderMaterial(OrderMaterial orderMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderMaterial de where 1 = 1 " );
		if(orderMaterial != null){
			if (StringUtil.isNotBlank(orderMaterial.getOiId())) {
				hql.append(" and de.oiId = ").append(orderMaterial.getOiId());
			}
		}
		hql.append(" order by de.omId desc ");
		List list = this.getObjects( hql.toString() );
		orderMaterial = new OrderMaterial();
		if(list!=null&&list.size()>0){
			orderMaterial = (OrderMaterial)list.get(0);
		}
		return orderMaterial;
	}
	
	/**
	 * 获得所有订单物资信息表数据集
	 * @param orderMaterial 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderMaterialList(OrderMaterial orderMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderMaterial de where 1 = 1 " );
		if(orderMaterial != null){
			if (StringUtil.isNotBlank(orderMaterial.getOiId())) {
				hql.append(" and de.oiId = ").append(orderMaterial.getOiId());
			}
		}
		hql.append(" order by de.omId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有订单物资信息表数据集
	 * @param rollPage 分页对象
	 * @param orderMaterial 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderMaterialList(RollPage rollPage, OrderMaterial orderMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderMaterial de where 1 = 1 " );
		if(orderMaterial != null){
			if (StringUtil.isNotBlank(orderMaterial.getOiId())) {
				hql.append(" and de.oiId = ").append(orderMaterial.getOiId());
			}
		}
		hql.append(" order by de.omId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 删除订单物资信息表实例    订单Id
	 * @param oiId 主键数组
	 * @throws BaseException 
	 */
	public void deleteOrderMaterialByCiId(Long oiId) throws BaseException {
		String sql="delete from order_material where oi_id="+oiId;
		this.updateJdbcSql(sql);	
	}

	/**
	 * 根据发货数量更新可订单明细中的发货数量
	 * @param sendAmount
	 * @param omId
	 * @throws BaseException
	 */
	public void updateSendAmount(Double sendAmount, Long omId)
			throws BaseException {
		String sql="update order_material set real_send_amount=real_send_amount+"+sendAmount+",wait_send_amount=wait_send_amount-"+sendAmount+" where om_id="+omId;
		//System.out.println(sql);
		this.updateJdbcSql(sql);
		
	}

	/**
	 * 根据收货数量更新可订单明细中的收货数量
	 * @param sendAmount
	 * @param omId
	 * @throws BaseException
	 */
	public void updateInputAmount(Double inputAmount, Long omId)
			throws BaseException {
		String sql="update order_material set real_input_amount=real_input_amount+"+inputAmount+",wait_input_amount=wait_input_amount-"+inputAmount+" where om_id="+omId;
		//System.out.println(sql);
		this.updateJdbcSql(sql);
		
	}
	
}
