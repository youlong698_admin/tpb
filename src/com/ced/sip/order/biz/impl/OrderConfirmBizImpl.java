package com.ced.sip.order.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.order.biz.IOrderConfirmBiz;
import com.ced.sip.order.entity.OrderConfirm;
/** 
 * 类名称：OrderConfirmBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public class OrderConfirmBizImpl extends BaseBizImpl implements IOrderConfirmBiz  {
	
	/**
	 * 根据主键获得订单供应商确认表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public OrderConfirm getOrderConfirm(Long id) throws BaseException {
		return (OrderConfirm)this.getObject(OrderConfirm.class, id);
	}
	
	/**
	 * 获得订单供应商确认表实例
	 * @param orderConfirm 订单供应商确认表实例
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public OrderConfirm getOrderConfirm(OrderConfirm orderConfirm) throws BaseException {
		return (OrderConfirm)this.getObject(OrderConfirm.class, orderConfirm.getOcId() );
	}
	
	/**
	 * 添加订单供应商确认信息
	 * @param orderConfirm 订单供应商确认表实例
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void saveOrderConfirm(OrderConfirm orderConfirm) throws BaseException{
		this.saveObject( orderConfirm ) ;
	}
	
	/**
	 * 更新订单供应商确认表实例
	 * @param orderConfirm 订单供应商确认表实例
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void updateOrderConfirm(OrderConfirm orderConfirm) throws BaseException{
		this.updateObject( orderConfirm ) ;
	}
	
	/**
	 * 删除订单供应商确认表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void deleteOrderConfirm(String id) throws BaseException {
		this.removeObject( this.getOrderConfirm( new Long(id) ) ) ;
	}
	
	/**
	 * 删除订单供应商确认表实例
	 * @param orderConfirm 订单供应商确认表实例
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void deleteOrderConfirm(OrderConfirm orderConfirm) throws BaseException {
		this.removeObject( orderConfirm ) ;
	}
	
	/**
	 * 删除订单供应商确认表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-05
	 * @throws BaseException 
	 */
	public void deleteOrderConfirms(String[] id) throws BaseException {
		this.removeBatchObject(OrderConfirm.class, id) ;
	}
	
	/**
	 * 获得订单供应商确认表数据集
	 * orderConfirm 订单供应商确认表实例
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public OrderConfirm getOrderConfirmByOrderConfirm(OrderConfirm orderConfirm) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderConfirm de where 1 = 1 " );

		hql.append(" order by de.ocId desc ");
		List list = this.getObjects( hql.toString() );
		orderConfirm = new OrderConfirm();
		if(list!=null&&list.size()>0){
			orderConfirm = (OrderConfirm)list.get(0);
		}
		return orderConfirm;
	}
	
	/**
	 * 获得所有订单供应商确认表数据集
	 * @param orderConfirm 查询参数对象
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderConfirmList(OrderConfirm orderConfirm) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderConfirm de where 1 = 1 " );
		if(orderConfirm != null){
			if (StringUtil.isNotBlank(orderConfirm.getOiId())) {
				hql.append(" and de.oiId = ").append(orderConfirm.getOiId());
			}
		}
		hql.append(" order by de.ocId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有订单供应商确认表数据集
	 * @param rollPage 分页对象
	 * @param orderConfirm 查询参数对象
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderConfirmList(RollPage rollPage, OrderConfirm orderConfirm) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderConfirm de where 1 = 1 " );
		if(orderConfirm != null){
			if (StringUtil.isNotBlank(orderConfirm.getOiId())) {
				hql.append(" and de.oiId = ").append(orderConfirm.getOiId());
			}
		}
		hql.append(" order by de.ocId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
