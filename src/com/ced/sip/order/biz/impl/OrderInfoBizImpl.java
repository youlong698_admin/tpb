package com.ced.sip.order.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.order.util.OrderStatus;
/** 
 * 类名称：OrderInfoBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class OrderInfoBizImpl extends BaseBizImpl implements IOrderInfoBiz  {
	
	/**
	 * 根据主键获得订单信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public OrderInfo getOrderInfo(Long id) throws BaseException {
		return (OrderInfo)this.getObject(OrderInfo.class, id);
	}
	
	/**
	 * 获得订单信息表实例
	 * @param orderInfo 订单信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public OrderInfo getOrderInfo(OrderInfo orderInfo) throws BaseException {
		return (OrderInfo)this.getObject(OrderInfo.class, orderInfo.getOiId() );
	}
	
	/**
	 * 添加订单信息信息
	 * @param orderInfo 订单信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void saveOrderInfo(OrderInfo orderInfo) throws BaseException{
		this.saveObject( orderInfo ) ;
	}
	
	/**
	 * 更新订单信息表实例
	 * @param orderInfo 订单信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void updateOrderInfo(OrderInfo orderInfo) throws BaseException{
		this.updateObject( orderInfo ) ;
	}
	
	/**
	 * 删除订单信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteOrderInfo(String id) throws BaseException {
		this.removeObject( this.getOrderInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除订单信息表实例
	 * @param orderInfo 订单信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteOrderInfo(OrderInfo orderInfo) throws BaseException {
		this.removeObject( orderInfo ) ;
	}
	
	/**
	 * 删除订单信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteOrderInfos(String[] id) throws BaseException {
		this.removeBatchObject(OrderInfo.class, id) ;
	}
	
	/**
	 * 获得订单信息表数据集
	 * orderInfo 订单信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public OrderInfo getOrderInfoByOrderInfo(OrderInfo orderInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderInfo de where 1 = 1 " );
		if(orderInfo != null){
			if (StringUtil.isNotBlank(orderInfo.getOrderCode())) {
				hql.append(" and de.orderCode like '%").append(orderInfo.getOrderCode()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderName())) {
				hql.append(" and de.orderName like '%").append(orderInfo.getOrderName()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderNameB())) {
				hql.append(" and de.orderNameB like '%").append(orderInfo.getOrderNameB()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.orderPersonNameA like '%").append(orderInfo.getOrderPersonNameA()).append("'");
			}
		}
		hql.append(" order by de.oiId desc ");
		List list = this.getObjects( hql.toString() );
		orderInfo = new OrderInfo();
		if(list!=null&&list.size()>0){
			orderInfo = (OrderInfo)list.get(0);
		}
		return orderInfo;
	}
	
	/**
	 * 获得所有订单信息表数据集
	 * @param orderInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderInfoList(OrderInfo orderInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderInfo de where 1 = 1 " );
		if(orderInfo != null){
			if (StringUtil.isNotBlank(orderInfo.getOrderCode())) {
				hql.append(" and de.orderCode like '%").append(orderInfo.getOrderCode()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderName())) {
				hql.append(" and de.orderName like '%").append(orderInfo.getOrderName()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderNameB())) {
				hql.append(" and de.orderNameB like '%").append(orderInfo.getOrderNameB()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.orderPersonNameA like '%").append(orderInfo.getOrderPersonNameA()).append("'");
			}
		}
		hql.append(" order by de.oiId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有订单信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @param sqlstr 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderInfoList(RollPage rollPage, OrderInfo orderInfo,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderInfo de where 1 = 1 " );
		if(orderInfo != null){
			if (StringUtil.isNotBlank(orderInfo.getOrderCode())) {
				hql.append(" and de.orderCode like '%").append(orderInfo.getOrderCode()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderName())) {
				hql.append(" and de.orderName like '%").append(orderInfo.getOrderName()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderNameB())) {
				hql.append(" and de.orderNameB like '%").append(orderInfo.getOrderNameB()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.orderPersonNameA like '%").append(orderInfo.getOrderPersonNameA()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.comId =").append(orderInfo.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有订单信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @param sqlStr 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderInfoSuperviseList(RollPage rollPage, OrderInfo orderInfo,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderInfo de where de.runStatus!='01' " );
		if(orderInfo != null){
			if (StringUtil.isNotBlank(orderInfo.getOrderCode())) {
				hql.append(" and de.orderCode like '%").append(orderInfo.getOrderCode()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderName())) {
				hql.append(" and de.orderName like '%").append(orderInfo.getOrderName()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderNameB())) {
				hql.append(" and de.orderNameB like '%").append(orderInfo.getOrderNameB()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.orderPersonNameA like '%").append(orderInfo.getOrderPersonNameA()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.comId =").append(orderInfo.getComId());
			}
		} 
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有订单信息表数据集   供应商端查询所用
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @author luguanglei 2017-08-05
	 * @return
	 * @throws BaseException 
	 */
	public List getOrderInfoListBySupplier(RollPage rollPage, OrderInfo orderInfo,int sign) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderInfo de where 1 = 1 " );
		if(orderInfo != null){
			if (StringUtil.isNotBlank(orderInfo.getOrderCode())) {
				hql.append(" and de.orderCode like '%").append(orderInfo.getOrderCode()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderName())) {
				hql.append(" and de.orderName like '%").append(orderInfo.getOrderName()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderNameB())) {
				hql.append(" and de.orderNameB like '%").append(orderInfo.getOrderNameB()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.orderPersonNameA like '%").append(orderInfo.getOrderPersonNameA()).append("'");
			}
		}
		if(sign==1) hql.append(" and de.runStatus='"+OrderStatus.ORDER_RUN_STATUS_04+"' ");
		else if(sign==3) hql.append(" and (de.runStatus='"+OrderStatus.ORDER_RUN_STATUS_05+"'  or de.runStatus='"+OrderStatus.ORDER_RUN_STATUS_07+"') ");
		hql.append(" and de.supplierId="+orderInfo.getSupplierId()+"");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有货单签收信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getInputBillOrderInfoList(RollPage rollPage, OrderInfo orderInfo,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from OrderInfo de where de.runStatus='"+OrderStatus.ORDER_RUN_STATUS_07+"' " );
		if(orderInfo != null){
			if (StringUtil.isNotBlank(orderInfo.getOrderCode())) {
				hql.append(" and de.orderCode like '%").append(orderInfo.getOrderCode()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderName())) {
				hql.append(" and de.orderName like '%").append(orderInfo.getOrderName()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderNameB())) {
				hql.append(" and de.orderNameB like '%").append(orderInfo.getOrderNameB()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.orderPersonNameA like '%").append(orderInfo.getOrderPersonNameA()).append("'");
			}
			if (StringUtil.isNotBlank(orderInfo.getOrderPersonNameA())) {
				hql.append(" and de.comId =").append(orderInfo.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 根据当前年获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	public int getMaxFloatCodeByOrderCodePrefix(String contractCodePrefix,Long comId)throws BaseException{
		int floatCode = 0;
		StringBuffer hql = new StringBuffer("select max(de.floatCode) from OrderInfo de where 1 = 1 " );
		hql.append(" and de.comId = ").append(comId).append("");
		hql.append(" and de.orderCode like '").append(contractCodePrefix).append("%'");
		
		List list = this.getObjects(hql.toString());
		if(list!=null && list.get(0)!= null){
			floatCode = Integer.parseInt(list.get(0).toString());
		}
		return floatCode;
	}
	/**
	 * 获得订单总金额
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  double sumOrderInfoMoney(Map<String,Object> mapParams,String sqlStr) throws BaseException {		
		StringBuffer hql = new StringBuffer("select sum(de.orderMoney) from OrderInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		hql.append(" and de.auditDate is not null ");
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		return (Double)this.sumOrMinOrMaxObjects(hql.toString(),Double.class);
	}
}
