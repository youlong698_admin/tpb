package com.ced.sip.order.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.order.entity.OrderConfirm;
/** 
 * 类名称：IOrderConfirmBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public interface IOrderConfirmBiz {

	/**
	 * 根据主键获得订单供应商确认表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderConfirm getOrderConfirm(Long id) throws BaseException;

	/**
	 * 添加订单供应商确认信息
	 * @param orderConfirm 订单供应商确认表实例
	 * @throws BaseException 
	 */
	abstract void saveOrderConfirm(OrderConfirm orderConfirm) throws BaseException;

	/**
	 * 更新订单供应商确认表实例
	 * @param orderConfirm 订单供应商确认表实例
	 * @throws BaseException 
	 */
	abstract void updateOrderConfirm(OrderConfirm orderConfirm) throws BaseException;

	/**
	 * 删除订单供应商确认表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderConfirm(String id) throws BaseException;

	/**
	 * 删除订单供应商确认表实例
	 * @param orderConfirm 订单供应商确认表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrderConfirm(OrderConfirm orderConfirm) throws BaseException;

	/**
	 * 删除订单供应商确认表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderConfirms(String[] id) throws BaseException;

	/**
	 * 获得订单供应商确认表数据集
	 * orderConfirm 订单供应商确认表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderConfirm getOrderConfirmByOrderConfirm(OrderConfirm orderConfirm) throws BaseException ;
	
	/**
	 * 获得所有订单供应商确认表数据集
	 * @param orderConfirm 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderConfirmList(OrderConfirm orderConfirm) throws BaseException ;
	
	/**
	 * 获得所有订单供应商确认表数据集
	 * @param rollPage 分页对象
	 * @param orderConfirm 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderConfirmList(RollPage rollPage, OrderConfirm orderConfirm)
			throws BaseException;

}