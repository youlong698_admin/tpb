package com.ced.sip.order.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.order.entity.OrderMaterial;
/** 
 * 类名称：IOrderMaterialBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public interface IOrderMaterialBiz {

	/**
	 * 根据主键获得订单物资信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderMaterial getOrderMaterial(Long id) throws BaseException;

	/**
	 * 添加订单物资信息信息
	 * @param orderMaterial 订单物资信息表实例
	 * @throws BaseException 
	 */
	abstract void saveOrderMaterial(OrderMaterial orderMaterial) throws BaseException;

	/**
	 * 更新订单物资信息表实例
	 * @param orderMaterial 订单物资信息表实例
	 * @throws BaseException 
	 */
	abstract void updateOrderMaterial(OrderMaterial orderMaterial) throws BaseException;

	/**
	 * 删除订单物资信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderMaterial(String id) throws BaseException;

	/**
	 * 删除订单物资信息表实例
	 * @param orderMaterial 订单物资信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrderMaterial(OrderMaterial orderMaterial) throws BaseException;

	/**
	 * 删除订单物资信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderMaterials(String[] id) throws BaseException;

	/**
	 * 获得订单物资信息表数据集
	 * orderMaterial 订单物资信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderMaterial getOrderMaterialByOrderMaterial(OrderMaterial orderMaterial) throws BaseException ;
	
	/**
	 * 获得所有订单物资信息表数据集
	 * @param orderMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderMaterialList(OrderMaterial orderMaterial) throws BaseException ;
	
	/**
	 * 获得所有订单物资信息表数据集
	 * @param rollPage 分页对象
	 * @param orderMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderMaterialList(RollPage rollPage, OrderMaterial orderMaterial)
			throws BaseException;
	/**
	 * 删除订单物资信息表实例    订单Id
	 * @param oiId 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderMaterialByCiId(Long oiId) throws BaseException;
	/**
	 * 根据发货数量更新可订单明细中的发货数量
	 * @param sendAmount
	 * @param omId
	 * @throws BaseException
	 */
	abstract void updateSendAmount(Double sendAmount,Long omId) throws BaseException;
	/**
	 * 根据收货数量更新可订单明细中的收货数量
	 * @param inputAmount
	 * @param omId
	 * @throws BaseException
	 */
	abstract void updateInputAmount(Double inputAmount,Long omId) throws BaseException;
}