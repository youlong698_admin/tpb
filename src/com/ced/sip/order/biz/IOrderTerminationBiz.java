package com.ced.sip.order.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.order.entity.OrderTermination;
/** 
 * 类名称：IOrderTerminationBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-05
 */
public interface IOrderTerminationBiz {

	/**
	 * 根据主键获得订单终止表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderTermination getOrderTermination(Long id) throws BaseException;

	/**
	 * 添加订单终止表信息
	 * @param orderTermination 订单终止表表实例
	 * @throws BaseException 
	 */
	abstract void saveOrderTermination(OrderTermination orderTermination) throws BaseException;

	/**
	 * 更新订单终止表表实例
	 * @param orderTermination 订单终止表表实例
	 * @throws BaseException 
	 */
	abstract void updateOrderTermination(OrderTermination orderTermination) throws BaseException;

	/**
	 * 删除订单终止表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderTermination(String id) throws BaseException;

	/**
	 * 删除订单终止表表实例
	 * @param orderTermination 订单终止表表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrderTermination(OrderTermination orderTermination) throws BaseException;

	/**
	 * 删除订单终止表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderTerminations(String[] id) throws BaseException;

	/**
	 * 获得订单终止表表数据集
	 * orderTermination 订单终止表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderTermination getOrderTerminationByOrderTermination(OrderTermination orderTermination) throws BaseException ;
	
	/**
	 * 获得所有订单终止表表数据集
	 * @param orderTermination 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderTerminationList(OrderTermination orderTermination) throws BaseException ;
	
	/**
	 * 获得所有订单终止表表数据集
	 * @param rollPage 分页对象
	 * @param orderTermination 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderTerminationList(RollPage rollPage, OrderTermination orderTermination)
			throws BaseException;

}