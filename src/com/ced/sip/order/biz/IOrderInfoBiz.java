package com.ced.sip.order.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.order.entity.OrderInfo;
/** 
 * 类名称：IOrderInfoBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public interface IOrderInfoBiz {

	/**
	 * 根据主键获得订单信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderInfo getOrderInfo(Long id) throws BaseException;

	/**
	 * 添加订单信息信息
	 * @param orderInfo 订单信息表实例
	 * @throws BaseException 
	 */
	abstract void saveOrderInfo(OrderInfo orderInfo) throws BaseException;

	/**
	 * 更新订单信息表实例
	 * @param orderInfo 订单信息表实例
	 * @throws BaseException 
	 */
	abstract void updateOrderInfo(OrderInfo orderInfo) throws BaseException;

	/**
	 * 删除订单信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderInfo(String id) throws BaseException;

	/**
	 * 删除订单信息表实例
	 * @param orderInfo 订单信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteOrderInfo(OrderInfo orderInfo) throws BaseException;

	/**
	 * 删除订单信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteOrderInfos(String[] id) throws BaseException;

	/**
	 * 获得订单信息表数据集
	 * orderInfo 订单信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract OrderInfo getOrderInfoByOrderInfo(OrderInfo orderInfo) throws BaseException ;
	
	/**
	 * 获得所有订单信息表数据集
	 * @param orderInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderInfoList(OrderInfo orderInfo) throws BaseException ;
	/**
	 * 获得所有订单信息表数据集   供应商端查询所用
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderInfoListBySupplier(RollPage rollPage, OrderInfo orderInfo,int sign)
			throws BaseException;
	
	/**
	 * 获得所有订单信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @param sqlStr 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderInfoList(RollPage rollPage, OrderInfo orderInfo,String sqlStr)
			throws BaseException;

	/**
	 * 获得所有订单监督信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @param sqlStr 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getOrderInfoSuperviseList(RollPage rollPage, OrderInfo orderInfo,String sqlStr)
			throws BaseException;
	
	/**
	 * 获得所有货单签收信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getInputBillOrderInfoList(RollPage rollPage, OrderInfo orderInfo,String sqlStr)
			throws BaseException;
	/**
	 * 根据当前年获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	abstract int getMaxFloatCodeByOrderCodePrefix(String orderCodePrefix,Long comId)throws BaseException;
	/**
	 * 获得订单总金额
	 * @param map 查询参数对象
	 * @param sqlStr 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract double sumOrderInfoMoney(Map<String,Object> mapParams,String sqlStr)
	         throws BaseException;
}