package com.ced.sip.capital.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.capital.biz.ICapitalPlanBiz;
import com.ced.sip.capital.entity.CapitalPlan;
import com.ced.sip.common.utils.StringUtil;
/** 
 * 类名称：CapitalPlanBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-10-13
 */
public class CapitalPlanBizImpl extends BaseBizImpl implements ICapitalPlanBiz  {
	
	/**
	 * 根据主键获得资金计划表实例
	 * @param id 主键
	 * @author luguanglei 2017-10-13
	 * @return
	 * @throws BaseException 
	 */
	public CapitalPlan getCapitalPlan(Long id) throws BaseException {
		return (CapitalPlan)this.getObject(CapitalPlan.class, id);
	}
	
	/**
	 * 获得资金计划表实例
	 * @param capitalPlan 资金计划表实例
	 * @author luguanglei 2017-10-13
	 * @return
	 * @throws BaseException 
	 */
	public CapitalPlan getCapitalPlan(CapitalPlan capitalPlan) throws BaseException {
		return (CapitalPlan)this.getObject(CapitalPlan.class, capitalPlan.getCpId() );
	}
	
	/**
	 * 添加资金计划信息
	 * @param capitalPlan 资金计划表实例
	 * @author luguanglei 2017-10-13
	 * @throws BaseException 
	 */
	public void saveCapitalPlan(CapitalPlan capitalPlan) throws BaseException{
		this.saveObject( capitalPlan ) ;
	}
	
	/**
	 * 更新资金计划表实例
	 * @param capitalPlan 资金计划表实例
	 * @author luguanglei 2017-10-13
	 * @throws BaseException 
	 */
	public void updateCapitalPlan(CapitalPlan capitalPlan) throws BaseException{
		this.updateObject( capitalPlan ) ;
	}
	
	/**
	 * 删除资金计划表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-10-13
	 * @throws BaseException 
	 */
	public void deleteCapitalPlan(String id) throws BaseException {
		this.removeObject( this.getCapitalPlan( new Long(id) ) ) ;
	}
	
	/**
	 * 删除资金计划表实例
	 * @param capitalPlan 资金计划表实例
	 * @author luguanglei 2017-10-13
	 * @throws BaseException 
	 */
	public void deleteCapitalPlan(CapitalPlan capitalPlan) throws BaseException {
		this.removeObject( capitalPlan ) ;
	}
	
	/**
	 * 删除资金计划表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-10-13
	 * @throws BaseException 
	 */
	public void deleteCapitalPlans(String[] id) throws BaseException {
		this.removeBatchObject(CapitalPlan.class, id) ;
	}
	
	/**
	 * 获得资金计划表数据集
	 * capitalPlan 资金计划表实例
	 * @author luguanglei 2017-10-13
	 * @return
	 * @throws BaseException 
	 */
	public CapitalPlan getCapitalPlanByCapitalPlan(CapitalPlan capitalPlan) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CapitalPlan de where 1 = 1 " );

		hql.append(" order by de.cpId desc ");
		List list = this.getObjects( hql.toString() );
		capitalPlan = new CapitalPlan();
		if(list!=null&&list.size()>0){
			capitalPlan = (CapitalPlan)list.get(0);
		}
		return capitalPlan;
	}
	
	/**
	 * 获得所有资金计划表数据集
	 * @param capitalPlan 查询参数对象
	 * @author luguanglei 2017-10-13
	 * @return
	 * @throws BaseException 
	 */
	public List getCapitalPlanList(CapitalPlan capitalPlan) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CapitalPlan de where 1 = 1 " );

		hql.append(" order by de.cpId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有资金计划表数据集
	 * @param rollPage 分页对象
	 * @param capitalPlan 查询参数对象
	 * @author luguanglei 2017-10-13
	 * @return
	 * @throws BaseException 
	 */
	public List getCapitalPlanList(RollPage rollPage, CapitalPlan capitalPlan,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CapitalPlan de where 1 = 1 " );
		if(capitalPlan != null){
			if (StringUtil.isNotBlank(capitalPlan.getPayCode())) {
				hql.append(" and de.payCode like '%").append(capitalPlan.getPayCode()).append("'");
			}
			if (StringUtil.isNotBlank(capitalPlan.getProjectContractName())) {
				hql.append(" and de.projectContractName like '%").append(capitalPlan.getProjectContractName()).append("'");
			}
			if (StringUtil.isNotBlank(capitalPlan.getSupplierName())) {
				hql.append(" and de.supplierName like '%").append(capitalPlan.getSupplierName()).append("'");
			}
			if (StringUtil.isNotBlank(capitalPlan.getComId())) {
				hql.append(" and de.comId= ").append(capitalPlan.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有资金计划监督数据集
	 * @param rollPage 分页对象
	 * @param capitalPlan 查询参数对象
	 * @author luguanglei 2017-10-13
	 * @return
	 * @throws BaseException 
	 */
	public List getCapitalPlanSuperviseList(RollPage rollPage, CapitalPlan capitalPlan,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CapitalPlan de where de.runStatus!='01' " );
		if(capitalPlan != null){
			if (StringUtil.isNotBlank(capitalPlan.getPayCode())) {
				hql.append(" and de.payCode like '%").append(capitalPlan.getPayCode()).append("'");
			}
			if (StringUtil.isNotBlank(capitalPlan.getProjectContractName())) {
				hql.append(" and de.projectContractName like '%").append(capitalPlan.getProjectContractName()).append("'");
			}
			if (StringUtil.isNotBlank(capitalPlan.getSupplierName())) {
				hql.append(" and de.supplierName like '%").append(capitalPlan.getSupplierName()).append("'");
			}
			if (StringUtil.isNotBlank(capitalPlan.getComId())) {
				hql.append(" and de.comId= ").append(capitalPlan.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 根据当前年获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	public int getMaxFloatCodeByPayCodePrefix(String payCodePrefix,Long comId)throws BaseException{
		int floatCode = 0;
		StringBuffer hql = new StringBuffer("select max(de.floatCode) from CapitalPlan de where 1 = 1 " );
		hql.append(" and de.comId = ").append(comId).append("");
		hql.append(" and de.payCode like '").append(payCodePrefix).append("%'");
		
		List list = this.getObjects(hql.toString());
		if(list!=null && list.get(0)!= null){
			floatCode = Integer.parseInt(list.get(0).toString());
		}
		return floatCode;
	}
}
