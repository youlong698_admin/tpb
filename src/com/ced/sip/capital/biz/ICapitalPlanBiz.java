package com.ced.sip.capital.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.capital.entity.CapitalPlan;
/** 
 * 类名称：ICapitalPlanBiz
 * 创建人：luguanglei 
 * 创建时间：2017-10-13
 */
public interface ICapitalPlanBiz {

	/**
	 * 根据主键获得资金计划表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract CapitalPlan getCapitalPlan(Long id) throws BaseException;

	/**
	 * 添加资金计划信息
	 * @param capitalPlan 资金计划表实例
	 * @throws BaseException 
	 */
	abstract void saveCapitalPlan(CapitalPlan capitalPlan) throws BaseException;

	/**
	 * 更新资金计划表实例
	 * @param capitalPlan 资金计划表实例
	 * @throws BaseException 
	 */
	abstract void updateCapitalPlan(CapitalPlan capitalPlan) throws BaseException;

	/**
	 * 删除资金计划表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteCapitalPlan(String id) throws BaseException;

	/**
	 * 删除资金计划表实例
	 * @param capitalPlan 资金计划表实例
	 * @throws BaseException 
	 */
	abstract void deleteCapitalPlan(CapitalPlan capitalPlan) throws BaseException;

	/**
	 * 删除资金计划表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteCapitalPlans(String[] id) throws BaseException;

	/**
	 * 获得资金计划表数据集
	 * capitalPlan 资金计划表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract CapitalPlan getCapitalPlanByCapitalPlan(CapitalPlan capitalPlan) throws BaseException ;
	
	/**
	 * 获得所有资金计划表数据集
	 * @param capitalPlan 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCapitalPlanList(CapitalPlan capitalPlan) throws BaseException ;
	
	/**
	 * 获得所有资金计划表数据集
	 * @param rollPage 分页对象
	 * @param capitalPlan 查询参数对象
	 * @param sqlStr 
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCapitalPlanList(RollPage rollPage, CapitalPlan capitalPlan,String sqlStr)
			throws BaseException;
	/**
	 * 获得所有资金计划监督数据集
	 * @param rollPage 分页对象
	 * @param capitalPlan 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCapitalPlanSuperviseList(RollPage rollPage, CapitalPlan capitalPlan,String sqlStr)
			throws BaseException;
	/**
	 * 根据当前年获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	abstract int getMaxFloatCodeByPayCodePrefix(String payCodePrefix,Long comId)throws BaseException;
	

}