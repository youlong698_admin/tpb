package com.ced.sip.capital.util;

public class CapitalStatus {
	//	-------------------------------------- 资金计划状态  -----------------------------------------
	/** 资金计划运行状态:已起草 "00" */
	public static final String CAPITAL_RUN_STATUS_01="01";
	
	/** 资金计划运行状态:已起草*/
	public static final String CAPITAL_RUN_STATUS_01_Text = "已起草";
	
	/** 资金计划运行状态:审批通过 "0" */
	public static final String CAPITAL_RUN_STATUS_00="0";
	
	/** 资金计划运行状态:审批通过*/
	public static final String CAPITAL_RUN_STATUS_00_Text = "审批通过";
	
	/** 资金计划运行状态:审批通过 "1" */
	public static final String CAPITAL_RUN_STATUS_1="1";
	
	/** 资金计划运行状态:审批通过*/
	public static final String CAPITAL_RUN_STATUS_1_Text = "审批中";
	
	/** 资金计划运行状态:审批退回 "03" */
	public static final String CAPITAL_RUN_STATUS_03="3";
	
	/** 资金计划运行状态:审批退回*/
	public static final String CAPITAL_RUN_STATUS_03_Text = "审批退回";
	
	/** 资金计划运行状态:已发送 "04" */
	public static final String CAPITAL_RUN_STATUS_04="4";
	
	/** 资金计划运行状态:已发送*/
	public static final String CAPITAL_RUN_STATUS_04_Text = "财务已支付";
	
}
