package com.ced.sip.capital.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：CapitalPlan
 * 创建人：luguanglei 
 * 创建时间：2017-10-13
 */
public class CapitalPlan extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long cpId;     //主键
	private int type;     //类型
	private Long baCiId;     //授权合同ID
	private String bidCode;	 //项目合同编号
	private String projectContractName;	 //项目合同名称
	private String payCode;	 //付款申请单编号
	private Double totalMoney;	//总金额
	private Double thisPaymentAmount;	//本次申请付款金额
	private String bankAccount;	 //银行账号
	private String bankAccountName;	 //银行户名
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String supplierPersonB;	 //供应商联系人
	private String supplierMobileB;	 //供应商联系人手机
	private String runStatus;	 //运行状态
	private String statusCn;	 //状态中文
	private String isUsable;	 //是否有效
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Long comId;     //
	private Long deptId;     //部门ID
	private Long floatCode;     //流水号
	private String payType;	 //支付方式
	private String bank;	 //开户银行
	private Date auditDate;
	private Long applicantDeptId;//申请单位Id
	private String applicantReson;//申请事由
	private String dutyParagraph;//税号
	
	private String writerCN;
	private String applicantDeptName;//申请单位
	
	private String orderId;//流程实例标示
	private String processId;//流程定义标示
	private String processName;//流程定义名称
	private String orderState;//流程状态
	private String orderStateName;//流程状态名称
	private String instanceUrl;//实例启动页面
	
	public CapitalPlan() {
		super();
	}
	
	public Long getCpId(){
	   return  cpId;
	} 
	public void setCpId(Long cpId) {
	   this.cpId = cpId;
    }     
	public Long getBaCiId(){
	   return  baCiId;
	} 
	public void setBaCiId(Long baCiId) {
	   this.baCiId = baCiId;
    }     
	public String getBidCode(){
	   return  bidCode;
	} 
	public void setBidCode(String bidCode) {
	   this.bidCode = bidCode;
    }
	public String getProjectContractName(){
	   return  projectContractName;
	} 
	public void setProjectContractName(String projectContractName) {
	   this.projectContractName = projectContractName;
    }
	public String getPayCode(){
	   return  payCode;
	} 
	public void setPayCode(String payCode) {
	   this.payCode = payCode;
    }
	public Double getTotalMoney(){
	   return  totalMoney;
	} 
	public void setTotalMoney(Double totalMoney) {
	   this.totalMoney = totalMoney;
    }	
	public Double getThisPaymentAmount(){
	   return  thisPaymentAmount;
	} 
	public void setThisPaymentAmount(Double thisPaymentAmount) {
	   this.thisPaymentAmount = thisPaymentAmount;
    }	
	public String getBankAccount(){
	   return  bankAccount;
	} 
	public void setBankAccount(String bankAccount) {
	   this.bankAccount = bankAccount;
    }
	public String getBankAccountName(){
	   return  bankAccountName;
	} 
	public void setBankAccountName(String bankAccountName) {
	   this.bankAccountName = bankAccountName;
    }
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getSupplierPersonB(){
	   return  supplierPersonB;
	} 
	public void setSupplierPersonB(String supplierPersonB) {
	   this.supplierPersonB = supplierPersonB;
    }
	public String getSupplierMobileB(){
	   return  supplierMobileB;
	} 
	public void setSupplierMobileB(String supplierMobileB) {
	   this.supplierMobileB = supplierMobileB;
    }
	public String getRunStatus(){
	   return  runStatus;
	} 
	public void setRunStatus(String runStatus) {
	   this.runStatus = runStatus;
    }
	public String getStatusCn(){
	   return  statusCn;
	} 
	public void setStatusCn(String statusCn) {
	   this.statusCn = statusCn;
    }
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }     
	public Long getDeptId(){
	   return  deptId;
	} 
	public void setDeptId(Long deptId) {
	   this.deptId = deptId;
    }     
	public Long getFloatCode(){
	   return  floatCode;
	} 
	public void setFloatCode(Long floatCode) {
	   this.floatCode = floatCode;
    }     
	public String getPayType(){
	   return  payType;
	} 
	public void setPayType(String payType) {
	   this.payType = payType;
    }
	public String getBank(){
	   return  bank;
	} 
	public void setBank(String bank) {
	   this.bank = bank;
    }

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getOrderStateName() {
		return orderStateName;
	}

	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getWriterCN() {
		return writerCN;
	}

	public void setWriterCN(String writerCN) {
		this.writerCN = writerCN;
	}

	public String getApplicantDeptName() {
		return applicantDeptName;
	}

	public void setApplicantDeptName(String applicantDeptName) {
		this.applicantDeptName = applicantDeptName;
	}

	public Long getApplicantDeptId() {
		return applicantDeptId;
	}

	public void setApplicantDeptId(Long applicantDeptId) {
		this.applicantDeptId = applicantDeptId;
	}

	public String getApplicantReson() {
		return applicantReson;
	}

	public void setApplicantReson(String applicantReson) {
		this.applicantReson = applicantReson;
	}

	public String getDutyParagraph() {
		return dutyParagraph;
	}

	public void setDutyParagraph(String dutyParagraph) {
		this.dutyParagraph = dutyParagraph;
	}
}