package com.ced.sip.capital.action.epp;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.HistoryTask;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.capital.biz.ICapitalPlanBiz;
import com.ced.sip.capital.entity.CapitalPlan;
import com.ced.sip.capital.util.CapitalStatus;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAward;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.requirement.biz.IRequiredMaterialBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.biz.IPurchaseDepartBuyerBiz;
import com.ced.sip.system.biz.ISysCompanyBiz;
import com.ced.sip.system.entity.PurchaseDepartBuyer;
import com.ced.sip.system.entity.SysCompany;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.workflow.base.entity.WFHistoryTask;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
/** 
 * 类名称：CapitalPlanAction
 * 创建人：luguanglei 
 * 创建时间：2017-10-13
 */
public class CapitalPlanAction extends BaseAction {

	@Autowired
	private SnakerEngineFacets facets;
	// 资金计划 
	private ICapitalPlanBiz iCapitalPlanBiz;
	//项目信息
	private IRequiredCollectBiz iRequiredCollectBiz;
	//授标信息
	private IBidAwardBiz iBidAwardBiz;
	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	//供应商服务类
	private ISupplierInfoBiz iSupplierInfoBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	//采购单位服务类
	private ISysCompanyBiz iSysCompanyBiz;
	//采购人采购部门服务类
	private IPurchaseDepartBuyerBiz iPurchaseDepartBuyerBiz;
	//需求计划服务类
	private IRequiredMaterialBiz iRequiredMaterialBiz;
	
	// 资金计划
	private CapitalPlan capitalPlan;
	// 授标
	private BidAward bidAward;
	
	private RequiredCollect requiredCollect;
	// 合同信息
	private ContractInfo contractInfo;
	
    private SupplierInfo supplierInfo;
	//采购员所管理的采购部门
	private PurchaseDepartBuyer purchaseDepartBuyer;
	
	/**
	 * 查看资金计划信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewCapitalPlan() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			this.getRequest().setAttribute("capitalPlanWorkflow", systemConfiguration.getCapitalPlanWorkflow());
			this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.CP_WorkFlow_Type);
		} catch (Exception e) {
			log.error("查看资金计划信息列表错误！", e);
			throw new BaseException("查看资金计划信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看资金计划信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findCapitalPlan() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			String payCode=this.getRequest().getParameter("payCode");
			String projectContractName=this.getRequest().getParameter("projectContractName");
			String supplierName=this.getRequest().getParameter("supplierName");
			capitalPlan=new CapitalPlan();
			capitalPlan.setPayCode(payCode);
			capitalPlan.setProjectContractName(projectContractName);
			capitalPlan.setSupplierName(supplierName);
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			List list=this.iCapitalPlanBiz.getCapitalPlanList(this.getRollPageDataTables(), capitalPlan,sqlStr);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					capitalPlan = (CapitalPlan) list.get(i);
					capitalPlan.setWriterCN(BaseDataInfosUtil
							.convertLoginNameToChnName(capitalPlan.getWriter()));
					capitalPlan.setApplicantDeptName(BaseDataInfosUtil.convertDeptIdToName(capitalPlan.getApplicantDeptId()));
					if(StringUtil.convertNullToBlank(systemConfiguration.getOrderWorkflow()).equals("0")){
						  setProcessLisOrder(capitalPlan, WorkFlowStatus.CapitalPlan_Work_Item,comId);
						}else{
							capitalPlan.setOrderState(capitalPlan.getRunStatus());
						}
				}
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看资金计划信息列表错误！", e);
			throw new BaseException("查看资金计划信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 设置计划流程相关列表
	 */
	private void setProcessLisOrder(CapitalPlan capitalPlan, String processName,Long comId){
		//step1：首先获取流程定义实体
		org.snaker.engine.entity.Process process = facets.getEngine().process().getProcessByName(processName,comId);
		//step2：1、判断流程定义是否存在，2、遍历集合添加流程实例
		if(process != null){
				String newOrderNo=WorkFlowStatus.CP_WorkFlow_Type+capitalPlan.getCpId().toString();
				//step3:判断流程实例是否运行
				QueryFilter filter = new QueryFilter();
               filter.setOrderNo(newOrderNo);
				filter.setProcessId(process.getId());
				List<HistoryOrder> holist = facets.getEngine().query().getHistoryOrders(filter);
				if(holist.size() > 0){
					//step5:获取正在运行的流程实例
					filter.setOrderNo(newOrderNo);
					filter.setProcessId(process.getId());
					List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
					if(orderlist.size() > 0){
						Order order = orderlist.get((orderlist.size()-1));
						if(order != null){
							List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
							if(tasklist.size() > 0){
								//设置当前流程名称
								capitalPlan.setProcessName(tasklist.get(0).getDisplayName());
							}
							capitalPlan.setOrderId(order.getId());//设置实例标示
						}
					}
					HistoryOrder ho = holist.get(0);
					capitalPlan.setOrderState(ho.getOrderState().toString());
					capitalPlan.setOrderStateName(WorkFlowStatusMap.getWorkflowOrderStatus(ho.getOrderState().toString()));
				}else{
					capitalPlan.setOrderStateName(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01_TEXT);
				}
				capitalPlan.setProcessId(process.getId());//设置流程标示
				capitalPlan.setInstanceUrl(process.getInstanceUrl());//设置流程实例URL
			
		}
	}
	/**
	 * 保存资金计划信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveCapitalPlanInit() throws BaseException {
		try{
            Long comId=UserRightInfoUtil.getComId(this.getRequest());
		    
		    capitalPlan=new CapitalPlan();  	    
		    List applicantDeptList=null;
		    
            this.getRequest().setAttribute("comId", comId);
            int type=Integer.parseInt(this.getRequest().getParameter("type"));
            //1为采购项目 2为合同信息
            if(type==1){
            	String rcId=this.getRequest().getParameter("rcId");
                Long baId=Long.parseLong(this.getRequest().getParameter("baId"));
		    	
		    	requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(rcId));
		    	capitalPlan.setProjectContractName(requiredCollect.getBuyRemark());
		    	capitalPlan.setBidCode(requiredCollect.getBidCode());
		    	
		    	capitalPlan.setBaCiId(baId);
			    
		    	bidAward=this.iBidAwardBiz.getBidAward(baId);
		    	capitalPlan.setThisPaymentAmount(bidAward.getBidPrice());		    	
				
				supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(bidAward.getSupplierId());
				capitalPlan.setSupplierName(supplierInfo.getSupplierName());
				capitalPlan.setSupplierId(supplierInfo.getSupplierId());
                capitalPlan.setSupplierPersonB(supplierInfo.getContactPerson());
                capitalPlan.setBank(supplierInfo.getBank());
                capitalPlan.setBankAccount(supplierInfo.getBankAccount());
                capitalPlan.setBankAccountName(supplierInfo.getBankAccountName());
                capitalPlan.setSupplierMobileB(supplierInfo.getMobilePhone());
                capitalPlan.setThisPaymentAmount(bidAward.getBidPrice());
                capitalPlan.setDutyParagraph(supplierInfo.getDutyParagraph());
                applicantDeptList=this.iRequiredMaterialBiz.getDeptListByRcId(bidAward.getRcId());
                
                
            }else if(type==2){
            	String ciId=this.getRequest().getParameter("ciId");
            	contractInfo=this.iContractInfoBiz.getContractInfo(Long.parseLong(ciId));
            	capitalPlan.setProjectContractName(contractInfo.getContractName());
            	capitalPlan.setBidCode(contractInfo.getContractCode());
		    	capitalPlan.setBaCiId(contractInfo.getCiId());
		    	capitalPlan.setThisPaymentAmount(contractInfo.getContractMoney());
                capitalPlan.setSupplierId(contractInfo.getSupplierId());
                capitalPlan.setSupplierPersonB(contractInfo.getContractPersonB());
                capitalPlan.setSupplierMobileB(contractInfo.getContractMobileB());
                
                supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(contractInfo.getSupplierId());
				capitalPlan.setSupplierName(supplierInfo.getSupplierName());
				capitalPlan.setSupplierId(supplierInfo.getSupplierId());
                capitalPlan.setBank(supplierInfo.getBank());
                capitalPlan.setBankAccount(supplierInfo.getBankAccount());
                capitalPlan.setBankAccountName(supplierInfo.getBankAccountName());
                capitalPlan.setThisPaymentAmount(contractInfo.getContractMoney());
                capitalPlan.setDutyParagraph(contractInfo.getDutyParagraph());
                applicantDeptList=this.iRequiredMaterialBiz.getDeptListByRcId(contractInfo.getRcId());
    		    
            }else{
            	Long userId=UserRightInfoUtil.getUserId(this.getRequest());
            	purchaseDepartBuyer=new PurchaseDepartBuyer();
            	purchaseDepartBuyer.setBuyerId(userId);
            	applicantDeptList=this.iPurchaseDepartBuyerBiz.getPurchaseDepartBuyerList(purchaseDepartBuyer);
            }
        	this.getRequest().setAttribute("applicantDeptList", applicantDeptList);
            this.getRequest().setAttribute("type", type);
		} catch (Exception e) {
			log("保存资金计划信息初始化错误！", e);
			throw new BaseException("保存资金计划信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存资金计划信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveCapitalPlan() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            String capitalPlanCodePrefix = systemConfiguration.getCapitalPlanPrefix()+"-"+sdf.format(new Date());
			int floatCode = this.iCapitalPlanBiz
					.getMaxFloatCodeByPayCodePrefix(capitalPlanCodePrefix,comId);
			capitalPlan.setPayCode(systemConfiguration.getCapitalPlanPrefix()+"-"+sdf.format(new Date())
					+"-"+ (new DecimalFormat("0000").format(floatCode + 1)));
			capitalPlan.setFloatCode((long) floatCode + 1);
			
            String writer=UserRightInfoUtil.getUserName(this.getRequest());
            //编制人
            capitalPlan.setWriter(writer);
            capitalPlan.setDeptId(UserRightInfoUtil.getUserDeptId(this.getRequest()));
            capitalPlan.setComId(comId);
	 	    //编制日期 
            capitalPlan.setWriteDate(DateUtil.getCurrentDateTime());
			 
	 	    //合同运行状态
            capitalPlan.setRunStatus(CapitalStatus.CAPITAL_RUN_STATUS_01);
	 	    
	 	    //合同运行状态中文
            capitalPlan.setStatusCn(CapitalStatus.CAPITAL_RUN_STATUS_01_Text);
            
			this.iCapitalPlanBiz.saveCapitalPlan(capitalPlan);	
			// 保存附件
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					capitalPlan, capitalPlan.getCpId(),
					AttachmentStatus.ATTACHMENT_CODE_1401, UserRightInfoUtil
							.getUserName(this.getRequest())));
			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增资金计划");
		} catch (Exception e) {
			log("保存资金计划信息错误！", e);
			throw new BaseException("保存资金计划信息错误！", e);
		}
		
		return "capitalPlanSuccess";
		
	}
	
	/**
	 * 修改资金计划信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateCapitalPlanInit() throws BaseException {
		
		try{
			capitalPlan=this.iCapitalPlanBiz.getCapitalPlan(capitalPlan.getCpId() );
			capitalPlan.setApplicantDeptName(BaseDataInfosUtil.convertDeptIdToName(capitalPlan.getApplicantDeptId()));
			// 获取附件
			Map<String, Object> map = iAttachmentBiz
					.getAttachmentMap(new Attachment(
							capitalPlan.getCpId(),
							AttachmentStatus.ATTACHMENT_CODE_1401));
			capitalPlan.setUuIdData((String) map.get("uuIdData"));
			capitalPlan.setFileNameData((String) map.get("fileNameData"));
			capitalPlan.setFileTypeData((String) map.get("fileTypeData"));
			capitalPlan.setAttIdData((String) map.get("attIdData"));
			capitalPlan.setAttIds((String) map.get("attIds"));
			
		} catch (Exception e) {
			log("修改资金计划信息初始化错误！", e);
			throw new BaseException("修改资金计划信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改资金计划信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateCapitalPlan() throws BaseException {
		
		try{
			this.iCapitalPlanBiz.updateCapitalPlan(capitalPlan);
			// 删除附件
			iAttachmentBiz.deleteAttachments( parseAttachIds( capitalPlan.getAttIds() ) ) ;
			//保存附件			
			iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( capitalPlan, capitalPlan.getCpId(), AttachmentStatus.ATTACHMENT_CODE_1401, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改资金计划");
		} catch (Exception e) {
			log("修改资金计划信息错误！", e);
			throw new BaseException("修改资金计划信息错误！", e);
		}
		return "capitalPlanSuccess";
		
	}
	
	/**
	 * 删除资金计划信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteCapitalPlan() throws BaseException {
		try{
		    CapitalPlan capitalPlan=new CapitalPlan();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				capitalPlan=this.iCapitalPlanBiz.getCapitalPlan(new Long(str[i]));
	 				this.iCapitalPlanBiz.deleteCapitalPlan(capitalPlan);
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除资金计划");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除资金计划信息错误！", e);
			throw new BaseException("删除资金计划信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看资金计划明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewCapitalPlanDetail() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			SysCompany sysCompany=this.iSysCompanyBiz.getSysCompany(comId);
			this.getRequest().setAttribute("sysCompany", sysCompany);
			
			capitalPlan=this.iCapitalPlanBiz.getCapitalPlan(capitalPlan.getCpId());
			capitalPlan.setWriterCN(BaseDataInfosUtil
					.convertLoginNameToChnName(capitalPlan.getWriter()));
			capitalPlan.setApplicantDeptName(BaseDataInfosUtil.convertDeptIdToName(capitalPlan.getApplicantDeptId()));
			// 获取附件
			capitalPlan.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									capitalPlan.getCpId(),
									AttachmentStatus.ATTACHMENT_CODE_1401)), "0",
							this.getRequest()));
			List<WFHistoryTask> wfhytList = new ArrayList<WFHistoryTask>();
			List<HistoryOrder> hoList=facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(WorkFlowStatus.CP_WorkFlow_Type+capitalPlan.getCpId()));
			if(hoList!=null&&hoList.size()>0){
				HistoryOrder hisorder =  facets.getEngine().query().getHistOrder(hoList.get(0).getId());
				HistoryTask hyTask=null;
				WFHistoryTask wfhyt=null;
				QueryFilter qfilter = new QueryFilter();
				qfilter.setOrderId(hisorder.getId());
				qfilter.setOrder("asc");
				qfilter.setOrderBy("finish_Time");
				List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(qfilter);
				for(Iterator iter = hytList.iterator();iter.hasNext();){
					hyTask = (HistoryTask)iter.next();
					wfhyt = new WFHistoryTask();
					BeanUtils.copyProperties(wfhyt, hyTask);
					wfhyt.setPerformType(hyTask.getVariableMap().get("result") == null ? "0" :hyTask.getVariableMap().get("result").toString());
					wfhyt.setVariable(hyTask.getVariableMap().get("signContent") == null ? "" :hyTask.getVariableMap().get("signContent").toString());
					wfhyt.setOperatorCn(BaseDataInfosUtil.convertLoginNameToChnName(wfhyt.getOperator()));
					wfhytList.add(wfhyt);				
				}
			}
			this.getRequest().setAttribute("hytList", wfhytList);
		} catch (Exception e) {
			log("查看资金计划明细信息错误！", e);
			throw new BaseException("查看资金计划明细信息错误！", e);
		}
		return DETAIL;
		
	}	
	/**
	 * 资金计划不需要审批的时候提交资金计划信息
	 * @return
	 * @throws BaseException
	 */
    public String updateProcessCapitalPlan() throws BaseException{
    	
    	try {
			String ids = this.getRequest().getParameter("ids");
			// 页面所选汇总明细信息
			String[] idss = ids.split(",");
			for (int i = 0; i < idss.length; i++) {
				if (idss[i] != null) {
					capitalPlan= this.iCapitalPlanBiz
							.getCapitalPlan(new Long(idss[i]));
					capitalPlan.setRunStatus(CapitalStatus.CAPITAL_RUN_STATUS_00);
					capitalPlan.setStatusCn(CapitalStatus.CAPITAL_RUN_STATUS_00_Text);
					capitalPlan.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iCapitalPlanBiz.updateCapitalPlan(capitalPlan);
				}
			}
			PrintWriter out = this.getResponse().getWriter();
			String message = "提交成功";
			this.getRequest().setAttribute("message", "提交成功");
			this.getRequest().setAttribute("operModule", "提交资金计划成功");
			out.print(message);
		} catch (Exception e) {
			log("资金计划不需要审批的时候提交资金计划信息错误！", e);
			throw new BaseException("资金计划不需要审批的时候提交资金计划信息错误！", e);
		}
		return null;
    }
    /**
	 * 资金监督页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewCapitalPlanSupervise() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			this.getRequest().setAttribute("capitalPlanWorkflow", systemConfiguration.getCapitalPlanWorkflow());
			this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.CP_WorkFlow_Type);
		} catch (Exception e) {
			log.error("资金监督页面信息列表错误！", e);
			throw new BaseException("资金监督页面信息列表错误！", e);
		}	
			
		return "capitalPlanSupervise" ;
				
	}
	
	/**
	 * 查看资金计划监督信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findCapitalPlanSupervise() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			String payCode=this.getRequest().getParameter("payCode");
			String projectContractName=this.getRequest().getParameter("projectContractName");
			String supplierName=this.getRequest().getParameter("supplierName");
			capitalPlan=new CapitalPlan();
			capitalPlan.setPayCode(payCode);
			capitalPlan.setProjectContractName(projectContractName);
			capitalPlan.setSupplierName(supplierName);
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameCapitalPlanHql(getRequest(),
						"de");
			}
			List list=this.iCapitalPlanBiz.getCapitalPlanSuperviseList(this.getRollPageDataTables(), capitalPlan,sqlStr);
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					capitalPlan = (CapitalPlan) list.get(i);
					capitalPlan.setWriterCN(BaseDataInfosUtil
							.convertLoginNameToChnName(capitalPlan.getWriter()));
					capitalPlan.setApplicantDeptName(BaseDataInfosUtil.convertDeptIdToName(capitalPlan.getApplicantDeptId()));
					if(StringUtil.convertNullToBlank(systemConfiguration.getOrderWorkflow()).equals("0")){
						  setProcessLisOrder(capitalPlan, WorkFlowStatus.CapitalPlan_Work_Item,comId);
						}else{
							capitalPlan.setOrderState(capitalPlan.getRunStatus());
						}
				}
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看资金计划监督信息列表错误！", e);
			throw new BaseException("查看资金计划监督信息列表错误！", e);
		}
		
		return null ;
		
	}
	public ICapitalPlanBiz getiCapitalPlanBiz() {
		return iCapitalPlanBiz;
	}

	public void setiCapitalPlanBiz(ICapitalPlanBiz iCapitalPlanBiz) {
		this.iCapitalPlanBiz = iCapitalPlanBiz;
	}

	public CapitalPlan getCapitalPlan() {
		return capitalPlan;
	}

	public void setCapitalPlan(CapitalPlan capitalPlan) {
		this.capitalPlan = capitalPlan;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}

	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}

	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}

	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public ISysCompanyBiz getiSysCompanyBiz() {
		return iSysCompanyBiz;
	}

	public void setiSysCompanyBiz(ISysCompanyBiz iSysCompanyBiz) {
		this.iSysCompanyBiz = iSysCompanyBiz;
	}

	public IPurchaseDepartBuyerBiz getiPurchaseDepartBuyerBiz() {
		return iPurchaseDepartBuyerBiz;
	}

	public void setiPurchaseDepartBuyerBiz(
			IPurchaseDepartBuyerBiz iPurchaseDepartBuyerBiz) {
		this.iPurchaseDepartBuyerBiz = iPurchaseDepartBuyerBiz;
	}

	public IRequiredMaterialBiz getiRequiredMaterialBiz() {
		return iRequiredMaterialBiz;
	}

	public void setiRequiredMaterialBiz(IRequiredMaterialBiz iRequiredMaterialBiz) {
		this.iRequiredMaterialBiz = iRequiredMaterialBiz;
	}
	
}
