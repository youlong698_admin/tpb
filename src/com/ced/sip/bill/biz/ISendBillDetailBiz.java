package com.ced.sip.bill.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.bill.entity.SendBillDetail;
/** 
 * 类名称：ISendBillDetailBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public interface ISendBillDetailBiz {

	/**
	 * 根据主键获得发货单物资明细表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SendBillDetail getSendBillDetail(Long id) throws BaseException;

	/**
	 * 添加发货单物资明细信息
	 * @param sendBillDetail 发货单物资明细表实例
	 * @throws BaseException 
	 */
	abstract void saveSendBillDetail(SendBillDetail sendBillDetail) throws BaseException;

	/**
	 * 更新发货单物资明细表实例
	 * @param sendBillDetail 发货单物资明细表实例
	 * @throws BaseException 
	 */
	abstract void updateSendBillDetail(SendBillDetail sendBillDetail) throws BaseException;

	/**
	 * 删除发货单物资明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSendBillDetail(String id) throws BaseException;

	/**
	 * 删除发货单物资明细表实例
	 * @param sendBillDetail 发货单物资明细表实例
	 * @throws BaseException 
	 */
	abstract void deleteSendBillDetail(SendBillDetail sendBillDetail) throws BaseException;

	/**
	 * 删除发货单物资明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSendBillDetails(String[] id) throws BaseException;

	/**
	 * 获得发货单物资明细表数据集
	 * sendBillDetail 发货单物资明细表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract SendBillDetail getSendBillDetailBySendBillDetail(SendBillDetail sendBillDetail) throws BaseException ;
	
	/**
	 * 获得所有发货单物资明细表数据集
	 * @param sendBillDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSendBillDetailList(SendBillDetail sendBillDetail) throws BaseException ;
	
	/**
	 * 获得所有发货单物资明细表数据集
	 * @param rollPage 分页对象
	 * @param sendBillDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSendBillDetailList(RollPage rollPage, SendBillDetail sendBillDetail)
			throws BaseException;
	/**
	 * 获得所有发货单物资明细表数据集  收货用
	 * @param ids 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSendBillDetailListForInputBill(String ids) throws BaseException ;

	/**
	 * 收货更新发货单物资明细表实例    
	 * @param sendBillDetail 发货单物资明细表实例
	 * @throws BaseException 
	 */
	abstract void updateInputBillDetail(SendBillDetail sendBillDetail) throws BaseException;

}