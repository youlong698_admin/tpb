package com.ced.sip.bill.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.bill.entity.InputBill;
/** 
 * 类名称：IInputBillBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-08
 */
public interface IInputBillBiz {

	/**
	 * 根据主键获得收货单管理表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract InputBill getInputBill(Long id) throws BaseException;

	/**
	 * 添加收货单管理信息
	 * @param inputBill 收货单管理表实例
	 * @throws BaseException 
	 */
	abstract void saveInputBill(InputBill inputBill) throws BaseException;

	/**
	 * 更新收货单管理表实例
	 * @param inputBill 收货单管理表实例
	 * @throws BaseException 
	 */
	abstract void updateInputBill(InputBill inputBill) throws BaseException;

	/**
	 * 删除收货单管理表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteInputBill(String id) throws BaseException;

	/**
	 * 删除收货单管理表实例
	 * @param inputBill 收货单管理表实例
	 * @throws BaseException 
	 */
	abstract void deleteInputBill(InputBill inputBill) throws BaseException;

	/**
	 * 删除收货单管理表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteInputBills(String[] id) throws BaseException;

	/**
	 * 获得收货单管理表数据集
	 * inputBill 收货单管理表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract InputBill getInputBillByInputBill(InputBill inputBill) throws BaseException ;
	
	/**
	 * 获得所有收货单管理表数据集
	 * @param inputBill 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getInputBillList(InputBill inputBill) throws BaseException ;
	
	/**
	 * 获得所有收货单管理表数据集
	 * @param rollPage 分页对象
	 * @param inputBill 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getInputBillList(RollPage rollPage, InputBill inputBill,Map<String, Object> map)
			throws BaseException;

}