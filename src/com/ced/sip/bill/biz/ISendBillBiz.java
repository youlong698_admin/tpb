package com.ced.sip.bill.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.bill.entity.SendBill;
/** 
 * 类名称：ISendBillBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public interface ISendBillBiz {

	/**
	 * 根据主键获得发货单管理表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SendBill getSendBill(Long id) throws BaseException;

	/**
	 * 添加发货单管理信息
	 * @param sendBill 发货单管理表实例
	 * @throws BaseException 
	 */
	abstract void saveSendBill(SendBill sendBill) throws BaseException;

	/**
	 * 更新发货单管理表实例
	 * @param sendBill 发货单管理表实例
	 * @throws BaseException 
	 */
	abstract void updateSendBill(SendBill sendBill) throws BaseException;

	/**
	 * 删除发货单管理表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSendBill(String id) throws BaseException;

	/**
	 * 删除发货单管理表实例
	 * @param sendBill 发货单管理表实例
	 * @throws BaseException 
	 */
	abstract void deleteSendBill(SendBill sendBill) throws BaseException;

	/**
	 * 删除发货单管理表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSendBills(String[] id) throws BaseException;

	/**
	 * 获得发货单管理表数据集
	 * sendBill 发货单管理表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract SendBill getSendBillBySendBill(SendBill sendBill) throws BaseException ;
	
	/**
	 * 获得所有发货单管理表数据集
	 * @param sendBill 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSendBillList(SendBill sendBill) throws BaseException ;
	
	/**
	 * 获得所有发货单管理表数据集
	 * @param rollPage 分页对象
	 * @param sendBill 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSendBillList(RollPage rollPage, SendBill sendBill,Map<String, Object> map)
			throws BaseException;
	

}