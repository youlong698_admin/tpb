package com.ced.sip.bill.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.bill.biz.IInputBillBiz;
import com.ced.sip.bill.entity.InputBill;
/** 
 * 类名称：InputBillBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-08
 */
public class InputBillBizImpl extends BaseBizImpl implements IInputBillBiz  {
	
	/**
	 * 根据主键获得收货单管理表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-08
	 * @return
	 * @throws BaseException 
	 */
	public InputBill getInputBill(Long id) throws BaseException {
		return (InputBill)this.getObject(InputBill.class, id);
	}
	
	/**
	 * 获得收货单管理表实例
	 * @param inputBill 收货单管理表实例
	 * @author luguanglei 2017-08-08
	 * @return
	 * @throws BaseException 
	 */
	public InputBill getInputBill(InputBill inputBill) throws BaseException {
		return (InputBill)this.getObject(InputBill.class, inputBill.getIbId() );
	}
	
	/**
	 * 添加收货单管理信息
	 * @param inputBill 收货单管理表实例
	 * @author luguanglei 2017-08-08
	 * @throws BaseException 
	 */
	public void saveInputBill(InputBill inputBill) throws BaseException{
		this.saveObject( inputBill ) ;
	}
	
	/**
	 * 更新收货单管理表实例
	 * @param inputBill 收货单管理表实例
	 * @author luguanglei 2017-08-08
	 * @throws BaseException 
	 */
	public void updateInputBill(InputBill inputBill) throws BaseException{
		this.updateObject( inputBill ) ;
	}
	
	/**
	 * 删除收货单管理表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-08
	 * @throws BaseException 
	 */
	public void deleteInputBill(String id) throws BaseException {
		this.removeObject( this.getInputBill( new Long(id) ) ) ;
	}
	
	/**
	 * 删除收货单管理表实例
	 * @param inputBill 收货单管理表实例
	 * @author luguanglei 2017-08-08
	 * @throws BaseException 
	 */
	public void deleteInputBill(InputBill inputBill) throws BaseException {
		this.removeObject( inputBill ) ;
	}
	
	/**
	 * 删除收货单管理表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-08
	 * @throws BaseException 
	 */
	public void deleteInputBills(String[] id) throws BaseException {
		this.removeBatchObject(InputBill.class, id) ;
	}
	
	/**
	 * 获得收货单管理表数据集
	 * inputBill 收货单管理表实例
	 * @author luguanglei 2017-08-08
	 * @return
	 * @throws BaseException 
	 */
	public InputBill getInputBillByInputBill(InputBill inputBill) throws BaseException {
		StringBuffer hql = new StringBuffer(" from InputBill de where 1 = 1 " );

		hql.append(" order by de.ibId desc ");
		List list = this.getObjects( hql.toString() );
		inputBill = new InputBill();
		if(list!=null&&list.size()>0){
			inputBill = (InputBill)list.get(0);
		}
		return inputBill;
	}
	
	/**
	 * 获得所有收货单管理表数据集
	 * @param inputBill 查询参数对象
	 * @author luguanglei 2017-08-08
	 * @return
	 * @throws BaseException 
	 */
	public List getInputBillList(InputBill inputBill) throws BaseException {
		StringBuffer hql = new StringBuffer(" from InputBill de where 1 = 1 " );

		hql.append(" order by de.ibId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有收货单管理表数据集
	 * @param rollPage 分页对象
	 * @param inputBill 查询参数对象
	 * @author luguanglei 2017-08-08
	 * @return
	 * @throws BaseException 
	 */
	public List getInputBillList(RollPage rollPage, InputBill inputBill,Map<String, Object> map) throws BaseException {
		StringBuffer hql = new StringBuffer(" from InputBill de where 1 = 1 " );
		hql.append(" and de.oiId = ").append(inputBill.getOiId());
		hql.append(" and de.type = ").append(inputBill.getType());
		
		if (StringUtil.isNotBlank(map.get("inputDate"))) {
			hql.append(" and de.inputDate = to_date('").append(map.get("inputDate")).append("','yyyy-MM-dd') ");
		}
		
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
