package com.ced.sip.bill.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.bill.biz.ISendBillBiz;
import com.ced.sip.bill.entity.SendBill;
/** 
 * 类名称：SendBillBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public class SendBillBizImpl extends BaseBizImpl implements ISendBillBiz  {
	
	/**
	 * 根据主键获得发货单管理表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public SendBill getSendBill(Long id) throws BaseException {
		return (SendBill)this.getObject(SendBill.class, id);
	}
	
	/**
	 * 获得发货单管理表实例
	 * @param sendBill 发货单管理表实例
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public SendBill getSendBill(SendBill sendBill) throws BaseException {
		return (SendBill)this.getObject(SendBill.class, sendBill.getSbId() );
	}
	
	/**
	 * 添加发货单管理信息
	 * @param sendBill 发货单管理表实例
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void saveSendBill(SendBill sendBill) throws BaseException{
		this.saveObject( sendBill ) ;
	}
	
	/**
	 * 更新发货单管理表实例
	 * @param sendBill 发货单管理表实例
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void updateSendBill(SendBill sendBill) throws BaseException{
		this.updateObject( sendBill ) ;
	}
	
	/**
	 * 删除发货单管理表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void deleteSendBill(String id) throws BaseException {
		this.removeObject( this.getSendBill( new Long(id) ) ) ;
	}
	
	/**
	 * 删除发货单管理表实例
	 * @param sendBill 发货单管理表实例
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void deleteSendBill(SendBill sendBill) throws BaseException {
		this.removeObject( sendBill ) ;
	}
	
	/**
	 * 删除发货单管理表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void deleteSendBills(String[] id) throws BaseException {
		this.removeBatchObject(SendBill.class, id) ;
	}
	
	/**
	 * 获得发货单管理表数据集
	 * sendBill 发货单管理表实例
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public SendBill getSendBillBySendBill(SendBill sendBill) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SendBill de where 1 = 1 " );

		hql.append(" order by de.sbId desc ");
		List list = this.getObjects( hql.toString() );
		sendBill = new SendBill();
		if(list!=null&&list.size()>0){
			sendBill = (SendBill)list.get(0);
		}
		return sendBill;
	}
	
	/**
	 * 获得所有发货单管理表数据集
	 * @param sendBill 查询参数对象
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public List getSendBillList(SendBill sendBill) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SendBill de where 1 = 1 " );

		hql.append(" order by de.sbId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有发货单管理表数据集
	 * @param rollPage 分页对象
	 * @param sendBill 查询参数对象
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public List getSendBillList(RollPage rollPage, SendBill sendBill,Map<String, Object> map) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SendBill de where 1 = 1 " );
		hql.append(" and de.oiId = ").append(sendBill.getOiId());
		hql.append(" and de.type = ").append(sendBill.getType());
		if (StringUtil.isNotBlank(map.get("sendDate"))) {
			hql.append(" and de.sendDate = to_date('").append(map.get("sendDate")).append("','yyyy-MM-dd') ");
		}
		if (StringUtil.isNotBlank(map.get("isInput"))) {
			hql.append(" and de.isInput= "+map.get("isInput"));
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
