package com.ced.sip.bill.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.bill.biz.ISendBillDetailBiz;
import com.ced.sip.bill.entity.SendBillDetail;
/** 
 * 类名称：SendBillDetailBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public class SendBillDetailBizImpl extends BaseBizImpl implements ISendBillDetailBiz  {
	
	/**
	 * 根据主键获得发货单物资明细表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public SendBillDetail getSendBillDetail(Long id) throws BaseException {
		return (SendBillDetail)this.getObject(SendBillDetail.class, id);
	}
	
	/**
	 * 获得发货单物资明细表实例
	 * @param sendBillDetail 发货单物资明细表实例
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public SendBillDetail getSendBillDetail(SendBillDetail sendBillDetail) throws BaseException {
		return (SendBillDetail)this.getObject(SendBillDetail.class, sendBillDetail.getSbdId() );
	}
	
	/**
	 * 添加发货单物资明细信息
	 * @param sendBillDetail 发货单物资明细表实例
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void saveSendBillDetail(SendBillDetail sendBillDetail) throws BaseException{
		this.saveObject( sendBillDetail ) ;
	}
	
	/**
	 * 更新发货单物资明细表实例
	 * @param sendBillDetail 发货单物资明细表实例
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void updateSendBillDetail(SendBillDetail sendBillDetail) throws BaseException{
		this.updateObject( sendBillDetail ) ;
	}
	
	/**
	 * 删除发货单物资明细表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void deleteSendBillDetail(String id) throws BaseException {
		this.removeObject( this.getSendBillDetail( new Long(id) ) ) ;
	}
	
	/**
	 * 删除发货单物资明细表实例
	 * @param sendBillDetail 发货单物资明细表实例
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void deleteSendBillDetail(SendBillDetail sendBillDetail) throws BaseException {
		this.removeObject( sendBillDetail ) ;
	}
	
	/**
	 * 删除发货单物资明细表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-06
	 * @throws BaseException 
	 */
	public void deleteSendBillDetails(String[] id) throws BaseException {
		this.removeBatchObject(SendBillDetail.class, id) ;
	}
	
	/**
	 * 获得发货单物资明细表数据集
	 * sendBillDetail 发货单物资明细表实例
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public SendBillDetail getSendBillDetailBySendBillDetail(SendBillDetail sendBillDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SendBillDetail de where 1 = 1 " );

		hql.append(" order by de.sbdId desc ");
		List list = this.getObjects( hql.toString() );
		sendBillDetail = new SendBillDetail();
		if(list!=null&&list.size()>0){
			sendBillDetail = (SendBillDetail)list.get(0);
		}
		return sendBillDetail;
	}
	
	/**
	 * 获得所有发货单物资明细表数据集
	 * @param sendBillDetail 查询参数对象
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public List getSendBillDetailList(SendBillDetail sendBillDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SendBillDetail de where 1 = 1 " );
		if(sendBillDetail != null){
			if (StringUtil.isNotBlank(sendBillDetail.getSbId())) {
				hql.append(" and de.sbId = ").append(sendBillDetail.getSbId());
			}
			if (StringUtil.isNotBlank(sendBillDetail.getIbId())) {
				hql.append(" and de.ibId = ").append(sendBillDetail.getIbId());
			}
		}
		hql.append(" order by de.sbdId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有发货单物资明细表数据集
	 * @param rollPage 分页对象
	 * @param sendBillDetail 查询参数对象
	 * @author luguanglei 2017-08-06
	 * @return
	 * @throws BaseException 
	 */
	public List getSendBillDetailList(RollPage rollPage, SendBillDetail sendBillDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SendBillDetail de where 1 = 1 " );

		hql.append(" order by de.sbdId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 获得所有发货单物资明细表数据集  收货用
	 * @param ids 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getSendBillDetailListForInputBill(String ids)
			throws BaseException {
		StringBuffer hql = new StringBuffer("select de,sb.sendDate from SendBillDetail de,SendBill sb where sb.sbId=de.sbId and ( sb.sbId=0 " );
		String[] str=ids.split(",");
		for(String id:str){
			hql.append(" or sb.sbId="+id);
		}
		hql.append(" ) ");
		hql.append(" order by de.sbdId desc ");
		return this.getObjects( hql.toString() );
	}

	/**
	 * 收货更新发货单物资明细表实例    
	 * @param sendBillDetail 发货单物资明细表实例
	 * @throws BaseException 
	 */
	public void updateInputBillDetail(SendBillDetail sendBillDetail)
			throws BaseException {
		String sql="update send_bill_detail set input_amount="+sendBillDetail.getInputAmount()+",input_remark='"+sendBillDetail.getInputRemark()+"',ib_id="+sendBillDetail.getIbId()+" where sbd_id="+sendBillDetail.getSbdId();
		this.updateJdbcSql(sql);
		
	}
	
}
