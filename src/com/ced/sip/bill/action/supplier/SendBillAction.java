package com.ced.sip.bill.action.supplier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.bill.biz.ISendBillBiz;
import com.ced.sip.bill.biz.ISendBillDetailBiz;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.bill.entity.SendBill;
import com.ced.sip.bill.entity.SendBillDetail;
/** 
 * 类名称：SendBillAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-07
 */
public class SendBillAction extends BaseAction {

	// 发货单信息 
	private ISendBillBiz iSendBillBiz;
	//发货单明细
	private ISendBillDetailBiz iSendBillDetailBiz;
	//附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// 订单信息 
	private IOrderInfoBiz iOrderInfoBiz;
	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	
	// 发货单信息
	private SendBill sendBill;
	//发货单明细信息
	private SendBillDetail sendBillDetail;
	
	private OrderInfo orderInfo;
	
	private ContractInfo contractInfo;
	
	/**
	 * 查看发货单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSendBill() throws BaseException {		
		try{
			Long ociId=Long.parseLong(this.getRequest().getParameter("ociId"));
			int type=Integer.parseInt(this.getRequest().getParameter("type"));
			if(type==1){
				orderInfo=this.iOrderInfoBiz.getOrderInfo(ociId);
				this.getRequest().setAttribute("countAmount", orderInfo.getContractAmount());
			}else{
				contractInfo=this.iContractInfoBiz.getContractInfo(ociId);
				this.getRequest().setAttribute("countAmount", contractInfo.getContractAmount());
			}
			this.getRequest().setAttribute("oiId", ociId);
			this.getRequest().setAttribute("type", type);
			
		} catch (Exception e) {
			log.error("查看发货单信息信息列表错误！", e);
			throw new BaseException("查看发货单信息信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看发货单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findSendBill() throws BaseException {
		
		try{
			String sendDate=this.getRequest().getParameter("sendDate");
			Long oiId=Long.parseLong(this.getRequest().getParameter("oiId"));
			int type=Integer.parseInt(this.getRequest().getParameter("type"));
			sendBill=new SendBill();
			sendBill.setOiId(oiId);
			sendBill.setType(type);
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("sendDate", sendDate);
			
			List list=this.iSendBillBiz.getSendBillList(this.getRollPageDataTables(), sendBill,map);
			this.getPagejsonDataTables(list);
			
			
		} catch (Exception e) {
			log.error("查看发货单信息信息列表错误！", e);
			throw new BaseException("查看发货单信息信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 查看发货单信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSendBillDetail() throws BaseException {
		
		try{
			sendBill=this.iSendBillBiz.getSendBill(sendBill.getSbId() );
			//获取附件
			sendBill.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( sendBill.getSbId(), AttachmentStatus.ATTACHMENT_CODE_1201 ) ) , "0", this.getRequest() ) ) ;
			
			sendBillDetail=new SendBillDetail();
			sendBillDetail.setSbId(sendBill.getSbId());
			List<SendBillDetail> sbdList=this.iSendBillDetailBiz.getSendBillDetailList(sendBillDetail);
			this.getRequest().setAttribute("sbdList", sbdList);
			
		} catch (Exception e) {
			log("查看发货单信息明细信息错误！", e);
			throw new BaseException("查看发货单信息明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public SendBill getSendBill() {
		return sendBill;
	}

	public void setSendBill(SendBill sendBill) {
		this.sendBill = sendBill;
	}

	public ISendBillBiz getiSendBillBiz() {
		return iSendBillBiz;
	}

	public void setiSendBillBiz(ISendBillBiz iSendBillBiz) {
		this.iSendBillBiz = iSendBillBiz;
	}

	public ISendBillDetailBiz getiSendBillDetailBiz() {
		return iSendBillDetailBiz;
	}

	public void setiSendBillDetailBiz(ISendBillDetailBiz iSendBillDetailBiz) {
		this.iSendBillDetailBiz = iSendBillDetailBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}

	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}
	
}
