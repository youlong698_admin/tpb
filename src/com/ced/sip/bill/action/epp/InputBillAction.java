package com.ced.sip.bill.action.epp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.bill.biz.IInputBillBiz;
import com.ced.sip.bill.biz.ISendBillDetailBiz;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.bill.entity.InputBill;
import com.ced.sip.bill.entity.SendBillDetail;
/** 
 * 类名称：InputBillAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-08
 */
public class InputBillAction extends BaseAction {

	// 收货单信息 
	private IInputBillBiz iInputBillBiz;
	//收货单明细
	private ISendBillDetailBiz iSendBillDetailBiz;
	//附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// 订单信息 
	private IOrderInfoBiz iOrderInfoBiz;
	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	
	// 收货单信息
	private InputBill inputBill;
	//收货单明细信息
	private SendBillDetail sendBillDetail;
	
	private OrderInfo orderInfo;
	
	private ContractInfo contractInfo;
	/**
	 * 查看收货单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewInputBill() throws BaseException {		
		try{
			Long ociId=Long.parseLong(this.getRequest().getParameter("ociId"));
			int type=Integer.parseInt(this.getRequest().getParameter("type"));
			if(type==1){
				orderInfo=this.iOrderInfoBiz.getOrderInfo(ociId);
				this.getRequest().setAttribute("countAmount", orderInfo.getContractAmount());
			}else{
				contractInfo=this.iContractInfoBiz.getContractInfo(ociId);
				this.getRequest().setAttribute("countAmount", contractInfo.getContractAmount());
			}
			this.getRequest().setAttribute("ociId", ociId);
			this.getRequest().setAttribute("type", type);
			
		} catch (Exception e) {
			log.error("查看收货单信息信息列表错误！", e);
			throw new BaseException("查看收货单信息信息列表错误！", e);
		}	
			
		return VIEW;
				
	}
	/**
	 * 查看收货单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findInputBill() throws BaseException {
		
		try{
			String sendDate=this.getRequest().getParameter("sendDate");
			Long oiId=Long.parseLong(this.getRequest().getParameter("oiId"));
			int type=Integer.parseInt(this.getRequest().getParameter("type"));
			inputBill=new InputBill();
			inputBill.setOiId(oiId);
			inputBill.setType(type);
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("sendDate", sendDate);
			
			List list=this.iInputBillBiz.getInputBillList(this.getRollPageDataTables(), inputBill,map);
			this.getPagejsonDataTables(list);
			
			
		} catch (Exception e) {
			log.error("查看收货单信息信息列表错误！", e);
			throw new BaseException("查看收货单信息信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 查看收货单信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewInputBillDetail() throws BaseException {
		
		try{
			inputBill=this.iInputBillBiz.getInputBill(inputBill.getIbId() );
			//获取附件
			inputBill.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( inputBill.getIbId(), AttachmentStatus.ATTACHMENT_CODE_1301 ) ) , "0", this.getRequest() ) ) ;
			
			sendBillDetail=new SendBillDetail();
			sendBillDetail.setIbId(inputBill.getIbId());
			List<SendBillDetail> sbdList=this.iSendBillDetailBiz.getSendBillDetailList(sendBillDetail);
			this.getRequest().setAttribute("sbdList", sbdList);
			
		} catch (Exception e) {
			log("查看收货单信息明细信息错误！", e);
			throw new BaseException("查看收货单信息明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public InputBill getInputBill() {
		return inputBill;
	}

	public void setInputBill(InputBill inputBill) {
		this.inputBill = inputBill;
	}

	public IInputBillBiz getiInputBillBiz() {
		return iInputBillBiz;
	}

	public void setiInputBillBiz(IInputBillBiz iInputBillBiz) {
		this.iInputBillBiz = iInputBillBiz;
	}

	public ISendBillDetailBiz getiSendBillDetailBiz() {
		return iSendBillDetailBiz;
	}

	public void setiSendBillDetailBiz(ISendBillDetailBiz iSendBillDetailBiz) {
		this.iSendBillDetailBiz = iSendBillDetailBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}
	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}
	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}
	
}
