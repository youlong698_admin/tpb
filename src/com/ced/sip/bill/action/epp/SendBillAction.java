package com.ced.sip.bill.action.epp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.biz.IContractMaterialBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.contract.util.ContractStatus;
import com.ced.sip.bill.biz.IInputBillBiz;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.order.biz.IOrderMaterialBiz;
import com.ced.sip.bill.biz.ISendBillBiz;
import com.ced.sip.bill.biz.ISendBillDetailBiz;
import com.ced.sip.bill.entity.InputBill;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.bill.entity.SendBill;
import com.ced.sip.bill.entity.SendBillDetail;
import com.ced.sip.order.util.OrderStatus;
/** 
 * 类名称：SendBillAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-08
 */
public class SendBillAction extends BaseAction {

	// 发货单信息 
	private ISendBillBiz iSendBillBiz;
	// 收货单信息 
	private IInputBillBiz iInputBillBiz;
	//发货单明细
	private ISendBillDetailBiz iSendBillDetailBiz;
	//附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// 订单信息 
	private IOrderInfoBiz iOrderInfoBiz;
	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	//订单明细信息
	private IOrderMaterialBiz iOrderMaterialBiz;
	//合同明细信息
	private IContractMaterialBiz iContractMaterialBiz;
	
	// 发货单信息
	private SendBill sendBill;
	// 收货单信息
	private InputBill inputBill;
	//发货单明细信息
	private SendBillDetail sendBillDetail;

	private ContractInfo contractInfo;
	
	private OrderInfo orderInfo;
	
	private List<SendBillDetail> sbdList;
	
	/**
	 * 查看发货单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSendBill() throws BaseException {		
		try{
			Long ociId=Long.parseLong(this.getRequest().getParameter("ociId"));
			int type=Integer.parseInt(this.getRequest().getParameter("type"));
			if(type==1){
				orderInfo=this.iOrderInfoBiz.getOrderInfo(ociId);
				this.getRequest().setAttribute("countAmount", orderInfo.getContractAmount());
			}else{
				contractInfo=this.iContractInfoBiz.getContractInfo(ociId);
				this.getRequest().setAttribute("countAmount", contractInfo.getContractAmount());
			}
			this.getRequest().setAttribute("ociId", ociId);
			this.getRequest().setAttribute("type", type);
			
		} catch (Exception e) {
			log.error("查看发货单信息信息列表错误！", e);
			throw new BaseException("查看发货单信息信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看发货单信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findSendBill() throws BaseException {
		
		try{
			String sendDate=this.getRequest().getParameter("sendDate");
			Long oiId=Long.parseLong(this.getRequest().getParameter("oiId"));
			int type=Integer.parseInt(this.getRequest().getParameter("type"));
			sendBill=new SendBill();
			sendBill.setOiId(oiId);
			sendBill.setType(type);
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("sendDate", sendDate);
			map.put("isInput", TableStatus.COMMON_INT_1);
			
			List list=this.iSendBillBiz.getSendBillList(this.getRollPageDataTables(), sendBill,map);
			this.getPagejsonDataTables(list);
			
			
		} catch (Exception e) {
			log.error("查看发货单信息信息列表错误！", e);
			throw new BaseException("查看发货单信息信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 查看发货单信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSendBillDetail() throws BaseException {
		
		try{
			sendBill=this.iSendBillBiz.getSendBill(sendBill.getSbId() );
			//获取附件
			sendBill.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( sendBill.getSbId(), AttachmentStatus.ATTACHMENT_CODE_1201 ) ) , "0", this.getRequest() ) ) ;
			
			sendBillDetail=new SendBillDetail();
			sendBillDetail.setSbId(sendBill.getSbId());
			List<SendBillDetail> sbdList=this.iSendBillDetailBiz.getSendBillDetailList(sendBillDetail);
			this.getRequest().setAttribute("sbdList", sbdList);
			
		} catch (Exception e) {
			log("查看发货单信息明细信息错误！", e);
			throw new BaseException("查看发货单信息明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 订单收货信息初始化
	 * @return
	 * @throws BaseException
	 */
    public String updateOrderInputBillInit() throws BaseException{
    	try {
			String ids=this.getRequest().getParameter("ids");
			
			List<SendBillDetail> sbdList=new ArrayList<SendBillDetail>();
			List<Object[]> list=this.iSendBillDetailBiz.getSendBillDetailListForInputBill(ids);
			for(Object[] obj:list){
				sendBillDetail=(SendBillDetail)obj[0];
				sendBillDetail.setSendDate((Date)obj[1]);
				sbdList.add(sendBillDetail);
			}
			this.getRequest().setAttribute("sbdList", sbdList);

			Long oiId=Long.parseLong(this.getRequest().getParameter("oiId"));
			orderInfo=this.iOrderInfoBiz.getOrderInfo(oiId);
			this.getRequest().setAttribute("ids", ids);
			this.getRequest().setAttribute("orderInfo", orderInfo);
			this.getRequest().setAttribute("today", DateUtil.getDefaultDateFormat(new Date()));
		}catch (Exception e) {
			log("订单收货信息初始化错误！", e);
			throw new BaseException("订单收货信息初始化错误！", e);
		}
		return "updateOrderInputBillInit";
    }
    /**
	 * 订单供应商收货信息保存
	 * @return
	 * @throws BaseException 
	 */
	public String saveOrderInputSendBill() throws BaseException {		
		try{
			String writer=UserRightInfoUtil.getUserName(this.getRequest());
			//编制人
			inputBill.setInputWriter(writer);	 	   
	 	    //编制日期 
			inputBill.setInputWriteDate(DateUtil.getCurrentDateTime());	
			inputBill.setStatus(TableStatus.COMMON_0);
			this.iInputBillBiz.saveInputBill(inputBill);
			
			//保存附件		
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(inputBill,
					inputBill.getIbId(), AttachmentStatus.ATTACHMENT_CODE_1301,
					writer));
			
			String ids=this.getRequest().getParameter("ids");
			String[] str=ids.split(",");
			for(String id:str){
				sendBill=this.iSendBillBiz.getSendBill(Long.parseLong(id));
				sendBill.setIsInput(TableStatus.COMMON_INT_0);
				
				this.iSendBillBiz.updateSendBill(sendBill);
			}
			
			Double realInputAmount=0.00;
			// 保存合同物资明细数据
			if (sbdList != null) {
				for (int i = 0; i < sbdList.size(); i++) {
					sendBillDetail = sbdList.get(i);
					if (sendBillDetail != null
							&& StringUtil.isNotBlank(sendBillDetail.getSbdId())) {
						sendBillDetail.setIbId(inputBill.getIbId());
						
						iSendBillDetailBiz.updateInputBillDetail(sendBillDetail);	
						
						//更新订单明细表中的可收货数量，实际收货数量
						iOrderMaterialBiz.updateInputAmount(sendBillDetail.getInputAmount(), sendBillDetail.getOmId());
						
						realInputAmount+=sendBillDetail.getInputAmount();
					}
				}
			}
			inputBill.setInputAmount(realInputAmount);
			this.iInputBillBiz.updateInputBill(inputBill);
						
			orderInfo=this.iOrderInfoBiz.getOrderInfo(sendBill.getOiId());
			orderInfo.setRealInputAmount(orderInfo.getRealInputAmount()+realInputAmount);
			orderInfo.setLastGoodArriveDate(DateUtil.getCurrentDateTime());
			if(orderInfo.getRealInputAmount()==orderInfo.getContractAmount()){
				orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_08);
				orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_08_Text);
			}
			this.iOrderInfoBiz.updateOrderInfo(orderInfo);
			
			this.getRequest().setAttribute("message", "收货成功");
			this.getRequest().setAttribute("operModule", "供应商收货信息保存");
		} catch (Exception e) {
			log("订单供应商收货信息保存错误！", e);
			throw new BaseException("订单供应商收货信息保存错误！", e);
		}
		return "orderInputBillView";		
	}
	/**
	 * 合同收货信息初始化
	 * @return
	 * @throws BaseException
	 */
    public String updateContractInputBillInit() throws BaseException{
    	try {
			String ids=this.getRequest().getParameter("ids");
			
			List<SendBillDetail> sbdList=new ArrayList<SendBillDetail>();
			List<Object[]> list=this.iSendBillDetailBiz.getSendBillDetailListForInputBill(ids);
			for(Object[] obj:list){
				sendBillDetail=(SendBillDetail)obj[0];
				sendBillDetail.setSendDate((Date)obj[1]);
				sbdList.add(sendBillDetail);
			}
			this.getRequest().setAttribute("sbdList", sbdList);

			Long ciId=Long.parseLong(this.getRequest().getParameter("ciId"));
			contractInfo=this.iContractInfoBiz.getContractInfo(ciId);
			this.getRequest().setAttribute("ids", ids);
			this.getRequest().setAttribute("contractInfo", contractInfo);
			this.getRequest().setAttribute("today", DateUtil.getDefaultDateFormat(new Date()));
		}catch (Exception e) {
			log("合同收货信息初始化错误！", e);
			throw new BaseException("合同收货信息初始化错误！", e);
		}
		return "updateContractInputBillInit";
    }
    /**
	 * 合同供应商收货信息保存
	 * @return
	 * @throws BaseException 
	 */
	public String saveContractInputSendBill() throws BaseException {		
		try{
			String writer=UserRightInfoUtil.getUserName(this.getRequest());
			//编制人
			inputBill.setInputWriter(writer);	 	   
	 	    //编制日期 
			inputBill.setInputWriteDate(DateUtil.getCurrentDateTime());	
			inputBill.setStatus(TableStatus.COMMON_0);
			this.iInputBillBiz.saveInputBill(inputBill);
			
			//保存附件		
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(inputBill,
					inputBill.getIbId(), AttachmentStatus.ATTACHMENT_CODE_1301,
					writer));
			
			String ids=this.getRequest().getParameter("ids");
			String[] str=ids.split(",");
			for(String id:str){
				sendBill=this.iSendBillBiz.getSendBill(Long.parseLong(id));
				sendBill.setIsInput(TableStatus.COMMON_INT_0);
				
				this.iSendBillBiz.updateSendBill(sendBill);
			}
			
			Double realInputAmount=0.00;
			// 保存合同物资明细数据
			if (sbdList != null) {
				for (int i = 0; i < sbdList.size(); i++) {
					sendBillDetail = sbdList.get(i);
					if (sendBillDetail != null
							&& StringUtil.isNotBlank(sendBillDetail.getSbdId())) {
						sendBillDetail.setIbId(inputBill.getIbId());
						
						iSendBillDetailBiz.updateInputBillDetail(sendBillDetail);	
						
						//更新订单明细表中的可收货数量，实际收货数量
						iContractMaterialBiz.updateInputAmount(sendBillDetail.getInputAmount(), sendBillDetail.getOmId());
						
						realInputAmount+=sendBillDetail.getInputAmount();
					}
				}
			}
			inputBill.setInputAmount(realInputAmount);
			this.iInputBillBiz.updateInputBill(inputBill);
						
			contractInfo=this.iContractInfoBiz.getContractInfo(sendBill.getOiId());
			contractInfo.setRealInputAmount(contractInfo.getRealInputAmount()+realInputAmount);
			contractInfo.setLastGoodArriveDate(DateUtil.getCurrentDateTime());
			if(contractInfo.getRealInputAmount()==contractInfo.getContractAmount()){
				contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_08);
				contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_08_Text);
			}
			this.iContractInfoBiz.updateContractInfo(contractInfo);
			
			this.getRequest().setAttribute("message", "收货成功");
			this.getRequest().setAttribute("operModule", "供应商收货信息保存");
		} catch (Exception e) {
			log("合同供应商收货信息保存错误！", e);
			throw new BaseException("合同供应商收货信息保存错误！", e);
		}
		return "contractInputBillView";		
	}
	public SendBill getSendBill() {
		return sendBill;
	}

	public void setSendBill(SendBill sendBill) {
		this.sendBill = sendBill;
	}

	public ISendBillBiz getiSendBillBiz() {
		return iSendBillBiz;
	}

	public void setiSendBillBiz(ISendBillBiz iSendBillBiz) {
		this.iSendBillBiz = iSendBillBiz;
	}

	public ISendBillDetailBiz getiSendBillDetailBiz() {
		return iSendBillDetailBiz;
	}

	public void setiSendBillDetailBiz(ISendBillDetailBiz iSendBillDetailBiz) {
		this.iSendBillDetailBiz = iSendBillDetailBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}

	public List<SendBillDetail> getSbdList() {
		return sbdList;
	}

	public void setSbdList(List<SendBillDetail> sbdList) {
		this.sbdList = sbdList;
	}

	public IOrderMaterialBiz getiOrderMaterialBiz() {
		return iOrderMaterialBiz;
	}

	public void setiOrderMaterialBiz(IOrderMaterialBiz iOrderMaterialBiz) {
		this.iOrderMaterialBiz = iOrderMaterialBiz;
	}

	public IInputBillBiz getiInputBillBiz() {
		return iInputBillBiz;
	}

	public void setiInputBillBiz(IInputBillBiz iInputBillBiz) {
		this.iInputBillBiz = iInputBillBiz;
	}

	public InputBill getInputBill() {
		return inputBill;
	}

	public void setInputBill(InputBill inputBill) {
		this.inputBill = inputBill;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

	public IContractMaterialBiz getiContractMaterialBiz() {
		return iContractMaterialBiz;
	}

	public void setiContractMaterialBiz(IContractMaterialBiz iContractMaterialBiz) {
		this.iContractMaterialBiz = iContractMaterialBiz;
	}
	
}
