package com.ced.sip.bill.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：SendBill
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public class InputBill extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long ibId;     //主键
	private Long oiId;     //订单主键
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String sendRemark;	 //发货说明
	private String status;	 //状态
	private Date inputDate;    //收货日期
	private Double inputAmount;	//收货总数量
	private String inputWriter;	 //收货操作人
	private Date inputWriteDate;    //收货操作时间
	private String inputRemark;    //收货说明
	private int type;//类型
	
	
	public InputBill() {
		super();
	}    
	public Long getIbId() {
		return ibId;
	}
	public void setIbId(Long ibId) {
		this.ibId = ibId;
	}
	public Long getOiId(){
	   return  oiId;
	} 
	public void setOiId(Long oiId) {
	   this.oiId = oiId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getSendRemark(){
	   return  sendRemark;
	} 
	public void setSendRemark(String sendRemark) {
	   this.sendRemark = sendRemark;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public Double getInputAmount() {
		return inputAmount;
	}

	public void setInputAmount(Double inputAmount) {
		this.inputAmount = inputAmount;
	}

	public String getInputWriter() {
		return inputWriter;
	}

	public void setInputWriter(String inputWriter) {
		this.inputWriter = inputWriter;
	}

	public Date getInputWriteDate() {
		return inputWriteDate;
	}

	public void setInputWriteDate(Date inputWriteDate) {
		this.inputWriteDate = inputWriteDate;
	}

	public String getInputRemark() {
		return inputRemark;
	}

	public void setInputRemark(String inputRemark) {
		this.inputRemark = inputRemark;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}	    
}