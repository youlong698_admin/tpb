package com.ced.sip.bill.entity;

import java.util.Date;

/** 
 * 类名称：SendBillDetail
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public class SendBillDetail implements java.io.Serializable {

	// 属性信息
	private Long sbdId;     //主键
	private Long sbId;     //发货单主键
	private String materialCode;	 //物资编码
	private String materialName;	 //物资名称
	private String materialType;	 //物资型号
	private String trademark;	 //品牌
	private Double price;	//单价
	private String unit;	 //单位
	private Double sendAmount;	//发货数量
	private String remark;
	private Long omId;
	private Double inputAmount;	//收货数量
	private String inputRemark;
	private Long ibId;
	
	private Date sendDate;
	
	
	public SendBillDetail() {
		super();
	}
	
	public Long getSbdId(){
	   return  sbdId;
	} 
	public void setSbdId(Long sbdId) {
	   this.sbdId = sbdId;
    }     
	public Long getSbId(){
	   return  sbId;
	} 
	public void setSbId(Long sbId) {
	   this.sbId = sbId;
    }     
	public String getMaterialCode(){
	   return  materialCode;
	} 
	public void setMaterialCode(String materialCode) {
	   this.materialCode = materialCode;
    }
	public String getMaterialName(){
	   return  materialName;
	} 
	public void setMaterialName(String materialName) {
	   this.materialName = materialName;
    }
	public String getMaterialType(){
	   return  materialType;
	} 
	public void setMaterialType(String materialType) {
	   this.materialType = materialType;
    }
	public String getTrademark(){
	   return  trademark;
	} 
	public void setTrademark(String trademark) {
	   this.trademark = trademark;
    }
	public Double getPrice(){
	   return  price;
	} 
	public void setPrice(Double price) {
	   this.price = price;
    }	
	public String getUnit(){
	   return  unit;
	} 
	public void setUnit(String unit) {
	   this.unit = unit;
    }
	public Double getSendAmount(){
	   return  sendAmount;
	} 
	public void setSendAmount(Double sendAmount) {
	   this.sendAmount = sendAmount;
    }
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOmId() {
		return omId;
	}

	public void setOmId(Long omId) {
		this.omId = omId;
	}

	public Double getInputAmount() {
		return inputAmount;
	}

	public void setInputAmount(Double inputAmount) {
		this.inputAmount = inputAmount;
	}

	public String getInputRemark() {
		return inputRemark;
	}

	public void setInputRemark(String inputRemark) {
		this.inputRemark = inputRemark;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public Long getIbId() {
		return ibId;
	}

	public void setIbId(Long ibId) {
		this.ibId = ibId;
	}	
}