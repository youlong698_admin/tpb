package com.ced.sip.bill.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：SendBill
 * 创建人：luguanglei 
 * 创建时间：2017-08-06
 */
public class SendBill extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long sbId;     //主键
	private Long oiId;     //订单主键
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String sendRemark;	 //发货说明
	private String status;	 //状态
	private Date sendDate;    //发货日期
	private Double sendAmount;	//发货总数量
	private String writer;	 //操作人
	private Date writeDate;    //操作时间
	private int isInput;//是否收货
	private int type;//类型
	
	public SendBill() {
		super();
	}
	
	public Long getSbId(){
	   return  sbId;
	} 
	public void setSbId(Long sbId) {
	   this.sbId = sbId;
    }     
	public Long getOiId(){
	   return  oiId;
	} 
	public void setOiId(Long oiId) {
	   this.oiId = oiId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getSendRemark(){
	   return  sendRemark;
	} 
	public void setSendRemark(String sendRemark) {
	   this.sendRemark = sendRemark;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public Date getSendDate(){
	   return  sendDate;
	} 
	public void setSendDate(Date sendDate) {
	   this.sendDate = sendDate;
    }	    
	public Double getSendAmount(){
	   return  sendAmount;
	} 
	public void setSendAmount(Double sendAmount) {
	   this.sendAmount = sendAmount;
    }	
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }

	public int getIsInput() {
		return isInput;
	}

	public void setIsInput(int isInput) {
		this.isInput = isInput;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}