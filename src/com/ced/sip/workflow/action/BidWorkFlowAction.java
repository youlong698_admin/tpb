package com.ced.sip.workflow.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.HistoryTask;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Process;
import org.snaker.engine.entity.Task;
import org.snaker.engine.model.TaskModel.PerformType;
import org.springframework.beans.factory.annotation.Autowired;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.capital.biz.ICapitalPlanBiz;
import com.ced.sip.capital.entity.CapitalPlan;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.biz.IContractMaterialBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.contract.entity.ContractMaterial;
import com.ced.sip.contract.util.ContractStatus;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.order.biz.IOrderMaterialBiz;
import com.ced.sip.order.entity.OrderInfo;
import com.ced.sip.order.entity.OrderMaterial;
import com.ced.sip.order.util.OrderStatus;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.requirement.biz.IRequiredMaterialBiz;
import com.ced.sip.requirement.entity.RequiredMaterial;
import com.ced.sip.requirement.entity.RequiredMaterialDetail;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.biz.ISysMessageLogBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.SysMessageLog;
import com.ced.sip.system.entity.Users;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
import com.ced.sip.workflow.biz.IWfProcessNodeBiz;
import com.ced.sip.workflow.biz.IWfProcessStationRightsBiz;
import com.ced.sip.workflow.biz.IWfTextBiz;
import com.ced.sip.workflow.entity.WfProcessNode;
import com.ced.sip.workflow.entity.WfProcessStationRights;
import com.ced.sip.workflow.entity.WfText;



public class BidWorkFlowAction extends BaseAction {
	@Autowired
	private SnakerEngineFacets facets;

	//需求计划
	private IRequiredMaterialBiz iRequiredMaterialBiz;
	//需求汇总分包
	private IRequiredCollectBiz iRequiredCollectBiz;
	//流程节点分配处理人 服务类
	private IWfProcessNodeBiz iWfProcessNodeBiz;
    //流程审批正文服务类
	private IWfTextBiz iWfTextBiz;
	//附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	//组织机构服务类
	private IDepartmentsBiz iDepartmentsBiz;
	//用户服务类
	private IUsersBiz iUsersBiz;
	//消息服务类
	private ISysMessageLogBiz iSysMessageLogBiz;
	//品类库
	private IMaterialBiz iMaterialBiz;
	//日志服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	//授标
	private IBidAwardBiz iBidAwardBiz;
	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	//合同明细信息
	private IContractMaterialBiz iContractMaterialBiz;
	// 订单信息 
	private IOrderInfoBiz iOrderInfoBiz;
	//订单明细信息
	private IOrderMaterialBiz iOrderMaterialBiz;
	// 资金计划 
	private ICapitalPlanBiz iCapitalPlanBiz;
	
	private IWfProcessStationRightsBiz iWfProcessStationRightsBiz;
	
	private WfText wfText;	
	private WfProcessNode wfProcessNode;
	private RequiredMaterial requiredMaterial;
	private RequiredMaterialDetail requiredMaterialDetail;
	private RequiredCollect requiredCollect;
	private Departments departments;
	private Users users;
	private PurchaseRecordLog purchaseRecordLog;
	private WfProcessStationRights wfProcessStationRights;

	// 合同信息
	private ContractInfo contractInfo;
	private ContractMaterial contractMaterial;
	private List<ContractMaterial> cmList;
	// 订单信息
	private OrderInfo orderInfo;
	
	private OrderMaterial orderMaterial;
	// 资金计划
	private CapitalPlan capitalPlan;

	private List<OrderMaterial> omList;
	
	private Map purchaseMap=TableStatusMap.purchaseWay;
	

	
	
    /***************************处理审批流程 begin******************************************/
	
	/**
	 * @title 申请人初始化审批流程
	 * @return
	 * @throws BaseException 
	 */
	public String initiatorToApply() throws BaseException {
		String initPage="";
		try{
			String from = getRequest().getParameter("from");
			String processId = getRequest().getParameter("processId");
			String orderId = getRequest().getParameter("orderId");
			String orderNo = getRequest().getParameter("orderNo");
			String taskId = getRequest().getParameter("taskId");
			int orderState=99;
			String workFlowType = "";
			String detail = getRequest().getParameter("detail");
			HistoryOrder hisorder=null;
			Task task =null;
			if(StringUtil.isNotBlank(orderId)){
				hisorder=  facets.getEngine().query().getHistOrder(orderId);
				if(hisorder.getOrderState()==0||hisorder.getOrderState()==1) detail="detail";
				orderState=hisorder.getOrderState();
			}
			
			if(StringUtil.isNotBlank(taskId)){
				task = facets.getEngine().query().getTask(taskId);	
				if(task==null) detail="detail";
			}
			
			//查看的时候taskId为 wf_hist_task的id，申请提交的时候则为wf_task的id wf_his_task 的id
			if(!StringUtil.isBlank(detail)){
				    HistoryTask histask = facets.getEngine().query().getHistTask(taskId);
					if(hisorder==null) hisorder =  facets.getEngine().query().getHistOrder(histask.getOrderId());				
					getRequest().setAttribute("creator", BaseDataInfosUtil.convertLoginNameToChnName(hisorder.getCreator()));
		            getRequest().setAttribute("deptName", BaseDataInfosUtil.convertDepartNameToChnName(hisorder.getCreator()));
					getRequest().setAttribute("createTime", hisorder.getCreateTime());
					getRequest().setAttribute("description", this.getWorkFlowText(hisorder.getId()));
					//查询审批记录
					List hytList = this.getHistoryTaskInfo(hisorder.getId());
					this.getRequest().setAttribute("hytList", hytList);
					workFlowType=hisorder.getOrderNo().substring(0,2);
					orderNo=hisorder.getOrderNo();
					orderId=hisorder.getId();
					orderState=hisorder.getOrderState();
			
			}else{
              getRequest().setAttribute("creator", UserRightInfoUtil.getChineseName(getRequest()));
              getRequest().setAttribute("deptName", UserRightInfoUtil.getUserDeptName(getRequest()));
			  getRequest().setAttribute("createTime", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
			  if(!StringUtil.isBlank(taskId)){	
				    if(task==null) task = facets.getEngine().query().getTask(taskId);
					Order order =  facets.getEngine().query().getOrder(task.getOrderId());		
					 //初始化请示正文  
					getRequest().setAttribute("description", this.getWorkFlowText(task.getOrderId()));
					//查询审批记录
					List hytList = this.getHistoryTaskInfo(task.getOrderId());
					this.getRequest().setAttribute("hytList", hytList);
					workFlowType=order.getOrderNo().substring(0,2);
					orderNo=order.getOrderNo();
					orderId=order.getId();
			   }else{
				   //需要判断是不是退回重新提交的信息 此时虽没有taskId, 但是可以查询得到
				   QueryFilter filter = new QueryFilter();
				   filter.setOrderNo(orderNo);
				   filter.setProcessId(processId);
				   List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
				   if(orderlist.size()>0){
					   Order order = orderlist.get((orderlist.size()-1));
					   List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
						if(tasklist.size() > 0){
							taskId = tasklist.get(0).getId();
						}

						//初始化请示正文  
						getRequest().setAttribute("description", this.getWorkFlowText(order.getId()));
						//查询审批记录
						List hytList = this.getHistoryTaskInfo(order.getId());
						this.getRequest().setAttribute("hytList", hytList);
						orderNo=order.getOrderNo();
						orderId=order.getId();
				   }
				   workFlowType=orderNo.substring(0,2);
					   
			   }
			}
			//以下代码是获取业务数据的代码
			if(workFlowType.equals(WorkFlowStatus.RM_WorkFlow_Type)){
				setRequirementData(orderNo.replace(WorkFlowStatus.RM_WorkFlow_Type, ""));
				initPage="initiatorToApplyByRequirePlan";
			}else if(workFlowType.equals(WorkFlowStatus.RC_WorkFlow_Type)){
				setPurchaseData(orderNo.replace(WorkFlowStatus.RC_WorkFlow_Type, ""));
				initPage="initiatorToApplyByPurchase";
			}else if(workFlowType.equals(WorkFlowStatus.PR_WorkFlow_Type)){
				setPurchaseResultData(orderNo.replace(WorkFlowStatus.PR_WorkFlow_Type, ""));
				initPage="initiatorToApplyByPurchaseResult";
			}else if(workFlowType.equals(WorkFlowStatus.CT_WorkFlow_Type)){
				setContractData(orderNo.replace(WorkFlowStatus.CT_WorkFlow_Type, ""));
				initPage="initiatorToApplyByContract";
			}else if(workFlowType.equals(WorkFlowStatus.OD_WorkFlow_Type)){
				setOrderData(orderNo.replace(WorkFlowStatus.OD_WorkFlow_Type, ""));
				initPage="initiatorToApplyByOrder";
			}else if(workFlowType.equals(WorkFlowStatus.CP_WorkFlow_Type)){
				setCapitalData(orderNo.replace(WorkFlowStatus.CP_WorkFlow_Type, ""));
				initPage="initiatorToApplyByCapital";
			}
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
            
			getRequest().setAttribute("workFlowType", workFlowType);
			getRequest().setAttribute("detail", detail);
			getRequest().setAttribute("processId", processId);
			getRequest().setAttribute("orderId", orderId);
			getRequest().setAttribute("orderNo", orderNo);
			getRequest().setAttribute("taskId", taskId);
			getRequest().setAttribute("orderState", orderState);
			
		} catch (Exception e) {
			log("申请人初始化审批流程错误！", e);
			throw new BaseException("申请人初始化审批流程错误！", e);
		}
		return initPage;
		
	}

	/**
	 * @title 保存发起人文件申请提交
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 */
	public String saveInitiatorToApply() throws BaseException, IOException {
		String page="workflowResult";
		try{
			String from = getRequest().getParameter("from");
			String processId = getRequest().getParameter("processId");
			String orderNo = getRequest().getParameter("orderNo");
			String taskId = getRequest().getParameter("taskId");
			String description = getRequest().getParameter("description");
			String signContent = getRequest().getParameter("signContent");
			String workFlowType = getRequest().getParameter("workFlowType");

			boolean falg=false;
			//以下代码是获取业务数据的代码
			if(workFlowType.equals(WorkFlowStatus.RM_WorkFlow_Type)){
				orderNo=orderNo.replace(WorkFlowStatus.RM_WorkFlow_Type, "");
				falg=saveInitiatorRequirement(orderNo, taskId, processId, signContent, description);
				this.getRequest().setAttribute("operModule", "需求计划审批保存发起人申请提交");
			}else if(workFlowType.equals(WorkFlowStatus.RC_WorkFlow_Type)){
				orderNo=orderNo.replace(WorkFlowStatus.RC_WorkFlow_Type, "");
				falg=saveInitiatorPurchase(orderNo, taskId, processId, signContent, description);
				this.getRequest().setAttribute("operModule", "项目立项审批保存发起人申请提交");
			}else if(workFlowType.equals(WorkFlowStatus.PR_WorkFlow_Type)){
				orderNo=orderNo.replace(WorkFlowStatus.PR_WorkFlow_Type, "");
				falg=saveInitiatorPurchaseResult(orderNo, taskId, processId, signContent, description);
				this.getRequest().setAttribute("operModule", "授标审批保存发起人申请提交");
			}else if(workFlowType.equals(WorkFlowStatus.CT_WorkFlow_Type)){
				orderNo=orderNo.replace(WorkFlowStatus.CT_WorkFlow_Type, "");
				falg=saveInitiatorContract(orderNo, taskId, processId, signContent, description);
				this.getRequest().setAttribute("operModule", "合同审批保存发起人申请提交");
			}else if(workFlowType.equals(WorkFlowStatus.OD_WorkFlow_Type)){
				orderNo=orderNo.replace(WorkFlowStatus.OD_WorkFlow_Type, "");
				falg=saveInitiatorOrder(orderNo, taskId, processId, signContent, description);
				this.getRequest().setAttribute("operModule", "订单审批保存发起人申请提交");
			}else if(workFlowType.equals(WorkFlowStatus.CP_WorkFlow_Type)){
				orderNo=orderNo.replace(WorkFlowStatus.CP_WorkFlow_Type, "");
				falg=saveInitiatorCapital(orderNo, taskId, processId, signContent, description);
				this.getRequest().setAttribute("operModule", "资金计划审批保存发起人申请提交");
			}
			//触屏版使用
            if(StringUtil.isNotBlank(from)) page+="Mobile";
            if(!falg){
            	this.getRequest().setAttribute("message", "申请提交成功");
            }else{
            	this.getRequest().setAttribute("error", "error");
            	this.getRequest().setAttribute("message", "系统查找不到流程审批人，请确认流程节点审批人已经设置！");
            }
	  		
		} catch (Exception e) {
			log("保存发起人文件申请提交错误！", e);
			throw new BaseException("保存发起人文件申请提交错误！", e);
		}
		return page;
	}
	/**
	 * @title 领导审批初始化
	 * @return
	 * @throws BaseException 
	 */
	public String initiatorToApprove() throws BaseException {
		String initPage="";
		Task task = null;
		try{
			String from = getRequest().getParameter("from");
			String taskId = getRequest().getParameter("taskId");
			String detail = getRequest().getParameter("detail");
			String workFlowType = "";
			int orderState=99;
			String orderNo="";
			String[] taskActor = null;
			task = facets.getEngine().query().getTask(taskId);
			if(task==null) detail="detail";
			//查看的时候taskId为 wf_hist_task的id，申请提交的时候则为wf_task的id
			if(!StringUtil.isBlank(detail)){
				
				HistoryTask histask = facets.getEngine().query().getHistTask(taskId);
				HistoryOrder hisorder =  facets.getEngine().query().getHistOrder(histask.getOrderId());				
				getRequest().setAttribute("creator", BaseDataInfosUtil.convertLoginNameToChnName(hisorder.getCreator()));
	            getRequest().setAttribute("deptName", BaseDataInfosUtil.convertDepartNameToChnName(hisorder.getCreator()));
				getRequest().setAttribute("createTime", hisorder.getCreateTime());
				
				getRequest().setAttribute("signContent", histask.getVariableMap().get("signContent") == null ? "" : histask.getVariableMap().get("signContent").toString());
				getRequest().setAttribute("result", histask.getVariableMap().get("result") == null ? "" : histask.getVariableMap().get("result").toString());
				//初始化请示正文  
				getRequest().setAttribute("description", this.getWorkFlowText(histask.getOrderId()));
			
				taskActor = facets.getEngine().query().getHistoryTaskActorsByTaskId(taskId);
				
				getRequest().setAttribute("task", histask);
				//查询审批记录
				List hytList = this.getHistoryTaskInfo(histask.getOrderId());
				this.getRequest().setAttribute("hytList", hytList);
				orderNo=hisorder.getOrderNo();
				workFlowType=hisorder.getOrderNo().substring(0,2);
				orderState=hisorder.getOrderState();
			}else{
				
			//领导审批初始化,系统需要读取申请人的信息,比如 正文,申请人\申请时间等信息			
			Order order = null;
			order = facets.getEngine().query().getOrder(task.getOrderId());
			taskActor = facets.getEngine().query().getTaskActorsByTaskId(taskId);
			
			getRequest().setAttribute("creator", BaseDataInfosUtil.convertLoginNameToChnName(order.getCreator()));

            getRequest().setAttribute("deptName", BaseDataInfosUtil.convertDepartNameToChnName(order.getCreator()));
			getRequest().setAttribute("createTime", order.getCreateTime());
			//初始化请示正文
			getRequest().setAttribute("description", this.getWorkFlowText(order.getId()));
			

			//审批记录
			List hytList = this.getHistoryTaskInfo(order.getId());

			getRequest().setAttribute("task", task);
			getRequest().setAttribute("order", order);
			getRequest().setAttribute("hytList",hytList);
			
			orderNo=order.getOrderNo();
			workFlowType=order.getOrderNo().substring(0,2);
			}
			String taskActors = "";
			for(int i=0; i<taskActor.length; i++){
				if(i == taskActor.length-1){
					taskActors += BaseDataInfosUtil.convertLoginNameToChnName(taskActor[i]);
				}else{
					taskActors += BaseDataInfosUtil.convertLoginNameToChnName(taskActor[i]) + ",";
				}
			}
			getRequest().setAttribute("taskActors", taskActors);
			//以下代码是获取业务数据的代码
			if(workFlowType.equals(WorkFlowStatus.RM_WorkFlow_Type)){
				setRequirementData(orderNo.replace(WorkFlowStatus.RM_WorkFlow_Type, ""));
				initPage="initiatorToApproveByRequirePlan";
			}else if(workFlowType.equals(WorkFlowStatus.RC_WorkFlow_Type)){
				setPurchaseData(orderNo.replace(WorkFlowStatus.RC_WorkFlow_Type, ""));
				initPage="initiatorToApproveByPurchase";
			}else if(workFlowType.equals(WorkFlowStatus.PR_WorkFlow_Type)){
				setPurchaseResultData(orderNo.replace(WorkFlowStatus.PR_WorkFlow_Type, ""));
				initPage="initiatorToApproveByPurchaseResult";
			}else if(workFlowType.equals(WorkFlowStatus.CT_WorkFlow_Type)){
				setContractData(orderNo.replace(WorkFlowStatus.CT_WorkFlow_Type, ""));
				initPage="initiatorToApproveByContract";
			}else if(workFlowType.equals(WorkFlowStatus.OD_WorkFlow_Type)){
				setOrderData(orderNo.replace(WorkFlowStatus.OD_WorkFlow_Type, ""));
				initPage="initiatorToApproveByOrder";
			}else if(workFlowType.equals(WorkFlowStatus.CP_WorkFlow_Type)){
				setCapitalData(orderNo.replace(WorkFlowStatus.CP_WorkFlow_Type, ""));
				initPage="initiatorToApproveByCapital";
			}
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
            
			getRequest().setAttribute("workFlowType", workFlowType);
			this.getRequest().setAttribute("detail", detail);
			getRequest().setAttribute("taskId", taskId);
			getRequest().setAttribute("orderState", orderState);
		} catch (Exception e) {
			log("领导审批初始化错误！", e);
			throw new BaseException("领导审批初始化错误！", e);
		}
		return initPage;
		
	}
	/**
	 * @title 保存领导审批
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitiatorToApprove() throws BaseException {
		String page="workflowResult";
		try{
			String from = getRequest().getParameter("from");
			String taskId = getRequest().getParameter("taskId");
			String result = getRequest().getParameter("result");
			String signContent = getRequest().getParameter("signContent");
			String opAddTaskActor = getRequest().getParameter("addOpTaskValue");
			String workFlowType = getRequest().getParameter("workFlowType");
			
			//以下代码是获取业务数据的代码
			if(workFlowType.equals(WorkFlowStatus.RM_WorkFlow_Type)){
				saveApproveRequirement( taskId, result, signContent, opAddTaskActor);
				this.getRequest().setAttribute("operModule", "需求计划审批领导审批成功交");
			}else if(workFlowType.equals(WorkFlowStatus.RC_WorkFlow_Type)){
				saveApprovePurchase( taskId, result, signContent, opAddTaskActor);
				this.getRequest().setAttribute("operModule", "项目立项审批领导审批成功交");
			}else if(workFlowType.equals(WorkFlowStatus.PR_WorkFlow_Type)){
				saveApprovePurchaseResult( taskId, result, signContent, opAddTaskActor);
				this.getRequest().setAttribute("operModule", "授标结果审批领导审批成功交");
			}else if(workFlowType.equals(WorkFlowStatus.CT_WorkFlow_Type)){
				saveApproveContract( taskId, result, signContent, opAddTaskActor);
				this.getRequest().setAttribute("operModule", "合同审批领导审批成功交");
			}else if(workFlowType.equals(WorkFlowStatus.OD_WorkFlow_Type)){
				saveApproveOrder( taskId, result, signContent, opAddTaskActor);
				this.getRequest().setAttribute("operModule", "订单审批领导审批成功交");
			}else if(workFlowType.equals(WorkFlowStatus.CP_WorkFlow_Type)){
				saveApproveCapital( taskId, result, signContent, opAddTaskActor);
				this.getRequest().setAttribute("operModule", "资金计划审批领导审批成功交");
			}
			//触屏版使用
            if(StringUtil.isNotBlank(from)) page+="Mobile";
            
			this.getRequest().setAttribute("message", "领导审批成功");
	  		
		} catch (Exception e) {
			log("保存领导审批提交错误！", e);
			throw new BaseException("保存领导审批提交错误！", e);
		}
		return page;
		
	}
	/**
	 * 获取需求原因或请示正文信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String getWorkFlowText(String orderId) throws BaseException {
		   String workFlowext = "";
           try{
				wfText = new WfText();
				wfText.setOrderId(orderId);
				List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
				if(wtList!=null&&wtList.size()>0){
					workFlowext = wtList.get(0).getWfText();
				}
		} catch (Exception e) {
			log.error("获取需求原因或请示正文信息错误！", e);
			throw new BaseException("获取需求原因或请示正文信息错误！", e);
		}
		return workFlowext;
	}
	/**
	 * 获取流程标题
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String getWorkFlowName(String orderId) throws BaseException {
		   String workFlowName = "";
           try{
				wfText = new WfText();
				wfText.setOrderId(orderId);
				List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
				if(wtList!=null&&wtList.size()>0){
					workFlowName = wtList.get(0).getWfName();
				}
		} catch (Exception e) {
			log.error("获取流程标题信息错误！", e);
			throw new BaseException("获取流程标题信息错误！", e);
		}
		return workFlowName;
	}

	/**
	 * 获取需求计划业务数据
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void setRequirementData(String rmId)throws BaseException{
		 try{
			// 取需求计划信息
			requiredMaterial = iRequiredMaterialBiz
					.getRequiredMaterial(Long.parseLong(rmId));
			requiredMaterial.setWriterCN(BaseDataInfosUtil
					.convertLoginNameToChnName(requiredMaterial.getWriter()));
			requiredMaterial.setRmTypeCn(BaseDataInfosUtil
					.convertPurchKindType(requiredMaterial.getRmType()));
	
			requiredMaterialDetail = new RequiredMaterialDetail();
			requiredMaterialDetail.setRmId(requiredMaterial.getRmId());
			this.setListValue(iRequiredMaterialBiz
					.getRequiredMaterialDetailList(requiredMaterialDetail));
			for (int i = 0; i < this.getListValue().size(); i++) {
				requiredMaterialDetail = (RequiredMaterialDetail) this
						.getListValue().get(i);
				requiredMaterialDetail.setEstimatePriceStr(StringUtil
						.formatNumbertoEnglishForm(requiredMaterialDetail
								.getEstimatePrice()));
			}
			// 获取附件
			requiredMaterial.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									requiredMaterial.getRmId(),
									AttachmentStatus.ATTACHMENT_CODE_102)), "0",
							this.getRequest()));
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredMaterial.getPurchaseDeptId()));
			//this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredMaterial.getWriter()));
			//this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredMaterial.getDeptId()));
			this.getRequest().setAttribute("requireTypeCn",BaseDataInfosUtil.convertPurchKindType(requiredMaterial.getRmType()));
			this.getRequest().setAttribute("requiredMaterial",requiredMaterial);
		
	 } catch (Exception e) {
			log.error("获取需求计划业务数据错误！", e);
			throw new BaseException("获取需求计划业务数据错误！", e);
		}
	}
	/**
	 * 获取项目立项业务数据
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void setPurchaseData(String rcId)throws BaseException{
		 try{
			   requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(rcId));
			   String buyWayCn=(String)purchaseMap.get(requiredCollect.getBuyWay());
			   // 获取附件
				requiredCollect.setAttachmentUrl(iAttachmentBiz
						.getAttachmentPageUrl(
								iAttachmentBiz.getAttachmentList(new Attachment(
										requiredCollect.getRcId(),
										AttachmentStatus.ATTACHMENT_CODE_103)), "0",
								this.getRequest()));
				
				this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
				//this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
				//this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
				this.getRequest().setAttribute("buyWayCn", BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
				this.getRequest().setAttribute("supplierTypeCn", BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
				this.getRequest().setAttribute("buyWayCn", buyWayCn);
				this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
		
	    } catch (Exception e) {
			log.error("获取项目立项业务数据错误！", e);
			throw new BaseException("获取项目立项业务数据错误！", e);
		}
	}
	/**
	 * 获取采购结果业务数据
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void setPurchaseResultData(String rcId)throws BaseException{
		 try{ 
			   requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(rcId));
			   String buyWayCn=(String)purchaseMap.get(requiredCollect.getBuyWay());

			   List list=this.iBidPriceBiz.getBidPriceAndAwardListByRcId(requiredCollect.getRcId());
				
			   this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
			   //this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
			   //this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
			   this.getRequest().setAttribute("buyWayCn", BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
			   this.getRequest().setAttribute("supplierTypeCn", BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
			   this.getRequest().setAttribute("buyWayCn", buyWayCn);
			   this.getRequest().setAttribute("list", list);
				
		
	    } catch (Exception e) {
			log.error("获取采购结果业务数据错误！", e);
			throw new BaseException("获取采购结果业务数据错误！", e);
		}
	}
	/**
	 * 获取合同业务数据
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void setContractData(String ciId)throws BaseException{
		 try{ 
			 contractInfo=this.iContractInfoBiz.getContractInfo(Long.parseLong(ciId) );
				
			 contractMaterial=new ContractMaterial();
			 contractMaterial.setCiId(contractInfo.getCiId());
			 cmList=this.iContractMaterialBiz.getContractMaterialList(contractMaterial);

			 this.getRequest().setAttribute("cmList", cmList);	
	    } catch (Exception e) {
			log.error("获取合同业务数据错误！", e);
			throw new BaseException("获取合同业务数据错误！", e);
		}
	}
	/**
	 * 获取订单业务数据
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void setOrderData(String oiId)throws BaseException{
		 try{ 
			 orderInfo=this.iOrderInfoBiz.getOrderInfo(Long.parseLong(oiId));
			
			 orderMaterial=new OrderMaterial();
			 orderMaterial.setOiId(orderInfo.getOiId());
			 omList=this.iOrderMaterialBiz.getOrderMaterialList(orderMaterial);		 

			 this.getRequest().setAttribute("omList", omList);		
	    } catch (Exception e) {
			log.error("获取订单业务数据错误！", e);
			throw new BaseException("获取订单业务数据错误！", e);
		}
	}
	/**
	 * 获取资金计划业务数据
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void setCapitalData(String cpId)throws BaseException{
		 try{ 
			capitalPlan=this.iCapitalPlanBiz.getCapitalPlan(Long.parseLong(cpId));
			capitalPlan.setWriterCN(BaseDataInfosUtil
					.convertLoginNameToChnName(capitalPlan.getWriter()));
			capitalPlan.setApplicantDeptName(BaseDataInfosUtil.convertDeptIdToName(capitalPlan.getApplicantDeptId()));
			// 获取附件
			capitalPlan.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									capitalPlan.getCpId(),
									AttachmentStatus.ATTACHMENT_CODE_1401)), "0",
							this.getRequest()));		
	    } catch (Exception e) {
			log.error("获取金计划业务数据错误！", e);
			throw new BaseException("获取金计划业务数据错误！", e);
		}
	}
	/**
	 * 保存流程审批请示正文
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveWorkFlowText(String orderNo,String description,String wfName) throws BaseException {
           try{
        	   wfText = new WfText();
        		List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(orderNo));
        		if(orderList!=null&&orderList.size()>0){
        			wfText.setOrderId(orderList.get(0).getId());
        			List<WfText> wftList = this.iWfTextBiz.getWfTextList(wfText);
        			for(WfText wft:wftList){
        				this.iWfTextBiz.deleteWfText(wft);
        			}
        		}
        		wfText.setWfText(description);
        		wfText.setWfName(wfName);
        		this.iWfTextBiz.saveWfText(wfText);
		} catch (Exception e) {
			log.error("保存流程审批请示正文错误！", e);
			throw new BaseException("保存流程审批请示正文错误！", e);
		}
		
	}
	/**
	 * 保存需求计划审批申请人提交
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public boolean  saveInitiatorRequirement(String orderNo,String taskId,String processId,String signContent,String description) throws BaseException {
        boolean falg=false;   
		try{
        	String newOrderNo=WorkFlowStatus.RM_WorkFlow_Type+orderNo;
        	requiredMaterial = this.iRequiredMaterialBiz.getRequiredMaterial(Long.parseLong(orderNo));
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			String reback="";
   			departments = iDepartmentsBiz.getDepartments(requiredMaterial.getDeptId());
			String userChineseName=UserRightInfoUtil.getChineseName(this.getRequest());
   			List<Order> orderList = new ArrayList<Order>();
   			if(StringUtil.isNotBlank(taskId)){
   				Map<String, Object> args = new HashMap<String, Object>();			
   				args.put("buyRemark", requiredMaterial.getRmName());//计划名称
   				args.put("result", "1");
   				args.put("signContent", signContent);//申请人意见
   				facets.executeBackToInit(taskId, user, args);   				
   				
   			}else{
   				if(StringUtil.isNotBlank(processId)){
   					Map<String, Object> args = new HashMap<String, Object>();
   					//动态读取流程节点处理人
   					wfProcessNode = new WfProcessNode();
   					wfProcessNode.setProcessId(processId);
   					List<WfProcessNode> wfpNodeList = this.iWfProcessNodeBiz.getWfProcessNodeList(wfProcessNode);
   					for(WfProcessNode wfpn:wfpNodeList){
   						if("initiator".equals(wfpn.getNodeCode())){//申请人
							args.put("initiator.operator", user);
						}else if("initDepLeader".equals(wfpn.getNodeCode())){//申请人部门经理
							args.put("initDepLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getDepLeaderId()));
						}else if("initCompLeader".equals(wfpn.getNodeCode())){//分管领导
							args.put("initCompLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getCompLeaderId()));
						}else {
							String str="";
							//if(StringUtil.isNotBlank(wfpn.getNodeUserName())){
   							if(wfpn.getUserType()==10){
   								str=wfpn.getNodeOperator();
							    args.put(wfpn.getNodeOperator(), wfpn.getNodeUserName());
   							}else if(wfpn.getUserType()==11){
   							   str=getNextUserNamesBy11(wfpn.getNodeUserId(), wfpn.getUserOrigin());
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==12){
   							   str=getNextUserNamesBy12(departments.getSelflevCode(),wfpn.getNodeUserId(), wfpn.getUserOrigin()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==13){
   							   str=getNextUserNamesBy13(departments.getDepId(),wfpn.getWpnId(), requiredMaterial.getComId()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}
   							if(StringUtil.isBlank(str)) falg=true;
   						}

   					}
   					if(!falg){
	   					args.put("snaker.orderNo", newOrderNo);
	   					args.put("result", "1");
	   					args.put("signContent", signContent);//申请人意见
	   					facets.startAndExecute(processId, user, args);
	   					
	   					reback="需求计划已提交审批";
   					}
   					
   				}
   			}
   			if(!falg){
             //更新需求计划表信息
			requiredMaterial.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1);
			this.iRequiredMaterialBiz.updateRequiredMaterial(requiredMaterial);
			
			//保存流程审批正文
			this.saveWorkFlowText(newOrderNo, description,requiredMaterial.getRmName());
			
			//得到下一个审批人
			orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(newOrderNo));
			Map<String,Object> map=this.getTaskActors(orderList);
			if(map!=null){
				String actorsEn = (String)map.get("actorsEn");
				String[] acArr = actorsEn.split(",");
		        	for(String ac:acArr){
				    //发送邮件提醒领导审批
				    String subject = "待办提示：您有一个需求计划需要审批，计划名称为"+requiredMaterial.getRmName()+"，估算金额为"+requiredMaterial.getTotalBudget()+"。";
	   			    String content = "待办信息：计划编号为"+requiredMaterial.getRmCode()+"，计划名称为"+requiredMaterial.getRmName()+
	   							 "，计划申请人为"+BaseDataInfosUtil.convertLoginNameToChnName(requiredMaterial.getWriter())+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
		   			Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
					if(users!=null){
					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
					   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
				    }
		        }
			}
   		    //更新流程跟踪表记录信息
			purchaseRecordLog = new PurchaseRecordLog(null, requiredMaterial.getRmId(), null, UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(),reback+"，待【"+departments.getDepLeaderCn()+"】处理！","需求计划审批");
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);	
   		  }
		} catch (Exception e) {
			log.error("保存需求计划申请提交流程错误！", e);
			throw new BaseException("保存需求计划申请提交流程错误！", e);
		}
		return falg;
		
	}
	/**
	 * 保存需求计划审批领导审批
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveApproveRequirement(String taskId,String result,String signContent,String opAddTaskActor) throws BaseException {
         try{
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			Task task = facets.getEngine().query().getTask(taskId);
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			Order order = facets.getEngine().query().getOrder(task.getOrderId());
			String orderNo=order.getOrderNo().replace(WorkFlowStatus.RM_WorkFlow_Type, "");
   			requiredMaterial = this.iRequiredMaterialBiz.getRequiredMaterial(Long.parseLong(orderNo));
   			Map<String, Object> args = new HashMap<String, Object>();
   			args.put("result", result);
   			args.put("signContent", signContent);
   			args.put("buyRemark", requiredMaterial.getRmName());//项目名称
   			if(result.equals("-1")){
   				List<Task> tkList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(task.getOrderId()));
   				if(tkList!=null&&tkList.size()>1){
   					for(Task tk:tkList){
   						String[] tActors = facets.getEngine().query().getTaskActorsByTaskId(tk.getId());
   						if(!tActors[0].equals(UserRightInfoUtil.getUserName(getRequest()))){
   							facets.getEngine().task().complete(tk.getId(),tActors[0]);
   						}
   					}
   				}
   				facets.executeAndJumpReback(taskId,order.getId(), user, args, "start");
   				
   				String subject = "待办提示：需求计划编号为："+requiredMaterial.getRmCode()+"的流程被退回！";
   				String content = "需求计划审批未通过："+signContent+"";

				requiredMaterial.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3);
				this.iRequiredMaterialBiz.updateRequiredMaterial(requiredMaterial);
					
   				Users users=BaseDataInfosUtil.convertLoginNameToUsers(requiredMaterial.getWriter());
				if(users!=null){
				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
			    }
   				
   				//更新流程跟踪表记录信息
   				purchaseRecordLog = new PurchaseRecordLog(null, requiredMaterial.getRmId(), null, UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,"需求计划审批");
   				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   				
   			}else{
   				if(StringUtil.isNotBlank(opAddTaskActor)){
   					String[] ops = opAddTaskActor.split(",");
   					facets.getEngine().task().addTaskActor(taskId, PerformType.ALL.ordinal(), ops);
   				}
   				facets.execute(taskId, user, args);
   				
   				//更新需求计划数据表状态和审批时间
   				HistoryOrder hyOrder = facets.getEngine().query().getHistOrder(order.getId());
   				if(hyOrder.getOrderState()==0){
   					requiredMaterial.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
   					requiredMaterial.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iRequiredMaterialBiz.updateRequiredMaterial(requiredMaterial);
   		   			
						
   		   		    String content = "需求计划审批已完成！需求计划编号为"+requiredMaterial.getRmCode()+"。";
   		       	    String subject = "待办提示：需求计划审批已完成，待相关采购员汇总需求计划！需求计划编号为"+requiredMaterial.getRmCode()+"。";
	   				
   					
   				    //更新流程跟踪表记录信息
   					purchaseRecordLog = new PurchaseRecordLog(null, requiredMaterial.getRmId(), null, UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,"需求计划审批");
   					this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   					
   					 Users users=BaseDataInfosUtil.convertLoginNameToUsers(requiredMaterial.getWriter());
   					 if(users!=null){
	   					this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   				 }
   					
   				}else{
   					
   					List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(order.getOrderNo()));
 	 				Map<String,Object> map=this.getTaskActors(orderList);
 	 				if(map!=null){
 	 				String actorsEn = (String)map.get("actorsEn");
 	 				String actorsCn = (String)map.get("actorsCn");
 					
 	 				 //更新流程跟踪表记录信息
   					String content = "需求计划审批已通过："+signContent+"，待【"+actorsCn+"】处理！";
   					purchaseRecordLog = new PurchaseRecordLog(null, requiredMaterial.getRmId(), null, UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,"需求计划审批");
   					this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   				
 					String[] acArr = actorsEn.split(",");
 		        	for(String ac:acArr){
	   				       //发送邮件提醒领导审批
	   						String subject = "待办提示：您有一个需求计划需要审批，计划名称为"+requiredMaterial.getRmName()+"计划名称为:"+requiredMaterial.getRmName()+"，估算金额为"+requiredMaterial.getTotalBudget()+"。";
	   						content = "待办信息：计划编号为"+requiredMaterial.getRmCode()+"，计划名称为"+requiredMaterial.getRmName()+
	   							 "，计划申请人为"+BaseDataInfosUtil.convertLoginNameToChnName(requiredMaterial.getWriter())+"，需求计划审批【"+userChineseName+"】已通过："+signContent+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
	   						
	   						Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
	      					if(users!=null){
	   	   					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
							   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   	   				    }
	   			        }
	   		        }
   				}
   			}
		} catch (Exception e) {
			log.error("保存需求计划领导审批流程错误！", e);
			throw new BaseException("保存需求计划领导审批流程错误！", e);
		}
		
	}
	/**
	 * 撤回待审批的流程
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */
	public String deleteCancelReqProcess() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			this.ajaxHeadInit();
			if(StringUtil.isNotBlank(requiredMaterial.getRmId())){
				requiredMaterial = this.iRequiredMaterialBiz.getRequiredMaterial(requiredMaterial.getRmId());
				String newOrderNo=WorkFlowStatus.RM_WorkFlow_Type+requiredMaterial.getRmId();
                List<HistoryOrder> hyOrderList = facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(newOrderNo));
				if(hyOrderList!=null&&hyOrderList.size()>0){
					List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(new QueryFilter().setOrderId(hyOrderList.get(0).getId()).setOperator(hyOrderList.get(0).getCreator()));
					HistoryTask hyt=hytList.get(hytList.size()-1);
						if("start".equals(hyt.getParentTaskId())){
							facets.getEngine().task().withdrawTask(hyt.getId(), hyOrderList.get(0).getCreator());
							out.print("0");//撤销成功
							//删除所有之前的流程
							facets.getEngine().order().cascadeRemove(hyOrderList.get(0).getId());
							//删除流程正文
							wfText = new WfText();
							wfText.setOrderId(hyOrderList.get(0).getId());
							List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
							if(wtList!=null&&wtList.size()>0){
								for(WfText wt:wtList){
									this.iWfTextBiz.deleteWfText(wt);
								}
							}
							//更新流程状态
							requiredMaterial.setStatus("01");
							this.iRequiredMaterialBiz.updateRequiredMaterial(requiredMaterial);
							
							
							//更新采购流程跟踪表审批记录信息
							purchaseRecordLog = new PurchaseRecordLog();
							purchaseRecordLog.setRmId(requiredMaterial.getRmId());
							purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(getRequest())+"");
							purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(getRequest()));
							purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
							purchaseRecordLog.setOperateContent("【"+BaseDataInfosUtil.convertLoginNameToChnName(hyOrderList.get(0).getCreator())+"】撤回需求计划编号为"+requiredMaterial.getRmCode()+"的审批流程。");
							this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
						}else{
							out.print("3");//流程已经处理，不能撤回
						}
				}else{
					out.print("2");//没有提交流程
				}
			}else{
				out.print("1");//流程编号不存在
			}
		} catch (Exception e) {
			out.print("3");//流程已经处理，不能撤回
			log.error("撤回待审批的流程错误！", e);
			throw new BaseException("撤回待审批的流程错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 保存采购立项审批申请人提交
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public boolean  saveInitiatorPurchase(String orderNo,String taskId,String processId,String signContent,String description) throws BaseException {
		boolean falg=false;      
		try{
        	String newOrderNo=WorkFlowStatus.RC_WorkFlow_Type+orderNo;
        	requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(orderNo));
        	String buyWayCn=(String)purchaseMap.get(requiredCollect.getBuyWay());
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			String user = UserRightInfoUtil.getUserName(getRequest());
   			String reback="";
   			departments = iDepartmentsBiz.getDepartments(requiredCollect.getDeptId());
   			List<Order> orderList = new ArrayList<Order>();
   			if(StringUtil.isNotBlank(taskId)){
   				Map<String, Object> args = new HashMap<String, Object>();			
   				args.put("buyRemark", requiredCollect.getBuyRemark());//计划名称
   				args.put("result", "1");
   				args.put("signContent", signContent);//申请人意见
   				facets.executeBackToInit(taskId, user, args);   				
   				
   			}else{
   				if(StringUtil.isNotBlank(processId)){
   					Map<String, Object> args = new HashMap<String, Object>();
   					//动态读取流程节点处理人
   					wfProcessNode = new WfProcessNode();
   					wfProcessNode.setProcessId(processId);
   					List<WfProcessNode> wfpNodeList = this.iWfProcessNodeBiz.getWfProcessNodeList(wfProcessNode);
   					for(WfProcessNode wfpn:wfpNodeList){
   						if("initiator".equals(wfpn.getNodeCode())){//申请人
							args.put("initiator.operator", user);
						}else if("initDepLeader".equals(wfpn.getNodeCode())){//申请人部门经理
							args.put("initDepLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getDepLeaderId()));
						}else if("initCompLeader".equals(wfpn.getNodeCode())){//分管领导
							args.put("initCompLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getCompLeaderId()));
						}else {
							String str="";
							//if(StringUtil.isNotBlank(wfpn.getNodeUserName())){
   							if(wfpn.getUserType()==10){
   								str=wfpn.getNodeOperator();
							    args.put(wfpn.getNodeOperator(), wfpn.getNodeUserName());
   							}else if(wfpn.getUserType()==11){
   							   str=getNextUserNamesBy11(wfpn.getNodeUserId(), wfpn.getUserOrigin());
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==12){
   							   str=getNextUserNamesBy12(departments.getSelflevCode(),wfpn.getNodeUserId(), wfpn.getUserOrigin()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==13){
   							   str=getNextUserNamesBy13(departments.getDepId(),wfpn.getWpnId(), requiredCollect.getComId()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}
   							if(StringUtil.isBlank(str)) falg=true;
   						}

   					}
   					if(!falg){
	   					args.put("snaker.orderNo", newOrderNo);
	   					args.put("result", "1");
	   					args.put("signContent", signContent);//申请人意见
	   					facets.startAndExecute(processId, user, args);
	   					
	   					reback=buyWayCn+"项目立项已提交审批";
   					}
   					
   				}
   			}
   			if(!falg){
	             //更新需求计划表信息
	   			requiredCollect.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1);
				this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
				
				//保存流程审批正文
				this.saveWorkFlowText(newOrderNo, description,requiredCollect.getBuyRemark());
				
				//得到下一个审批人
				orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(newOrderNo));
				Map<String,Object> map=this.getTaskActors(orderList);
				if(map!=null){
					String actorsEn = (String)map.get("actorsEn");
					String[] acArr = actorsEn.split(",");
			        	for(String ac:acArr){
					    //发送邮件提醒领导审批
					    String subject = "待办提示：您有一个项目立项需要审批，项目名称为"+requiredCollect.getBuyRemark()+"，预算金额为"+requiredCollect.getTotalBudget()+"。";
		   			    String content = "待办信息：项目编号为"+requiredCollect.getBidCode()+"，项目名称为"+requiredCollect.getBuyRemark()+
		   							 "项目立项人为"+BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter())+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
		   			    
			   			 Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
						 if(users!=null){
						   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
					     }
			        }
				}
	   		    //更新流程跟踪表记录信息
				purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(),reback+"，待【"+departments.getDepLeaderCn()+"】处理！",buyWayCn+"项目立项审批。");
				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);	
   			}
		} catch (Exception e) {
			log.error("保存项目立项申请提交流程错误！", e);
			throw new BaseException("保存项目立项申请提交流程错误！", e);
		}
		return falg;
	}

	/**
	 * 保存项目立项审批领导审批
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveApprovePurchase(String taskId,String result,String signContent,String opAddTaskActor) throws BaseException {
         try{
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			Task task = facets.getEngine().query().getTask(taskId);
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			Order order = facets.getEngine().query().getOrder(task.getOrderId());
			String orderNo=order.getOrderNo().replace(WorkFlowStatus.RC_WorkFlow_Type, "");
			requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(orderNo));
        	String buyWayCn=(String)purchaseMap.get(requiredCollect.getBuyWay());
   			Map<String, Object> args = new HashMap<String, Object>();
   			args.put("result", result);
   			args.put("signContent", signContent);
   			args.put("buyRemark", requiredCollect.getBuyRemark());//项目名称
   			if(result.equals("-1")){
   				List<Task> tkList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(task.getOrderId()));
   				if(tkList!=null&&tkList.size()>1){
   					for(Task tk:tkList){
   						String[] tActors = facets.getEngine().query().getTaskActorsByTaskId(tk.getId());
   						if(!tActors[0].equals(UserRightInfoUtil.getUserName(getRequest()))){
   							facets.getEngine().task().complete(tk.getId(),tActors[0]);
   						}
   					}
   				}
   				facets.executeAndJumpReback(taskId,order.getId(), user, args, "start");
   				

				requiredCollect.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3);
				this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect); 
					
   				String subject = "待办提示："+buyWayCn+"项目立项编号为："+requiredCollect.getBidCode()+"的流程被退回！";
   				String content = buyWayCn+"项目立项审批未通过："+signContent+"";
   				
   				Users users=BaseDataInfosUtil.convertLoginNameToUsers(requiredCollect.getWriter());
				if(users!=null){
				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
				   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
			    }
   				
   				//更新流程跟踪表记录信息
   				purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,buyWayCn+"项目立项审批。");
   				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   				
   			}else{
   				if(StringUtil.isNotBlank(opAddTaskActor)){
   					String[] ops = opAddTaskActor.split(",");
   					facets.getEngine().task().addTaskActor(taskId, PerformType.ALL.ordinal(), ops);
   				}
   				facets.execute(taskId, user, args);
   				
   				//更新需求计划数据表状态和审批时间
   				HistoryOrder hyOrder = facets.getEngine().query().getHistOrder(order.getId());
   				if(hyOrder.getOrderState()==0){
   					requiredCollect.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
   					requiredCollect.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);   		   			
						
   		   		    String content = buyWayCn+"项目立项审批已完成！项目编号为"+requiredCollect.getBidCode()+"";
   		       	    String subject = "待办提示："+buyWayCn+"项目立项审批已完成！项目编号为"+requiredCollect.getBidCode()+"";
	   				
   					
   				    //更新流程跟踪表记录信息
   					purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,buyWayCn+"项目立项审批。");
   					this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   					
   					Users users=BaseDataInfosUtil.convertLoginNameToUsers(requiredCollect.getWriter());
	   				if(users!=null){
	   				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
					   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   			    }
   					
   				}else{
   					
   					List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(order.getOrderNo()));
 	 				Map<String,Object> map=this.getTaskActors(orderList);
 	 				if(map!=null){
 	 				String actorsEn = (String)map.get("actorsEn");
 	 				String actorsCn = (String)map.get("actorsCn");
 					
 	 				 //更新流程跟踪表记录信息
   					String content = buyWayCn+"项目立项审批已通过："+signContent+"，待【"+actorsCn+"】处理！";
   					purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,buyWayCn+"项目立项审批。");
   					this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   				
 					String[] acArr = actorsEn.split(",");
 		        	for(String ac:acArr){
	   				       //发送邮件提醒领导审批
	   						String subject = "待办提示：您有一个项目立项需要审批，预算金额为"+requiredCollect.getTotalBudget()+"。";
	   						content = "待办信息：项目编号为"+requiredCollect.getBidCode()+"，项目名称为"+requiredCollect.getBuyRemark()+
	   							 "项目立项人为"+BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter())+"，项目立项审批【"+userChineseName+"】已通过："+signContent+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
	   						
	   						Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
		   					if(users!=null){
		   					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
							   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
		   				    }
	   			        }
	   		        }
   				}
   			}
		} catch (Exception e) {
			log.error("保存项目立项领导审批流程错误！", e);
			throw new BaseException("保存项目立项领导审批流程错误！", e);
		}
		
	}
	/**
	 * 撤回待审批的流程
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */
	public String deleteCancelPurchaseProcess() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			this.ajaxHeadInit();
			if(StringUtil.isNotBlank(requiredCollect.getRcId())){
				requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
				String newOrderNo=WorkFlowStatus.RC_WorkFlow_Type+requiredCollect.getRcId();
                List<HistoryOrder> hyOrderList = facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(newOrderNo));
				if(hyOrderList!=null&&hyOrderList.size()>0){
					List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(new QueryFilter().setOrderId(hyOrderList.get(0).getId()).setOperator(hyOrderList.get(0).getCreator()));
					HistoryTask hyt=hytList.get(hytList.size()-1);
						if("start".equals(hyt.getParentTaskId())){
							facets.getEngine().task().withdrawTask(hyt.getId(), hyOrderList.get(0).getCreator());
							out.print("0");//撤销成功
							//删除所有之前的流程
							facets.getEngine().order().cascadeRemove(hyOrderList.get(0).getId());
							//删除流程正文
							wfText = new WfText();
							wfText.setOrderId(hyOrderList.get(0).getId());
							List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
							if(wtList!=null&&wtList.size()>0){
								for(WfText wt:wtList){
									this.iWfTextBiz.deleteWfText(wt);
								}
							}
							//更新流程状态
							requiredCollect.setStatus("01");
							this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
							
							
							//更新采购流程跟踪表审批记录信息
							purchaseRecordLog = new PurchaseRecordLog();
							purchaseRecordLog.setRcId(requiredCollect.getRcId());
							purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(getRequest())+"");
							purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(getRequest()));
							purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
							purchaseRecordLog.setOperateContent("【"+BaseDataInfosUtil.convertLoginNameToChnName(hyOrderList.get(0).getCreator())+"】撤回项目编号为"+requiredCollect.getBidCode()+"的审批流程。");
							this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
						}else{
							out.print("3");//流程已经处理，不能撤回
						}
				}else{
					out.print("2");//没有提交流程
				}
			}else{
				out.print("1");//流程编号不存在
			}
		} catch (Exception e) {
			out.print("3");//流程已经处理，不能撤回
			log.error("撤回待审批的流程错误！", e);
			throw new BaseException("撤回待审批的流程错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 保存授标审批申请人提交
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public boolean  saveInitiatorPurchaseResult(String orderNo,String taskId,String processId,String signContent,String description) throws BaseException {
		boolean falg=false;      
		try{
        	String newOrderNo=WorkFlowStatus.PR_WorkFlow_Type+orderNo;
        	requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(orderNo));
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
        	String buyWayCn=(String)purchaseMap.get(requiredCollect.getBuyWay());
			String user = UserRightInfoUtil.getUserName(getRequest());
   			String reback="";
   			departments = iDepartmentsBiz.getDepartments(requiredCollect.getDeptId());
   			List<Order> orderList = new ArrayList<Order>();
   			if(StringUtil.isNotBlank(taskId)){
   				Map<String, Object> args = new HashMap<String, Object>();			
   				args.put("buyRemark", requiredCollect.getBuyRemark());//项目名称
   				args.put("result", "1");
   				args.put("signContent", signContent);//申请人意见
   				facets.executeBackToInit(taskId, user, args);   				
   				
   			}else{
   				if(StringUtil.isNotBlank(processId)){
   					Map<String, Object> args = new HashMap<String, Object>();
   					//动态读取流程节点处理人
   					wfProcessNode = new WfProcessNode();
   					wfProcessNode.setProcessId(processId);
   					List<WfProcessNode> wfpNodeList = this.iWfProcessNodeBiz.getWfProcessNodeList(wfProcessNode);
   					for(WfProcessNode wfpn:wfpNodeList){
   						if("initiator".equals(wfpn.getNodeCode())){//申请人
							args.put("initiator.operator", user);
						}else if("initDepLeader".equals(wfpn.getNodeCode())){//申请人部门经理
							args.put("initDepLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getDepLeaderId()));
						}else if("initCompLeader".equals(wfpn.getNodeCode())){//分管领导
							args.put("initCompLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getCompLeaderId()));
						}else {
							String str="";
							//if(StringUtil.isNotBlank(wfpn.getNodeUserName())){
   							if(wfpn.getUserType()==10){
   								str=wfpn.getNodeOperator();
							    args.put(wfpn.getNodeOperator(), wfpn.getNodeUserName());
   							}else if(wfpn.getUserType()==11){
   							   str=getNextUserNamesBy11(wfpn.getNodeUserId(), wfpn.getUserOrigin());
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==12){
   							   str=getNextUserNamesBy12(departments.getSelflevCode(),wfpn.getNodeUserId(), wfpn.getUserOrigin()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==13){
   							   str=getNextUserNamesBy13(departments.getDepId(),wfpn.getWpnId(), requiredCollect.getComId()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}
   							if(StringUtil.isBlank(str)) falg=true;
   						}

   					}
   					if(!falg){
	   					args.put("snaker.orderNo", newOrderNo);
	   					args.put("result", "1");
	   					args.put("signContent", signContent);//申请人意见
	   					facets.startAndExecute(processId, user, args);
	   					
	   					reback=buyWayCn+"项目授标已提交审批";
   					}
   					
   				}
   			}	
   			if(!falg){
				//保存流程审批正文
				this.saveWorkFlowText(newOrderNo, description,requiredCollect.getBuyRemark());
				
				//得到下一个审批人
				orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(newOrderNo));
				Map<String,Object> map=this.getTaskActors(orderList);
				if(map!=null){
					String actorsEn = (String)map.get("actorsEn");
					String[] acArr = actorsEn.split(",");
			        	for(String ac:acArr){
					    //发送邮件提醒领导审批
					    String subject = "待办提示：您有一个项目授标需要审批，项目名称为"+requiredCollect.getBuyRemark()+"。";
		   			    String content = "待办信息：项目编号为"+requiredCollect.getBidCode()+"，项目名称为"+requiredCollect.getBuyRemark()+
		   							 "，项目立项人为"+BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter())+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
			   			 
		   			    Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
						if(users!=null){
						   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
					    }
			        }
				}
	   		    //更新流程跟踪表记录信息
				purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(),reback+"，待【"+departments.getDepLeaderCn()+"】处理！",buyWayCn+"项目授标审批。");
				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);	
	   			}
		} catch (Exception e) {
			log.error("保存项目授标申请提交流程错误！", e);
			throw new BaseException("保存项目授标申请提交流程错误！", e);
		}
		return falg;
	}

	/**
	 * 保存项目授标审批领导审批
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveApprovePurchaseResult(String taskId,String result,String signContent,String opAddTaskActor) throws BaseException {
         try{
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			Task task = facets.getEngine().query().getTask(taskId);
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			Order order = facets.getEngine().query().getOrder(task.getOrderId());
			String orderNo=order.getOrderNo().replace(WorkFlowStatus.PR_WorkFlow_Type, "");
			requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(Long.parseLong(orderNo));
        	String buyWayCn=(String)purchaseMap.get(requiredCollect.getBuyWay());
   			Map<String, Object> args = new HashMap<String, Object>();
   			args.put("result", result);
   			args.put("signContent", signContent);
   			args.put("buyRemark", requiredCollect.getBuyRemark());//项目名称
   			if(result.equals("-1")){
   				List<Task> tkList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(task.getOrderId()));
   				if(tkList!=null&&tkList.size()>1){
   					for(Task tk:tkList){
   						String[] tActors = facets.getEngine().query().getTaskActorsByTaskId(tk.getId());
   						if(!tActors[0].equals(UserRightInfoUtil.getUserName(getRequest()))){
   							facets.getEngine().task().complete(tk.getId(),tActors[0]);
   						}
   					}
   				}
   				facets.executeAndJumpReback(taskId,order.getId(), user, args, "start");
   				
   				String subject = "待办提示："+buyWayCn+"项目编号为："+requiredCollect.getBidCode()+"的流程被退回！";
   				String content = buyWayCn+"项目授标审批未通过："+signContent+"";
   				
   				Users users=BaseDataInfosUtil.convertLoginNameToUsers(requiredCollect.getWriter());
				if(users!=null){
				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
				   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
			    }
   				//更新流程跟踪表记录信息
   				purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,buyWayCn+"项目授标审批。");
   				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   				
   			}else{
   				if(StringUtil.isNotBlank(opAddTaskActor)){
   					String[] ops = opAddTaskActor.split(",");
   					facets.getEngine().task().addTaskActor(taskId, PerformType.ALL.ordinal(), ops);
   				}
   				facets.execute(taskId, user, args);
   				
   				//更新需求计划数据表状态和审批时间
   				HistoryOrder hyOrder = facets.getEngine().query().getHistOrder(order.getId());
   				if(hyOrder.getOrderState()==0){		   			
					this.iBidAwardBiz.updateBidAwardStatus(requiredCollect.getRcId());
   					
   		   		    String content = buyWayCn+"项目授标审批已完成！项目编号为【"+requiredCollect.getBidCode()+"】";
   		       	    String subject = "待办提示："+buyWayCn+"项目授标审批已完成！项目编号为【"+requiredCollect.getBidCode()+"】";
	   				
   					
   				    //更新流程跟踪表记录信息
   					purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,buyWayCn+"项目授标审批。");
   					this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   					
   					Users users=BaseDataInfosUtil.convertLoginNameToUsers(requiredCollect.getWriter());
	   				if(users!=null){
	   				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
					   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   			    }
   					
   				}else{
   					
   					List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(order.getOrderNo()));
 	 				Map<String,Object> map=this.getTaskActors(orderList);
 	 				if(map!=null){
 	 				String actorsEn = (String)map.get("actorsEn");
 	 				String actorsCn = (String)map.get("actorsCn");
 					
 	 				 //更新流程跟踪表记录信息
   					String content = buyWayCn+"项目授标审批已通过："+signContent+"，待【"+actorsCn+"】处理！";
   					purchaseRecordLog = new PurchaseRecordLog(null, null, requiredCollect.getRcId(), UserRightInfoUtil.getUserId(getRequest())+"", user, DateUtil.getCurrentDateTime(), content,buyWayCn+"项目授标审批。");
   					this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
   				
 					String[] acArr = actorsEn.split(",");
 		        	for(String ac:acArr){
	   				       //发送邮件提醒领导审批
	   						String subject = "待办提示：您有一个项目授标需要审批，项目名称为"+requiredCollect.getBuyRemark()+"";
	   						content = "待办信息：项目编号为"+requiredCollect.getBidCode()+"，项目名称为"+requiredCollect.getBuyRemark()+
	   							 "，项目立项人为"+BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter())+"，项目授标审批【"+userChineseName+"】已通过："+signContent+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
	   						Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
		   					if(users!=null){
		   					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
							   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
		   				    }
	   			        }
	   		        }
   				}
   			}
		} catch (Exception e) {
			log.error("保存项目授标领导审批流程错误！", e);
			throw new BaseException("保存项目授标领导审批流程错误！", e);
		}
		
	}
	/**
	 * 撤回待审批的流程
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */
	public String deleteCancelPurchaseResultProcess() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			this.ajaxHeadInit();
			if(StringUtil.isNotBlank(requiredCollect.getRcId())){
				requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
				String newOrderNo=WorkFlowStatus.PR_WorkFlow_Type+requiredCollect.getRcId();
                List<HistoryOrder> hyOrderList = facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(newOrderNo));
				if(hyOrderList!=null&&hyOrderList.size()>0){
					List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(new QueryFilter().setOrderId(hyOrderList.get(0).getId()).setOperator(hyOrderList.get(0).getCreator()));
					HistoryTask hyt=hytList.get(hytList.size()-1);
						if("start".equals(hyt.getParentTaskId())){
							facets.getEngine().task().withdrawTask(hyt.getId(), hyOrderList.get(0).getCreator());
							out.print("0");//撤销成功
							//删除所有之前的流程
							facets.getEngine().order().cascadeRemove(hyOrderList.get(0).getId());
							//删除流程正文
							wfText = new WfText();
							wfText.setOrderId(hyOrderList.get(0).getId());
							List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
							if(wtList!=null&&wtList.size()>0){
								for(WfText wt:wtList){
									this.iWfTextBiz.deleteWfText(wt);
								}
							}
							//更新流程状态
							requiredCollect.setStatus("01");
							this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
							
							
							//更新采购流程跟踪表审批记录信息
							purchaseRecordLog = new PurchaseRecordLog();
							purchaseRecordLog.setRcId(requiredCollect.getRcId());
							purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(getRequest())+"");
							purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(getRequest()));
							purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
							purchaseRecordLog.setOperateContent("【"+BaseDataInfosUtil.convertLoginNameToChnName(hyOrderList.get(0).getCreator())+"】撤回项目编号为"+requiredCollect.getBidCode()+"的审批流程。");
							this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
						}else{
							out.print("3");//流程已经处理，不能撤回
						}
				}else{
					out.print("2");//没有提交流程
				}
			}else{
				out.print("1");//流程编号不存在
			}
		} catch (Exception e) {
			out.print("3");//流程已经处理，不能撤回
			log.error("撤回待审批的流程错误！", e);
			throw new BaseException("撤回待审批的流程错误！", e);
		}
		return null ;		
	}
	/**
	 * 流程消息提醒
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */
	public String saveWorkFlowReminders() throws BaseException{
        String result="0";	
        PrintWriter writer =null; 
    	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
		try{
			writer = getResponse().getWriter(); 
			String orderId = getRequest().getParameter("orderId");
			String taskName = getRequest().getParameter("taskName");
				Order order = facets.getEngine().query().getOrder(orderId);
				String wfName=getWorkFlowName(orderId);
				List<Task> tasks = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(orderId));
				Process process = facets.getEngine().process().getProcessById(order.getProcessId());
		        SysMessageLog messageLog=null;
		        for(Task task : tasks) {
		            if(task.getTaskName().equalsIgnoreCase(taskName)) {
		                String[] actors = facets.getEngine().query().getTaskActorsByTaskId(task.getId());
		                for(String actor : actors) {
		                	messageLog=new SysMessageLog();
		                	messageLog.setMessageContent("流程催办提醒：【"+process.getDisplayName()+"】--【"+wfName+"】请领导审批！");
		                	messageLog.setSendDate(new Date());
		                	messageLog.setSendPerson(UserRightInfoUtil.getUserName(this.getRequest()));
		                	messageLog.setSendPersonCn(UserRightInfoUtil.getChineseName(this.getRequest()));
		                	messageLog.setReceivePerson(actor);
		                	messageLog.setStatus("0");
		                	messageLog.setReceivePersonCn(BaseDataInfosUtil.convertLoginNameToChnName(actor));
		                	messageLog.setMessageType("0");
		                	this.iSysMessageLogBiz.saveSysMessageLog(messageLog);
		                	
		                	//发送邮件
		    				String subject = "流程催办提醒：【"+process.getDisplayName()+"】--【"+wfName+"】请领导审批！";
		    				String content = "流程催办提醒：【"+process.getDisplayName()+"】--【"+wfName+"】请领导审批！";
		    				
		    				Users users=BaseDataInfosUtil.convertLoginNameToUsers(actor);
			   				if(users!=null){
			   				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
							   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
			   			    }
		                }
		            }
		        }
		        result="1"; 
			
		} catch (Exception e) {
			log("流程催办错误！", e);
			throw new BaseException("流程催办错误！", e);
		} 
        writer.print(result);  
        writer.flush();  
        writer.close();
		return null;
	}
	/**
	 * 保存合同审批申请人提交
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public boolean  saveInitiatorContract(String orderNo,String taskId,String processId,String signContent,String description) throws BaseException {
		boolean falg=false;      
		try{
        	String newOrderNo=WorkFlowStatus.CT_WorkFlow_Type+orderNo;
        	contractInfo = this.iContractInfoBiz.getContractInfo(Long.parseLong(orderNo));
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			departments = iDepartmentsBiz.getDepartments(contractInfo.getDeptId());
   			List<Order> orderList = new ArrayList<Order>();
   			if(StringUtil.isNotBlank(taskId)){
   				Map<String, Object> args = new HashMap<String, Object>();			
   				args.put("buyRemark", contractInfo.getContractName());//合同名称
   				args.put("result", "1");
   				args.put("signContent", signContent);//申请人意见
   				facets.executeBackToInit(taskId, user, args);   				
   				
   			}else{
   				if(StringUtil.isNotBlank(processId)){
   					Map<String, Object> args = new HashMap<String, Object>();
   					//动态读取流程节点处理人
   					wfProcessNode = new WfProcessNode();
   					wfProcessNode.setProcessId(processId);
   					List<WfProcessNode> wfpNodeList = this.iWfProcessNodeBiz.getWfProcessNodeList(wfProcessNode);
   					for(WfProcessNode wfpn:wfpNodeList){
   						if("initiator".equals(wfpn.getNodeCode())){//申请人
							args.put("initiator.operator", user);
						}else if("initDepLeader".equals(wfpn.getNodeCode())){//申请人部门经理
							args.put("initDepLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getDepLeaderId()));
						}else if("initCompLeader".equals(wfpn.getNodeCode())){//分管领导
							args.put("initCompLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getCompLeaderId()));
						}else {
							String str="";
							//if(StringUtil.isNotBlank(wfpn.getNodeUserName())){
   							if(wfpn.getUserType()==10){
   								str=wfpn.getNodeOperator();
							    args.put(wfpn.getNodeOperator(), wfpn.getNodeUserName());
   							}else if(wfpn.getUserType()==11){
   							   str=getNextUserNamesBy11(wfpn.getNodeUserId(), wfpn.getUserOrigin());
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==12){
   							   str=getNextUserNamesBy12(departments.getSelflevCode(),wfpn.getNodeUserId(), wfpn.getUserOrigin()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==13){
   							   str=getNextUserNamesBy13(departments.getDepId(),wfpn.getWpnId(), contractInfo.getComId()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}
   							if(StringUtil.isBlank(str)) falg=true;
   						}

   					}
   					if(!falg){
	   					args.put("snaker.orderNo", newOrderNo);
	   					args.put("result", "1");
	   					args.put("signContent", signContent);//申请人意见
	   					facets.startAndExecute(processId, user, args);
   					}
   				}
   			}
   			if(!falg){
	             //更新合同表信息
				contractInfo.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1);
				contractInfo.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1_TEXT);
				this.iContractInfoBiz.updateContractInfo(contractInfo);
				
				//保存流程审批正文
				this.saveWorkFlowText(newOrderNo, description,contractInfo.getContractName());
				
				//得到下一个审批人
				orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(newOrderNo));
				Map<String,Object> map=this.getTaskActors(orderList);
				if(map!=null){
					String actorsEn = (String)map.get("actorsEn");
					String[] acArr = actorsEn.split(",");
			        	for(String ac:acArr){
					    //发送邮件提醒领导审批
					    String subject = "待办提示：您有一个合同需要审批，合同编号为"+contractInfo.getContractName()+"，合同金额为"+contractInfo.getContractMoney()+"。";
		   			    String content = "待办信息：合同编号为"+contractInfo.getContractCode()+"，合同名称为"+contractInfo.getContractName()+
		   							 "，合同负责人为"+BaseDataInfosUtil.convertLoginNameToChnName(contractInfo.getWriter())+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
			   			
		   			    Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
						if(users!=null){
						   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
					    }
			        }
				}   		    
   			}
		} catch (Exception e) {
			log.error("保存合同审批提交流程错误！", e);
			throw new BaseException("保存合同审批提交流程错误！", e);
		}
		return falg;
	}
	/**
	 * 保存合同审批领导审批
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveApproveContract(String taskId,String result,String signContent,String opAddTaskActor) throws BaseException {
         try{
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			Task task = facets.getEngine().query().getTask(taskId);
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			Order order = facets.getEngine().query().getOrder(task.getOrderId());
			String orderNo=order.getOrderNo().replace(WorkFlowStatus.CT_WorkFlow_Type, "");
   			contractInfo = this.iContractInfoBiz.getContractInfo(Long.parseLong(orderNo));
   			Map<String, Object> args = new HashMap<String, Object>();
   			args.put("result", result);
   			args.put("signContent", signContent);
   			args.put("buyRemark", contractInfo.getContractName());//合同名称
   			if(result.equals("-1")){
   				List<Task> tkList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(task.getOrderId()));
   				if(tkList!=null&&tkList.size()>1){
   					for(Task tk:tkList){
   						String[] tActors = facets.getEngine().query().getTaskActorsByTaskId(tk.getId());
   						if(!tActors[0].equals(UserRightInfoUtil.getUserName(getRequest()))){
   							facets.getEngine().task().complete(tk.getId(),tActors[0]);
   						}
   					}
   				}
   				facets.executeAndJumpReback(taskId,order.getId(), user, args, "start");

				contractInfo.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3);
				contractInfo.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3_TEXT);
				this.iContractInfoBiz.updateContractInfo(contractInfo);
					
   				String subject = "待办提示：合同编号为【"+contractInfo.getContractCode()+"】的流程被退回！";
   				String content = "合同审批未通过："+signContent+"";
   				
   				Users users=BaseDataInfosUtil.convertLoginNameToUsers(contractInfo.getWriter());
				if(users!=null){
				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
				   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
			    }
   				
   				
   			}else{
   				if(StringUtil.isNotBlank(opAddTaskActor)){
   					String[] ops = opAddTaskActor.split(",");
   					facets.getEngine().task().addTaskActor(taskId, PerformType.ALL.ordinal(), ops);
   				}
   				facets.execute(taskId, user, args);
   				
   				//更新合同审批数据表状态和审批时间
   				HistoryOrder hyOrder = facets.getEngine().query().getHistOrder(order.getId());
   				if(hyOrder.getOrderState()==0){
   					contractInfo.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
   					contractInfo.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0_TEXT);
   					contractInfo.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iContractInfoBiz.updateContractInfo(contractInfo);
   					
   		   			
						
   		   		    String content = "合同审批已完成！合同编号为"+contractInfo.getContractCode()+"。";
   		       	    String subject = "待办提示：合同审批已完成，请发送此合同向供应商进行确认！合同编号为【"+contractInfo.getContractCode()+"】，合同名称为【"+contractInfo.getContractName()+"】";
	   				
   					
   					 Users users=BaseDataInfosUtil.convertLoginNameToUsers(contractInfo.getWriter());
   					 if(users!=null){
	   					this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   				 }
   					
   				}else{
   					
   					List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(order.getOrderNo()));
 	 				Map<String,Object> map=this.getTaskActors(orderList);
 	 				if(map!=null){
 	 				String actorsEn = (String)map.get("actorsEn"); 					
 	 				
   					String content = "";
   					
 					String[] acArr = actorsEn.split(",");
 		        	for(String ac:acArr){
	   				       //发送邮件提醒领导审批
	   						String subject = "待办提示：您有一个合同需要审批，合同编号为"+contractInfo.getContractCode()+"，合同金额为"+contractInfo.getContractMoney()+"。";
	   						content = "待办信息：合同编号为"+contractInfo.getContractCode()+"，合同名称为"+contractInfo.getContractName()+
	   							 "，合同负责人为"+BaseDataInfosUtil.convertLoginNameToChnName(contractInfo.getWriter())+"，合同审批【"+userChineseName+"】已通过："+signContent+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
	   						
	   						Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
	      					if(users!=null){
	   	   					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
	   						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   	   				    }
	   			        }
	   		        }
   				}
   			}
		} catch (Exception e) {
			log.error("保存合同审批流程错误！", e);
			throw new BaseException("保存合同审批流程错误！", e);
		}		
	}
	/**
	 * 撤回待审批的合同流程
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */
	public String deleteCancelContractInfoProcess() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			this.ajaxHeadInit();
			if(StringUtil.isNotBlank(contractInfo.getCiId())){
				contractInfo = this.iContractInfoBiz.getContractInfo(contractInfo.getCiId());
				String newOrderNo=WorkFlowStatus.CT_WorkFlow_Type+contractInfo.getCiId();
                List<HistoryOrder> hyOrderList = facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(newOrderNo));
				if(hyOrderList!=null&&hyOrderList.size()>0){
					List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(new QueryFilter().setOrderId(hyOrderList.get(0).getId()).setOperator(hyOrderList.get(0).getCreator()));
					HistoryTask hyt=hytList.get(hytList.size()-1);
						if("start".equals(hyt.getParentTaskId())){
							facets.getEngine().task().withdrawTask(hyt.getId(), hyOrderList.get(0).getCreator());
							out.print("0");//撤销成功
							//删除所有之前的流程
							facets.getEngine().order().cascadeRemove(hyOrderList.get(0).getId());
							//删除流程正文
							wfText = new WfText();
							wfText.setOrderId(hyOrderList.get(0).getId());
							List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
							if(wtList!=null&&wtList.size()>0){
								for(WfText wt:wtList){
									this.iWfTextBiz.deleteWfText(wt);
								}
							}
							//更新流程状态
							contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_01);
							contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_01_Text);
							this.iContractInfoBiz.updateContractInfo(contractInfo);
							
						}else{
							out.print("3");//流程已经处理，不能撤回
						}
				}else{
					out.print("2");//没有提交流程
				}
			}else{
				out.print("1");//流程编号不存在
			}
		} catch (Exception e) {
			out.print("3");//流程已经处理，不能撤回
			log.error("撤回待审批的合同流程错误！", e);
			throw new BaseException("撤回待审批的合同流程错误！", e);
		}
		return null ;		
	}
	/**
	 * 保存订单审批申请人提交
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public boolean  saveInitiatorOrder(String orderNo,String taskId,String processId,String signContent,String description) throws BaseException {
		boolean falg=false;      
		try{
        	String newOrderNo=WorkFlowStatus.OD_WorkFlow_Type+orderNo;
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
        	orderInfo = this.iOrderInfoBiz.getOrderInfo(Long.parseLong(orderNo));
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			departments = iDepartmentsBiz.getDepartments(orderInfo.getDeptId());
   			List<Order> orderList = new ArrayList<Order>();
   			if(StringUtil.isNotBlank(taskId)){
   				Map<String, Object> args = new HashMap<String, Object>();			
   				args.put("buyRemark", orderInfo.getOrderName());//订单名称
   				args.put("result", "1");
   				args.put("signContent", signContent);//申请人意见
   				facets.executeBackToInit(taskId, user, args);   				
   				
   			}else{
   				if(StringUtil.isNotBlank(processId)){
   					Map<String, Object> args = new HashMap<String, Object>();
   					//动态读取流程节点处理人
   					wfProcessNode = new WfProcessNode();
   					wfProcessNode.setProcessId(processId);
   					List<WfProcessNode> wfpNodeList = this.iWfProcessNodeBiz.getWfProcessNodeList(wfProcessNode);
   					for(WfProcessNode wfpn:wfpNodeList){
   						if("initiator".equals(wfpn.getNodeCode())){//申请人
							args.put("initiator.operator", user);
						}else if("initDepLeader".equals(wfpn.getNodeCode())){//申请人部门经理
							args.put("initDepLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getDepLeaderId()));
						}else if("initCompLeader".equals(wfpn.getNodeCode())){//分管领导
							args.put("initCompLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getCompLeaderId()));
						}else {
							String str="";
							//if(StringUtil.isNotBlank(wfpn.getNodeUserName())){
   							if(wfpn.getUserType()==10){
   								str=wfpn.getNodeOperator();
							    args.put(wfpn.getNodeOperator(), wfpn.getNodeUserName());
   							}else if(wfpn.getUserType()==11){
   							   str=getNextUserNamesBy11(wfpn.getNodeUserId(), wfpn.getUserOrigin());
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==12){
   							   str=getNextUserNamesBy12(departments.getSelflevCode(),wfpn.getNodeUserId(), wfpn.getUserOrigin()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==13){
   							   str=getNextUserNamesBy13(departments.getDepId(),wfpn.getWpnId(), orderInfo.getComId()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}
   							if(StringUtil.isBlank(str)) falg=true;
   						}

   					}
   					if(!falg){
	   					args.put("snaker.orderNo", newOrderNo);
	   					args.put("result", "1");
	   					args.put("signContent", signContent);//申请人意见
	   					facets.startAndExecute(processId, user, args);
   					}
   				}
   			}
   			if(!falg){
	             //更新订单表信息
				orderInfo.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1);
				orderInfo.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1_TEXT);
				this.iOrderInfoBiz.updateOrderInfo(orderInfo);
				
				//保存流程审批正文
				this.saveWorkFlowText(newOrderNo, description,orderInfo.getOrderName());
				
				//得到下一个审批人
				orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(newOrderNo));
				Map<String,Object> map=this.getTaskActors(orderList);
				if(map!=null){
					String actorsEn = (String)map.get("actorsEn");
					String[] acArr = actorsEn.split(",");
			        	for(String ac:acArr){
					    //发送邮件提醒领导审批
					    String subject = "待办提示：您有一个订单需要审批，订单编号为"+orderInfo.getOrderName()+",订单金额为"+orderInfo.getOrderMoney()+"。";
		   			    String content = "待办信息：订单编号为"+orderInfo.getOrderCode()+"，订单名称为"+orderInfo.getOrderName()+
		   							 "，订单负责人为"+BaseDataInfosUtil.convertLoginNameToChnName(orderInfo.getWriter())+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
			   			
		   			    Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
						if(users!=null){
						   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
					    }
			        }
				}   		    
   			}
		} catch (Exception e) {
			log.error("保存订单审批提交流程错误！", e);
			throw new BaseException("保存订单审批提交流程错误！", e);
		}
		return falg;
	}
	/**
	 * 保存订单审批领导审批
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveApproveOrder(String taskId,String result,String signContent,String opAddTaskActor) throws BaseException {
         try{
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			Task task = facets.getEngine().query().getTask(taskId);
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			Order order = facets.getEngine().query().getOrder(task.getOrderId());
			String orderNo=order.getOrderNo().replace(WorkFlowStatus.OD_WorkFlow_Type, "");
   			orderInfo = this.iOrderInfoBiz.getOrderInfo(Long.parseLong(orderNo));
   			Map<String, Object> args = new HashMap<String, Object>();
   			args.put("result", result);
   			args.put("signContent", signContent);
   			args.put("buyRemark", orderInfo.getOrderName());//订单名称
   			if(result.equals("-1")){
   				List<Task> tkList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(task.getOrderId()));
   				if(tkList!=null&&tkList.size()>1){
   					for(Task tk:tkList){
   						String[] tActors = facets.getEngine().query().getTaskActorsByTaskId(tk.getId());
   						if(!tActors[0].equals(UserRightInfoUtil.getUserName(getRequest()))){
   							facets.getEngine().task().complete(tk.getId(),tActors[0]);
   						}
   					}
   				}
   				facets.executeAndJumpReback(taskId,order.getId(), user, args, "start");

				orderInfo.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3);
				orderInfo.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3_TEXT);
				this.iOrderInfoBiz.updateOrderInfo(orderInfo);
					
   				String subject = "待办提示：订单编号为【"+orderInfo.getOrderCode()+"】的流程被退回！";
   				String content = "订单审批未通过："+signContent+"";
   				
   				Users users=BaseDataInfosUtil.convertLoginNameToUsers(orderInfo.getWriter());
				if(users!=null){
				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
				   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
			    }
   				
   				
   			}else{
   				if(StringUtil.isNotBlank(opAddTaskActor)){
   					String[] ops = opAddTaskActor.split(",");
   					facets.getEngine().task().addTaskActor(taskId, PerformType.ALL.ordinal(), ops);
   				}
   				facets.execute(taskId, user, args);
   				
   				//更新订单审批数据表状态和审批时间
   				HistoryOrder hyOrder = facets.getEngine().query().getHistOrder(order.getId());
   				if(hyOrder.getOrderState()==0){
   					orderInfo.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
   					orderInfo.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0_TEXT);
   					orderInfo.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iOrderInfoBiz.updateOrderInfo(orderInfo);
   					
						
   		   		    String content = "订单审批已完成！订单编号为"+orderInfo.getOrderCode()+"。";
   		       	    String subject = "待办提示：订单审批已完成，请发送此订单向供应商进行确认！订单编号为【"+orderInfo.getOrderCode()+"】，订单名称为【"+orderInfo.getOrderName()+"】";
	   				
   					
   					 Users users=BaseDataInfosUtil.convertLoginNameToUsers(orderInfo.getWriter());
   					 if(users!=null){
	   					this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   				 }
   					
   				}else{
   					
   					List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(order.getOrderNo()));
 	 				Map<String,Object> map=this.getTaskActors(orderList);
 	 				if(map!=null){
 	 				String actorsEn = (String)map.get("actorsEn"); 					
 	 				
   					String content = "";
   					
 					String[] acArr = actorsEn.split(",");
 		        	for(String ac:acArr){
	   				       //发送邮件提醒领导审批
	   						String subject = "待办提示：您有一个订单需要审批，订单编号为"+orderInfo.getOrderCode()+",订单金额为"+orderInfo.getOrderMoney()+"。";
	   						content = "待办信息：订单编号为"+orderInfo.getOrderCode()+"，订单名称为"+orderInfo.getOrderName()+
	   							 "，订单负责人为"+BaseDataInfosUtil.convertLoginNameToChnName(orderInfo.getWriter())+"，订单审批【"+userChineseName+"】已通过："+signContent+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
	   						
	   						Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
	      					if(users!=null){
	   	   					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
	   						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   	   				    }
	   			        }
	   		        }
   				}
   			}
		} catch (Exception e) {
			log.error("保存订单审批流程错误！", e);
			throw new BaseException("保存订单审批流程错误！", e);
		}		
	}
	/**
	 * 撤回待审批的订单流程
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */
	public String deleteCancelOrderInfoProcess() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			this.ajaxHeadInit();
			if(StringUtil.isNotBlank(orderInfo.getOiId())){
				orderInfo = this.iOrderInfoBiz.getOrderInfo(orderInfo.getOiId());
				String newOrderNo=WorkFlowStatus.OD_WorkFlow_Type+orderInfo.getOiId();
                List<HistoryOrder> hyOrderList = facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(newOrderNo));
				if(hyOrderList!=null&&hyOrderList.size()>0){
					List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(new QueryFilter().setOrderId(hyOrderList.get(0).getId()).setOperator(hyOrderList.get(0).getCreator()));
					HistoryTask hyt=hytList.get(hytList.size()-1);
						if("start".equals(hyt.getParentTaskId())){
							facets.getEngine().task().withdrawTask(hyt.getId(), hyOrderList.get(0).getCreator());
							out.print("0");//撤销成功
							//删除所有之前的流程
							facets.getEngine().order().cascadeRemove(hyOrderList.get(0).getId());
							//删除流程正文
							wfText = new WfText();
							wfText.setOrderId(hyOrderList.get(0).getId());
							List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
							if(wtList!=null&&wtList.size()>0){
								for(WfText wt:wtList){
									this.iWfTextBiz.deleteWfText(wt);
								}
							}
							//更新流程状态
							orderInfo.setRunStatus(OrderStatus.ORDER_RUN_STATUS_01);
							orderInfo.setStatusCn(OrderStatus.ORDER_RUN_STATUS_01_Text);
							this.iOrderInfoBiz.updateOrderInfo(orderInfo);
							
						}else{
							out.print("3");//流程已经处理，不能撤回
						}
				}else{
					out.print("2");//没有提交流程
				}
			}else{
				out.print("1");//流程编号不存在
			}
		} catch (Exception e) {
			out.print("3");//流程已经处理，不能撤回
			log.error("撤回待审批的订单流程错误！", e);
			throw new BaseException("撤回待审批的订单流程错误！", e);
		}
		return null ;		
	}
	/**
	 * 保存资金计划审批申请人提交
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public boolean  saveInitiatorCapital(String orderNo,String taskId,String processId,String signContent,String description) throws BaseException {
		boolean falg=false;      
		try{
        	String newOrderNo=WorkFlowStatus.CP_WorkFlow_Type+orderNo;
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
        	capitalPlan = this.iCapitalPlanBiz.getCapitalPlan(Long.parseLong(orderNo));
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			departments = iDepartmentsBiz.getDepartments(capitalPlan.getDeptId());
   			List<Order> orderList = new ArrayList<Order>();
   			if(StringUtil.isNotBlank(taskId)){
   				Map<String, Object> args = new HashMap<String, Object>();			
   				args.put("buyRemark", capitalPlan.getProjectContractName()+"--"+capitalPlan.getPayCode());//资金计划名称
   				args.put("result", "1");
   				args.put("signContent", signContent);//申请人意见
   				facets.executeBackToInit(taskId, user, args);   				
   				
   			}else{
   				if(StringUtil.isNotBlank(processId)){
   					Map<String, Object> args = new HashMap<String, Object>();
   					//动态读取流程节点处理人
   					wfProcessNode = new WfProcessNode();
   					wfProcessNode.setProcessId(processId);
   					List<WfProcessNode> wfpNodeList = this.iWfProcessNodeBiz.getWfProcessNodeList(wfProcessNode);
   					for(WfProcessNode wfpn:wfpNodeList){
   						if("initiator".equals(wfpn.getNodeCode())){//申请人
							args.put("initiator.operator", user);
						}else if("initDepLeader".equals(wfpn.getNodeCode())){//申请人部门经理
							args.put("initDepLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getDepLeaderId()));
						}else if("initCompLeader".equals(wfpn.getNodeCode())){//分管领导
							args.put("initCompLeader.operator", BaseDataInfosUtil.convertUserIdToEnnName(departments.getCompLeaderId()));
						}else {
							String str="";
							//if(StringUtil.isNotBlank(wfpn.getNodeUserName())){
   							if(wfpn.getUserType()==10){
   								str=wfpn.getNodeOperator();
							    args.put(wfpn.getNodeOperator(), wfpn.getNodeUserName());
   							}else if(wfpn.getUserType()==11){
   							   str=getNextUserNamesBy11(wfpn.getNodeUserId(), wfpn.getUserOrigin());
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==12){
   							   str=getNextUserNamesBy12(departments.getSelflevCode(),wfpn.getNodeUserId(), wfpn.getUserOrigin()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}else if(wfpn.getUserType()==13){
   								//衡水中通资金计划设置审批人单独处理
   							   str=getNextUserNamesBy13(capitalPlan.getApplicantDeptId(),wfpn.getWpnId(), capitalPlan.getComId()); 
   							   args.put(wfpn.getNodeOperator(), str);
   							}
   							if(StringUtil.isBlank(str)) falg=true;
   						}
   					}
   					if(!falg){
	   					args.put("snaker.orderNo", newOrderNo);
	   					args.put("result", "1");
	   					args.put("signContent", signContent);//申请人意见
	   					facets.startAndExecute(processId, user, args);
   					}
   					
   				}
   			}
   			if(!falg){
	             //更新资金计划表信息
	   			capitalPlan.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1);
	   			capitalPlan.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1_TEXT);
				this.iCapitalPlanBiz.updateCapitalPlan(capitalPlan);
				
				//保存流程审批正文
				this.saveWorkFlowText(newOrderNo, description,capitalPlan.getProjectContractName()+"--"+capitalPlan.getPayCode());
				
				//得到下一个审批人
				orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(newOrderNo));
				Map<String,Object> map=this.getTaskActors(orderList);
				if(map!=null){
					String actorsEn = (String)map.get("actorsEn");
					String[] acArr = actorsEn.split(",");
			        	for(String ac:acArr){
					    //发送邮件提醒领导审批
					    String subject = "待办提示：您有一个资金计划需要审批，资金计划编号为"+capitalPlan.getPayCode()+",资金计划金额为"+capitalPlan.getThisPaymentAmount()+"。";
		   			    String content = "待办信息：资金计划编号为"+capitalPlan.getPayCode()+"，资金计划金额为"+capitalPlan.getThisPaymentAmount()+
		   							 "，资金计划负责人为"+BaseDataInfosUtil.convertLoginNameToChnName(capitalPlan.getWriter())+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
			   			Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
						if(users!=null){
						   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
					    }
			        }
				}   		    
   			}
		} catch (Exception e) {
			log.error("保存资金计划审批提交流程错误！", e);
			throw new BaseException("保存资金计划审批提交流程错误！", e);
		}
		return falg;
	}
	/**
	 * 保存资金计划审批领导审批
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveApproveCapital(String taskId,String result,String signContent,String opAddTaskActor) throws BaseException {
         try{
        	String user = UserRightInfoUtil.getUserName(getRequest());
   			Task task = facets.getEngine().query().getTask(taskId);
        	String userChineseName = UserRightInfoUtil.getChineseName(getRequest());
			Order order = facets.getEngine().query().getOrder(task.getOrderId());
			String orderNo=order.getOrderNo().replace(WorkFlowStatus.CP_WorkFlow_Type, "");
   			capitalPlan = this.iCapitalPlanBiz.getCapitalPlan(Long.parseLong(orderNo));
   			Map<String, Object> args = new HashMap<String, Object>();
   			args.put("result", result);
   			args.put("signContent", signContent);
   			args.put("buyRemark", capitalPlan.getProjectContractName()+"--"+capitalPlan.getPayCode());//资金计划名称
   			if(result.equals("-1")){
   				List<Task> tkList = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(task.getOrderId()));
   				if(tkList!=null&&tkList.size()>1){
   					for(Task tk:tkList){
   						String[] tActors = facets.getEngine().query().getTaskActorsByTaskId(tk.getId());
   						if(!tActors[0].equals(UserRightInfoUtil.getUserName(getRequest()))){
   							facets.getEngine().task().complete(tk.getId(),tActors[0]);
   						}
   					}
   				}
   				facets.executeAndJumpReback(taskId,order.getId(), user, args, "start");

   				capitalPlan.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3);
   				capitalPlan.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3_TEXT);
				this.iCapitalPlanBiz.updateCapitalPlan(capitalPlan);
					
   				String subject = "待办提示：资金计划编号为【"+capitalPlan.getPayCode()+"】的流程被退回！";
   				String content = "资金计划审批未通过："+signContent+"";
   				
   				Users users=BaseDataInfosUtil.convertLoginNameToUsers(capitalPlan.getWriter());
				if(users!=null){
				   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
				   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
			    }
   				
   				
   			}else{
   				if(StringUtil.isNotBlank(opAddTaskActor)){
   					String[] ops = opAddTaskActor.split(",");
   					facets.getEngine().task().addTaskActor(taskId, PerformType.ALL.ordinal(), ops);
   				}
   				facets.execute(taskId, user, args);
   				
   				//更新资金计划审批数据表状态和审批时间
   				HistoryOrder hyOrder = facets.getEngine().query().getHistOrder(order.getId());
   				if(hyOrder.getOrderState()==0){
   					capitalPlan.setRunStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
   					capitalPlan.setStatusCn(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0_TEXT);
   					capitalPlan.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iCapitalPlanBiz.updateCapitalPlan(capitalPlan);
   					
						
   		   		    String content = "资金计划审批已完成！资金计划编号为"+capitalPlan.getPayCode()+"。";
   		       	    String subject = "待办提示：资金计划审批已完成。资金计划编号为【"+capitalPlan.getPayCode()+"】，资金计划金额为"+capitalPlan.getThisPaymentAmount();
	   				
   					
   					 Users users=BaseDataInfosUtil.convertLoginNameToUsers(capitalPlan.getWriter());
   					 if(users!=null){
	   					this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
						this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   				 }
   					
   				}else{
   					
   					List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(order.getOrderNo()));
 	 				Map<String,Object> map=this.getTaskActors(orderList);
 	 				if(map!=null){
 	 				String actorsEn = (String)map.get("actorsEn"); 					
 	 				
   					String content = "";
   					
 					String[] acArr = actorsEn.split(",");
 		        	for(String ac:acArr){
	   				       //发送邮件提醒领导审批
	   						String subject = "待办提示：您有一个资金计划需要审批，资金计划编号为"+capitalPlan.getPayCode()+",资金计划金额为"+capitalPlan.getThisPaymentAmount()+"。";
	   						content = "待办信息：资金计划编号为"+capitalPlan.getPayCode()+"，,资金计划金额为"+capitalPlan.getThisPaymentAmount()+"，"+
	   							 "，资金计划负责人为"+BaseDataInfosUtil.convertLoginNameToChnName(capitalPlan.getWriter())+"，资金计划审批【"+userChineseName+"】已通过："+signContent+"，待【"+BaseDataInfosUtil.convertLoginNameToChnName(ac)+"】处理！请领导审批！";
	   						
	   						Users users=BaseDataInfosUtil.convertLoginNameToUsers(ac);
	      					if(users!=null){
	   	   					   this.saveSendMailToUsers(users.getEmail(), subject, content,userChineseName);
	   						   this.saveSmsMessageToUsers(users.getPhoneNum(), subject, userChineseName);
	   	   				    }
	   			        }
	   		        }
   				}
   			}
		} catch (Exception e) {
			log.error("保存资金计划审批流程错误！", e);
			throw new BaseException("保存资金计划审批流程错误！", e);
		}		
	}
	/**
	 * 撤回待审批的资金计划流程
	 * @return
	 * @throws BaseException 
	 * @throws IOException 
	 * @Action
	 */           
	public String deleteCancelCapitalPlanProcess() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		try{
			this.ajaxHeadInit();
			if(StringUtil.isNotBlank(capitalPlan.getCpId())){
				capitalPlan = this.iCapitalPlanBiz.getCapitalPlan(capitalPlan.getCpId());
				String newOrderNo=WorkFlowStatus.CP_WorkFlow_Type+capitalPlan.getCpId();
                List<HistoryOrder> hyOrderList = facets.getEngine().query().getHistoryOrders(new QueryFilter().setOrderNo(newOrderNo));
				if(hyOrderList!=null&&hyOrderList.size()>0){
					List<HistoryTask> hytList = facets.getEngine().query().getHistoryTasks(new QueryFilter().setOrderId(hyOrderList.get(0).getId()).setOperator(hyOrderList.get(0).getCreator()));
					HistoryTask hyt=hytList.get(hytList.size()-1);
						if("start".equals(hyt.getParentTaskId())){
							facets.getEngine().task().withdrawTask(hyt.getId(), hyOrderList.get(0).getCreator());
							out.print("0");//撤销成功
							//删除所有之前的流程
							facets.getEngine().order().cascadeRemove(hyOrderList.get(0).getId());
							//删除流程正文
							wfText = new WfText();
							wfText.setOrderId(hyOrderList.get(0).getId());
							List<WfText> wtList = this.iWfTextBiz.getWfTextList(wfText);
							if(wtList!=null&&wtList.size()>0){
								for(WfText wt:wtList){
									this.iWfTextBiz.deleteWfText(wt);
								}
							}
							//更新流程状态
							capitalPlan.setRunStatus(OrderStatus.ORDER_RUN_STATUS_01);
							capitalPlan.setStatusCn(OrderStatus.ORDER_RUN_STATUS_01_Text);
							this.iCapitalPlanBiz.updateCapitalPlan(capitalPlan);
							
						}else{
							out.print("3");//流程已经处理，不能撤回
						}
				}else{
					out.print("2");//没有提交流程
				}
			}else{
				out.print("1");//流程编号不存在
			}
		} catch (Exception e) {
			out.print("3");//流程已经处理，不能撤回
			log.error("撤回待审批的资金计划流程错误！", e);
			throw new BaseException("撤回待审批的资金计划流程错误！", e);
		}
		return null ;		
	}
	/**
	 * 根据组织机构和岗位查询下一个审批人  用户来源为11
	 * @param deptId
	 * @param staton
	 * @throws BaseException
	 */
	public String getNextUserNamesBy11(Long deptId,String station) throws BaseException {
        String str="";   
		try{
        	   users=new Users();
        	   users.setDepId(deptId);
        	   users.setStation(station);
        	   List<Users> list=this.iUsersBiz.getUsersList(users);
        	   int i=0;
        	   for(Users users:list){
        		   i++;
        		   if(i==1){
        			   str=users.getUserName();  
        		   }else{
        			   str+=","+users.getUserName();  
        		   }
        	   }
		} catch (Exception e) {
			log.error("根据组织机构和岗位查询下一个审批人(用户来源为11)错误！", e);
			throw new BaseException("根据组织机构和岗位查询下一个审批人(用户来源为11)错误！", e);
		}
		return str;
	}
	/**
	 * 根据组织级别和岗位查询下一个审批人  用户来源为12
	 * @param deptId
	 * @param staton
	 * @throws BaseException
	 */
	public String getNextUserNamesBy12(String selflevCode,Long deptLevel,String station) throws BaseException {
        String str="";   
		try{    
			   departments=this.iDepartmentsBiz.getDepartmentsBySelflevCode(selflevCode, deptLevel);
			   if(departments!=null){
	        	   users=new Users();
	        	   users.setDepId(departments.getDepId());
	        	   users.setStation(station);
	        	   List<Users> list=this.iUsersBiz.getUsersList(users);
	        	   int i=0;
	        	   for(Users users:list){
	        		   i++;
	        		   if(i==1){
	        			   str=users.getUserName();  
	        		   }else{
	        			   str+=","+users.getUserName();  
	        		   }
	        	   }
			   }
		} catch (Exception e) {
			log.error("根据组织机构和岗位查询下一个审批人(用户来源为12)错误！", e);
			throw new BaseException("根据组织机构和岗位查询下一个审批人(用户来源为12)错误！", e);
		}
		return str;
	}
	/**
	 * 不同岗位管理不同组织获取下一个审批人  用户来源为13
	 * @param deptId
	 * @param wpnId
	 * @param comId
	 * @throws BaseException
	 */
	public String getNextUserNamesBy13(Long deptId,Long wpnId,Long comId) throws BaseException {
        String str="";   
		try{    
			wfProcessStationRights= new WfProcessStationRights(); 
			wfProcessStationRights.setWpnId(wpnId);
			wfProcessStationRights.setDeptId(","+deptId+",");
			List<WfProcessStationRights> wfProcessStationRightList=this.iWfProcessStationRightsBiz.getWfProcessStationRightsList(wfProcessStationRights);
			
			users=new Users();
			users.setComId(comId);
			List<Users> list=this.iUsersBiz.getUsersListByNotStation(users);
	        	   int i=0;
	        	   for(Users users:list){
	        		   for(WfProcessStationRights wfProcessStationRights:wfProcessStationRightList){
		        		   if(users.getStation().contains(wfProcessStationRights.getStation())){
			        		   i++;
			        		   if(i==1){
			        			   str=users.getUserName();  
			        		   }else{
			        			   str+=","+users.getUserName();  
			        		   }
		        		   }
	        		   }
	        	   }
		} catch (Exception e) {
			log.error("不同岗位管理不同组织获取下一个审批人(用户来源为13)错误！", e);
			throw new BaseException("不同岗位管理不同组织获取下一个审批人(用户来源为13)错误！", e);
		}
		return str;
	}
	public SnakerEngineFacets getFacets() {
		return facets;
	}

	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}

	public IRequiredMaterialBiz getiRequiredMaterialBiz() {
		return iRequiredMaterialBiz;
	}

	public void setiRequiredMaterialBiz(IRequiredMaterialBiz iRequiredMaterialBiz) {
		this.iRequiredMaterialBiz = iRequiredMaterialBiz;
	}

	public IWfProcessNodeBiz getiWfProcessNodeBiz() {
		return iWfProcessNodeBiz;
	}

	public void setiWfProcessNodeBiz(IWfProcessNodeBiz iWfProcessNodeBiz) {
		this.iWfProcessNodeBiz = iWfProcessNodeBiz;
	}

	public IWfTextBiz getiWfTextBiz() {
		return iWfTextBiz;
	}

	public void setiWfTextBiz(IWfTextBiz iWfTextBiz) {
		this.iWfTextBiz = iWfTextBiz;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IDepartmentsBiz getiDepartmentsBiz() {
		return iDepartmentsBiz;
	}

	public void setiDepartmentsBiz(IDepartmentsBiz iDepartmentsBiz) {
		this.iDepartmentsBiz = iDepartmentsBiz;
	}

	public ISysMessageLogBiz getiSysMessageLogBiz() {
		return iSysMessageLogBiz;
	}

	public void setiSysMessageLogBiz(ISysMessageLogBiz iSysMessageLogBiz) {
		this.iSysMessageLogBiz = iSysMessageLogBiz;
	}

	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}

	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}

	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}

	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}

	public RequiredMaterial getRequiredMaterial() {
		return requiredMaterial;
	}

	public void setRequiredMaterial(RequiredMaterial requiredMaterial) {
		this.requiredMaterial = requiredMaterial;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}

	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}

	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}

	public IContractMaterialBiz getiContractMaterialBiz() {
		return iContractMaterialBiz;
	}

	public void setiContractMaterialBiz(IContractMaterialBiz iContractMaterialBiz) {
		this.iContractMaterialBiz = iContractMaterialBiz;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}

	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}

	public IOrderMaterialBiz getiOrderMaterialBiz() {
		return iOrderMaterialBiz;
	}

	public void setiOrderMaterialBiz(IOrderMaterialBiz iOrderMaterialBiz) {
		this.iOrderMaterialBiz = iOrderMaterialBiz;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}

	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}

	public ICapitalPlanBiz getiCapitalPlanBiz() {
		return iCapitalPlanBiz;
	}

	public void setiCapitalPlanBiz(ICapitalPlanBiz iCapitalPlanBiz) {
		this.iCapitalPlanBiz = iCapitalPlanBiz;
	}

	public CapitalPlan getCapitalPlan() {
		return capitalPlan;
	}

	public void setCapitalPlan(CapitalPlan capitalPlan) {
		this.capitalPlan = capitalPlan;
	}

	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}

	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}

	public IWfProcessStationRightsBiz getiWfProcessStationRightsBiz() {
		return iWfProcessStationRightsBiz;
	}

	public void setiWfProcessStationRightsBiz(
			IWfProcessStationRightsBiz iWfProcessStationRightsBiz) {
		this.iWfProcessStationRightsBiz = iWfProcessStationRightsBiz;
	}	
	
}
