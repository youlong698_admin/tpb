package com.ced.sip.workflow.entity;


/**
 * WfTextId entity. @author MyEclipse Persistence Tools
 */

public class WfText implements java.io.Serializable {

	// Fields

	private Long wftid;
	private String orderId;
	private String wfText;
	private String wfName;

	// Constructors

	/** default constructor */
	public WfText() {
	}

	/** minimal constructor */
	public WfText(Long wftid) {
		this.wftid = wftid;
	}

	/** full constructor */
	public WfText(Long wftid, String orderId, String wfText) {
		this.wftid = wftid;
		this.orderId = orderId;
		this.wfText = wfText;
	}

	// Property accessors

	public Long getWftid() {
		return this.wftid;
	}

	public void setWftid(Long wftid) {
		this.wftid = wftid;
	}

	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getWfText() {
		return this.wfText;
	}

	public void setWfText(String wfText) {
		this.wfText = wfText;
	}

	public String getWfName() {
		return wfName;
	}

	public void setWfName(String wfName) {
		this.wfName = wfName;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof WfText))
			return false;
		WfText castOther = (WfText) other;

		return ((this.getWftid() == castOther.getWftid()) || (this.getWftid() != null
				&& castOther.getWftid() != null && this.getWftid().equals(
				castOther.getWftid())))
				&& ((this.getOrderId() == castOther.getOrderId()) || (this
						.getOrderId() != null
						&& castOther.getOrderId() != null && this.getOrderId()
						.equals(castOther.getOrderId())))
				&& ((this.getWfText() == castOther.getWfText()) || (this
						.getWfText() != null
						&& castOther.getWfText() != null && this.getWfText()
						.equals(castOther.getWfText())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getWftid() == null ? 0 : this.getWftid().hashCode());
		result = 37 * result
				+ (getOrderId() == null ? 0 : this.getOrderId().hashCode());
		result = 37 * result
				+ (getWfText() == null ? 0 : this.getWfText().hashCode());
		return result;
	}

}