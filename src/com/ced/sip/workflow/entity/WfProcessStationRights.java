package com.ced.sip.workflow.entity;

/** 
 * 类名称：WfProcessStationRights
 * 创建人：luguanglei 
 * 创建时间：2017-10-26
 */
public class WfProcessStationRights implements java.io.Serializable {

	// 属性信息
	private Long wpsrId;     //流程岗位权限ID
	private Long wpnId;     //流程节点分配人ID
	private String station;	 //岗位
	private String deptId;     //发起人部门ID
	private String deptName; //发起人部门
	
	
	public WfProcessStationRights() {
		super();
	}
	
	public Long getWpsrId(){
	   return  wpsrId;
	} 
	public void setWpsrId(Long wpsrId) {
	   this.wpsrId = wpsrId;
    }     
	public Long getWpnId(){
	   return  wpnId;
	} 
	public void setWpnId(Long wpnId) {
	   this.wpnId = wpnId;
    }     
	public String getStation(){
	   return  station;
	} 
	public void setStation(String station) {
	   this.station = station;
    }
	public String getDeptId(){
	   return  deptId;
	} 
	public void setDeptId(String deptId) {
	   this.deptId = deptId;
    }
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}     
}