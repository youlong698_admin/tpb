package com.ced.sip.workflow.entity;

/**
 * WfProcessNodeId entity. @author MyEclipse Persistence Tools
 */

public class WfProcessNode implements java.io.Serializable {

	// Fields

	private Long wpnId;
	private String processId;
	private String nodeTagName;
	private String nodeCode;
	private String nodeName;
	private String nodeOperator;
	private Long nodeUserId;
	private String nodeUserName;
	private String nodeUserNameCn;
	private String remark;
	private int userType;//用户类型
	private String userOrigin;//用户来源
	private int sort;

	// Constructors

	/** default constructor */
	public WfProcessNode() {
	}

	/** minimal constructor */
	public WfProcessNode(Long wpnId) {
		this.wpnId = wpnId;
	}

	/** full constructor */
	public WfProcessNode(Long wpnId, String processId, String nodeTagName,
			String nodeCode, String nodeName, String nodeOperator, Long nodeUserId,
			String nodeUserName, String remark) {
		this.wpnId = wpnId;
		this.processId = processId;
		this.nodeTagName = nodeTagName;
		this.nodeCode = nodeCode;
		this.nodeName = nodeName;
		this.nodeOperator = nodeOperator;
		this.nodeUserId = nodeUserId;
		this.nodeUserName = nodeUserName;
		this.remark = remark;
	}

	// Property accessors

	public Long getWpnId() {
		return this.wpnId;
	}

	public void setWpnId(Long wpnId) {
		this.wpnId = wpnId;
	}

	public String getProcessId() {
		return this.processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getNodeTagName() {
		return this.nodeTagName;
	}

	public void setNodeTagName(String nodeTagName) {
		this.nodeTagName = nodeTagName;
	}

	public String getNodeCode() {
		return this.nodeCode;
	}

	public void setNodeCode(String nodeCode) {
		this.nodeCode = nodeCode;
	}

	public String getNodeName() {
		return this.nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	
	public String getNodeOperator() {
		return nodeOperator;
	}

	public void setNodeOperator(String nodeOperator) {
		this.nodeOperator = nodeOperator;
	}

	public Long getNodeUserId() {
		return this.nodeUserId;
	}

	public void setNodeUserId(Long nodeUserId) {
		this.nodeUserId = nodeUserId;
	}

	public String getNodeUserName() {
		return this.nodeUserName;
	}

	public void setNodeUserName(String nodeUserName) {
		this.nodeUserName = nodeUserName;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNodeUserNameCn() {
		return nodeUserNameCn;
	}

	public void setNodeUserNameCn(String nodeUserNameCn) {
		this.nodeUserNameCn = nodeUserNameCn;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getUserOrigin() {
		return userOrigin;
	}

	public void setUserOrigin(String userOrigin) {
		this.userOrigin = userOrigin;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

}