package com.ced.sip.workflow.base.entity;

public class WFHistoryTask {
	 private String taskId;
	 private String taskName;
	 private String displayName;
	 private String taskType;//任务类型
	 private String performType;//参与类型
	 private String taskState;//任务状态
	 private String operator;//任务处理人
	 private String operatorCn;
	 private String orderId;
	 private String processId;
	 private String processDisplayName;
	 private Integer orderState;
	 private String orderStateName;
	 private String creator;
	 private String creatorCn;
	 private String createTime;
	 private String finishTime;
	 private String taskParentId;//父任务ID
	 private String expireTime;
	 private Integer priority;
	 private String orderNo;
	 private String variable;
	 private String actionUrl;
	 

	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public Integer getOrderState() {
		return orderState;
	}
	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}
	public String getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getVariable() {
		return variable;
	}
	public void setVariable(String variable) {
		this.variable = variable;
	}
	public String getOrderStateName() {
		return orderStateName;
	}
	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}
	public String getProcessDisplayName() {
		return processDisplayName;
	}
	public void setProcessDisplayName(String processDisplayName) {
		this.processDisplayName = processDisplayName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getActionUrl() {
		return actionUrl;
	}
	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getCreatorCn() {
		return creatorCn;
	}
	public void setCreatorCn(String creatorCn) {
		this.creatorCn = creatorCn;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getPerformType() {
		return performType;
	}
	public void setPerformType(String performType) {
		this.performType = performType;
	}
	public String getTaskState() {
		return taskState;
	}
	public void setTaskState(String taskState) {
		this.taskState = taskState;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperatorCn() {
		return operatorCn;
	}
	public void setOperatorCn(String operatorCn) {
		this.operatorCn = operatorCn;
	}
	public String getTaskParentId() {
		return taskParentId;
	}
	public void setTaskParentId(String taskParentId) {
		this.taskParentId = taskParentId;
	}	
	
	
}
