package com.ced.sip.workflow.base.entity;

import java.util.Map;

/**
 * WFOrder entity. @author MyEclipse Persistence Tools
 */

public class WFOrder implements java.io.Serializable {

	// Fields

	private String id;
	private String processId;
	private String creator;
	private String creatorCn;
	private String createTime;
	private String endTime;
	private String expireTime;
	private String lastUpdateTime;
	private String lastUpdator;
	private String lastUpdatorCn;
	private Boolean priority;
	private String parentNodeName;
	private String orderNo;
	private String variable;
	private Map variableMap;
	private Integer version;
	
	private String processDisplayName;
	private Integer orderState;
	private String orderStateName;
	private String actionUrl;
	private String taskId;
	private String deptName;//发起人部门
	private String taskTitle;//流程标题

	// Constructors

	/** default constructor */
	public WFOrder() {
	}

	/** minimal constructor */
	public WFOrder(String processId, String createTime) {
		this.processId = processId;
		this.createTime = createTime;
	}

	/** full constructor */
	public WFOrder(String processId, String creator,
			String createTime, String expireTime, String lastUpdateTime,
			String lastUpdator, Boolean priority, String parentNodeName,
			String orderNo, String variable, Integer version) {
		this.processId = processId;
		this.creator = creator;
		this.createTime = createTime;
		this.expireTime = expireTime;
		this.lastUpdateTime = lastUpdateTime;
		this.lastUpdator = lastUpdator;
		this.priority = priority;
		this.parentNodeName = parentNodeName;
		this.orderNo = orderNo;
		this.variable = variable;
		this.version = version;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessId() {
		return this.processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getExpireTime() {
		return this.expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

	public String getLastUpdateTime() {
		return this.lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getLastUpdator() {
		return this.lastUpdator;
	}

	public void setLastUpdator(String lastUpdator) {
		this.lastUpdator = lastUpdator;
	}

	public Boolean getPriority() {
		return this.priority;
	}

	public void setPriority(Boolean priority) {
		this.priority = priority;
	}

	public String getParentNodeName() {
		return this.parentNodeName;
	}

	public void setParentNodeName(String parentNodeName) {
		this.parentNodeName = parentNodeName;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getVariable() {
		return this.variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getProcessDisplayName() {
		return processDisplayName;
	}

	public void setProcessDisplayName(String processDisplayName) {
		this.processDisplayName = processDisplayName;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public String getOrderStateName() {
		return orderStateName;
	}

	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}

	public String getActionUrl() {
		return actionUrl;
	}

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCreatorCn() {
		return creatorCn;
	}

	public void setCreatorCn(String creatorCn) {
		this.creatorCn = creatorCn;
	}

	public String getLastUpdatorCn() {
		return lastUpdatorCn;
	}

	public void setLastUpdatorCn(String lastUpdatorCn) {
		this.lastUpdatorCn = lastUpdatorCn;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public Map getVariableMap() {
		return variableMap;
	}

	public void setVariableMap(Map variableMap) {
		this.variableMap = variableMap;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	
	
}