package com.ced.sip.workflow.base.entity;

public class WFSurrogate {
	
	 private String id;
	 private String processName;
	 private String processNameCn;
	 private String operator;
	 private String operatorCn;
	 private String surrogate;
	 private String surrogateCn;
	 private String odate;
	 private String sdate;
	 private String edate;
	 private Integer state;
	 private Long comId;
	 
	 
	 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperatorCn() {
		return operatorCn;
	}
	public void setOperatorCn(String operatorCn) {
		this.operatorCn = operatorCn;
	}
	public String getSurrogate() {
		return surrogate;
	}
	public void setSurrogate(String surrogate) {
		this.surrogate = surrogate;
	}
	public String getSurrogateCn() {
		return surrogateCn;
	}
	public void setSurrogateCn(String surrogateCn) {
		this.surrogateCn = surrogateCn;
	}
	public String getOdate() {
		return odate;
	}
	public void setOdate(String odate) {
		this.odate = odate;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getProcessNameCn() {
		return processNameCn;
	}
	public void setProcessNameCn(String processNameCn) {
		this.processNameCn = processNameCn;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	} 
	
}
