package com.ced.sip.workflow.base.helper;

import org.snaker.engine.model.TaskModel;

/**
 * @author yuqs
 * @since 1.0
 */
public class ExtTaskModel extends TaskModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -537606539410151969L;
	private String ccperson;
	public String getCcperson() {
		return ccperson;
	}
	public void setCcperson(String ccperson) {
		this.ccperson = ccperson;
	}
}
