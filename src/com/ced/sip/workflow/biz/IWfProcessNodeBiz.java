package com.ced.sip.workflow.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.workflow.entity.WfProcessNode;

public interface IWfProcessNodeBiz {

	/**
	 * 根据主键获得节点处理人表实例
	 * @param id 主键
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public abstract WfProcessNode getWfProcessNode(Long id)
			throws BaseException;

	/**
	 * 获得节点处理人表实例
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public abstract WfProcessNode getWfProcessNode(WfProcessNode wfProcessNode)
			throws BaseException;

	/**
	 * 添加节点处理人信息
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public abstract void saveWfProcessNode(WfProcessNode wfProcessNode)
			throws BaseException;

	/**
	 * 更新节点处理人表实例
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public abstract void updateWfProcessNode(WfProcessNode wfProcessNode)
			throws BaseException;

	/**
	 * 删除节点处理人表实例
	 * @param id 主键数组
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public abstract void deleteWfProcessNode(String id) throws BaseException;

	/**
	 * 删除节点处理人表实例
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public abstract void deleteWfProcessNode(WfProcessNode wfProcessNode)
			throws BaseException;

	/**
	 * 获得所有节点处理人表数据集
	 * @param rollPage 分页对象
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getWfProcessNodeList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有节点处理人表数据集
	 * @param wfProcessNode 查询参数对象
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getWfProcessNodeList(WfProcessNode wfProcessNode)
			throws BaseException;

	/**
	 * 获得所有节点处理人表数据集
	 * @param rollPage 分页对象
	 * @param wfProcessNode 查询参数对象
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getWfProcessNodeList(RollPage rollPage,
			WfProcessNode wfProcessNode) throws BaseException;

	/**
	 * 初始化流程审批信息表
	 * @throws BaseException
	 */
	public abstract void saveWorkFlowProcess(Long comId) throws BaseException;
}