package com.ced.sip.workflow.biz.impl;

import java.util.Date;
import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.workflow.biz.IWfProcessNodeBiz;
import com.ced.sip.workflow.entity.WfProcessNode;

public class WfProcessNodeBizImpl extends BaseBizImpl implements IWfProcessNodeBiz  {
	
	/**
	 * 根据主键获得节点处理人表实例
	 * @param id 主键
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public WfProcessNode getWfProcessNode(Long id) throws BaseException {
		return (WfProcessNode)this.getObject(WfProcessNode.class, id);
	}
	
	/**
	 * 获得节点处理人表实例
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public WfProcessNode getWfProcessNode( WfProcessNode wfProcessNode ) throws BaseException {
		return (WfProcessNode)this.getObject(WfProcessNode.class, wfProcessNode.getWpnId());
	}
	
	/**
	 * 添加节点处理人信息
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public void saveWfProcessNode(WfProcessNode wfProcessNode) throws BaseException{
		this.saveObject( wfProcessNode ) ;
	}
	
	/**
	 * 更新节点处理人表实例
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public void updateWfProcessNode(WfProcessNode wfProcessNode) throws BaseException{
		this.updateObject( wfProcessNode ) ;
	}
	
	/**
	 * 删除节点处理人表实例
	 * @param id 主键数组
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public void deleteWfProcessNode(String id) throws BaseException {
		this.removeObject( this.getWfProcessNode( new Long(id) ) ) ;
	}
	
	/**
	 * 删除节点处理人表实例
	 * @param wfProcessNode 节点处理人表实例
	 * @author zy 2016-12-25
	 * @throws BaseException 
	 */
	public void deleteWfProcessNode(WfProcessNode wfProcessNode) throws BaseException {
		this.removeObject( wfProcessNode ) ;
	}
	
	/**
	 * 获得所有节点处理人表数据集
	 * @param rollPage 分页对象
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public List getWfProcessNodeList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfProcessNode de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有节点处理人表数据集
	 * @param wfProcessNode 查询参数对象
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public List getWfProcessNodeList(  WfProcessNode wfProcessNode ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfProcessNode de where 1 = 1 " );
		if(wfProcessNode!=null){
			if(StringUtil.isNotBlank(wfProcessNode.getProcessId())){
				hql.append(" and de.processId='").append(wfProcessNode.getProcessId()).append("'");
			}
		}
		hql.append(" order by de.wpnId asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有节点处理人表数据集
	 * @param rollPage 分页对象
	 * @param wfProcessNode 查询参数对象
	 * @author zy 2016-12-25
	 * @return
	 * @throws BaseException 
	 */
	public List getWfProcessNodeList( RollPage rollPage, WfProcessNode wfProcessNode ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfProcessNode de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	
	public void saveWorkFlowProcess(Long comId) throws BaseException {
		String dateTime=DateUtil.getStringFromDate(new Date());
		String uuid=java.util.UUID.randomUUID().toString().replace("-", "");
		String sql="begin ";
		       //计划审批
		       sql+="insert into wf_process(id,name,display_name,type,instance_url,state,version,create_time,creator,com_id,content) " +
				" select '"+uuid+"',name,display_name,type,instance_url,state,version,'"+dateTime+"',creator,"+comId+",content from wf_process where id='b90f44293d83490cb1880344ac21a8eb';" +
						"insert into wf_process_node(wpn_id,process_id,node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort)" +
						" select SEQ_PROCESS_NODE_ID.nextval,'"+uuid+"',node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort from wf_process_node where process_id='b90f44293d83490cb1880344ac21a8eb';";
		     //项目立项审批
		       uuid=java.util.UUID.randomUUID().toString().replace("-", "");
		       sql+="insert into wf_process(id,name,display_name,type,instance_url,state,version,create_time,creator,com_id,content) " +
				" select '"+uuid+"',name,display_name,type,instance_url,state,version,'"+dateTime+"',creator,"+comId+",content from wf_process where id='a9b11af0b9234b4f8b28fb20ffe304f5';" +
						"insert into wf_process_node(wpn_id,process_id,node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort)" +
						" select SEQ_PROCESS_NODE_ID.nextval,'"+uuid+"',node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort from wf_process_node where process_id='a9b11af0b9234b4f8b28fb20ffe304f5';";
		     //授标审批 
		       uuid=java.util.UUID.randomUUID().toString().replace("-", "");
		       sql+="insert into wf_process(id,name,display_name,type,instance_url,state,version,create_time,creator,com_id,content) " +
				" select '"+uuid+"',name,display_name,type,instance_url,state,version,'"+dateTime+"',creator,"+comId+",content from wf_process where id='dfcd51b03a9d445aafc13dcc7a0f4823';" +
						"insert into wf_process_node(wpn_id,process_id,node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort)" +
						" select SEQ_PROCESS_NODE_ID.nextval,'"+uuid+"',node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort from wf_process_node where process_id='dfcd51b03a9d445aafc13dcc7a0f4823';";
		     //合同审批 
		       uuid=java.util.UUID.randomUUID().toString().replace("-", "");
		       sql+="insert into wf_process(id,name,display_name,type,instance_url,state,version,create_time,creator,com_id,content) " +
				" select '"+uuid+"',name,display_name,type,instance_url,state,version,'"+dateTime+"',creator,"+comId+",content from wf_process where id='18976293cd80417395b26e1a842f1b35';" +
						"insert into wf_process_node(wpn_id,process_id,node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort)" +
						" select SEQ_PROCESS_NODE_ID.nextval,'"+uuid+"',node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort from wf_process_node where process_id='18976293cd80417395b26e1a842f1b35';";
		     
		     //订单审批 
		       uuid=java.util.UUID.randomUUID().toString().replace("-", "");
		       sql+="insert into wf_process(id,name,display_name,type,instance_url,state,version,create_time,creator,com_id,content) " +
				" select '"+uuid+"',name,display_name,type,instance_url,state,version,'"+dateTime+"',creator,"+comId+",content from wf_process where id='eacf7d97d79944a2918866f58279be0d';" +
						"insert into wf_process_node(wpn_id,process_id,node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort)" +
						" select SEQ_PROCESS_NODE_ID.nextval,'"+uuid+"',node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort from wf_process_node where process_id='eacf7d97d79944a2918866f58279be0d';";
		     
		     //资金计划审批 
		       uuid=java.util.UUID.randomUUID().toString().replace("-", "");
		       sql+="insert into wf_process(id,name,display_name,type,instance_url,state,version,create_time,creator,com_id,content) " +
				" select '"+uuid+"',name,display_name,type,instance_url,state,version,'"+dateTime+"',creator,"+comId+",content from wf_process where id='a007db16b2a043fc8420385488317bb5';" +
						"insert into wf_process_node(wpn_id,process_id,node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort)" +
						" select SEQ_PROCESS_NODE_ID.nextval,'"+uuid+"',node_tag_name,node_code,node_name,node_operator,node_user_id,node_user_name,remark,sort from wf_process_node where process_id='a007db16b2a043fc8420385488317bb5';";
		       
		       sql+="end;";
		this.updateJdbcSql(sql);
		
	}
	
}
