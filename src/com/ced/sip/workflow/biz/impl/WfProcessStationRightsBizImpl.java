package com.ced.sip.workflow.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.workflow.biz.IWfProcessStationRightsBiz;
import com.ced.sip.workflow.entity.WfProcessStationRights;
/** 
 * 类名称：WfProcessStationRightsBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-10-26
 */
public class WfProcessStationRightsBizImpl extends BaseBizImpl implements IWfProcessStationRightsBiz  {
	
	/**
	 * 根据主键获得流程岗位权限表实例
	 * @param id 主键
	 * @author luguanglei 2017-10-26
	 * @return
	 * @throws BaseException 
	 */
	public WfProcessStationRights getWfProcessStationRights(Long id) throws BaseException {
		return (WfProcessStationRights)this.getObject(WfProcessStationRights.class, id);
	}
	
	/**
	 * 获得流程岗位权限表实例
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @author luguanglei 2017-10-26
	 * @return
	 * @throws BaseException 
	 */
	public WfProcessStationRights getWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException {
		return (WfProcessStationRights)this.getObject(WfProcessStationRights.class, wfProcessStationRights.getWpsrId() );
	}
	
	/**
	 * 添加流程岗位权限信息
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @author luguanglei 2017-10-26
	 * @throws BaseException 
	 */
	public void saveWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException{
		this.saveObject( wfProcessStationRights ) ;
	}
	
	/**
	 * 更新流程岗位权限表实例
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @author luguanglei 2017-10-26
	 * @throws BaseException 
	 */
	public void updateWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException{
		this.updateObject( wfProcessStationRights ) ;
	}
	
	/**
	 * 删除流程岗位权限表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-10-26
	 * @throws BaseException 
	 */
	public void deleteWfProcessStationRights(String id) throws BaseException {
		this.removeObject( this.getWfProcessStationRights( new Long(id) ) ) ;
	}
	
	/**
	 * 删除流程岗位权限表实例
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @author luguanglei 2017-10-26
	 * @throws BaseException 
	 */
	public void deleteWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException {
		this.removeObject( wfProcessStationRights ) ;
	}
	
	/**
	 * 删除流程岗位权限表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-10-26
	 * @throws BaseException 
	 */
	public void deleteWfProcessStationRightss(String[] id) throws BaseException {
		this.removeBatchObject(WfProcessStationRights.class, id) ;
	}
	
	/**
	 * 获得流程岗位权限表数据集
	 * wfProcessStationRights 流程岗位权限表实例
	 * @author luguanglei 2017-10-26
	 * @return
	 * @throws BaseException 
	 */
	public WfProcessStationRights getWfProcessStationRightsByWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfProcessStationRights de where 1 = 1 " );

		hql.append(" order by de.wpsrId desc ");
		List list = this.getObjects( hql.toString() );
		wfProcessStationRights = new WfProcessStationRights();
		if(list!=null&&list.size()>0){
			wfProcessStationRights = (WfProcessStationRights)list.get(0);
		}
		return wfProcessStationRights;
	}
	
	/**
	 * 获得所有流程岗位权限表数据集
	 * @param wfProcessStationRights 查询参数对象
	 * @author luguanglei 2017-10-26
	 * @return
	 * @throws BaseException 
	 */
	public List getWfProcessStationRightsList(WfProcessStationRights wfProcessStationRights) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfProcessStationRights de where 1 = 1 " );
		if(wfProcessStationRights!=null){
			if(StringUtil.isNotBlank(wfProcessStationRights.getWpnId())){
				hql.append(" and de.wpnId='").append(wfProcessStationRights.getWpnId()).append("'");
			}
			if(StringUtil.isNotBlank(wfProcessStationRights.getDeptId())){
				hql.append(" and de.deptId like '%").append(wfProcessStationRights.getDeptId()).append("%'");
			}
		}
		hql.append(" order by de.wpsrId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有流程岗位权限表数据集
	 * @param rollPage 分页对象
	 * @param wfProcessStationRights 查询参数对象
	 * @author luguanglei 2017-10-26
	 * @return
	 * @throws BaseException 
	 */
	public List getWfProcessStationRightsList(RollPage rollPage, WfProcessStationRights wfProcessStationRights) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfProcessStationRights de where 1 = 1 " );

		hql.append(" order by de.wpsrId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
