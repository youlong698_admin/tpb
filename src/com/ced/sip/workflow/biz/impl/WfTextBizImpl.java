package com.ced.sip.workflow.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.workflow.biz.IWfTextBiz;
import com.ced.sip.workflow.entity.WfText;

public class WfTextBizImpl extends BaseBizImpl implements IWfTextBiz  {
	
	/**
	 * 根据主键获得流程审批正文表实例
	 * @param id 主键
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public WfText getWfText(Long id) throws BaseException {
		return (WfText)this.getObject(WfText.class, id);
	}
	
	/**
	 * 获得流程审批正文表实例
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public WfText getWfText( WfText wfText ) throws BaseException {
		WfText wText=new WfText();
		StringBuffer hql = new StringBuffer(" from WfText de where 1 = 1 " );
		if(wfText!=null){
			if(StringUtil.isNotBlank(wfText.getOrderId())){
				hql.append(" and de.orderId ='").append(wfText.getOrderId()).append("'");
			}
		}
		hql.append(" order by de.id desc ");
		List list=this.getObjects( hql.toString() );
		if(list.size()>0) wText=(WfText)list.get(0);
		return wText;
	}
	
	/**
	 * 添加流程审批正文信息
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public void saveWfText(WfText wfText) throws BaseException{
		this.saveObject( wfText ) ;
	}
	
	/**
	 * 更新流程审批正文表实例
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public void updateWfText(WfText wfText) throws BaseException{
		this.updateObject( wfText ) ;
	}
	
	/**
	 * 删除流程审批正文表实例
	 * @param id 主键数组
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public void deleteWfText(String id) throws BaseException {
		this.removeObject( this.getWfText( new Long(id) ) ) ;
	}
	
	/**
	 * 删除流程审批正文表实例
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public void deleteWfText(WfText wfText) throws BaseException {
		this.removeObject( wfText ) ;
	}
	
	/**
	 * 获得所有流程审批正文表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public List getWfTextList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfText de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有流程审批正文表数据集
	 * @param wfText 查询参数对象
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public List getWfTextList(  WfText wfText ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfText de where 1 = 1 " );
		if(wfText!=null){
			if(StringUtil.isNotBlank(wfText.getOrderId())){
				hql.append(" and de.orderId ='").append(wfText.getOrderId()).append("'");
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有流程审批正文表数据集
	 * @param rollPage 分页对象
	 * @param wfText 查询参数对象
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public List getWfTextList( RollPage rollPage, WfText wfText ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from WfText de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
