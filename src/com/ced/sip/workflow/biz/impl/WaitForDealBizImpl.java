package com.ced.sip.workflow.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.workflow.biz.IWaitForDealBiz;
import com.ced.sip.workflow.entity.WaitWorkItem;
 
public class WaitForDealBizImpl extends BaseBizImpl implements IWaitForDealBiz  {

	
	public List getWaitForDealList(RollPage rollPage,Map<String,Object> map)
			throws BaseException {
		StringBuffer sql=new StringBuffer();
		sql.append(" select ho.order_state,o.process_Id, t.order_Id, t.id as id, t.id as task_Id, p.display_Name as process_Name, p.instance_Url, o.parent_Id, o.creator, ");
		sql.append(" o.create_Time as order_Create_Time, o.expire_Time as order_Expire_Time, o.order_No, ");
		sql.append(" t.display_Name as task_Name, t.task_Name as task_Key, t.task_Type, t.perform_Type, t.operator, t.action_Url, ");
		sql.append(" t.create_Time as task_Create_Time, t.finish_Time as task_End_Time, t.expire_Time as task_Expire_Time,o.last_updator, wtx.wf_name ");
		sql.append(" from wf_task t ");
		sql.append(" left join wf_order o on t.order_id = o.id ");
		sql.append(" left join wf_hist_order ho on t.order_id = ho.id ");
		if(StringUtil.isNotBlank(map.get("user"))){
			sql.append(" right join (select k.task_id from wf_task_actor k where k.actor_id='"+map.get("user")+"') ta on ta.task_id=t.id ");
		}else{
		    sql.append(" right join wf_task_actor ta on ta.task_id=t.id ");
		}
		sql.append(" left join wf_process p on p.id = o.process_id ");
		sql.append(" left join wf_text wtx on o.id = wtx.order_id ");
		sql.append(" where 1=1 ");
		if(StringUtil.isNotBlank(map.get("wfdName")))
		{
			sql.append(" and wtx.wf_name like '%"+map.get("wfdName")+"%'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeStart")))
		{
			sql.append(" and o.create_Time >= '"+map.get("createTimeStart")+"'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeEnd")))
		{
			sql.append(" and o.create_Time <= '"+map.get("createTimeEnd")+"'");
		}
		if(StringUtil.isNotBlank(map.get("wfType")))
		{
			sql.append(" and p.name = '"+map.get("wfType")+"'");
		}
		sql.append(" order by t.create_time desc");
		return this.getObjectsListRollPage(WaitWorkItem.class, rollPage, sql.toString());
	}
	/**
	 * 获得所有待办信息表总条数
	 * @param waitForDeal 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public int getWaitForDealSize(Map<String,Object> map)
		throws BaseException {
		StringBuffer sql=new StringBuffer();
		sql.append(" select o.process_Id, t.order_Id, t.id as id, t.id as task_Id, p.display_Name as process_Name, p.instance_Url, o.parent_Id, o.creator, ");
		sql.append(" o.create_Time as order_Create_Time, o.expire_Time as order_Expire_Time, o.order_No, ");
		sql.append(" t.display_Name as task_Name, t.task_Name as task_Key, t.task_Type, t.perform_Type, t.operator, t.action_Url, ");
		sql.append(" t.create_Time as task_Create_Time, t.finish_Time as task_End_Time, t.expire_Time as task_Expire_Time,o.last_updator, wtx.wf_name ");
		sql.append(" from wf_task t ");
		sql.append(" left join wf_order o on t.order_id = o.id ");
		if(StringUtil.isNotBlank(map.get("user"))){
			sql.append(" right join (select k.task_id from wf_task_actor k where k.actor_id='"+map.get("user")+"') ta on ta.task_id=t.id ");
		}else{
		    sql.append(" right join wf_task_actor ta on ta.task_id=t.id ");
		}
		sql.append(" left join wf_process p on p.id = o.process_id ");
		sql.append(" left join wf_text wtx on o.id = wtx.order_id ");
		sql.append(" where 1=1 ");
		if(StringUtil.isNotBlank(map.get("wfdName")))
		{
			sql.append(" and wtx.wf_name like '%"+map.get("wfdName")+"%'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeStart")))
		{
			sql.append(" and o.create_Time >= '"+map.get("createTimeStart")+"'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeEnd")))
		{
			sql.append(" and o.create_Time <= '"+map.get("createTimeEnd")+"'");
		}
		if(StringUtil.isNotBlank(map.get("wfType")))
		{
			sql.append(" and p.name = '"+map.get("wfType")+"'");
		}
		sql.append(" order by t.create_time desc");
		List list=this.getObjectsList(WaitWorkItem.class, sql.toString());
		
		return list.size();
	}

	
	public List getWaitAlreadyDoneList(RollPage rollPage,Map<String,Object> map) throws BaseException {
		StringBuffer sql=new StringBuffer();
		sql.append(" select o.process_Id, t.order_Id, t.id as id, t.id as task_Id, p.display_Name as process_Name, p.instance_Url, o.parent_Id, o.creator, ");
		sql.append(" o.create_Time as order_Create_Time, o.expire_Time as order_Expire_Time, o.order_No, ");
		sql.append(" t.display_Name as task_Name, t.task_Name as task_Key, t.task_Type, t.perform_Type,t.operator, t.action_Url, ");
		sql.append(" t.create_Time as task_Create_Time, t.finish_Time as task_End_Time, t.expire_Time as task_Expire_Time, t.variable as task_Variable, wtx.wf_name ");
		sql.append(" from wf_hist_task t ");
		sql.append(" left join wf_hist_order o on t.order_id = o.id ");
		if(StringUtil.isNotBlank(map.get("user"))){
			sql.append(" right join (select k.task_id from wf_hist_task_actor k where k.actor_id='"+map.get("user")+"') ta on ta.task_id=t.id ");
		}else{
		    sql.append(" right join wf_hist_task_actor ta on ta.task_id=t.id ");
		}
		sql.append(" left join wf_process p on p.id = o.process_id ");
		sql.append(" left join wf_text wtx on o.id = wtx.order_id ");
		sql.append(" where 1=1 ");
		if(StringUtil.isNotBlank(map.get("wfdName")))
		{
			sql.append(" and wtx.wf_name like '%"+map.get("wfdName")+"%'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeStart")))
		{
			sql.append(" and o.create_Time >= '"+map.get("createTimeStart")+"'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeEnd")))
		{
			sql.append(" and o.create_Time <= '"+map.get("createTimeEnd")+"'");
		}
		if(StringUtil.isNotBlank(map.get("wfType")))
		{
			sql.append(" and p.name = '"+map.get("wfType")+"'");
		}
		sql.append(" order by t.finish_Time desc");
		return this.getObjectsListRollPage(WaitWorkItem.class, rollPage, sql.toString());
	}

	/**
	 * 获得所有待办信息表总条数
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public int getWaitAlreadyDoneSize(Map<String,Object> map) throws BaseException {
		StringBuffer sql=new StringBuffer();
		sql.append(" select o.process_Id, t.order_Id, t.id as id, t.id as task_Id, p.display_Name as process_Name, p.instance_Url, o.parent_Id, o.creator, ");
		sql.append(" o.create_Time as order_Create_Time, o.expire_Time as order_Expire_Time, o.order_No, ");
		sql.append(" t.display_Name as task_Name, t.task_Name as task_Key, t.task_Type, t.perform_Type,t.operator, t.action_Url, ");
		sql.append(" t.create_Time as task_Create_Time, t.finish_Time as task_End_Time, t.expire_Time as task_Expire_Time, t.variable as task_Variable, wtx.wf_name ");
		sql.append(" from wf_hist_task t ");
		sql.append(" left join wf_hist_order o on t.order_id = o.id ");
		if(StringUtil.isNotBlank(map.get("user"))){
			sql.append(" right join (select k.task_id from wf_hist_task_actor k where k.actor_id='"+map.get("user")+"') ta on ta.task_id=t.id ");
		}else{
		    sql.append(" right join wf_hist_task_actor ta on ta.task_id=t.id ");
		}
		sql.append(" left join wf_process p on p.id = o.process_id ");
		sql.append(" left join wf_text wtx on o.id = wtx.order_id ");
		sql.append(" where 1=1 ");
		if(StringUtil.isNotBlank(map.get("wfdName")))
		{
			sql.append(" and wtx.wf_name like '%"+map.get("wfdName")+"%'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeStart")))
		{
			sql.append(" and o.create_Time >= '"+map.get("createTimeStart")+"'");
		}
		if(StringUtil.isNotBlank(map.get("createTimeEnd")))
		{
			sql.append(" and o.create_Time <= '"+map.get("createTimeEnd")+"'");
		}
		if(StringUtil.isNotBlank(map.get("wfType")))
		{
			sql.append(" and p.name = '"+map.get("wfType")+"'");
		}
		sql.append(" order by t.finish_Time desc");
		List list= this.getObjectsList(WaitWorkItem.class, sql.toString());
		return list.size();
	}

	
}
