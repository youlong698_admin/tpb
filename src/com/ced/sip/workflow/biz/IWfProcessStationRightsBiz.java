package com.ced.sip.workflow.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.workflow.entity.WfProcessStationRights;
/** 
 * 类名称：IWfProcessStationRightsBiz
 * 创建人：luguanglei 
 * 创建时间：2017-10-26
 */
public interface IWfProcessStationRightsBiz {

	/**
	 * 根据主键获得流程岗位权限表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract WfProcessStationRights getWfProcessStationRights(Long id) throws BaseException;

	/**
	 * 添加流程岗位权限信息
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @throws BaseException 
	 */
	abstract void saveWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException;

	/**
	 * 更新流程岗位权限表实例
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @throws BaseException 
	 */
	abstract void updateWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException;

	/**
	 * 删除流程岗位权限表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteWfProcessStationRights(String id) throws BaseException;

	/**
	 * 删除流程岗位权限表实例
	 * @param wfProcessStationRights 流程岗位权限表实例
	 * @throws BaseException 
	 */
	abstract void deleteWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException;

	/**
	 * 删除流程岗位权限表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteWfProcessStationRightss(String[] id) throws BaseException;

	/**
	 * 获得流程岗位权限表数据集
	 * wfProcessStationRights 流程岗位权限表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract WfProcessStationRights getWfProcessStationRightsByWfProcessStationRights(WfProcessStationRights wfProcessStationRights) throws BaseException ;
	
	/**
	 * 获得所有流程岗位权限表数据集
	 * @param wfProcessStationRights 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWfProcessStationRightsList(WfProcessStationRights wfProcessStationRights) throws BaseException ;
	
	/**
	 * 获得所有流程岗位权限表数据集
	 * @param rollPage 分页对象
	 * @param wfProcessStationRights 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWfProcessStationRightsList(RollPage rollPage, WfProcessStationRights wfProcessStationRights)
			throws BaseException;

}