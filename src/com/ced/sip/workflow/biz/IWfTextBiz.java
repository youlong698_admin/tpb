package com.ced.sip.workflow.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.workflow.entity.WfText;

public interface IWfTextBiz {

	/**
	 * 根据主键获得流程审批正文表实例
	 * @param id 主键
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract WfText getWfText(Long id) throws BaseException;

	/**
	 * 获得流程审批正文表实例
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract WfText getWfText(WfText wfText) throws BaseException;

	/**
	 * 添加流程审批正文信息
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public abstract void saveWfText(WfText wfText) throws BaseException;

	/**
	 * 更新流程审批正文表实例
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public abstract void updateWfText(WfText wfText) throws BaseException;

	/**
	 * 删除流程审批正文表实例
	 * @param id 主键数组
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public abstract void deleteWfText(String id) throws BaseException;

	/**
	 * 删除流程审批正文表实例
	 * @param wfText 流程审批正文表实例
	 * @author luguanglei 2016-12-20
	 * @throws BaseException 
	 */
	public abstract void deleteWfText(WfText wfText) throws BaseException;

	/**
	 * 获得所有流程审批正文表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getWfTextList(RollPage rollPage) throws BaseException;

	/**
	 * 获得所有流程审批正文表数据集
	 * @param wfText 查询参数对象
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getWfTextList(WfText wfText) throws BaseException;

	/**
	 * 获得所有流程审批正文表数据集
	 * @param rollPage 分页对象
	 * @param wfText 查询参数对象
	 * @author luguanglei 2016-12-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getWfTextList(RollPage rollPage, WfText wfText)
			throws BaseException;

}