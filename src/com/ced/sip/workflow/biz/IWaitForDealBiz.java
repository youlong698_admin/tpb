package com.ced.sip.workflow.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

public interface IWaitForDealBiz {

	
	/**
	 * 获得所有待办信息表数据集
	 * @param rollPage 分页对象
	 * @param waitForDeal 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getWaitForDealList(RollPage rollPage,Map<String,Object> map) throws BaseException; 
	
	/**
	 * 获得所有待办信息表总条数
	 * @param waitForDeal 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int getWaitForDealSize(Map<String,Object> map) throws BaseException; 
	
	
	/**
	 * 获得所有待办信息表数据集
	 * 代办全部信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getWaitAlreadyDoneList( RollPage rollPage,Map<String,Object> map) throws BaseException; 
	
	/**
	 * 获得所有待办信息表总条数
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public int getWaitAlreadyDoneSize(Map<String,Object> map) throws BaseException; 
	
}