package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * SupplierEvaluateSup entity. @author MyEclipse Persistence Tools
 */

public class SupplierEvaluateSup implements java.io.Serializable {

	// Fields

	private Long sesId;
	private Long seId;
	private Long supplierId;
	private String supplierName;
	private String contactPerson;
	private Double supSumPoints;
	private Long seuId;//考核人ID
	private Date seDate;
	private String status;
	private Long seuCount;//已提交考核数
	private Long comId;
	
	private String supAvgPoint;//供应商平均得分
	private String submitUserNum;//已提交考核人数量
	private String sumUserNum;//应提交考核人数量
	
	private String evaUsers;//考核人集合
	private String evaUserIds;//考核人集合
	private String evaUserNames;//考核人姓名集合
	private String seDescribe;


	// Constructors

	/** default constructor */
	public SupplierEvaluateSup() {
	}

	/** full constructor */
	public SupplierEvaluateSup(Long seId, Long supplierId, String supplierName,
			String contactPerson, Double supSumPoints) {
		this.seId = seId;
		this.supplierId = supplierId;
		this.supplierName = supplierName;
		this.contactPerson = contactPerson;
		this.supSumPoints = supSumPoints;
	}

	// Property accessors

	public Long getSesId() {
		return this.sesId;
	}

	public void setSesId(Long sesId) {
		this.sesId = sesId;
	}

	public Long getSeId() {
		return this.seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public Long getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Double getSupSumPoints() {
		return this.supSumPoints;
	}

	public void setSupSumPoints(Double supSumPoints) {
		this.supSumPoints = supSumPoints;
	}
	
	public String getSupAvgPoint() {
		return supAvgPoint;
	}

	public void setSupAvgPoint(String supAvgPoint) {
		this.supAvgPoint = supAvgPoint;
	}

	public String getSubmitUserNum() {
		return submitUserNum;
	}

	public void setSubmitUserNum(String submitUserNum) {
		this.submitUserNum = submitUserNum;
	}

	public String getSumUserNum() {
		return sumUserNum;
	}

	public void setSumUserNum(String sumUserNum) {
		this.sumUserNum = sumUserNum;
	}

	public Long getSeuId() {
		return seuId;
	}

	public void setSeuId(Long seuId) {
		this.seuId = seuId;
	}

	public String getEvaUsers() {
		return evaUsers;
	}

	public void setEvaUsers(String evaUsers) {
		this.evaUsers = evaUsers;
	}

	public String getEvaUserNames() {
		return evaUserNames;
	}

	public void setEvaUserNames(String evaUserNames) {
		this.evaUserNames = evaUserNames;
	}

	public Date getSeDate() {
		return seDate;
	}

	public void setSeDate(Date seDate) {
		this.seDate = seDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getSeuCount() {
		return seuCount;
	}

	public void setSeuCount(Long seuCount) {
		this.seuCount = seuCount;
	}

	public String getSeDescribe() {
		return seDescribe;
	}

	public void setSeDescribe(String seDescribe) {
		this.seDescribe = seDescribe;
	}

	public String getEvaUserIds() {
		return evaUserIds;
	}

	public void setEvaUserIds(String evaUserIds) {
		this.evaUserIds = evaUserIds;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}	
}