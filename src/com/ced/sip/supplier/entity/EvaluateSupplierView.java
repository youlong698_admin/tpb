package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * EvaluateSupplierViewId entity. @author MyEclipse Persistence Tools
 */

public class EvaluateSupplierView implements java.io.Serializable {

	// Fields

	private Long seId;
	private String seCode;
	private String seDescribe;
	private Long supNum;
	private Long userNum;
	private Date timeStart;
	private Date timeEnd;
	private String writer;
	private Date writeDate;
	private String status;
	private String isUsable;
	private String evastatus;
	private Long comId;
	
	private String orderId;//流程实例标示
	private String processId;//流程定义标示
	private String processName;//流程定义名称
	private String orderState;//流程状态
	private String orderStateName;//流程状态名称
	private String instanceUrl;//实例启动页面

	// Constructors

	/** default constructor */
	public EvaluateSupplierView() {
	}

	/** minimal constructor */
	public EvaluateSupplierView(Long seId) {
		this.seId = seId;
	}

	/** full constructor */
	public EvaluateSupplierView(Long seId, String seCode, String seDescribe,
			Long supNum, Long userNum, Date timeStart,
			Date timeEnd, String writer, Date writeDate, String status,
			String isUsable, String evastatus) {
		this.seId = seId;
		this.seCode = seCode;
		this.seDescribe = seDescribe;
		this.supNum = supNum;
		this.userNum = userNum;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.writer = writer;
		this.writeDate = writeDate;
		this.status = status;
		this.isUsable = isUsable;
		this.evastatus = evastatus;
	}

	// Property accessors

	public Long getSeId() {
		return this.seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public String getSeCode() {
		return this.seCode;
	}

	public void setSeCode(String seCode) {
		this.seCode = seCode;
	}

	public String getSeDescribe() {
		return this.seDescribe;
	}

	public void setSeDescribe(String seDescribe) {
		this.seDescribe = seDescribe;
	}

	public Long getSupNum() {
		return supNum;
	}

	public void setSupNum(Long supNum) {
		this.supNum = supNum;
	}

	public Long getUserNum() {
		return userNum;
	}

	public void setUserNum(Long userNum) {
		this.userNum = userNum;
	}

	public Date getTimeStart() {
		return this.timeStart;
	}

	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}

	public Date getTimeEnd() {
		return this.timeEnd;
	}

	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsUsable() {
		return this.isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getEvastatus() {
		return this.evastatus;
	}

	public void setEvastatus(String evastatus) {
		this.evastatus = evastatus;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getOrderStateName() {
		return orderStateName;
	}

	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof EvaluateSupplierView))
			return false;
		EvaluateSupplierView castOther = (EvaluateSupplierView) other;

		return ((this.getSeId() == castOther.getSeId()) || (this.getSeId() != null
				&& castOther.getSeId() != null && this.getSeId().equals(
				castOther.getSeId())))
				&& ((this.getSeCode() == castOther.getSeCode()) || (this
						.getSeCode() != null
						&& castOther.getSeCode() != null && this.getSeCode()
						.equals(castOther.getSeCode())))
				&& ((this.getSeDescribe() == castOther.getSeDescribe()) || (this
						.getSeDescribe() != null
						&& castOther.getSeDescribe() != null && this
						.getSeDescribe().equals(castOther.getSeDescribe())))
				&& ((this.getSupNum() == castOther.getSupNum()) || (this
						.getSupNum() != null
						&& castOther.getSupNum() != null && this.getSupNum()
						.equals(castOther.getSupNum())))
				&& ((this.getSupNum() == castOther.getSupNum()) || (this
						.getSupNum() != null
						&& castOther.getSupNum() != null && this.getSupNum()
						.equals(castOther.getSupNum())))
				&& ((this.getTimeStart() == castOther.getTimeStart()) || (this
						.getTimeStart() != null
						&& castOther.getTimeStart() != null && this
						.getTimeStart().equals(castOther.getTimeStart())))
				&& ((this.getTimeEnd() == castOther.getTimeEnd()) || (this
						.getTimeEnd() != null
						&& castOther.getTimeEnd() != null && this.getTimeEnd()
						.equals(castOther.getTimeEnd())))
				&& ((this.getWriter() == castOther.getWriter()) || (this
						.getWriter() != null
						&& castOther.getWriter() != null && this.getWriter()
						.equals(castOther.getWriter())))
				&& ((this.getWriteDate() == castOther.getWriteDate()) || (this
						.getWriteDate() != null
						&& castOther.getWriteDate() != null && this
						.getWriteDate().equals(castOther.getWriteDate())))
				&& ((this.getStatus() == castOther.getStatus()) || (this
						.getStatus() != null
						&& castOther.getStatus() != null && this.getStatus()
						.equals(castOther.getStatus())))
				&& ((this.getIsUsable() == castOther.getIsUsable()) || (this
						.getIsUsable() != null
						&& castOther.getIsUsable() != null && this
						.getIsUsable().equals(castOther.getIsUsable())))
				&& ((this.getEvastatus() == castOther.getEvastatus()) || (this
						.getEvastatus() != null
						&& castOther.getEvastatus() != null && this
						.getEvastatus().equals(castOther.getEvastatus())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getSeId() == null ? 0 : this.getSeId().hashCode());
		result = 37 * result
				+ (getSeCode() == null ? 0 : this.getSeCode().hashCode());
		result = 37
				* result
				+ (getSeDescribe() == null ? 0 : this.getSeDescribe()
						.hashCode());
		result = 37 * result
				+ (getSupNum() == null ? 0 : this.getSupNum().hashCode());
		result = 37 * result
				+ (getSupNum() == null ? 0 : this.getSupNum().hashCode());
		result = 37 * result
				+ (getTimeStart() == null ? 0 : this.getTimeStart().hashCode());
		result = 37 * result
				+ (getTimeEnd() == null ? 0 : this.getTimeEnd().hashCode());
		result = 37 * result
				+ (getWriter() == null ? 0 : this.getWriter().hashCode());
		result = 37 * result
				+ (getWriteDate() == null ? 0 : this.getWriteDate().hashCode());
		result = 37 * result
				+ (getStatus() == null ? 0 : this.getStatus().hashCode());
		result = 37 * result
				+ (getIsUsable() == null ? 0 : this.getIsUsable().hashCode());
		result = 37 * result
				+ (getEvastatus() == null ? 0 : this.getEvastatus().hashCode());
		return result;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	
}