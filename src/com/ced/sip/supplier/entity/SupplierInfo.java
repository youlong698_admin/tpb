package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * SupplierInfo entity. @author MyEclipse Persistence Tools
 */

public class SupplierInfo implements java.io.Serializable {

	// Fields

	private Long supplierId;
	private String supplierLoginName;
	private String supplierLoginPwd;
	private String supplierNameSimple;
	private String supplierName;
	private String supplierAddress;
	private String supplierPhone;
	private String supplierFax;
	private String legalPerson;
	private String contactPerson;
	private String mobilePhone;
	private String contactEmail;
	private String registerFunds;
	private Date createDate;
	private String supplierType;//供应商性质
	private String supplierTypeCn;
	private String orgCode;
	private String introduce;
	private String prodCodes;//产品类别组
	private String prodKindNames;//产品类别组中文
	private String scopeBusiness;//经营范围
	private Date registDate;
	private String isUsable;
	private String registrationType;//注册代码证类型
	private String deptName;//所在部门
	private String duty;//职务
	private String isRegister;
	private String isAudit;
	private String isChange;
	private String industryOwned; //所属行业
	private String managementModel;//经营模式
	private String province;//省
	private String city;//市
	private String companyWebsite;//企业网站
	private String logo;	 //LOGO
	private String bankAccount;//银行账号
	private String bankAccountName;//银行户名
	private String bank;//开户银行
	private String dutyParagraph;//税号
	
	//核心供应商
	private String status;
	private String statusCn;
	private String supplierLevel;
	
	private String registDateStart;
	private String registDateEnd;
	private String industryOwnedCn; //所属行业
	private String managementModelCn;//经营模式
	
	
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	public String getSupplierLoginName() {
		return supplierLoginName;
	}
	public void setSupplierLoginName(String supplierLoginName) {
		this.supplierLoginName = supplierLoginName;
	}
	public String getSupplierLoginPwd() {
		return supplierLoginPwd;
	}
	public void setSupplierLoginPwd(String supplierLoginPwd) {
		this.supplierLoginPwd = supplierLoginPwd;
	}
	public String getSupplierNameSimple() {
		return supplierNameSimple;
	}
	public void setSupplierNameSimple(String supplierNameSimple) {
		this.supplierNameSimple = supplierNameSimple;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	public String getSupplierPhone() {
		return supplierPhone;
	}
	public void setSupplierPhone(String supplierPhone) {
		this.supplierPhone = supplierPhone;
	}
	public String getLegalPerson() {
		return legalPerson;
	}
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}	
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getRegisterFunds() {
		return registerFunds;
	}
	public void setRegisterFunds(String registerFunds) {
		this.registerFunds = registerFunds;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getSupplierType() {
		return supplierType;
	}
	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}
	public String getSupplierTypeCn() {
		return supplierTypeCn;
	}
	public void setSupplierTypeCn(String supplierTypeCn) {
		this.supplierTypeCn = supplierTypeCn;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public Date getRegistDate() {
		return registDate;
	}
	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}
	public String getIsUsable() {
		return isUsable;
	}
	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getRegistDateStart() {
		return registDateStart;
	}
	public void setRegistDateStart(String registDateStart) {
		this.registDateStart = registDateStart;
	}
	public String getRegistDateEnd() {
		return registDateEnd;
	}
	public void setRegistDateEnd(String registDateEnd) {
		this.registDateEnd = registDateEnd;
	}
	public String getIsRegister() {
		return isRegister;
	}
	public void setIsRegister(String isRegister) {
		this.isRegister = isRegister;
	}
	public String getIsAudit() {
		return isAudit;
	}
	public void setIsAudit(String isAudit) {
		this.isAudit = isAudit;
	}
	public String getIsChange() {
		return isChange;
	}
	public void setIsChange(String isChange) {
		this.isChange = isChange;
	}
	public String getSupplierFax() {
		return supplierFax;
	}
	public void setSupplierFax(String supplierFax) {
		this.supplierFax = supplierFax;
	}
	public String getProdCodes() {
		return prodCodes;
	}
	public void setProdCodes(String prodCodes) {
		this.prodCodes = prodCodes;
	}
	public String getProdKindNames() {
		return prodKindNames;
	}
	public void setProdKindNames(String prodKindNames) {
		this.prodKindNames = prodKindNames;
	}
	public String getScopeBusiness() {
		return scopeBusiness;
	}
	public void setScopeBusiness(String scopeBusiness) {
		this.scopeBusiness = scopeBusiness;
	}
	public String getIndustryOwned() {
		return industryOwned;
	}
	public void setIndustryOwned(String industryOwned) {
		this.industryOwned = industryOwned;
	}
	public String getManagementModel() {
		return managementModel;
	}
	public void setManagementModel(String managementModel) {
		this.managementModel = managementModel;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getIndustryOwnedCn() {
		return industryOwnedCn;
	}
	public void setIndustryOwnedCn(String industryOwnedCn) {
		this.industryOwnedCn = industryOwnedCn;
	}
	public String getManagementModelCn() {
		return managementModelCn;
	}
	public void setManagementModelCn(String managementModelCn) {
		this.managementModelCn = managementModelCn;
	}
	public String getStatusCn() {
		return statusCn;
	}
	public void setStatusCn(String statusCn) {
		this.statusCn = statusCn;
	}
	public String getSupplierLevel() {
		return supplierLevel;
	}
	public void setSupplierLevel(String supplierLevel) {
		this.supplierLevel = supplierLevel;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getBankAccountName() {
		return bankAccountName;
	}
	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getDutyParagraph() {
		return dutyParagraph;
	}
	public void setDutyParagraph(String dutyParagraph) {
		this.dutyParagraph = dutyParagraph;
	} 
	
}