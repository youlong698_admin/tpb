package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * SupplierEvaluations entity. @author MyEclipse Persistence Tools
 */

public class SupplierEvaluations implements java.io.Serializable {

	// Fields

	private Long seId;
	private String seCode;
	private String seDescribe;
	private Date timeStart;
	private Date timeEnd;
	private String writer;
	private Date writeDate;
	private String status;
	private String seType;//考核类别
	private String isUsable;
    private String remark;
    private Long comId;
    
    private String supIds;
    private String supNames;
    private String userIds;
    private String userNames;
    private String indexIds;
    private String indexNames;

	// Constructors

	/** default constructor */
	public SupplierEvaluations() {
	}

	/** full constructor */
	public SupplierEvaluations(String seCode, String seDescribe,
			Date timeStart, Date timeEnd, String writer, Date writeDate,
			String status, String seType) {
		this.seCode = seCode;
		this.seDescribe = seDescribe;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.writer = writer;
		this.writeDate = writeDate;
		this.status = status;
		this.seType = seType;
	}

	// Property accessors

	public Long getSeId() {
		return this.seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public String getSeCode() {
		return this.seCode;
	}

	public void setSeCode(String seCode) {
		this.seCode = seCode;
	}

	public String getSeDescribe() {
		return this.seDescribe;
	}

	public void setSeDescribe(String seDescribe) {
		this.seDescribe = seDescribe;
	}

	public Date getTimeStart() {
		return this.timeStart;
	}

	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}

	public Date getTimeEnd() {
		return this.timeEnd;
	}

	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSeType() {
		return this.seType;
	}

	public void setSeType(String seType) {
		this.seType = seType;
	}

	public String getIsUsable() {
		return isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSupIds() {
		return supIds;
	}

	public void setSupIds(String supIds) {
		this.supIds = supIds;
	}

	public String getSupNames() {
		return supNames;
	}

	public void setSupNames(String supNames) {
		this.supNames = supNames;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getUserNames() {
		return userNames;
	}

	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}

	public String getIndexIds() {
		return indexIds;
	}

	public void setIndexIds(String indexIds) {
		this.indexIds = indexIds;
	}

	public String getIndexNames() {
		return indexNames;
	}

	public void setIndexNames(String indexNames) {
		this.indexNames = indexNames;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	
	
}