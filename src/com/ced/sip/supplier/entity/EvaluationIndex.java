package com.ced.sip.supplier.entity;

import java.util.Date;


/**
 * EvaluationIndex entity. @author MyEclipse Persistence Tools
 */

public class EvaluationIndex  implements java.io.Serializable {


    // Fields    

     private Long eiId;
     private String  eiIdString;
     private Long eikId;
     private String eikName;
     private String eiName;
     private String eiDescribe;
     private Double referencePoints;
     private String remark;
     private String writer;
     private Date writeDate;
     private String isUsable;
     private Double adjustedPoints;//调整后的分值
     private Long comId;
     

    // Constructors

    /** default constructor */
    public EvaluationIndex() {
    }

    
    /** full constructor */
    public EvaluationIndex(Long eikId, String eikName, String eiName, String eiDescribe, Double referencePoints, String remark, String writer, Date writeDate) {
        this.eikId = eikId;
        this.eikName = eikName;
        this.eiName = eiName;
        this.eiDescribe = eiDescribe;
        this.referencePoints = referencePoints;
        this.remark = remark;
        this.writer = writer;
        this.writeDate = writeDate;
    }

   
    // Property accessors

    public Long getEiId() {
        return this.eiId;
    }
    
    public void setEiId(Long eiId) {
        this.eiId = eiId;
    }

    public Long getEikId() {
        return this.eikId;
    }
    
    public void setEikId(Long eikId) {
        this.eikId = eikId;
    }

    public String getEikName() {
        return this.eikName;
    }
    
    public void setEikName(String eikName) {
        this.eikName = eikName;
    }

    public String getEiName() {
        return this.eiName;
    }
    
    public void setEiName(String eiName) {
        this.eiName = eiName;
    }

    public String getEiDescribe() {
        return this.eiDescribe;
    }
    
    public void setEiDescribe(String eiDescribe) {
        this.eiDescribe = eiDescribe;
    }

    public Double getReferencePoints() {
        return this.referencePoints;
    }
    
    public void setReferencePoints(Double referencePoints) {
        this.referencePoints = referencePoints;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getWriter() {
        return this.writer;
    }
    
    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }


	public String getIsUsable() {
		return isUsable;
	}


	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}


	public Double getAdjustedPoints() {
		return adjustedPoints;
	}


	public void setAdjustedPoints(Double adjustedPoints) {
		this.adjustedPoints = adjustedPoints;
	}


	public String getEiIdString() {
		return eiIdString;
	}


	public void setEiIdString(String eiIdString) {
		this.eiIdString = eiIdString;
	}


	public Long getComId() {
		return comId;
	}


	public void setComId(Long comId) {
		this.comId = comId;
	}
	

}