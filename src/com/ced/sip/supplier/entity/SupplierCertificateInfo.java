package com.ced.sip.supplier.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;


/**
 * SupplierCertificateInfo entity. @author MyEclipse Persistence Tools
 */

public class SupplierCertificateInfo extends BaseObject implements java.io.Serializable {


    // Fields    

     private Long sciId;
     private Long qcId;
     private Long supplierId;
     private String supplierName;
     private String certificateName;
     private String certificateCode;
     private Date timeBegain;
     private Date timeEnd;
     private String certificateProfession;
     private String writer;
     private Date writeDate;
     private String remark;
     private String isUsable;
     
     private String contactPerson;
     private String mobilePhone;
     private Integer csc;//总的资质文件数
     private Integer cscr;//到期的资质文件数


    // Constructors

    /** default constructor */
    public SupplierCertificateInfo() {
    }

    
    /** full constructor */
    public SupplierCertificateInfo(Long supplierId, String supplierName, String certificateName, String certificateCode, Date timeBegain, Date timeEnd, String certificateProfession, String writer, Date writeDate, String remark) {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.certificateName = certificateName;
        this.certificateCode = certificateCode;
        this.timeBegain = timeBegain;
        this.timeEnd = timeEnd;
        this.certificateProfession = certificateProfession;
        this.writer = writer;
        this.writeDate = writeDate;
        this.remark = remark;
    }

   
    // Property accessors

    public Long getSciId() {
        return this.sciId;
    }
    
    public void setSciId(Long sciId) {
        this.sciId = sciId;
    }

    public Long getSupplierId() {
        return this.supplierId;
    }
    
    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return this.supplierName;
    }
    
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getCertificateName() {
        return this.certificateName;
    }
    
    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getCertificateCode() {
        return this.certificateCode;
    }
    
    public void setCertificateCode(String certificateCode) {
        this.certificateCode = certificateCode;
    }

    public Date getTimeBegain() {
        return this.timeBegain;
    }
    
    public void setTimeBegain(Date timeBegain) {
        this.timeBegain = timeBegain;
    }

    public Date getTimeEnd() {
        return this.timeEnd;
    }
    
    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getCertificateProfession() {
        return this.certificateProfession;
    }
    
    public void setCertificateProfession(String certificateProfession) {
        this.certificateProfession = certificateProfession;
    }

    public String getWriter() {
        return this.writer;
    }
    
    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }


	public String getIsUsable() {
		return isUsable;
	}


	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}


	public Long getQcId() {
		return qcId;
	}


	public void setQcId(Long qcId) {
		this.qcId = qcId;
	}


	public Integer getCsc() {
		return csc;
	}


	public void setCsc(Integer csc) {
		this.csc = csc;
	}


	public Integer getCscr() {
		return cscr;
	}


	public void setCscr(Integer cscr) {
		this.cscr = cscr;
	}


	public String getContactPerson() {
		return contactPerson;
	}


	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}


	public String getMobilePhone() {
		return mobilePhone;
	}


	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
   

}