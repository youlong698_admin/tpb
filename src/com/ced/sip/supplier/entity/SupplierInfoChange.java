package com.ced.sip.supplier.entity;

import java.util.Date;

/** 
 * 类名称：SupplierInfoChange
 * 创建人：luguanglei 
 * 创建时间：2017-04-17
 */
public class SupplierInfoChange implements java.io.Serializable {

	// 属性信息
	private Long supplierId;     //供应商ID
	private String supplierNameSimple;	 //供应商简称
	private String supplierName;	 //供应商全称
	private String supplierAddress;	 //办公地址
	private String supplierPhone;	 //办公电话
	private String supplierFax;	 //办公电话
	private String legalPerson;	 //法定代表人
	private String registerFunds;	//注册资金
	private Date createDate;    //成立日期
	private Integer isChangeEffect;     //变更是否有效
	private Long sicId;     //主键
	
	
	public SupplierInfoChange() {
		super();
	}
	
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierNameSimple(){
	   return  supplierNameSimple;
	} 
	public void setSupplierNameSimple(String supplierNameSimple) {
	   this.supplierNameSimple = supplierNameSimple;
    }
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getSupplierAddress(){
	   return  supplierAddress;
	} 
	public void setSupplierAddress(String supplierAddress) {
	   this.supplierAddress = supplierAddress;
    }
	public String getSupplierPhone(){
	   return  supplierPhone;
	} 
	public void setSupplierPhone(String supplierPhone) {
	   this.supplierPhone = supplierPhone;
    }
	public String getLegalPerson(){
	   return  legalPerson;
	} 
	public void setLegalPerson(String legalPerson) {
	   this.legalPerson = legalPerson;
    }
	public String getRegisterFunds(){
	   return  registerFunds;
	} 
	public void setRegisterFunds(String registerFunds) {
	   this.registerFunds = registerFunds;
    }	
	public Date getCreateDate(){
	   return  createDate;
	} 
	public void setCreateDate(Date createDate) {
	   this.createDate = createDate;
    }	   
	public Integer getIsChangeEffect(){
	   return  isChangeEffect;
	} 
	public void setIsChangeEffect(Integer isChangeEffect) {
	   this.isChangeEffect = isChangeEffect;
    }     
	public Long getSicId(){
	   return  sicId;
	} 
	public void setSicId(Long sicId) {
	   this.sicId = sicId;
    }

	public String getSupplierFax() {
		return supplierFax;
	}

	public void setSupplierFax(String supplierFax) {
		this.supplierFax = supplierFax;
	}     
}