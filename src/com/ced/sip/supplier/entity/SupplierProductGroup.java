package com.ced.sip.supplier.entity;

/**
 * SupplierProductGroup entity. @author MyEclipse Persistence Tools
 */

public class SupplierProductGroup implements java.io.Serializable {

	// Fields

	private Long spgId;
	private Long supplierId;
	private Long pdId;
	private String pdCode;
	private String pdName;

	// Constructors

	/** default constructor */
	public SupplierProductGroup() {
	}

	/** full constructor */
	public SupplierProductGroup(Long supplierId, Long pdId, String pdCode,
			String pdName) {
		this.supplierId = supplierId;
		this.pdId = pdId;
		this.pdCode = pdCode;
		this.pdName = pdName;
	}

	// Property accessors

	public Long getSpgId() {
		return this.spgId;
	}

	public void setSpgId(Long spgId) {
		this.spgId = spgId;
	}

	public Long getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Long getPdId() {
		return this.pdId;
	}

	public void setPdId(Long pdId) {
		this.pdId = pdId;
	}

	public String getPdCode() {
		return this.pdCode;
	}

	public void setPdCode(String pdCode) {
		this.pdCode = pdCode;
	}

	public String getPdName() {
		return this.pdName;
	}

	public void setPdName(String pdName) {
		this.pdName = pdName;
	}

}