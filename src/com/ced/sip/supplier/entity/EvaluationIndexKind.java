package com.ced.sip.supplier.entity;

import java.util.Date;


/**
 * EvaluationIndexKind entity. @author MyEclipse Persistence Tools
 */

public class EvaluationIndexKind  implements java.io.Serializable {


    // Fields    

     private Long eikId;
     private String eikName;
     private Long displaySort;
     private Long parentId;
     private String isHaveChild;
     private Integer childCount;
     private Long materialCount;
     private String selflevCode;
     private Integer levels;
     private String writer;
     private Date writerDate;
     private String isUsable;
     private String remark;
     private Long comId;
     
     //操作类型
     private String operType;


    // Constructors

    /** default constructor */
    public EvaluationIndexKind() {
    }

    
    /** full constructor */
    public EvaluationIndexKind( String eikName, Long displaySort, Long parentId, String isHaveChild, Integer childCount, Long materialCount, String selflevCode, Integer levels, String writer, Date writerDate, String isUsable, String remark, String operType) {
        this.eikName = eikName;
        this.displaySort = displaySort;
        this.parentId = parentId;
        this.isHaveChild = isHaveChild;
        this.childCount = childCount;
        this.materialCount = materialCount;
        this.selflevCode = selflevCode;
        this.levels = levels;
        this.writer = writer;
        this.writerDate = writerDate;
        this.isUsable = isUsable;
        this.remark = remark;
        this.operType = operType;
    }

   
    // Property accessors

    public Long getEikId() {
        return this.eikId;
    }
    
    public void setEikId(Long eikId) {
        this.eikId = eikId;
    }

    public String getEikName() {
        return this.eikName;
    }
    
    public void setEikName(String eikName) {
        this.eikName = eikName;
    }

    public Long getDisplaySort() {
        return this.displaySort;
    }
    
    public void setDisplaySort(Long displaySort) {
        this.displaySort = displaySort;
    }

    public Long getParentId() {
        return this.parentId;
    }
    
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getIsHaveChild() {
        return this.isHaveChild;
    }
    
    public void setIsHaveChild(String isHaveChild) {
        this.isHaveChild = isHaveChild;
    }

    public Integer getChildCount() {
        return this.childCount;
    }
    
    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public Long getMaterialCount() {
        return this.materialCount;
    }
    
    public void setMaterialCount(Long materialCount) {
        this.materialCount = materialCount;
    }

    public String getSelflevCode() {
        return this.selflevCode;
    }
    
    public void setSelflevCode(String selflevCode) {
        this.selflevCode = selflevCode;
    }

    public Integer getLevels() {
        return this.levels;
    }
    
    public void setLevels(Integer levels) {
        this.levels = levels;
    }

    public String getWriter() {
        return this.writer;
    }
    
    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Date getWriterDate() {
        return this.writerDate;
    }
    
    public void setWriterDate(Date writerDate) {
        this.writerDate = writerDate;
    }

    public String getIsUsable() {
        return this.isUsable;
    }
    
    public void setIsUsable(String isUsable) {
        this.isUsable = isUsable;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }


	public String getOperType() {
		return operType;
	}


	public void setOperType(String operType) {
		this.operType = operType;
	}


	public Long getComId() {
		return comId;
	}


	public void setComId(Long comId) {
		this.comId = comId;
	}
   
}