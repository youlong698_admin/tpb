package com.ced.sip.supplier.entity;

import java.util.Date;



/**
 * SupplierProductInfo entity. @author MyEclipse Persistence Tools
 */

public class SupplierProductInfo implements java.io.Serializable {


    // Fields    

     private Long spiId;
     private Long supplierId;
     private String supplierName;
     private String productId;
     private String productDictionaryName;
     private String productName;
     private String productKind;
     private String productTechParameter;
     private String unit;
     private String productSales;
     private String salesPercent;
     private String status;
     private Date modifyDate;
     private String modifyPerson;
     private String writer;
     private Date writeDate;
     private String remark;
     private String isUsable;
     private Double price;
     private String description;
     private String img;


    // Constructors

    /** default constructor */
    public SupplierProductInfo() {
    }

    
    /** full constructor */
    public SupplierProductInfo(Long supplierId, String supplierName, String productId, String productName, String productKind, String productTechParameter, String unit, String productSales, String salesPercent, String status, Date modifyDate, String modifyPerson, String writer, Date writeDate, String remark) {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.productId = productId;
        this.productName = productName;
        this.productKind = productKind;
        this.productTechParameter = productTechParameter;
        this.unit = unit;
        this.productSales = productSales;
        this.salesPercent = salesPercent;
        this.status = status;
        this.modifyDate = modifyDate;
        this.modifyPerson = modifyPerson;
        this.writer = writer;
        this.writeDate = writeDate;
        this.remark = remark;
    }

   
    // Property accessors

    public Long getSpiId() {
        return this.spiId;
    }
    
    public void setSpiId(Long spiId) {
        this.spiId = spiId;
    }

    public Long getSupplierId() {
        return this.supplierId;
    }
    
    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return this.supplierName;
    }
    
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getProductId() {
        return this.productId;
    }
    
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return this.productName;
    }
    
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductKind() {
        return this.productKind;
    }
    
    public void setProductKind(String productKind) {
        this.productKind = productKind;
    }

    public String getProductTechParameter() {
        return this.productTechParameter;
    }
    
    public void setProductTechParameter(String productTechParameter) {
        this.productTechParameter = productTechParameter;
    }
    
    public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public String getProductSales() {
        return this.productSales;
    }
    
    public void setProductSales(String productSales) {
        this.productSales = productSales;
    }

    public String getSalesPercent() {
        return this.salesPercent;
    }
    
    public void setSalesPercent(String salesPercent) {
        this.salesPercent = salesPercent;
    }

    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public Date getModifyDate() {
        return this.modifyDate;
    }
    
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getModifyPerson() {
        return this.modifyPerson;
    }
    
    public void setModifyPerson(String modifyPerson) {
        this.modifyPerson = modifyPerson;
    }

    public String getWriter() {
        return this.writer;
    }
    
    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Date getWriteDate() {
        return this.writeDate;
    }
    
    public void setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }

	public String getIsUsable() {
		return isUsable;
	}


	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getProductDictionaryName() {
		return productDictionaryName;
	}


	public void setProductDictionaryName(String productDictionaryName) {
		this.productDictionaryName = productDictionaryName;
	}


	public String getImg() {
		return img;
	}


	public void setImg(String img) {
		this.img = img;
	}


}