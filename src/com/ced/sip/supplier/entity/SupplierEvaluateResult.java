package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * SupplierEvaluateResult entity. @author MyEclipse Persistence Tools
 */

public class SupplierEvaluateResult implements java.io.Serializable {

	// Fields

	private Long serId;
	private Long seiId;
	private Long seuId;
	private Long sesId;
	private Long seId;
	private Long eikId;
	private String eikName;
	private Long eiId;
	private String eiName;
	private String eiDescribe;
	private Double points;
	private Double adjustPoints;
	private Double judgePoint;
	private Date seDate;
	private String status;
	private String userName;
	private String supplierName;
	
	private Long supplierId;
	private String supIndexPoint;


	// Constructors

	/** default constructor */
	public SupplierEvaluateResult() {
	}

	/** full constructor */
	public SupplierEvaluateResult(Long seiId, Long seuId, Long sesId,
			Long seId, Long eikId, String eikName, Long eiId, String eiName,
			Double points, Double adjustPoints, Double judgePoint, Date seDate,String status) {
		this.seiId = seiId;
		this.seuId = seuId;
		this.sesId = sesId;
		this.seId = seId;
		this.eikId = eikId;
		this.eikName = eikName;
		this.eiId = eiId;
		this.eiName = eiName;
		this.points = points;
		this.adjustPoints = adjustPoints;
		this.judgePoint = judgePoint;
		this.seDate = seDate;
		this.status = status;
	}

	// Property accessors

	public Long getSerId() {
		return this.serId;
	}

	public void setSerId(Long serId) {
		this.serId = serId;
	}

	public Long getSeiId() {
		return this.seiId;
	}

	public void setSeiId(Long seiId) {
		this.seiId = seiId;
	}

	public Long getSeuId() {
		return this.seuId;
	}

	public void setSeuId(Long seuId) {
		this.seuId = seuId;
	}

	public Long getSesId() {
		return this.sesId;
	}

	public void setSesId(Long sesId) {
		this.sesId = sesId;
	}

	public Long getSeId() {
		return this.seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public Long getEikId() {
		return this.eikId;
	}

	public void setEikId(Long eikId) {
		this.eikId = eikId;
	}

	public String getEikName() {
		return this.eikName;
	}

	public void setEikName(String eikName) {
		this.eikName = eikName;
	}

	public Long getEiId() {
		return this.eiId;
	}

	public void setEiId(Long eiId) {
		this.eiId = eiId;
	}

	public String getEiName() {
		return this.eiName;
	}

	public void setEiName(String eiName) {
		this.eiName = eiName;
	}

	public Double getPoints() {
		return this.points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	public Double getAdjustPoints() {
		return this.adjustPoints;
	}

	public void setAdjustPoints(Double adjustPoints) {
		this.adjustPoints = adjustPoints;
	}

	public Double getJudgePoint() {
		return this.judgePoint;
	}

	public void setJudgePoint(Double judgePoint) {
		this.judgePoint = judgePoint;
	}

	public Date getSeDate() {
		return this.seDate;
	}

	public void setSeDate(Date seDate) {
		this.seDate = seDate;
	}

	public String getEiDescribe() {
		return eiDescribe;
	}

	public void setEiDescribe(String eiDescribe) {
		this.eiDescribe = eiDescribe;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public String getSupIndexPoint() {
		return supIndexPoint;
	}

	public void setSupIndexPoint(String supIndexPoint) {
		this.supIndexPoint = supIndexPoint;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	
}