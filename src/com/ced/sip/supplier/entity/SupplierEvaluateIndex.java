package com.ced.sip.supplier.entity;

/**
 * SupplierEvaluateIndex entity. @author MyEclipse Persistence Tools
 */

public class SupplierEvaluateIndex implements java.io.Serializable {

	// Fields

	private Long seiId;
	private Long seId;
	private Long eikId;
	private String eikName;
	private Long eiId;
	private String eiName;
	private String eiDescribe;
	private Double points;
	private Double adjustPoints;
	private String comId;

	// Constructors

	/** default constructor */
	public SupplierEvaluateIndex() {
	}

	/** full constructor */
	public SupplierEvaluateIndex(Long seId, Long eikId, String eikName,
			Long eiId, String eiName, Double points) {
		this.seId = seId;
		this.eikId = eikId;
		this.eikName = eikName;
		this.eiId = eiId;
		this.eiName = eiName;
		this.points = points;
	}

	// Property accessors

	public Long getSeiId() {
		return this.seiId;
	}

	public void setSeiId(Long seiId) {
		this.seiId = seiId;
	}

	public Long getSeId() {
		return this.seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public Long getEikId() {
		return this.eikId;
	}

	public void setEikId(Long eikId) {
		this.eikId = eikId;
	}

	public String getEikName() {
		return this.eikName;
	}

	public void setEikName(String eikName) {
		this.eikName = eikName;
	}

	public Long getEiId() {
		return this.eiId;
	}

	public void setEiId(Long eiId) {
		this.eiId = eiId;
	}

	public String getEiName() {
		return this.eiName;
	}

	public void setEiName(String eiName) {
		this.eiName = eiName;
	}

	public Double getPoints() {
		return this.points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	public Double getAdjustPoints() {
		return adjustPoints;
	}

	public void setAdjustPoints(Double adjustPoints) {
		this.adjustPoints = adjustPoints;
	}

	public String getEiDescribe() {
		return eiDescribe;
	}

	public void setEiDescribe(String eiDescribe) {
		this.eiDescribe = eiDescribe;
	}

	public String getComId() {
		return comId;
	}

	public void setComId(String comId) {
		this.comId = comId;
	}

}