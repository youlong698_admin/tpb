package com.ced.sip.supplier.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：SupplierStatusChange
 * 创建人：luguanglei 
 * 创建时间：2017-04-18
 */
public class SupplierStatusChange extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long sscId;     //主键
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String oldStatus;	 //变更前状态
	private String newStatus;	 //变更后状态
	private String reason;	 //变更原因
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark;	 //备注
	private Long comId;
	private Long sscomId;
	
	private String writerCn;
	private String oldStatusCn;
	private String newStatusCn;
	
	
	public SupplierStatusChange() {
		super();
	}
	
	public Long getSscId(){
	   return  sscId;
	} 
	public void setSscId(Long sscId) {
	   this.sscId = sscId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getOldStatus(){
	   return  oldStatus;
	} 
	public void setOldStatus(String oldStatus) {
	   this.oldStatus = oldStatus;
    }
	public String getNewStatus(){
	   return  newStatus;
	} 
	public void setNewStatus(String newStatus) {
	   this.newStatus = newStatus;
    }
	public String getReason(){
	   return  reason;
	} 
	public void setReason(String reason) {
	   this.reason = reason;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getWriterCn() {
		return writerCn;
	}

	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}

	public String getOldStatusCn() {
		return oldStatusCn;
	}

	public void setOldStatusCn(String oldStatusCn) {
		this.oldStatusCn = oldStatusCn;
	}

	public String getNewStatusCn() {
		return newStatusCn;
	}

	public void setNewStatusCn(String newStatusCn) {
		this.newStatusCn = newStatusCn;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	public Long getSscomId() {
		return sscomId;
	}

	public void setSscomId(Long sscomId) {
		this.sscomId = sscomId;
	}
	
}