package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * SupplierEvaluateUser entity. @author MyEclipse Persistence Tools
 */

public class SupplierEvaluateUser implements java.io.Serializable {

	// Fields

	private Long seuId;
	private Long seId;
	private Long userId;
	private String userName;
	private String userNameCn;
	private Long sedCount;
	private Long sesId;//被考核供应商ID
	private Long comId;
	
	private Long sumSupplier;
	private String seDate;//考核日期
	private String evaPoint;//考核分数
	
	private String evaSuppliers;//被考核供应商信息集合
	private String evaSupplierIds;//被考核供应商信息集合
	private String evaSupNames;//被考核供应商名称

	// Constructors

	/** default constructor */
	public SupplierEvaluateUser() {
	}

	/** full constructor */
	public SupplierEvaluateUser(Long seId, Long userId, String userName,
			Long sedCount) {
		this.seId = seId;
		this.userId = userId;
		this.userName = userName;
		this.sedCount = sedCount;
	}

	// Property accessors

	public Long getSeuId() {
		return this.seuId;
	}

	public void setSeuId(Long seuId) {
		this.seuId = seuId;
	}

	public Long getSeId() {
		return this.seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getSedCount() {
		return this.sedCount;
	}

	public void setSedCount(Long sedCount) {
		this.sedCount = sedCount;
	}

	public Long getSumSupplier() {
		return sumSupplier;
	}

	public void setSumSupplier(Long sumSupplier) {
		this.sumSupplier = sumSupplier;
	}

	public String getSeDate() {
		return seDate;
	}

	public void setSeDate(String seDate) {
		this.seDate = seDate;
	}

	public String getEvaPoint() {
		return evaPoint;
	}

	public void setEvaPoint(String evaPoint) {
		this.evaPoint = evaPoint;
	}

	public Long getSesId() {
		return sesId;
	}

	public void setSesId(Long sesId) {
		this.sesId = sesId;
	}

	public String getEvaSuppliers() {
		return evaSuppliers;
	}

	public void setEvaSuppliers(String evaSuppliers) {
		this.evaSuppliers = evaSuppliers;
	}

	public String getEvaSupNames() {
		return evaSupNames;
	}

	public void setEvaSupNames(String evaSupNames) {
		this.evaSupNames = evaSupNames;
	}

	public String getUserNameCn() {
		return userNameCn;
	}

	public void setUserNameCn(String userNameCn) {
		this.userNameCn = userNameCn;
	}

	public String getEvaSupplierIds() {
		return evaSupplierIds;
	}

	public void setEvaSupplierIds(String evaSupplierIds) {
		this.evaSupplierIds = evaSupplierIds;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}
}