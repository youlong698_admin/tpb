package com.ced.sip.supplier.entity;

import java.util.Date;

/**
 * SupplierEvaluationUserSup entity. @author MyEclipse Persistence Tools
 */

public class SupplierEvaluationUserSup implements java.io.Serializable {

	// Fields

	private Long usId;
	private Long seuId;
	private Long sesId;
	private String userNameCn;
	private Long supplierId;
	private String supplierName;
	private Long seId;
	private Double supPoints;
	private Date seDate;
	private String status;
	private String projectName;//项目名称
	private Long comId;

	// Constructors

	/** default constructor */
	public SupplierEvaluationUserSup() {
	}

	/** minimal constructor */
	public SupplierEvaluationUserSup(Long seuId) {
		this.seuId = seuId;
	}

	/** full constructor */
	public SupplierEvaluationUserSup(Long seuId, Long sesId) {
		this.seuId = seuId;
		this.sesId = sesId;
	}

	// Property accessors

	public Long getUsId() {
		return this.usId;
	}

	public void setUsId(Long usId) {
		this.usId = usId;
	}

	public Long getSeuId() {
		return this.seuId;
	}

	public void setSeuId(Long seuId) {
		this.seuId = seuId;
	}

	public Long getSesId() {
		return this.sesId;
	}

	public void setSesId(Long sesId) {
		this.sesId = sesId;
	}

	public String getUserNameCn() {
		return userNameCn;
	}

	public void setUserNameCn(String userNameCn) {
		this.userNameCn = userNameCn;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Long getSeId() {
		return seId;
	}

	public void setSeId(Long seId) {
		this.seId = seId;
	}

	public Double getSupPoints() {
		return supPoints;
	}

	public void setSupPoints(Double supPoints) {
		this.supPoints = supPoints;
	}

	public Date getSeDate() {
		return seDate;
	}

	public void setSeDate(Date seDate) {
		this.seDate = seDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

}