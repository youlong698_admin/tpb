package com.ced.sip.supplier.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：SupplierCertificateChange
 * 创建人：luguanglei 
 * 创建时间：2017-04-17
 */
public class SupplierCertificateChange extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long scicId;     //主键
	private Long sciId;     //资质主键
	private Long supplierId;     //供应商主键
	private String supplierName;	 //供应商名称
	private String certificateName;	 //资质名称
	private String certificateCode;	 //资质证书编号
	private Date timeBegain;    //有效期起
	private Date timeEnd;    //有效期止
	private String certificateProfession;	 //资质专业
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark;	 //备注
	private String isUsable;	 //是否有效
	private Long qcId;     //资质ID
	private Integer isChangeEffect;	 //变更是否有效
	
	
	public SupplierCertificateChange() {
		super();
	}
	
	public Long getScicId(){
	   return  scicId;
	} 
	public void setScicId(Long scicId) {
	   this.scicId = scicId;
    }     
	public Long getSciId(){
	   return  sciId;
	} 
	public void setSciId(Long sciId) {
	   this.sciId = sciId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getCertificateName(){
	   return  certificateName;
	} 
	public void setCertificateName(String certificateName) {
	   this.certificateName = certificateName;
    }
	public String getCertificateCode(){
	   return  certificateCode;
	} 
	public void setCertificateCode(String certificateCode) {
	   this.certificateCode = certificateCode;
    }
	public Date getTimeBegain(){
	   return  timeBegain;
	} 
	public void setTimeBegain(Date timeBegain) {
	   this.timeBegain = timeBegain;
    }	    
	public Date getTimeEnd(){
	   return  timeEnd;
	} 
	public void setTimeEnd(Date timeEnd) {
	   this.timeEnd = timeEnd;
    }	    
	public String getCertificateProfession(){
	   return  certificateProfession;
	} 
	public void setCertificateProfession(String certificateProfession) {
	   this.certificateProfession = certificateProfession;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    } 
	public Integer getIsChangeEffect(){
	   return  isChangeEffect;
	} 
	public void setIsChangeEffect(Integer isChangeEffect) {
	   this.isChangeEffect = isChangeEffect;
    }

	public Long getQcId() {
		return qcId;
	}

	public void setQcId(Long qcId) {
		this.qcId = qcId;
	}
	
}