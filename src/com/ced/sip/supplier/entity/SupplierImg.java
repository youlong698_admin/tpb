package com.ced.sip.supplier.entity;

/** 
 * 类名称：SupplierImg
 * 创建人：luguanglei 
 * 创建时间：2017-06-30
 */
public class SupplierImg implements java.io.Serializable {

	// 属性信息
	private Long siId;     //主键
	private Long spiId;     //产品ID
	private String img;	 //IMG
	
	
	public SupplierImg() {
		super();
	}
	
	public Long getSiId(){
	   return  siId;
	} 
	public void setSiId(Long siId) {
	   this.siId = siId;
    }     
	public Long getSpiId(){
	   return  spiId;
	} 
	public void setSpiId(Long spiId) {
	   this.spiId = spiId;
    }     
	public String getImg(){
	   return  img;
	} 
	public void setImg(String img) {
	   this.img = img;
    }
}