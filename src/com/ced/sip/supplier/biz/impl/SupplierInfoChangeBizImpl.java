package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.biz.ISupplierInfoChangeBiz;
import com.ced.sip.supplier.entity.SupplierInfoChange;
/** 
 * 类名称：SupplierInfoChangeBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-17
 */
public class SupplierInfoChangeBizImpl extends BaseBizImpl implements ISupplierInfoChangeBiz  {

	
	/**
	 * 添加供应商基本信息变更表信息
	 * @param supplierInfoChange 供应商基本信息变更表表实例
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void saveSupplierInfoChange(SupplierInfoChange supplierInfoChange) throws BaseException{
		this.saveObject( supplierInfoChange ) ;
	}
	/**
	 * 根据主键获得供应商基本信息变更表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public SupplierInfoChange getSupplierInfoChangeBySupplierId(Long supplierId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfoChange de where 1 = 1 " );
		hql.append(" and de.supplierId= "+supplierId+" and de.isChangeEffect=1 ");
		hql.append(" order by de.supplierId desc ");
		List list=this.getObjects(hql.toString());
		SupplierInfoChange supplierInfoChange=null;
		if(list.size()>0) supplierInfoChange=(SupplierInfoChange)list.get(0);
		return supplierInfoChange;
	}
	
	/**
	 * 更新供应商基本信息变更表表实例
	 * @param supplierInfoChange 供应商基本信息变更表表实例
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void updateSupplierInfoChange(SupplierInfoChange supplierInfoChange) throws BaseException{
		this.updateObject( supplierInfoChange ) ;
	}
	/**
	 * 获得所有供应商基本信息变更表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoChangeList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfoChange de where 1 = 1 " );

		hql.append(" order by de.supplierId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商基本信息变更表表数据集
	 * @param supplierInfoChange 查询参数对象
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoChangeList(SupplierInfoChange supplierInfoChange) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfoChange de where 1 = 1 " );

		hql.append(" order by de.supplierId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商基本信息变更表表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfoChange 查询参数对象
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoChangeList(RollPage rollPage, SupplierInfoChange supplierInfoChange) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfoChange de where 1 = 1 " );

		hql.append(" order by de.supplierId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
}
