package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierImgBiz;
import com.ced.sip.supplier.entity.SupplierImg;
/** 
 * 类名称：SupplierImgBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-06-30
 */
public class SupplierImgBizImpl extends BaseBizImpl implements ISupplierImgBiz  {
	
	/**
	 * 根据主键获得产品图片表实例
	 * @param id 主键
	 * @author luguanglei 2017-06-30
	 * @return
	 * @throws BaseException 
	 */
	public SupplierImg getSupplierImg(Long id) throws BaseException {
		return (SupplierImg)this.getObject(SupplierImg.class, id);
	}
	
	/**
	 * 获得产品图片表实例
	 * @param supplierImg 产品图片表实例
	 * @author luguanglei 2017-06-30
	 * @return
	 * @throws BaseException 
	 */
	public SupplierImg getSupplierImg(SupplierImg supplierImg) throws BaseException {
		return (SupplierImg)this.getObject(SupplierImg.class, supplierImg.getSiId() );
	}
	
	/**
	 * 添加产品图片信息
	 * @param supplierImg 产品图片表实例
	 * @author luguanglei 2017-06-30
	 * @throws BaseException 
	 */
	public void saveSupplierImg(SupplierImg supplierImg) throws BaseException{
		this.saveObject( supplierImg ) ;
	}
	
	/**
	 * 更新产品图片表实例
	 * @param supplierImg 产品图片表实例
	 * @author luguanglei 2017-06-30
	 * @throws BaseException 
	 */
	public void updateSupplierImg(SupplierImg supplierImg) throws BaseException{
		this.updateObject( supplierImg ) ;
	}
	
	/**
	 * 删除产品图片表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-06-30
	 * @throws BaseException 
	 */
	public void deleteSupplierImg(String id) throws BaseException {
		this.removeObject( this.getSupplierImg( new Long(id) ) ) ;
	}
	
	/**
	 * 删除产品图片表实例
	 * @param supplierImg 产品图片表实例
	 * @author luguanglei 2017-06-30
	 * @throws BaseException 
	 */
	public void deleteSupplierImg(SupplierImg supplierImg) throws BaseException {
		this.removeObject( supplierImg ) ;
	}
	
	/**
	 * 删除产品图片表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-06-30
	 * @throws BaseException 
	 */
	public void deleteSupplierImgs(String[] id) throws BaseException {
		this.removeBatchObject(SupplierImg.class, id) ;
	}
	
	/**
	 * 获得产品图片表数据集
	 * supplierImg 产品图片表实例
	 * @author luguanglei 2017-06-30
	 * @return
	 * @throws BaseException 
	 */
	public SupplierImg getSupplierImgBySupplierImg(SupplierImg supplierImg) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierImg de where 1 = 1 " );

		hql.append(" order by de.siId desc ");
		List list = this.getObjects( hql.toString() );
		supplierImg = new SupplierImg();
		if(list!=null&&list.size()>0){
			supplierImg = (SupplierImg)list.get(0);
		}
		return supplierImg;
	}
	
	/**
	 * 获得所有产品图片表数据集
	 * @param supplierImg 查询参数对象
	 * @author luguanglei 2017-06-30
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierImgList(SupplierImg supplierImg) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierImg de where 1 = 1 " );
		if(supplierImg!=null){
			if(StringUtil.isNotBlank(supplierImg.getSpiId())){
				hql.append(" and de.spiId = ").append(supplierImg.getSpiId());
			}
		}
		hql.append(" order by de.siId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有产品图片表数据集
	 * @param rollPage 分页对象
	 * @param supplierImg 查询参数对象
	 * @author luguanglei 2017-06-30
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierImgList(RollPage rollPage, SupplierImg supplierImg) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierImg de where 1 = 1 " );

		hql.append(" order by de.siId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
