package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.IEvaluationIndexBiz;
import com.ced.sip.supplier.entity.EvaluationIndex;

public class EvaluationIndexBizImpl extends BaseBizImpl implements IEvaluationIndexBiz  {
	
	/**
	 * 根据主键获得考核指标信息表实例
	 * @param id 主键
	 * @author zhuyu 2014-08-27
	 * @return
	 * @throws BaseException 
	 */
	public EvaluationIndex getEvaluationIndex(Long id) throws BaseException {
		return (EvaluationIndex)this.getObject(EvaluationIndex.class, id);
	}
	
	/**
	 * 获得考核指标信息表实例
	 * @param evaluationIndex 考核指标信息表实例
	 * @author zhuyu 2014-08-27
	 * @return
	 * @throws BaseException 
	 */
	public EvaluationIndex getEvaluationIndex( EvaluationIndex evaluationIndex ) throws BaseException {
		return (EvaluationIndex)this.getObject(EvaluationIndex.class, evaluationIndex.getEiId() );
	}
	
	/**
	 * 添加考核指标信息信息
	 * @param evaluationIndex 考核指标信息表实例
	 * @author zhuyu 2014-08-27
	 * @throws BaseException 
	 */
	public void saveEvaluationIndex(EvaluationIndex evaluationIndex) throws BaseException{
		this.saveObject( evaluationIndex ) ;
	}
	
	/**
	 * 更新考核指标信息表实例
	 * @param evaluationIndex 考核指标信息表实例
	 * @author zhuyu 2014-08-27
	 * @throws BaseException 
	 */
	public void updateEvaluationIndex(EvaluationIndex evaluationIndex) throws BaseException{
		this.updateObject( evaluationIndex ) ;
	}
	
	/**
	 * 删除考核指标信息表实例
	 * @param id 主键数组
	 * @author zhuyu 2014-08-27
	 * @throws BaseException 
	 */
	public void deleteEvaluationIndex(String id) throws BaseException {
		this.removeObject( this.getEvaluationIndex( new Long(id) ) ) ;
	}
	
	/**
	 * 删除考核指标信息表实例
	 * @param evaluationIndex 考核指标信息表实例
	 * @author zhuyu 2014-08-27
	 * @throws BaseException 
	 */
	public void deleteEvaluationIndex(EvaluationIndex evaluationIndex) throws BaseException {
		this.removeObject( evaluationIndex ) ;
	}
	
	/**
	 * 删除考核指标信息表实例
	 * @param id 主键数组
	 * @author zhuyu 2014-08-27
	 * @throws BaseException 
	 */
	public void deleteEvaluationIndexs(String[] id) throws BaseException {
		this.removeBatchObject(EvaluationIndex.class, id) ;
	}
	
	/**
	 * 获得所有考核指标信息表数据集
	 * @param rollPage 分页对象
	 * @author zhuyu 2014-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluationIndex de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有考核指标信息表数据集
	 * @param evaluationIndex 查询参数对象
	 * @author zhuyu 2014-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexList(  EvaluationIndex evaluationIndex ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluationIndex de where 1 = 1 " );
		if(evaluationIndex != null){
			if(!StringUtil.isBlank(evaluationIndex.getEikId())){
				hql.append(" and de.eikId =").append(evaluationIndex.getEikId());
			}
			if(!StringUtil.isBlank(evaluationIndex.getEiName())){
				hql.append(" and de.eiName like '%").append(evaluationIndex.getEiName()).append("%'");
			}
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有考核指标信息表数据集
	 * @param rollPage 分页对象
	 * @param evaluationIndex 查询参数对象
	 * @author zhuyu 2014-08-27
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexList( RollPage rollPage, EvaluationIndex evaluationIndex ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluationIndex de where 1 = 1 " );
		if(!StringUtil.isBlank(evaluationIndex)){
			if(!StringUtil.isBlank(evaluationIndex.getEiName())){
				hql.append(" and de.eiName like '%").append(evaluationIndex.getEiName()).append("%'");
			}
			if(!StringUtil.isBlank(evaluationIndex.getEikId())){
				hql.append(" and de.eikId =").append(evaluationIndex.getEikId());
			}
			if(!StringUtil.isBlank(evaluationIndex.getComId())){
				hql.append(" and de.comId =").append(evaluationIndex.getComId());
			}
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
