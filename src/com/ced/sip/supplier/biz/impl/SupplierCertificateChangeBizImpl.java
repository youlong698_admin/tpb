package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierCertificateChangeBiz;
import com.ced.sip.supplier.entity.SupplierCertificateChange;
/** 
 * 类名称：SupplierCertificateChangeBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-17
 */
public class SupplierCertificateChangeBizImpl extends BaseBizImpl implements ISupplierCertificateChangeBiz  {
	
	/**
	 * 根据主键获得供应商资质信息变更表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public SupplierCertificateChange getSupplierCertificateChange(Long id) throws BaseException {
		return (SupplierCertificateChange)this.getObject(SupplierCertificateChange.class, id);
	}
	
	/**
	 * 获得供应商资质信息变更表表实例
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public SupplierCertificateChange getSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException {
		return (SupplierCertificateChange)this.getObject(SupplierCertificateChange.class, supplierCertificateChange.getScicId() );
	}
	
	/**
	 * 添加供应商资质信息变更表信息
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void saveSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException{
		this.saveObject( supplierCertificateChange ) ;
	}
	
	/**
	 * 更新供应商资质信息变更表表实例
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void updateSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException{
		this.updateObject( supplierCertificateChange ) ;
	}
	
	/**
	 * 删除供应商资质信息变更表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void deleteSupplierCertificateChange(String id) throws BaseException {
		this.removeObject( this.getSupplierCertificateChange( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商资质信息变更表表实例
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void deleteSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException {
		this.removeObject( supplierCertificateChange ) ;
	}
	
	/**
	 * 删除供应商资质信息变更表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-17
	 * @throws BaseException 
	 */
	public void deleteSupplierCertificateChanges(String[] id) throws BaseException {
		this.removeBatchObject(SupplierCertificateChange.class, id) ;
	}
	
	/**
	 * 获得所有供应商资质信息变更表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateChangeList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateChange de where 1 = 1 " );

		hql.append(" order by de.scicId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商资质信息变更表表数据集
	 * @param supplierCertificateChange 查询参数对象
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateChangeList(SupplierCertificateChange supplierCertificateChange) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateChange de where 1 = 1 " );

		hql.append(" order by de.scicId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商资质信息变更表表数据集
	 * @param rollPage 分页对象
	 * @param supplierCertificateChange 查询参数对象
	 * @author luguanglei 2017-04-17
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateChangeList(RollPage rollPage, SupplierCertificateChange supplierCertificateChange) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateChange de where 1 = 1 " );
        if(StringUtil.isNotBlank(supplierCertificateChange)){
			
			if(StringUtil.isNotBlank(supplierCertificateChange.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierCertificateChange.getSupplierName()).append("%'");
			}
			
			if(StringUtil.isNotBlank(supplierCertificateChange.getCertificateName())){
				hql.append(" and de.certificateName like '%").append(supplierCertificateChange.getCertificateName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierCertificateChange.getTimeBegain())){
				hql.append(" and trunc(de.timeBegain,'dd') = to_date('").append( DateUtil.getDefaultDateFormat(supplierCertificateChange.getTimeBegain())).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(supplierCertificateChange.getSupplierId())){
				hql.append(" and de.supplierId =").append( supplierCertificateChange.getSupplierId());
			}
		}
		hql.append(" and de.isChangeEffect = 1");
		if(StringUtil.isNotBlank(rollPage.getOrderColumn())) hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
