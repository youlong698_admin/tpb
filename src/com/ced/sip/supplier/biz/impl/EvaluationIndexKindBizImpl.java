package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.IEvaluationIndexKindBiz;
import com.ced.sip.supplier.entity.EvaluationIndexKind;

public class EvaluationIndexKindBizImpl extends BaseBizImpl implements IEvaluationIndexKindBiz  {
	
	/**
	 * 根据主键获得考核指标类别树表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public EvaluationIndexKind getEvaluationIndexKind(Long id) throws BaseException {
		return (EvaluationIndexKind)this.getObject(EvaluationIndexKind.class, id);
	}
	
	/**
	 * 获得考核指标类别树表实例
	 * @param evaluationIndexKind 考核指标类别树表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public EvaluationIndexKind getEvaluationIndexKind( EvaluationIndexKind evaluationIndexKind ) throws BaseException {
		return (EvaluationIndexKind)this.getObject(EvaluationIndexKind.class, evaluationIndexKind.getEikId() );
	}
	
	/**
	 * 添加考核指标类别树信息
	 * @param evaluationIndexKind 考核指标类别树表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveEvaluationIndexKind(EvaluationIndexKind evaluationIndexKind) throws BaseException{
		this.saveObject( evaluationIndexKind ) ;
	}
	
	/**
	 * 更新考核指标类别树表实例
	 * @param evaluationIndexKind 考核指标类别树表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateEvaluationIndexKind(EvaluationIndexKind evaluationIndexKind) throws BaseException{
		this.updateObject( evaluationIndexKind ) ;
	}
	
	/**
	 * 删除考核指标类别树表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteEvaluationIndexKind(String id) throws BaseException {
		this.removeObject( this.getEvaluationIndexKind( new Long(id) ) ) ;
	}
	
	/**
	 * 删除考核指标类别树表实例
	 * @param evaluationIndexKind 考核指标类别树表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteEvaluationIndexKind(EvaluationIndexKind evaluationIndexKind) throws BaseException {
		this.removeObject( evaluationIndexKind ) ;
	}
	
	/**
	 * 删除考核指标类别树表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteEvaluationIndexKinds(String[] id) throws BaseException {
		this.removeBatchObject(EvaluationIndexKind.class, id) ;
	}
	
	/**
	 * 获得所有考核指标类别树表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexKindList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluationIndexKind de where 1 = 1 " );

		hql.append(" and de.isUsable = '0' order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有考核指标类别树表数据集
	 * @param evaluationIndexKind 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexKindList(  EvaluationIndexKind evaluationIndexKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluationIndexKind de where 1 = 1 " );
		if(StringUtil.isNotBlank(evaluationIndexKind)){
			if(StringUtil.isNotBlank(evaluationIndexKind.getEikId())){
				hql.append(" and de.eikId = '").append(evaluationIndexKind.getEikId()).append("'");
				
			}
			if(StringUtil.isNotBlank(evaluationIndexKind.getParentId())){
				hql.append(" and de.parentId = '").append(evaluationIndexKind.getParentId()).append("'");
			}
			if(StringUtil.isNotBlank(evaluationIndexKind.getComId())){
				hql.append(" and de.comId = '").append(evaluationIndexKind.getComId()).append("'");
			}
		}
		hql.append(" and de.isUsable = '0' order by de.displaySort ASC ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有考核指标类别树表数据集
	 * @param rollPage 分页对象
	 * @param EvaluationIndexKind 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexKindList( RollPage rollPage, EvaluationIndexKind EvaluationIndexKind ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluationIndexKind de where 1 = 1 " );

		hql.append(" and de.isUsable = '0' order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得考核指标类别个数
	 * @param evaluationIndexKind 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public int getCountEvaluationIndexList(EvaluationIndexKind evaluationIndexKind ) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(*) from EvaluationIndexKind de where 1 = 1 " );
		if( evaluationIndexKind != null ) {
			// 父类ID
			if( evaluationIndexKind.getParentId() != null ) {
				hql.append(" and de.parentId = " ).append( evaluationIndexKind.getParentId() ) ;
			}
		}
		hql.append( " and de.isUsable = '" ).append( TableStatus.COMMON_STATUS_VALID ).append("' ") ;
		return this.countObjects(hql.toString());
	}
	
	
	/**
	 *  获得指定供应商考核指标库信息表数据集
	 * @param EvaluationIndexKind 查询参数对象
	 * @author xcb 2014-10-26
	 * @return
	 * @throws BaseException 
	 */
	public List getEvaluationIndexKindCode(String materialType,String mkCodeLength) throws BaseException {
		StringBuffer hql = new StringBuffer(" select max(de.mkCode) from EvaluationIndexKind de where 1 = 1 " );
			hql.append(" and de.mkCode like '%").append(materialType).append("%'");
			hql.append(" and length(de.mkCode) = ").append(mkCodeLength);
			return this.getObjects(hql.toString() );
		}
	
		
}
