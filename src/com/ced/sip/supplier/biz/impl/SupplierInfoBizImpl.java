package com.ced.sip.supplier.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductGroup;

public class SupplierInfoBizImpl extends BaseBizImpl implements ISupplierInfoBiz  {
	
	/**
	 * 根据主键获得供应商基本信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierInfo getSupplierInfo(Long id) throws BaseException {
		return (SupplierInfo)this.getObject(SupplierInfo.class, id);
	}
	
	/**
	 * 获得供应商基本信息表实例
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierInfo getSupplierInfo( SupplierInfo supplierInfo ) throws BaseException {
		return (SupplierInfo)this.getObject(SupplierInfo.class, supplierInfo.getSupplierId() );
	}
	
	/**
	 * 添加供应商基本信息信息
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierInfo(SupplierInfo supplierInfo) throws BaseException{
		this.saveObject( supplierInfo ) ;
	}
	
	/**
	 * 更新供应商基本信息表实例
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierInfo(SupplierInfo supplierInfo) throws BaseException{
		this.updateObject( supplierInfo ) ;
	}
	
	/**
	 * 删除供应商基本信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierInfo(String id) throws BaseException {
		this.removeObject( this.getSupplierInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商基本信息表实例
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierInfo(SupplierInfo supplierInfo) throws BaseException {
		this.removeObject( supplierInfo ) ;
	}
	
	/**
	 * 删除供应商基本信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierInfos(String[] id) throws BaseException {
		this.removeBatchObject(SupplierInfo.class, id) ;
	}
	
	public SupplierInfo getSupplierInfoByName( String supplierName  ) throws BaseException {
		SupplierInfo supp = null;
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierName)){
			hql.append(" and de.supplierName like'%").append(supplierName.trim()).append("%'");
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by de.id desc ");
		List suppList = this.getObjects( hql.toString() );
		if(suppList.size()>0){
			supp = (SupplierInfo) suppList.get(0);
		}
		return supp;
	}
	
	/**
	 * 获得所有供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
    /**
	 * 获得所有供应商基本信息表数据集
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoList(  SupplierInfo supplierInfo ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank((supplierInfo))){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getContactPerson())){
				hql.append(" and de.contactPerson like '%").append(supplierInfo.getContactPerson()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getSupplierId())){
				hql.append(" and de.supplierId != '").append(supplierInfo.getSupplierId()).append("'");
			}
			
			if(StringUtil.isNotBlank(supplierInfo.getRegistDateStart())){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append( supplierInfo.getRegistDateStart()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getRegistDateEnd())){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append( supplierInfo.getRegistDateEnd()).append("'");
			}
			
		}
		hql.append(" and de.status='0' ");
		hql.append(" and de.isAudit = '0'");
		hql.append(" order by de.supplierId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得核心供应商基本信息表数据集
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoListCompany(Map<String,Object> map,Long comId) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,ssc.status,ssc.supplierLevel,ssc.sscId from SupplierInfo de,SupplierSysCompany ssc where  de.supplierId=ssc.supplierId " );
		if(StringUtil.isNotBlank(map)){
			if(StringUtil.isNotBlank(map.get("supplierName"))){
				hql.append(" and de.supplierName like '%").append(map.get("de.supplierName")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("contactPerson"))){
				hql.append(" and de.contactPerson like '%").append(map.get("contactPerson")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("status"))){
				hql.append(" and ssc.status='").append(map.get("status")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("supplierLevel"))){
				hql.append(" and ssc.supplierLevel='").append(map.get("supplierLevel")).append("'");
			}
		}
		if(StringUtil.isNotBlank(comId)){
			 hql.append(" and ssc.comId="+comId );
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by de.supplierId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商基本信息表数据集   所有供应商 包含各种状态
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoListAll(RollPage rollPage,SupplierInfo supplierInfo ) throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("de.supplierId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank((supplierInfo))){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getContactPerson())){
				hql.append(" and de.contactPerson like '%").append(supplierInfo.getContactPerson()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getSupplierId())){
				hql.append(" and de.supplierId != '").append(supplierInfo.getSupplierId()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getRegistDateStart())){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append( supplierInfo.getRegistDateStart()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getRegistDateEnd())){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append( supplierInfo.getRegistDateEnd()).append("'");
			}
			
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage,hql.toString() );
	}
	/**
	 * 获得所有供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoList( RollPage rollPage, SupplierInfo supplierInfo ) throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("de.supplierId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierInfo)){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getContactPerson())){
				hql.append(" and de.contactPerson like '%").append(supplierInfo.getContactPerson()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getLogo())){
				hql.append(" and de.logo is not null ");
			}
			
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoListAudit( RollPage rollPage, SupplierInfo supplierInfo,String sign) throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("de.supplierId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierInfo)){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getContactPerson())){
				hql.append(" and de.contactPerson like '%").append(supplierInfo.getContactPerson()).append("%'");
			}
		}
		if(StringUtil.isNotBlank(sign)){
			if(sign.equals("1")) hql.append(" and de.isAudit='1'  ");
			if(sign.equals("2")) hql.append(" and de.isChange = '1'  ");
		}else{
			hql.append(" and de.isAudit='0'  ");
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有供应商基本信息表数据集总数
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public int countSupplierInfoListAudit(Map<String,Object> mapParams,String sign) throws BaseException {		
		StringBuffer hql = new StringBuffer("select count(de.supplierId) from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.registDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.registDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		if(StringUtil.isNotBlank(sign)){
			if(sign.equals("1")) hql.append(" and de.isAudit='1'  ");
			if(sign.equals("2")) hql.append(" and de.isChange = '1'  ");
		}else{
			hql.append(" and de.isAudit='0'  ");
		}
		hql.append(" and de.isUsable = '0'");
		return this.countObjects(hql.toString() );
	}
	/**
	 * 获得核心供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoListCompany( RollPage rollPage,Map<String,Object> map,Long comId) throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("de.supplierId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		
		StringBuffer hql = new StringBuffer("select de,ssc.status,ssc.supplierLevel,ssc.sscId from SupplierInfo de,SupplierSysCompany ssc where  de.supplierId=ssc.supplierId " );
		if(StringUtil.isNotBlank(map)){
			if(StringUtil.isNotBlank(map.get("supplierName"))){
				hql.append(" and de.supplierName like '%").append(map.get("supplierName")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("contactPerson"))){
				hql.append(" and de.contactPerson like '%").append(map.get("contactPerson")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("status"))){
				hql.append(" and ssc.status='").append(map.get("status")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("supplierLevel"))){
				hql.append(" and ssc.supplierLevel='").append(map.get("supplierLevel")).append("'");
			}
		}
		if(StringUtil.isNotBlank(comId)){
			 hql.append(" and ssc.comId="+comId );
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得盘外供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoListOutCompany( RollPage rollPage, SupplierInfo supplierInfo,Long comId) throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("de.supplierId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		
		StringBuffer hql = new StringBuffer("select de from SupplierInfo de where not exists (select ssc.supplierId  from SupplierSysCompany ssc where de.supplierId=ssc.supplierId and  ssc.comId="+comId+") " );
       	if(StringUtil.isNotBlank(supplierInfo)){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getContactPerson())){
				hql.append(" and de.contactPerson like '%").append(supplierInfo.getContactPerson()).append("%'");
			}
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得序列的下一个值
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public String getSupplierUserName(String seqName) throws BaseException {
		return this.getSequenceNextValue(seqName);
	}
	/**
	 * 根据rcId查询类别中的供应商
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	public List getSuppListByRcId(Long rcId,SupplierInfo supplierInfo) throws BaseException {
		StringBuffer hql = new StringBuffer("select distinct su from " );
		hql.append(" RequiredCollectDetail rcd,MaterialKind mk, SupplierInfo su, SupplierProductGroup spg where 1 = 1 ");
		hql.append(" and substr(rcd.buyCode,0,2) = mk.mkCode ");
		hql.append(" and mk.mkId = spg.pdId");
		hql.append(" and spg.supplierId = su.supplierId");
		hql.append(" and rcd.rcId='").append(rcId).append("'");
		if(StringUtil.isNotBlank(supplierInfo)){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				hql.append(" and su.supplierName like '%").append(supplierInfo.getSupplierName()).append("%'");
			}			
		}
		hql.append(" order by su.supplierLevel");
		
		return this.getObjects( hql.toString() );
	}

	/**
     * 根据列名、结果、供应商id更新供应商信息表，主要是审核用。 注册审核、变更审核
     * @param column
     * @param result
     * @param supplierId
     * @throws BaseException
     */
	public void updateSupplierInfoByColumn(String column, String result,
			Long supplierId) throws BaseException {
		String sql="update supplier_info set "+column+"="+result+" where supplier_id="+supplierId;
		this.updateJdbcSql(sql);
		
	}

	/**
	 * 根据用户名和密码查询供应商列表
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  List getSupplierInfoListByValidate(SupplierInfo supplierInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank((supplierInfo))){
			if(StringUtil.isNotBlank(supplierInfo.getSupplierLoginName())){
				hql.append(" and de.supplierLoginName ='").append(supplierInfo.getSupplierLoginName()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getSupplierLoginPwd())){
				hql.append(" and de.supplierLoginPwd = '").append(supplierInfo.getSupplierLoginPwd()).append("'");
			}
			
		}
		return this.getObjects( hql.toString() );
	}
	/**
	 * 添加供应商产品类别组信息
	 * @param SupplierProductGroup 供应商产品类别组表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierProductGroup(SupplierProductGroup supplierProductGroup) throws BaseException{
		this.saveObject( supplierProductGroup ) ;
	}
	/**
	 * 获得所有供应商产品类别组表数据集
	 * @param supplierProductGroup 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierProductGroupList(  SupplierProductGroup supplierProductGroup ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierProductGroup de where 1 = 1 " );
		if(supplierProductGroup!=null){
			if(StringUtil.isNotBlank(supplierProductGroup.getSupplierId())){
				hql.append(" and de.supplierId = ").append(supplierProductGroup.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierProductGroup.getPdId())){
				hql.append(" and de.pdId = ").append(supplierProductGroup.getPdId());
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 删除供应商产品类别组表实例
	 * @param supplierProductGroup 供应商产品类别组表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierProductGroup(SupplierProductGroup supplierProductGroup) throws BaseException {
		this.removeObject( supplierProductGroup ) ;
	}
}
