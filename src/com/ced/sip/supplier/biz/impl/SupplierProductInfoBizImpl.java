package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierProductInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductInfo;

public class SupplierProductInfoBizImpl extends BaseBizImpl implements ISupplierProductInfoBiz  {
	
	/**
	 * 根据主键获得供应商产品表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierProductInfo getSupplierProductInfo(Long id) throws BaseException {
		return (SupplierProductInfo)this.getObject(SupplierProductInfo.class, id);
	}
	
	/**
	 * 获得供应商产品表实例
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierProductInfo getSupplierProductInfo( SupplierProductInfo supplierProductInfo ) throws BaseException {
		return (SupplierProductInfo)this.getObject(SupplierProductInfo.class, supplierProductInfo.getSpiId() );
	}
	
	/**
	 * 添加供应商产品信息
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierProductInfo(SupplierProductInfo supplierProductInfo) throws BaseException{
		this.saveObject( supplierProductInfo ) ;
	}
	
	/**
	 * 更新供应商产品表实例
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierProductInfo(SupplierProductInfo supplierProductInfo) throws BaseException{
		this.updateObject( supplierProductInfo ) ;
	}
	
	/**
	 * 删除供应商产品表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierProductInfo(String id) throws BaseException {
		this.removeObject( this.getSupplierProductInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商产品表实例
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierProductInfo(SupplierProductInfo supplierProductInfo) throws BaseException {
		this.removeObject( supplierProductInfo ) ;
	}
	
	/**
	 * 删除供应商产品表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierProductInfos(String[] id) throws BaseException {
		this.removeBatchObject(SupplierProductInfo.class, id) ;
	}
	
	/**
	 * 获得所有供应商产品表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierProductInfoList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierProductInfo de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商产品表数据集
	 * @param supplierProductInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierProductInfoList(  SupplierProductInfo supplierProductInfo ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierProductInfo de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商产品表数据集
	 * @param rollPage 分页对象
	 * @param supplierProductInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierProductInfoList( RollPage rollPage, SupplierProductInfo supplierProductInfo ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierProductInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierProductInfo)){
			if(StringUtil.isNotBlank(supplierProductInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierProductInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierProductInfo.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getProductKind())){
				hql.append(" and de.productKind like '%").append(supplierProductInfo.getProductKind()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getProductName())){
				hql.append(" and de.productName like '%").append(supplierProductInfo.getProductName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getStatus())){
				hql.append(" and de.status = '").append(supplierProductInfo.getStatus()).append("'");
			}
			
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	public List getSupplierProductInfoListForWeb( RollPage rollPage, SupplierProductInfo supplierProductInfo,SupplierInfo supplierInfo) throws BaseException {
		StringBuffer hql = new StringBuffer("select de from SupplierProductInfo de,SupplierInfo si where si.supplierId=de.supplierId " );
		if(StringUtil.isNotBlank(supplierProductInfo)){
			if(StringUtil.isNotBlank(supplierProductInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierProductInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierProductInfo.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getProductId())){
				hql.append(" and de.supplierId =").append(supplierProductInfo.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getProductName())){
				hql.append(" and (de.productName like '%").append(supplierProductInfo.getProductName()).append("%' or  de.productName like '%").append(supplierProductInfo.getProductName()).append("%')");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getStatus())){
				hql.append(" and de.status = '").append(supplierProductInfo.getStatus()).append("'");
			}
		}
		if(StringUtil.isNotBlank(supplierInfo)){
			if(StringUtil.isNotBlank(supplierInfo.getCity())){
				hql.append(" and si.city = '").append(supplierInfo.getCity()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getProvince())){
				hql.append(" and si.province = '").append(supplierInfo.getProvince()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getIndustryOwned())){
				hql.append(" and si.industryOwned = '").append(supplierInfo.getIndustryOwned()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getManagementModel())){
				hql.append(" and si.managementModel = '").append(supplierInfo.getManagementModel()).append("'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getScopeBusiness())){
				hql.append(" and si.scopeBusiness like '%").append(supplierInfo.getScopeBusiness()).append("%'");
			}
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by de.spiId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
     * 查询产品总数
     * @param supplierProductInfo
     * @return
     * @throws BaseException
     */
	public int countSupplierProductInfo(SupplierProductInfo supplierProductInfo)
			throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.spiId) from SupplierProductInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierProductInfo)){
			if(StringUtil.isNotBlank(supplierProductInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierProductInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierProductInfo.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getProductKind())){
				hql.append(" and de.productKind like '%").append(supplierProductInfo.getProductKind()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getProductName())){
				hql.append(" and de.productName like '%").append(supplierProductInfo.getProductName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierProductInfo.getStatus())){
				hql.append(" and de.status = '").append(supplierProductInfo.getStatus()).append("'");
			}
			
		}
		hql.append(" and de.isUsable = '0'");
		return this.countObjects(hql.toString());
	}
	
}
