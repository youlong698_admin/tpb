package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierStatusChangeBiz;
import com.ced.sip.supplier.entity.SupplierStatusChange;
/** 
 * 类名称：SupplierStatusChangeBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-18
 */
public class SupplierStatusChangeBizImpl extends BaseBizImpl implements ISupplierStatusChangeBiz  {
	
	/**
	 * 根据主键获得供应商状态变更表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-18
	 * @return
	 * @throws BaseException 
	 */
	public SupplierStatusChange getSupplierStatusChange(Long id) throws BaseException {
		return (SupplierStatusChange)this.getObject(SupplierStatusChange.class, id);
	}
	
	/**
	 * 获得供应商状态变更表实例
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @author luguanglei 2017-04-18
	 * @return
	 * @throws BaseException 
	 */
	public SupplierStatusChange getSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException {
		return (SupplierStatusChange)this.getObject(SupplierStatusChange.class, supplierStatusChange.getSscId() );
	}
	
	/**
	 * 添加供应商状态变更信息
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @author luguanglei 2017-04-18
	 * @throws BaseException 
	 */
	public void saveSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException{
		this.saveObject( supplierStatusChange ) ;
	}
	
	/**
	 * 更新供应商状态变更表实例
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @author luguanglei 2017-04-18
	 * @throws BaseException 
	 */
	public void updateSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException{
		this.updateObject( supplierStatusChange ) ;
	}
	
	/**
	 * 删除供应商状态变更表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-18
	 * @throws BaseException 
	 */
	public void deleteSupplierStatusChange(String id) throws BaseException {
		this.removeObject( this.getSupplierStatusChange( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商状态变更表实例
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @author luguanglei 2017-04-18
	 * @throws BaseException 
	 */
	public void deleteSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException {
		this.removeObject( supplierStatusChange ) ;
	}
	
	/**
	 * 删除供应商状态变更表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-18
	 * @throws BaseException 
	 */
	public void deleteSupplierStatusChanges(String[] id) throws BaseException {
		this.removeBatchObject(SupplierStatusChange.class, id) ;
	}
	
	/**
	 * 获得所有供应商状态变更表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-18
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierStatusChangeList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierStatusChange de where 1 = 1 " );

		hql.append(" order by de.sscId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商状态变更表数据集
	 * @param supplierStatusChange 查询参数对象
	 * @author luguanglei 2017-04-18
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierStatusChangeList(SupplierStatusChange supplierStatusChange) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierStatusChange de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierStatusChange)){
			if(StringUtil.isNotBlank(supplierStatusChange.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierStatusChange.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierStatusChange.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierStatusChange.getSupplierId());
			}
			
		}
		hql.append(" order by de.sscId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商状态变更表数据集
	 * @param rollPage 分页对象
	 * @param supplierStatusChange 查询参数对象
	 * @author luguanglei 2017-04-18
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierStatusChangeList(RollPage rollPage, SupplierStatusChange supplierStatusChange) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierStatusChange de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierStatusChange)){
			if(StringUtil.isNotBlank(supplierStatusChange.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierStatusChange.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierStatusChange.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierStatusChange.getSupplierId());
			}
			
		}
		hql.append(" order by de.sscId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
     * 根据列名、结果、供应商id更新核心供应商表
     * @param column
     * @param result
     * @param supplierId
     * @throws BaseException
     */
	public void updateSupplierInfoByColumn(String column, String result,
			Long supplierId) throws BaseException {
		String sql="update supplier_info set "+column+"="+result+" where supplier_id="+supplierId;
		this.updateJdbcSql(sql);
		
	}
}
