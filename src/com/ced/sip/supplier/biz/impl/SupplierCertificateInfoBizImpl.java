package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierCertificateInfoBiz;
import com.ced.sip.supplier.entity.SupplierCertificateInfo;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.QualityCategory;

public class SupplierCertificateInfoBizImpl extends BaseBizImpl implements ISupplierCertificateInfoBiz  {
	
	/**
	 * 根据主键获得供应商资质表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierCertificateInfo getSupplierCertificateInfo(Long id) throws BaseException {
		return (SupplierCertificateInfo)this.getObject(SupplierCertificateInfo.class, id);
	}
	
	/**
	 * 获得供应商资质表实例
	 * @param supplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierCertificateInfo getSupplierCertificateInfo( SupplierCertificateInfo supplierCertificateInfo ) throws BaseException {
		return (SupplierCertificateInfo)this.getObject(SupplierCertificateInfo.class, supplierCertificateInfo.getSciId() );
	}
	
	/**
	 * 添加供应商资质信息
	 * @param SupplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierCertificateInfo(SupplierCertificateInfo SupplierCertificateInfo) throws BaseException{
		this.saveObject( SupplierCertificateInfo ) ;
	}
	
	/**
	 * 更新供应商资质表实例
	 * @param supplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierCertificateInfo(SupplierCertificateInfo supplierCertificateInfo) throws BaseException{
		this.updateObject( supplierCertificateInfo ) ;
	}
	
	/**
	 * 删除供应商资质表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierCertificateInfo(String id) throws BaseException {
		this.removeObject( this.getSupplierCertificateInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商资质表实例
	 * @param supplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierCertificateInfo(SupplierCertificateInfo supplierCertificateInfo) throws BaseException {
		this.removeObject( supplierCertificateInfo ) ;
	}
	
	/**
	 * 删除供应商资质表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierCertificateInfos(String[] id) throws BaseException {
		this.removeBatchObject(SupplierCertificateInfo.class, id) ;
	}
	
	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateInfoList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateInfo de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商资质表数据集
	 * @param supplierCertificateInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateInfoList(  SupplierCertificateInfo supplierCertificateInfo ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateInfo de where 1 = 1 " );
        if(StringUtil.isNotBlank(supplierCertificateInfo)){
			
        	if(StringUtil.isNotBlank(supplierCertificateInfo.getQcId())){
				hql.append(" and de.qcId =").append(supplierCertificateInfo.getQcId());
			}
			if(StringUtil.isNotBlank(supplierCertificateInfo.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierCertificateInfo.getSupplierId());
			}
			
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @param supplierCertificateInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateInfoList( RollPage rollPage, SupplierCertificateInfo supplierCertificateInfo ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierCertificateInfo)){
			
			if(StringUtil.isNotBlank(supplierCertificateInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierCertificateInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierCertificateInfo.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierCertificateInfo.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierCertificateInfo.getCertificateName())){
				hql.append(" and de.certificateName like '%").append(supplierCertificateInfo.getCertificateName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierCertificateInfo.getTimeBegain())){
				hql.append(" and trunc(de.timeBegain,'dd') = to_date('").append( DateUtil.getDefaultDateFormat(supplierCertificateInfo.getTimeBegain())).append("','yyyy-MM-dd')");
			}
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @param supplierCertificateInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierCertificateInfoList2( RollPage rollPage, SupplierCertificateInfo supplierCertificateInfo ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierCertificateInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierCertificateInfo)){
			
			if(StringUtil.isNotBlank(supplierCertificateInfo.getSupplierName())){
				hql.append(" and de.supplierName like '%").append(supplierCertificateInfo.getSupplierName()).append("%'");
			}
			
			if(StringUtil.isNotBlank(supplierCertificateInfo.getCertificateName())){
				hql.append(" and de.certificateName like '%").append(supplierCertificateInfo.getCertificateName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierCertificateInfo.getTimeBegain())){
				hql.append(" and trunc(de.timeBegain,'dd') = to_date('").append( DateUtil.getDefaultDateFormat(supplierCertificateInfo.getTimeBegain())).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(supplierCertificateInfo.getSupplierId())){
				hql.append(" and de.supplierId =").append( supplierCertificateInfo.getSupplierId());
			}
		}
		hql.append(" and de.isUsable = '0'");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}

	
	public List getSupplierCertificateInRemindfoList(RollPage rollPage,
			SupplierInfo supplierInfo)
			throws BaseException {
		StringBuffer sql = new StringBuffer("select de.*,si.supplier_name,si.contact_person,si.mobile_phone from certificate_remind_view de left join supplier_info si on si.supplier_id=de.supplier_id where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierInfo)){
			
			if(StringUtil.isNotBlank(supplierInfo.getSupplierName())){
				sql.append(" and si.supplier_name like '%").append(supplierInfo.getSupplierName()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierInfo.getIsAudit())){
				sql.append(" and si.is_audit = '").append(supplierInfo.getIsAudit()).append("'");
			}
			
		}
		sql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjectsListRollPage(SupplierCertificateInfo.class,rollPage, sql.toString());
	}


	
	public List getQualityCategoryList(Long supplierId,String registrationType) throws BaseException {
		String sql="";
		if(registrationType.equals("01"))
		sql="select t.qc_id, t.quality_name "
					  +"from quality_category t where t.code!='01' and t.code!='03' and t.code!='04' ";
		else
			sql="select t.qc_id, t.quality_name "
				  +"from quality_category t where t.code!='02'";	
		return this.getObjectsList(QualityCategory.class, sql);
	}
}
