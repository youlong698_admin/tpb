package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.IEvaluateSupplierRelationBiz;
import com.ced.sip.supplier.entity.SupplierEvaluateIndex;
import com.ced.sip.supplier.entity.SupplierEvaluateResult;
import com.ced.sip.supplier.entity.SupplierEvaluateSup;
import com.ced.sip.supplier.entity.SupplierEvaluateUser;
import com.ced.sip.supplier.entity.SupplierEvaluationUserSup;

public class EvaluateSupplierRelationBizImpl extends BaseBizImpl implements  IEvaluateSupplierRelationBiz {
	
/*******************************供应商和考核人关联信息************************************/
	
	/**
	 * 根据主键获得供应商和考核人关联表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluationUserSup getSupplierEvaluationUserSup(Long id) throws BaseException {
		return (SupplierEvaluationUserSup)this.getObject(SupplierEvaluationUserSup.class, id);
	}
	
	/**
	 * 获得供应商和考核人关联表实例
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluationUserSup getSupplierEvaluationUserSup( SupplierEvaluationUserSup supplierEvaluationUserSup ) throws BaseException {
		return (SupplierEvaluationUserSup)this.getObject(SupplierEvaluationUserSup.class, supplierEvaluationUserSup.getUsId() );
	}
	
	/**
	 * 添加供应商考核人信息信息
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluationUserSup(SupplierEvaluationUserSup supplierEvaluationUserSup) throws BaseException{
		this.saveObject( supplierEvaluationUserSup ) ;
	}
	
	/**
	 * 更新供应商和考核人关联表实例
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluationUserSup(SupplierEvaluationUserSup supplierEvaluationUserSup) throws BaseException{
		this.updateObject( supplierEvaluationUserSup ) ;
	}
	
	/**
	 * 删除供应商和考核人关联表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationUserSup(String id) throws BaseException {
		this.removeObject( this.getSupplierEvaluationUserSup( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商和考核人关联表实例
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationUserSup(SupplierEvaluationUserSup supplierEvaluationUserSup) throws BaseException {
		this.removeObject( supplierEvaluationUserSup ) ;
	}
	
	/**
	 * 删除供应商和考核人关联表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationUserSup(String[] id) throws BaseException {
		this.removeBatchObject(SupplierEvaluationUserSup.class, id) ;
	}
	
	/**
	 * 获得所有供应商和考核人关联表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluationUserSupList(  SupplierEvaluationUserSup supplierEvaluationUserSup ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluationUserSup de where 1 = 1 " );
		if(supplierEvaluationUserSup!=null){
			if(StringUtil.isNotBlank(supplierEvaluationUserSup.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluationUserSup.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluationUserSup.getSesId())){
				hql.append(" and de.sesId =").append(supplierEvaluationUserSup.getSesId());
			}
			if(StringUtil.isNotBlank(supplierEvaluationUserSup.getSeuId())){
				hql.append(" and de.seuId =").append(supplierEvaluationUserSup.getSeuId());
			}
		}
		hql.append(" order by de.seuId,de.sesId ");
		return this.getObjects( hql.toString() );
	}
	
	
	/****************************供应商考核指标数据********************************/
	/**
	 * 根据主键获得供应商考核指标表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateIndex getSupplierEvaluateIndex(Long id) throws BaseException {
		return (SupplierEvaluateIndex)this.getObject(SupplierEvaluateIndex.class, id);
	}
	
	/**
	 * 获得供应商考核指标表实例
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateIndex getSupplierEvaluateIndex( SupplierEvaluateIndex supplierEvaluateIndex ) throws BaseException {
		return (SupplierEvaluateIndex)this.getObject(SupplierEvaluateIndex.class, supplierEvaluateIndex.getSeiId() );
	}
	
	/**
	 * 添加供应商考核指标信息
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluateIndex(SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException{
		this.saveObject( supplierEvaluateIndex ) ;
	}
	
	/**
	 * 更新供应商考核指标表实例
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluateIndex(SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException{
		this.updateObject( supplierEvaluateIndex ) ;
	}
	
	/**
	 * 删除供应商考核指标表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateIndex(String id) throws BaseException {
		this.removeObject( this.getSupplierEvaluateIndex( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商考核指标表实例
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateIndex(SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException {
		this.removeObject( supplierEvaluateIndex ) ;
	}
	
	/**
	 * 删除供应商考核指标表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateIndexs(String[] id) throws BaseException {
		this.removeBatchObject(SupplierEvaluateIndex.class, id) ;
	}
	
	/**
	 * 获得所有供应商考核指标表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateIndexList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateIndex de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核指标表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateIndexList(  SupplierEvaluateIndex supplierEvaluateIndex ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateIndex de where 1 = 1 " );
		if(supplierEvaluateIndex!=null){
			if(StringUtil.isNotBlank(supplierEvaluateIndex.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateIndex.getSeId());
			}
		}
		hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核指标表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateIndexList( RollPage rollPage, SupplierEvaluateIndex supplierEvaluateIndex ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateIndex de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/*******************************供应商考核人信息************************************/
	
	/**
	 * 根据主键获得供应商考核人信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateUser getSupplierEvaluateUser(Long id) throws BaseException {
		return (SupplierEvaluateUser)this.getObject(SupplierEvaluateUser.class, id);
	}
	
	/**
	 * 获得供应商考核人信息表实例
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateUser getSupplierEvaluateUser( SupplierEvaluateUser supplierEvaluateUser ) throws BaseException {
		return (SupplierEvaluateUser)this.getObject(SupplierEvaluateUser.class, supplierEvaluateUser.getSeuId() );
	}
	
	/**
	 * 添加供应商考核人信息信息
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluateUser(SupplierEvaluateUser supplierEvaluateUser) throws BaseException{
		this.saveObject( supplierEvaluateUser ) ;
	}
	
	/**
	 * 更新供应商考核人信息表实例
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluateUser(SupplierEvaluateUser supplierEvaluateUser) throws BaseException{
		this.updateObject( supplierEvaluateUser ) ;
	}
	
	/**
	 * 删除供应商考核人信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateUser(String id) throws BaseException {
		this.removeObject( this.getSupplierEvaluateIndex( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商考核人信息表实例
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateUser(SupplierEvaluateUser supplierEvaluateUser) throws BaseException {
		this.removeObject( supplierEvaluateUser ) ;
	}
	
	/**
	 * 删除供应商考核人信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateUsers(String[] id) throws BaseException {
		this.removeBatchObject(SupplierEvaluateUser.class, id) ;
	}
	/**
	 * 删除供应商考核人信息表实例  依据userId 和seId
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public  void deleteSupplierEvaluateUsersByUserIdAndSeId(String userId,Long seId)
			throws BaseException {
		String sql="begin delete from supplier_evaluation_user_sup where seu_id=(select seu_id from supplier_evaluataion_user where user_id="+userId+" and se_id="+seId+");delete from supplier_evaluataion_user where user_id="+userId+" and se_id="+seId+";end;";
	    this.updateJdbcSql(sql);
	}
	/**
	 * 获得所有供应商考核人信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateUserList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateUser de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核人信息表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateUserList(  SupplierEvaluateUser supplierEvaluateUser ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateUser de where 1 = 1 " );
		if(supplierEvaluateUser!=null){
			if(StringUtil.isNotBlank(supplierEvaluateUser.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateUser.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateUser.getSeuId())){
				hql.append(" and de.seuId =").append(supplierEvaluateUser.getSeuId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateUser.getUserId())){
				hql.append(" and de.userId =").append(supplierEvaluateUser.getUserId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateUser.getUserName())){
				hql.append(" and de.userName = '"+supplierEvaluateUser.getUserName()+"'");
			}
		}
		hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核人信息表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateUserList( RollPage rollPage, SupplierEvaluateUser supplierEvaluateUser ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateUser de where 1 = 1 " );
		if(supplierEvaluateUser!=null){
			if(StringUtil.isNotBlank(supplierEvaluateUser.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateUser.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateUser.getSeuId())){
				hql.append(" and de.seuId =").append(supplierEvaluateUser.getSeuId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateUser.getUserId())){
				hql.append(" and de.userId =").append(supplierEvaluateUser.getUserId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateUser.getUserName())){
				hql.append(" and de.userName like '%"+supplierEvaluateUser.getUserName()+"%'");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/*******************************被考核供应商信息************************************/
	
	/**
	 * 根据主键获得被考核供应商信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateSup getSupplierEvaluateSup(Long id) throws BaseException {
		return (SupplierEvaluateSup)this.getObject(SupplierEvaluateSup.class, id);
	}
	
	/**
	 * 获得被考核供应商信息表实例
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateSup getSupplierEvaluateSup( SupplierEvaluateSup supplierEvaluateSup ) throws BaseException {
		return (SupplierEvaluateSup)this.getObject(SupplierEvaluateSup.class, supplierEvaluateSup.getSesId() );
	}
	
	/**
	 * 添加被考核供应商信息信息
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluateSup(SupplierEvaluateSup supplierEvaluateSup) throws BaseException{
		this.saveObject( supplierEvaluateSup ) ;
	}
	
	/**
	 * 更新被考核供应商信息表实例
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluateSup(SupplierEvaluateSup supplierEvaluateSup) throws BaseException{
		this.updateObject( supplierEvaluateSup ) ;
	}
	
	/**
	 * 删除被考核供应商信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateSup(String id) throws BaseException {
		this.removeObject( this.getSupplierEvaluateIndex( new Long(id) ) ) ;
	}
	
	/**
	 * 删除被考核供应商信息表实例
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateSup(SupplierEvaluateSup supplierEvaluateSup) throws BaseException {
		this.removeObject( supplierEvaluateSup ) ;
	}
	
	/**
	 * 删除被考核供应商信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateSups(String[] id) throws BaseException {
		this.removeBatchObject(SupplierEvaluateSup.class, id) ;
	}
	
	/**
	 * 获得所有被考核供应商信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateSupList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateSup de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有被考核供应商信息表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateSupList(  SupplierEvaluateSup supplierEvaluateSup ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateSup de where 1 = 1 " );
		if(supplierEvaluateSup!=null){
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateSup.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSesId())){
				hql.append(" and de.sesId =").append(supplierEvaluateSup.getSesId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSupplierId())){
				hql.append(" and de.supplierId =").append(supplierEvaluateSup.getSupplierId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSupplierName())){
				hql.append(" and de.supplierName like '%"+supplierEvaluateSup.getSupplierName()+"%'");
			}
			
		}
		
		hql.append(" order by de.seId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有被考核供应商信息表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateSupList( RollPage rollPage, SupplierEvaluateSup supplierEvaluateSup ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateSup de where 1 = 1 " );
		if(supplierEvaluateSup!=null){
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateSup.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSesId())){
				hql.append(" and de.sesId =").append(supplierEvaluateSup.getSesId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSupplierName())){
				hql.append(" and de.supplierName like '%"+supplierEvaluateSup.getSupplierName()+"%'");
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getStatus())){
				hql.append(" and de.status= '"+supplierEvaluateSup.getStatus()+"'");
			}
			
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有被考核供应商信息表数据集  供应商信息查看tab页所用
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateSupListForTab( RollPage rollPage, SupplierEvaluateSup supplierEvaluateSup ) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,se.seDescribe from SupplierEvaluateSup de,SupplierEvaluations se where se.seId=de.seId " );
		if(supplierEvaluateSup!=null){
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateSup.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSesId())){
				hql.append(" and de.sesId =").append(supplierEvaluateSup.getSesId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getSupplierName())){
				hql.append(" and de.supplierName like '%"+supplierEvaluateSup.getSupplierName()+"%'");
			}
			if(StringUtil.isNotBlank(supplierEvaluateSup.getStatus())){
				hql.append(" and de.status= '"+supplierEvaluateSup.getStatus()+"'");
			}
			
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/******************************供应商考核结果信息**********************************/
	
	/**
	 * 根据主键获得供应商考核结果信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateResult getSupplierEvaluateResult(Long id) throws BaseException {
		return (SupplierEvaluateResult)this.getObject(SupplierEvaluateResult.class, id);
	}
	
	/**
	 * 获得供应商考核结果信息表实例
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluateResult getSupplierEvaluateResult( SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		return (SupplierEvaluateResult)this.getObject(SupplierEvaluateResult.class, supplierEvaluateResult.getSerId() );
	}
	
	/**
	 * 添加供应商考核结果信息信息
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluateResult(SupplierEvaluateResult supplierEvaluateResult) throws BaseException{
		this.saveObject( supplierEvaluateResult ) ;
	}
	
	/**
	 * 更新供应商考核结果信息表实例
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluateResult(SupplierEvaluateResult supplierEvaluateResult) throws BaseException{
		this.updateObject( supplierEvaluateResult ) ;
	}
	
	/**
	 * 删除供应商考核结果信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateResult(String id) throws BaseException {
		this.removeObject( this.getSupplierEvaluateIndex( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商考核结果信息表实例
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateResult(SupplierEvaluateResult supplierEvaluateResult) throws BaseException {
		this.removeObject( supplierEvaluateResult ) ;
	}
	
	/**
	 * 删除供应商考核结果信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluateResults(String[] id) throws BaseException {
		this.removeBatchObject(SupplierEvaluateResult.class, id) ;
	}
	/**
	 * 删除供应商考核人信息表实例  依据supplierId 和seId
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public  void deleteSupplierEvaluationUserSupBySeId(Long seId)
			throws BaseException {
		String sql="delete from supplier_evaluation_user_sup where se_id="+seId;
	    this.updateJdbcSql(sql);
	}
	/**
	 * 删除供应商考核人信息表实例  依据supplierId 和seId
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public  void deleteSupplierEvaluationUserSupBySupplierIdAndSeId(String supplierId,Long seId)
			throws BaseException {
		String sql="begin delete from supplier_evaluation_user_sup where ses_id=(select ses_id from supplier_evaluataion_sup where supplier_id="+supplierId+" and se_id="+seId+");delete from supplier_evaluataion_sup where supplier_id="+supplierId+" and se_id="+seId+";end;";
	    this.updateJdbcSql(sql);
	}
	/**
	 * 获得所有供应商考核结果信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateResultList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateResult de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核结果信息表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateResultList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateResult de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
			hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核结果信息表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateResultList( RollPage rollPage, SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateResult de where 1 = 1 " );
		
		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 根据seId和seuId
	 * 获得所有被考核供应商结果表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateResultAboutSupList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de.sesId,sum(de.judgePoint) from SupplierEvaluateResult de where 1 = 1 " );
		if(supplierEvaluateResult!=null){
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeuId())){
				hql.append(" and de.seuId =").append(supplierEvaluateResult.getSeuId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getStatus())){
				hql.append(" and de.status ='").append(supplierEvaluateResult.getStatus()).append("'");
			}
		}
		hql.append(" group by de.sesId");
		//hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 根据seId、seuId、sesId
	 * 获得所有被考核供应商结果表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateAboutIndexList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluateResult de where 1 = 1 " );
		if(supplierEvaluateResult!=null){
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeuId())){
				hql.append(" and de.seuId =").append(supplierEvaluateResult.getSeuId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSesId())){
				hql.append(" and de.sesId =").append(supplierEvaluateResult.getSesId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getStatus())){
				hql.append(" and de.status ='").append(supplierEvaluateResult.getStatus()).append("'");
			}
		}
		hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有被考核供应商总得分表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateSumPointList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer("select de.sesId,sum(de.judgePoint),ses.supplierId,ses.supplierName,ses.supplierNameSimple from SupplierEvaluateResult de,SupplierEvaluateSup ses where de.sesId=ses.sesId" );
		if(supplierEvaluateResult!=null){
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getStatus())){
				hql.append(" and de.status ='").append(supplierEvaluateResult.getStatus()).append("'");
			}
		}
		hql.append(" group by de.sesId,ses.supplierId,ses.supplierName,ses.supplierNameSimple");
		//hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有已提交考核人数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateUserSubmitList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(distinct de.seuId) from SupplierEvaluateResult de where 1 = 1 " );
		if(supplierEvaluateResult!=null){
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSesId())){
				hql.append(" and de.sesId =").append(supplierEvaluateResult.getSesId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getStatus())){
				hql.append(" and de.status ='").append(supplierEvaluateResult.getStatus()).append("'");
			}
		}
		//hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有已提交被考核供应商数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluateSubmitList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(distinct de.sesId) from SupplierEvaluateResult de where 1 = 1 " );
		if(supplierEvaluateResult!=null){
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
				hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getSeuId())){
				hql.append(" and de.seuId =").append(supplierEvaluateResult.getSeuId());
			}
			if(StringUtil.isNotBlank(supplierEvaluateResult.getStatus())){
				hql.append(" and de.status ='").append(supplierEvaluateResult.getStatus()).append("'");
			}
		}
		//hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}

	 /**
		 * 获得考核人对某个被考核供应商评分表数据集
		 * @param demo 查询参数对象
		 * @author 
		 * @return
		 * @throws BaseException 
		 */
		public List getSupplierEvaluatePointByUserList(  SupplierEvaluateResult supplierEvaluateResult ) throws BaseException {
			StringBuffer hql = new StringBuffer("select de.seuId,sum(de.judgePoint),to_char(de.seDate,'yyyy-MM-dd'),seu.userId,seu.userName from SupplierEvaluateResult de,SupplierEvaluateUser seu where de.seuId=seu.seuId" );
			if(supplierEvaluateResult!=null){
				if(StringUtil.isNotBlank(supplierEvaluateResult.getSeId())){
					hql.append(" and de.seId =").append(supplierEvaluateResult.getSeId());
				}
				if(StringUtil.isNotBlank(supplierEvaluateResult.getSesId())){
					hql.append(" and de.sesId =").append(supplierEvaluateResult.getSesId());
				}
				if(StringUtil.isNotBlank(supplierEvaluateResult.getStatus())){
					hql.append(" and de.status ='").append(supplierEvaluateResult.getStatus()).append("'");
				}
			}
			hql.append(" group by de.seuId,to_char(de.seDate,'yyyy-MM-dd'),seu.userId,seu.userName");
			//hql.append(" order by de.id asc ");
			return this.getObjects( hql.toString() );
		}
}
