package com.ced.sip.supplier.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.IEvaluateSupplierBiz;
import com.ced.sip.supplier.entity.SupplierEvaluations;

public class EvaluateSupplierBizImpl extends BaseBizImpl implements IEvaluateSupplierBiz  {
	
	/**
	 * 根据主键获得供应商考核主表信息表实例
	 * @param id 主键
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluations getSupplierEvaluations(Long id) throws BaseException {
		return (SupplierEvaluations)this.getObject(SupplierEvaluations.class, id);
	}
	
	/**
	 * 获得供应商考核主表信息表实例
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluations getSupplierEvaluations( SupplierEvaluations supplierEvaluations ) throws BaseException {
		return (SupplierEvaluations)this.getObject(SupplierEvaluations.class, supplierEvaluations.getSeId() );
	}
	
	/**
	 * 添加供应商考核主表信息信息
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluations(SupplierEvaluations supplierEvaluations) throws BaseException{
		this.saveObject( supplierEvaluations ) ;
	}
	
	/**
	 * 更新供应商考核主表信息表实例
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluations(SupplierEvaluations supplierEvaluations) throws BaseException{
		this.updateObject( supplierEvaluations ) ;
	}
	
	/**
	 * 删除供应商考核主表信息表实例
	 * @param id 主键数组
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluations(String id) throws BaseException {
		this.removeObject( this.getSupplierEvaluations( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商考核主表信息表实例
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluations(SupplierEvaluations supplierEvaluations) throws BaseException {
		this.removeObject( supplierEvaluations ) ;
	}
	
	/**
	 * 删除供应商考核主表信息表实例
	 * @param id 主键数组
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationss(String[] id) throws BaseException {
		this.removeBatchObject(SupplierEvaluations.class, id) ;
	}
	
	/**
	 * 获得所有供应商考核主表信息表数据集
	 * @param rollPage 分页对象
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluationsList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluations de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核主表信息表数据集
	 * @param supplierEvaluations 查询参数对象
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluationsList(  SupplierEvaluations supplierEvaluations ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from SupplierEvaluations de where 1 = 1 " );
		if(supplierEvaluations!=null){
			if(StringUtil.isNotBlank(supplierEvaluations.getSeCode())){
				hql.append(" and de.seCode ='").append(supplierEvaluations.getSeCode()).append("'");
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商考核主表信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierEvaluations 查询参数对象
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluationsList( RollPage rollPage, SupplierEvaluations supplierEvaluations ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from EvaluateSupplierView de where 1 = 1 " );
		if(StringUtil.isNotBlank(supplierEvaluations)){
			if(StringUtil.isNotBlank(supplierEvaluations.getSeDescribe())){
				hql.append(" and de.seDescribe like '%").append(supplierEvaluations.getSeDescribe()).append("%'");
			}
			if(StringUtil.isNotBlank(supplierEvaluations.getTimeEnd())){
				hql.append(" and de.timeEnd <= to_date('").append( DateUtil.getDefaultDateFormat( supplierEvaluations.getTimeEnd())).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(supplierEvaluations.getTimeStart())){
				hql.append(" and de.timeStart >= to_date('").append( DateUtil.getDefaultDateFormat( supplierEvaluations.getTimeStart())).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(supplierEvaluations.getComId())){
				hql.append(" and de.comId = ").append(supplierEvaluations.getComId());
			}
		}
		hql.append(" and de.isUsable = '0' ");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
