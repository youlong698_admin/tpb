package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.EvaluationIndexKind;

public interface IEvaluationIndexKindBiz {

	/**
	 * 根据主键获得考核指标类别表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract EvaluationIndexKind getEvaluationIndexKind(Long id)
			throws BaseException;

	/**
	 * 获得考核指标类别表实例
	 * @param evaluationIndexKind 考核指标类别表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract EvaluationIndexKind getEvaluationIndexKind(
			EvaluationIndexKind evaluationIndexKind) throws BaseException;

	/**
	 * 添加考核指标类别信息
	 * @param evaluationIndexKind 考核指标类别表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveEvaluationIndexKind(
			EvaluationIndexKind evaluationIndexKind) throws BaseException;

	/**
	 * 更新考核指标类别表实例
	 * @param evaluationIndexKind 考核指标类别表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateEvaluationIndexKind(
			EvaluationIndexKind evaluationIndexKind) throws BaseException;

	/**
	 * 删除考核指标类别表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteEvaluationIndexKind(String id)
			throws BaseException;

	/**
	 * 删除考核指标类别表实例
	 * @param evaluationIndexKind 考核指标类别表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteEvaluationIndexKind(
			EvaluationIndexKind evaluationIndexKind) throws BaseException;

	/**
	 * 删除考核指标类别表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteEvaluationIndexKinds(String[] id)
			throws BaseException;

	/**
	 * 获得所有考核指标类别表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getEvaluationIndexKindList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有考核指标类别表数据集
	 * @param evaluationIndexKind 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getEvaluationIndexKindList(
			EvaluationIndexKind evaluationIndexKind) throws BaseException;

	/**
	 * 获得所有考核指标类别表数据集
	 * @param rollPage 分页对象
	 * @param evaluationIndexKind 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getEvaluationIndexKindList(RollPage rollPage,
			EvaluationIndexKind evaluationIndexKind) throws BaseException;
	
	/**
	 * 获得考核指标类别个数
	 * @param evaluationIndexKind 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public abstract int getCountEvaluationIndexList(EvaluationIndexKind evaluationIndexKind ) throws BaseException;

	/**
	 * 获得指定供应商考核指标库信息表数据集
	 * @param EvaluationIndexKind 查询参数对象
	 * @author xuchengbin 2014-10-22
	 * @return
	 * @throws BaseException 
	 */
	abstract List getEvaluationIndexKindCode(  String materialType,String mkCodeLength ) throws BaseException ;
}