package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierEvaluateIndex;
import com.ced.sip.supplier.entity.SupplierEvaluateResult;
import com.ced.sip.supplier.entity.SupplierEvaluateSup;
import com.ced.sip.supplier.entity.SupplierEvaluateUser;
import com.ced.sip.supplier.entity.SupplierEvaluationUserSup;

public interface IEvaluateSupplierRelationBiz {


	/****************************供应商考核指标数据********************************/
	/**
	 * 根据主键获得供应商考核指标表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateIndex getSupplierEvaluateIndex(Long id)
			throws BaseException;

	/**
	 * 获得供应商考核指标表实例
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateIndex getSupplierEvaluateIndex(
			SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException;

	/**
	 * 添加供应商考核指标信息
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierEvaluateIndex(
			SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException;

	/**
	 * 更新供应商考核指标表实例
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierEvaluateIndex(
			SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException;

	/**
	 * 删除供应商考核指标表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateIndex(String id)
			throws BaseException;

	/**
	 * 删除供应商考核指标表实例
	 * @param demo 供应商考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateIndex(
			SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException;

	/**
	 * 删除供应商考核指标表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateIndexs(String[] id)
			throws BaseException;

	/**
	 * 获得所有供应商考核指标表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateIndexList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商考核指标表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateIndexList(
			SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException;

	/**
	 * 获得所有供应商考核指标表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateIndexList(RollPage rollPage,
			SupplierEvaluateIndex supplierEvaluateIndex) throws BaseException;

	/**
	 * 根据主键获得供应商考核人信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateUser getSupplierEvaluateUser(Long id)
			throws BaseException;

	/**
	 * 获得供应商考核人信息表实例
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateUser getSupplierEvaluateUser(
			SupplierEvaluateUser supplierEvaluateUser) throws BaseException;

	/**
	 * 添加供应商考核人信息信息
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierEvaluateUser(
			SupplierEvaluateUser supplierEvaluateUser) throws BaseException;

	/**
	 * 更新供应商考核人信息表实例
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierEvaluateUser(
			SupplierEvaluateUser supplierEvaluateUser) throws BaseException;

	/**
	 * 删除供应商考核人信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateUser(String id)
			throws BaseException;

	/**
	 * 删除供应商考核人信息表实例
	 * @param demo 供应商考核人信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateUser(
			SupplierEvaluateUser supplierEvaluateUser) throws BaseException;

	/**
	 * 删除供应商考核人信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateUsers(String[] id)
			throws BaseException;
	/**
	 * 删除供应商考核人信息表实例  依据userId 和seId
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateUsersByUserIdAndSeId(String userId,Long seId)
			throws BaseException;
	/**
	 * 获得所有供应商考核人信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateUserList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商考核人信息表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateUserList(
			SupplierEvaluateUser supplierEvaluateUser) throws BaseException;

	/**
	 * 获得所有供应商考核人信息表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateUserList(RollPage rollPage,
			SupplierEvaluateUser supplierEvaluateUser) throws BaseException;

	/**
	 * 根据主键获得被考核供应商信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateSup getSupplierEvaluateSup(Long id)
			throws BaseException;

	/**
	 * 获得被考核供应商信息表实例
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateSup getSupplierEvaluateSup(
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;

	/**
	 * 添加被考核供应商信息信息
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierEvaluateSup(
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;

	/**
	 * 更新被考核供应商信息表实例
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierEvaluateSup(
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;

	/**
	 * 删除被考核供应商信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateSup(String id)
			throws BaseException;

	/**
	 * 删除被考核供应商信息表实例
	 * @param demo 被考核供应商信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateSup(
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;

	/**
	 * 删除被考核供应商信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateSups(String[] id)
			throws BaseException;

	/**
	 * 获得所有被考核供应商信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateSupList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有被考核供应商信息表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateSupList(
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;

	/**
	 * 获得所有被考核供应商信息表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateSupList(RollPage rollPage,
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;
	/**
	 * 获得所有被考核供应商信息表数据集 供应商信息查看tab页所用
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateSupListForTab(RollPage rollPage,
			SupplierEvaluateSup supplierEvaluateSup) throws BaseException;
	/**
	 * 根据主键获得供应商考核结果信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateResult getSupplierEvaluateResult(Long id)
			throws BaseException;

	/**
	 * 获得供应商考核结果信息表实例
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluateResult getSupplierEvaluateResult(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 添加供应商考核结果信息信息
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierEvaluateResult(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 更新供应商考核结果信息表实例
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierEvaluateResult(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 删除供应商考核结果信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateResult(String id)
			throws BaseException;

	/**
	 * 删除供应商考核结果信息表实例
	 * @param demo 供应商考核结果信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateResult(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 删除供应商考核结果信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluateResults(String[] id)
			throws BaseException;

	/**
	 * 获得所有供应商考核结果信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateResultList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商考核结果信息表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateResultList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 获得所有供应商考核结果信息表数据集
	 * @param rollPage 分页对象
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateResultList(RollPage rollPage,
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 根据seId和seuId
	 * 获得所有被考核供应商结果表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateResultAboutSupList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 根据seId、seuId、sesId
	 * 获得所有被考核供应商结果表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateAboutIndexList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 获得所有被考核供应商总得分表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateSumPointList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 获得所有已提交考核人数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateUserSubmitList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 获得所有已提交被考核供应商数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluateSubmitList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;

	/**
	 * 获得考核人对某个被考核供应商评分表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluatePointByUserList(
			SupplierEvaluateResult supplierEvaluateResult) throws BaseException;
	/**
	 * 根据主键获得供应商和考核人关联表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluationUserSup getSupplierEvaluationUserSup(Long id) throws BaseException;
	
	/**
	 * 获得供应商和考核人关联表实例
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public SupplierEvaluationUserSup getSupplierEvaluationUserSup( SupplierEvaluationUserSup supplierEvaluationUserSup ) 
		throws BaseException;
	
	/**
	 * 添加供应商和考核人关联表信息
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void saveSupplierEvaluationUserSup(SupplierEvaluationUserSup supplierEvaluationUserSup) 
		throws BaseException;
	
	/**
	 * 更新供应商和考核人关联表实例
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void updateSupplierEvaluationUserSup(SupplierEvaluationUserSup supplierEvaluationUserSup) 
		throws BaseException;
	
	/**
	 * 删除供应商和考核人关联表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationUserSup(String id) throws BaseException;
	
	
	/**
	 * 删除供应商和考核人关联表实例
	 * @param demo 供应商和考核人关联表实例
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationUserSup(SupplierEvaluationUserSup supplierEvaluationUserSup) 
		throws BaseException;
	
	/**
	 * 删除供应商和考核人关联表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public void deleteSupplierEvaluationUserSup(String[] id) throws BaseException;
	/**
	 * 删除供应商考核人信息表实例  依据supplierId 和seId
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluationUserSupBySeId(Long seId)
			throws BaseException;
	/**
	 * 删除供应商考核人信息表实例  依据supplierId 和seId
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluationUserSupBySupplierIdAndSeId(String supplierId,Long seId)
			throws BaseException;
	/**
	 * 获得所有供应商和考核人关联表数据集
	 * @param demo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierEvaluationUserSupList(  SupplierEvaluationUserSup supplierEvaluationUserSup ) 
		throws BaseException;
}