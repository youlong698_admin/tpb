package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductInfo;

public interface ISupplierProductInfoBiz {

	/**
	 * 根据主键获得供应商产品表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierProductInfo getSupplierProductInfo(Long id)
			throws BaseException;

	/**
	 * 获得供应商产品表实例
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierProductInfo getSupplierProductInfo(
			SupplierProductInfo supplierProductInfo) throws BaseException;

	/**
	 * 添加供应商产品信息
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierProductInfo(
			SupplierProductInfo supplierProductInfo) throws BaseException;

	/**
	 * 更新供应商产品表实例
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierProductInfo(
			SupplierProductInfo supplierProductInfo) throws BaseException;

	/**
	 * 删除供应商产品表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierProductInfo(String id)
			throws BaseException;

	/**
	 * 删除供应商产品表实例
	 * @param supplierProductInfo 供应商产品表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierProductInfo(
			SupplierProductInfo supplierProductInfo) throws BaseException;

	/**
	 * 删除供应商产品表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierProductInfos(String[] id)
			throws BaseException;

	/**
	 * 获得所有供应商产品表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierProductInfoList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商产品表数据集
	 * @param supplierProductInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierProductInfoList(
			SupplierProductInfo supplierProductInfo) throws BaseException;

	/**
	 * 获得所有供应商产品表数据集
	 * @param rollPage 分页对象
	 * @param supplierProductInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierProductInfoList(RollPage rollPage,
			SupplierProductInfo supplierProductInfo) throws BaseException;
	/**
	 * 获得所有供应商产品表数据集 网站信息
	 * @param rollPage 分页对象
	 * @param supplierProductInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierProductInfoListForWeb(RollPage rollPage,
			SupplierProductInfo supplierProductInfo,SupplierInfo supplierInfo) throws BaseException;
    /**
     * 查询产品总数
     * @param supplierProductInfo
     * @return
     * @throws BaseException
     */
	public int countSupplierProductInfo(SupplierProductInfo supplierProductInfo) throws BaseException;
}