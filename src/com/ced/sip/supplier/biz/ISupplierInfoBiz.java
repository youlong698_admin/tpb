package com.ced.sip.supplier.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductGroup;

public interface ISupplierInfoBiz {

	/**
	 * 根据主键获得供应商基本信息表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierInfo getSupplierInfo(Long id) throws BaseException;
	
	/**
	 * 根据供应商名称获得供应商基本信息表实例
	 * @param supplierName 供应商名称
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierInfo getSupplierInfoByName(String supplierName) throws BaseException;

	/**
	 * 获得供应商基本信息表实例
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierInfo getSupplierInfo(SupplierInfo supplierInfo)
			throws BaseException;

	/**
	 * 添加供应商基本信息信息
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierInfo(SupplierInfo supplierInfo)
			throws BaseException;

	/**
	 * 更新供应商基本信息表实例
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierInfo(SupplierInfo supplierInfo)
			throws BaseException;

	/**
	 * 删除供应商基本信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierInfo(String id) throws BaseException;

	/**
	 * 删除供应商基本信息表实例
	 * @param supplierInfo 供应商基本信息表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierInfo(SupplierInfo supplierInfo)
			throws BaseException;

	/**
	 * 删除供应商基本信息表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierInfos(String[] id) throws BaseException;

	/**
	 * 获得所有供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商基本信息表数据集
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoList(SupplierInfo supplierInfo)
			throws BaseException;
	/**
	 * 获得核心供应商基本信息表数据集
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoListCompany(Map<String,Object> map,Long comId)
			throws BaseException;
	/**
	 * 获得所有供应商基本信息表数据集   所有供应商 包含各种状态
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierInfoListAll(RollPage rollPage,SupplierInfo supplierInfo ) throws BaseException;
	/**
	 * 获得所有供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoList(RollPage rollPage,
			SupplierInfo supplierInfo) throws BaseException;
	/**
	 * 获得所有供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoListAudit(RollPage rollPage,
			SupplierInfo supplierInfo,String sign) throws BaseException;
	/**
	 * 获得所有供应商基本信息表数据集总数
	 * @param map 查询参数对象
	 * @param sign 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract int countSupplierInfoListAudit(Map<String,Object> mapParams,String sign)
	         throws BaseException;
	/**
	 * 获得核心供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoListCompany(RollPage rollPage,
			Map<String,Object> map,Long comId) throws BaseException;
	/**
	 * 获得盘外供应商基本信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoListOutCompany(RollPage rollPage,
			SupplierInfo supplierInfo,Long comId) throws BaseException;

	/**
	 * 获得序列的下一个值
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract String getSupplierUserName(String seqName)
			throws BaseException;

	/**
	 * 根据rcId查询类别中的供应商
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	public List getSuppListByRcId(Long rcId,SupplierInfo supplierInfo)throws BaseException;
    /**
     * 根据列名、结果、供应商id更新供应商信息表，主要是审核用。 注册审核、变更审核
     * @param column
     * @param result
     * @param supplierId
     * @throws BaseException
     */
	public void updateSupplierInfoByColumn(String column,String result,Long supplierId) throws BaseException;

	/**
	 * 根据用户名和密码查询供应商列表
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierInfoListByValidate(SupplierInfo supplierInfo)
			throws BaseException;
	/**
	 * 添加供应商产品类别组信息
	 * @param SupplierProductGroup 供应商产品类别组表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierProductGroup(
			SupplierProductGroup supplierProductGroup) throws BaseException;
	/**
	 * 获得所有供应商产品类别组表数据集
	 * @param SupplierProductGroup 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierProductGroupList(
			SupplierProductGroup supplierProductGroup) throws BaseException;
	/**
	 * 删除供应商产品类别组表实例
	 * @param SupplierProductGroup 供应商产品类别组表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierProductGroup(
			SupplierProductGroup supplierProductGroup) throws BaseException;
}