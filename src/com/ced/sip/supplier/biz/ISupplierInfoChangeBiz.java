package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierInfoChange;
/** 
 * 类名称：ISupplierInfoChangeBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-17
 */
public interface ISupplierInfoChangeBiz {

	/**
	 * 添加供应商基本信息变更表信息
	 * @param supplierInfoChange 供应商基本信息变更表表实例
	 * @throws BaseException 
	 */
	abstract void saveSupplierInfoChange(SupplierInfoChange supplierInfoChange) throws BaseException;
	/**
	 * 根据供应商Id获得供应商基本信息变更表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SupplierInfoChange getSupplierInfoChangeBySupplierId(Long supplierId) throws BaseException;
	/**
	 * 获得所有供应商基本信息变更表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierInfoChangeList(RollPage rollPage) throws BaseException ;
	/**
	 * 更新供应商基本信息变更表表实例
	 * @param supplierInfoChange 供应商基本信息变更表表实例
	 * @throws BaseException 
	 */
	abstract void updateSupplierInfoChange(SupplierInfoChange supplierInfoChange) throws BaseException;
    /**
	 * 获得所有供应商基本信息变更表表数据集
	 * @param supplierInfoChange 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierInfoChangeList(SupplierInfoChange supplierInfoChange) throws BaseException ;
	
}