package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierStatusChange;
/** 
 * 类名称：ISupplierStatusChangeBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-18
 */
public interface ISupplierStatusChangeBiz {

	/**
	 * 根据主键获得供应商状态变更表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SupplierStatusChange getSupplierStatusChange(Long id) throws BaseException;

	/**
	 * 添加供应商状态变更信息
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @throws BaseException 
	 */
	abstract void saveSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException;

	/**
	 * 更新供应商状态变更表实例
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @throws BaseException 
	 */
	abstract void updateSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException;

	/**
	 * 删除供应商状态变更表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSupplierStatusChange(String id) throws BaseException;

	/**
	 * 删除供应商状态变更表实例
	 * @param supplierStatusChange 供应商状态变更表实例
	 * @throws BaseException 
	 */
	abstract void deleteSupplierStatusChange(SupplierStatusChange supplierStatusChange) throws BaseException;

	/**
	 * 删除供应商状态变更表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSupplierStatusChanges(String[] id) throws BaseException;

	/**
	 * 获得所有供应商状态变更表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierStatusChangeList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商状态变更表数据集
	 * @param supplierStatusChange 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierStatusChangeList(SupplierStatusChange supplierStatusChange) throws BaseException ;
	
	/**
	 * 获得所有供应商状态变更表数据集
	 * @param rollPage 分页对象
	 * @param supplierStatusChange 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierStatusChangeList(RollPage rollPage, SupplierStatusChange supplierStatusChange)
			throws BaseException;
	/**
     * 根据列名、结果、供应商id更新核心供应商表
     * @param column
     * @param result
     * @param supplierId
     * @throws BaseException
     */
	public void updateSupplierInfoByColumn(String column, String result,
			Long supplierId) throws BaseException;
}