package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierCertificateInfo;
import com.ced.sip.supplier.entity.SupplierInfo;

public interface ISupplierCertificateInfoBiz {

	/**
	 * 根据主键获得供应商资质表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierCertificateInfo getSupplierCertificateInfo(Long id)
			throws BaseException;

	/**
	 * 获得供应商资质表实例
	 * @param supplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierCertificateInfo getSupplierCertificateInfo(
			SupplierCertificateInfo supplierCertificateInfo)
			throws BaseException;

	/**
	 * 添加供应商资质信息
	 * @param SupplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveSupplierCertificateInfo(
			SupplierCertificateInfo SupplierCertificateInfo)
			throws BaseException;

	/**
	 * 更新供应商资质表实例
	 * @param supplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateSupplierCertificateInfo(
			SupplierCertificateInfo supplierCertificateInfo)
			throws BaseException;

	/**
	 * 删除供应商资质表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierCertificateInfo(String id)
			throws BaseException;

	/**
	 * 删除供应商资质表实例
	 * @param supplierCertificateInfo 供应商资质表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierCertificateInfo(
			SupplierCertificateInfo supplierCertificateInfo)
			throws BaseException;

	/**
	 * 删除供应商资质表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierCertificateInfos(String[] id)
			throws BaseException;

	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierCertificateInfoList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商资质表数据集
	 * @param supplierCertificateInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierCertificateInfoList(
			SupplierCertificateInfo supplierCertificateInfo)
			throws BaseException;

	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @param supplierCertificateInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierCertificateInfoList(RollPage rollPage,
			SupplierCertificateInfo supplierCertificateInfo)
			throws BaseException;
	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @param supplierCertificateInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierCertificateInfoList2(RollPage rollPage,
			SupplierCertificateInfo supplierCertificateInfo)
			throws BaseException;
	/**
	 * 获得所有供应商资质表数据集
	 * @param rollPage 分页对象
	 * @param supplierInfo 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierCertificateInRemindfoList(RollPage rollPage,
			SupplierInfo supplierInfo)
			throws BaseException;
	/**
	 * 根据供应商id查询应上传的资质文件信息
	 * @param supplierId
	 * @return
	 * @throws BaseException
	 */
	public abstract List getQualityCategoryList(Long supplierId,String registrationType)
			throws BaseException;
}