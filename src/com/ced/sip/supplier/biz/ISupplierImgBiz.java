package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierImg;
/** 
 * 类名称：ISupplierImgBiz
 * 创建人：luguanglei 
 * 创建时间：2017-06-30
 */
public interface ISupplierImgBiz {

	/**
	 * 根据主键获得产品图片表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SupplierImg getSupplierImg(Long id) throws BaseException;

	/**
	 * 添加产品图片信息
	 * @param supplierImg 产品图片表实例
	 * @throws BaseException 
	 */
	abstract void saveSupplierImg(SupplierImg supplierImg) throws BaseException;

	/**
	 * 更新产品图片表实例
	 * @param supplierImg 产品图片表实例
	 * @throws BaseException 
	 */
	abstract void updateSupplierImg(SupplierImg supplierImg) throws BaseException;

	/**
	 * 删除产品图片表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSupplierImg(String id) throws BaseException;

	/**
	 * 删除产品图片表实例
	 * @param supplierImg 产品图片表实例
	 * @throws BaseException 
	 */
	abstract void deleteSupplierImg(SupplierImg supplierImg) throws BaseException;

	/**
	 * 删除产品图片表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSupplierImgs(String[] id) throws BaseException;

	/**
	 * 获得产品图片表数据集
	 * supplierImg 产品图片表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract SupplierImg getSupplierImgBySupplierImg(SupplierImg supplierImg) throws BaseException ;
	
	/**
	 * 获得所有产品图片表数据集
	 * @param supplierImg 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierImgList(SupplierImg supplierImg) throws BaseException ;
	
	/**
	 * 获得所有产品图片表数据集
	 * @param rollPage 分页对象
	 * @param supplierImg 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierImgList(RollPage rollPage, SupplierImg supplierImg)
			throws BaseException;

}