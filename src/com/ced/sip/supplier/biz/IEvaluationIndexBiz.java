package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.EvaluationIndex;

public interface IEvaluationIndexBiz {

	/**
	 * 根据主键获得考核指标表实例
	 * @param id 主键
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract EvaluationIndex getEvaluationIndex(Long id)
			throws BaseException;

	/**
	 * 获得考核指标表实例
	 * @param evaluationIndex 考核指标表实例
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract EvaluationIndex getEvaluationIndex(
			EvaluationIndex evaluationIndex) throws BaseException;

	/**
	 * 添加考核指标信息
	 * @param evaluationIndex 考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void saveEvaluationIndex(EvaluationIndex evaluationIndex)
			throws BaseException;

	/**
	 * 更新考核指标表实例
	 * @param evaluationIndex 考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void updateEvaluationIndex(EvaluationIndex evaluationIndex)
			throws BaseException;

	/**
	 * 删除考核指标表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteEvaluationIndex(String id) throws BaseException;

	/**
	 * 删除考核指标表实例
	 * @param evaluationIndex 考核指标表实例
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteEvaluationIndex(EvaluationIndex evaluationIndex)
			throws BaseException;

	/**
	 * 删除考核指标表实例
	 * @param id 主键数组
	 * @author 
	 * @throws BaseException 
	 */
	public abstract void deleteEvaluationIndexs(String[] id)
			throws BaseException;

	/**
	 * 获得所有考核指标表数据集
	 * @param rollPage 分页对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getEvaluationIndexList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有考核指标表数据集
	 * @param evaluationIndex 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getEvaluationIndexList(EvaluationIndex evaluationIndex)
			throws BaseException;

	/**
	 * 获得所有考核指标表数据集
	 * @param rollPage 分页对象
	 * @param evaluationIndex 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getEvaluationIndexList(RollPage rollPage,
			EvaluationIndex evaluationIndex) throws BaseException;

}