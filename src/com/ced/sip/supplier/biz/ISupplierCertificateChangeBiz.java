package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierCertificateChange;
/** 
 * 类名称：ISupplierCertificateChangeBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-17
 */
public interface ISupplierCertificateChangeBiz {

	/**
	 * 根据主键获得供应商资质信息变更表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract SupplierCertificateChange getSupplierCertificateChange(Long id) throws BaseException;

	/**
	 * 添加供应商资质信息变更表信息
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @throws BaseException 
	 */
	abstract void saveSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException;

	/**
	 * 更新供应商资质信息变更表表实例
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @throws BaseException 
	 */
	abstract void updateSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException;

	/**
	 * 删除供应商资质信息变更表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSupplierCertificateChange(String id) throws BaseException;

	/**
	 * 删除供应商资质信息变更表表实例
	 * @param supplierCertificateChange 供应商资质信息变更表表实例
	 * @throws BaseException 
	 */
	abstract void deleteSupplierCertificateChange(SupplierCertificateChange supplierCertificateChange) throws BaseException;

	/**
	 * 删除供应商资质信息变更表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteSupplierCertificateChanges(String[] id) throws BaseException;

	/**
	 * 获得所有供应商资质信息变更表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierCertificateChangeList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商资质信息变更表表数据集
	 * @param supplierCertificateChange 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierCertificateChangeList(SupplierCertificateChange supplierCertificateChange) throws BaseException ;
	
	/**
	 * 获得所有供应商资质信息变更表表数据集
	 * @param rollPage 分页对象
	 * @param supplierCertificateChange 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierCertificateChangeList(RollPage rollPage, SupplierCertificateChange supplierCertificateChange)
			throws BaseException;

}