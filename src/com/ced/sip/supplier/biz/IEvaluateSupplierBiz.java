package com.ced.sip.supplier.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.supplier.entity.SupplierEvaluations;

public interface IEvaluateSupplierBiz {

	/**
	 * 根据主键获得供应商考核主表信息表实例
	 * @param id 主键
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluations getSupplierEvaluations(Long id)
			throws BaseException;

	/**
	 * 获得供应商考核主表信息表实例
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public abstract SupplierEvaluations getSupplierEvaluations(
			SupplierEvaluations supplierEvaluations) throws BaseException;

	/**
	 * 添加供应商考核主表信息信息
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public abstract void saveSupplierEvaluations(
			SupplierEvaluations supplierEvaluations) throws BaseException;

	/**
	 * 更新供应商考核主表信息表实例
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public abstract void updateSupplierEvaluations(
			SupplierEvaluations supplierEvaluations) throws BaseException;

	/**
	 * 删除供应商考核主表信息表实例
	 * @param id 主键数组
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluations(String id)
			throws BaseException;

	/**
	 * 删除供应商考核主表信息表实例
	 * @param supplierEvaluations 供应商考核主表信息表实例
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluations(
			SupplierEvaluations supplierEvaluations) throws BaseException;

	/**
	 * 删除供应商考核主表信息表实例
	 * @param id 主键数组
	 * @author zy 2014-11-10
	 * @throws BaseException 
	 */
	public abstract void deleteSupplierEvaluationss(String[] id)
			throws BaseException;

	/**
	 * 获得所有供应商考核主表信息表数据集
	 * @param rollPage 分页对象
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluationsList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有供应商考核主表信息表数据集
	 * @param supplierEvaluations 查询参数对象
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluationsList(
			SupplierEvaluations supplierEvaluations) throws BaseException;

	/**
	 * 获得所有供应商考核主表信息表数据集
	 * @param rollPage 分页对象
	 * @param supplierEvaluations 查询参数对象
	 * @author zy 2014-11-10
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getSupplierEvaluationsList(RollPage rollPage,
			SupplierEvaluations supplierEvaluations) throws BaseException;

}