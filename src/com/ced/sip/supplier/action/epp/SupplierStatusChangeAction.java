package com.ced.sip.supplier.action.epp;

import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.biz.ISupplierStatusChangeBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierStatusChange;
/** 
 * 类名称：SupplierStatusChangeAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-18
 */
public class SupplierStatusChangeAction extends BaseAction {

	// 供应商状态变更 
	private ISupplierStatusChangeBiz iSupplierStatusChangeBiz;
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	
	// 供应商状态变更
	private SupplierStatusChange supplierStatusChange;
	
	/**
	 * 查看供应商状态变更信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierStatusChange_base() throws BaseException {
		
		try{
			String supplierId=this.getRequest().getParameter("supplierId");
			this.getRequest().setAttribute("supplierId", supplierId);
		} catch (Exception e) {
			log.error("查看供应商状态变更信息列表错误！", e);
			throw new BaseException("查看供应商状态变更信息列表错误！", e);
		}	
			
		return "view2" ;
				
	}
	
	/**
	 * 查看供应商状态变更信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findSupplierStatusChange() throws BaseException {
		
		try{
			supplierStatusChange=new SupplierStatusChange();
			String supplierId=this.getRequest().getParameter("supplierId");
			supplierStatusChange.setSupplierId(Long.parseLong(supplierId));
			List<SupplierStatusChange> list=this.iSupplierStatusChangeBiz.getSupplierStatusChangeList(this.getRollPageDataTables(), supplierStatusChange);
			for(SupplierStatusChange supplierStatusChange:list){
				supplierStatusChange.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(supplierStatusChange.getWriter()));
				supplierStatusChange.setOldStatusCn(BaseDataInfosUtil.convertDictCodeToName(supplierStatusChange.getOldStatus(), DictStatus.COMMON_DICT_TYPE_1713));
				supplierStatusChange.setNewStatusCn(BaseDataInfosUtil.convertDictCodeToName(supplierStatusChange.getNewStatus(), DictStatus.COMMON_DICT_TYPE_1713));
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看供应商状态变更信息列表错误！", e);
			throw new BaseException("查看供应商状态变更信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存供应商状态变更信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveSupplierStatusChangeInit() throws BaseException {
		try{
			Long supplierId=Long.parseLong(this.getRequest().getParameter("supplierId"));
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			SupplierInfo supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
			this.getRequest().setAttribute("oldStatus",supplierInfo.getStatus());
			this.getRequest().setAttribute("oldStatusCn",BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getStatus(), DictStatus.COMMON_DICT_TYPE_1713));
			this.getRequest().setAttribute("now",DateUtil.getCurrentDateTime());
			this.getRequest().setAttribute("writerCn",UserRightInfoUtil.getChineseName(this.getRequest()));
			this.getRequest().setAttribute("writer",UserRightInfoUtil.getUserName(this.getRequest()));
			this.getRequest().setAttribute("supplierId",supplierId);
			this.getRequest().setAttribute("comId",comId);
			this.getRequest().setAttribute("supplierName",supplierInfo.getSupplierName());
			this.getRequest().setAttribute("statusMap", BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713));
			
		} catch (Exception e) {
			log("保存供应商状态变更信息初始化错误！", e);
			throw new BaseException("保存供应商状态变更信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存供应商状态变更信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveSupplierStatusChange() throws BaseException {
		
		try{
			this.iSupplierStatusChangeBiz.saveSupplierStatusChange(supplierStatusChange);
			// 保存附件
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					supplierStatusChange, supplierStatusChange.getSscId(),
					AttachmentStatus.ATTACHMENT_CODE_606, UserRightInfoUtil
							.getUserName(this.getRequest())));
			this.iSupplierStatusChangeBiz.updateSupplierInfoByColumn("status", supplierStatusChange.getNewStatus(), supplierStatusChange.getSupplierId()); //更新供应商状态为合格供应商
			this.getRequest().setAttribute("message", "变更成功");
			this.getRequest().setAttribute("operModule", "供应商状态变更");
		} catch (Exception e) {
			log("保存供应商状态变更信息错误！", e);
			throw new BaseException("保存供应商状态变更信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	
	/**
	 * 查看供应商状态变更明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierStatusChangeDetail() throws BaseException {
		
		try{
			supplierStatusChange=this.iSupplierStatusChangeBiz.getSupplierStatusChange(supplierStatusChange.getSscId() );
			supplierStatusChange.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(supplierStatusChange.getWriter()));
			supplierStatusChange.setOldStatusCn(BaseDataInfosUtil.convertDictCodeToName(supplierStatusChange.getOldStatus(), DictStatus.COMMON_DICT_TYPE_1713));
			supplierStatusChange.setNewStatusCn(BaseDataInfosUtil.convertDictCodeToName(supplierStatusChange.getNewStatus(), DictStatus.COMMON_DICT_TYPE_1713));
			supplierStatusChange.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( supplierStatusChange.getSscId(), AttachmentStatus.ATTACHMENT_CODE_606 ) ) , "0", this.getRequest() ) ) ;
			
		} catch (Exception e) {
			log("查看供应商状态变更明细信息错误！", e);
			throw new BaseException("查看供应商状态变更明细信息错误！", e);
		}
		return "detail2";
		
	}

	public ISupplierStatusChangeBiz getiSupplierStatusChangeBiz() {
		return iSupplierStatusChangeBiz;
	}

	public void setiSupplierStatusChangeBiz(ISupplierStatusChangeBiz iSupplierStatusChangeBiz) {
		this.iSupplierStatusChangeBiz = iSupplierStatusChangeBiz;
	}

	public SupplierStatusChange getSupplierStatusChange() {
		return supplierStatusChange;
	}

	public void setSupplierStatusChange(SupplierStatusChange supplierStatusChange) {
		this.supplierStatusChange = supplierStatusChange;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	
}
