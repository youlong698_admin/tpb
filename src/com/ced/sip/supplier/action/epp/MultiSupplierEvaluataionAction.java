package com.ced.sip.supplier.action.epp;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.apache.commons.beanutils.BeanUtils;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.CodeGetByConditionsUtil;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ListCompare;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.IEvaluateSupplierBiz;
import com.ced.sip.supplier.biz.IEvaluateSupplierRelationBiz;
import com.ced.sip.supplier.biz.IEvaluationIndexBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.EvaluateSupplierView;
import com.ced.sip.supplier.entity.EvaluationIndex;
import com.ced.sip.supplier.entity.SupplierEvaluateIndex;
import com.ced.sip.supplier.entity.SupplierEvaluateResult;
import com.ced.sip.supplier.entity.SupplierEvaluateSup;
import com.ced.sip.supplier.entity.SupplierEvaluateUser;
import com.ced.sip.supplier.entity.SupplierEvaluationUserSup;
import com.ced.sip.supplier.entity.SupplierEvaluations;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.system.entity.Users;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
import com.ced.sip.workflow.biz.IWfTextBiz;
import com.ced.sip.workflow.entity.WfText;

public class MultiSupplierEvaluataionAction extends BaseAction {
	@Autowired
	private SnakerEngineFacets facets;
	// EvaluateSupplier 服务类
	private IEvaluateSupplierBiz iEvaluateSupplierBiz  ;
	// 供应商考核关联表信息 服务类
	private IEvaluateSupplierRelationBiz iEvaluateSupplierRelationBiz;
	// 考核指标信息 服务类
	private IEvaluationIndexBiz iEvaluationIndexBiz  ;
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz  ;
	//流程审批正文服务类
	private IWfTextBiz iWfTextBiz;
	//用户信息 服务类
	private IUsersBiz iUsersBiz;
	// 供应商考核人信息 实例
	private SupplierEvaluateUser supplierEvaluateUser;
	// 被考核供应商信息 实例
	private SupplierEvaluateSup supplierEvaluateSup;
	// 供应商考核指标信息 实例
	private SupplierEvaluateIndex supplierEvaluateIndex;
	// 供应商考核结果信息 实例
	private SupplierEvaluateResult supplierEvaluateResult;
	// EvaluateSupplier 实例
	private SupplierEvaluations supplierEvaluations ;
	// 考核指标信息 实体类
	private EvaluationIndex evaluationIndex ;
	// 供应商基本信息 实例
	private SupplierInfo supplierInfo ;
	//用户信息 实体类
	private Users user;
	//考核人和供应商关联 实体类
	private SupplierEvaluationUserSup supplierEvaluationUserSup;
	// 临时参数
	private String tempSupEvaInfo;
	//考核指标使用参数
	private String strIndex;
	// 基准分之和
	private Double refPointsSum = 0.0;
	// 调整分之和
    private Double adjPointsSum = 0.0;
    private String adjPointsSumStr = "";
	// 考核指标信息List
	private List<SupplierEvaluateIndex> evaIndexList;
	//被考核供应商信息List
	private List<SupplierEvaluateSup> mesSupList;
	//考核人信息List
	private List<SupplierEvaluateUser> mesUserList;
	/**
	 * 查看供应商考核信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierEvaluataion() throws BaseException {
		
		return VIEW ;
	}
	public String viewToSupplierEvaluataion() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());

			if(supplierEvaluations==null)
			{
				supplierEvaluations=new SupplierEvaluations();
			}
			String seDescribe=this.getRequest().getParameter("seDescribe");
			supplierEvaluations.setSeDescribe(seDescribe);
			String timeStart=this.getRequest().getParameter("timeStart");
			String timeEnd=this.getRequest().getParameter("timeEnd");
			
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); 
			if(StringUtil.isNotBlank(timeStart))
			{
				supplierEvaluations.setTimeStart(sdf.parse(timeStart));
			}
			if(StringUtil.isNotBlank(timeEnd))
			{
				supplierEvaluations.setTimeEnd(sdf.parse(timeEnd));
			}
			supplierEvaluations.setComId(comId);
			this.setListValue(this.iEvaluateSupplierBiz.getSupplierEvaluationsList(this.getRollPageDataTables(), supplierEvaluations));
			if(this.getListValue()!=null&&this.getListValue().size()>0){
				for(int i=0;i<this.getListValue().size();i++){
					EvaluateSupplierView evaSupplierView = (EvaluateSupplierView) this.getListValue().get(i);
					SupplierEvaluations supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(evaSupplierView.getSeId());
					if(!"2".equals(supplierEvaluations.getStatus())){
						//判断考核是否全部完成
						supplierEvaluateSup = new SupplierEvaluateSup();
						supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
						List<SupplierEvaluateSup> sesList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
						//统计完全提交状态
						Integer isAllSubmit = 0;
						for(SupplierEvaluateSup ses:sesList){
							if(StringUtil.isNotBlank(ses.getStatus())){
								if("1".equals(ses.getStatus())){
									isAllSubmit++;
									supplierInfo = this.iSupplierInfoBiz.getSupplierInfo(ses.getSupplierId());
									supplierInfo.setSupplierLevel(BaseDataInfosUtil.getDifferentRankByPoints(ses.getSupSumPoints()));
									this.iSupplierInfoBiz.updateSupplierInfo(supplierInfo);
								}
							}
						}
						if(isAllSubmit.equals(sesList.size())){
							supplierEvaluations.setStatus("2");
						}else{
							Date timeEnds = sdf.parse(sdf.format(supplierEvaluations.getTimeEnd()));
							Date nowTime = sdf.parse(sdf.format(new Date()));
							if(nowTime.getTime()>timeEnds.getTime()){
								supplierEvaluations.setStatus("3");
							}
						}
						this.iEvaluateSupplierBiz.updateSupplierEvaluations(supplierEvaluations);
					}
				}
			}
			setProcessList(this.getListValue(), WorkFlowStatus.SupplierEvaluate_Work_Item,comId);
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商考核信息列表错误！", e);
			throw new BaseException("查看供应商考核信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存考核信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluateSupplierInit() throws BaseException {
		
		try{
			supplierEvaluations = new SupplierEvaluations();
			supplierEvaluations.setSeCode(CodeGetByConditionsUtil.generateCodeByConditions("EL", "yyyyMMdd", "0000", "SEQ_SUPPLIER_EVA_ID"));
		} catch (Exception e) {
			log("保存考核信息初始化错误！", e);
			throw new BaseException("保存考核信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存考核信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluateSupplier() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			String[] userIds = this.getRequest().getParameter("userIds").split(",");
			String[] supIds = this.getRequest().getParameter("supplierIds").split(",");
			if(supplierEvaluations.getSeCode()!=null){
				// 保存EvaluateSupplier信息
				supplierEvaluations.setWriter(UserRightInfoUtil.getChineseName(this.getRequest()));
				supplierEvaluations.setWriteDate(DateUtil.getCurrentDateTime());
				supplierEvaluations.setComId(comId);
				this.iEvaluateSupplierBiz.saveSupplierEvaluations(supplierEvaluations);
				//保存考核人
				if(StringUtil.isNotBlank(userIds)){
					for(int i=0;i<userIds.length;i++){
						if(StringUtil.isNotBlank(userIds[i])){
							supplierEvaluateUser = new SupplierEvaluateUser();
							supplierEvaluateUser.setUserId(Long.parseLong(userIds[i]));
							supplierEvaluateUser.setUserName(BaseDataInfosUtil.convertUserIdToEnnName(Long.parseLong(userIds[i])));
							supplierEvaluateUser.setUserNameCn(BaseDataInfosUtil.convertUserIdToChnName(Long.parseLong(userIds[i])));
							supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
							supplierEvaluateUser.setComId(comId);
							this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateUser(supplierEvaluateUser);
						}
					}
				}
				//保存被考核供应商
				if(StringUtil.isNotBlank(supIds)){
					for(int i=0;i<supIds.length;i++){
						if(StringUtil.isNotBlank(supIds[i])){
							supplierEvaluateSup = new SupplierEvaluateSup();
							supplierEvaluateSup.setSupplierId(Long.parseLong(supIds[i]));
							supplierInfo = this.iSupplierInfoBiz.getSupplierInfo(Long.parseLong(supIds[i]));
							supplierEvaluateSup.setSupplierName(supplierInfo.getSupplierName());
							supplierEvaluateSup.setContactPerson(supplierInfo.getContactPerson());
							supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
							supplierEvaluateSup.setComId(comId);
							this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateSup(supplierEvaluateSup);
						}
					}
				}
			}
			
			this.getRequest().setAttribute("message", "保存成功");
			this.getRequest().setAttribute("operModule", "保存考核信息");
		} catch (Exception e) {
			log("保存考核信息错误！", e);
			throw new BaseException("保存考核信息错误！", e);
		}
		return ADD;
		
	}
	
	/**
	 * 修改考核信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierInit() throws BaseException {
		
		try{
			// 取得考核明细信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			
			supplierEvaluateUser=new SupplierEvaluateUser();
			supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateUser> supplierEvaluateUserList=this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
			String userIds="",userName="";
			for(SupplierEvaluateUser evaluateUser:supplierEvaluateUserList)
			{
				userName=BaseDataInfosUtil.convertUserIdToChnName(evaluateUser.getUserId());
				userIds+=evaluateUser.getUserId()+":"+userName+",";
			}
			if(StringUtil.isNotBlank(userIds))
			{
				this.getRequest().setAttribute("returnValues", userIds.substring(0,userIds.lastIndexOf(",")));
			}
			
			supplierEvaluateSup=new SupplierEvaluateSup();
			supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateSup> supplierEvaluateSupList=this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
			String supplierIds="";
			for(SupplierEvaluateSup evaluateSup:supplierEvaluateSupList)
			{
				supplierInfo=iSupplierInfoBiz.getSupplierInfo(evaluateSup.getSupplierId());
				supplierIds+=evaluateSup.getSupplierId()+":"+supplierInfo.getSupplierName()+",";
			}
			if(StringUtil.isNotBlank(supplierIds))
			{
				this.getRequest().setAttribute("returnValuesSupplier", supplierIds.substring(0,supplierIds.lastIndexOf(",")));
			}
		} catch (Exception e) {
			log("保存考核信息初始化错误！", e);
			throw new BaseException("保存考核信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改考核信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplier() throws BaseException {
		
		try{
			this.iEvaluateSupplierBiz.updateSupplierEvaluations(supplierEvaluations);
			
			String[] userIds = this.getRequest().getParameter("userIds").split(",");
			String[] supIds = this.getRequest().getParameter("supplierIds").split(",");
			List<String> list;
			
			supplierEvaluateUser=new SupplierEvaluateUser();
			supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateUser> supplierEvaluateUserList=iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
			String[] oldUserIds=new String[supplierEvaluateUserList.size()];
			int j=0;
			for(SupplierEvaluateUser sUser:supplierEvaluateUserList){
				oldUserIds[j]=sUser.getUserId().toString();
				j++;
			}
			//原先的考核人有，但是新的考核人没有
            list=ListCompare.compare(userIds, oldUserIds);
			for(String str:list){
				iEvaluateSupplierRelationBiz.deleteSupplierEvaluateUsersByUserIdAndSeId(str, supplierEvaluations.getSeId());
			}
			//原先的考核人没有，但是新的考核人有
			list=ListCompare.compare(oldUserIds, userIds);
			for(String str:list){
				supplierEvaluateUser = new SupplierEvaluateUser();
				supplierEvaluateUser.setUserId(Long.parseLong(str));
				supplierEvaluateUser.setUserName(BaseDataInfosUtil.convertUserIdToEnnName(Long.parseLong(str)));
				supplierEvaluateUser.setUserNameCn(BaseDataInfosUtil.convertUserIdToChnName(Long.parseLong(str)));
				supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
				supplierEvaluateUser.setComId(supplierEvaluations.getComId());
				this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateUser(supplierEvaluateUser);
			}
			
			//处理被考核供应商
			supplierEvaluateSup=new SupplierEvaluateSup();
			supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateSup> supplierEvaluateSupList=iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
			String[] oldSupplierIds=new String[supplierEvaluateSupList.size()];
			j=0;
			for(SupplierEvaluateSup sup:supplierEvaluateSupList){
				oldSupplierIds[j]=sup.getSupplierId().toString();
				j++;
			}
			//原先的被考核供应商有，但是新的被考核供应商没有
			list=ListCompare.compare(supIds, oldSupplierIds);
			for(String str:list){
				iEvaluateSupplierRelationBiz.deleteSupplierEvaluationUserSupBySupplierIdAndSeId(str, supplierEvaluations.getSeId());
			}
			//原先的被考核供应商没有，但是新的被考核供应商有
			list=ListCompare.compare(oldSupplierIds, supIds);
			for(String str:list){
				supplierEvaluateSup = new SupplierEvaluateSup();
				supplierEvaluateSup.setSupplierId(Long.parseLong(str));
				supplierInfo = this.iSupplierInfoBiz.getSupplierInfo(Long.parseLong(str));
				supplierEvaluateSup.setSupplierName(supplierInfo.getSupplierName());
				supplierEvaluateSup.setContactPerson(supplierInfo.getContactPerson());
				supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
				supplierEvaluateSup.setComId(supplierEvaluations.getComId());
				this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateSup(supplierEvaluateSup);
			}
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改保存考核信息");
		} catch (Exception e) {
			log("保存考核信息错误！", e);
			throw new BaseException("保存考核信息错误！", e);
		}
		return updateEvaluateSupplierInit();
		
	}
	
	/**
	 * 删除供应商考核信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteEvaluateSupplier() throws BaseException {
		try{
			if(StringUtil.isNotBlank(tempSupEvaInfo)){
				String[] supIdArr = tempSupEvaInfo.split(",");
				// 删除EvaluateSupplier信息
				this.iEvaluateSupplierBiz.deleteSupplierEvaluationss(supIdArr);
				for(int i=0;i<supIdArr.length;i++){
					if(StringUtil.isNotBlank(supIdArr[i])){
						//删除考核人信息
						supplierEvaluateUser = new SupplierEvaluateUser();
						supplierEvaluateUser.setSeId(Long.parseLong(supIdArr[i]));
						List<SupplierEvaluateUser> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
						if(seuList!=null&&seuList.size()>0){
							for(SupplierEvaluateUser seu:seuList){
								this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateUser(seu);
							}
						}
						//删除被考核供应商信息
						supplierEvaluateSup = new SupplierEvaluateSup();
						supplierEvaluateSup.setSeId(Long.parseLong(supIdArr[i]));
						List<SupplierEvaluateSup> sepList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
						if(sepList!=null&&sepList.size()>0){
							for(SupplierEvaluateSup sep:sepList){
								this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateSup(sep);
							}
						}
						//删除考核指标信息
						supplierEvaluateIndex = new SupplierEvaluateIndex();
						supplierEvaluateIndex.setSeId(Long.parseLong(supIdArr[i]));
						List<SupplierEvaluateIndex> seiList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supplierEvaluateIndex);
						for(SupplierEvaluateIndex sei:seiList){
							this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateIndex(sei);
						}
						//删除考核结果信息
						supplierEvaluateResult = new SupplierEvaluateResult();
						supplierEvaluateResult.setSeId(Long.parseLong(supIdArr[i]));
						List<SupplierEvaluateResult> serList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateResultList(supplierEvaluateResult);
						for(SupplierEvaluateResult ser:serList){
							this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateResult(ser);
						}
						//删除供应商和考核人关联关系信息
						supplierEvaluationUserSup = new SupplierEvaluationUserSup();
						supplierEvaluationUserSup.setSeId(Long.parseLong(supIdArr[i]));
						List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
						for(SupplierEvaluationUserSup seus:seusList){
							this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluationUserSup(seus);
						}
					} 
				}
			}
		} catch (Exception e) {
			log("删除供应商考核信息错误！", e);
			throw new BaseException("删除供应商考核信息错误！", e);
		}
		return DELETE;
		
	}
	
	/**
	 * 查看供应商考核明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierEvaluataionDetail() throws BaseException {
		
		try{
			supplierEvaluations=this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations.getSeId());
		} catch (Exception e) {
			log("查看供应商考核明细信息错误！", e);
			throw new BaseException("查看供应商考核明细信息错误！", e);
		}
		return DETAIL;
		
	}
	
	/**
	 * 供应商考核提交前校验指标分值
	 * @return
	 * @throws BaseException 
	 */
	public String caculateEvaIndexPoint() throws BaseException {
		
		try{
			this.ajaxHeadInit();
			PrintWriter out = this.getResponse().getWriter();
			SupplierEvaluateIndex supEvaluateIndex = new SupplierEvaluateIndex();
			supEvaluateIndex.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateIndex> seiList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supEvaluateIndex);
			Double evaIndexPoint = 0.0;
			if(seiList==null||seiList.size()==0){
				out.println(0);
			}else if(seiList!=null&&seiList.size()>0){
				for(SupplierEvaluateIndex sei:seiList){
					evaIndexPoint += sei.getAdjustPoints();
				}
				out.println(StringUtil.formateNumberTo(evaIndexPoint));
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			log("供应商考核提交前校验指标分值错误！", e);
			throw new BaseException("供应商考核提交前校验指标分值错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 供应商考核提交前校验供应商和考核人的关联情况
	 * @return
	 * @throws BaseException 
	 */
	public String validateEvaIndexSupRefUser() throws BaseException {
		
		try{
			this.ajaxHeadInit();
			PrintWriter out = this.getResponse().getWriter();
			//校验供应商和考核人是否已经都关联
			supplierEvaluateSup = new SupplierEvaluateSup();
			supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateSup> sesList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
			String notRefSuppliers = "";
			Integer notRefSupNum = 0;
			for(SupplierEvaluateSup ses:sesList){
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
				supplierEvaluationUserSup.setSesId(ses.getSesId());
				List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				if(seusList.size()==0){
					notRefSupNum++;
					notRefSuppliers += ses.getSupplierName()+",";
				}
			}
			//全部关联标识
			if(notRefSupNum!=0){
				out.println(StringUtil.subStringLastOfSeparator(notRefSuppliers, ','));
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			log("供应商考核提交前校验供应商和考核人的关联情况错误！", e);
			throw new BaseException("供应商考核提交前校验供应商和考核人的关联情况错误！", e);
		}
		return null;
		
	}
	public String validateEvaIndexUserRefSup() throws BaseException {
		
		try{
			this.ajaxHeadInit();
			PrintWriter out = this.getResponse().getWriter();
			//校验供应商和考核人是否已经都关联
			supplierEvaluateUser = new SupplierEvaluateUser();
			supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateUser> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
			Integer notRefSupNum = 0;
			for(SupplierEvaluateUser seu:seuList){
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
				supplierEvaluationUserSup.setSeuId(seu.getSeuId());
				List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				if(seusList.size()==0){
					notRefSupNum++;
				}
			}
			//全部关联标识
			out.println(notRefSupNum);
			out.flush();
			out.close();
		} catch (Exception e) {
			log("供应商考核提交前校验供应商和考核人的关联情况错误！", e);
			throw new BaseException("供应商考核提交前校验供应商和考核人的关联情况错误！", e);
		}
		return null;
		
	}
	/**
	 * 供应商考核提交
	 * @return
	 * @throws BaseException 
	 */
	public String saveSubmitSupplierEvaluation() throws BaseException {
		
		try{
			String processId = getRequest().getParameter("processId");
			String orderNo = getRequest().getParameter("orderNo");
			String taskId = getRequest().getParameter("taskId");
			String newOrderNo=WorkFlowStatus.SE_WorkFlow_Type+orderNo;
			String userName = UserRightInfoUtil.getUserName(getRequest());//当前任务发起人
			Map<String, Object> args = new HashMap<String, Object>();//考核人待办处理信息集合
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			//发送供应商考核代办信息
			if(supplierEvaluations!=null){
				if(!StringUtil.isBlank(taskId)){
					args.put("buyRemark",  supplierEvaluations.getSeDescribe());
					facets.execute(taskId, UserRightInfoUtil.getUserName(getRequest()), null);
				}else{
					supplierEvaluateUser = new SupplierEvaluateUser();
					supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
					List<SupplierEvaluateUser> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
					String userGroups = "";
					args.put("initiator.operator", userName);
					args.put("snaker.orderNo", newOrderNo);
					if(seusList!=null&&seusList.size()>0){
						for(SupplierEvaluateUser seu:seusList){
							userGroups += seu.getUserName()+",";
						}
					}
					userGroups = StringUtil.subStringLastOfSeparator(userGroups, ',');
					args.put("viewAndEvaluateSupplier.operator",  userGroups);
					args.put("buyRemark",  supplierEvaluations.getSeDescribe());
					facets.startAndExecute(processId, userName, args);
				}
				//保存流程审批正文
   				this.saveWorkFlowText(newOrderNo, "",supplierEvaluations.getSeDescribe());
   				
				supplierEvaluations=this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations.getSeId());
				supplierEvaluations.setStatus("1");
				this.iEvaluateSupplierBiz.updateSupplierEvaluations(supplierEvaluations);
			}
		} catch (Exception e) {
			log("供应商考核提交信息错误！", e);
			throw new BaseException("供应商考核提交信息错误！", e);
		}
		return "submit_EvaSup";
		
	}
	/**
	 * 保存流程审批请示正文
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void saveWorkFlowText(String orderNo,String description,String wfName) throws BaseException {
           try{
        	   WfText wfText = new WfText();
        		List<Order> orderList = facets.getEngine().query().getActiveOrders(new QueryFilter().setOrderNo(orderNo));
        		if(orderList!=null&&orderList.size()>0){
        			wfText.setOrderId(orderList.get(0).getId());
        			List<WfText> wftList = this.iWfTextBiz.getWfTextList(wfText);
        			for(WfText wft:wftList){
        				this.iWfTextBiz.deleteWfText(wft);
        			}
        		}
        		wfText.setWfText(description);
        		wfText.setWfName(wfName);
        		this.iWfTextBiz.saveWfText(wfText);
		} catch (Exception e) {
			log.error("保存流程审批请示正文错误！", e);
			throw new BaseException("保存流程审批请示正文错误！", e);
		}
		
	}
	/*********************************考核指标信息、被考核供应商和考核人信息增加、修改、删除**************************************/
	
	/**
	 * 查看供应商考核索引页
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluateSupplierIndex() throws BaseException {
		
		try{
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
		} catch (Exception e) {
			log("查看供应商考核明细索引页错误！", e);
			throw new BaseException("查看供应商考核明细索引页错误！", e);
		}
		return INDEX;
		
	}
	
	/**
	 * 查看供应商考核索引页Tab
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluateSupplierIndexTab() throws BaseException {
		
		try{
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
		} catch (Exception e) {
			log("查看供应商考核明细索引页错误！", e);
			throw new BaseException("查看供应商考核明细索引页错误！", e);
		}
		return "index_tab";
		
	}

	/**
	 * 编辑供应商考核指标明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluateSupplierEvaIndex() throws BaseException {
		
		try{
			// 取得供应商考核明细信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			//获得增加指标信息
			SupplierEvaluateIndex supEvaluateIndex = new SupplierEvaluateIndex();
			supEvaluateIndex.setSeId(supplierEvaluations.getSeId());
			evaIndexList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supEvaluateIndex);
			if(evaIndexList!=null&&evaIndexList.size()>0){
				for(SupplierEvaluateIndex sei:evaIndexList){
					if(StringUtil.isNotBlank(sei.getPoints())){
						refPointsSum += sei.getPoints();
						adjPointsSum +=sei.getAdjustPoints();
					}
				}
			}
			adjPointsSumStr = StringUtil.formateNumberTo(adjPointsSum);
		} catch (Exception e) {
			log("编辑供应商考核指标明细信息错误！", e);
			throw new BaseException("编辑供应商考核指标明细信息错误！", e);
		}
		return "index_modifyInit";
	}
	
	/**
	 * 新增供应商考核指标信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluateSupplierIndexInit() throws BaseException {
		try{
			if(StringUtil.isNotBlank(supplierEvaluations.getSeId())){
				if(StringUtil.isNotBlank(strIndex)){
					String[] supIndexArr = strIndex.split(",");
					if(supIndexArr!=null){
						for(int i=0;i<supIndexArr.length;i++){
							if(StringUtil.isNotBlank(supIndexArr[i])){
								String[] indexArr = supIndexArr[i].split(":");
								if(StringUtil.isNotBlank(indexArr[0])){
									evaluationIndex = this.iEvaluationIndexBiz.getEvaluationIndex(Long.parseLong(indexArr[0]));
									supplierEvaluateIndex = new SupplierEvaluateIndex();
									BeanUtils.copyProperties(supplierEvaluateIndex, evaluationIndex);
									supplierEvaluateIndex.setPoints(evaluationIndex.getReferencePoints());
									supplierEvaluateIndex.setAdjustPoints(evaluationIndex.getReferencePoints());
									supplierEvaluateIndex.setSeId(supplierEvaluations.getSeId());
									this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateIndex(supplierEvaluateIndex);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log("新增供应商考核指标信息错误！", e);
			throw new BaseException("新增供应商考核指标信息错误！", e);
		}
		return "index_modify";
		
	}
	
	/**
	 * 更新供应商考核指标信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierIndex() throws BaseException {
		try{
			if(StringUtil.isNotBlank(supplierEvaluations.getSeId())){
				SupplierEvaluateIndex supEvaluateIndex = new SupplierEvaluateIndex();
				supEvaluateIndex.setSeId(supplierEvaluations.getSeId());
				List<SupplierEvaluateIndex> seiList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supEvaluateIndex);
				if(seiList!=null&&seiList.size()>0){
					if(StringUtil.isNotBlank(strIndex)){
						String[] supIndexArr = strIndex.split(",");
						if(supIndexArr!=null){
							for(int i=0;i<supIndexArr.length;i++){
								if(StringUtil.isNotBlank(supIndexArr[i])){
									String[] indexArr = supIndexArr[i].split(":");
									for(SupplierEvaluateIndex sei:seiList){
										if(sei.getEiId().equals(Long.parseLong(indexArr[0]))){
											this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateIndex(sei);
										}
									}
									//保存新的考核指标数据
									if(StringUtil.isNotBlank(indexArr[0])){
										evaluationIndex = this.iEvaluationIndexBiz.getEvaluationIndex(Long.parseLong(indexArr[0]));
										supplierEvaluateIndex = new SupplierEvaluateIndex();
										BeanUtils.copyProperties(supplierEvaluateIndex, evaluationIndex);
										supplierEvaluateIndex.setPoints(evaluationIndex.getReferencePoints());
										supplierEvaluateIndex.setAdjustPoints(evaluationIndex.getReferencePoints());
										supplierEvaluateIndex.setSeId(supplierEvaluations.getSeId());
										this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateIndex(supplierEvaluateIndex);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log("更新供应商考核指标信息错误！", e);
			throw new BaseException("更新供应商考核指标信息错误！", e);
		}
		return "index_modify";
		
	}
	
	/**
	 * 删除供应商考核指标信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteEvaluateSupplierIndex() throws BaseException {
		try{
			if(StringUtil.isNotBlank(strIndex)){
				String[] supIndexArr = strIndex.split(",");
				this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateIndexs(supIndexArr);
			}
		} catch (Exception e) {
			log("删除供应商考核指标信息错误！", e);
			throw new BaseException("删除供应商考核指标信息错误！", e);
		}
		return "index_modify";
		
	}
	
	/**
	 * 保存供应商考核指标调整信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluateSupplierIndex() throws BaseException {
		try{
			if(StringUtil.isNotBlank(supplierEvaluations.getSeId())){
				//删除考核指标信息
				SupplierEvaluateIndex supEvaluateIndex = new SupplierEvaluateIndex();
				supEvaluateIndex.setSeId(supplierEvaluations.getSeId());
				List<SupplierEvaluateIndex> seiList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supEvaluateIndex);
				if(seiList!=null&&seiList.size()>0){
					for(SupplierEvaluateIndex sei:seiList){
						this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluateIndex(sei);
					}
				}
				//保存考核指标信息（调整指标分值后）
				if(evaIndexList!=null&&evaIndexList.size()>0){
					for(SupplierEvaluateIndex evaIndex:evaIndexList){
						if(evaIndex!=null){
							SupplierEvaluateIndex seIndex = new SupplierEvaluateIndex();
							BeanUtils.copyProperties(seIndex, evaIndex);
							seIndex.setSeId(supplierEvaluations.getSeId());
							this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateIndex(seIndex);
						}
					}
				}
			}
			
			this.getRequest().setAttribute("message", "保存成功");
			this.getRequest().setAttribute("operModule", "保存供应商考核指标信息");
		} catch (Exception e) {
			log("保存供应商考核指标信息错误！", e);
			throw new BaseException("保存供应商考核指标信息错误！", e);
		}
		return "index_modify";
		
	}
	
	/**
	 * 修改供应商考核指标信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierEvaIndexInit() throws BaseException {
		
		try{
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
		} catch (Exception e) {
			log("修改供应商考核指标信息初始化错误！", e);
			throw new BaseException("修改供应商考核指标信息初始化错误！", e);
		}
		return "index_modifyInit";
		
	}
	
	/**
	 * 修改被考核供应商信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierSupInit() throws BaseException {
		
		try{
			// 取得考核明细信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			supplierEvaluateSup = new SupplierEvaluateSup();
			supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
			this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup));
			for(Iterator iter=this.getListValue().iterator();iter.hasNext();){
				SupplierEvaluateSup ses = (SupplierEvaluateSup) iter.next();
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSesId(ses.getSesId());
				supplierEvaluationUserSup.setSeId(ses.getSeId());
				List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				String evaUsers = "";//考核人集合
				String evaUserIds = "";//考核人Id集合
				String evaUserNames = "";//考核人姓名集合
				for(SupplierEvaluationUserSup seus:seusList){
					evaUserNames += seus.getUserNameCn()+",";
					evaUsers += seus.getSeuId()+":"+seus.getUserNameCn()+",";
					evaUserIds+=seus.getSeuId()+",";
				}
				ses.setEvaUserNames(StringUtil.subStringLastOfSeparator(evaUserNames, ','));
				ses.setEvaUsers(StringUtil.subStringLastOfSeparator(evaUsers, ','));
				ses.setEvaUserIds(StringUtil.subStringLastOfSeparator(evaUserIds, ','));
			}
		} catch (Exception e) {
			log("修改被考核供应商信息初始化错误！", e);
			throw new BaseException("修改被考核供应商信息初始化错误！", e);
		}
		return "sup_modifyInit";
		
	}
	
	/**
	 * 修改被考核供应商信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierSup() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluationUserSupBySeId(supplierEvaluations.getSeId());
			SupplierEvaluationUserSup seUserSup =null;
			for(SupplierEvaluateSup ses:mesSupList){
						if(StringUtil.isNotBlank(ses.getEvaUsers())){
							String[] strArr = ses.getEvaUsers().split(",");
							for(int i=0;i<strArr.length;i++){
								if(StringUtil.isNotBlank(strArr[i])){
									String[] idNames = strArr[i].split(":");
									seUserSup = new SupplierEvaluationUserSup();
									seUserSup.setSeuId(Long.parseLong(idNames[0]));
									seUserSup.setUserNameCn(idNames[1]);
									seUserSup.setSupplierName(ses.getSupplierName());
									seUserSup.setSesId(ses.getSesId());
									seUserSup.setSeId(supplierEvaluations.getSeId());
									seUserSup.setComId(comId);
									this.iEvaluateSupplierRelationBiz.saveSupplierEvaluationUserSup(seUserSup);
								}
							}
						}
			}
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改被考核供应商信息");
		} catch (Exception e) {
			log("修改被考核供应商信息初始化错误！", e);
			throw new BaseException("修改被考核供应商信息初始化错误！", e);
		}
		return "sup_modify";
		
	}
	
	/**
	 * 修改供应商考核人信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierUsersInit() throws BaseException {
		
		try{
			// 取得考核基本信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			supplierEvaluateUser = new SupplierEvaluateUser();
			supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
			this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser));
			for(Iterator iter=this.getListValue().iterator();iter.hasNext();){
				SupplierEvaluateUser seu = (SupplierEvaluateUser) iter.next();
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSeuId(seu.getSeuId());
				supplierEvaluationUserSup.setSeId(seu.getSeId());
				List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				String evaSuppliers = "";//被考核供应商信息集合
				String evaSupplierIds = "";//被考核供应商信息集合
				String evaSupNames = "";//被考核供应商名称
				for(SupplierEvaluationUserSup seus:seusList){
					evaSupNames += seus.getSupplierName()+",";
					evaSuppliers += seus.getSesId()+":"+seus.getSupplierName()+",";
					evaSupplierIds += seus.getSesId()+",";
				}
				seu.setEvaSupNames(StringUtil.subStringLastOfSeparator(evaSupNames, ','));
				seu.setEvaSuppliers(StringUtil.subStringLastOfSeparator(evaSuppliers, ','));
				seu.setEvaSupplierIds(StringUtil.subStringLastOfSeparator(evaSupplierIds, ','));
			}
		} catch (Exception e) {
			log("修改供应商考核人信息初始化错误！", e);
			throw new BaseException("修改供应商考核人信息初始化错误！", e);
		}
		return "user_modifyInit";
		
	}
	
	/**
	 * 修改供应商考核人信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluateSupplierUsers() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			this.iEvaluateSupplierRelationBiz.deleteSupplierEvaluationUserSupBySeId(supplierEvaluations.getSeId());
			SupplierEvaluationUserSup seUserSup=null;
			//保存
			for(SupplierEvaluateUser seu:mesUserList){
				if(StringUtil.isNotBlank(seu)){
					//保存考核人分配供应商关联信息
					if(StringUtil.isNotBlank(seu.getEvaSuppliers())){
						String[] strArr = seu.getEvaSuppliers().split(",");
						for(int i=0;i<strArr.length;i++){
							if(StringUtil.isNotBlank(strArr[i])){
								String[] idNames = strArr[i].split(":");
								seUserSup = new SupplierEvaluationUserSup();
								seUserSup.setSesId(Long.parseLong(idNames[0]));
								seUserSup.setSupplierName(idNames[1]);
								seUserSup.setUserNameCn(seu.getUserNameCn());
								seUserSup.setSeuId(seu.getSeuId());
								seUserSup.setSeId(supplierEvaluations.getSeId());
								seUserSup.setComId(comId);
								this.iEvaluateSupplierRelationBiz.saveSupplierEvaluationUserSup(seUserSup);
							}
						}
					}
				}
				
			}
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改供应商考核人信息");
		} catch (Exception e) {
			log("修改供应商考核人信息错误！", e);
			throw new BaseException("修改供应商考核人信息错误！", e);
		}
		return "user_modify";
		
	}
	
	/**
	 * 查看供应商考核指标明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluateSupplierEvaIndexDetail() throws BaseException {
		
		try{
			// 取得供应商考核明细信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			//获得增加指标信息
			SupplierEvaluateIndex supEvaluateIndex = new SupplierEvaluateIndex();
			supEvaluateIndex.setSeId(supplierEvaluations.getSeId());
			evaIndexList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supEvaluateIndex);
			if(evaIndexList!=null&&evaIndexList.size()>0){
				for(SupplierEvaluateIndex sei:evaIndexList){
					refPointsSum += sei.getPoints();
					adjPointsSum +=sei.getAdjustPoints();
				}
			}
			adjPointsSumStr = StringUtil.formateNumberTo(adjPointsSum);
		} catch (Exception e) {
			log("编辑供应商考核指标明细信息错误！", e);
			throw new BaseException("编辑供应商考核指标明细信息错误！", e);
		}
		return "index_detail";
	}
	
	/**
	 * 查看被考核供应商明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluateSupplierSupDetail() throws BaseException {
		
		try{
			// 取得考核明细信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			if(supplierEvaluations!=null){
				supplierEvaluateSup = new SupplierEvaluateSup();
				supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
				this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup));
				for(Iterator iter=this.getListValue().iterator();iter.hasNext();){
					SupplierEvaluateSup ses = (SupplierEvaluateSup) iter.next();
					supplierEvaluationUserSup = new SupplierEvaluationUserSup();
					supplierEvaluationUserSup.setSesId(ses.getSesId());
					List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
					String evaUserNames = "";//考核人姓名集合
					for(SupplierEvaluationUserSup seus:seusList){
						evaUserNames += seus.getUserNameCn()+",";
					}
					ses.setEvaUserNames(StringUtil.subStringLastOfSeparator(evaUserNames, ','));
				}
			}
		} catch (Exception e) {
			log("查看考核明细信息错误！", e);
			throw new BaseException("查看考核明细信息错误！", e);
		}
		return "sup_detail";
		
	}
	
	/**
	 * 查看供应商考核人明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluateSupplierUsersDetail() throws BaseException {
		
		try{
			// 取得考核明细信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			if(supplierEvaluations!=null){
				supplierEvaluateUser = new SupplierEvaluateUser();
				supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
				this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser));
				for(Iterator iter=this.getListValue().iterator();iter.hasNext();){
					SupplierEvaluateUser seu = (SupplierEvaluateUser) iter.next();
					supplierEvaluationUserSup = new SupplierEvaluationUserSup();
					supplierEvaluationUserSup.setSeuId(seu.getSeuId());
					List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
					String evaSupNames = "";//被考核供应商名称
					for(SupplierEvaluationUserSup seus:seusList){
						evaSupNames += seus.getSupplierName()+",";
					}
					seu.setEvaSupNames(StringUtil.subStringLastOfSeparator(evaSupNames, ','));
				}
			}
		} catch (Exception e) {
			log("查看考核明细信息错误！", e);
			throw new BaseException("查看考核明细信息错误！", e);
		}
		return "user_detail";
		
	}
	
	/**
	 * 供应商考核评分
	 * @return
	 * @throws BaseException 
	 */
	public String viewAndEvaluateSupplier() throws BaseException {
		
		try{
			//获取考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			//获取考核人信息
			supplierEvaluateUser = new SupplierEvaluateUser();
			supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
			supplierEvaluateUser.setUserName(UserRightInfoUtil.getUserName(getRequest()));
			List<SupplierEvaluateUser> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
			supplierEvaluateUser = seuList.get(0);
			//设置待办信息
			supplierEvaluationUserSup = new SupplierEvaluationUserSup();
			supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
			supplierEvaluationUserSup.setSeuId(supplierEvaluateUser.getSeuId());
			List<SupplierEvaluationUserSup>  seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
			for(SupplierEvaluationUserSup seus:seusList){
				//获得供应商ID
				supplierInfo = this.iSupplierInfoBiz.getSupplierInfoByName(seus.getSupplierName());
				seus.setSupplierId(supplierInfo.getSupplierId());				
			}
			this.setListValue(seusList);
			//设置任务标识
			this.getRequest().setAttribute("taskId", getRequest().getParameter("taskId"));
		} catch (Exception e) {
			log("供应商考核评分信息错误！", e);
			throw new BaseException("供应商考核评分信息错误！", e);
		}
		return "supEva_points";
		
	}
	
	/**
	 * 考核人对供应商进行评分
	 * @return
	 * @throws BaseException 
	 */
	public String saveViewEvaPointsSuppliers() throws BaseException {
		
		try{
			String index=this.getRequest().getParameter("index");
			//查看考核结果时生效
			if("supIndex".equals(tempSupEvaInfo)){
				String seDate = supplierEvaluateUser.getSeDate();
				this.getRequest().setAttribute("seDate", seDate);
			}
			//获取考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluateResult.getSeId());
			//获取考核人信息
			supplierEvaluateUser = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUser(supplierEvaluateResult.getSeuId());
			//获取考核供应商信息
			supplierEvaluateSup = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSup(supplierEvaluateResult.getSesId());
			//获取考核指标信息
			this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateAboutIndexList(supplierEvaluateResult));
			//初始化考核结果信息
			if(this.getListValue()==null||this.getListValue().size()==0){
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
				supplierEvaluationUserSup.setSeuId(supplierEvaluateUser.getSeuId());
				List<SupplierEvaluationUserSup>  seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				for(SupplierEvaluationUserSup seus:seusList){
					supplierEvaluateIndex = new SupplierEvaluateIndex();
					supplierEvaluateIndex.setSeId(supplierEvaluations.getSeId());
					List<SupplierEvaluateIndex> seiList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supplierEvaluateIndex);
					for(SupplierEvaluateIndex sei:seiList){
						SupplierEvaluateResult supEvaluateResult = new SupplierEvaluateResult();
						BeanUtils.copyProperties(supEvaluateResult, sei);
						supEvaluateResult.setSesId(seus.getSesId());
						supEvaluateResult.setSupplierName(seus.getSupplierName());
						supEvaluateResult.setSeuId(seus.getSeuId());
						supEvaluateResult.setUserName(seus.getUserNameCn());
						supEvaluateResult.setSeId(seus.getSeId());
						this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateResult(supEvaluateResult);
					}
				}
				//列表
				this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateAboutIndexList(supplierEvaluateResult));
			}
			//设置考核日期
			supplierEvaluateResult.setSeDate(DateUtil.getCurrentDateTime());
			this.getRequest().setAttribute("index", index);
		} catch (Exception e) {
			log("考核人对供应商进行评分错误！", e);
			throw new BaseException("考核人对供应商进行评分错误！", e);
		}
		if("supIndex".equals(tempSupEvaInfo)){
			return "indexScore_detail";
		}
		return "supScore";
		
	}
	
	/**
	 * 供应商考核分值保存（提交）
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaPointsSuppliers() throws BaseException {
		
		try{
			this.ajaxHeadInit();
			PrintWriter out = this.getResponse().getWriter();
			String type = this.getRequest().getParameter("type");
			supplierEvaluationUserSup = new SupplierEvaluationUserSup();
			Double supPoints = 0.0;//供应商指标分值之和
			if(StringUtil.isNotBlank(tempSupEvaInfo)){
				if("save".equals(type)){
					String[] str = tempSupEvaInfo.split(",");
					for(int i=0;i<str.length;i++){
						if(StringUtil.isNotBlank(str[i])){
							String[] idPoints = str[i].split(":");
							SupplierEvaluateResult supEvaluateResult = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateResult(Long.parseLong(idPoints[0]));
							supEvaluateResult.setJudgePoint(Double.parseDouble(idPoints[1]));
							supEvaluateResult.setSeDate(DateUtil.getCurrentDateTime());
							supEvaluateResult.setStatus("0");
							this.iEvaluateSupplierRelationBiz.updateSupplierEvaluateResult(supEvaluateResult);
							supPoints += supEvaluateResult.getJudgePoint();
						}
					}
					out.println(DateUtil.getCurrentZhTimeFormat());
					//保存供应商得分
					supplierEvaluateResult.setStatus("0");
					supplierEvaluationUserSup.setStatus("0");
				}else if("submit".equals(type)){
					String[] ids = tempSupEvaInfo.split(":");
					//获取考核指标信息
					SupplierEvaluateResult supEvaluateResult = new SupplierEvaluateResult();
					supEvaluateResult.setSeId(Long.parseLong(ids[0]));
					supEvaluateResult.setSeuId(Long.parseLong(ids[1]));
					supEvaluateResult.setSesId(Long.parseLong(ids[2]));
					List<SupplierEvaluateResult> serList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateAboutIndexList(supEvaluateResult);
					for(SupplierEvaluateResult ser:serList){
						ser.setStatus("1");
						this.iEvaluateSupplierRelationBiz.updateSupplierEvaluateResult(ser);
					}
					out.println("1");
					//保存供应商得分
					if(supplierEvaluateResult==null){
						supplierEvaluateResult = new SupplierEvaluateResult();
					}
					BeanUtils.copyProperties(supplierEvaluateResult, supEvaluateResult);
					supplierEvaluateResult.setStatus("1");
					supplierEvaluationUserSup.setStatus("1");
				}
				//保存供应商得分
				SupplierEvaluationUserSup supEvaluationUserSup = new SupplierEvaluationUserSup();
				supEvaluationUserSup.setSeId(supplierEvaluateResult.getSeId());
				supEvaluationUserSup.setSeuId(supplierEvaluateResult.getSeuId());
				supEvaluationUserSup.setSesId(supplierEvaluateResult.getSesId());
				List<SupplierEvaluationUserSup>  seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supEvaluationUserSup);
				if(seusList!=null&&seusList.size()>0){
					SupplierEvaluationUserSup seus = seusList.get(0);
					if("0".equals(supplierEvaluationUserSup.getStatus())){
						seus.setSupPoints(supPoints);
					}
					seus.setStatus(supplierEvaluationUserSup.getStatus());
					seus.setSeDate(DateUtil.getCurrentDateTime());
					this.iEvaluateSupplierRelationBiz.updateSupplierEvaluationUserSup(seus);
				}
				//1.更新被考核供应商总得分
				int submitUserNum = 0;//已提交评分考核人数量
				Double supSumPoints = 0.0;
				SupplierEvaluateSup seSup = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSup(supplierEvaluateResult.getSesId());
				SupplierEvaluationUserSup seUserSup1 = new SupplierEvaluationUserSup();
				seUserSup1.setSeId(supplierEvaluateResult.getSeId());
				seUserSup1.setSesId(supplierEvaluateResult.getSesId());
				List<SupplierEvaluationUserSup>  seUSList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(seUserSup1);
				for(SupplierEvaluationUserSup seUS:seUSList){
					if("1".equals(seUS.getStatus())){
						submitUserNum++;
						if(StringUtil.isNotBlank(seUS.getSupPoints())){
							supSumPoints += seUS.getSupPoints();
						}
					}
				}
				//判断是该供应商的所有考核人都已提交
				if(seUSList.size()==submitUserNum){
					seSup.setStatus("1");
				}else{
					seSup.setStatus("0");
				}
				seSup.setSupSumPoints(supSumPoints);
				seSup.setSeDate(DateUtil.getCurrentDateTime());
				if(submitUserNum!=0){
					seSup.setSeuCount(Long.parseLong(submitUserNum+""));
				}
				this.iEvaluateSupplierRelationBiz.updateSupplierEvaluateSup(seSup);
				//2.更新考核人已评供应商个数
				int submitSupNum = 0;//已考核供应商数量
				SupplierEvaluationUserSup seUserSup2 = new SupplierEvaluationUserSup();
				seUserSup2.setSeId(supplierEvaluateResult.getSeId());
				seUserSup2.setSeuId(supplierEvaluateResult.getSeuId());
				List<SupplierEvaluationUserSup>  seUSList2 = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(seUserSup2);
				for(SupplierEvaluationUserSup seUS:seUSList2){
					if("1".equals(seUS.getStatus())){
						submitSupNum++;
					}
				}
				SupplierEvaluateUser seUser = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUser(supplierEvaluateResult.getSeuId());
				if(submitSupNum!=0){
					seUser.setSedCount(Long.parseLong(submitSupNum+""));
				}
				//更新供应商考核待办信息
				if(submitSupNum==seUSList2.size()){
					String taskId = getRequest().getParameter("taskId");//获得当前待办任务标识
					String result = getRequest().getParameter("result");
					Map<String, Object> args = new HashMap<String, Object>();
					args.put("result", result);
					supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(seUser.getSeId());
					args.put("buyRemark",  supplierEvaluations.getSeDescribe());
					facets.execute(taskId, UserRightInfoUtil.getUserName(getRequest()), args);
				}
				this.iEvaluateSupplierRelationBiz.updateSupplierEvaluateUser(seUser);
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			log("保存供应商考核考核分数信息错误！", e);
			throw new BaseException("保存供应商考核考核分数信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看被评供应商平均得分明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierNumberDetail() throws BaseException {
		
		try{
			String supplierId=this.getRequest().getParameter("supplierId");
			//获取被考核供应商信息
			supplierEvaluateSup = new SupplierEvaluateSup();
			supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
			if(StringUtil.isNotBlank(supplierId)) supplierEvaluateSup.setSupplierId(Long.parseLong(supplierId));
			List<SupplierEvaluateSup> sesSupList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
			for(SupplierEvaluateSup sesSup:sesSupList){
				//获得应提交考核人数量
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
				supplierEvaluationUserSup.setSesId(sesSup.getSesId());
				List<SupplierEvaluationUserSup> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				sesSup.setSumUserNum(seuList.size()+"");
				if(StringUtil.isNotBlank(sesSup.getSeuCount())){
					if("0".equals(sesSup.getSubmitUserNum())){
						sesSup.setSupAvgPoint(StringUtil.formateNumberTo(sesSup.getSupSumPoints()));
					}else{
						sesSup.setSupAvgPoint(StringUtil.formateNumberTo(sesSup.getSupSumPoints()/sesSup.getSeuCount()));
					}
				}
			}
			this.getRequest().setAttribute("supAvgPointList", sesSupList);
			//获得考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations.getSeId());
		} catch (Exception e) {
			log("查看被评供应商平均得分明细信息错误！", e);
			throw new BaseException("查看被评供应商平均得分明细信息错误！", e);
		}
		return "supNumber_detail";
		
	}
	
	/**
	 * 查看每个考核人对供应商的评分明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierJudgerScoreDetail() throws BaseException {
		
		try{
			//获取考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations.getSeId());
			//获取供应商信息
			SupplierEvaluateSup  seSup = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSup(supplierEvaluateSup.getSesId());
			seSup.setSupSumPoints(supplierEvaluateSup.getSupSumPoints());
			//获取考核人信息
			supplierEvaluationUserSup = new SupplierEvaluationUserSup();
			supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
			supplierEvaluationUserSup.setSesId(supplierEvaluateSup.getSesId());
			List<SupplierEvaluationUserSup> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
			this.getRequest().setAttribute("seuPointList", seuList);
			this.getRequest().setAttribute("seSup", seSup);
		} catch (Exception e) {
			log("查看每个考核人对供应商评分明细信息错误！", e);
			throw new BaseException("查看每个考核人对供应商评分明细信息错误！", e);
		}
		return "supScore_detail";
		
	}
	
	/**
	 * 查看考核人对供应商考核指标的评分明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierEvaIndexScoreDetail() throws BaseException {
		
		try{
			//获取考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluateResult.getSeId());
			//获取考核人信息
			supplierEvaluateUser = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUser(supplierEvaluateResult.getSeuId());
			//获取考核供应商信息
			supplierEvaluateSup = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSup(supplierEvaluateResult.getSesId());
			//获取考核指标信息
			this.setListValue(this.iEvaluateSupplierRelationBiz.getSupplierEvaluateAboutIndexList(supplierEvaluateResult));
		} catch (Exception e) {
			log("查看考核人对供应商考核指标的评分明细信息错误！", e);
			throw new BaseException("查看考核人对供应商考核指标的评分明细信息错误！", e);
		}
		return "indexScore_detail";
		
	}
	
	/**
	 * 查看考核人考核的供应商个数信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierNumberToJudger() throws BaseException {
		
		try{
			//获得考核人列表
			supplierEvaluateUser = new SupplierEvaluateUser();
			supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
			List<SupplierEvaluateUser> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
			for(SupplierEvaluateUser seUser:seuList){
				supplierEvaluationUserSup = new SupplierEvaluationUserSup();
				supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
				supplierEvaluationUserSup.setSeuId(seUser.getSeuId());
				List<SupplierEvaluationUserSup>  seUSList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
				seUser.setSumSupplier(Long.parseLong(seUSList.size()+""));
			}
			this.getRequest().setAttribute("seuList", seuList);
			//获取考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
		} catch (Exception e) {
			log("查看考核人考核的供应商个数信息错误！", e);
			throw new BaseException("查看考核人考核的供应商个数信息错误！", e);
		}
		return "supNumToJudger_detail";
		
	}
	
	/**
	 * 查看供应商考核评分明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewAndEvaluateSupplierDetail() throws BaseException {
		
		try{
			//获取考核信息
			supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(supplierEvaluations);
			//获取考核人信息
			supplierEvaluateUser = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUser(supplierEvaluateUser);
			//设置待办信息
			supplierEvaluationUserSup = new SupplierEvaluationUserSup();
			supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
			supplierEvaluationUserSup.setSeuId(supplierEvaluateUser.getSeuId());
			List<SupplierEvaluationUserSup>  seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
			this.setListValue(seusList);
		} catch (Exception e) {
			log("查看供应商考核评分明细信息错误！", e);
			throw new BaseException("查看供应商考核评分明细信息错误！", e);
		}
		return "sup_points";
		
	}
	
	/**
	 * 复制供应商考核信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveCopySupEvalationInfo() throws BaseException {
		
		try{	
			if(StringUtil.isNotBlank(tempSupEvaInfo)){
				String[] supIdArr = tempSupEvaInfo.split(",");
				// 复制考核信息
				for(int i=0;i<supIdArr.length;i++){
					if(StringUtil.isNotBlank(supIdArr[i])){
						supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(Long.parseLong(supIdArr[i]));
						SupplierEvaluations Evaluations = new SupplierEvaluations();
						BeanUtils.copyProperties(Evaluations, supplierEvaluations);
						Evaluations.setSeCode(CodeGetByConditionsUtil.generateCodeByConditions("EL", "yyyyMMdd", "0000", "SEQ_SUPPLIER_EVA_ID"));
						Evaluations.setStatus("0");
						Evaluations.setIsUsable("0");
						this.iEvaluateSupplierBiz.saveSupplierEvaluations(Evaluations);
						List<SupplierEvaluations> evaSupplierList = this.iEvaluateSupplierBiz.getSupplierEvaluationsList(Evaluations);
						if(evaSupplierList!=null&&evaSupplierList.size()>0){
							SupplierEvaluations se = evaSupplierList.get(0);
							//复制考核人信息
							supplierEvaluateUser = new SupplierEvaluateUser();
							supplierEvaluateUser.setSeId(supplierEvaluations.getSeId());
							List<SupplierEvaluateUser> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
							if(seuList!=null&&seuList.size()>0){
								for(SupplierEvaluateUser seu:seuList){
									SupplierEvaluateUser supEvaluateUser = new SupplierEvaluateUser();
									//BeanUtils.copyProperties(supEvaluateUser, seu);
									supEvaluateUser.setSeId(se.getSeId());
									supEvaluateUser.setUserId(seu.getUserId());
									supEvaluateUser.setUserName(seu.getUserName());
									supEvaluateUser.setUserNameCn(seu.getUserNameCn());
									this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateUser(supEvaluateUser);
									//复制考核人和供应商关联关系信息
									supplierEvaluationUserSup = new SupplierEvaluationUserSup();
									supplierEvaluationUserSup.setSeId(supplierEvaluations.getSeId());
									List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
									for(SupplierEvaluationUserSup seus:seusList){
										if(supEvaluateUser.getUserNameCn().equals(seus.getUserNameCn())){
											SupplierEvaluationUserSup supEvaluationUserSup = new SupplierEvaluationUserSup();
											BeanUtils.copyProperties(supEvaluationUserSup, seus);
											supEvaluationUserSup.setSeuId(supEvaluateUser.getSeuId());
											supEvaluationUserSup.setSeId(se.getSeId());
											this.iEvaluateSupplierRelationBiz.saveSupplierEvaluationUserSup(supEvaluationUserSup);
										}
									}
								}
							}
							//复制被考核供应商信息
							supplierEvaluateSup = new SupplierEvaluateSup();
							supplierEvaluateSup.setSeId(supplierEvaluations.getSeId());
							List<SupplierEvaluateSup> sepList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(supplierEvaluateSup);
							if(sepList!=null&&sepList.size()>0){
								for(SupplierEvaluateSup sep:sepList){
									SupplierEvaluateSup supEvaluateSup = new SupplierEvaluateSup();
									//BeanUtils.copyProperties(supEvaluateSup, sep);
									supEvaluateSup.setSeId(se.getSeId());
									supEvaluateSup.setSupplierId(sep.getSupplierId());
									supEvaluateSup.setSupplierName(sep.getSupplierName());
									supEvaluateSup.setContactPerson(sep.getContactPerson());
									this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateSup(supEvaluateSup);
									//复制考核人和供应商关联关系信息
									supplierEvaluationUserSup = new SupplierEvaluationUserSup();
									supplierEvaluationUserSup.setSeId(se.getSeId());
									List<SupplierEvaluationUserSup> seusList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluationUserSupList(supplierEvaluationUserSup);
									for(SupplierEvaluationUserSup seus:seusList){
										if(supEvaluateSup.getSupplierName().equals(seus.getSupplierName())){
											seus.setSesId(supEvaluateSup.getSesId());
											this.iEvaluateSupplierRelationBiz.saveSupplierEvaluationUserSup(seus);
										}
									}
								}
							}
							//复制考核指标信息
							supplierEvaluateIndex = new SupplierEvaluateIndex();
							supplierEvaluateIndex.setSeId(supplierEvaluations.getSeId());
							List<SupplierEvaluateIndex> seiList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateIndexList(supplierEvaluateIndex);
							for(SupplierEvaluateIndex sei:seiList){
								SupplierEvaluateIndex SupEvaluateIndex = new SupplierEvaluateIndex();
								//BeanUtils.copyProperties(SupEvaluateIndex, sei);
								SupEvaluateIndex.setSeId(se.getSeId());
								SupEvaluateIndex.setEikId(sei.getEiId());
								SupEvaluateIndex.setEikName(sei.getEikName());
								SupEvaluateIndex.setEiId(sei.getEiId());
								SupEvaluateIndex.setEiName(sei.getEiName());
								SupEvaluateIndex.setEiDescribe(sei.getEiDescribe());
								SupEvaluateIndex.setPoints(sei.getPoints());
								SupEvaluateIndex.setAdjustPoints(sei.getAdjustPoints());
								this.iEvaluateSupplierRelationBiz.saveSupplierEvaluateIndex(SupEvaluateIndex);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log("复制供应商考核信息错误！", e);
			throw new BaseException("复制供应商考核信息错误！", e);
		}
		return "copy_view";
		
	}

	
	/*************************供应商考核信息***************************/
	/**
	 * 打印合作供应商信息
	 * @return
	 * @throws BaseException 
	 */
	public String printviewSupplierNumber() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);			
			//获取被考核供应商信息
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierInfo.getSupplierId());
			this.getRequest().setAttribute("systemCurrentDept", systemConfiguration.getSystemCurrentDept());
		} catch (Exception e) {
			log("查看被评供应商平均得分明细信息错误！", e);
			throw new BaseException("查看被评供应商平均得分明细信息错误！", e);
		}
		return "print_SupNumber";
		
	}
	
	/**
	 * 设置流程相关列表
	 */
	private void setProcessList(List<EvaluateSupplierView> list, String processName,Long comId){
		//step1：首先获取流程定义实体
		org.snaker.engine.entity.Process process = facets.getEngine().process().getProcessByName(processName,1L);
		//step2：1、判断流程定义是否存在，2、遍历集合添加流程实例
		if(process != null){
			for(EvaluateSupplierView esv : list){
				//step3:判断流程实例是否运行
				QueryFilter filter = new QueryFilter();
				if(StringUtil.isNotBlank(esv.getSeId())){
					String newOrderNo=WorkFlowStatus.SE_WorkFlow_Type+esv.getSeId().toString();
					filter.setOrderNo(newOrderNo);
					filter.setProcessId(process.getId());
					List<HistoryOrder> holist = facets.getEngine().query().getHistoryOrders(filter);
					if(holist.size() > 0){
						//step5:获取正在运行的流程实例
						filter.setOrderNo(newOrderNo);
						filter.setProcessId(process.getId());
						List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
						if(orderlist.size() > 0){
							Order order = orderlist.get((orderlist.size()-1));
							if(order != null){
								List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
								if(tasklist.size() > 0){
									//设置当前流程名称
									esv.setProcessName(tasklist.get(0).getDisplayName());
								}
								esv.setOrderId(order.getId());//设置实例标示
							}
						}
						HistoryOrder ho = holist.get(0);
						esv.setOrderState(ho.getOrderState().toString());
						esv.setOrderStateName(WorkFlowStatusMap.getWorkflowOrderStatus(ho.getOrderState().toString()));
					}else{
						esv.setOrderStateName(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01_TEXT);
					}					
					esv.setProcessId(process.getId());//设置流程标示
					esv.setInstanceUrl(process.getInstanceUrl());//设置流程实例URL
				}
			}
		}
	}
	
	/*********************供应商考核选人和选择被考核供应商*********************/
	
	
	/**
	 * 选择被考核供应商
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierIndex() {
		String ul=this.getRequest().getParameter("ul");
		this.getRequest().setAttribute("ul", ul);
		this.getRequest().setAttribute("seId", supplierEvaluateSup.getSeId());
	
		return "indexSupplier";
	}
	/**
	 * 选择被考核供应商上部
	 * @return
	 * @throws Exception
	 */
	public String viewSupplierTopIndex() throws Exception {
		this.getRequest().setAttribute("seId", supplierEvaluateSup.getSeId());
		return "topSupplier" ;
	}
	/**
	 * 查询被考核供应商
	 * @return
	 * @throws Exception
	 */
	public String findSupplierTopIndex() throws Exception {
		
		String seId=this.getRequest().getParameter("seId");
		String supplierName=this.getRequest().getParameter("supplierName");
		if(supplierEvaluateSup==null)
		{
			supplierEvaluateSup=new SupplierEvaluateSup();
		}
		supplierEvaluateSup.setSeId(new Long(seId));
		supplierEvaluateSup.setSupplierName(supplierName);
		
		this.setListValue(iEvaluateSupplierRelationBiz.getSupplierEvaluateSupList(this.getRollPageDataTables(),supplierEvaluateSup));
		this.getPagejsonDataTables(this.getListValue());
		
		return null;
	}
	/**
	 * 选择被考核供应商下部	
	 * @return
	 * @throws Exception
	 */
	public String viewSupplierButtonIndex() throws Exception {
		
		String ul=this.getRequest().getParameter("ul");
		if(ul=="-1" || "-1".equals(ul)){ul="";}
		if( StringUtil.isNotBlank(ul)){
			String [] mn =ul.split(",");
			String mc ="";
			for(int i=0;i<mn.length;i++){
				String m=iEvaluateSupplierRelationBiz.getSupplierEvaluateSup(new Long(mn[i])).getSupplierName();
				mc+=mn[i]+":"+m+",";
			}
			//System.out.println( mc.substring(0, mc.lastIndexOf(",")));
			this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
		}else{
		this.getRequest().setAttribute("ul", "-1");
		}
		
		return "buttonSupplier" ;
	}
	/**
	 * 选择考核人
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewUserIndex() {
		String ul=this.getRequest().getParameter("ul");
		this.getRequest().setAttribute("ul", ul);
		this.getRequest().setAttribute("seId", supplierEvaluateUser.getSeId());
		return "indexUser";
	}
	/**
	 * 选择考核人上部
	 * @return
	 * @throws Exception
	 */
	public String viewUserTopIndex() throws Exception {
		String seId=this.getRequest().getParameter("seId");
		//System.out.println(seId+"----");
		this.getRequest().setAttribute("seId",seId);
		return "topUser" ;
	}
	/**
	 * 查询选择考核人
	 * @return
	 * @throws Exception
	 */
	public String findUserTopIndex() throws Exception {
		String seId=this.getRequest().getParameter("seId");
		String userChinesename=this.getRequest().getParameter("userChinesename");
		if(supplierEvaluateUser==null){
			supplierEvaluateUser=new SupplierEvaluateUser();
		}
		if(userChinesename!=null || "".equals(userChinesename))
		{
			supplierEvaluateUser.setUserName(userChinesename);
		}
		supplierEvaluateUser.setSeId(new Long(seId));
		
		this.setListValue(iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(this.getRollPageDataTables(),supplierEvaluateUser));
		this.getPagejsonDataTables(this.getListValue());
		return null;
	}
	/**
	 * 选择考核人下部
	 * @return
	 * @throws Exception
	 */
	public String viewUserButtonIndex() throws Exception {
	
		String ul=this.getRequest().getParameter("ul");
		if(ul=="-1" || "-1".equals(ul)){ul="";}
		if( StringUtil.isNotBlank(ul)){
			String [] mn =ul.split(",");
			String mc ="";
			for(int i=0;i<mn.length;i++){
				String m=iEvaluateSupplierRelationBiz.getSupplierEvaluateUser(new Long(mn[i])).getUserNameCn();
				mc+=mn[i]+":"+m+",";
			}
			//System.out.println( mc.substring(0, mc.lastIndexOf(",")));
			this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
		}else{
			this.getRequest().setAttribute("ul", "-1");
		}
		return "buttonUser" ;
	}
	/************************************供应商 tab页考核信息***************************************************/
	/**
	 * 查看供应商考核信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMultiEvaluataions_base() throws BaseException {
		
		try{
			String supplierId=this.getRequest().getParameter("supplierId");
			this.getRequest().setAttribute("supplierId", supplierId);
		} catch (Exception e) {
			log.error("查看供应商考核信息列表错误！", e);
			throw new BaseException("查看供应商考核信息列表错误！", e);
		}	
			
		return "view2" ;
				
	}
	/**
	 * 查看供应商考核信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewToMultiEvaluataions_base() throws BaseException {
		
		try{
			supplierEvaluateSup=new SupplierEvaluateSup();
			String supplierId=this.getRequest().getParameter("supplierId");
			supplierEvaluateSup.setSupplierId(Long.parseLong(supplierId));
			supplierEvaluateSup.setStatus("1");
			List<Object[]> list=this.iEvaluateSupplierRelationBiz.getSupplierEvaluateSupListForTab(this.getRollPageDataTables(), supplierEvaluateSup);
			List<SupplierEvaluateSup> sesSupList=new ArrayList<SupplierEvaluateSup>();
			for(Object[] object:list){
				supplierEvaluateSup=(SupplierEvaluateSup)object[0];
				if(StringUtil.isNotBlank(supplierEvaluateSup.getSeuCount())){
					supplierEvaluateSup.setSupAvgPoint(StringUtil.formateNumberTo(supplierEvaluateSup.getSupSumPoints()/supplierEvaluateSup.getSeuCount()));
				}
				supplierEvaluateSup.setSeDescribe((String)object[1]);
				sesSupList.add(supplierEvaluateSup);
			}
			this.getPagejsonDataTables(sesSupList);
		} catch (Exception e) {
			log.error("查看供应商考核信息列表错误！", e);
			throw new BaseException("查看供应商考核信息列表错误！", e);
		}
		
		return null ;
		
	}
	public IEvaluateSupplierBiz getiEvaluateSupplierBiz() {
		return iEvaluateSupplierBiz;
	}
	public void setiEvaluateSupplierBiz(IEvaluateSupplierBiz iEvaluateSupplierBiz) {
		this.iEvaluateSupplierBiz = iEvaluateSupplierBiz;
	}
	public SupplierEvaluations getSupplierEvaluations() {
		return supplierEvaluations;
	}
	public void setSupplierEvaluations(SupplierEvaluations supplierEvaluations) {
		this.supplierEvaluations = supplierEvaluations;
	}
	public IEvaluateSupplierRelationBiz getiEvaluateSupplierRelationBiz() {
		return iEvaluateSupplierRelationBiz;
	}
	public void setiEvaluateSupplierRelationBiz(
			IEvaluateSupplierRelationBiz iEvaluateSupplierRelationBiz) {
		this.iEvaluateSupplierRelationBiz = iEvaluateSupplierRelationBiz;
	}
	public SupplierEvaluateUser getSupplierEvaluateUser() {
		return supplierEvaluateUser;
	}
	public void setSupplierEvaluateUser(SupplierEvaluateUser supplierEvaluateUser) {
		this.supplierEvaluateUser = supplierEvaluateUser;
	}
	public SupplierEvaluateSup getSupplierEvaluateSup() {
		return supplierEvaluateSup;
	}
	public void setSupplierEvaluateSup(SupplierEvaluateSup supplierEvaluateSup) {
		this.supplierEvaluateSup = supplierEvaluateSup;
	}
	public SupplierEvaluateIndex getSupplierEvaluateIndex() {
		return supplierEvaluateIndex;
	}
	public void setSupplierEvaluateIndex(SupplierEvaluateIndex supplierEvaluateIndex) {
		this.supplierEvaluateIndex = supplierEvaluateIndex;
	}
	public SupplierEvaluateResult getSupplierEvaluateResult() {
		return supplierEvaluateResult;
	}
	public void setSupplierEvaluateResult(
			SupplierEvaluateResult supplierEvaluateResult) {
		this.supplierEvaluateResult = supplierEvaluateResult;
	}
	public IEvaluationIndexBiz getiEvaluationIndexBiz() {
		return iEvaluationIndexBiz;
	}
	public void setiEvaluationIndexBiz(IEvaluationIndexBiz iEvaluationIndexBiz) {
		this.iEvaluationIndexBiz = iEvaluationIndexBiz;
	}
	public EvaluationIndex getEvaluationIndex() {
		return evaluationIndex;
	}
	public void setEvaluationIndex(EvaluationIndex evaluationIndex) {
		this.evaluationIndex = evaluationIndex;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}
	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}
	
	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}
	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public String getTempSupEvaInfo() {
		return tempSupEvaInfo;
	}
	public void setTempSupEvaInfo(String tempSupEvaInfo) {
		this.tempSupEvaInfo = tempSupEvaInfo;
	}
	public List<SupplierEvaluateSup> getMesSupList() {
		return mesSupList;
	}
	public void setMesSupList(List<SupplierEvaluateSup> mesSupList) {
		this.mesSupList = mesSupList;
	}
	public List<SupplierEvaluateUser> getMesUserList() {
		return mesUserList;
	}
	public void setMesUserList(List<SupplierEvaluateUser> mesUserList) {
		this.mesUserList = mesUserList;
	}
	public List<SupplierEvaluateIndex> getEvaIndexList() {
		return evaIndexList;
	}
	public void setEvaIndexList(List<SupplierEvaluateIndex> evaIndexList) {
		this.evaIndexList = evaIndexList;
	}
	public Double getRefPointsSum() {
		return refPointsSum;
	}
	public void setRefPointsSum(Double refPointsSum) {
		this.refPointsSum = refPointsSum;
	}
	public Double getAdjPointsSum() {
		return adjPointsSum;
	}
	public void setAdjPointsSum(Double adjPointsSum) {
		this.adjPointsSum = adjPointsSum;
	}
	public String getAdjPointsSumStr() {
		return adjPointsSumStr;
	}
	public void setAdjPointsSumStr(String adjPointsSumStr) {
		this.adjPointsSumStr = adjPointsSumStr;
	}
	public String getStrIndex() {
		return strIndex;
	}
	public void setStrIndex(String strIndex) {
		this.strIndex = strIndex;
	}
	public SupplierEvaluationUserSup getSupplierEvaluationUserSup() {
		return supplierEvaluationUserSup;
	}
	public void setSupplierEvaluationUserSup(
			SupplierEvaluationUserSup supplierEvaluationUserSup) {
		this.supplierEvaluationUserSup = supplierEvaluationUserSup;
	}
	public SnakerEngineFacets getFacets() {
		return facets;
	}
	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}

	public IWfTextBiz getiWfTextBiz() {
		return iWfTextBiz;
	}

	public void setiWfTextBiz(IWfTextBiz iWfTextBiz) {
		this.iWfTextBiz = iWfTextBiz;
	}

	
	
	
}
