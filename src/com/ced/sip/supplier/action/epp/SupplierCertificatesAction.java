package com.ced.sip.supplier.action.epp;

import java.text.SimpleDateFormat;
import java.util.List;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierCertificateChangeBiz;
import com.ced.sip.supplier.biz.ISupplierCertificateInfoBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierCertificateChange;
import com.ced.sip.supplier.entity.SupplierCertificateInfo;
import com.ced.sip.supplier.entity.SupplierInfo;

public class SupplierCertificatesAction extends BaseAction {
	// 供应商资质信息 服务类
	private ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz  ;
	// 供应商资质信息变更服务类
	private ISupplierCertificateChangeBiz iSupplierCertificateChangeBiz;
	//供应商基本信息
	private ISupplierInfoBiz iSupplierInfoBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// 供应商资质信息 实例
	private SupplierCertificateInfo supplierCertificateInfo ;
	// 供应商资质信息变更 实例
	private SupplierCertificateChange supplierCertificateChange ;
	
	private SupplierInfo supplierInfo;
	
	/**
	 * 查看供应商资质信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierCertificates_base() throws BaseException {

		String typePage=this.getRequest().getParameter("typePage");
		String supplierId=this.getRequest().getParameter("supplierId");
		this.getRequest().setAttribute("supplierId", supplierId);
		this.getRequest().setAttribute("typePage", typePage);		
		return "view2";
	}
	/**
	 * 查看供应商资质信息(供应商变更审核)
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierCertificatesChange_base() throws BaseException {
		
		String supplierId=this.getRequest().getParameter("supplierId");
		this.getRequest().setAttribute("supplierId", supplierId);
		return "viewChange";
	}
	/**
	 * 查询供应商资质信息列表
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierCertificates_base() throws BaseException {
			
		try{
			supplierCertificateInfo=new SupplierCertificateInfo();
			
			String supplierId=this.getRequest().getParameter("supplierId");
			supplierCertificateInfo.setSupplierId(Long.parseLong(supplierId));
			
			String certificateName=this.getRequest().getParameter("certificateName");
			supplierCertificateInfo.setCertificateName(certificateName);
			String timeBegain=this.getRequest().getParameter("timeBegain");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
			  
			if(StringUtil.isNotBlank(timeBegain))
			{
				supplierCertificateInfo.setTimeBegain(sdf.parse(timeBegain));
			}
			this.setListValue( this.iSupplierCertificateInfoBiz.getSupplierCertificateInfoList2( this.getRollPageDataTables(), supplierCertificateInfo ) ) ;
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商资质信息列表错误！", e);
			throw new BaseException("查看供应商资质信息列表错误！", e);
		}
		return null ;
	}
	/**
	 * 查询供应商资质信息列表(供应商变更审核)
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierCertificatesChange_base() throws BaseException {
			
		try{
			supplierCertificateChange=new SupplierCertificateChange();
			
			String supplierId=this.getRequest().getParameter("supplierId");
			supplierCertificateChange.setSupplierId(Long.parseLong(supplierId));
			
			String certificateName=this.getRequest().getParameter("certificateName");
			supplierCertificateChange.setCertificateName(certificateName);
			String timeBegain=this.getRequest().getParameter("timeBegain");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
			  
			if(StringUtil.isNotBlank(timeBegain))
			{
				supplierCertificateChange.setTimeBegain(sdf.parse(timeBegain));
			}
			this.setListValue( this.iSupplierCertificateChangeBiz.getSupplierCertificateChangeList(this.getRollPageDataTables(), supplierCertificateChange ) ) ;
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商资质信息列表(供应商变更审核)错误！", e);
			throw new BaseException("查看供应商资质信息列表(供应商变更审核)错误！", e);
		}
		return null ;
	}
	/**
	 * 查看供应商资质明细信息(供应商变更审核)
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierCertificatesDetailChange_base() throws BaseException {
		
		try{
			supplierCertificateChange=this.iSupplierCertificateChangeBiz.getSupplierCertificateChange(supplierCertificateChange.getScicId());
			supplierCertificateChange.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( supplierCertificateChange.getScicId(), AttachmentStatus.ATTACHMENT_CODE_6021 ) ) , "0", this.getRequest() ) ) ;
		} catch (Exception e) {
			log("查看供应商资质明细信息错误！", e);
			throw new BaseException("查看供应商资质明细信息错误！", e);
		}
		return "detailChange";
		
	}

	/**
	 * 查看供应商资质明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierCertificatesDetail_base() throws BaseException {
		
		try{
			supplierCertificateInfo=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfo( supplierCertificateInfo.getSciId() );
			//获取附件，参数“0”没有删除功能，参数“null”存在删除
			supplierCertificateInfo.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( supplierCertificateInfo.getSciId(), AttachmentStatus.ATTACHMENT_CODE_602 ) ) , "0", this.getRequest() ) ) ;
		} catch (Exception e) {
			log("查看供应商资质明细信息错误！", e);
			throw new BaseException("查看供应商资质明细信息错误！", e);
		}
		return "detail2";
		
	}
	/**
	 * 资质文件提醒信息页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierCertificatesRemind() throws BaseException {
		return "viewRemind";
	}
	/**
	 * 查询供应商资质信息列表
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierCertificatesRemind() throws BaseException {
			
		try{
			//审核通过的供应商进行资质提醒
			supplierInfo=new SupplierInfo();
			supplierInfo.setIsAudit(TableStatus.COMMON_0);
			String supplierName=this.getRequest().getParameter("supplierName");
			supplierInfo.setSupplierName(supplierName);
			
			List<SupplierCertificateInfo> list=this.iSupplierCertificateInfoBiz.getSupplierCertificateInRemindfoList(this.getRollPageDataTables(),supplierInfo);
					
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看供应商资质提醒列表错误！", e);
			throw new BaseException("查看供应商资质提醒列表错误！", e);
		}
		return null ;
	}
	public ISupplierCertificateInfoBiz getiSupplierCertificateInfoBiz() {
		return iSupplierCertificateInfoBiz;
	}

	public void setiSupplierCertificateInfoBiz(
			ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz) {
		this.iSupplierCertificateInfoBiz = iSupplierCertificateInfoBiz;
	}

	public SupplierCertificateInfo getSupplierCertificateInfo() {
		return supplierCertificateInfo;
	}

	public void setSupplierCertificateInfo(
			SupplierCertificateInfo supplierCertificateInfo) {
		this.supplierCertificateInfo = supplierCertificateInfo;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public ISupplierCertificateChangeBiz getiSupplierCertificateChangeBiz() {
		return iSupplierCertificateChangeBiz;
	}
	public void setiSupplierCertificateChangeBiz(
			ISupplierCertificateChangeBiz iSupplierCertificateChangeBiz) {
		this.iSupplierCertificateChangeBiz = iSupplierCertificateChangeBiz;
	}
	public SupplierCertificateChange getSupplierCertificateChange() {
		return supplierCertificateChange;
	}
	public void setSupplierCertificateChange(
			SupplierCertificateChange supplierCertificateChange) {
		this.supplierCertificateChange = supplierCertificateChange;
	}
	
}
