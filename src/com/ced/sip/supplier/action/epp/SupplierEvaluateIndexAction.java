package com.ced.sip.supplier.action.epp;


import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.IEvaluationIndexBiz;
import com.ced.sip.supplier.biz.IEvaluationIndexKindBiz;
import com.ced.sip.supplier.entity.EvaluationIndex;
import com.ced.sip.supplier.entity.EvaluationIndexKind;

public class SupplierEvaluateIndexAction extends BaseAction {
	
	//考核指标类别 服务类
	private IEvaluationIndexKindBiz iEvaluationIndexKindBiz  ;
	//考核指标信息 服务类
	private IEvaluationIndexBiz iEvaluationIndexBiz  ;
	//考核指标类别 实体类
	private EvaluationIndexKind evaluationIndexKind ;
	//考核指标类别 实体类
	private EvaluationIndexKind parentEvaluationIndexKind ;
	//考核指标信息 实体类
	private EvaluationIndex evaluationIndex ;
	//结构树 参数
	private String tree ;
	//消息 参数
	private String message ;
	//临时参数
	private String tempSupIndexInfo;
	

	/********************************考核指标类别管理*********************************/
	
	/**
	 * 查看考核指标类别首页列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewEvaluationIndexKindIndex() {

		return INDEX;
	}

	/**
	 * 查看考核指标类别表初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewEvaluationIndexKindInitTree() throws BaseException {
		
		try{
		} catch (Exception e) {
			log("查看考核指标类别表初始树列表错误！", e);
			throw new BaseException("查看考核指标类别表初始树列表错误！", e);
		}
		
		return TREE ;
	}
	
	/**
	 * 查看考核指标类别表树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void viewEvaluationIndexKindTree() throws BaseException {
		
		try{			
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			
			String id = this.getRequest().getParameter("id");
			if(StringUtil.isBlank(id)) id="0";
			
			// 初始父ID为零
			EvaluationIndexKind evaIndexKind = new EvaluationIndexKind() ;
			evaIndexKind.setParentId(new Long(id));
			evaIndexKind.setComId(comId);
			List<EvaluationIndexKind> evaIndexKindList = iEvaluationIndexKindBiz.getEvaluationIndexKindList( evaIndexKind );
			JSONArray jsonArray = new JSONArray();
			JSONObject subJsonObject =null;
			for (EvaluationIndexKind evaIndex:evaIndexKindList) {
				subJsonObject = new JSONObject();
				subJsonObject.element("id", evaIndex.getEikId());
				subJsonObject.element("pid", evaIndex.getParentId());
				subJsonObject.element("name",evaIndex.getEikName());
				
				if("0".equals(evaIndex.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);
					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log("查看考核指标类别表初始树列表错误！", e);
			throw new BaseException("查看考核指标类别表初始树列表错误！", e);
		}
		
	}
	
	/**
	 * 保存考核指标类别初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluationIndexKindInit() throws BaseException {
		try{
			if(StringUtil.isNotBlank(evaluationIndexKind.getEikId())){
			    evaluationIndexKind = this.iEvaluationIndexKindBiz.getEvaluationIndexKind( evaluationIndexKind.getEikId() ) ;
			}			
		} catch (Exception e) {
			log("保存考核指标类别初始化错误！", e);
			throw new BaseException("保存考核指标类别初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存考核指标类别
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluationIndexKind() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			if(evaluationIndexKind.getParentId()==null){
				parentEvaluationIndexKind=new EvaluationIndexKind();
				parentEvaluationIndexKind.setIsHaveChild("0");
				parentEvaluationIndexKind.setLevels(0);
				parentEvaluationIndexKind.setSelflevCode("");
				parentEvaluationIndexKind.setEikId(0L);
			}else{
				// 更新父节点中是否有子节点为存在 
				parentEvaluationIndexKind = this.iEvaluationIndexKindBiz.getEvaluationIndexKind( evaluationIndexKind.getParentId() ) ;
				parentEvaluationIndexKind.setIsHaveChild("0");
				this.iEvaluationIndexKindBiz.updateEvaluationIndexKind(parentEvaluationIndexKind);
			}
			//保存下级节点信息
			evaluationIndexKind.setWriter(UserRightInfoUtil.getUserName( this.getRequest() ));
			evaluationIndexKind.setWriterDate(DateUtil.getCurrentDateTime());
			evaluationIndexKind.setIsHaveChild("1");
			evaluationIndexKind.setLevels(parentEvaluationIndexKind.getLevels()+1);
			evaluationIndexKind.setParentId(parentEvaluationIndexKind.getEikId());
			evaluationIndexKind.setComId(comId);
			this.iEvaluationIndexKindBiz.saveEvaluationIndexKind( evaluationIndexKind );
			
			//更新指标类别级联码
			evaluationIndexKind.setSelflevCode( parentEvaluationIndexKind.getSelflevCode() + "," + evaluationIndexKind.getEikId().toString() ); 
			this.iEvaluationIndexKindBiz.updateEvaluationIndexKind(evaluationIndexKind);
			//BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_08 ) ;
			evaluationIndexKind.setOperType("add");
			message = " 新增成功! " ;
			
			this.getRequest().setAttribute("message", "新增成功");
		    this.getRequest().setAttribute("operModule", "新增考核指标类别");
		} catch (Exception e) {
			log("保存考核指标类别错误！", e);
			throw new BaseException("保存考核指标类别错误！", e);
		}
		
		return "message";
		
	}
	
	/**
	 * 修改考核指标类别初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluationIndexKindInit() throws BaseException {
		
		try{
			if(evaluationIndexKind!=null){
				evaluationIndexKind=this.iEvaluationIndexKindBiz.getEvaluationIndexKind( evaluationIndexKind.getEikId() );
				if(evaluationIndexKind.getParentId()!=0)
				{
					EvaluationIndexKind peve=this.iEvaluationIndexKindBiz.getEvaluationIndexKind( evaluationIndexKind.getParentId() );
					this.getRequest().setAttribute("peveName", peve.getEikName());
				}else{
					this.getRequest().setAttribute("peveName", "考核指标类别库");
				}
			}
		} catch (Exception e) {
			log("修改考核指标类别初始化错误！", e);
			throw new BaseException("修改考核指标类别初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改考核指标类别
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluationIndexKind() throws BaseException {
		
		try{
			if(evaluationIndexKind!=null){
				evaluationIndexKind.setWriter(UserRightInfoUtil.getUserName( this.getRequest() ) );
				evaluationIndexKind.setWriterDate(DateUtil.getCurrentDateTime());
				this.iEvaluationIndexKindBiz.updateEvaluationIndexKind( evaluationIndexKind );
				//更新指标信息
				evaluationIndex = new EvaluationIndex();
				evaluationIndex.setEikId(evaluationIndexKind.getEikId());
				this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(evaluationIndex));
				if(this.getListValue()!=null&&this.getListValue().size()>0){
					for(int i=0;i<this.getListValue().size();i++){
						EvaluationIndex evaIndex = (EvaluationIndex)this.getListValue().get(i);
						evaIndex.setEikName(evaluationIndexKind.getEikName());
						this.iEvaluationIndexBiz.updateEvaluationIndex(evaIndex);
					}
				}
				//BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_08 ) ;
			}
			evaluationIndexKind.setOperType("update");
			message = " 修改成功! " ;
			
			this.getRequest().setAttribute("message", "修改成功");
		    this.getRequest().setAttribute("operModule", "修改考核指标类别");
		} catch (Exception e) {
			log("修改考核指标类别错误！", e);
			throw new BaseException("修改考核指标类别错误！", e);
		}
		return "message";
		
	}
	
	/**
	 * 删除考核指标类别
	 * @return
	 * @throws BaseException 
	 */
	public String deleteEvaluationIndexKind() throws BaseException {
		try{
			String eikId=this.getRequest().getParameter("eikId");
			
			evaluationIndexKind=this.iEvaluationIndexKindBiz.getEvaluationIndexKind( new Long(eikId) );
			evaluationIndexKind.setIsUsable( TableStatus.COMMON_STATUS_INVALID ) ;
			this.iEvaluationIndexKindBiz.updateEvaluationIndexKind( evaluationIndexKind );
			if(evaluationIndexKind.getEikId()!=null){
				EvaluationIndexKind tf = new EvaluationIndexKind();
				tf.setParentId(evaluationIndexKind.getEikId());
				List childrenList = this.iEvaluationIndexKindBiz.getEvaluationIndexKindList(tf);
				if(childrenList!=null&&childrenList.size()>0){
					for(int j=0;j<childrenList.size();j++){
						EvaluationIndexKind info = (EvaluationIndexKind)childrenList.get(j);
						if(info.getEikId()!=null){
							this.iEvaluationIndexKindBiz.deleteEvaluationIndexKind(info);
						}
					}
				}
			}
			//BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_08 ) ;
			evaluationIndexKind.setOperType("delete");
			PrintWriter out = this.getResponse().getWriter();
			String message="删除成功";
			out.println(message);
			
			this.getRequest().setAttribute("message", "删除成功");
		    this.getRequest().setAttribute("operModule", "删除考核指标类别");
		} catch (Exception e) {
			log("删除考核指标类别错误！", e);
			throw new BaseException("删除考核指标类别错误！", e);
		}
		return null;
		
	}
	
	/********************************考核指标信息管理*********************************/
	/**
	 * 查看考核指标信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewEvaluationIndex() throws BaseException {
		
		try{
		//	System.out.println("sssss");
		} catch (Exception e) {
			log.error("查看考核指标信息列表错误！", e);
			throw new BaseException("查看考核指标信息列表错误！", e);
		}
		
		return VIEW ;
		
	}
	public String viewToEvaluationIndex() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			if(evaluationIndex==null)
			{
				evaluationIndex=new EvaluationIndex();
			}
			String eiName=this.getRequest().getParameter("eiName");
			evaluationIndex.setEiName(eiName);
			evaluationIndex.setComId(comId);
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPageDataTables(), evaluationIndex));
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看考核指标信息列表错误！", e);
			throw new BaseException("查看考核指标信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存考核指标信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluationIndexInit() throws BaseException {
		try{
			parentEvaluationIndexKind = this.iEvaluationIndexKindBiz.getEvaluationIndexKind(evaluationIndexKind.getEikId());
			
			
		} catch (Exception e) {
			log("保存考核指标信息初始化错误！", e);
			throw new BaseException("保存考核指标信息初始化错误！", e);
		}
		return "addInit_info";
		
	}
	
	/**
	 * 保存考核指标信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveEvaluationIndex() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			evaluationIndex.setWriter(UserRightInfoUtil.getUserName( this.getRequest() ));
			evaluationIndex.setWriteDate(DateUtil.getCurrentDateTime());
			evaluationIndex.setComId(comId);
			this.iEvaluationIndexBiz.saveEvaluationIndex( evaluationIndex );
			
			this.getRequest().setAttribute("message", "保存成功");
		    this.getRequest().setAttribute("operModule", "保存考核指标信息");
		} catch (Exception e) {
			log("保存考核指标信息错误！", e);
			throw new BaseException("保存考核指标信息错误！", e);
		}
		
		return "add_info";
		
	}
	
	/**
	 * 修改考核指标信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluationIndexInit() throws BaseException {
		
		try{
			evaluationIndex=this.iEvaluationIndexBiz.getEvaluationIndex( evaluationIndex.getEiId() );
		} catch (Exception e) {
			log("修改考核指标信息初始化错误！", e);
			throw new BaseException("修改考核指标信息初始化错误！", e);
		}
		return "modifyInit_info";
		
	}
	
	/**
	 * 修改考核指标信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateEvaluationIndex() throws BaseException {
		
		try{
			this.iEvaluationIndexBiz.updateEvaluationIndex( evaluationIndex );
			//BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_08 ) ;
			
			this.getRequest().setAttribute("message", "修改成功");
		    this.getRequest().setAttribute("operModule", "修改考核指标信息");
		} catch (Exception e) {
			log("修改考核指标信息错误！", e);
			throw new BaseException("修改考核指标信息错误！", e);
		}
		return "modify_info";
		
	}
	
	/**
	 * 删除考核指标信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteEvaluationIndex() throws BaseException {
		try{
			if(tempSupIndexInfo!=null){
				String[] supIdArr = tempSupIndexInfo.split(",");
				for(int i=0;i<supIdArr.length;i++){
					if(supIdArr[i]!=null){
						evaluationIndex = this.iEvaluationIndexBiz.getEvaluationIndex(Long.parseLong(supIdArr[i]));
						if(evaluationIndex!=null){
							evaluationIndex.setIsUsable("1");
							this.iEvaluationIndexBiz.updateEvaluationIndex(evaluationIndex);
						}
					}
				}
				
				PrintWriter out = this.getResponse().getWriter();
				String message="删除成功";
				out.println(message);
				this.getRequest().setAttribute("message", "删除成功");
			    this.getRequest().setAttribute("operModule", "删除考核指标信息");
			}
			//BaseDataInfosUtil.initBaseIndexInfos( TableStatus.BASE_DATA_TYPE_03 ) ;
		} catch (Exception e) {
			log("删除考核指标信息错误！", e);
			throw new BaseException("删除考核指标信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看考核指标明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewEvaluationIndexDetail() throws BaseException {
		
		try{
			evaluationIndex=this.iEvaluationIndexBiz.getEvaluationIndex( evaluationIndex.getEiId() );
		} catch (Exception e) {
			log("查看考核指标明细信息错误！", e);
			throw new BaseException("查看考核指标明细信息错误！", e);
		}
		return "detail_info";
		
	}

	/**
	 * 查看部门信息首页列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDeptIndex() {
		
		this.getRequest().setAttribute("ul", evaluationIndex.getEiIdString());
		this.getRequest().setAttribute("eiName", evaluationIndex.getEiName());
		return INDEX ;
	}
	

/**
 * 查看部门信息表初始树列表 getTree
 * @return
 * @throws BaseException 
 * @Action
 */
public String viewDeptInitTree() throws BaseException {
	
	try{
		
	} catch (Exception e) {
		log.error("查看部门信息表初始树列表错误！", e);
		throw new BaseException("查看部门信息表初始树列表错误！", e);
	}
	
	return "tree" ;
}
public String viewDeptTree() throws BaseException {
	
	try{
		Long comId=UserRightInfoUtil.getComId(this.getRequest());
		String id = this.getRequest().getParameter("id");
		if(StringUtil.isBlank(id)) id="0";
		// 初始父ID为零
		EvaluationIndexKind evaIndexKind = new EvaluationIndexKind() ;
		evaIndexKind.setParentId(new Long(id));
		evaIndexKind.setComId(comId);
		List<EvaluationIndexKind> evaIndexKindList = iEvaluationIndexKindBiz.getEvaluationIndexKindList( evaIndexKind );
		JSONArray jsonArray = new JSONArray();
		//System.out.println(evaIndexKindList.size());
		for (EvaluationIndexKind evaIndex:evaIndexKindList) {
			JSONObject subJsonObject = new JSONObject();
			subJsonObject.element("id", evaIndex.getEikId());
			subJsonObject.element("pid", evaIndex.getParentId());
			subJsonObject.element("name",evaIndex.getEikName());
			
			if("0".equals(evaIndex.getIsHaveChild()))
			{
				subJsonObject.element("isParent", true);
				
			}
			
			jsonArray.add(subJsonObject);
		}
		PrintWriter writer = getResponse().getWriter();  
		//System.out.println(jsonArray);
        writer.print(jsonArray);  
        writer.flush();  
        writer.close(); 
        
	} catch (Exception e) {
		log.error("查看部门信息表初始树列表错误！", e);
		throw new BaseException("查看部门信息表初始树列表错误！", e);
	}
	
	return "tree" ;
}
/*
 * top_frameset
 * 框架上部
 * 
 * */
public String viewTopIndex() throws Exception {
	
	this.getRequest().setAttribute("eiName", evaluationIndex.getEiName());
	this.getRequest().setAttribute("parentId", evaluationIndexKind.getParentId());
	
	/*if(StringUtil.isNotBlank(evaluationIndexKind)){
		if(StringUtil.isNotBlank(evaluationIndexKind.getParentId())){
			evaluationIndex.setEikId(evaluationIndexKind.getParentId());
			evaluationIndex.setEiName("");
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPage1(),evaluationIndex));
//			if(departments.getSelflevCode().split(",").length<=2){
//				users.setDepId(null);
//			}
		}
	}else {
		if (evaluationIndex.getEiName().equals("0")) {
			evaluationIndex.setEiName("");
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPage1(), evaluationIndex));
		}else{
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPage1(), evaluationIndex));
		}
	}*/
	return "top" ;
}
public String findTopIndex() throws Exception {
	Long comId=UserRightInfoUtil.getComId(this.getRequest());
	String parentId=this.getRequest().getParameter("parentId");
	if(!parentId.equals("0"))
	{
		evaluationIndexKind=new EvaluationIndexKind();
		evaluationIndexKind.setParentId(new Long(parentId));
	}
	if(evaluationIndex==null)
	{
		evaluationIndex=new EvaluationIndex();
	}
	String eiName=this.getRequest().getParameter("eiName");
	evaluationIndex.setEiName(eiName);
	evaluationIndex.setComId(comId);
	if(StringUtil.isNotBlank(evaluationIndexKind)){
		if(StringUtil.isNotBlank(evaluationIndexKind.getParentId())){
			evaluationIndex.setEikId(evaluationIndexKind.getParentId());
			evaluationIndex.setEiName("");
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPageDataTables(),evaluationIndex));
//			if(departments.getSelflevCode().split(",").length<=2){
//				users.setDepId(null);
//			}
		}
	}else {
		if (evaluationIndex.getEiName().equals("0")) {
			evaluationIndex.setEiName("");
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPageDataTables(), evaluationIndex));
		}else{
			this.setListValue(this.iEvaluationIndexBiz.getEvaluationIndexList(this.getRollPageDataTables(), evaluationIndex));
		}
	}
	
	this.getPagejsonDataTables(this.getListValue());
	return null;
}
/*
 * button_frameset
 * 框架下部
 * 
 * */

public String viewButtonIndex() throws Exception {
	
	if( StringUtil.isNotBlank(evaluationIndex) &&!(evaluationIndex.getEiIdString().equals("-1"))){
		
		String [] mn = evaluationIndex.getEiIdString().toString().split(",");
		String mc ="";
		for(int i=0;i<mn.length;i++){
			String m=iEvaluationIndexBiz.getEvaluationIndex(new Long(mn[i])).getEiName();
			//String m=BaseDataInfosUtil.convertUserIdToChnName(new Long(mn[i])).toString();
			mc+=mn[i]+":"+m+",";
		}
		//System.out.println( mc.substring(0, mc.lastIndexOf(",")));
		this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
	}else{
	this.getRequest().setAttribute("ul", "-1");
	}
	
	return "button" ;
}
	
	public IEvaluationIndexKindBiz getiEvaluationIndexKindBiz() {
		return iEvaluationIndexKindBiz;
	}

	public void setiEvaluationIndexKindBiz(
			IEvaluationIndexKindBiz iEvaluationIndexKindBiz) {
		this.iEvaluationIndexKindBiz = iEvaluationIndexKindBiz;
	}

	public EvaluationIndexKind getEvaluationIndexKind() {
		return evaluationIndexKind;
	}

	public void setEvaluationIndexKind(EvaluationIndexKind evaluationIndexKind) {
		this.evaluationIndexKind = evaluationIndexKind;
	}

	public EvaluationIndexKind getParentEvaluationIndexKind() {
		return parentEvaluationIndexKind;
	}

	public void setParentEvaluationIndexKind(
			EvaluationIndexKind parentEvaluationIndexKind) {
		this.parentEvaluationIndexKind = parentEvaluationIndexKind;
	}

	public IEvaluationIndexBiz getiEvaluationIndexBiz() {
		return iEvaluationIndexBiz;
	}

	public void setiEvaluationIndexBiz(IEvaluationIndexBiz iEvaluationIndexBiz) {
		this.iEvaluationIndexBiz = iEvaluationIndexBiz;
	}

	public EvaluationIndex getEvaluationIndex() {
		return evaluationIndex;
	}

	public void setEvaluationIndex(EvaluationIndex evaluationIndex) {
		this.evaluationIndex = evaluationIndex;
	}

	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTempSupIndexInfo() {
		return tempSupIndexInfo;
	}

	public void setTempSupIndexInfo(String tempSupIndexInfo) {
		this.tempSupIndexInfo = tempSupIndexInfo;
	}

	
}
