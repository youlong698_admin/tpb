package com.ced.sip.supplier.action.epp;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;

import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierCertificateChangeBiz;
import com.ced.sip.supplier.biz.ISupplierCertificateInfoBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.biz.ISupplierInfoChangeBiz;
import com.ced.sip.supplier.entity.SupplierCertificateChange;
import com.ced.sip.supplier.entity.SupplierCertificateInfo;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierInfoChange;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.entity.MaterialKind;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplierBaseInfoAction extends BaseAction {
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz;

	// 供应商基本信息变更服务类
	private ISupplierInfoChangeBiz iSupplierInfoChangeBiz;

	// 供应商资质信息 服务类
	private ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz;

	// 供应商资质信息变更服务类
	private ISupplierCertificateChangeBiz iSupplierCertificateChangeBiz;

	// 主营产品类别
	private IMaterialBiz iMaterialBiz;

	// 供应商基本信息 实例
	private SupplierInfo supplierInfo;


	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;

	/**
	 * 查看供应商基本信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierBaseInfo() throws BaseException {
		this.getRequest().setAttribute(
				"statusMap",
				BaseDataInfosUtil
						.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713));
		this.getRequest().setAttribute(
				"levelMap",
				BaseDataInfosUtil
						.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1712));

		return VIEW;
	}

	/**
	 * 供应商注册信息审核
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierBaseInfoAudit() throws BaseException {
		return "viewAudit";
	}

	/**
	 * 供应商变更信息审核
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierBaseInfoChange() throws BaseException {
		this.getRequest().setAttribute(
				"statusMap",
				BaseDataInfosUtil
						.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713));
		this.getRequest().setAttribute(
				"levelMap",
				BaseDataInfosUtil
						.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1712));

		return "viewChange";
	}

	/**
	 * 查看供应商状态变更表信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierStatusChange() throws BaseException {
		try {
			this
					.getRequest()
					.setAttribute(
							"statusMap",
							BaseDataInfosUtil
									.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713));
			this
					.getRequest()
					.setAttribute(
							"levelMap",
							BaseDataInfosUtil
									.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1712));
		} catch (Exception e) {
			log.error("查看供应商状态变更表信息列表错误！", e);
			throw new BaseException("查看供应商状态变更表信息列表错误！", e);
		}

		return "viewSupplierStatusChange";
	}

	/**
	 * 查看供应商基本信息tab
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierSelectInfo() throws BaseException {
		try {
		} catch (Exception e) {
			log("查看供应商基本信息tab初始化错误！", e);
			throw new BaseException("查看供应商基本信息tab初始化错误！", e);
		}

		return "select_index";
	}

	/**
	 * 审核供应商注册信息tab
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierSelectInfoAudit() throws BaseException {
		try {
		} catch (Exception e) {
			log("查看供应商基本信息tab初始化错误！", e);
			throw new BaseException("查看供应商基本信息tab初始化错误！", e);
		}

		return "select_index_audit";
	}

	/**
	 * 审核供应商注册信息tab
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierSelectInfoChange() throws BaseException {
		try {
		} catch (Exception e) {
			log("查看供应商基本信息tab初始化错误！", e);
			throw new BaseException("查看供应商基本信息tab初始化错误！", e);
		}

		return "select_index_change";
	}

	/**
	 * 查询供应商信息列表
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierBaseInfo() throws BaseException {
		try {
			supplierInfo = new SupplierInfo();

			String sign = this.getRequest().getParameter("sign"); // sign 1为注册审核
																	//  2位变更审核																	// 
																	// 3供应商状态变更

			String supplierName = this.getRequest()
					.getParameter("supplierName");
			supplierInfo.setSupplierName(supplierName);

			String contactPerson = this.getRequest().getParameter(
					"contactPerson");
			supplierInfo.setContactPerson(contactPerson);

			List<SupplierInfo> list = this.iSupplierInfoBiz
					.getSupplierInfoListAudit(this.getRollPageDataTables(),
							supplierInfo, sign);
			for(SupplierInfo supplierInfo:list){
				supplierInfo.setStatusCn(BaseDataInfosUtil
						.convertDictCodeToName(supplierInfo.getStatus(),
								DictStatus.COMMON_DICT_TYPE_1713));
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看供应商基本信息列表错误！", e);
			throw new BaseException("查看供应商基本信息列表错误！", e);
		}

		return null;
	}

	/**
	 * 查询所有的供应商信息列表 包含各种状态
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String findAllSupplierBaseInfo() throws BaseException {
		try {
			supplierInfo = new SupplierInfo();

			String supplierName = this.getRequest()
					.getParameter("supplierName");
			supplierInfo.setSupplierName(supplierName);

			String contactPerson = this.getRequest().getParameter(
					"contactPerson");
			supplierInfo.setContactPerson(contactPerson);

			List<SupplierInfo> list = this.iSupplierInfoBiz
					.getSupplierInfoListAll(this.getRollPageDataTables(),
							supplierInfo);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看供应商基本信息列表(包含各种状态)错误！", e);
			throw new BaseException("查看供应商基本信息列表(包含各种状态)错误！", e);
		}

		return null;
	}

	/**
	 * 查看供应商基本明细信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewSupplierBaseInfoDetail() throws BaseException {
		try {
			supplierInfo = this.iSupplierInfoBiz.getSupplierInfo(supplierInfo
					.getSupplierId());

			if (StringUtil.isNotBlank(supplierInfo.getProdCodes())) {
				String[] proKindIds = supplierInfo.getProdCodes().split(",");
				supplierInfo.setProdKindNames(this
						.convertProKindIdToProKindName(proKindIds));
			}

			supplierInfo.setSupplierTypeCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getSupplierType(),
							DictStatus.COMMON_DICT_TYPE_1710));
			supplierInfo.setIndustryOwnedCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getIndustryOwned(),
							DictStatus.COMMON_DICT_TYPE_1707));
			supplierInfo.setManagementModelCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getManagementModel(),
							DictStatus.COMMON_DICT_TYPE_1708));
		} catch (Exception e) {
			log("查看供应商基本明细信息错误！", e);
			throw new BaseException("查看供应商基本明细信息错误！", e);
		}

		return DETAIL;
	}

	/**
	 * 查看供应商基本明细信息(变更信息审核)
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewSupplierBaseInfoDetailChange() throws BaseException {
		try {
			supplierInfo = this.iSupplierInfoBiz.getSupplierInfo(supplierInfo
					.getSupplierId());

			if (StringUtil.isNotBlank(supplierInfo.getProdCodes())) {
				String[] proKindIds = supplierInfo.getProdCodes().split(",");
				supplierInfo.setProdKindNames(this
						.convertProKindIdToProKindName(proKindIds));
			}

			supplierInfo.setSupplierTypeCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getSupplierType(),
							DictStatus.COMMON_DICT_TYPE_1710));
			supplierInfo.setIndustryOwnedCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getIndustryOwned(),
							DictStatus.COMMON_DICT_TYPE_1707));
			supplierInfo.setManagementModelCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getManagementModel(),
							DictStatus.COMMON_DICT_TYPE_1708));
			SupplierInfoChange supplierInfoChange = this.iSupplierInfoChangeBiz
					.getSupplierInfoChangeBySupplierId(supplierInfo
							.getSupplierId());
			this.getRequest().setAttribute("supplierInfoChange",
					supplierInfoChange);
		} catch (Exception e) {
			log("查看供应商基本明细信息(变更信息审核)错误！", e);
			throw new BaseException("查看供应商基本明细信息(变更信息审核)错误！", e);
		}

		return "detail_change";
	}

	/**
	 * 供应商基本信息Excel导出
	 * 
	 * @return
	 * @throws BaseException
	 */
	public void exportSupplierBaseInfoExcel() throws BaseException {
		try {
			List<String> titleList = new ArrayList<String>();
			titleList.add("公司名称");
			titleList.add("公司简称");
			titleList.add("成立日期");
			titleList.add("企业性质");
			titleList.add("企业法人");
			titleList.add("注册资金");
			titleList.add("组织机构代码证编号");
			titleList.add("地址");
			titleList.add("公司电话");
			titleList.add("公司传真");
			titleList.add("联系人");
			titleList.add("联系手机号");
			titleList.add("联系人E-mail");
			titleList.add("企业简介");
			titleList.add("级别");
			titleList.add("状态");

			List list = this.iSupplierInfoBiz.getSupplierInfoList(supplierInfo);
			List<Object[]> objList = new ArrayList<Object[]>();

			for (int i = 0; i < list.size(); i++) {
				SupplierInfo supplierinfo = (SupplierInfo) list.get(i);

				if (StringUtil.isNotBlank(supplierinfo)) {
					supplierinfo.setSupplierTypeCn(BaseDataInfosUtil
							.convertDictCodeToName(supplierinfo
									.getSupplierType(), "1710"));

					Object[] obj = new Object[] {
							supplierinfo.getSupplierName(),
							supplierinfo.getSupplierNameSimple(),
							DateUtil.getWateStringFromDate(supplierinfo
									.getCreateDate()),
							supplierinfo.getSupplierTypeCn(),
							supplierinfo.getLegalPerson(),
							supplierinfo.getRegisterFunds(),
							supplierinfo.getOrgCode(),
							supplierinfo.getSupplierAddress(),
							supplierinfo.getSupplierPhone(),
							supplierinfo.getSupplierFax(),
							supplierinfo.getContactPerson(),
							supplierinfo.getMobilePhone(),
							supplierinfo.getContactEmail(),
							supplierinfo.getIntroduce(),
							supplierinfo.getSupplierLevel(),
							BaseDataInfosUtil
							.convertDictCodeToName(supplierInfo.getStatus(),
									DictStatus.COMMON_DICT_TYPE_1713)};
					objList.add(obj);
				}
			}

			// 输出的excel文件名
			String file = "供应商基本信息.xls";
			String targetfile = this.getServletContext().getRealPath(file);

			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("worksheet", "供应商基本信息");
			map.put("titleList", titleList);
			map.put("valueList", objList);
			excelList.add(map);
			new ExcelUtil().expCommonExcel(targetfile, excelList);
			this.getResponse().setContentType(
					"application/octet-stream; charset=utf-8");
			this.getResponse().setHeader(
					"Content-Disposition",
					"attachment; filename="
							+ new String(file.getBytes("gbk"), "iso-8859-1"));

			// this.getResponse().setHeader( "Set-Cookie",
			// "name=value; HttpOnly");
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			int i = 0;
			readCount = is.read(buff);

			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}

			if (is != null) {
				is.close();
			}

			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			log.error("导出供应商查询信息列表错误！", e);
			throw new BaseException("导出采购进度查询信息列表错误！", e);
		}
	}

	/**
	 * 根据产品类别主键获得名称
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String convertProKindIdToProKindName(String[] proKindIds)
			throws BaseException {
		String proKindNames = "";
		MaterialKind materialKind = null;

		try {
			if ((proKindIds != null) && (proKindIds.length > 0)) {
				for (int i = 1; i < proKindIds.length - 1; i++) {
					if (proKindIds[i] != "") {
						materialKind = this.iMaterialBiz.getMaterialKind(Long
										.parseLong(proKindIds[i]));
						proKindNames += (materialKind.getMkName() + ",");
					}
				}
			}

			proKindNames = StringUtil.subStringLastOfSeparator(proKindNames,
					',');
		} catch (Exception e) {
			log("根据产品类别主键获得名称信息错误！", e);
			throw new BaseException("根据产品类别主键获得名称信息错误！", e);
		}

		return proKindNames;
	}

	/**
	 * 根据供应商主键获得名称
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String convertSupplierIdToSupplierName(String[] supplierIds)
			throws BaseException {
		String supplierNames = "";

		try {
			if ((supplierIds != null) && (supplierIds.length > 0)) {
				for (int i = 0; i < supplierIds.length; i++) {
					if (supplierIds[i] != "") {
						supplierNames += ",";
					}
				}
			}

			supplierNames = StringUtil.subStringLastOfSeparator(supplierNames,
					',');
		} catch (Exception e) {
			log("根据供应商主键获得名称信息错误！", e);
			throw new BaseException("根据供应商主键获得名称信息错误！", e);
		}

		return supplierNames;
	}

	/**
	 * 供应商注册信息审核提交
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateSupplierInfoAudit() throws BaseException {
		try {
			String result = this.getRequest().getParameter("result");

			// 审核通过时候更新是否注册字段为0
			if (result.equals("0")) {
				this.iSupplierInfoBiz.updateSupplierInfoByColumn("is_register",
						"0", supplierInfo.getSupplierId());
			}

			this.iSupplierInfoBiz.updateSupplierInfoByColumn("is_audit",
					result, supplierInfo.getSupplierId());

			if (result.equals("0")) {
				this.iSupplierInfoBiz.updateSupplierInfoByColumn("status", "1",
						supplierInfo.getSupplierId()); // 更新供应商状态为合格供应商
			} else if (result.equals("2")) {
				this.iSupplierInfoBiz.updateSupplierInfoByColumn("is_register",
						"1", supplierInfo.getSupplierId());
			}

			PrintWriter out = this.getResponse().getWriter();
			String message = "供应商注册信息审核成功";
			this.getRequest().setAttribute("message", "审核成功");
			this.getRequest().setAttribute("operModule", "供应商注册信息审核成功");
			out.print(message);
		} catch (Exception e) {
			log("供应商注册信息审核提交信息错误！", e);
			throw new BaseException("供应商注册信息审核提交信息错误！", e);
		}

		return null;
	}

	/**
	 * 供应商变更信息审核提交
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateSupplierInfoChange() throws BaseException {
		try {
			String result = this.getRequest().getParameter("result");
			this.iSupplierInfoBiz.updateSupplierInfoByColumn("is_change",
					result, supplierInfo.getSupplierId());

			// 审核成功时候，需要更新supplier_info 表，处理supplier_certificate_info表
			if (result.equals("0")) {
				SupplierInfoChange supplierInfoChange = this.iSupplierInfoChangeBiz
						.getSupplierInfoChangeBySupplierId(supplierInfo
								.getSupplierId());
				supplierInfo = this.iSupplierInfoBiz
						.getSupplierInfo(supplierInfo.getSupplierId());

				if (StringUtil.isNotBlank(supplierInfoChange.getSupplierName())) {
					supplierInfo.setSupplierName(supplierInfoChange
							.getSupplierName());
				}

				if (StringUtil.isNotBlank(supplierInfoChange
						.getSupplierNameSimple())) {
					supplierInfo.setSupplierNameSimple(supplierInfoChange
							.getSupplierNameSimple());
				}

				if (StringUtil.isNotBlank(supplierInfoChange
						.getSupplierAddress())) {
					supplierInfo.setSupplierAddress(supplierInfoChange
							.getSupplierAddress());
				}

				if (StringUtil.isNotBlank(supplierInfoChange.getSupplierFax())) {
					supplierInfo.setSupplierFax(supplierInfoChange
							.getSupplierFax());
				}

				if (StringUtil
						.isNotBlank(supplierInfoChange.getSupplierPhone())) {
					supplierInfo.setSupplierPhone(supplierInfoChange
							.getSupplierPhone());
				}

				if (StringUtil.isNotBlank(supplierInfoChange.getLegalPerson())) {
					supplierInfo.setLegalPerson(supplierInfoChange
							.getLegalPerson());
				}

				if (StringUtil
						.isNotBlank(supplierInfoChange.getRegisterFunds())) {
					supplierInfo.setRegisterFunds(supplierInfoChange
							.getRegisterFunds());
				}

				if (StringUtil.isNotBlank(supplierInfoChange.getCreateDate())) {
					supplierInfo.setCreateDate(supplierInfoChange
							.getCreateDate());
				}

				this.iSupplierInfoBiz.updateSupplierInfo(supplierInfo);

				supplierInfoChange.setIsChangeEffect(0);
				this.iSupplierInfoChangeBiz
						.updateSupplierInfoChange(supplierInfoChange);

				SupplierCertificateInfo supplierCertificateInfo = null;
				List<SupplierCertificateChange> list = this.iSupplierCertificateChangeBiz
						.getSupplierCertificateChangeList(this
								.getRollPageDataTables(), null);

				for (SupplierCertificateChange supplierCertificateChange : list) {
					supplierCertificateInfo = iSupplierCertificateInfoBiz
							.getSupplierCertificateInfo(supplierCertificateChange
									.getSciId());
					supplierCertificateInfo
							.setCertificateName(supplierCertificateChange
									.getCertificateName());
					supplierCertificateInfo
							.setCertificateCode(supplierCertificateChange
									.getCertificateCode());
					supplierCertificateInfo
							.setTimeBegain(supplierCertificateChange
									.getTimeBegain());
					supplierCertificateInfo
							.setTimeEnd(supplierCertificateChange.getTimeEnd());
					supplierCertificateInfo
							.setCertificateProfession(supplierCertificateChange
									.getCertificateProfession());
					supplierCertificateInfo.setRemark(supplierCertificateChange
							.getRemark());
					this.iSupplierCertificateInfoBiz
							.updateSupplierCertificateInfo(supplierCertificateInfo);

					// 删除原有的资质附件
					List<Attachment> list_old = iAttachmentBiz
							.getAttachmentList(new Attachment(
									supplierCertificateInfo.getSciId(),
									AttachmentStatus.ATTACHMENT_CODE_602));

					for (Attachment attachment : list_old) {
						iAttachmentBiz.deleteAttachment(attachment);
					}

					// 变更资质附件
					List<Attachment> list_new = iAttachmentBiz
							.getAttachmentList(new Attachment(
									supplierCertificateChange.getScicId(),
									AttachmentStatus.ATTACHMENT_CODE_6021));

					for (Attachment attachment : list_new) {
						attachment.setAttachmentTypeId(supplierCertificateInfo
								.getSciId());
						attachment
								.setAttachmentField(AttachmentStatus.ATTACHMENT_CODE_602);
						iAttachmentBiz.saveAttachment(attachment);
					}

					supplierCertificateChange.setIsChangeEffect(0);
					this.iSupplierCertificateChangeBiz
							.updateSupplierCertificateChange(supplierCertificateChange);
				}
			}

			if (result.equals("2")) {
				this.iSupplierInfoBiz.updateSupplierInfoByColumn("is_change",
						"2", supplierInfo.getSupplierId());

				SupplierInfoChange supplierInfoChange = this.iSupplierInfoChangeBiz
						.getSupplierInfoChangeBySupplierId(supplierInfo
								.getSupplierId());
				supplierInfoChange.setIsChangeEffect(2);
				this.iSupplierInfoChangeBiz
						.updateSupplierInfoChange(supplierInfoChange);

				List<SupplierCertificateChange> list = this.iSupplierCertificateChangeBiz
						.getSupplierCertificateChangeList(this
								.getRollPageDataTables(), null);

				for (SupplierCertificateChange supplierCertificateChange : list) {
					supplierCertificateChange.setIsChangeEffect(2);
					this.iSupplierCertificateChangeBiz
							.updateSupplierCertificateChange(supplierCertificateChange);
				}
			}

			PrintWriter out = this.getResponse().getWriter();
			String message = "供应商变更信息审核成功";
			this.getRequest().setAttribute("message", "审核成功");
			this.getRequest().setAttribute("operModule", "供应商变更信息审核成功");
			out.print(message);
		} catch (Exception e) {
			log("供应商变更信息审核提交信息错误！", e);
			throw new BaseException("供应商变更信息审核提交信息错误！", e);
		}

		return null;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}

	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}

	public ISupplierInfoChangeBiz getiSupplierInfoChangeBiz() {
		return iSupplierInfoChangeBiz;
	}

	public void setiSupplierInfoChangeBiz(
			ISupplierInfoChangeBiz iSupplierInfoChangeBiz) {
		this.iSupplierInfoChangeBiz = iSupplierInfoChangeBiz;
	}

	public ISupplierCertificateInfoBiz getiSupplierCertificateInfoBiz() {
		return iSupplierCertificateInfoBiz;
	}

	public void setiSupplierCertificateInfoBiz(
			ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz) {
		this.iSupplierCertificateInfoBiz = iSupplierCertificateInfoBiz;
	}

	public ISupplierCertificateChangeBiz getiSupplierCertificateChangeBiz() {
		return iSupplierCertificateChangeBiz;
	}

	public void setiSupplierCertificateChangeBiz(
			ISupplierCertificateChangeBiz iSupplierCertificateChangeBiz) {
		this.iSupplierCertificateChangeBiz = iSupplierCertificateChangeBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}

	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}
}
