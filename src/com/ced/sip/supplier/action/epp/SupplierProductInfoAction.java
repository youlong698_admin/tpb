package com.ced.sip.supplier.action.epp;


import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.supplier.biz.ISupplierImgBiz;
import com.ced.sip.supplier.biz.ISupplierProductInfoBiz;
import com.ced.sip.supplier.entity.SupplierImg;
import com.ced.sip.supplier.entity.SupplierProductInfo;

public class SupplierProductInfoAction extends BaseAction {
	// 供应商产品信息 服务类
	private ISupplierProductInfoBiz iSupplierProductInfoBiz  ;
	//产品图片
	private ISupplierImgBiz iSupplierImgBiz;
	// 供应商产品信息 实例
	private SupplierProductInfo supplierProductInfo ;

	
	private SupplierImg supplierImg;
	
	/**
	 * 查看供应商产品信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierProductInfo_base() throws BaseException {
		String supplierId=this.getRequest().getParameter("supplierId");
		this.getRequest().setAttribute("supplierId", supplierId);		
		return "view2";
	}
	public String viewToSupplierProductInfo_base() throws BaseException {
			
		try{
			supplierProductInfo=new SupplierProductInfo();
			String supplierId=this.getRequest().getParameter("supplierId");
			supplierProductInfo.setSupplierId(Long.parseLong(supplierId));
			
			String productName=this.getRequest().getParameter("productName");
			supplierProductInfo.setProductName(productName);
			String productKind=this.getRequest().getParameter("productKind");
			supplierProductInfo.setProductKind(productKind);
			
			this.setListValue( this.iSupplierProductInfoBiz.getSupplierProductInfoList( this.getRollPageDataTables(), supplierProductInfo ) ) ;
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商产品信息列表错误！", e);
			throw new BaseException("查看供应商产品信息列表错误！", e);
		}
		return null ;
	}
	
	/**
	 * 查看供应商产品明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierProductInfoDetail_base() throws BaseException {
		
		try{
			supplierProductInfo=this.iSupplierProductInfoBiz.getSupplierProductInfo( supplierProductInfo.getSpiId() );
			supplierImg=new SupplierImg();
			supplierImg.setSpiId(supplierProductInfo.getSpiId());
			List listImg=this.iSupplierImgBiz.getSupplierImgList(supplierImg);
			this.getRequest().setAttribute("listImg", listImg);
		} catch (Exception e) {
			log("查看供应商产品明细信息错误！", e);
			throw new BaseException("查看供应商产品明细信息错误！", e);
		}
		return "detail2";
		
	}

	public ISupplierProductInfoBiz getiSupplierProductInfoBiz() {
		return iSupplierProductInfoBiz;
	}

	public void setiSupplierProductInfoBiz(
			ISupplierProductInfoBiz iSupplierProductInfoBiz) {
		this.iSupplierProductInfoBiz = iSupplierProductInfoBiz;
	}

	public SupplierProductInfo getSupplierProductInfo() {
		return supplierProductInfo;
	}

	public void setSupplierProductInfo(SupplierProductInfo supplierProductInfo) {
		this.supplierProductInfo = supplierProductInfo;
	}
	public ISupplierImgBiz getiSupplierImgBiz() {
		return iSupplierImgBiz;
	}
	public void setiSupplierImgBiz(ISupplierImgBiz iSupplierImgBiz) {
		this.iSupplierImgBiz = iSupplierImgBiz;
	}
	
}
