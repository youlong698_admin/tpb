package com.ced.sip.supplier.action.supplier;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierCertificateChangeBiz;
import com.ced.sip.supplier.biz.ISupplierCertificateInfoBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierCertificateChange;
import com.ced.sip.supplier.entity.SupplierCertificateInfo;
import com.ced.sip.supplier.entity.SupplierInfo;

public class SupplierCertificatesAction extends BaseAction {
	// 供应商资质信息 服务类
	private ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz  ;
	// 供应商资质信息变更 服务类
	private ISupplierCertificateChangeBiz iSupplierCertificateChangeBiz   ;
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz  ;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// 供应商资质信息 实例
	private SupplierCertificateInfo supplierCertificateInfo ;
	// 供应商资质信息变更 实例
	private SupplierCertificateChange supplierCertificateChange ;
	// 供应商基本信息 实例
	private SupplierInfo supplierInfo ;
	// 临时参数
	private String tempSupCertInfo;
	
	
	/**
	 * 查看供应商资质信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierCertificates() throws BaseException {
		String initPage="view";
		try{
			String from=this.getRequest().getParameter("from");
			String type=this.getRequest().getParameter("type");
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			
			SupplierInfo supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
			
			if(StringUtil.isBlank(supplierInfo.getRegistrationType())) supplierInfo.setRegistrationType("0");
			
			//查找供应商应上传的资质
			List list=this.iSupplierCertificateInfoBiz.getQualityCategoryList(supplierId,supplierInfo.getRegistrationType());
			this.getRequest().setAttribute("list", list);
			
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
			boolean falg=false;
			if(StringUtil.isBlank(type)){
				 if(!TableStatus.COMMON_0.equals(supplierInfo.getIsRegister())||TableStatus.COMMON_2.equals(supplierInfo.getIsAudit())) falg=true;
			}else{
				falg=true;
			}
			this.getRequest().setAttribute("falg", falg);
			this.getRequest().setAttribute("type", type);
			this.getRequest().setAttribute("isAudit", supplierInfo.getIsAudit());
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		}catch (Exception e) {
			log.error("供应商资质信息列表页面错误！", e);
			throw new BaseException("供应商资质信息列表页面错误！", e);
		}
		return initPage ;
	}
	/**
	 * 查看供应商资质信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewToSupplierCertificates() throws BaseException {
			
		try{
			supplierCertificateInfo=new SupplierCertificateInfo();
			
			supplierCertificateInfo.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
			
			String certificateName=this.getRequest().getParameter("certificateName");
			supplierCertificateInfo.setCertificateName(certificateName);
			String timeBegain=this.getRequest().getParameter("timeBegain");
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
			  
			if(StringUtil.isNotBlank(timeBegain))
			{
				supplierCertificateInfo.setTimeBegain(sdf.parse(timeBegain));
			}
			List<SupplierCertificateInfo> list=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfoList( this.getRollPageDataTables(), supplierCertificateInfo );
			for(SupplierCertificateInfo supplierCertificateInfo:list){
				supplierCertificateInfo.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( supplierCertificateInfo.getSciId(), AttachmentStatus.ATTACHMENT_CODE_602 ) ) , "0", this.getRequest() ) ) ;
			}
			this.setListValue(list) ;
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商资质信息列表错误！", e);
			throw new BaseException("查看供应商资质信息列表错误！", e);
		}
		return null ;
	}
	/**
	 * 保存供应商资质信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveSupplierCertificatesInit() throws BaseException {
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			SupplierInfo supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
			//查找供应商应上传的资质
			List list=this.iSupplierCertificateInfoBiz.getQualityCategoryList(supplierId,supplierInfo.getRegistrationType());
			this.getRequest().setAttribute("list", list);
		
		} catch (Exception e) {
			log("保存供应商资质信息初始化错误！", e);
			throw new BaseException("保存供应商资质信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存供应商资质信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveSupplierCertificates() throws BaseException {
		
		try{
			if(supplierCertificateInfo!=null){
				supplierCertificateInfo.setWriter(UserRightInfoUtil.getSupplierLoginName(this.getRequest()));
				supplierCertificateInfo.setWriteDate(DateUtil.getCurrentDateTime());
				supplierCertificateInfo.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
				supplierCertificateInfo.setSupplierName(UserRightInfoUtil.getSupplierName(this.getRequest()));
				
				this.iSupplierCertificateInfoBiz.saveSupplierCertificateInfo( supplierCertificateInfo );
				//保存附件
				supplierCertificateInfo.setAttIdData(supplierCertificateInfo.getAttIdData());
				supplierCertificateInfo.setUuIdData(supplierCertificateInfo.getUuIdData());
				supplierCertificateInfo.setFileNameData(supplierCertificateInfo.getFileNameData());
				supplierCertificateInfo.setFileTypeData(supplierCertificateInfo.getFileTypeData());
				iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(supplierCertificateInfo, supplierCertificateInfo.getSciId(),AttachmentStatus.ATTACHMENT_CODE_602,UserRightInfoUtil.getSupplierLoginName(this.getRequest())));

				this.getRequest().setAttribute("message", "保存成功");
				this.getRequest().setAttribute("operModule", "保存资质信息");
			}
		} catch (Exception e) {
			log("保存供应商资质信息错误！", e);
			throw new BaseException("保存供应商资质信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改供应商资质信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierCertificatesInit() throws BaseException {
		
		try{
			supplierCertificateInfo=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfo( supplierCertificateInfo.getSciId() );
			
			//获取附件
			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( supplierCertificateInfo.getSciId(), AttachmentStatus.ATTACHMENT_CODE_602 ) );
			supplierCertificateInfo.setUuIdData((String)map.get("uuIdData"));
			supplierCertificateInfo.setFileNameData((String)map.get("fileNameData"));
			supplierCertificateInfo.setFileTypeData((String)map.get("fileTypeData"));
			supplierCertificateInfo.setAttIdData((String)map.get("attIdData"));
			
		} catch (Exception e) {
			log("修改供应商资质信息初始化错误！", e);
			throw new BaseException("修改供应商资质信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改供应商资质信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierCertificates() throws BaseException {
		
		try{
			this.iSupplierCertificateInfoBiz.updateSupplierCertificateInfo( supplierCertificateInfo );
			
			//保存附件
			supplierCertificateInfo.setAttIdData(supplierCertificateInfo.getAttIdData());
			supplierCertificateInfo.setUuIdData(supplierCertificateInfo.getUuIdData());
			supplierCertificateInfo.setFileNameData(supplierCertificateInfo.getFileNameData());
			supplierCertificateInfo.setFileTypeData(supplierCertificateInfo.getFileTypeData());
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(supplierCertificateInfo, supplierCertificateInfo.getSciId(),AttachmentStatus.ATTACHMENT_CODE_602,UserRightInfoUtil.getSupplierLoginName(this.getRequest())));
		
			// 删除附件
			iAttachmentBiz.deleteAttachments( parseAttachIds( supplierCertificateInfo.getAttIds() ) ) ;
			
			//supplierCertificateInfo.setUuIdData("["+supplierCertificateInfo.getAttIdData()+"]");
			//supplierCertificateInfo.setUuIdData("["+supplierCertificateInfo.getUuIdData()+"]");
			//supplierCertificateInfo.setFileNameData("["+supplierCertificateInfo.getFileNameData()+"]");
			//supplierCertificateInfo.setFileTypeData("["+supplierCertificateInfo.getFileTypeData()+"]");

			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改资质信息");
		} catch (Exception e) {
			log("修改供应商资质信息错误！", e);
			throw new BaseException("修改供应商资质信息错误！", e);
		}
		return "success";
		
	}
	/**
	 * 变更供应商资质信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierCertificatesInitChange() throws BaseException {
		
		try{
			supplierCertificateInfo=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfo( supplierCertificateInfo.getSciId() );
			
			//获取附件
			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( supplierCertificateInfo.getSciId(), AttachmentStatus.ATTACHMENT_CODE_602 ) );
			supplierCertificateInfo.setUuIdData((String)map.get("uuIdData"));
			supplierCertificateInfo.setFileNameData((String)map.get("fileNameData"));
			supplierCertificateInfo.setFileTypeData((String)map.get("fileTypeData"));
			supplierCertificateInfo.setAttIdData((String)map.get("attIdData"));
			
		} catch (Exception e) {
			log("变更供应商资质信息初始化错误！", e);
			throw new BaseException("变更供应商资质信息初始化错误！", e);
		}
		return "modifyInitChange";
		
	}

	/**
	 * 修改供应商资质信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierCertificatesChange() throws BaseException {
		
		try{
			supplierCertificateChange.setIsChangeEffect(1);
			supplierCertificateChange.setWriter(UserRightInfoUtil.getSupplierLoginName(this.getRequest()));
			supplierCertificateChange.setWriteDate(DateUtil.getCurrentDateTime());
			this.iSupplierCertificateChangeBiz.saveSupplierCertificateChange(supplierCertificateChange);
			
			//保存附件
			supplierCertificateChange.setAttIdData(supplierCertificateChange.getAttIdData());
			supplierCertificateChange.setUuIdData(supplierCertificateChange.getUuIdData());
			supplierCertificateChange.setFileNameData(supplierCertificateChange.getFileNameData());
			supplierCertificateChange.setFileTypeData(supplierCertificateChange.getFileTypeData());
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(supplierCertificateChange, supplierCertificateChange.getScicId(),AttachmentStatus.ATTACHMENT_CODE_6021,UserRightInfoUtil.getSupplierLoginName(this.getRequest())));
		
			
            supplierCertificateInfo=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfo( supplierCertificateChange.getSciId() );
			
			//获取附件
			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( supplierCertificateInfo.getSciId(), AttachmentStatus.ATTACHMENT_CODE_602 ) );
			supplierCertificateInfo.setUuIdData((String)map.get("uuIdData"));
			supplierCertificateInfo.setFileNameData((String)map.get("fileNameData"));
			supplierCertificateInfo.setFileTypeData((String)map.get("fileTypeData"));
			supplierCertificateInfo.setAttIdData((String)map.get("attIdData"));

			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierCertificateChange.getSupplierId());
			supplierInfo.setIsChange("1");
			this.iSupplierInfoBiz.updateSupplierInfo(supplierInfo);
			
			this.getRequest().setAttribute("message", "变更成功");
			this.getRequest().setAttribute("operModule", "变更资质信息");
		} catch (Exception e) {
			log("变更供应商资质信息错误！", e);
			throw new BaseException("变更供应商资质信息错误！", e);
		}
		return "modifyInitChange";
		
	}
	/**
	 * 删除供应商资质信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteSupplierCertificates() throws BaseException {
		try{
			if(tempSupCertInfo!=null){
				String[] sciIdArr = tempSupCertInfo.split(",");
				for(int i=0;i<sciIdArr.length;i++){
					if(sciIdArr[i]!=null){
						supplierCertificateInfo = this.iSupplierCertificateInfoBiz.getSupplierCertificateInfo(Long.parseLong(sciIdArr[i]));
						if(supplierCertificateInfo!=null){
							supplierCertificateInfo.setIsUsable("1");
							this.iSupplierCertificateInfoBiz.updateSupplierCertificateInfo(supplierCertificateInfo);
						}
					}
				}
				PrintWriter out = this.getResponse().getWriter();
				String message="删除成功";
				out.println(message);
			}

			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除资质信息");
		} catch (Exception e) {
			log("删除供应商资质信息错误！", e);
			throw new BaseException("删除供应商资质信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看供应商资质明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierCertificatesDetail() throws BaseException {
		
		try{
			supplierCertificateInfo=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfo( supplierCertificateInfo.getSciId() );
			//获取附件，参数“0”没有删除功能，参数“null”存在删除
			supplierCertificateInfo.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( supplierCertificateInfo.getSciId(), AttachmentStatus.ATTACHMENT_CODE_602 ) ) , "0", this.getRequest() ) ) ;
		} catch (Exception e) {
			log("查看供应商资质明细信息错误！", e);
			throw new BaseException("查看供应商资质明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 查看供应商资质信息(供应商变更审核)
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierCertificatesChange_base() throws BaseException {
		
		String supplierId=this.getRequest().getParameter("supplierId");
		this.getRequest().setAttribute("supplierId", supplierId);
		return "viewChange";
	}
	/**
	 * 查询供应商资质信息列表(供应商变更审核)
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierCertificatesChange_base() throws BaseException {
			
		try{
			supplierCertificateChange=new SupplierCertificateChange();
			
			String supplierId=this.getRequest().getParameter("supplierId");
			supplierCertificateChange.setSupplierId(Long.parseLong(supplierId));
			
			String certificateName=this.getRequest().getParameter("certificateName");
			supplierCertificateChange.setCertificateName(certificateName);
			String timeBegain=this.getRequest().getParameter("timeBegain");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
			  
			if(StringUtil.isNotBlank(timeBegain))
			{
				supplierCertificateChange.setTimeBegain(sdf.parse(timeBegain));
			}
			this.setListValue( this.iSupplierCertificateChangeBiz.getSupplierCertificateChangeList(this.getRollPageDataTables(), supplierCertificateChange ) ) ;
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商资质信息列表(供应商变更审核)错误！", e);
			throw new BaseException("查看供应商资质信息列表(供应商变更审核)错误！", e);
		}
		return null ;
	}
	/**
	 * 查看供应商资质明细信息(供应商变更审核)
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierCertificatesDetailChange_base() throws BaseException {
		
		try{
			supplierCertificateChange=this.iSupplierCertificateChangeBiz.getSupplierCertificateChange(supplierCertificateChange.getScicId());
			supplierCertificateChange.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( supplierCertificateChange.getScicId(), AttachmentStatus.ATTACHMENT_CODE_6021 ) ) , "0", this.getRequest() ) ) ;
		} catch (Exception e) {
			log("查看供应商资质明细信息错误！", e);
			throw new BaseException("查看供应商资质明细信息错误！", e);
		}
		return "detailChange";
		
	}

	public ISupplierCertificateInfoBiz getiSupplierCertificateInfoBiz() {
		return iSupplierCertificateInfoBiz;
	}

	public void setiSupplierCertificateInfoBiz(
			ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz) {
		this.iSupplierCertificateInfoBiz = iSupplierCertificateInfoBiz;
	}

	public SupplierCertificateInfo getSupplierCertificateInfo() {
		return supplierCertificateInfo;
	}

	public void setSupplierCertificateInfo(
			SupplierCertificateInfo supplierCertificateInfo) {
		this.supplierCertificateInfo = supplierCertificateInfo;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public String getTempSupCertInfo() {
		return tempSupCertInfo;
	}
	public void setTempSupCertInfo(String tempSupCertInfo) {
		this.tempSupCertInfo = tempSupCertInfo;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public ISupplierCertificateChangeBiz getiSupplierCertificateChangeBiz() {
		return iSupplierCertificateChangeBiz;
	}
	public void setiSupplierCertificateChangeBiz(
			ISupplierCertificateChangeBiz iSupplierCertificateChangeBiz) {
		this.iSupplierCertificateChangeBiz = iSupplierCertificateChangeBiz;
	}
	public SupplierCertificateChange getSupplierCertificateChange() {
		return supplierCertificateChange;
	}
	public void setSupplierCertificateChange(
			SupplierCertificateChange supplierCertificateChange) {
		this.supplierCertificateChange = supplierCertificateChange;
	}
	
}
