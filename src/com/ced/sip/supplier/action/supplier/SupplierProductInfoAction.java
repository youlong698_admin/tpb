package com.ced.sip.supplier.action.supplier;

import java.io.PrintWriter;
import java.util.List;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierImgBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.biz.ISupplierProductInfoBiz;
import com.ced.sip.supplier.entity.SupplierImg;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductGroup;
import com.ced.sip.supplier.entity.SupplierProductInfo;
import com.ced.sip.system.entity.Dictionary;

public class SupplierProductInfoAction extends BaseAction {
	// 供应商产品信息 服务类
	private ISupplierProductInfoBiz iSupplierProductInfoBiz  ;
	//产品图片
	private ISupplierImgBiz iSupplierImgBiz;
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz;  
	// 供应商产品信息 实例
	private SupplierProductInfo supplierProductInfo ;
	//临时参数
	private String tempSupProInfo;
	// 供应商基本信息 实例
	private SupplierInfo supplierInfo;
	
	private SupplierImg supplierImg;
	
	private String imgData;
	
	
	/**
	 * 查看供应商产品信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierProductInfo() throws BaseException {
		String initPage="view";
		try{
			String from=this.getRequest().getParameter("from");
			String type=this.getRequest().getParameter("type");
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());			
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
			boolean falg=false;
			if(StringUtil.isBlank(type)){
				 if(!TableStatus.COMMON_0.equals(supplierInfo.getIsRegister())||TableStatus.COMMON_2.equals(supplierInfo.getIsAudit())) falg=true;
			}else{
				falg=true;
			}
			this.getRequest().setAttribute("falg", falg);
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		}catch (Exception e) {
			log.error("供应商产品信息列表页面错误！", e);
			throw new BaseException("供应商产品信息列表页面错误！", e);
		}
		return initPage ;
	}
	/**
	 * 查询供应商产品信息数据
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierProductInfo() throws BaseException {
			
		try{
			supplierProductInfo=new SupplierProductInfo();
			
			supplierProductInfo.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
			
			String productName=this.getRequest().getParameter("productName");
			supplierProductInfo.setProductName(productName);
			String productKind=this.getRequest().getParameter("productKind");
			supplierProductInfo.setProductKind(productKind);
			this.setListValue( this.iSupplierProductInfoBiz.getSupplierProductInfoList( this.getRollPageDataTables(), supplierProductInfo ) ) ;
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商产品信息列表错误！", e);
			throw new BaseException("查看供应商产品信息列表错误！", e);
		}
		return null ;
	}
	/**
	 * 触屏版查询供应商产品信息数据
	 * @return
	 * @throws BaseException
	 */
	public String viewToSupplierProductInfoMobile() throws BaseException {
			
		try{
			supplierProductInfo=new SupplierProductInfo();
			
			supplierProductInfo.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
			
			String productName=this.getRequest().getParameter("productName");
			supplierProductInfo.setProductName(productName);
			String productKind=this.getRequest().getParameter("productKind");
			supplierProductInfo.setProductKind(productKind);
			List<SupplierProductInfo> list=this.iSupplierProductInfoBiz.getSupplierProductInfoList( this.getRollPageDataTables(), supplierProductInfo );
			
			this.setListValue(list);			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看供应商产品信息列表错误！", e);
			throw new BaseException("查看供应商产品信息列表错误！", e);
		}
		return null ;
	}
	
	/**
	 * 保存供应商产品信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveSupplierProductInfoInit() throws BaseException {
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());	
			SupplierProductGroup supplierProductGroup=new SupplierProductGroup();
			supplierProductGroup.setSupplierId(supplierId);
			List<SupplierProductGroup> supplierProductGroupList=this.iSupplierInfoBiz.getSupplierProductGroupList(supplierProductGroup);
			this.getRequest().setAttribute("supplierProductGroupList", supplierProductGroupList);
			
			List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1711);
			 this.getRequest().setAttribute("dictionaryList", dictionaryList);
		} catch (Exception e) {
			log("保存供应商产品信息初始化错误！", e);
			throw new BaseException("保存供应商产品信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存供应商产品信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveSupplierProductInfo() throws BaseException {
		
		try{
			if(supplierProductInfo!=null){
				supplierProductInfo.setWriter(UserRightInfoUtil.getSupplierLoginName(this.getRequest()));
				supplierProductInfo.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
				supplierProductInfo.setSupplierName(UserRightInfoUtil.getSupplierName(this.getRequest()));
				supplierProductInfo.setWriteDate(DateUtil.getCurrentDateTime());
				this.iSupplierProductInfoBiz.saveSupplierProductInfo( supplierProductInfo );
				
				String img="upload/SupplierProductInfo/images/no.jpg";
				if(StringUtil.isNotBlank(imgData)){
					String[] imgUrl=imgData.split(",");
					for(int i=0; i<imgUrl.length; i++ ){	
						supplierImg=new SupplierImg();
						supplierImg.setImg(imgUrl[i]);
						supplierImg.setSpiId(supplierProductInfo.getSpiId());
						this.iSupplierImgBiz.saveSupplierImg(supplierImg);
						img=imgUrl[i];
					}
				}
				supplierProductInfo.setImg(img);
				iSupplierProductInfoBiz.updateSupplierProductInfo(supplierProductInfo);
			}

			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "新增产品信息");
		} catch (Exception e) {
			log("保存供应商产品信息错误！", e);
			throw new BaseException("保存供应商产品信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改供应商产品信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierProductInfoInit() throws BaseException {
		
		try{
			supplierProductInfo=this.iSupplierProductInfoBiz.getSupplierProductInfo( supplierProductInfo.getSpiId() );
			String imgDataTmp="";
			supplierImg=new SupplierImg();
			supplierImg.setSpiId(supplierProductInfo.getSpiId());
			List list=this.iSupplierImgBiz.getSupplierImgList(supplierImg);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					supplierImg=(SupplierImg)list.get(i);
					if(i==(list.size()-1)){
						imgDataTmp+="\""+supplierImg.getImg()+"\"";
					}else{
						imgDataTmp+="\""+supplierImg.getImg()+"\",";
					}
				}
			}
			imgData="["+imgDataTmp+"]";
			
			List<Dictionary> dictionaryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1711);
			 this.getRequest().setAttribute("dictionaryList", dictionaryList);
		} catch (Exception e) {
			log("修改供应商产品信息初始化错误！", e);
			throw new BaseException("修改供应商产品信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改供应商产品信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierProductInfo() throws BaseException {
		
		try{
			if(supplierProductInfo!=null){
				supplierProductInfo.setModifyPerson(UserRightInfoUtil.getSupplierLoginName(this.getRequest()));
				supplierProductInfo.setModifyDate(DateUtil.getCurrentDateTime());
				this.iSupplierProductInfoBiz.updateSupplierProductInfo( supplierProductInfo );
			}
			String img="upload/SupplierProductInfo/images/no.jpg";
			supplierImg=new SupplierImg();
			supplierImg.setSpiId(supplierProductInfo.getSpiId());
			List<SupplierImg> listImg=this.iSupplierImgBiz.getSupplierImgList(supplierImg);
			for(SupplierImg supplierImg:listImg){
				this.iSupplierImgBiz.deleteSupplierImg(supplierImg);
			}
			if(StringUtil.isNotBlank(imgData)){
				String[] imgUrl=imgData.split(",");
				for(int i=0; i<imgUrl.length; i++ ){	
					supplierImg=new SupplierImg();
					supplierImg.setImg(imgUrl[i]);
					supplierImg.setSpiId(supplierProductInfo.getSpiId());
					this.iSupplierImgBiz.saveSupplierImg(supplierImg);
					img=imgUrl[i];
				}
			}
			supplierProductInfo.setImg(img);
			iSupplierProductInfoBiz.updateSupplierProductInfo(supplierProductInfo);
			
			imgData="[]";
			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改产品信息");
		} catch (Exception e) {
			log("修改供应商产品信息错误！", e);
			throw new BaseException("修改供应商产品信息错误！", e);
		}
		return "success";
		
	}
	
	/**
	 * 删除供应商产品信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteSupplierProductInfo() throws BaseException {
		try{
			PrintWriter out = this.getResponse().getWriter();
			if(tempSupProInfo!=null){
				String[] spiIdArr = tempSupProInfo.split(",");
				for(int i=0;i<spiIdArr.length;i++){
					if(spiIdArr[i]!=null){
						supplierProductInfo = this.iSupplierProductInfoBiz.getSupplierProductInfo(Long.parseLong(spiIdArr[i]));
						if(supplierProductInfo!=null){
							supplierProductInfo.setIsUsable("1");
							this.iSupplierProductInfoBiz.updateSupplierProductInfo(supplierProductInfo);
						}
						supplierImg=new SupplierImg();
						supplierImg.setSpiId(supplierProductInfo.getSpiId());
						List<SupplierImg> listImg=this.iSupplierImgBiz.getSupplierImgList(supplierImg);
						for(SupplierImg supplierImg:listImg){
							this.iSupplierImgBiz.deleteSupplierImg(supplierImg);
						}
					}
				}
				
				
				String message="删除成功";
				out.println(message);

			}
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除产品信息");
		} catch (Exception e) {
			log("删除供应商产品信息错误！", e);
			throw new BaseException("删除供应商产品信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看供应商产品明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierProductInfoDetail() throws BaseException {
		String initPage="detail";
		try{
			String from=this.getRequest().getParameter("from");
			supplierProductInfo=this.iSupplierProductInfoBiz.getSupplierProductInfo( supplierProductInfo.getSpiId() );
			supplierImg=new SupplierImg();
			supplierImg.setSpiId(supplierProductInfo.getSpiId());
			List listImg=this.iSupplierImgBiz.getSupplierImgList(supplierImg);
			this.getRequest().setAttribute("listImg", listImg);
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("查看供应商产品明细信息错误！", e);
			throw new BaseException("查看供应商产品明细信息错误！", e);
		}
		return initPage;
		
	}
	

	public ISupplierProductInfoBiz getiSupplierProductInfoBiz() {
		return iSupplierProductInfoBiz;
	}

	public void setiSupplierProductInfoBiz(
			ISupplierProductInfoBiz iSupplierProductInfoBiz) {
		this.iSupplierProductInfoBiz = iSupplierProductInfoBiz;
	}

	public SupplierProductInfo getSupplierProductInfo() {
		return supplierProductInfo;
	}

	public void setSupplierProductInfo(SupplierProductInfo supplierProductInfo) {
		this.supplierProductInfo = supplierProductInfo;
	}
	public String getTempSupProInfo() {
		return tempSupProInfo;
	}
	public void setTempSupProInfo(String tempSupProInfo) {
		this.tempSupProInfo = tempSupProInfo;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public ISupplierImgBiz getiSupplierImgBiz() {
		return iSupplierImgBiz;
	}
	public void setiSupplierImgBiz(ISupplierImgBiz iSupplierImgBiz) {
		this.iSupplierImgBiz = iSupplierImgBiz;
	}
	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}
	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}
	public String getImgData() {
		return imgData;
	}
	public void setImgData(String imgData) {
		this.imgData = imgData;
	}
	
}
