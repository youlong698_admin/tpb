package com.ced.sip.supplier.action.supplier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.GlobalSettingBase;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierCertificateInfoBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.biz.ISupplierInfoChangeBiz;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.entity.MaterialKind;
import com.ced.sip.system.entity.QualityCategory;
import com.ced.sip.supplier.entity.SupplierCertificateInfo;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierInfoChange;
import com.ced.sip.supplier.entity.SupplierProductGroup;

public class SupplierBaseInfoAction extends BaseAction {
    //上传文件存放路径   
    private final static String UPLOADDIR = "upload/supplier/logo/";
	// 供应商资质信息 服务类
	private ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz  ;
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz  ;
	// 供应商基本信息 变更服务类
	private ISupplierInfoChangeBiz iSupplierInfoChangeBiz  ;
	// 供应商基本信息 实例
	private SupplierInfo supplierInfo ;
	//主营产品类别
	private MaterialKind materialKind;
	// 供应商产品类别 实例
	private SupplierProductGroup supplierProductGroup;
	// 供应商基本信息 变更实例
	private SupplierInfoChange supplierInfoChange ;
	//主营产品类别
	private IMaterialBiz iMaterialBiz;
	
	private SupplierCertificateInfo supplierCertificateInfo;
	//供应商性质Map
	private Map supplierTypeMap;
	//供应商状态
	private Map supplierStatusMap;
	//供应商级别
	private Map supplierLevelMap;
	//注册代码证类型
	private Map registrationTypeMap;
		

	private File file;
	
	private String fileContentType;   
	
	private String fileFileName; 
	/**
	 * 查看供应商基本信息tab
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierSelectInfo() throws BaseException {
		try{
		} catch (Exception e) {
			log("查看供应商基本信息tab初始化错误！", e);
			throw new BaseException("查看供应商基本信息tab初始化错误！", e);
		}
		return "select_index" ;
	}
	
	/**
	 * 修改供应商基本信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierBaseInfoInit() throws BaseException {
		
		try{
			
			supplierTypeMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1710);
			supplierStatusMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713);
			registrationTypeMap=BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1714);
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierInfo.getSupplierId());
			if(supplierInfo.getProdCodes()!=null){
				String[] proKindIds = supplierInfo.getProdCodes().split(",");
				supplierInfo.setProdKindNames(this.convertProKindIdToProKindName(proKindIds));
			}	
			Map industryOwnedMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1707);
			Map managementModelMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1708);
			this.getRequest().setAttribute("industryOwnedMap", industryOwnedMap);
			this.getRequest().setAttribute("managementModelMap", managementModelMap);
			this.getRequest().setAttribute("supplierStatusMap",supplierStatusMap);
			this.getRequest().setAttribute("supplierTypeMap",supplierTypeMap );
		} catch (Exception e) {
			log("修改供应商基本信息初始化错误！", e);
			throw new BaseException("修改供应商基本信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改供应商基本信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierBaseInfo() throws BaseException {
		
		try{
			 String logo="",fileType="";  
	            if(file!=null){
					InputStream in = new FileInputStream(file);   
		            String dir = GlobalSettingBase.getFilePath()+UPLOADDIR; 
		            
		            int index = fileFileName.lastIndexOf(".");
		    		if (index > -1) {
		    			fileType =fileFileName.substring(index);
		    		}
		    		fileType=fileType.toLowerCase().substring(1);
					String cjrq = new SimpleDateFormat("yyyyMMddHHmmss")
							.format(new Date());
					String fjTpName = cjrq + "." + fileType;// 文件的名字
		            File uploadFile = new File(dir, fjTpName);   
		            logo=UPLOADDIR+fjTpName;
		            OutputStream out = new FileOutputStream(uploadFile);   
		            byte[] buffer = new byte[1024 * 1024];   
		            int length;   
		            while ((length = in.read(buffer)) > 0) {   
		                out.write(buffer, 0, length);   
		            }   
		            in.close();   
		            out.close();   
		            supplierInfo.setLogo(logo);
	            }
			this.iSupplierInfoBiz.updateSupplierInfo( supplierInfo );
			if(StringUtil.isNotBlank(supplierInfo.getProdCodes())){
				String[] prodCodes = supplierInfo.getProdCodes().split(",");
				supplierProductGroup=new SupplierProductGroup();
				supplierProductGroup.setSupplierId(supplierInfo.getSupplierId());
				//删除供应商对应的产品类别信息
				if(StringUtil.isNotBlank(supplierProductGroup)){
					this.setListValue(this.iSupplierInfoBiz.getSupplierProductGroupList(supplierProductGroup));
					if(this.getListValue()!=null&&this.getListValue().size()>0){
						for(int i=0;i<this.getListValue().size();i++){
							SupplierProductGroup supProductGroup = (SupplierProductGroup) this.getListValue().get(i);
							this.iSupplierInfoBiz.deleteSupplierProductGroup(supProductGroup);
						}
					}
				}
				//保存供应商对应的产品类别信息
				for(int i=1;i<prodCodes.length-1;i++){
					if(prodCodes[i]!=null){
						SupplierProductGroup supplierProductGroup = new SupplierProductGroup();
						supplierProductGroup.setSupplierId(supplierInfo.getSupplierId());
						materialKind = this.iMaterialBiz.getMaterialKind(Long.parseLong(prodCodes[i]));
						supplierProductGroup.setPdId(materialKind.getMkId());
						supplierProductGroup.setPdCode(materialKind.getMkCode());
						supplierProductGroup.setPdName(materialKind.getMkName());
						this.iSupplierInfoBiz.saveSupplierProductGroup(supplierProductGroup);
					}
				}
			}
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改供应商基本信息");
		} catch (Exception e) {
			log("修改供应商基本信息错误！", e);
			throw new BaseException("修改供应商基本信息错误！", e);
		}
		return updateSupplierBaseInfoInit();
		
	}
	/**
	 * 变更供应商基本信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierBaseInfoInitChange() throws BaseException {
		
		try{
			
			supplierTypeMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1710);
			supplierStatusMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713);
			registrationTypeMap=BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1714);
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierInfo.getSupplierId());
			if(supplierInfo.getProdCodes()!=null){
				String[] proKindIds = supplierInfo.getProdCodes().split(",");
				supplierInfo.setProdKindNames(this.convertProKindIdToProKindName(proKindIds));
			}
			supplierInfoChange=this.iSupplierInfoChangeBiz.getSupplierInfoChangeBySupplierId(supplierInfo.getSupplierId());	
			Map industryOwnedMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1707);
			Map managementModelMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1708);
			this.getRequest().setAttribute("industryOwnedMap", industryOwnedMap);
			this.getRequest().setAttribute("managementModelMap", managementModelMap);
			this.getRequest().setAttribute("supplierStatusMap",supplierStatusMap);
			this.getRequest().setAttribute("supplierTypeMap",supplierTypeMap );
		} catch (Exception e) {
			log("变更供应商基本信息初始化错误！", e);
			throw new BaseException("变更供应商基本信息初始化错误！", e);
		}
		return "modifyInitChange";
		
	}

	
	/**
	 * 变更供应商基本信息提交
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierBaseInfoChange() throws BaseException {
		
		try{
			String logo="",fileType="";  
            if(file!=null){
				InputStream in = new FileInputStream(file);   
	            String dir =GlobalSettingBase.getFilePath()+UPLOADDIR; 
	            
	            int index = fileFileName.lastIndexOf(".");
	    		if (index > -1) {
	    			fileType =fileFileName.substring(index);
	    		}
	    		fileType=fileType.toLowerCase().substring(1);
				String cjrq = new SimpleDateFormat("yyyyMMddHHmmss")
						.format(new Date());
				String fjTpName = cjrq + "." + fileType;// 文件的名字
	            File uploadFile = new File(dir, fjTpName);   
	            logo=UPLOADDIR+fjTpName;
	            OutputStream out = new FileOutputStream(uploadFile);   
	            byte[] buffer = new byte[1024 * 1024];   
	            int length;   
	            while ((length = in.read(buffer)) > 0) {   
	                out.write(buffer, 0, length);   
	            }   
	            in.close();   
	            out.close();   
	            supplierInfo.setLogo(logo);
            }
			SupplierInfoChange supplierInfoChange_temp=new SupplierInfoChange();
			supplierInfoChange_temp.setSupplierId(supplierInfo.getSupplierId());
			supplierInfoChange_temp.setIsChangeEffect(1);
			int diff=0;
			if(!supplierInfoChange.getSupplierName().equals(supplierInfo.getSupplierName()))
			{
				diff=1;
				supplierInfoChange_temp.setSupplierName(supplierInfoChange.getSupplierName());
			}
			if(!supplierInfoChange.getSupplierNameSimple().equals(supplierInfo.getSupplierNameSimple()))
			{
				diff=1;
				supplierInfoChange_temp.setSupplierNameSimple(supplierInfoChange.getSupplierNameSimple());
			}
			if(!supplierInfoChange.getSupplierAddress().equals(supplierInfo.getSupplierAddress()))
			{
				diff=1;
				supplierInfoChange_temp.setSupplierAddress(supplierInfoChange.getSupplierAddress());
			}
			if(!supplierInfoChange.getSupplierPhone().equals(supplierInfo.getSupplierPhone()))
			{
				diff=1;
				supplierInfoChange_temp.setSupplierPhone(supplierInfoChange.getSupplierPhone());
			}
			if(!supplierInfoChange.getSupplierFax().equals(supplierInfo.getSupplierFax()))
			{
				diff=1;
				supplierInfoChange_temp.setLegalPerson(supplierInfoChange.getLegalPerson());
			}
			if(!supplierInfoChange.getRegisterFunds().equals(supplierInfo.getRegisterFunds()))
			{
				diff=1;
				supplierInfoChange_temp.setRegisterFunds(supplierInfoChange.getRegisterFunds());
			}
			if(!StringUtil.convertNullToBlank(supplierInfoChange.getCreateDate()).equals(StringUtil.convertNullToBlank(supplierInfo.getCreateDate())))
			{
				diff=1;
				supplierInfoChange_temp.setCreateDate(supplierInfoChange.getCreateDate());
			}
			if(diff==1) {
				supplierInfo.setIsChange("1");
				this.iSupplierInfoChangeBiz.saveSupplierInfoChange(supplierInfoChange_temp);
			}
			
			this.iSupplierInfoBiz.updateSupplierInfo( supplierInfo );
			if(StringUtil.isNotBlank(supplierInfo.getProdCodes())){
				String[] prodCodes = supplierInfo.getProdCodes().split(",");
				supplierProductGroup=new SupplierProductGroup();
				supplierProductGroup.setSupplierId(supplierInfo.getSupplierId());
				//删除供应商对应的产品类别信息
				if(StringUtil.isNotBlank(supplierProductGroup)){
					this.setListValue(this.iSupplierInfoBiz.getSupplierProductGroupList(supplierProductGroup));
					if(this.getListValue()!=null&&this.getListValue().size()>0){
						for(int i=0;i<this.getListValue().size();i++){
							SupplierProductGroup supProductGroup = (SupplierProductGroup) this.getListValue().get(i);
							this.iSupplierInfoBiz.deleteSupplierProductGroup(supProductGroup);
						}
					}
				}
				//保存供应商对应的产品类别信息
				for(int i=1;i<prodCodes.length-1;i++){
					if(prodCodes[i]!=null){
						SupplierProductGroup supplierProductGroup = new SupplierProductGroup();
						supplierProductGroup.setSupplierId(supplierInfo.getSupplierId());
						materialKind = this.iMaterialBiz.getMaterialKind(Long.parseLong(prodCodes[i]));
						supplierProductGroup.setPdId(materialKind.getMkId());
						supplierProductGroup.setPdCode(materialKind.getMkCode());
						supplierProductGroup.setPdName(materialKind.getMkName());
						this.iSupplierInfoBiz.saveSupplierProductGroup(supplierProductGroup);
					}
				}
			}
			
			this.getRequest().setAttribute("message", "变更供应商信息成功");
			this.getRequest().setAttribute("operModule", "变更供应商信息");
		} catch (Exception e) {
			log("变更供应商信息错误！", e);
			throw new BaseException("变更供应商信息错误！", e);
		}
		return updateSupplierBaseInfoInitChange();
		
	}
	/**
	 * 查看供应商基本明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierBaseInfoDetail() throws BaseException {
		String page=DETAIL;
		try{
			String from=this.getRequest().getParameter("from");
			String type=this.getRequest().getParameter("type"); //type有值的话是更新
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(UserRightInfoUtil.getSupplierId(this.getRequest()));
			if(StringUtil.isNotBlank(supplierInfo.getProdCodes())){
				String[] proKindIds = supplierInfo.getProdCodes().split(",");
				supplierInfo.setProdKindNames(this.convertProKindIdToProKindName(proKindIds));
			}
			supplierInfo.setSupplierTypeCn(BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getSupplierType(),DictStatus.COMMON_DICT_TYPE_1710));
			supplierInfo.setIndustryOwnedCn(BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getIndustryOwned(), DictStatus.COMMON_DICT_TYPE_1707));
			supplierInfo.setManagementModelCn(BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getManagementModel(), DictStatus.COMMON_DICT_TYPE_1708));
			boolean falg=false;
			if(StringUtil.isBlank(type)){
			    if(!TableStatus.COMMON_0.equals(supplierInfo.getIsRegister())||TableStatus.COMMON_2.equals(supplierInfo.getIsAudit())) falg=true;
			}else{
				falg=true;
			}
			if(StringUtil.isNotBlank(from)) page+="Mobile";
			this.getRequest().setAttribute("falg", falg);
			this.getRequest().setAttribute("type", type);
		} catch (Exception e) {
			log("查看供应商基本明细信息错误！", e);
			throw new BaseException("查看供应商基本明细信息错误！", e);
		}
		return page;
		
	}
	/**
	 * 供应商信息完善 第四步
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierBaseInfoComplete() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			SupplierInfo supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
			if(StringUtil.isBlank(supplierInfo.getRegistrationType())) supplierInfo.setRegistrationType("0");
			//查找供应商应上传的资质
			List<QualityCategory> list=this.iSupplierCertificateInfoBiz.getQualityCategoryList(supplierId,supplierInfo.getRegistrationType());
			List<QualityCategory> list_noting=new ArrayList<QualityCategory>();
			List<SupplierCertificateInfo> list2;
			//查找供应商实际上传的资质
			for(QualityCategory qualityCategory:list){
				 supplierCertificateInfo=new SupplierCertificateInfo();
				 supplierCertificateInfo.setSupplierId(supplierId);
				 supplierCertificateInfo.setQcId(qualityCategory.getQcId());
				 list2=this.iSupplierCertificateInfoBiz.getSupplierCertificateInfoList(supplierCertificateInfo);
				 if(list2.size()==0) list_noting.add(qualityCategory);
			}
			boolean falg=false;
			if(list_noting.size()==0) {
				falg=true;
				supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
				this.getRequest().setAttribute("isRegister", supplierInfo.getIsRegister());
				this.getRequest().setAttribute("isAudit", supplierInfo.getIsAudit());
				}
			this.getRequest().setAttribute("list_noting", list_noting);
			this.getRequest().setAttribute("falg", falg);
			this.getRequest().setAttribute("supplierId", supplierId);
			
		} catch (Exception e) {
			log("供应商信息完善 第四步信息错误！", e);
			throw new BaseException("供应商信息完善 第四步信息错误！", e);
		}
		return "supplierBaseInfoComplete";
		
	}
	/**
	 * 供应商信息完善 提交审核
	 * @return
	 * @throws BaseException 
	 */
	public String updateSupplierBaseInfoAudit() throws BaseException {
		
		try{
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierInfo.getSupplierId());
			supplierInfo.setIsRegister(TableStatus.COMMON_1); //注册成功
			supplierInfo.setIsAudit(TableStatus.COMMON_1);  //提交审核
			iSupplierInfoBiz.updateSupplierInfo(supplierInfo);
			
			this.getRequest().setAttribute("isRegister", supplierInfo.getIsRegister());
			this.getRequest().setAttribute("list_noting", null);
			this.getRequest().setAttribute("falg", true);
			this.getRequest().setAttribute("supplierId", supplierInfo.getSupplierId());
			this.getRequest().setAttribute("isAudit", supplierInfo.getIsAudit());
			this.getRequest().setAttribute("message", "提交审核成功");
			this.getRequest().setAttribute("operModule", "提交供应商基本信息审核");
		} catch (Exception e) {
			log("供应商信息完善 提交审核信息错误！", e);
			throw new BaseException("供应商信息完善 提交审核信息错误！", e);
		}
		return "supplierBaseInfoComplete";
		
	}
	/**
	 * 根据产品类别主键获得名称
	 * @return
	 * @throws BaseException 
	 */
	public String convertProKindIdToProKindName(String[] proKindIds) throws BaseException {
		
		String proKindNames = "";
		MaterialKind materialKind=null;
		try{
			if(proKindIds!=null&&proKindIds.length>0){
				for(int i=1;i<proKindIds.length-1;i++){
					if(proKindIds[i]!=""){
						materialKind = this.iMaterialBiz.getMaterialKind(Long.parseLong(proKindIds[i]));
						proKindNames += materialKind.getMkName()+",";
					}
				}
			}
			proKindNames = StringUtil.subStringLastOfSeparator(proKindNames, ',');
		} catch (Exception e) {
			log("根据产品类别主键获得名称信息错误！", e);
			throw new BaseException("根据产品类别主键获得名称信息错误！", e);
		}
		return proKindNames;
		
	}
	/**
	 * 根据供应商主键获得名称
	 * @return
	 * @throws BaseException 
	 */
	public String convertSupplierIdToSupplierName(String[] supplierIds) throws BaseException {
		
		String supplierNames = "";
		try{
			if(supplierIds!=null&&supplierIds.length>0){
				for(int i=0;i<supplierIds.length;i++){
					if(supplierIds[i]!=""){
						supplierNames += ",";
					}
				}
			}
			supplierNames = StringUtil.subStringLastOfSeparator(supplierNames, ',');
		} catch (Exception e) {
			log("根据供应商主键获得名称信息错误！", e);
			throw new BaseException("根据供应商主键获得名称信息错误！", e);
		}
		return supplierNames;
		
	}
	/**
	 * 待审核的供应商变更信息tab
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSupplierSelectInfoChange() throws BaseException {
		try{
          Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
          supplierInfo=new SupplierInfo();
          supplierInfo.setSupplierId(supplierId);
		} catch (Exception e) {
			log("查看供应商基本信息tab初始化错误！", e);
			throw new BaseException("查看供应商基本信息tab初始化错误！", e);
		}
		return "select_index_change" ;
	}

	/**
	 * 查看供应商基本明细信息(变更信息审核)
	 * @return
	 * @throws BaseException 
	 */
	public String viewSupplierBaseInfoDetailChange() throws BaseException {
		
		try{
			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo( supplierInfo.getSupplierId() );
			
			supplierInfo.setSupplierTypeCn(BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getSupplierType(),DictStatus.COMMON_DICT_TYPE_1710));
			supplierInfo.setIndustryOwnedCn(BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getIndustryOwned(), DictStatus.COMMON_DICT_TYPE_1707));
			supplierInfo.setManagementModelCn(BaseDataInfosUtil.convertDictCodeToName(supplierInfo.getManagementModel(), DictStatus.COMMON_DICT_TYPE_1708));
			
			SupplierInfoChange supplierInfoChange=this.iSupplierInfoChangeBiz.getSupplierInfoChangeBySupplierId(supplierInfo.getSupplierId());
			this.getRequest().setAttribute("supplierInfoChange", supplierInfoChange);
		} catch (Exception e) {
			log("查看供应商基本明细信息(变更信息审核)错误！", e);
			throw new BaseException("查看供应商基本明细信息(变更信息审核)错误！", e);
		}
		return "detail_change";
		
	}
	/**
	 * 触屏版供应商个人中心
	 * @return
	 * @throws BaseException
	 */
	public String viewSuppplierPhoneMobile() throws BaseException{
		try {

			supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(UserRightInfoUtil.getSupplierId(this.getRequest()));
		} catch (Exception e) {
			log("触屏版供应商个人中心错误！", e);
			throw new BaseException("触屏版供应商个人中心错误！", e);
		}
		return "supplierPhoneMobile";
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}

	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}
	public Map getSupplierTypeMap() {
		return supplierTypeMap;
	}

	public void setSupplierTypeMap(Map supplierTypeMap) {
		this.supplierTypeMap = supplierTypeMap;
	}

	public Map getSupplierLevelMap() {
		return supplierLevelMap;
	}
	
	public void setSupplierLevelMap(Map supplierLevelMap) {
		this.supplierLevelMap = supplierLevelMap;
	}
	
	public Map getSupplierStatusMap() {
		return supplierStatusMap;
	}

	public void setSupplierStatusMap(Map supplierStatusMap) {
		this.supplierStatusMap = supplierStatusMap;
	}
	public Map getRegistrationTypeMap() {
		return registrationTypeMap;
	}

	public void setRegistrationTypeMap(Map registrationTypeMap) {
		this.registrationTypeMap = registrationTypeMap;
	}

	public ISupplierCertificateInfoBiz getiSupplierCertificateInfoBiz() {
		return iSupplierCertificateInfoBiz;
	}

	public void setiSupplierCertificateInfoBiz(
			ISupplierCertificateInfoBiz iSupplierCertificateInfoBiz) {
		this.iSupplierCertificateInfoBiz = iSupplierCertificateInfoBiz;
	}

	public ISupplierInfoChangeBiz getiSupplierInfoChangeBiz() {
		return iSupplierInfoChangeBiz;
	}

	public void setiSupplierInfoChangeBiz(
			ISupplierInfoChangeBiz iSupplierInfoChangeBiz) {
		this.iSupplierInfoChangeBiz = iSupplierInfoChangeBiz;
	}

	public SupplierInfoChange getSupplierInfoChange() {
		return supplierInfoChange;
	}

	public void setSupplierInfoChange(SupplierInfoChange supplierInfoChange) {
		this.supplierInfoChange = supplierInfoChange;
	}

	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}

	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}	

}
