package com.ced.sip.priceLibrary.action;

import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.priceLibrary.biz.IPriceLibraryBiz;
import com.ced.sip.priceLibrary.entity.PriceLibrary;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.entity.MaterialList;
/** 
 * 类名称：PriceLibraryAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-12
 */
public class PriceLibraryAction extends BaseAction {

	// 价格库 
	private IPriceLibraryBiz iPriceLibraryBiz;
	//物料库
	private IMaterialBiz iMaterialBiz;
	
	// 价格库
	private PriceLibrary priceLibrary;
	//物料数据
	private MaterialList materialList;
	
	/**
	 * 查看价格库信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewPriceLibrary() throws BaseException {
		
		try{
		
		} catch (Exception e) {
			log.error("查看价格库信息列表错误！", e);
			throw new BaseException("查看价格库信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看价格库信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findPriceLibrary() throws BaseException {
		
		try{
			if(materialList==null)
			{
				materialList=new MaterialList();
			}
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String materialName=this.getRequest().getParameter("materialName");
			materialList.setMaterialName(materialName);
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) materialList.setComId(comId);
			
			//String iden=this.getRequest().getParameter("iden");
			this.setListValue( this.iMaterialBiz.getMaterialListList(getRollPageDataTables(), materialList));
			
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看价格库信息列表错误！", e);
			throw new BaseException("查看价格库信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存价格库信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String savePriceLibraryInit() throws BaseException {
		try{
		
		} catch (Exception e) {
			log("保存价格库信息初始化错误！", e);
			throw new BaseException("保存价格库信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存价格库信息
	 * @return
	 * @throws BaseException 
	 */
	public String savePriceLibrary() throws BaseException {
		
		try{
			this.iPriceLibraryBiz.savePriceLibrary(priceLibrary);			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增价格库");
		} catch (Exception e) {
			log("保存价格库信息错误！", e);
			throw new BaseException("保存价格库信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 查看历史价格信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewHistoryPrice() throws BaseException {
		
		try{
			String materialIdTmp=this.getRequest().getParameter("materialId");
			String buyCode=this.getRequest().getParameter("buyCode");
			
			priceLibrary=new PriceLibrary();
			if(StringUtil.isNotBlank(materialIdTmp)){
				Long materialId=Long.parseLong(materialIdTmp);
				priceLibrary.setMaterialId(materialId);
				materialList=this.iMaterialBiz.getMaterialList(materialId);
			}else{
				priceLibrary.setMaterialCode(buyCode);
				materialList=this.iMaterialBiz.getMaterialListByCode(buyCode);
			}
			List<PriceLibrary> list=this.iPriceLibraryBiz.getPriceLibraryList(priceLibrary);
			String xAxisData="",yAxisData="";
			int i=0;
			for(PriceLibrary priceLibrary:list){
				if(i==0){
				   xAxisData+="'"+DateUtil.getDefaultDateFormat(priceLibrary.getPriceTime())+"'";
				   yAxisData+=priceLibrary.getPrice();
				}else{
				   xAxisData+=",'"+DateUtil.getDefaultDateFormat(priceLibrary.getPriceTime())+"'";
				   yAxisData+=","+priceLibrary.getPrice();
				}
				i++;
			}
			this.getRequest().setAttribute("xAxisData", xAxisData);
			this.getRequest().setAttribute("yAxisData", yAxisData);
			this.getRequest().setAttribute("materialName", materialList.getMaterialName());
		} catch (Exception e) {
			log("查看历史价格信息错误！", e);
			throw new BaseException("查看历史价格信息错误！", e);
		}
		return "historyPriceLibrary";
		
	}

	public IPriceLibraryBiz getiPriceLibraryBiz() {
		return iPriceLibraryBiz;
	}

	public void setiPriceLibraryBiz(IPriceLibraryBiz iPriceLibraryBiz) {
		this.iPriceLibraryBiz = iPriceLibraryBiz;
	}

	public PriceLibrary getPriceLibrary() {
		return priceLibrary;
	}

	public void setPriceLibrary(PriceLibrary priceLibrary) {
		this.priceLibrary = priceLibrary;
	}

	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}

	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}
	
}
