package com.ced.sip.priceLibrary.entity;

import java.util.Date;

/** 
 * 类名称：PriceLibrary
 * 创建人：luguanglei 
 * 创建时间：2017-08-12
 */
public class PriceLibrary implements java.io.Serializable {

	// 属性信息
	private Long plid;     //主键
	private Long materialId;     //采购对象ID
	private String materialCode;	 //采购对象编码
	private String mnemonicCode;	 //
	private String materialName;	 //采购对象名称
	private String materialType;	 //规格型号
	private String unit;	 //计量单位
	private String priceType;	 //价格类别
	private Date priceTime;    //价格采集时间
	private Double price;	//价格
	private String remark;	 //备注
	private String writer;	 //编制人
	private Date writeDate;    //编制时间
	private Long comId;     //	
	private Long supplierId;//供应商id
	private String supplierName;//供应商名称
	
	
	public PriceLibrary() {
		super();
	}
	
	public Long getPlid(){
	   return  plid;
	} 
	public void setPlid(Long plid) {
	   this.plid = plid;
    }     
	public Long getMaterialId(){
	   return  materialId;
	} 
	public void setMaterialId(Long materialId) {
	   this.materialId = materialId;
    }     
	public String getMaterialCode(){
	   return  materialCode;
	} 
	public void setMaterialCode(String materialCode) {
	   this.materialCode = materialCode;
    }
	public String getMnemonicCode(){
	   return  mnemonicCode;
	} 
	public void setMnemonicCode(String mnemonicCode) {
	   this.mnemonicCode = mnemonicCode;
    }
	public String getMaterialName(){
	   return  materialName;
	} 
	public void setMaterialName(String materialName) {
	   this.materialName = materialName;
    }
	public String getMaterialType(){
	   return  materialType;
	} 
	public void setMaterialType(String materialType) {
	   this.materialType = materialType;
    }
	public String getUnit(){
	   return  unit;
	} 
	public void setUnit(String unit) {
	   this.unit = unit;
    }
	public String getPriceType(){
	   return  priceType;
	} 
	public void setPriceType(String priceType) {
	   this.priceType = priceType;
    }
	public Date getPriceTime(){
	   return  priceTime;
	} 
	public void setPriceTime(Date priceTime) {
	   this.priceTime = priceTime;
    }	    
	public Double getPrice(){
	   return  price;
	} 
	public void setPrice(Double price) {
	   this.price = price;
    }	
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }
	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}     
}