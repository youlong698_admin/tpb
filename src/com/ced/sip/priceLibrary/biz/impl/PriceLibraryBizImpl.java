package com.ced.sip.priceLibrary.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.priceLibrary.biz.IPriceLibraryBiz;
import com.ced.sip.priceLibrary.entity.PriceLibrary;
/** 
 * 类名称：PriceLibraryBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-08-12
 */
public class PriceLibraryBizImpl extends BaseBizImpl implements IPriceLibraryBiz  {
	
	/**
	 * 根据主键获得价格库表实例
	 * @param id 主键
	 * @author luguanglei 2017-08-12
	 * @return
	 * @throws BaseException 
	 */
	public PriceLibrary getPriceLibrary(Long id) throws BaseException {
		return (PriceLibrary)this.getObject(PriceLibrary.class, id);
	}
	
	/**
	 * 获得价格库表实例
	 * @param priceLibrary 价格库表实例
	 * @author luguanglei 2017-08-12
	 * @return
	 * @throws BaseException 
	 */
	public PriceLibrary getPriceLibrary(PriceLibrary priceLibrary) throws BaseException {
		return (PriceLibrary)this.getObject(PriceLibrary.class, priceLibrary.getPlid() );
	}
	
	/**
	 * 添加价格库信息
	 * @param priceLibrary 价格库表实例
	 * @author luguanglei 2017-08-12
	 * @throws BaseException 
	 */
	public void savePriceLibrary(PriceLibrary priceLibrary) throws BaseException{
		this.saveObject( priceLibrary ) ;
	}
	
	/**
	 * 更新价格库表实例
	 * @param priceLibrary 价格库表实例
	 * @author luguanglei 2017-08-12
	 * @throws BaseException 
	 */
	public void updatePriceLibrary(PriceLibrary priceLibrary) throws BaseException{
		this.updateObject( priceLibrary ) ;
	}
	
	/**
	 * 删除价格库表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-12
	 * @throws BaseException 
	 */
	public void deletePriceLibrary(String id) throws BaseException {
		this.removeObject( this.getPriceLibrary( new Long(id) ) ) ;
	}
	
	/**
	 * 删除价格库表实例
	 * @param priceLibrary 价格库表实例
	 * @author luguanglei 2017-08-12
	 * @throws BaseException 
	 */
	public void deletePriceLibrary(PriceLibrary priceLibrary) throws BaseException {
		this.removeObject( priceLibrary ) ;
	}
	
	/**
	 * 删除价格库表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-08-12
	 * @throws BaseException 
	 */
	public void deletePriceLibrarys(String[] id) throws BaseException {
		this.removeBatchObject(PriceLibrary.class, id) ;
	}
	
	/**
	 * 获得价格库表数据集
	 * priceLibrary 价格库表实例
	 * @author luguanglei 2017-08-12
	 * @return
	 * @throws BaseException 
	 */
	public PriceLibrary getPriceLibraryByPriceLibrary(PriceLibrary priceLibrary) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PriceLibrary de where 1 = 1 " );

		hql.append(" order by de.plid desc ");
		List list = this.getObjects( hql.toString() );
		priceLibrary = new PriceLibrary();
		if(list!=null&&list.size()>0){
			priceLibrary = (PriceLibrary)list.get(0);
		}
		return priceLibrary;
	}
	
	/**
	 * 获得所有价格库表数据集
	 * @param priceLibrary 查询参数对象
	 * @author luguanglei 2017-08-12
	 * @return
	 * @throws BaseException 
	 */
	public List getPriceLibraryList(PriceLibrary priceLibrary) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PriceLibrary de where 1 = 1 " );
		if(priceLibrary != null){
			if(StringUtil.isNotBlank(priceLibrary.getMaterialId())){
				hql.append(" and de.materialId =").append(priceLibrary.getMaterialId()).append("");
			}
			if(StringUtil.isNotBlank(priceLibrary.getMaterialCode())){
				hql.append(" and de.materialCode ='").append(priceLibrary.getMaterialCode()).append("'");
			}
		}
		hql.append(" order by de.priceTime ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有价格库表数据集
	 * @param rollPage 分页对象
	 * @param priceLibrary 查询参数对象
	 * @author luguanglei 2017-08-12
	 * @return
	 * @throws BaseException 
	 */
	public List getPriceLibraryList(RollPage rollPage, PriceLibrary priceLibrary) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PriceLibrary de where 1 = 1 " );

		hql.append(" order by de.plid desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 插入授标明细数据
	 * @param lx
	 * @param rcId
	 * @param comId
	 * @throws BaseException
	 */
	public void saveBidAwardPriceLibrary(String lx,Long rcId,Long comId)
			throws BaseException {
		String sql="insert into price_library(plid,material_id,material_code,mnemonic_code,material_name,material_type,unit,price_type,price,price_time,com_id,supplier_id) " +
				"select seq_price_library_id.nextval,rcd.material_id,rcd.buy_code,rcd.mnemonic_code,rcd.buy_name,rcd.material_type,rcd.unit,'"+lx+"',bad.price,ba.write_date,"+comId+",ba.supplier_id from required_collect_detail rcd,bid_award_detail bad,bid_award ba where ba.ba_id=bad.ba_id and rcd.rcd_id=bad.rcd_id and ba.rc_id="+rcId;
		this.updateJdbcSql(sql);
	}

	/**
	 * 插入供应商最后一次的项目报价明细数据
	 * @param lx
	 * @param rcId
	 * @param comId
	 * @throws BaseException
	 */
	public void saveBidPriceLibrary(String lx, Long rcId, Long comId)
			throws BaseException {
		String sql="insert into price_library(plid,material_id,material_code,mnemonic_code,material_name,material_type,unit,price_type,price,price_time,com_id,supplier_id) select seq_price_library_id.nextval,rcd.material_id,rcd.buy_code,rcd.mnemonic_code,rcd.buy_name,rcd.material_type,rcd.unit,'"+lx+"',bpd.price,bp.write_date,"+comId+",bp.supplier_id from required_collect_detail rcd,bid_price_detail bpd,bid_price bp where rcd.rcd_id=bpd.rcd_id and bp.bp_id=bpd.bp_id and bpd.rc_id="+rcId;
		this.updateJdbcSql(sql);
	}
	
	
}
