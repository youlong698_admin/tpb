package com.ced.sip.priceLibrary.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.priceLibrary.entity.PriceLibrary;
/** 
 * 类名称：IPriceLibraryBiz
 * 创建人：luguanglei 
 * 创建时间：2017-08-12
 */
public interface IPriceLibraryBiz {

	/**
	 * 根据主键获得价格库表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract PriceLibrary getPriceLibrary(Long id) throws BaseException;

	/**
	 * 添加价格库信息
	 * @param priceLibrary 价格库表实例
	 * @throws BaseException 
	 */
	abstract void savePriceLibrary(PriceLibrary priceLibrary) throws BaseException;

	/**
	 * 更新价格库表实例
	 * @param priceLibrary 价格库表实例
	 * @throws BaseException 
	 */
	abstract void updatePriceLibrary(PriceLibrary priceLibrary) throws BaseException;

	/**
	 * 删除价格库表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deletePriceLibrary(String id) throws BaseException;

	/**
	 * 删除价格库表实例
	 * @param priceLibrary 价格库表实例
	 * @throws BaseException 
	 */
	abstract void deletePriceLibrary(PriceLibrary priceLibrary) throws BaseException;

	/**
	 * 删除价格库表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deletePriceLibrarys(String[] id) throws BaseException;

	/**
	 * 获得价格库表数据集
	 * priceLibrary 价格库表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract PriceLibrary getPriceLibraryByPriceLibrary(PriceLibrary priceLibrary) throws BaseException ;
	
	/**
	 * 获得所有价格库表数据集
	 * @param priceLibrary 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getPriceLibraryList(PriceLibrary priceLibrary) throws BaseException ;
	
	/**
	 * 获得所有价格库表数据集
	 * @param rollPage 分页对象
	 * @param priceLibrary 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getPriceLibraryList(RollPage rollPage, PriceLibrary priceLibrary)
			throws BaseException;
	/**
	 * 插入授标明细数据
	 * @param lx
	 * @param rcId
	 * @param comId
	 * @throws BaseException
	 */
	abstract void saveBidAwardPriceLibrary(String lx,Long rcId,Long comId) throws BaseException;
	/**
	 * 插入供应商最后一次的项目报价明细数据
	 * @param lx
	 * @param rcId
	 * @param comId
	 * @throws BaseException
	 */
	abstract void saveBidPriceLibrary(String lx,Long rcId,Long comId) throws BaseException;
	
}