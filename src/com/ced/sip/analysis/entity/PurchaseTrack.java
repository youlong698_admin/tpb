package com.ced.sip.analysis.entity;

import java.util.Date;

public class PurchaseTrack {
	private Long rcId;
	private String bidCode;
	private String buyRemark;
	private String buyWay;
	private String supplierType;
	private String writer;
	private Date writeDate;
	private String status;
	private Long deptId;//立项人部门id
	private Long purchaseDeptId;//采购组织
	private Double totalBudget;
	private String serviceStatusCn;
	private String bidStatusCn;
	private Long comId;
    private Date noticePublishDate;
    private Date bidReturnDate;
    private Date bidOpenDate;
    private Date bidAwardDate;
    private Long changeNum;
    private Double bidWinningPrice;
    
	private String writerCn;
	private String purchaseDeptName;
	private Integer inviteSupplierNumber;//报名个数
	private Integer priceSupplierNumber;//回标个数
	
	
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getBidCode() {
		return bidCode;
	}
	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}
	public String getBuyRemark() {
		return buyRemark;
	}
	public void setBuyRemark(String buyRemark) {
		this.buyRemark = buyRemark;
	}
	public String getBuyWay() {
		return buyWay;
	}
	public void setBuyWay(String buyWay) {
		this.buyWay = buyWay;
	}
	public String getSupplierType() {
		return supplierType;
	}
	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public Date getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Long getPurchaseDeptId() {
		return purchaseDeptId;
	}
	public void setPurchaseDeptId(Long purchaseDeptId) {
		this.purchaseDeptId = purchaseDeptId;
	}
	public Double getTotalBudget() {
		return totalBudget;
	}
	public void setTotalBudget(Double totalBudget) {
		this.totalBudget = totalBudget;
	}
	public String getServiceStatusCn() {
		return serviceStatusCn;
	}
	public void setServiceStatusCn(String serviceStatusCn) {
		this.serviceStatusCn = serviceStatusCn;
	}
	public String getBidStatusCn() {
		return bidStatusCn;
	}
	public void setBidStatusCn(String bidStatusCn) {
		this.bidStatusCn = bidStatusCn;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public Date getNoticePublishDate() {
		return noticePublishDate;
	}
	public void setNoticePublishDate(Date noticePublishDate) {
		this.noticePublishDate = noticePublishDate;
	}
	public Date getBidReturnDate() {
		return bidReturnDate;
	}
	public void setBidReturnDate(Date bidReturnDate) {
		this.bidReturnDate = bidReturnDate;
	}
	public Date getBidOpenDate() {
		return bidOpenDate;
	}
	public void setBidOpenDate(Date bidOpenDate) {
		this.bidOpenDate = bidOpenDate;
	}
	public Date getBidAwardDate() {
		return bidAwardDate;
	}
	public void setBidAwardDate(Date bidAwardDate) {
		this.bidAwardDate = bidAwardDate;
	}
	public Long getChangeNum() {
		return changeNum;
	}
	public void setChangeNum(Long changeNum) {
		this.changeNum = changeNum;
	}
	public Double getBidWinningPrice() {
		return bidWinningPrice;
	}
	public void setBidWinningPrice(Double bidWinningPrice) {
		this.bidWinningPrice = bidWinningPrice;
	}
	public String getWriterCn() {
		return writerCn;
	}
	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}
	public String getPurchaseDeptName() {
		return purchaseDeptName;
	}
	public void setPurchaseDeptName(String purchaseDeptName) {
		this.purchaseDeptName = purchaseDeptName;
	}
	public Integer getInviteSupplierNumber() {
		return inviteSupplierNumber;
	}
	public void setInviteSupplierNumber(Integer inviteSupplierNumber) {
		this.inviteSupplierNumber = inviteSupplierNumber;
	}
	public Integer getPriceSupplierNumber() {
		return priceSupplierNumber;
	}
	public void setPriceSupplierNumber(Integer priceSupplierNumber) {
		this.priceSupplierNumber = priceSupplierNumber;
	}
	
}
