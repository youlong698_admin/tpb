package com.ced.sip.analysis.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.analysis.biz.IPurchaseAmountBiz;
import com.ced.sip.analysis.entity.MaterialListReport;
import com.ced.sip.analysis.entity.PurchaseAmount;
import com.ced.sip.analysis.entity.SupplierWinning;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.ListToMap;
import com.ced.sip.common.utils.StringUtil;

public class PurchaseChartAction extends BaseAction {

	// 采购金额统计
	private IPurchaseAmountBiz iPurchaseAmountBiz;
	
	private PurchaseAmount purchaseAmount;

	/**
	 * 采购金额查询统计
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewPurchaseAmount() throws BaseException {
		try {
			int type=Integer.parseInt(this.getRequest().getParameter("type"));  //0月 度  1季度   2半年度  3 年度
			String year = this.getRequest().getParameter(
					"year");
			if(StringUtil.isBlank(year)) year=DateUtil.getCurrentYear()+"";
			String buyWay = this.getRequest().getParameter("buyWay");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("buyWay", buyWay);
			map.put("year", year);
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
						"t");
			}
			
            //横坐标、采购金额、项目数量、饼图采购金额、饼图项目数量
			String xAxisData="",amountMoney="",totalNumber="",pieAmountMoney="",pieTotalNumber="";
			List<PurchaseAmount> list =null;
			Map<String, PurchaseAmount> mapAmount=null;
			if(type==0){
			    list=this.iPurchaseAmountBiz.getPurchaseAmountBuyMonth(map,sqlStr);
			    mapAmount=ListToMap.list2Map3(list,"getMonth",PurchaseAmount.class);  
			    xAxisData="'1月',";
			    if(mapAmount.get("01")!=null){
			    	purchaseAmount=mapAmount.get("01");
			    	amountMoney=purchaseAmount.getAmountMoney()+",";
			    	totalNumber=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'1月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'1月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'1月'},";
			    	pieTotalNumber+="{value:0, name:'1月'},";
			    }
			    xAxisData+="'2月',";
			    if(mapAmount.get("02")!=null){
			    	purchaseAmount=mapAmount.get("02");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'2月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'2月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'2月'},";
			    	pieTotalNumber+="{value:0, name:'2月'},";
			    }
			    xAxisData+="'3月',";
			    if(mapAmount.get("03")!=null){
			    	purchaseAmount=mapAmount.get("03");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'3月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'3月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'3月'},";
			    	pieTotalNumber+="{value:0, name:'3月'},";
			    }
			    xAxisData+="'4月',";
			    if(mapAmount.get("04")!=null){
			    	purchaseAmount=mapAmount.get("04");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'4月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'4月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'4月'},";
			    	pieTotalNumber+="{value:0, name:'4月'},";
			    }
			    xAxisData+="'5月',";
			    if(mapAmount.get("05")!=null){
			    	purchaseAmount=mapAmount.get("05");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'5月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'5月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'5月'},";
			    	pieTotalNumber+="{value:0, name:'5月'},";
			    }
			    xAxisData+="'6月',";
			    if(mapAmount.get("06")!=null){
			    	purchaseAmount=mapAmount.get("06");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'6月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'6月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'6月'},";
			    	pieTotalNumber+="{value:0, name:'6月'},";
			    }
			    xAxisData+="'7月',";
			    if(mapAmount.get("07")!=null){
			    	purchaseAmount=mapAmount.get("07");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'7月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'7月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'7月'},";
			    	pieTotalNumber+="{value:0, name:'7月'},";
			    }
			    xAxisData+="'8月',";
			    if(mapAmount.get("08")!=null){
			    	purchaseAmount=mapAmount.get("08");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'8月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'8月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'8月'},";
			    	pieTotalNumber+="{value:0, name:'8月'},";
			    }
			    xAxisData+="'9月',";
			    if(mapAmount.get("09")!=null){
			    	purchaseAmount=mapAmount.get("09");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'9月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'9月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'9月'},";
			    	pieTotalNumber+="{value:0, name:'9月'},";
			    }
			    xAxisData+="'10月',";
			    if(mapAmount.get("10")!=null){
			    	purchaseAmount=mapAmount.get("10");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'10月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'10月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'10月'},";
			    	pieTotalNumber+="{value:0, name:'10月'},";
			    }
			    xAxisData+="'11月',";
			    if(mapAmount.get("11")!=null){
			    	purchaseAmount=mapAmount.get("11");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    	totalNumber+=purchaseAmount.getTotalNumber()+",";
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'11月'},";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'11月'},";
			    }else{
			    	amountMoney+="0,";
			    	totalNumber+="0,";
			    	pieAmountMoney+="{value:0, name:'11月'},";
			    	pieTotalNumber+="{value:0, name:'11月'},";
			    }
			    xAxisData+="'12月'";
			    if(mapAmount.get("12")!=null){
			    	purchaseAmount=mapAmount.get("12");
			    	amountMoney+=purchaseAmount.getAmountMoney();
			    	totalNumber+=purchaseAmount.getTotalNumber();
			    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'12月'}";
			    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'12月'}";
			    }else{
			    	amountMoney+="0";
			    	totalNumber+="0";
			    	pieAmountMoney+="{value:0, name:'12月'}";
			    	pieTotalNumber+="{value:0, name:'12月'}";
			    }
			}else if(type==1){
				 list=this.iPurchaseAmountBiz.getPurchaseAmountBuySecond(map,sqlStr);
				 mapAmount=ListToMap.list2Map3(list,"getMonth",PurchaseAmount.class);  
				    xAxisData="'第1季度',";
				    if(mapAmount.get("1")!=null){
				    	purchaseAmount=mapAmount.get("1");
				    	amountMoney=purchaseAmount.getAmountMoney()+",";
				    	totalNumber=purchaseAmount.getTotalNumber()+",";
				    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'第1季度'},";
				    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'第1季度'},";
				    }else{
				    	amountMoney+="0,";
				    	totalNumber+="0,";
				    	pieAmountMoney+="{value:0, name:'第1季度'},";
				    	pieTotalNumber+="{value:0, name:'第1季度'},";
				    }
				    xAxisData+="'第2季度',";
				    if(mapAmount.get("2")!=null){
				    	purchaseAmount=mapAmount.get("2");
				    	amountMoney+=purchaseAmount.getAmountMoney()+",";
				    	totalNumber+=purchaseAmount.getTotalNumber()+",";
				    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'第2季度'},";
				    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'第2季度'},";
				    }else{
				    	amountMoney+="0,";
				    	totalNumber+="0,";
				    	pieAmountMoney+="{value:0, name:'第2季度'},";
				    	pieTotalNumber+="{value:0, name:'第2季度'},";
				    }
				    xAxisData+="'第3季度',";
				    if(mapAmount.get("3")!=null){
				    	purchaseAmount=mapAmount.get("3");
				    	amountMoney+=purchaseAmount.getAmountMoney()+",";
				    	totalNumber+=purchaseAmount.getTotalNumber()+",";
				    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'3季度'},";
				    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'3季度'},";
				    }else{
				    	amountMoney+="0,";
				    	totalNumber+="0,";
				    	pieAmountMoney+="{value:0, name:'第3季度'},";
				    	pieTotalNumber+="{value:0, name:'第3季度'},";
				    }
				    xAxisData+="'第4季度',";
				    if(mapAmount.get("4")!=null){
				    	purchaseAmount=mapAmount.get("4");
				    	amountMoney+=purchaseAmount.getAmountMoney()+"";
				    	totalNumber+=purchaseAmount.getTotalNumber()+"";
				    	pieAmountMoney+="{value:"+purchaseAmount.getAmountMoney()+", name:'第4季度'}";
				    	pieTotalNumber+="{value:"+purchaseAmount.getTotalNumber()+", name:'第4季度'}";
				    }else{
				    	amountMoney+="0";
				    	totalNumber+="0";
				    	pieAmountMoney+="{value:0, name:'第4季度'}";
				    	pieTotalNumber+="{value:0, name:'第4季度'}";
				    }
		    	
			}else if(type==2){
				 list=this.iPurchaseAmountBiz.getPurchaseAmountBuySecond(map,sqlStr);
				 double amountMoney0=0.00,amountMoney1=0.00;
				 int totalNumber0=0,totalNumber1=0;
				 for(PurchaseAmount purchaseAmount:list){
					 if(purchaseAmount.getMonth().equals("1")||purchaseAmount.getMonth().equals("2")){
						 amountMoney0+=purchaseAmount.getAmountMoney();
						 totalNumber0+=purchaseAmount.getTotalNumber();
					 }else if(purchaseAmount.getMonth().equals("3")||purchaseAmount.getMonth().equals("4")){
						 amountMoney1+=purchaseAmount.getAmountMoney();
						 totalNumber1+=purchaseAmount.getTotalNumber();
					 }
				 }
			     xAxisData="'上半年','下半年'";
		    	 amountMoney=amountMoney0+","+amountMoney1;
		    	 totalNumber=totalNumber0+","+totalNumber1;
		    	 pieAmountMoney="{value:"+amountMoney0+", name:'上半年'},{value:"+amountMoney1+", name:'下半年'}";
		    	 pieTotalNumber="{value:"+totalNumber0+", name:'上半年'},{value:"+totalNumber1+", name:'下半年'}";
			}else if(type==3){
				list=this.iPurchaseAmountBiz.getPurchaseAmountBuyYear(map,sqlStr);
				int i=0;
				for(PurchaseAmount purchaseAmount:list){
				  if(i==0){
					  xAxisData="'"+purchaseAmount.getYear()+"年'";
				      amountMoney=purchaseAmount.getAmountMoney()+"";
				      totalNumber=purchaseAmount.getTotalNumber()+"";
				      pieAmountMoney="{value:"+purchaseAmount.getAmountMoney()+", name:'"+purchaseAmount.getYear()+"年'}";
				      pieTotalNumber="{value:"+purchaseAmount.getTotalNumber()+", name:'"+purchaseAmount.getYear()+"年'}";
				  }else{
					  xAxisData+=",'"+purchaseAmount.getYear()+"年'";
				      amountMoney+=","+purchaseAmount.getAmountMoney();
				      totalNumber+=","+purchaseAmount.getTotalNumber();
				      pieAmountMoney+=",{value:"+purchaseAmount.getAmountMoney()+", name:'"+purchaseAmount.getYear()+"年'}";
				      pieTotalNumber+=",{value:"+purchaseAmount.getTotalNumber()+", name:'"+purchaseAmount.getYear()+"年'}";
				  }
				  i++;
				}
			}
			this.getRequest().setAttribute("xAxisData", xAxisData);
			this.getRequest().setAttribute("amountMoney", amountMoney);
			this.getRequest().setAttribute("totalNumber", totalNumber);
			this.getRequest().setAttribute("pieAmountMoney", pieAmountMoney);
			this.getRequest().setAttribute("pieTotalNumber", pieTotalNumber);
			this.getRequest().setAttribute("year", year);
			this.getRequest().setAttribute("buyWay", buyWay);
			this.getRequest().setAttribute("type", type);
			Map purchaseWayMap=TableStatusMap.purchaseWay;
			this.getRequest().setAttribute("purchaseWayMap", purchaseWayMap);
		} catch (Exception e) {
			log.error("采购金额查询统计错误！", e);
			throw new BaseException("采购金额查询统计错误！", e);
		}
		return "purchaseAmount";
	}
	/**
	 * 供应商采购成交统计 图表
	 * @return
	 * @throws BaseException
	 */
	public String viewSupplierWinning() throws BaseException{
		try {
			String buyWay=this.getRequest().getParameter("buyWay");
			String writeDateStart=this.getRequest().getParameter("writeDateStart");
			String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("buyWay", buyWay);
			map.put("writeDateStart", writeDateStart);
			map.put("writeDateEnd", writeDateEnd);
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
						"ba");
			}
			List list=this.iPurchaseAmountBiz.getSupplierWinningList(map, sqlStr);
            int count=list.size();
            if(count>20) count=20;
            SupplierWinning supplierWinning=null;
            String amountMoney="",totalNumber="",xAxis="";
            for(int i=0;i<count;i++){
            	supplierWinning=(SupplierWinning)list.get(i);
            	if(i==0){
            		amountMoney+=supplierWinning.getAmountMoney();
				    totalNumber+=supplierWinning.getTotalNumber();
				    xAxis+="'"+supplierWinning.getSupplierName()+"'";
            	}else{
	            	amountMoney+=","+supplierWinning.getAmountMoney();
				    totalNumber+=","+supplierWinning.getTotalNumber();
				    xAxis+=",'"+supplierWinning.getSupplierName()+"'";
            	}
            }
			
            this.getRequest().setAttribute("amountMoney", amountMoney);
            this.getRequest().setAttribute("totalNumber", totalNumber);
            this.getRequest().setAttribute("xAxis", xAxis);
            this.getRequest().setAttribute("count", count);
			this.getRequest().setAttribute("buyWay", buyWay);
			this.getRequest().setAttribute("writeDateStart", writeDateStart);
			this.getRequest().setAttribute("writeDateEnd", writeDateEnd);
			Map purchaseWayMap=TableStatusMap.purchaseWay;
			this.getRequest().setAttribute("purchaseWayMap", purchaseWayMap);
		} catch (Exception e) {
			log.error("供应商采购成交统计 图表错误！", e);
			throw new BaseException("供应商采购成交统计 图表错误！", e);
		}
		return "supplierWinning";
	}
	/**
	 * 采购品成交统计排行
	 * @return
	 * @throws BaseException
	 */
	public String viewMaterialReport() throws BaseException{
		try {
			String buyName=this.getRequest().getParameter("buyName");
			String materialType=this.getRequest().getParameter("materialType");
			String writeDateStart=this.getRequest().getParameter("writeDateStart");
			String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
			String mkCode=this.getRequest().getParameter("mkCode");
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("buyName", buyName);
			map.put("materialType", materialType);
			map.put("mkCode", mkCode);
			map.put("writeDateStart", writeDateStart);
			map.put("writeDateEnd", writeDateEnd);
			
		} catch (Exception e) {
			log.error("采购品成交统计排行错误！", e);
			throw new BaseException("采购品成交统计排行错误！", e);
		}
		return "materialReport";
	}
	/**
	 * 采购品成交统计排行
	 * @return
	 * @throws BaseException
	 */
	public String findMaterialReport() throws BaseException{
		try {
			String buyName=this.getRequest().getParameter("buyName");
			String materialType=this.getRequest().getParameter("materialType");
			String writeDateStart=this.getRequest().getParameter("writeDateStart");
			String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
			String mkCode=this.getRequest().getParameter("mkCode");
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("buyName", buyName);
			map.put("materialType", materialType);
			map.put("mkCode", mkCode);
			map.put("writeDateStart", writeDateStart);
			map.put("writeDateEnd", writeDateEnd);
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
						"ba");
			}
			List list=this.iPurchaseAmountBiz.getMaterialReportList(this.getRollPageDataTables(), map, sqlStr);
			this.getPagejsonDataTables(list);
			
		} catch (Exception e) {
			log.error("查询采购品成交统计排行错误！", e);
			throw new BaseException("查询采购品成交统计排行错误！", e);
		}
		return null;
	}
	/**
	 * 采购品成交统计Excel导出
	 * @return
	 * @throws BaseException 
	 */
	 public void  exportMaterialReportExcel() throws BaseException{
		try{
			 List<String> titleList = new ArrayList<String>();
			 titleList.add("物料编码");
			 titleList.add("物料名称");
			 titleList.add("规格型号");
			 titleList.add("中标项目数量");
			 titleList.add("中标总金额");
			 
			 String buyName=this.getRequest().getParameter("buyName");
				String materialType=this.getRequest().getParameter("materialType");
				String writeDateStart=this.getRequest().getParameter("writeDateStart");
				String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
				String mkCode=this.getRequest().getParameter("mkCode");
				Map<String, Object> map=new HashMap<String, Object>();
				map.put("buyName", buyName);
				map.put("materialType", materialType);
				map.put("mkCode", mkCode);
				map.put("writeDateStart", writeDateStart);
				map.put("writeDateEnd", writeDateEnd);
				
				String sqlStr = "";
				if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
					sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
							"ba");
				}
				List<MaterialListReport> list=this.iPurchaseAmountBiz.getMaterialReportListAll( map, sqlStr);
				
				List<Object[]> objList = new ArrayList<Object[]>();
				for(MaterialListReport materialListReport:list)
				{
					
					 Object[]  obj=new Object[]{
							 materialListReport.getBuyCode(),
							 materialListReport.getBuyName(),
							 materialListReport.getMaterialType(),
							 materialListReport.getTotalNumber(),
							 StringUtil.bigNumberTooriginal(materialListReport.getAmountMoney())							 
					 };
					  objList.add(obj);
			 }
			// 输出的excel文件名
			String file = "采购品成交统计.xls";
			String targetfile = this.getServletContext().getRealPath(file);
			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_excel = new HashMap<String, Object>();
			map_excel.put("worksheet", "采购品成交统计");
			map_excel.put("titleList", titleList);
			map_excel.put("valueList", objList);
			excelList.add(map_excel);
			new ExcelUtil().expCommonExcel(targetfile, excelList);
			this.getResponse().setContentType("application/octet-stream; charset=utf-8");
			this.getResponse().setHeader("Content-Disposition", "attachment; filename="+new String(file.getBytes("gbk"),"iso-8859-1") );
			//this.getResponse().setHeader( "Set-Cookie", "name=value; HttpOnly"); 
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			int i = 0;
			readCount = is.read(buff);
			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		}catch (Exception e) {
			log.error("导出采购品成交统计Excel列表错误！", e);
			throw new BaseException("导出采购品成交统计Excel表错误！", e);
	    }
     } 
	public IPurchaseAmountBiz getiPurchaseAmountBiz() {
		return iPurchaseAmountBiz;
	}

	public void setiPurchaseAmountBiz(IPurchaseAmountBiz iPurchaseAmountBiz) {
		this.iPurchaseAmountBiz = iPurchaseAmountBiz;
	}
}
