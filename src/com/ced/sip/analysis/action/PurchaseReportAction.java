package com.ced.sip.analysis.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.analysis.biz.IPurchaseAmountBiz;
import com.ced.sip.analysis.entity.PurchaseTrack;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.biz.IAskBidListBiz;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
import com.ced.sip.purchase.askPrice.util.AskProgressStatusList;
import com.ced.sip.purchase.base.biz.IBidAbnomalBiz;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IBidAwardDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAbnomal;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectBidAward;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatusList;
import com.ced.sip.purchase.bidding.util.BiddingStatusMap;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.util.TenderProgressStatusList;
import com.sun.org.apache.commons.beanutils.BeanUtils;

public class PurchaseReportAction extends BaseAction{
	//需求汇总分包
	private IRequiredCollectBiz iRequiredCollectBiz;
	// 授标 
	private IBidAwardBiz iBidAwardBiz;
	// 报价表
	private IBidPriceBiz iBidPriceBiz;
	// 授标明细 
	private IBidAwardDetailBiz iBidAwardDetailBiz;
	//异常采购
	private IBidAbnomalBiz iBidAbnomalBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	// BidMonitor 标段流程记录表
	private IBidProcessLogBiz iBidProcessLogBiz;
	//采购统计
	private IPurchaseAmountBiz iPurchaseAmountBiz;
	//询价信息
	private IAskBidListBiz iAskBidListBiz;
	//招标信息
	private ITenderBidListBiz iTenderBidListBiz;
	//竞价信息
	private IBiddingBidListBiz iBiddingBidListBiz;
	//日志服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	
	
	private RequiredCollect requiredCollect;
	private RequiredCollectBidAward requiredCollectBidAward; 
	
	private AskBidList askBidList;
	private TenderBidList tenderBidList;
	private BiddingBidList biddingBidList;
	private PurchaseRecordLog purchaseRecordLog;
	
	/**
	 * 采购概况报表
	 * @return
	 * @throws BaseException
	 */
	public String viewPurchaseReport() throws BaseException{
		try {
			Map purchaseWayMap=TableStatusMap.purchaseWay;
			this.getRequest().setAttribute("purchaseWayMap", purchaseWayMap);
		}  catch (Exception e) {
			log.error("采购概况报表错误！", e);
			throw new BaseException("采购概况报表错误！", e);
		}
		return "purchaseReport";
	}
	/**
	 * 获取采购概况报表数据信息
	 * @return
	 * @throws BaseException
	 */
	public String findPurchaseReport() throws BaseException{
		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			// 汇总 列表信息
			Map<String,Object> map= new HashMap<String,Object>();
			String bidCode=this.getRequest().getParameter("bidCode");
			map.put("bidCode", bidCode);
			String buyRemark=this.getRequest().getParameter("buyRemark");
			map.put("buyRemark", buyRemark);
			String supplierName=this.getRequest().getParameter("supplierName");
			map.put("supplierName", supplierName);
			String purchaseWay=this.getRequest().getParameter("purchaseWay");
			map.put("buyWay", purchaseWay);
			String dateStart=this.getRequest().getParameter("dateStart");
			map.put("dateStart", dateStart);
			String dateEnd=this.getRequest().getParameter("dateEnd");
			map.put("dateEnd", dateEnd);
		
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
				map.put("comId", comId);
			}
			List<RequiredCollectBidAward> rcList=new ArrayList<RequiredCollectBidAward>();
			List<Object[]> list=this.iRequiredCollectBiz.getRequiredCollectListForPurchaseReport( this.getRollPageDataTables(), map,sqlStr);
			for(Object[] obj:list){
				requiredCollect = (RequiredCollect)obj[0];
				requiredCollectBidAward= new RequiredCollectBidAward();
				BeanUtils.copyProperties(requiredCollectBidAward,requiredCollect);
				requiredCollectBidAward.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollectBidAward.getWriter()));
				requiredCollectBidAward.setPurchaseDeptName(BaseDataInfosUtil
						.convertDeptIdToName(requiredCollectBidAward
								.getPurchaseDeptId()));
				requiredCollectBidAward.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollectBidAward.getBuyWay()));
				requiredCollectBidAward.setBidPrice((Double)obj[1]);
				requiredCollectBidAward.setBaId((Long)obj[2]);
				requiredCollectBidAward.setSupplierId((Long)obj[3]);
				requiredCollectBidAward.setSupplierName((String)obj[4]);
				requiredCollectBidAward.setBaDate((Date)obj[5]);
				rcList.add(requiredCollectBidAward);
			}
			this.getPagejsonDataTables(rcList);
		}  catch (Exception e) {
			log.error("获取采购概况报表数据信息错误！", e);
			throw new BaseException("获取采购概况报表数据信息错误！", e);
		}
		return null;
	}
	/**
	 * 采购概况报表信息Excel导出
	 * @return
	 * @throws BaseException 
	 */
	 public void  exportPurchaseReportExcel() throws BaseException{
		try{
			 List<String> titleList = new ArrayList<String>();
			 titleList.add("采购组织");
			 titleList.add("项目编号");
			 titleList.add("项目名称");
			 titleList.add("采购方式");
			 titleList.add("中标供应商");
			 titleList.add("中标金额");
			 titleList.add("采购负责人");
			 titleList.add("授标日期");
			 
			 
			 Long comId=UserRightInfoUtil.getComId(getRequest());
			 // 汇总 列表信息
			 Map<String,Object> map= new HashMap<String,Object>();
			 String bidCode=this.getRequest().getParameter("bidCode");
			 map.put("bidCode", bidCode);
			 String buyRemark=this.getRequest().getParameter("buyRemark");
			 map.put("buyRemark", buyRemark);
			 String supplierName=this.getRequest().getParameter("supplierName");
			 map.put("supplierName", supplierName);
			 String purchaseWay=this.getRequest().getParameter("purchaseWay");
			 map.put("buyWay", purchaseWay);
			 String dateStart=this.getRequest().getParameter("dateStart");
			 map.put("dateStart", dateStart);
			 String dateEnd=this.getRequest().getParameter("dateEnd");
			 map.put("dateEnd", dateEnd);
		
			
			 String sqlStr = "";
			 if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
				 map.put("comId", comId);
			 }
			 List<Object[]> objList = new ArrayList<Object[]>();
			 
			 List<Object[]> list=this.iRequiredCollectBiz.getRequiredCollectListForPurchaseReport(map,sqlStr);
			 for(Object[] obj_rb:list){
					requiredCollect = (RequiredCollect)obj_rb[0];
					requiredCollectBidAward= new RequiredCollectBidAward();
					BeanUtils.copyProperties(requiredCollectBidAward,requiredCollect);
					requiredCollect.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollectBidAward.getWriter()));
					requiredCollect.setPurchaseDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredCollectBidAward
									.getPurchaseDeptId()));
					requiredCollect.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollectBidAward.getBuyWay()));
					
					 Object[]  obj=new Object[]{
							 requiredCollect.getPurchaseDeptName(),
							 requiredCollect.getBidCode(),
							 requiredCollect.getBuyRemark(),
							 requiredCollect.getBuyWayCn(),
							 obj_rb[4],
							 obj_rb[1],
							 requiredCollect.getWriterCn(),
							 obj_rb[5]
					 };
					  objList.add(obj);
			  }
		    // 输出的excel文件名
			String file = "采购概况报表信息.xls";
			String targetfile = this.getServletContext().getRealPath(file);
			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_excel = new HashMap<String, Object>();
			map_excel.put("worksheet", "采购概况报表信息");
			map_excel.put("titleList", titleList);
			map_excel.put("valueList", objList);
			excelList.add(map_excel);
			// 输出的excel文件工作表名
			new ExcelUtil().expCommonExcel(targetfile,excelList);
			this.getResponse().setContentType("application/octet-stream; charset=utf-8");
			this.getResponse().setHeader("Content-Disposition", "attachment; filename="+new String(file.getBytes("gbk"),"iso-8859-1") );
			//this.getResponse().setHeader( "Set-Cookie", "name=value; HttpOnly"); 
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			readCount = is.read(buff);
			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		}catch (Exception e) {
		log.error("导出采购概况报表信息列表错误！", e);
		throw new BaseException("导出采购概况报表信息列表错误！", e);
	  }
	}
	 /**
	  * 单个采购情况查看
	  * @return
	  * @throws BaseException
	  */
	public String viewPurchaseReportTab() throws BaseException{
		try {
			
		}catch (Exception e) {
			log.error("单个采购情况查看错误！", e);
			throw new BaseException("单个采购情况查看错误！", e);
		}
		return "purchase_select_index";
	}
	/**
	 * 查看采购基础数据信息
	 * @return
	 * @throws BaseException
	 */
	public String viewPurchaseResultInfo() throws BaseException{
		String initPage="purchaseResultInfo";
		try {
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
			
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
			this.getRequest().setAttribute("buyWayCn", BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
			this.getRequest().setAttribute("supplierTypeCn", BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
			
			List rcdList=this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId());
            this.getRequest().setAttribute("rcdList", rcdList);
			
            if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
            	tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(requiredCollect.getRcId());
            	tenderBidList.setBidOpenAdminCn(BaseDataInfosUtil.convertUserIdToChnName(tenderBidList.getBidOpenAdmin()));
            	this.getRequest().setAttribute("tenderBidList", tenderBidList);
            }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
            	askBidList=this.iAskBidListBiz.getAskBidListByRcId(requiredCollect.getRcId());
            	this.getRequest().setAttribute("askBidList", askBidList);
            }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
            	biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(requiredCollect.getRcId());
            	biddingBidList.setBidAdminCn(BaseDataInfosUtil.convertUserIdToChnName(biddingBidList.getBidAdmin())); 
            	biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
				biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
				biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
				biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
            	this.getRequest().setAttribute("biddingBidList", biddingBidList);
            }
            
			List list=this.iBidPriceBiz.getBidPriceAndAwardListByRcId(requiredCollect.getRcId());
			this.getRequest().setAttribute("list", list);
            
		} catch (Exception e) {
			log.error("采购基础数据查看错误！", e);
			throw new BaseException("采购基础数据查看错误！", e);
		}
		return initPage;
	}
	/**
	 * 查看采购基础数据信息   触屏版信息查看
	 * @return
	 * @throws BaseException
	 */
	public String viewPurchaseResultInfoMobile() throws BaseException{
		String initPage="purchaseResultInfoMobile";
		try {
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
			
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
			this.getRequest().setAttribute("buyWayCn", BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
			this.getRequest().setAttribute("supplierTypeCn", BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
			
			List rcdList=this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId());
            this.getRequest().setAttribute("rcdList", rcdList);
			
            if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
            	tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(requiredCollect.getRcId());
            	tenderBidList.setBidOpenAdminCn(BaseDataInfosUtil.convertUserIdToChnName(tenderBidList.getBidOpenAdmin()));
            	this.getRequest().setAttribute("tenderBidList", tenderBidList);
            }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
            	askBidList=this.iAskBidListBiz.getAskBidListByRcId(requiredCollect.getRcId());
            	this.getRequest().setAttribute("askBidList", askBidList);
            }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
            	biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(requiredCollect.getRcId());
            	biddingBidList.setBidAdminCn(BaseDataInfosUtil.convertUserIdToChnName(biddingBidList.getBidAdmin())); 
            	biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
				biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
				biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
				biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
            	this.getRequest().setAttribute("biddingBidList", biddingBidList);
            }
            
			List list=this.iBidPriceBiz.getBidPriceAndAwardListByRcId(requiredCollect.getRcId());
			this.getRequest().setAttribute("list", list);
            
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRcId(requiredCollect.getRcId());
			List purchaseRecordList=this.iPurchaseRecordLogBiz.getPurchaseRecordLogList(purchaseRecordLog);
			this.getRequest().setAttribute("purchaseRecordList", purchaseRecordList);
			
		} catch (Exception e) {
			log.error("采购基础数据（触屏版信息查看）查看错误！", e);
			throw new BaseException("采购基础数据（触屏版信息查看）查看错误！", e);
		}
		return initPage;
	}
	/**
	 * 采购整体过程查看
	 * @return
	 * @throws BaseException
	 */
	public String viewPurchaseNode() throws BaseException{
		try{
			if(StringUtil.isNotBlank(requiredCollect.getRcId())){
				requiredCollect = iRequiredCollectBiz.getRequiredCollectInfo(requiredCollect);
				requiredCollect.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
				BidProcessLog bidProcessLog=new BidProcessLog();
				bidProcessLog.setRcId(requiredCollect.getRcId());
				List<BidProcessLog> bidProcessLogList=this.iBidProcessLogBiz.getBidProcessLogList(bidProcessLog);
				Map<Long,Object> map=new HashMap<Long, Object>();
				for(BidProcessLog processLog:bidProcessLogList){
					if(processLog.getCompleteDate()!=null){
					  processLog.setDay(DateUtil.daysBetween(processLog.getReceiveDate(), processLog.getCompleteDate()));
					}
					map.put(processLog.getBidNode(),processLog);
				}
				if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				    bidProcessLogList=TenderProgressStatusList.getbidProcessLogList(map);
				} else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				    bidProcessLogList=AskProgressStatusList.getbidProcessLogList(map);
				}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				    bidProcessLogList=BiddingProgressStatusList.getbidProcessLogList(map);
				}
					
				this.getRequest().setAttribute("bidProcessLogList",bidProcessLogList);
				
				 boolean isBidAbnomal=false;
				 if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus()))
				 {
					isBidAbnomal=true;
				 }
				 this.getRequest().setAttribute("isBidAbnomal", isBidAbnomal);
				
			}
			} catch (Exception e) {
			log("进入采购整体过程查看错误！", e);
			throw new BaseException("进入采购整体过程查看错误！", e);
		}
		return "purchase_node";
	}
	/**
	 * 查询异常采购项目列表信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewAbnomalBidList() throws BaseException {
		
		try{
			this.getRequest().setAttribute("buyWay",TableStatusMap.purchaseWay);
			this.getRequest().setAttribute("bidAbnomal",TableStatusMap.bidAbnomalMap);
		} catch (Exception e) {
			log.error("查询异常采购项目列表信息错误！", e);
			throw new BaseException("查询异常采购项目列表信息错误！", e);
		}
		return "abnomalbidList" ;
	}
	
	/**
	 * 查询异常采购项目列表信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String fildAbnomalBidList() throws BaseException {
		
		try{
			String bidCode=this.getRequest().getParameter("bidCode");
			String buyRemark=this.getRequest().getParameter("buyRemark");
			String bidAbnomal=this.getRequest().getParameter("bidAbnomal");
			String buyWay=this.getRequest().getParameter("buyWay");
			String writeDateStart=this.getRequest().getParameter("writeDateStart");
			String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("bidCode", bidCode);
			map.put("buyRemark", buyRemark);
			map.put("buyWay", buyWay);
			map.put("bidAbnomal", bidAbnomal);
			map.put("writeDateStart", writeDateStart);
			map.put("writeDateEnd", writeDateEnd);
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			
			List<BidAbnomal> anabList=iBidAbnomalBiz.getAbnomalBidList(this.getRollPageDataTables(), map, sqlStr);
			for(BidAbnomal ba:anabList)
			{
				ba.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(ba.getBuyWay()));
				ba.setAbnomalTypeCn(BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(ba.getAbnomalType()));
				ba.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(ba.getWriter()));				
			}
			
			this.getPagejsonDataTables(anabList);
			
		} catch (Exception e) {
			log.error("查询异常采购项目列表信息错误！", e);
			throw new BaseException("查询异常采购项目列表信息错误！", e);
		}
		return null;
	}
	
	/**
	 * 异常采购项目Excel导出
	 * @return
	 * @throws BaseException 
	 */
	 public void  exportBidAbnomalExcel() throws BaseException{
		try{
			 List<String> titleList = new ArrayList<String>();
			 titleList.add("项目编号");
			 titleList.add("项目名称");
			 titleList.add("采购方式");
			 titleList.add("项目负责人");
			 titleList.add("异常类型");
			 titleList.add("操作人");
			 titleList.add("原因");
			 titleList.add("操作时间");
			 
			 String bidCode=this.getRequest().getParameter("bidCode");
				String buyRemark=this.getRequest().getParameter("buyRemark");
				String bidAbnomal=this.getRequest().getParameter("bidAbnomal");
				String buyWay=this.getRequest().getParameter("buyWay");
				String writeDateStart=this.getRequest().getParameter("writeDateStart");
				String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
				Map<String, Object> map=new HashMap<String, Object>();
				map.put("bidCode", bidCode);
				map.put("buyRemark", buyRemark);
				map.put("buyWay", buyWay);
				map.put("bidAbnomal", bidAbnomal);
				map.put("writeDateStart", writeDateStart);
				map.put("writeDateEnd", writeDateEnd);
				
				String sqlStr = "";
				if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
					sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
							"de");
				}
				
				List<BidAbnomal> anabList=iBidAbnomalBiz.getAbnomalBidList(this.getRollPageDataTables(), map, sqlStr);
				
				List<Object[]> objList = new ArrayList<Object[]>();
	     	
				for(BidAbnomal ba:anabList){
					ba.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(ba.getBuyWay()));
					ba.setAbnomalTypeCn(BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(ba.getAbnomalType()));
					ba.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(ba.getWriter()));
					
					 Object[]  obj=new Object[]{
							 ba.getBidCode(),
							 ba.getBuyRemark(),
							 ba.getBuyWayCn(),
							 ba.getWriterCn(),
							 ba.getWriteDete(),
							 ba.getAbnomalTypeCn(),
							 ba.getWriterCn(),
							 ba.getAbnomalReason(),
							 ba.getSubmitDate()
							 
					 };
					  objList.add(obj);
			 }
			// 输出的excel文件名
			String file = "异常采购项目基本信息.xls";
			String targetfile = this.getServletContext().getRealPath(file);
			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_excel = new HashMap<String, Object>();
			map_excel.put("worksheet", "异常采购项目基本信息");
			map_excel.put("titleList", titleList);
			map_excel.put("valueList", objList);
			excelList.add(map_excel);
			new ExcelUtil().expCommonExcel(targetfile, excelList);
			this.getResponse().setContentType("application/octet-stream; charset=utf-8");
			this.getResponse().setHeader("Content-Disposition", "attachment; filename="+new String(file.getBytes("gbk"),"iso-8859-1") );
			//this.getResponse().setHeader( "Set-Cookie", "name=value; HttpOnly"); 
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			int i = 0;
			readCount = is.read(buff);
			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		}catch (Exception e) {
			log.error("导出异常采购项目Excel列表错误！", e);
			throw new BaseException("导出异常采购项目Excel表错误！", e);
	    }
     } 

	/**
	 * 采购整体跟踪统计
	 * @return
	 * @throws BaseException
	 */
	public String viewPurchaseTrack() throws BaseException {
		try {
			Map purchaseWayMap = TableStatusMap.purchaseWay;
			this.getRequest().setAttribute("purchaseWayMap", purchaseWayMap);
		} catch (Exception e) {
			log.error("采购整体跟踪统计报表错误！", e);
			throw new BaseException("采购整体跟踪统计报表错误！", e);
		}
		return "purchaseTrack";
	}
	/**
	 * 查询采购整体跟踪统计信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String fildPurchaseTrackList() throws BaseException {
		
		try{
			String bidCode=this.getRequest().getParameter("bidCode");
			String buyRemark=this.getRequest().getParameter("buyRemark");
			String buyWay=this.getRequest().getParameter("buyWay");
			String writeDateStart=this.getRequest().getParameter("writeDateStart");
			String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
			Map<String, Object> map=new HashMap<String, Object>();
			map.put("bidCode", bidCode);
			map.put("buyRemark", buyRemark);
			map.put("buyWay", buyWay);
			map.put("writeDateStart", writeDateStart);
			map.put("writeDateEnd", writeDateEnd);
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
						"t");
			}
			
			List<PurchaseTrack> list=iPurchaseAmountBiz.getPurchaseTrackList(this.getRollPageDataTables(), map, sqlStr);
			for(PurchaseTrack pt:list)
			{
				pt.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(pt.getWriter()));
				pt.setPurchaseDeptName(BaseDataInfosUtil.convertDeptIdToName(pt.getPurchaseDeptId()));			
			}
			
			this.getPagejsonDataTables(list);
			
		} catch (Exception e) {
			log.error("查询采购整体跟踪统计信息错误！", e);
			throw new BaseException("查询采购整体跟踪统计信息错误！", e);
		}
		return null;
	}

	/**
	 * 采购整体跟踪统计Excel导出
	 * @return
	 * @throws BaseException 
	 */
	 public void  exportPurchaseTrackExcel() throws BaseException{
		try{
			 List<String> titleList = new ArrayList<String>();
			 titleList.add("采购组织");
			 titleList.add("项目编号");
			 titleList.add("项目名称");
			 titleList.add("采购方式");
			 titleList.add("供方选择方式");
			 titleList.add("负责人");
			 titleList.add("立项时间");
			 titleList.add("公告发布时间");
			 titleList.add("回标截止时间");
			 titleList.add("报名数/响应数");
			 titleList.add("开标时间");
			 titleList.add("授标时间");
			 titleList.add("公告变更次数");
			 titleList.add("预算金额");
			 titleList.add("成交金额");
			 titleList.add("节资率");
			 titleList.add("项目节点");
			 
			 String bidCode=this.getRequest().getParameter("bidCode");
				String buyRemark=this.getRequest().getParameter("buyRemark");
				String buyWay=this.getRequest().getParameter("buyWay");
				String writeDateStart=this.getRequest().getParameter("writeDateStart");
				String writeDateEnd=this.getRequest().getParameter("writeDateEnd");
				Map<String, Object> map=new HashMap<String, Object>();
				map.put("bidCode", bidCode);
				map.put("buyRemark", buyRemark);
				map.put("buyWay", buyWay);
				map.put("writeDateStart", writeDateStart);
				map.put("writeDateEnd", writeDateEnd);
				
				String sqlStr = "";
				if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
					sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
							"t");
				}
				
				List<PurchaseTrack> list=iPurchaseAmountBiz.getPurchaseTrackListAll(map, sqlStr);
				
				List<Object[]> objList = new ArrayList<Object[]>();
	     	    String jzl="";
				for(PurchaseTrack pt:list)
				{
					if(StringUtil.isNotBlank(pt.getTotalBudget())&&StringUtil.isNotBlank(pt.getBidWinningPrice())){
						jzl=((pt.getBidWinningPrice()-pt.getTotalBudget())/pt.getTotalBudget()*100)+"%";
					}else{
						jzl="";
					}
					pt.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(pt.getWriter()));
					pt.setPurchaseDeptName(BaseDataInfosUtil.convertDeptIdToName(pt.getPurchaseDeptId()));	
					
					 Object[]  obj=new Object[]{
							 pt.getPurchaseDeptName(),
							 pt.getBidCode(),
							 pt.getBuyRemark(),
							 BaseDataInfosUtil.convertBuyWayToBuyType(pt.getBuyWay()),
							 BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(pt.getSupplierType()),
							 pt.getWriterCn(),
							 pt.getWriteDate(),
							 pt.getNoticePublishDate(),
							 pt.getBidReturnDate(),
							 pt.getInviteSupplierNumber()+"/"+pt.getPriceSupplierNumber(),
							 pt.getBidOpenDate(),
							 pt.getBidAwardDate(),
							 pt.getChangeNum(),
							 pt.getTotalBudget(),
							 pt.getBidWinningPrice(),
							 jzl,
							 pt.getServiceStatusCn()
							 
					 };
					  objList.add(obj);
			 }
			// 输出的excel文件名
			String file = "采购整体跟踪统计.xls";
			String targetfile = this.getServletContext().getRealPath(file);
			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_excel = new HashMap<String, Object>();
			map_excel.put("worksheet", "采购整体跟踪统计");
			map_excel.put("titleList", titleList);
			map_excel.put("valueList", objList);
			excelList.add(map_excel);
			new ExcelUtil().expCommonExcel(targetfile, excelList);
			this.getResponse().setContentType("application/octet-stream; charset=utf-8");
			this.getResponse().setHeader("Content-Disposition", "attachment; filename="+new String(file.getBytes("gbk"),"iso-8859-1") );
			//this.getResponse().setHeader( "Set-Cookie", "name=value; HttpOnly"); 
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			int i = 0;
			readCount = is.read(buff);
			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		}catch (Exception e) {
			log.error("导出采购整体跟踪统计Excel列表错误！", e);
			throw new BaseException("导出采购整体跟踪统计Excel表错误！", e);
	    }
     } 
	 
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}
	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}
	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}
	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}
	public IBidAwardDetailBiz getiBidAwardDetailBiz() {
		return iBidAwardDetailBiz;
	}
	public void setiBidAwardDetailBiz(IBidAwardDetailBiz iBidAwardDetailBiz) {
		this.iBidAwardDetailBiz = iBidAwardDetailBiz;
	}
	public IBidAbnomalBiz getiBidAbnomalBiz() {
		return iBidAbnomalBiz;
	}
	public void setiBidAbnomalBiz(IBidAbnomalBiz iBidAbnomalBiz) {
		this.iBidAbnomalBiz = iBidAbnomalBiz;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}
	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IPurchaseAmountBiz getiPurchaseAmountBiz() {
		return iPurchaseAmountBiz;
	}
	public void setiPurchaseAmountBiz(IPurchaseAmountBiz iPurchaseAmountBiz) {
		this.iPurchaseAmountBiz = iPurchaseAmountBiz;
	}
	public IAskBidListBiz getiAskBidListBiz() {
		return iAskBidListBiz;
	}
	public void setiAskBidListBiz(IAskBidListBiz iAskBidListBiz) {
		this.iAskBidListBiz = iAskBidListBiz;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}
	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
}
