package com.ced.sip.analysis.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;

public interface IPurchaseAmountBiz {
	/**
	 * 采购金额成交统计查询  按月查询
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException
	 */
    public List getPurchaseAmountBuyMonth(Map<String, Object> map,String sqlStr) throws BaseException;
    /**
	 * 采购金额成交统计查询  按季度查询
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException
	 */
    public List getPurchaseAmountBuySecond(Map<String, Object> map,String sqlStr) throws BaseException;
    /**
	 * 采购金额成交统计查询  按年度查询
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException
	 */
    public List getPurchaseAmountBuyYear(Map<String, Object> map,String sqlStr) throws BaseException;
    
    /**
	 * 采购整体跟踪统计
	 * @param rollPage 分页对象
	 * @param map 查询参数对象
	 * @author lgl 2017-10-21
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getPurchaseTrackList(RollPage rollPage,
			Map<String,Object> map,String sqlStr) throws BaseException;
	

	/**
	 * 采购整体跟踪统计
	 * @param rollPage 分页对象
	 * @param map 查询参数对象
	 * @author lgl 2017-10-21
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getPurchaseTrackListAll(Map<String,Object> map,String sqlStr) throws BaseException;
    /**
     * 供应商采购成交统计
     * @param map
     * @param sqlStr
     * @return
     * @throws BaseException
     */
	public abstract List getSupplierWinningList(Map<String,Object> map,String sqlStr) throws BaseException;
	/**
     * 采购品成交统计排行
     * @param rollPage
     * @param map
     * @param sqlStr
     * @return
     * @throws BaseException
     */
	public abstract List getMaterialReportList(RollPage rollPage,Map<String,Object> map,String sqlStr) throws BaseException;
	/**
     * 采购品成交统计排行
     * @param rollPage
     * @param map
     * @param sqlStr
     * @return
     * @throws BaseException
     */
	public abstract List getMaterialReportListAll(Map<String,Object> map,String sqlStr) throws BaseException;

}
