package com.ced.sip.contract.action.epp;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractClassBiz;
import com.ced.sip.contract.entity.ContractClass;
/** 
 * 类名称：ContractClassAction
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractClassAction extends BaseAction {

	// 合同字典 
	private IContractClassBiz iContractClassBiz;
	
	// 合同字典
	private ContractClass contractClass;
	
	private List categoryList;
	
	/**
	 * 查看合同字典信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewContractClass() throws BaseException {
		
		try{
		   categoryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1709);
		   this.getRequest().setAttribute("categoryList", categoryList);
		} catch (Exception e) {
			log.error("查看合同字典信息列表错误！", e);
			throw new BaseException("查看合同字典信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看合同字典信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findContractClass() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			String category=this.getRequest().getParameter("category");
			contractClass=new ContractClass();
			contractClass.setCategory(category);
			contractClass.setComId(comId);
			List list=this.iContractClassBiz.getContractClassList(this.getRollPageDataTables(), contractClass);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看合同字典信息列表错误！", e);
			throw new BaseException("查看合同字典信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存合同字典信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveContractClassInit() throws BaseException {
		try{
			categoryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1709);
			this.getRequest().setAttribute("categoryList", categoryList);
		} catch (Exception e) {
			log("保存合同字典信息初始化错误！", e);
			throw new BaseException("保存合同字典信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存合同字典信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveContractClass() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			String userName=UserRightInfoUtil.getUserName(this.getRequest());
			contractClass.setComId(comId);
			contractClass.setWriter(userName);
			contractClass.setWriteDate(DateUtil.getCurrentDateTime());
			this.iContractClassBiz.saveContractClass(contractClass);			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增合同字典");
		} catch (Exception e) {
			log("保存合同字典信息错误！", e);
			throw new BaseException("保存合同字典信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改合同字典信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateContractClassInit() throws BaseException {
		
		try{
			categoryList=BaseDataInfosUtil.getDictInfoToList(DictStatus.COMMON_DICT_TYPE_1709);
			this.getRequest().setAttribute("categoryList", categoryList);
			contractClass=this.iContractClassBiz.getContractClass(contractClass.getCcId() );
		} catch (Exception e) {
			log("修改合同字典信息初始化错误！", e);
			throw new BaseException("修改合同字典信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改合同字典信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateContractClass() throws BaseException {
		
		try{
			contractClass.setWriteDate(DateUtil.getCurrentDateTime());
			this.iContractClassBiz.updateContractClass(contractClass);			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改合同字典");
		} catch (Exception e) {
			log("修改合同字典信息错误！", e);
			throw new BaseException("修改合同字典信息错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 删除合同字典信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteContractClass() throws BaseException {
		try{
		    ContractClass contractClass=new ContractClass();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				contractClass=this.iContractClassBiz.getContractClass(new Long(str[i]));
	 				this.iContractClassBiz.deleteContractClass(contractClass);
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除合同字典");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除合同字典信息错误！", e);
			throw new BaseException("删除合同字典信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看合同字典明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewContractClassDetail() throws BaseException {
		
		try{
			contractClass=this.iContractClassBiz.getContractClass(contractClass.getCcId() );
		} catch (Exception e) {
			log("查看合同字典明细信息错误！", e);
			throw new BaseException("查看合同字典明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public IContractClassBiz getiContractClassBiz() {
		return iContractClassBiz;
	}

	public void setiContractClassBiz(IContractClassBiz iContractClassBiz) {
		this.iContractClassBiz = iContractClassBiz;
	}

	public ContractClass getContractClass() {
		return contractClass;
	}

	public void setContractClass(ContractClass contractClass) {
		this.contractClass = contractClass;
	}
	
}
