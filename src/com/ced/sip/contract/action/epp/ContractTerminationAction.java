package com.ced.sip.contract.action.epp;

import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.biz.IContractTerminationBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.contract.entity.ContractTermination;
import com.ced.sip.contract.util.ContractStatus;
/** 
 * 类名称：ContractTerminationAction
 * 创建人：luguanglei 
 * 创建时间：2017-08-02
 */
public class ContractTerminationAction extends BaseAction {

	// 采购异常表 
	private IContractTerminationBiz iContractTerminationBiz;
	
	private IContractInfoBiz iContractInfoBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	
	// 采购异常表
	private ContractTermination contractTermination;
	
	private ContractInfo contractInfo;
	
	
	
	/**
	 * 合同终止初始化页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveContractTerminationInit() throws BaseException {
		
		try{
			contractInfo=iContractInfoBiz.getContractInfo(contractInfo.getCiId());
			
			contractTermination=new ContractTermination();
			contractTermination.setCiId(contractInfo.getCiId());
			List<ContractTermination> ctList=iContractTerminationBiz.getContractTerminationList(contractTermination);
			if(ctList.size()>0)
			{
				contractTermination=ctList.get(0);
				//获取附件
				Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( contractTermination.getCtId(), AttachmentStatus.ATTACHMENT_CODE_1001 ) );
				contractTermination.setUuIdData((String)map.get("uuIdData"));
				contractTermination.setFileNameData((String)map.get("fileNameData"));
				contractTermination.setFileTypeData((String)map.get("fileTypeData"));
				contractTermination.setAttIdData((String)map.get("attIdData"));
				contractTermination.setAttIds((String)map.get("attIds"));
			}else
			{
				contractTermination.setWriterId(UserRightInfoUtil.getUserId(this.getRequest()));
				contractTermination.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
				contractTermination.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(UserRightInfoUtil.getUserName(this.getRequest())));
				contractTermination.setWriteDete(DateUtil.getCurrentDateTime());
			}
			this.getRequest().setAttribute("abnomalList",TableStatusMap.bidAbnomalMap);
			
		} catch (Exception e) {
			log.error("查看合同业务终止初始化页面错误！", e);
			throw new BaseException("查看合同业务终止初始化页面错误！", e);
		}
		
		return "saveContractTerm";
		
	}
	
	/**
	 * 保存合同业务终止信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveContractTermination() throws BaseException {
		
		try{
			if(StringUtil.isNotBlank(contractTermination.getCtId()))
			{
				// 删除附件
				iAttachmentBiz.deleteAttachments( parseAttachIds( contractTermination.getAttIds() ) ) ;
				//保存附件			
				iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( contractTermination, contractTermination.getCtId(), AttachmentStatus.ATTACHMENT_CODE_1001, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
				
				String ciId=this.getRequest().getParameter("ciId");
				contractInfo=iContractInfoBiz.getContractInfo(new Long(ciId));
				contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_08);
				contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_08_Text);
				iContractInfoBiz.updateContractInfo(contractInfo);
				contractTermination.setCiId(contractInfo.getCiId());
				iContractTerminationBiz.updateContractTermination(contractTermination);
			}else
			{
				String ciId=this.getRequest().getParameter("ciId");
				contractInfo=iContractInfoBiz.getContractInfo(new Long(ciId));
				contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_08);
				contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_08_Text);
				iContractInfoBiz.updateContractInfo(contractInfo);
				
				contractTermination.setSubmitDate(DateUtil.getCurrentDateTime());
				contractTermination.setCiId(contractInfo.getCiId());
				iContractTerminationBiz.saveContractTermination(contractTermination);
				
				//保存附件			
				iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( contractTermination, contractTermination.getCtId(), AttachmentStatus.ATTACHMENT_CODE_1001, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
				
				
				this.getRequest().setAttribute("message", contractInfo.getContractCode()+"合同终止成功");
				this.getRequest().setAttribute("operModule", "保存合同业务终止信息");
			}
			
		} catch (Exception e) {
			log.error("保存合同业务终止信息错误！", e);
			throw new BaseException("保存合同业务终止信息错误！", e);
		}
		return "success";
		
	}
	
	/**
	 * 查看合同业务终止信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewContractTerminationDetail() throws BaseException {
		
		try{
			contractTermination=new ContractTermination();
			contractTermination.setCiId(contractInfo.getCiId());
			List<ContractTermination> ctList=iContractTerminationBiz.getContractTerminationList(contractTermination);
			
			contractTermination=ctList.get(0);
			contractTermination.setWriterCn(BaseDataInfosUtil.convertUserIdToChnName(contractTermination.getWriterId()));
			String attachment = iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment(contractTermination.getCtId(), AttachmentStatus.ATTACHMENT_CODE_1001 ) ) , "0", this.getRequest() );
		
			this.getRequest().setAttribute("attachment", attachment);
			
		} catch (Exception e) {
			log.error("查看合同业务终止信息错误！", e);
			throw new BaseException("查看合同业务终止信息错误！", e);
		}
		return "contractTermDetail";
		
	}

	public IContractTerminationBiz getiContractTerminationBiz() {
		return iContractTerminationBiz;
	}

	public void setiContractTerminationBiz(
			IContractTerminationBiz iContractTerminationBiz) {
		this.iContractTerminationBiz = iContractTerminationBiz;
	}

	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public ContractTermination getContractTermination() {
		return contractTermination;
	}

	public void setContractTermination(ContractTermination contractTermination) {
		this.contractTermination = contractTermination;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}
}
