package com.ced.sip.contract.action.epp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.GlobalSettingBase;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractTemplateBiz;
import com.ced.sip.contract.entity.ContractTemplate;
/** 
 * 类名称：ContractTemplateAction
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractTemplateAction extends BaseAction {

    //上传文件存放路径   
    private final static String UPLOADDIR = "upload/contract/template/";
    
	// 合同模板 
	private IContractTemplateBiz iContractTemplateBiz;
	
	// 合同模板
	private ContractTemplate contractTemplate;
	
	private File file;
	
	private String fileContentType;   
	
	private String fileFileName; 
	
	/**
	 * 查看合同模板信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewContractTemplate() throws BaseException {
		
		try{
		
		} catch (Exception e) {
			log.error("查看合同模板信息列表错误！", e);
			throw new BaseException("查看合同模板信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看合同模板信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findContractTemplate() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			contractTemplate=new ContractTemplate();
			contractTemplate.setComId(comId);
			List list=this.iContractTemplateBiz.getContractTemplateList(this.getRollPageDataTables(), contractTemplate);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看合同模板信息列表错误！", e);
			throw new BaseException("查看合同模板信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存合同模板信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveContractTemplateInit() throws BaseException {
		try{
		
		} catch (Exception e) {
			log("保存合同模板信息初始化错误！", e);
			throw new BaseException("保存合同模板信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存合同模板信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveContractTemplate() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String templateUrl="",fileType="";  
            if(file!=null){
				InputStream in = new FileInputStream(file);   
	            String dir = GlobalSettingBase.getFilePath()+UPLOADDIR; 
	            
	            int index = fileFileName.lastIndexOf(".");
	    		if (index > -1) {
	    			fileType =fileFileName.substring(index);
	    		}
	    		fileType=fileType.toLowerCase().substring(1);
				String cjrq = new SimpleDateFormat("yyyyMMddHHmmss")
						.format(new Date());
				String fjTpName = cjrq + "." + fileType;// 文件的名字
	            File uploadFile = new File(dir, fjTpName);   
	            templateUrl=UPLOADDIR+fjTpName;
	            OutputStream out = new FileOutputStream(uploadFile);   
	            byte[] buffer = new byte[1024 * 1024];   
	            int length;   
	            while ((length = in.read(buffer)) > 0) {   
	                out.write(buffer, 0, length);   
	            }   
	            in.close();   
	            out.close();   
	            contractTemplate.setTemplateUrl(templateUrl);
            }
            contractTemplate.setComId(comId);
            contractTemplate.setWriteDate(DateUtil.getCurrentDateTime());
            contractTemplate.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
			this.iContractTemplateBiz.saveContractTemplate(contractTemplate);			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增合同模板");
		} catch (Exception e) {
			log("保存合同模板信息错误！", e);
			throw new BaseException("保存合同模板信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改合同模板信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateContractTemplateInit() throws BaseException {
		
		try{
			contractTemplate=this.iContractTemplateBiz.getContractTemplate(contractTemplate.getCtId() );
		} catch (Exception e) {
			log("修改合同模板信息初始化错误！", e);
			throw new BaseException("修改合同模板信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改合同模板信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateContractTemplate() throws BaseException {
		
		try{
			String templateUrl="",fileType="";  
            if(file!=null){
				InputStream in = new FileInputStream(file);   
	            String dir = GlobalSettingBase.getFilePath()+UPLOADDIR; 
	            
	            int index = fileFileName.lastIndexOf(".");
	    		if (index > -1) {
	    			fileType =fileFileName.substring(index);
	    		}
	    		fileType=fileType.toLowerCase().substring(1);
				String cjrq = new SimpleDateFormat("yyyyMMddHHmmss")
						.format(new Date());
				String fjTpName = cjrq + "." + fileType;// 文件的名字
	            File uploadFile = new File(dir, fjTpName);   
	            templateUrl=UPLOADDIR+fjTpName;
	            OutputStream out = new FileOutputStream(uploadFile);   
	            byte[] buffer = new byte[1024 * 1024];   
	            int length;   
	            while ((length = in.read(buffer)) > 0) {   
	                out.write(buffer, 0, length);   
	            }   
	            in.close();   
	            out.close();   
	            contractTemplate.setTemplateUrl(templateUrl);
            }
			this.iContractTemplateBiz.updateContractTemplate(contractTemplate);			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改合同模板");
		} catch (Exception e) {
			log("修改合同模板信息错误！", e);
			throw new BaseException("修改合同模板信息错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 删除合同模板信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteContractTemplate() throws BaseException {
		try{
		    ContractTemplate contractTemplate=new ContractTemplate();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				contractTemplate=this.iContractTemplateBiz.getContractTemplate(new Long(str[i]));
	 				this.iContractTemplateBiz.deleteContractTemplate(contractTemplate);
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除合同模板");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除合同模板信息错误！", e);
			throw new BaseException("删除合同模板信息错误！", e);
		}
		return null;
		
	}

	public IContractTemplateBiz getiContractTemplateBiz() {
		return iContractTemplateBiz;
	}

	public void setiContractTemplateBiz(IContractTemplateBiz iContractTemplateBiz) {
		this.iContractTemplateBiz = iContractTemplateBiz;
	}

	public ContractTemplate getContractTemplate() {
		return contractTemplate;
	}

	public void setContractTemplate(ContractTemplate contractTemplate) {
		this.contractTemplate = contractTemplate;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	
}
