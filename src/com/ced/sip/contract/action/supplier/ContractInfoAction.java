package com.ced.sip.contract.action.supplier;

import java.util.Date;
import java.util.List;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractConfirmBiz;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.biz.IContractMaterialBiz;
import com.ced.sip.contract.entity.ContractConfirm;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.contract.entity.ContractMaterial;
import com.ced.sip.contract.util.ContractStatus;
import com.ced.sip.bill.biz.ISendBillBiz;
import com.ced.sip.bill.biz.ISendBillDetailBiz;
import com.ced.sip.bill.entity.SendBill;
import com.ced.sip.bill.entity.SendBillDetail;
/** 
 * 类名称：ContractInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public class ContractInfoAction extends BaseAction {

	// 合同信息 
	private IContractInfoBiz iContractInfoBiz;
	//合同明细信息
	private IContractMaterialBiz iContractMaterialBiz;
	//合同确认信息
	private IContractConfirmBiz iContractConfirmBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	//发货信息
	private ISendBillBiz iSendBillBiz;
	//发货物资明细
	private ISendBillDetailBiz iSendBillDetailBiz;
	
	// 合同信息
	private ContractInfo contractInfo;
	
	private ContractMaterial contractMaterial;	

	private ContractConfirm contractConfirm;

	private List<ContractMaterial> cmList;
	
	private SendBill sendBill;
	
	private SendBillDetail sendBillDetail;
	
	private List<SendBillDetail> sbdList;
	
	/**
	 * 查看合同确认信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewContractInfoApplication() throws BaseException {
		String initPage="contract_application";
		try{
			String from=this.getRequest().getParameter("from");
			String contractName=this.getRequest().getParameter("contractName");
			 //触屏版使用
	        if(StringUtil.isNotBlank(from)) initPage+="Mobile";
	        this.getRequest().setAttribute("contractName", contractName);
		} catch (Exception e) {
			log.error("查看合同确认信息信息列表错误！", e);
			throw new BaseException("查看合同确认信息信息列表错误！", e);
		}	
			
		return initPage;
				
	}

	/**
	 * 查看我的合同信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewContractInfoMy() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看我的合同信息列表错误！", e);
			throw new BaseException("查看我的合同信息列表错误！", e);
		}	
			
		return "contract_my" ;
				
	}
	/**
	 * 合同发货管理
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewSendContractInfo() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("合同发货管理列表错误！", e);
			throw new BaseException("合同发货管理列表错误！", e);
		}	
			
		return "contract_send" ;
				
	}
	/**
	 * 查看合同确认信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findContractInfoApplication() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			int sign=Integer.parseInt(this.getRequest().getParameter("sgin"));
			String contractPersonNameA=this.getRequest().getParameter("contractPersonNameA");
			String contractCode=this.getRequest().getParameter("contractCode");
			String contractName=this.getRequest().getParameter("contractName");
			contractInfo=new ContractInfo();
			contractInfo.setContractPersonNameA(contractPersonNameA);
			contractInfo.setContractCode(contractCode);
			contractInfo.setContractName(contractName);
			contractInfo.setSupplierId(supplierId);
			List list=this.iContractInfoBiz.getContractInfoListBySupplier(this.getRollPageDataTables(), contractInfo,sign);
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看合同确认信息列表错误！", e);
			throw new BaseException("查看合同确认信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 查看合同信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewContractInfoDetail() throws BaseException {
		
		try{
			contractInfo=this.iContractInfoBiz.getContractInfo(contractInfo.getCiId() );
			// 获取合同审批原件
			String appFile=iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									contractInfo.getCiId(),
									AttachmentStatus.ATTACHMENT_CODE_1002)), "0",
							this.getRequest());
			this.getRequest().setAttribute("appFile", appFile);
			
			contractMaterial=new ContractMaterial();
			contractMaterial.setCiId(contractInfo.getCiId());
			cmList=this.iContractMaterialBiz.getContractMaterialList(contractMaterial);
			
			contractConfirm=new ContractConfirm();
			contractConfirm.setCiId(contractInfo.getCiId());
			List confirmList=this.iContractConfirmBiz.getContractConfirmList(contractConfirm);
			this.getRequest().setAttribute("confirmList", confirmList);
		} catch (Exception e) {
			log("查看合同信息明细信息错误！", e);
			throw new BaseException("查看合同信息明细信息错误！", e);
		}
		return "contract_detail";
		
	}
	/**
	 * 供应商进行合同确认初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveConfirmContractInfoInit() throws BaseException {		
		String initPage="contract_confirm";
		try{
			String from=this.getRequest().getParameter("from");
            contractInfo=this.iContractInfoBiz.getContractInfo(contractInfo.getCiId() );
            // 获取合同审批原件
			String appFile=iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									contractInfo.getCiId(),
									AttachmentStatus.ATTACHMENT_CODE_1002)), "0",
							this.getRequest());
			this.getRequest().setAttribute("appFile", appFile);
			
			contractMaterial=new ContractMaterial();
			contractMaterial.setCiId(contractInfo.getCiId());
			cmList=this.iContractMaterialBiz.getContractMaterialList(contractMaterial);
			
			contractConfirm=new ContractConfirm();
			contractConfirm.setCiId(contractInfo.getCiId());
			List confirmList=this.iContractConfirmBiz.getContractConfirmList(contractConfirm);
			this.getRequest().setAttribute("confirmList", confirmList);
			 //触屏版使用
	        if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("发送合同至供应商确认初始化页面错误！", e);
			throw new BaseException("发送合同至供应商确认初始化页面错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 供应商进行合同确认操作
	 * @return
	 * @throws BaseException 
	 */
	public String saveConfirmContractInfo() throws BaseException {		
		String initPage="success";
		try{
		   String from=this.getRequest().getParameter("from");
			
           Long ciId=Long.parseLong(this.getRequest().getParameter("ciId"));
           String supplierName=UserRightInfoUtil.getSupplierName(this.getRequest());
           String status=this.getRequest().getParameter("status");
           String confirmRemark=this.getRequest().getParameter("confirmRemark");
           Long supplierId=Long.parseLong(this.getRequest().getParameter("supplierId"));
           contractInfo=this.iContractInfoBiz.getContractInfo(ciId);
           
           contractConfirm=new ContractConfirm();
           contractConfirm.setCiId(ciId);
           contractConfirm.setConfirmDate(DateUtil.getCurrentDateTime());
           contractConfirm.setConfirmRemark(confirmRemark);
           contractConfirm.setSupplierId(supplierId);
           contractConfirm.setSupplierName(supplierName);
           if(status.equals("1")){
        	   contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_05);
        	   contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_05_Text);
               contractConfirm.setStatus("无异议,确认");
           }else{
        	   contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_06);
        	   contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_06_Text);
               contractConfirm.setStatus("有异议,驳回");
           }
           this.iContractConfirmBiz.saveContractConfirm(contractConfirm);
    	   this.iContractInfoBiz.updateContractInfo(contractInfo);
    	   
    	   String content=""+supplierName+"对合同已经确认，合同名称为"+contractInfo.getContractName()+"，合同金额为"+contractInfo.getContractMoney()+"，确认结果为"+contractConfirm.getStatus();
		   this.saveSmsMessageToUsers(contractInfo.getContractMobileA(), content, supplierName);
		   
           this.getRequest().setAttribute("message", "确认成功");
		   this.getRequest().setAttribute("operModule", "供应商确认合同信息");
		   //触屏版使用
	       if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("发送合同至供应商确认操作错误！", e);
			throw new BaseException("发送合同至供应商确认操作错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 供应商发货初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveSendContractInfoInit() throws BaseException {		
		try{
            contractInfo=this.iContractInfoBiz.getContractInfo(contractInfo.getCiId() );
			
			contractMaterial=new ContractMaterial();
			contractMaterial.setCiId(contractInfo.getCiId());
			cmList=this.iContractMaterialBiz.getContractMaterialList(contractMaterial);
			
			this.getRequest().setAttribute("today", DateUtil.getDefaultDateFormat(new Date()));
		} catch (Exception e) {
			log("供应商发货初始化页面错误！", e);
			throw new BaseException("供应商发货初始化页面错误！", e);
		}
		return "addContractSendInit";		
	}
	/**
	 * 供应商发货信息保存
	 * @return
	 * @throws BaseException 
	 */
	public String saveSendContractInfo() throws BaseException {		
		try{
			String writer=UserRightInfoUtil.getSupplierLoginName(this.getRequest());			
            //编制人
			sendBill.setWriter(writer);	 	   
	 	    //编制日期 
			sendBill.setWriteDate(DateUtil.getCurrentDateTime());	
			sendBill.setStatus(TableStatus.COMMON_0);
			sendBill.setIsInput(TableStatus.COMMON_INT_1);
			this.iSendBillBiz.saveSendBill(sendBill);
			
			//保存附件		
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(sendBill,
					sendBill.getSbId(), AttachmentStatus.ATTACHMENT_CODE_1201,
					writer));
			
			Double realSendAmount=0.00;
			// 保存合同物资明细数据
			if (sbdList != null) {
				for (int i = 0; i < sbdList.size(); i++) {
					sendBillDetail = sbdList.get(i);
					if (sendBillDetail != null
							&& StringUtil.isNotBlank(sendBillDetail.getMaterialCode().trim())) {
						sendBillDetail.setSbId(sendBill.getSbId());
						iSendBillDetailBiz.saveSendBillDetail(sendBillDetail);					
						//更新订单明细表中的可发货数量，实际发货数量
						iContractMaterialBiz.updateSendAmount(sendBillDetail.getSendAmount(), sendBillDetail.getOmId());
						realSendAmount+=sendBillDetail.getSendAmount();
					}
				}
			}
			sendBill.setSendAmount(realSendAmount);
			this.iSendBillBiz.updateSendBill(sendBill);
						
			contractInfo=this.iContractInfoBiz.getContractInfo(sendBill.getOiId());
			contractInfo.setRealSendAmount(contractInfo.getRealSendAmount()+realSendAmount);
			contractInfo.setRunStatus(ContractStatus.CONTRACT_RUN_STATUS_09);
			contractInfo.setStatusCn(ContractStatus.CONTRACT_RUN_STATUS_09_Text);
			this.iContractInfoBiz.updateContractInfo(contractInfo);
			
			this.getRequest().setAttribute("message", "发货成功");
			this.getRequest().setAttribute("operModule", "供应商发货成功");
		} catch (Exception e) {
			log("供应商发货初始化页面错误！", e);
			throw new BaseException("供应商发货初始化页面错误！", e);
		}
		return "sendBillView";		
	}
	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}

	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

	public IContractMaterialBiz getiContractMaterialBiz() {
		return iContractMaterialBiz;
	}

	public void setiContractMaterialBiz(IContractMaterialBiz iContractMaterialBiz) {
		this.iContractMaterialBiz = iContractMaterialBiz;
	}
	public List<ContractMaterial> getCmList() {
		return cmList;
	}

	public void setCmList(List<ContractMaterial> cmList) {
		this.cmList = cmList;
	}

	public IContractConfirmBiz getiContractConfirmBiz() {
		return iContractConfirmBiz;
	}

	public void setiContractConfirmBiz(IContractConfirmBiz iContractConfirmBiz) {
		this.iContractConfirmBiz = iContractConfirmBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public SendBill getSendBill() {
		return sendBill;
	}

	public void setSendBill(SendBill sendBill) {
		this.sendBill = sendBill;
	}

	public List<SendBillDetail> getSbdList() {
		return sbdList;
	}

	public void setSbdList(List<SendBillDetail> sbdList) {
		this.sbdList = sbdList;
	}

	public ISendBillBiz getiSendBillBiz() {
		return iSendBillBiz;
	}

	public void setiSendBillBiz(ISendBillBiz iSendBillBiz) {
		this.iSendBillBiz = iSendBillBiz;
	}

	public ISendBillDetailBiz getiSendBillDetailBiz() {
		return iSendBillDetailBiz;
	}

	public void setiSendBillDetailBiz(ISendBillDetailBiz iSendBillDetailBiz) {
		this.iSendBillDetailBiz = iSendBillDetailBiz;
	}
	
}
