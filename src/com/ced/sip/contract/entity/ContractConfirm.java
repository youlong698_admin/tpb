package com.ced.sip.contract.entity;

import java.util.Date;

/** 
 * 类名称：ContractConfirm
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public class ContractConfirm implements java.io.Serializable {

	// 属性信息
	private Long ccId;     //主键
	private Long ciId;     //合同主键
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String confirmRemark;	 //确认说明
	private String status;	 //状态
	private Date confirmDate;    //确认日期
	
	
	public ContractConfirm() {
		super();
	}
	
	public Long getCcId(){
	   return  ccId;
	} 
	public void setCcId(Long ccId) {
	   this.ccId = ccId;
    }     
	public Long getCiId(){
	   return  ciId;
	} 
	public void setCiId(Long ciId) {
	   this.ciId = ciId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getConfirmRemark(){
	   return  confirmRemark;
	} 
	public void setConfirmRemark(String confirmRemark) {
	   this.confirmRemark = confirmRemark;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public Date getConfirmDate(){
	   return  confirmDate;
	} 
	public void setConfirmDate(Date confirmDate) {
	   this.confirmDate = confirmDate;
    }	    
}