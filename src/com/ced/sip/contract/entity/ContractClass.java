package com.ced.sip.contract.entity;

import java.util.Date;

/** 
 * 类名称：ContractClass
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractClass implements java.io.Serializable {

	// 属性信息
	private Long ccId;     //主键
	private String category;	 //类别
	private String content;	 //内容
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Long comId;     //
	
	
	public ContractClass() {
		super();
	}
	
	public Long getCcId(){
	   return  ccId;
	} 
	public void setCcId(Long ccId) {
	   this.ccId = ccId;
    }     
	public String getCategory(){
	   return  category;
	} 
	public void setCategory(String category) {
	   this.category = category;
    }
	public String getContent(){
	   return  content;
	} 
	public void setContent(String content) {
	   this.content = content;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }     
}