package com.ced.sip.contract.entity;

import java.util.Date;

/** 
 * 类名称：ContractTemplate
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractTemplate implements java.io.Serializable {

	// 属性信息
	private Long ctId;     //主键
	private String templateMame;	 //模板名称
	private String templateUrl;	 //模板路径
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Long comId;     //
	
	
	public ContractTemplate() {
		super();
	}
	
	public Long getCtId(){
	   return  ctId;
	} 
	public void setCtId(Long ctId) {
	   this.ctId = ctId;
    }     
	public String getTemplateMame(){
	   return  templateMame;
	} 
	public void setTemplateMame(String templateMame) {
	   this.templateMame = templateMame;
    }
	public String getTemplateUrl(){
	   return  templateUrl;
	} 
	public void setTemplateUrl(String templateUrl) {
	   this.templateUrl = templateUrl;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }     
}