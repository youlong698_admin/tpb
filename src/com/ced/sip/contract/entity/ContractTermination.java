package com.ced.sip.contract.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：ContractTermination
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public class ContractTermination extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long ctId;     //主键
	private Long ciId;     //合同ID
	private String contractCode;	 //合同编号
	private String contractName;	 //合同名称
	private String terminationReason;	 //终止原因
	private Date submitDate;    //提交日期
	private String status;	 //状态
	private String statusCn;	 //状态中文
	private Long writerId;     //填报人ID
	private String writer;	 //填报人
	private Date writeDete;    //填报日期
	
	//临时
	private String writerCn;
	
	public ContractTermination() {
		super();
	}
	
	public Long getCtId(){
	   return  ctId;
	} 
	public void setCtId(Long ctId) {
	   this.ctId = ctId;
    }     
	public Long getCiId(){
	   return  ciId;
	} 
	public void setCiId(Long ciId) {
	   this.ciId = ciId;
    }     
	public String getContractCode(){
	   return  contractCode;
	} 
	public void setContractCode(String contractCode) {
	   this.contractCode = contractCode;
    }
	public String getContractName(){
	   return  contractName;
	} 
	public void setContractName(String contractName) {
	   this.contractName = contractName;
    }
	public String getTerminationReason(){
	   return  terminationReason;
	} 
	public void setTerminationReason(String terminationReason) {
	   this.terminationReason = terminationReason;
    }
	public Date getSubmitDate(){
	   return  submitDate;
	} 
	public void setSubmitDate(Date submitDate) {
	   this.submitDate = submitDate;
    }	    
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public String getStatusCn(){
	   return  statusCn;
	} 
	public void setStatusCn(String statusCn) {
	   this.statusCn = statusCn;
    }
	public Long getWriterId(){
	   return  writerId;
	} 
	public void setWriterId(Long writerId) {
	   this.writerId = writerId;
    }     
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDete(){
	   return  writeDete;
	} 
	public void setWriteDete(Date writeDete) {
	   this.writeDete = writeDete;
    }
	public String getWriterCn() {
		return writerCn;
	}
	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}	    
}