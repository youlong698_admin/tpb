package com.ced.sip.contract.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：ContractInfo
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractInfo extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long ciId;     //主键
	private Long rcId;     //项目ID
	private Long baId;     //授标ID
	private String contractType;//合同类型
	private String projectType;//项目类型
	private String bidCode; //项目编号
	private String projectName;	 //项目名称
	private String contractCode;	 //合同编号
	private String contractName;	 //合同名称
	private String contractSignAddress;	 //合同签订地址
	private String contractPersonNameA;	 //甲方名称
	private String contractUndertaker;	 //甲方联系人
	private String contractTelA;	 //甲方电话
	private String contractFaxA;	 //甲方传真
	private String contractAddressA;	 //甲方地址
	private String contractNameB;	 //乙方名称
	private String contractPersonB;	 //乙方联系人
	private String contractTelB;	 //乙方电话
	private String contractFaxB;	 //乙方传真
	private String contractAddressB;	 //乙方地址
	private Double contractMoney;	//合同金额
	private String conMoneyType;	 //货币类型
	private Date requirementArrivalDate;    //到货日期
	private Date warrantyPeriodStartDate;    //质保开始日期
	private Date warrantyPeriodStopDate;    //质保结束日期
	private Date signDate;    //合同签订日期
	private Date conValidDate;    //合同生效日期
	private Date conFinishDate;    //合同结束日期
	private String paymentType;	 //付款方式
	private String conRemark;	 //备注
	private String runStatus;	 //运行状态
	private String statusCn;	 //状态中文
	private String isUsable;	 //是否有效
	private String lastModifyPerson;	 //合同最后修改人
	private Date lastModifyDate;	 //合同最后修改时间
	private Long supplierId;     //供应商ID
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Long comId;     //
	private Long deptId;     //
	private Long contractTempId;//合同模版ID
	private String contractAppFile;//合同审批附件
	private String contractScanFile;//合同审批扫描件
	private Date auditDate;
	private int isOrder;//是否生成订单
	private int type;
	private String contractMobileA;	 //甲方联系人手机
	private String contractMobileB;	 //甲方联系人手机
	private Long floatCode;
	private Double contractAmount;//合同物资总数量
	private Double realInputAmount;//合同物资实际收货数量
	private Double realSendAmount;//合同物资实际发货数量
	private Date lastGoodArriveDate;//最后收货日期
	private int framework;//框架协议
	private Long billUndertakerId; //收货联系人ID
	private String billUndertaker;	 //收货联系人
	private String billMobile;	 //收货联系人手机
	private String bankAccount;//银行账号
	private String bankAccountName;//银行户名
	private String bank;//开户银行
	private String dutyParagraph;//税号
	
	private String orderId;//流程实例标示
	private String processId;//流程定义标示
	private String processName;//流程定义名称
	private String orderState;//流程状态
	private String orderStateName;//流程状态名称
	private String instanceUrl;//实例启动页面
	
	public ContractInfo() {
		super();
	}
	
	public Long getCiId(){
	   return  ciId;
	} 
	public void setCiId(Long ciId) {
	   this.ciId = ciId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getBidCode() {
		return bidCode;
	}

	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}

	public String getProjectName(){
	   return  projectName;
	} 
	public void setProjectName(String projectName) {
	   this.projectName = projectName;
    }
	public String getContractCode(){
	   return  contractCode;
	} 
	public void setContractCode(String contractCode) {
	   this.contractCode = contractCode;
    }
	public String getContractName(){
	   return  contractName;
	} 
	public void setContractName(String contractName) {
	   this.contractName = contractName;
    }
	public String getContractSignAddress(){
	   return  contractSignAddress;
	} 
	public void setContractSignAddress(String contractSignAddress) {
	   this.contractSignAddress = contractSignAddress;
    }
	public String getContractPersonNameA(){
	   return  contractPersonNameA;
	} 
	public void setContractPersonNameA(String contractPersonNameA) {
	   this.contractPersonNameA = contractPersonNameA;
    }
	public String getContractUndertaker(){
	   return  contractUndertaker;
	} 
	public void setContractUndertaker(String contractUndertaker) {
	   this.contractUndertaker = contractUndertaker;
    }
	public String getContractTelA(){
	   return  contractTelA;
	} 
	public void setContractTelA(String contractTelA) {
	   this.contractTelA = contractTelA;
    }
	public String getContractFaxA(){
	   return  contractFaxA;
	} 
	public void setContractFaxA(String contractFaxA) {
	   this.contractFaxA = contractFaxA;
    }
	public String getContractAddressA(){
	   return  contractAddressA;
	} 
	public void setContractAddressA(String contractAddressA) {
	   this.contractAddressA = contractAddressA;
    }
	public String getContractNameB(){
	   return  contractNameB;
	} 
	public void setContractNameB(String contractNameB) {
	   this.contractNameB = contractNameB;
    }
	public String getContractPersonB(){
	   return  contractPersonB;
	} 
	public void setContractPersonB(String contractPersonB) {
	   this.contractPersonB = contractPersonB;
    }
	public String getContractTelB(){
	   return  contractTelB;
	} 
	public void setContractTelB(String contractTelB) {
	   this.contractTelB = contractTelB;
    }
	public String getContractFaxB(){
	   return  contractFaxB;
	} 
	public void setContractFaxB(String contractFaxB) {
	   this.contractFaxB = contractFaxB;
    }
	public String getContractAddressB(){
	   return  contractAddressB;
	} 
	public void setContractAddressB(String contractAddressB) {
	   this.contractAddressB = contractAddressB;
    }
	public Double getContractMoney(){
	   return  contractMoney;
	} 
	public void setContractMoney(Double contractMoney) {
	   this.contractMoney = contractMoney;
    }	
	public String getConMoneyType(){
	   return  conMoneyType;
	} 
	public void setConMoneyType(String conMoneyType) {
	   this.conMoneyType = conMoneyType;
    }
	public Date getRequirementArrivalDate(){
	   return  requirementArrivalDate;
	} 
	public void setRequirementArrivalDate(Date requirementArrivalDate) {
	   this.requirementArrivalDate = requirementArrivalDate;
    }	    
	public Date getWarrantyPeriodStartDate(){
	   return  warrantyPeriodStartDate;
	} 
	public void setWarrantyPeriodStartDate(Date warrantyPeriodStartDate) {
	   this.warrantyPeriodStartDate = warrantyPeriodStartDate;
    }	    
	public Date getWarrantyPeriodStopDate(){
	   return  warrantyPeriodStopDate;
	} 
	public void setWarrantyPeriodStopDate(Date warrantyPeriodStopDate) {
	   this.warrantyPeriodStopDate = warrantyPeriodStopDate;
    }	    
	public Date getSignDate(){
	   return  signDate;
	} 
	public void setSignDate(Date signDate) {
	   this.signDate = signDate;
    }	    
	public Date getConValidDate(){
	   return  conValidDate;
	} 
	public void setConValidDate(Date conValidDate) {
	   this.conValidDate = conValidDate;
    }	    
	public Date getConFinishDate(){
	   return  conFinishDate;
	} 
	public void setConFinishDate(Date conFinishDate) {
	   this.conFinishDate = conFinishDate;
    }	
	public String getPaymentType(){
	   return  paymentType;
	} 
	public void setPaymentType(String paymentType) {
	   this.paymentType = paymentType;
    }
	public String getConRemark(){
	   return  conRemark;
	} 
	public void setConRemark(String conRemark) {
	   this.conRemark = conRemark;
    }
	public String getRunStatus(){
	   return  runStatus;
	} 
	public void setRunStatus(String runStatus) {
	   this.runStatus = runStatus;
    }
	public String getStatusCn(){
	   return  statusCn;
	} 
	public void setStatusCn(String statusCn) {
	   this.statusCn = statusCn;
    }
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getLastModifyPerson(){
	   return  lastModifyPerson;
	} 
	public void setLastModifyPerson(String lastModifyPerson) {
	   this.lastModifyPerson = lastModifyPerson;
    }
	public Date getLastModifyDate(){
	   return  lastModifyDate;
	} 
	public void setLastModifyDate(Date lastModifyDate) {
	   this.lastModifyDate = lastModifyDate;
    }
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }     
	public Long getDeptId(){
	   return  deptId;
	} 
	public void setDeptId(Long deptId) {
	   this.deptId = deptId;
    }

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Long getContractTempId() {
		return contractTempId;
	}

	public void setContractTempId(Long contractTempId) {
		this.contractTempId = contractTempId;
	}

	public String getContractAppFile() {
		return contractAppFile;
	}

	public void setContractAppFile(String contractAppFile) {
		this.contractAppFile = contractAppFile;
	}

	public String getContractScanFile() {
		return contractScanFile;
	}

	public void setContractScanFile(String contractScanFile) {
		this.contractScanFile = contractScanFile;
	}

	public Long getBaId() {
		return baId;
	}

	public void setBaId(Long baId) {
		this.baId = baId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getOrderStateName() {
		return orderStateName;
	}

	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public int getIsOrder() {
		return isOrder;
	}

	public void setIsOrder(int isOrder) {
		this.isOrder = isOrder;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getContractMobileA() {
		return contractMobileA;
	}

	public void setContractMobileA(String contractMobileA) {
		this.contractMobileA = contractMobileA;
	}

	public String getContractMobileB() {
		return contractMobileB;
	}

	public void setContractMobileB(String contractMobileB) {
		this.contractMobileB = contractMobileB;
	}

	public Long getFloatCode() {
		return floatCode;
	}

	public void setFloatCode(Long floatCode) {
		this.floatCode = floatCode;
	}

	public Double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(Double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public Double getRealInputAmount() {
		return realInputAmount;
	}

	public void setRealInputAmount(Double realInputAmount) {
		this.realInputAmount = realInputAmount;
	}

	public Double getRealSendAmount() {
		return realSendAmount;
	}

	public void setRealSendAmount(Double realSendAmount) {
		this.realSendAmount = realSendAmount;
	}

	public Date getLastGoodArriveDate() {
		return lastGoodArriveDate;
	}

	public void setLastGoodArriveDate(Date lastGoodArriveDate) {
		this.lastGoodArriveDate = lastGoodArriveDate;
	}

	public int getFramework() {
		return framework;
	}

	public void setFramework(int framework) {
		this.framework = framework;
	}

	public Long getBillUndertakerId() {
		return billUndertakerId;
	}

	public void setBillUndertakerId(Long billUndertakerId) {
		this.billUndertakerId = billUndertakerId;
	}

	public String getBillUndertaker() {
		return billUndertaker;
	}

	public void setBillUndertaker(String billUndertaker) {
		this.billUndertaker = billUndertaker;
	}

	public String getBillMobile() {
		return billMobile;
	}

	public void setBillMobile(String billMobile) {
		this.billMobile = billMobile;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getDutyParagraph() {
		return dutyParagraph;
	}

	public void setDutyParagraph(String dutyParagraph) {
		this.dutyParagraph = dutyParagraph;
	}     
}