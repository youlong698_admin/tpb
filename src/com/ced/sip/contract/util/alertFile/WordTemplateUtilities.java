package com.ced.sip.contract.util.alertFile;



import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class WordTemplateUtilities {
    private static final WordTemplateUtilities word = new WordTemplateUtilities();

    private static WordTemplate template;

    private WordTemplateUtilities() {
    }

    public static WordTemplateUtilities GetInstance() {
        return word;
    }

    public void toWord(String infilePath, String outfilePath, Map<String, Object> map) {
        File intfile = new File(infilePath);
        FileInputStream fileInputStream = null;
        File outfile = new File(outfilePath);
        FileOutputStream out;
        try {
            fileInputStream = new FileInputStream(intfile);
            template = new WordTemplate(fileInputStream);

            template.replaceTag(map);// 鏇挎崲瀛楃涓叉ā鐗�

            out = new FileOutputStream(outfile);
            BufferedOutputStream bos = new BufferedOutputStream(out);
            template.write(bos);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
