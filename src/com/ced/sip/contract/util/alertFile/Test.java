package com.ced.sip.contract.util.alertFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
public class Test {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String infilePath = "d://15.doc";
		String outfilePath = "d://151.doc";
		Map<String, Object> resultsLabel = new HashMap<String, Object>();
		
		/**
		 * 替换段落
		 */
		Map<String, String> resultsString = new HashMap<String, String>();
		resultsString.put("年", "2014");
		resultsLabel.put("String", resultsString);
		
		/**
		 * 替换表格
		 *value= List<Map<String,Object>> 是数据中获取出来的
		 */
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		Map<String,Object> maptable=new LinkedHashMap<String, Object>();
		maptable.put("物资名称", "物资名称");
		maptable.put("规格型号", "规格型号");
		list.add(maptable);
		maptable=new LinkedHashMap<String, Object>();
		maptable.put("物资名称", "1");
		maptable.put("规格型号", "1");
		list.add(maptable);
		
		maptable=new LinkedHashMap<String, Object>();
		maptable.put("物资名称", "2");
		maptable.put("规格型号", "2");
		list.add(maptable);
		
		Map<String, Object> resultsTable = new HashMap<String, Object>();
		resultsTable.put("table", list);
		resultsLabel.put("Table", resultsTable);
		
		
		
		WordTemplateUtilities.GetInstance().toWord(infilePath, outfilePath, resultsLabel);
		
		//WordTemplateUtilities.GetInstance().convertWordToPdf(outfilePath, outfilePathPdf);
	}

}