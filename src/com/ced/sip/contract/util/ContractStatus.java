package com.ced.sip.contract.util;

public class ContractStatus {
   //	-------------------------------------- 合同管理  -----------------------------------------

	/**合同类型 */
	public static final String COMMON_CONTRACT_TYPE = "合同分类" ; 
	/**项目类型 */
	public static final String COMMON_PROJECT_TYPE = "项目分类" ; 
	/**货币类型 */
	public static final String COMMON_MONEY_TYPE = "币种" ; 
 
	//	-------------------------------------- 合同运行状态  -----------------------------------------
	/** 合同运行状态:已起草 "00" */
	public static final String CONTRACT_RUN_STATUS_01="01";
	
	/** 合同运行状态:已起草*/
	public static final String CONTRACT_RUN_STATUS_01_Text = "已起草";
	
	/** 合同运行状态:审批通过 "0" */
	public static final String CONTRACT_RUN_STATUS_00="0";
	
	/** 合同运行状态:审批通过*/
	public static final String CONTRACT_RUN_STATUS_00_Text = "审批通过";
	
	/** 合同运行状态:审批通过 "1" */
	public static final String CONTRACT_RUN_STATUS_1="1";
	
	/** 合同运行状态:审批通过*/
	public static final String CONTRACT_RUN_STATUS_1_Text = "审批中";
	
	/** 合同运行状态:审批退回 "03" */
	public static final String CONTRACT_RUN_STATUS_03="3";
	
	/** 合同运行状态:审批退回*/
	public static final String CONTRACT_RUN_STATUS_03_Text = "审批退回";
	
	/** 合同运行状态:已发送 "04" */
	public static final String CONTRACT_RUN_STATUS_04="4";
	
	/** 合同运行状态:已发送*/
	public static final String CONTRACT_RUN_STATUS_04_Text = "已发送至供应商";
	
	/** 合同运行状态:已确认 "05" */
	public static final String CONTRACT_RUN_STATUS_05="5";
	
	/** 合同运行状态:已确认*/
	public static final String CONTRACT_RUN_STATUS_05_Text = "供应商已确认";
	
	/** 合同运行状态:已确认 "6" */
	public static final String CONTRACT_RUN_STATUS_06="6";
	
	/** 合同运行状态:已确认*/
	public static final String CONTRACT_RUN_STATUS_06_Text = "供应商退回";
	
	/** 合同运行状态:已签订 "7" */
	public static final String CONTRACT_RUN_STATUS_07="7";
	
	/** 合同运行状态:已签订 */
	public static final String CONTRACT_RUN_STATUS_07_Text = "已签订";
	
	
	/** 合同运行状态:已中止 "8" */
	public static final String CONTRACT_RUN_STATUS_08="8";
	
	/** 合同运行状态:已中止*/
	public static final String CONTRACT_RUN_STATUS_08_Text = "已终止";
	
	/** 合同运行状态:已发货 "9" */
	public static final String CONTRACT_RUN_STATUS_09="9";
	
	/** 合同运行状态:已发货 */
	public static final String CONTRACT_RUN_STATUS_09_Text = "已发货";
	
	/** 合同运行状态:收货完成 "10" */
	public static final String CONTRACT_RUN_STATUS_10="10";
	
	/** 合同运行状态:收货完成 */
	public static final String CONTRACT_RUN_STATUS_10_Text = "收货完成";
	
}
