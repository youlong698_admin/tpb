package com.ced.sip.contract.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.contract.entity.ContractMaterial;
/** 
 * 类名称：IContractMaterialBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public interface IContractMaterialBiz {

	/**
	 * 根据主键获得合同物资信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractMaterial getContractMaterial(Long id) throws BaseException;

	/**
	 * 添加合同物资信息信息
	 * @param contractMaterial 合同物资信息表实例
	 * @throws BaseException 
	 */
	abstract void saveContractMaterial(ContractMaterial contractMaterial) throws BaseException;

	/**
	 * 更新合同物资信息表实例
	 * @param contractMaterial 合同物资信息表实例
	 * @throws BaseException 
	 */
	abstract void updateContractMaterial(ContractMaterial contractMaterial) throws BaseException;

	/**
	 * 删除合同物资信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractMaterial(String id) throws BaseException;

	/**
	 * 删除合同物资信息表实例
	 * @param contractMaterial 合同物资信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteContractMaterial(ContractMaterial contractMaterial) throws BaseException;

	/**
	 * 删除合同物资信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractMaterials(String[] id) throws BaseException;

	/**
	 * 获得合同物资信息表数据集
	 * contractMaterial 合同物资信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractMaterial getContractMaterialByContractMaterial(ContractMaterial contractMaterial) throws BaseException ;
	
	/**
	 * 获得所有合同物资信息表数据集
	 * @param contractMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractMaterialList(ContractMaterial contractMaterial) throws BaseException ;
	
	/**
	 * 获得所有合同物资信息表数据集
	 * @param rollPage 分页对象
	 * @param contractMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractMaterialList(RollPage rollPage, ContractMaterial contractMaterial)
			throws BaseException;

	/**
	 * 删除合同物资信息表实例    合同Id
	 * @param ciId 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractMaterialByCiId(Long ciId) throws BaseException;
	/**
	 * 根据发货数量更新可订单明细中的发货数量
	 * @param sendAmount
	 * @param omId
	 * @throws BaseException
	 */
	abstract void updateSendAmount(Double sendAmount,Long cmId) throws BaseException;
	/**
	 * 根据收货数量更新可订单明细中的收货数量
	 * @param inputAmount
	 * @param omId
	 * @throws BaseException
	 */
	abstract void updateInputAmount(Double inputAmount,Long cmId) throws BaseException;
}