package com.ced.sip.contract.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.contract.entity.ContractTemplate;
/** 
 * 类名称：IContractTemplateBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public interface IContractTemplateBiz {

	/**
	 * 根据主键获得合同模板表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractTemplate getContractTemplate(Long id) throws BaseException;

	/**
	 * 添加合同模板信息
	 * @param contractTemplate 合同模板表实例
	 * @throws BaseException 
	 */
	abstract void saveContractTemplate(ContractTemplate contractTemplate) throws BaseException;

	/**
	 * 更新合同模板表实例
	 * @param contractTemplate 合同模板表实例
	 * @throws BaseException 
	 */
	abstract void updateContractTemplate(ContractTemplate contractTemplate) throws BaseException;

	/**
	 * 删除合同模板表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractTemplate(String id) throws BaseException;

	/**
	 * 删除合同模板表实例
	 * @param contractTemplate 合同模板表实例
	 * @throws BaseException 
	 */
	abstract void deleteContractTemplate(ContractTemplate contractTemplate) throws BaseException;

	/**
	 * 删除合同模板表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractTemplates(String[] id) throws BaseException;

	/**
	 * 获得合同模板表数据集
	 * contractTemplate 合同模板表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractTemplate getContractTemplateByContractTemplate(ContractTemplate contractTemplate) throws BaseException ;
	
	/**
	 * 获得所有合同模板表数据集
	 * @param contractTemplate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractTemplateList(ContractTemplate contractTemplate) throws BaseException ;
	
	/**
	 * 获得所有合同模板表数据集
	 * @param rollPage 分页对象
	 * @param contractTemplate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractTemplateList(RollPage rollPage, ContractTemplate contractTemplate)
			throws BaseException;

}