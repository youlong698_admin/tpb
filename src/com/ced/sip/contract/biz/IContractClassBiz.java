package com.ced.sip.contract.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.contract.entity.ContractClass;
/** 
 * 类名称：IContractClassBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public interface IContractClassBiz {

	/**
	 * 根据主键获得合同字典表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractClass getContractClass(Long id) throws BaseException;

	/**
	 * 添加合同字典信息
	 * @param contractClass 合同字典表实例
	 * @throws BaseException 
	 */
	abstract void saveContractClass(ContractClass contractClass) throws BaseException;

	/**
	 * 更新合同字典表实例
	 * @param contractClass 合同字典表实例
	 * @throws BaseException 
	 */
	abstract void updateContractClass(ContractClass contractClass) throws BaseException;

	/**
	 * 删除合同字典表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractClass(String id) throws BaseException;

	/**
	 * 删除合同字典表实例
	 * @param contractClass 合同字典表实例
	 * @throws BaseException 
	 */
	abstract void deleteContractClass(ContractClass contractClass) throws BaseException;

	/**
	 * 删除合同字典表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractClasss(String[] id) throws BaseException;

	/**
	 * 获得合同字典表数据集
	 * contractClass 合同字典表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractClass getContractClassByContractClass(ContractClass contractClass) throws BaseException ;
	
	/**
	 * 获得所有合同字典表数据集
	 * @param contractClass 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractClassList(ContractClass contractClass) throws BaseException ;
	
	/**
	 * 获得所有合同字典表数据集
	 * @param rollPage 分页对象
	 * @param contractClass 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractClassList(RollPage rollPage, ContractClass contractClass)
			throws BaseException;

}