package com.ced.sip.contract.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.contract.entity.ContractConfirm;
/** 
 * 类名称：IContractConfirmBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public interface IContractConfirmBiz {

	/**
	 * 根据主键获得合同供应商确认表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractConfirm getContractConfirm(Long id) throws BaseException;

	/**
	 * 添加合同供应商确认信息
	 * @param contractConfirm 合同供应商确认表实例
	 * @throws BaseException 
	 */
	abstract void saveContractConfirm(ContractConfirm contractConfirm) throws BaseException;

	/**
	 * 更新合同供应商确认表实例
	 * @param contractConfirm 合同供应商确认表实例
	 * @throws BaseException 
	 */
	abstract void updateContractConfirm(ContractConfirm contractConfirm) throws BaseException;

	/**
	 * 删除合同供应商确认表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractConfirm(String id) throws BaseException;

	/**
	 * 删除合同供应商确认表实例
	 * @param contractConfirm 合同供应商确认表实例
	 * @throws BaseException 
	 */
	abstract void deleteContractConfirm(ContractConfirm contractConfirm) throws BaseException;

	/**
	 * 删除合同供应商确认表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractConfirms(String[] id) throws BaseException;

	/**
	 * 获得合同供应商确认表数据集
	 * contractConfirm 合同供应商确认表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractConfirm getContractConfirmByContractConfirm(ContractConfirm contractConfirm) throws BaseException ;
	
	/**
	 * 获得所有合同供应商确认表数据集
	 * @param contractConfirm 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractConfirmList(ContractConfirm contractConfirm) throws BaseException ;
	
	/**
	 * 获得所有合同供应商确认表数据集
	 * @param rollPage 分页对象
	 * @param contractConfirm 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractConfirmList(RollPage rollPage, ContractConfirm contractConfirm)
			throws BaseException;

}