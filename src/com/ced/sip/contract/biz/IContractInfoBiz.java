package com.ced.sip.contract.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.contract.entity.ContractInfo;
/** 
 * 类名称：IContractInfoBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public interface IContractInfoBiz {

	/**
	 * 根据主键获得合同信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractInfo getContractInfo(Long id) throws BaseException;

	/**
	 * 添加合同信息信息
	 * @param contractInfo 合同信息表实例
	 * @throws BaseException 
	 */
	abstract void saveContractInfo(ContractInfo contractInfo) throws BaseException;

	/**
	 * 更新合同信息表实例
	 * @param contractInfo 合同信息表实例
	 * @throws BaseException 
	 */
	abstract void updateContractInfo(ContractInfo contractInfo) throws BaseException;

	/**
	 * 删除合同信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractInfo(String id) throws BaseException;

	/**
	 * 删除合同信息表实例
	 * @param contractInfo 合同信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteContractInfo(ContractInfo contractInfo) throws BaseException;

	/**
	 * 删除合同信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractInfos(String[] id) throws BaseException;

	/**
	 * 获得合同信息表数据集
	 * contractInfo 合同信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractInfo getContractInfoByContractInfo(ContractInfo contractInfo) throws BaseException ;
	
	/**
	 * 获得所有合同信息表数据集
	 * @param contractInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractInfoList(ContractInfo contractInfo) throws BaseException ;
	
	/**
	 * 获得所有合同信息表数据集
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractInfoList(RollPage rollPage, ContractInfo contractInfo,String sqlstr)
			throws BaseException;
	/**
	 * 获得所有合同监督信息表数据集
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractInfoSuperviseList(RollPage rollPage, ContractInfo contractInfo,String sqlstr)
			throws BaseException;
	/**
	 * 获得所有合同信息表数据集   供应商端查询所用
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @param sign 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractInfoListBySupplier(RollPage rollPage, ContractInfo contractInfo,int sign)
			throws BaseException;
	/**
	 * 选择所有合同信息表数据集 订单所用
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @param sign 标识
	 * @param sqlstr 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractInfoListForOrder(RollPage rollPage, ContractInfo contractInfo,int sign,String sqlstr)
			throws BaseException;
	/**
	 * 根据当前年获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	abstract int getMaxFloatCodeByContractCodePrefix(String contractCodePrefix,Long comId)throws BaseException;
	/**
	 * 依据条件和合同D更新合同明细表
	 * @param param
	 * @param ciId
	 * @throws BaseException
	 */
	abstract void updateContractInfoByOrder(String param,Long ciId) throws BaseException;
	/**
	 * 获得所有货单签收信息表数据集
	 * @param rollPage 分页对象
	 * @param orderInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 * @throws BaseException 
	 */
	abstract List getInputBillContractInfoList(RollPage rollPage, ContractInfo contractInfo,String sqlStr) throws BaseException;
	/**
	 * 获得合同总金额
	 * @param map 查询参数对象
	 * @param sqlStr 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract double sumContractInfoMoney(Map<String,Object> mapParams,String sqlStr)
	         throws BaseException;
}