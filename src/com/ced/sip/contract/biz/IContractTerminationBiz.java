package com.ced.sip.contract.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.contract.entity.ContractTermination;
/** 
 * 类名称：IContractTerminationBiz
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public interface IContractTerminationBiz {

	/**
	 * 根据主键获得合同终止表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractTermination getContractTermination(Long id) throws BaseException;

	/**
	 * 添加合同终止表信息
	 * @param contractTermination 合同终止表表实例
	 * @throws BaseException 
	 */
	abstract void saveContractTermination(ContractTermination contractTermination) throws BaseException;

	/**
	 * 更新合同终止表表实例
	 * @param contractTermination 合同终止表表实例
	 * @throws BaseException 
	 */
	abstract void updateContractTermination(ContractTermination contractTermination) throws BaseException;

	/**
	 * 删除合同终止表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractTermination(String id) throws BaseException;

	/**
	 * 删除合同终止表表实例
	 * @param contractTermination 合同终止表表实例
	 * @throws BaseException 
	 */
	abstract void deleteContractTermination(ContractTermination contractTermination) throws BaseException;

	/**
	 * 删除合同终止表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteContractTerminations(String[] id) throws BaseException;

	/**
	 * 获得合同终止表表数据集
	 * contractTermination 合同终止表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ContractTermination getContractTerminationByContractTermination(ContractTermination contractTermination) throws BaseException ;
	
	/**
	 * 获得所有合同终止表表数据集
	 * @param contractTermination 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractTerminationList(ContractTermination contractTermination) throws BaseException ;
	
	/**
	 * 获得所有合同终止表表数据集
	 * @param rollPage 分页对象
	 * @param contractTermination 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getContractTerminationList(RollPage rollPage, ContractTermination contractTermination)
			throws BaseException;

}