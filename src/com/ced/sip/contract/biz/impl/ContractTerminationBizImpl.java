package com.ced.sip.contract.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractTerminationBiz;
import com.ced.sip.contract.entity.ContractTermination;
/** 
 * 类名称：ContractTerminationBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public class ContractTerminationBizImpl extends BaseBizImpl implements IContractTerminationBiz  {
	
	/**
	 * 根据主键获得合同终止表表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public ContractTermination getContractTermination(Long id) throws BaseException {
		return (ContractTermination)this.getObject(ContractTermination.class, id);
	}
	
	/**
	 * 获得合同终止表表实例
	 * @param contractTermination 合同终止表表实例
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public ContractTermination getContractTermination(ContractTermination contractTermination) throws BaseException {
		return (ContractTermination)this.getObject(ContractTermination.class, contractTermination.getCtId() );
	}
	
	/**
	 * 添加合同终止表信息
	 * @param contractTermination 合同终止表表实例
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void saveContractTermination(ContractTermination contractTermination) throws BaseException{
		this.saveObject( contractTermination ) ;
	}
	
	/**
	 * 更新合同终止表表实例
	 * @param contractTermination 合同终止表表实例
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void updateContractTermination(ContractTermination contractTermination) throws BaseException{
		this.updateObject( contractTermination ) ;
	}
	
	/**
	 * 删除合同终止表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void deleteContractTermination(String id) throws BaseException {
		this.removeObject( this.getContractTermination( new Long(id) ) ) ;
	}
	
	/**
	 * 删除合同终止表表实例
	 * @param contractTermination 合同终止表表实例
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void deleteContractTermination(ContractTermination contractTermination) throws BaseException {
		this.removeObject( contractTermination ) ;
	}
	
	/**
	 * 删除合同终止表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void deleteContractTerminations(String[] id) throws BaseException {
		this.removeBatchObject(ContractTermination.class, id) ;
	}
	
	/**
	 * 获得合同终止表表数据集
	 * contractTermination 合同终止表表实例
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public ContractTermination getContractTerminationByContractTermination(ContractTermination contractTermination) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractTermination de where 1 = 1 " );

		hql.append(" order by de.ctId desc ");
		List list = this.getObjects( hql.toString() );
		contractTermination = new ContractTermination();
		if(list!=null&&list.size()>0){
			contractTermination = (ContractTermination)list.get(0);
		}
		return contractTermination;
	}
	
	/**
	 * 获得所有合同终止表表数据集
	 * @param contractTermination 查询参数对象
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public List getContractTerminationList(ContractTermination contractTermination) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractTermination de where 1 = 1 " );
		if(contractTermination != null){
			if (StringUtil.isNotBlank(contractTermination.getCiId())) {
				hql.append(" and de.ciId = ").append(contractTermination.getCiId());
			}
			if (StringUtil.isNotBlank(contractTermination.getCtId())) {
				hql.append(" and de.ctId = ").append(contractTermination.getCtId());
			}
		}
		hql.append(" order by de.ctId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有合同终止表表数据集
	 * @param rollPage 分页对象
	 * @param contractTermination 查询参数对象
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public List getContractTerminationList(RollPage rollPage, ContractTermination contractTermination) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractTermination de where 1 = 1 " );
		if(contractTermination != null){
			if (StringUtil.isNotBlank(contractTermination.getCiId())) {
				hql.append(" and de.ciId = ").append(contractTermination.getCiId());
			}
			if (StringUtil.isNotBlank(contractTermination.getCtId())) {
				hql.append(" and de.ctId = ").append(contractTermination.getCtId());
			}
		}
		hql.append(" order by de.ctId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
