package com.ced.sip.contract.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractConfirmBiz;
import com.ced.sip.contract.entity.ContractConfirm;
/** 
 * 类名称：ContractConfirmBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-31
 */
public class ContractConfirmBizImpl extends BaseBizImpl implements IContractConfirmBiz  {
	
	/**
	 * 根据主键获得合同供应商确认表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public ContractConfirm getContractConfirm(Long id) throws BaseException {
		return (ContractConfirm)this.getObject(ContractConfirm.class, id);
	}
	
	/**
	 * 获得合同供应商确认表实例
	 * @param contractConfirm 合同供应商确认表实例
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public ContractConfirm getContractConfirm(ContractConfirm contractConfirm) throws BaseException {
		return (ContractConfirm)this.getObject(ContractConfirm.class, contractConfirm.getCcId() );
	}
	
	/**
	 * 添加合同供应商确认信息
	 * @param contractConfirm 合同供应商确认表实例
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void saveContractConfirm(ContractConfirm contractConfirm) throws BaseException{
		this.saveObject( contractConfirm ) ;
	}
	
	/**
	 * 更新合同供应商确认表实例
	 * @param contractConfirm 合同供应商确认表实例
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void updateContractConfirm(ContractConfirm contractConfirm) throws BaseException{
		this.updateObject( contractConfirm ) ;
	}
	
	/**
	 * 删除合同供应商确认表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void deleteContractConfirm(String id) throws BaseException {
		this.removeObject( this.getContractConfirm( new Long(id) ) ) ;
	}
	
	/**
	 * 删除合同供应商确认表实例
	 * @param contractConfirm 合同供应商确认表实例
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void deleteContractConfirm(ContractConfirm contractConfirm) throws BaseException {
		this.removeObject( contractConfirm ) ;
	}
	
	/**
	 * 删除合同供应商确认表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-31
	 * @throws BaseException 
	 */
	public void deleteContractConfirms(String[] id) throws BaseException {
		this.removeBatchObject(ContractConfirm.class, id) ;
	}
	
	/**
	 * 获得合同供应商确认表数据集
	 * contractConfirm 合同供应商确认表实例
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public ContractConfirm getContractConfirmByContractConfirm(ContractConfirm contractConfirm) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractConfirm de where 1 = 1 " );

		hql.append(" order by de.ccId desc ");
		List list = this.getObjects( hql.toString() );
		contractConfirm = new ContractConfirm();
		if(list!=null&&list.size()>0){
			contractConfirm = (ContractConfirm)list.get(0);
		}
		return contractConfirm;
	}
	
	/**
	 * 获得所有合同供应商确认表数据集
	 * @param contractConfirm 查询参数对象
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public List getContractConfirmList(ContractConfirm contractConfirm) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractConfirm de where 1 = 1 " );
		if(contractConfirm != null){
			if (StringUtil.isNotBlank(contractConfirm.getCiId())) {
				hql.append(" and de.ciId = ").append(contractConfirm.getCiId());
			}
		}
		hql.append(" order by de.ccId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有合同供应商确认表数据集
	 * @param rollPage 分页对象
	 * @param contractConfirm 查询参数对象
	 * @author luguanglei 2017-07-31
	 * @return
	 * @throws BaseException 
	 */
	public List getContractConfirmList(RollPage rollPage, ContractConfirm contractConfirm) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractConfirm de where 1 = 1 " );
		if(contractConfirm != null){
			if (StringUtil.isNotBlank(contractConfirm.getCiId())) {
				hql.append(" and de.ciId = ").append(contractConfirm.getCiId());
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
