package com.ced.sip.contract.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractClassBiz;
import com.ced.sip.contract.entity.ContractClass;
/** 
 * 类名称：ContractClassBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractClassBizImpl extends BaseBizImpl implements IContractClassBiz  {
	
	/**
	 * 根据主键获得合同字典表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractClass getContractClass(Long id) throws BaseException {
		return (ContractClass)this.getObject(ContractClass.class, id);
	}
	
	/**
	 * 获得合同字典表实例
	 * @param contractClass 合同字典表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractClass getContractClass(ContractClass contractClass) throws BaseException {
		return (ContractClass)this.getObject(ContractClass.class, contractClass.getCcId() );
	}
	
	/**
	 * 添加合同字典信息
	 * @param contractClass 合同字典表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void saveContractClass(ContractClass contractClass) throws BaseException{
		this.saveObject( contractClass ) ;
	}
	
	/**
	 * 更新合同字典表实例
	 * @param contractClass 合同字典表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void updateContractClass(ContractClass contractClass) throws BaseException{
		this.updateObject( contractClass ) ;
	}
	
	/**
	 * 删除合同字典表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractClass(String id) throws BaseException {
		this.removeObject( this.getContractClass( new Long(id) ) ) ;
	}
	
	/**
	 * 删除合同字典表实例
	 * @param contractClass 合同字典表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractClass(ContractClass contractClass) throws BaseException {
		this.removeObject( contractClass ) ;
	}
	
	/**
	 * 删除合同字典表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractClasss(String[] id) throws BaseException {
		this.removeBatchObject(ContractClass.class, id) ;
	}
	
	/**
	 * 获得合同字典表数据集
	 * contractClass 合同字典表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractClass getContractClassByContractClass(ContractClass contractClass) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractClass de where 1 = 1 " );

		hql.append(" order by de.ccId desc ");
		List list = this.getObjects( hql.toString() );
		contractClass = new ContractClass();
		if(list!=null&&list.size()>0){
			contractClass = (ContractClass)list.get(0);
		}
		return contractClass;
	}
	
	/**
	 * 获得所有合同字典表数据集
	 * @param contractClass 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractClassList(ContractClass contractClass) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractClass de where 1 = 1 " );
		if(contractClass != null){
			if (StringUtil.isNotBlank(contractClass.getCategory())) {
				hql.append(" and de.category = '").append(contractClass.getCategory()).append("'");
			}
			if (StringUtil.isNotBlank(contractClass.getComId())) {
				hql.append(" and de.comId = ").append(contractClass.getComId()).append("");
			}
		}
		hql.append(" order by de.ccId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有合同字典表数据集
	 * @param rollPage 分页对象
	 * @param contractClass 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractClassList(RollPage rollPage, ContractClass contractClass) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractClass de where 1 = 1 " );
		if(contractClass != null){
			if (StringUtil.isNotBlank(contractClass.getCategory())) {
				hql.append(" and de.category = '").append(contractClass.getCategory()).append("'");
			}
			if (StringUtil.isNotBlank(contractClass.getComId())) {
				hql.append(" and de.comId = ").append(contractClass.getComId()).append("");
			}
		}
		hql.append(" order by de.ccId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
