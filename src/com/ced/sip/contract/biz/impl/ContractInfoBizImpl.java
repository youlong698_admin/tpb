package com.ced.sip.contract.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.contract.entity.ContractInfo;
import com.ced.sip.contract.util.ContractStatus;
/** 
 * 类名称：ContractInfoBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractInfoBizImpl extends BaseBizImpl implements IContractInfoBiz  {
	
	/**
	 * 根据主键获得合同信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractInfo getContractInfo(Long id) throws BaseException {
		return (ContractInfo)this.getObject(ContractInfo.class, id);
	}
	
	/**
	 * 获得合同信息表实例
	 * @param contractInfo 合同信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractInfo getContractInfo(ContractInfo contractInfo) throws BaseException {
		return (ContractInfo)this.getObject(ContractInfo.class, contractInfo.getCiId() );
	}
	
	/**
	 * 添加合同信息信息
	 * @param contractInfo 合同信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void saveContractInfo(ContractInfo contractInfo) throws BaseException{
		this.saveObject( contractInfo ) ;
	}
	
	/**
	 * 更新合同信息表实例
	 * @param contractInfo 合同信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void updateContractInfo(ContractInfo contractInfo) throws BaseException{
		this.updateObject( contractInfo ) ;
	}
	
	/**
	 * 删除合同信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractInfo(String id) throws BaseException {
		this.removeObject( this.getContractInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除合同信息表实例
	 * @param contractInfo 合同信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractInfo(ContractInfo contractInfo) throws BaseException {
		this.removeObject( contractInfo ) ;
	}
	
	/**
	 * 删除合同信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractInfos(String[] id) throws BaseException {
		this.removeBatchObject(ContractInfo.class, id) ;
	}
	
	/**
	 * 获得合同信息表数据集
	 * contractInfo 合同信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractInfo getContractInfoByContractInfo(ContractInfo contractInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where 1 = 1 " );

		hql.append(" order by de.ciId desc ");
		List list = this.getObjects( hql.toString() );
		contractInfo = new ContractInfo();
		if(list!=null&&list.size()>0){
			contractInfo = (ContractInfo)list.get(0);
		}
		return contractInfo;
	}
	
	/**
	 * 获得所有合同信息表数据集
	 * @param contractInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractInfoList(ContractInfo contractInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where 1 = 1 " );
		if(contractInfo != null){
			if (StringUtil.isNotBlank(contractInfo.getContractCode())) {
				hql.append(" and de.contractCode like '%").append(contractInfo.getContractCode()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractName())) {
				hql.append(" and de.contractName like '%").append(contractInfo.getContractName()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractNameB())) {
				hql.append(" and de.contractNameB like '%").append(contractInfo.getContractNameB()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.contractPersonNameA like '%").append(contractInfo.getContractPersonNameA()).append("'");
			}
		}
		hql.append(" order by de.ciId desc ");
		return this.getObjects( hql.toString() );
	}	
	/**
	 * 获得所有合同信息表数据集
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractInfoList(RollPage rollPage, ContractInfo contractInfo,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where 1 = 1 " );
		if(contractInfo != null){
			if (StringUtil.isNotBlank(contractInfo.getContractCode())) {
				hql.append(" and de.contractCode like '%").append(contractInfo.getContractCode()).append("' and de.contractName like '%").append(contractInfo.getContractCode()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractName())) {
				hql.append(" and de.contractName like '%").append(contractInfo.getContractName()).append("' and de.contractCode like '%").append(contractInfo.getContractName()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractNameB())) {
				hql.append(" and de.contractNameB like '%").append(contractInfo.getContractNameB()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.contractPersonNameA like '%").append(contractInfo.getContractPersonNameA()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getComId())) {
				hql.append(" and de.comId= ").append(contractInfo.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有合同监督信息表数据集
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractInfoSuperviseList(RollPage rollPage, ContractInfo contractInfo,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where de.runStatus!='01' " );
		if(contractInfo != null){
			if (StringUtil.isNotBlank(contractInfo.getContractCode())) {
				hql.append(" and de.contractCode like '%").append(contractInfo.getContractCode()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractName())) {
				hql.append(" and de.contractName like '%").append(contractInfo.getContractName()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractNameB())) {
				hql.append(" and de.contractNameB like '%").append(contractInfo.getContractNameB()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.contractPersonNameA like '%").append(contractInfo.getContractPersonNameA()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getComId())) {
				hql.append(" and de.comId= ").append(contractInfo.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有合同信息表数据集   供应商端查询所用
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractInfoListBySupplier(RollPage rollPage, ContractInfo contractInfo,int sign) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where 1 = 1 " );
		if(contractInfo != null){
			if (StringUtil.isNotBlank(contractInfo.getContractCode())) {
				hql.append(" and de.contractCode like '%").append(contractInfo.getContractCode()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractName())) {
				hql.append(" and de.contractName like '%").append(contractInfo.getContractName()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractNameB())) {
				hql.append(" and de.contractNameB like '%").append(contractInfo.getContractNameB()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.contractPersonNameA like '%").append(contractInfo.getContractPersonNameA()).append("'");
			}
		}
		if(sign==1) hql.append(" and de.runStatus='"+ContractStatus.CONTRACT_RUN_STATUS_04+"' ");
		else if(sign==3) hql.append(" and (de.runStatus='"+ContractStatus.CONTRACT_RUN_STATUS_07+"'  or de.runStatus='"+ContractStatus.CONTRACT_RUN_STATUS_09+"') and de.framework=1 ");
		hql.append(" and de.supplierId="+contractInfo.getSupplierId()+"");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 选择所有合同信息表数据集 订单所用
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @param sign 标识
	 * @param sqlstr 查询参数对象
	 * @author luguanglei 2017-08-04
	 * @return
	 * @throws BaseException 
	 */
	public List getContractInfoListForOrder(RollPage rollPage, ContractInfo contractInfo,int sign,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where de.runStatus='"+ContractStatus.CONTRACT_RUN_STATUS_07+"' " );
		if(contractInfo != null){
			if (StringUtil.isNotBlank(contractInfo.getContractCode())) {
				hql.append(" and de.contractCode like '%").append(contractInfo.getContractCode()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractName())) {
				hql.append(" and de.contractName like '%").append(contractInfo.getContractName()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractNameB())) {
				hql.append(" and de.contractNameB like '%").append(contractInfo.getContractNameB()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.contractPersonNameA like '%").append(contractInfo.getContractPersonNameA()).append("'");
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		if(sign==2) hql.append(" and de.isOrder=1 and de.framework=0 ");
		else if(sign==3) hql.append("");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 根据当前年获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	public int getMaxFloatCodeByContractCodePrefix(String contractCodePrefix,Long comId)throws BaseException{
		int floatCode = 0;
		StringBuffer hql = new StringBuffer("select max(de.floatCode) from ContractInfo de where 1 = 1 " );
		hql.append(" and de.comId = ").append(comId).append("");
		hql.append(" and de.contractCode like '").append(contractCodePrefix).append("%'");
		
		List list = this.getObjects(hql.toString());
		if(list!=null && list.get(0)!= null){
			floatCode = Integer.parseInt(list.get(0).toString());
		}
		return floatCode;
	}
	/**
	 * 依据条件和合同D更新合同明细表
	 * @param param
	 * @param ciId
	 * @throws BaseException
	 */
	public void updateContractInfoByOrder(String param, Long ciId)
			throws BaseException {
		String sql="begin update contract_material t set t.is_order=0 where "+param+";update contract_info t set t.is_order=0 where not exists(select ci_id from contract_material where is_order=1 and ci_id="+ciId+") and ci_id="+ciId+";end;";
		this.updateJdbcSql(sql);
		
	}
	/**
	 * 获得所有货单签收信息表数据集
	 * @param rollPage 分页对象
	 * @param contractInfo 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getInputBillContractInfoList(RollPage rollPage, ContractInfo contractInfo,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractInfo de where de.runStatus='"+ContractStatus.CONTRACT_RUN_STATUS_09+"' " );
		if(contractInfo != null){
			if (StringUtil.isNotBlank(contractInfo.getContractCode())) {
				hql.append(" and de.contractCode like '%").append(contractInfo.getContractCode()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractName())) {
				hql.append(" and de.contractName like '%").append(contractInfo.getContractName()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractNameB())) {
				hql.append(" and de.contractNameB like '%").append(contractInfo.getContractNameB()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.contractPersonNameA like '%").append(contractInfo.getContractPersonNameA()).append("'");
			}
			if (StringUtil.isNotBlank(contractInfo.getContractPersonNameA())) {
				hql.append(" and de.comId =").append(contractInfo.getComId());
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得合同总金额
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  double sumContractInfoMoney(Map<String,Object> mapParams,String sqlStr) throws BaseException {		
		StringBuffer hql = new StringBuffer("select sum(de.contractMoney) from ContractInfo de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		hql.append(" and de.auditDate is not null ");
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		return (Double)this.sumOrMinOrMaxObjects(hql.toString(),Double.class);
	}
}
