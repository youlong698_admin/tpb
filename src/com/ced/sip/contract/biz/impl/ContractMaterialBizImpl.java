package com.ced.sip.contract.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractMaterialBiz;
import com.ced.sip.contract.entity.ContractMaterial;
/** 
 * 类名称：ContractMaterialBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractMaterialBizImpl extends BaseBizImpl implements IContractMaterialBiz  {
	
	/**
	 * 根据主键获得合同物资信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractMaterial getContractMaterial(Long id) throws BaseException {
		return (ContractMaterial)this.getObject(ContractMaterial.class, id);
	}
	
	/**
	 * 获得合同物资信息表实例
	 * @param contractMaterial 合同物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractMaterial getContractMaterial(ContractMaterial contractMaterial) throws BaseException {
		return (ContractMaterial)this.getObject(ContractMaterial.class, contractMaterial.getCmId() );
	}
	
	/**
	 * 添加合同物资信息信息
	 * @param contractMaterial 合同物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void saveContractMaterial(ContractMaterial contractMaterial) throws BaseException{
		this.saveObject( contractMaterial ) ;
	}
	
	/**
	 * 更新合同物资信息表实例
	 * @param contractMaterial 合同物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void updateContractMaterial(ContractMaterial contractMaterial) throws BaseException{
		this.updateObject( contractMaterial ) ;
	}
	
	/**
	 * 删除合同物资信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractMaterial(String id) throws BaseException {
		this.removeObject( this.getContractMaterial( new Long(id) ) ) ;
	}
	
	/**
	 * 删除合同物资信息表实例
	 * @param contractMaterial 合同物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractMaterial(ContractMaterial contractMaterial) throws BaseException {
		this.removeObject( contractMaterial ) ;
	}
	
	/**
	 * 删除合同物资信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractMaterials(String[] id) throws BaseException {
		this.removeBatchObject(ContractMaterial.class, id) ;
	}
	
	/**
	 * 获得合同物资信息表数据集
	 * contractMaterial 合同物资信息表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractMaterial getContractMaterialByContractMaterial(ContractMaterial contractMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractMaterial de where 1 = 1 " );
		hql.append(" order by de.cmId desc ");
		List list = this.getObjects( hql.toString() );
		contractMaterial = new ContractMaterial();
		if(list!=null&&list.size()>0){
			contractMaterial = (ContractMaterial)list.get(0);
		}
		return contractMaterial;
	}
	
	/**
	 * 获得所有合同物资信息表数据集
	 * @param contractMaterial 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractMaterialList(ContractMaterial contractMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractMaterial de where 1 = 1 " );
		if(contractMaterial != null){
			if (StringUtil.isNotBlank(contractMaterial.getCiId())) {
				hql.append(" and de.ciId = ").append(contractMaterial.getCiId());
			}
		}
		hql.append(" order by de.cmId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有合同物资信息表数据集
	 * @param rollPage 分页对象
	 * @param contractMaterial 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractMaterialList(RollPage rollPage, ContractMaterial contractMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractMaterial de where 1 = 1 " );
		if(contractMaterial != null){
			if (StringUtil.isNotBlank(contractMaterial.getCiId())) {
				hql.append(" and de.ciId = ").append(contractMaterial.getCiId());
			}
		}
		hql.append(" order by de.cmId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 删除合同物资信息表实例    合同Id
	 * @param ciId 主键数组
	 * @throws BaseException 
	 */
	public void deleteContractMaterialByCiId(Long ciId) throws BaseException {
		String sql="delete from contract_material where ci_id="+ciId;
		this.updateJdbcSql(sql);		
	}
	/**
	 * 根据发货数量更新可订单明细中的发货数量
	 * @param sendAmount
	 * @param omId
	 * @throws BaseException
	 */
	public void updateSendAmount(Double sendAmount, Long cmId)
			throws BaseException {
		String sql="update contract_material set real_send_amount=real_send_amount+"+sendAmount+",wait_send_amount=wait_send_amount-"+sendAmount+" where cm_id="+cmId;
		//System.out.println(sql);
		this.updateJdbcSql(sql);
		
	}

	/**
	 * 根据收货数量更新可订单明细中的收货数量
	 * @param sendAmount
	 * @param omId
	 * @throws BaseException
	 */
	public void updateInputAmount(Double inputAmount, Long cmId)
			throws BaseException {
		String sql="update contract_material set real_input_amount=real_input_amount+"+inputAmount+",wait_input_amount=wait_input_amount-"+inputAmount+" where cm_id="+cmId;
		//System.out.println(sql);
		this.updateJdbcSql(sql);
		
	}
	
}
