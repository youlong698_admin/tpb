package com.ced.sip.contract.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.contract.biz.IContractTemplateBiz;
import com.ced.sip.contract.entity.ContractTemplate;
/** 
 * 类名称：ContractTemplateBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-07-19
 */
public class ContractTemplateBizImpl extends BaseBizImpl implements IContractTemplateBiz  {
	
	/**
	 * 根据主键获得合同模板表实例
	 * @param id 主键
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractTemplate getContractTemplate(Long id) throws BaseException {
		return (ContractTemplate)this.getObject(ContractTemplate.class, id);
	}
	
	/**
	 * 获得合同模板表实例
	 * @param contractTemplate 合同模板表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractTemplate getContractTemplate(ContractTemplate contractTemplate) throws BaseException {
		return (ContractTemplate)this.getObject(ContractTemplate.class, contractTemplate.getCtId() );
	}
	
	/**
	 * 添加合同模板信息
	 * @param contractTemplate 合同模板表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void saveContractTemplate(ContractTemplate contractTemplate) throws BaseException{
		this.saveObject( contractTemplate ) ;
	}
	
	/**
	 * 更新合同模板表实例
	 * @param contractTemplate 合同模板表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void updateContractTemplate(ContractTemplate contractTemplate) throws BaseException{
		this.updateObject( contractTemplate ) ;
	}
	
	/**
	 * 删除合同模板表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractTemplate(String id) throws BaseException {
		this.removeObject( this.getContractTemplate( new Long(id) ) ) ;
	}
	
	/**
	 * 删除合同模板表实例
	 * @param contractTemplate 合同模板表实例
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractTemplate(ContractTemplate contractTemplate) throws BaseException {
		this.removeObject( contractTemplate ) ;
	}
	
	/**
	 * 删除合同模板表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-07-19
	 * @throws BaseException 
	 */
	public void deleteContractTemplates(String[] id) throws BaseException {
		this.removeBatchObject(ContractTemplate.class, id) ;
	}
	
	/**
	 * 获得合同模板表数据集
	 * contractTemplate 合同模板表实例
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public ContractTemplate getContractTemplateByContractTemplate(ContractTemplate contractTemplate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractTemplate de where 1 = 1 " );

		hql.append(" order by de.ctId desc ");
		List list = this.getObjects( hql.toString() );
		contractTemplate = new ContractTemplate();
		if(list!=null&&list.size()>0){
			contractTemplate = (ContractTemplate)list.get(0);
		}
		return contractTemplate;
	}
	
	/**
	 * 获得所有合同模板表数据集
	 * @param contractTemplate 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractTemplateList(ContractTemplate contractTemplate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractTemplate de where 1 = 1 " );
		if(contractTemplate != null){
			if (StringUtil.isNotBlank(contractTemplate.getComId())) {
				hql.append(" and de.comId = ").append(contractTemplate.getComId()).append("");
			}
		}
		hql.append(" order by de.ctId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有合同模板表数据集
	 * @param rollPage 分页对象
	 * @param contractTemplate 查询参数对象
	 * @author luguanglei 2017-07-19
	 * @return
	 * @throws BaseException 
	 */
	public List getContractTemplateList(RollPage rollPage, ContractTemplate contractTemplate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ContractTemplate de where 1 = 1 " );
		if(contractTemplate != null){
			if (StringUtil.isNotBlank(contractTemplate.getComId())) {
				hql.append(" and de.comId = ").append(contractTemplate.getComId()).append("");
			}
		}
		hql.append(" order by de.ctId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
