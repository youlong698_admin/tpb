package com.ced.sip.expert.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.expert.biz.IExpertBiz;
import com.ced.sip.expert.entity.Expert;

public class ExpertBizImpl extends BaseBizImpl implements IExpertBiz {

	/**
	 * 根据主键获得专家表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public Expert getExpert(Long id) throws BaseException {
		return (Expert) this.getObject(Expert.class, id);
	}
	
	/**
	 * 获得所有专家表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getExpertList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Expert ex where 1 = 1 and ex.isUsable = 0 ");		
		hql.append(" order by ex.expertId desc ");
		return this.getObjects(rollPage,hql.toString());
	}
	
	/**
	 * 获得所有专家表数据集
	 * @param Expert 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getExpertList(Expert expert) throws BaseException {
		StringBuffer hql = new StringBuffer(" from Expert ex where 1 = 1 and ex.isUsable = 0 ");
		if (StringUtil.isNotBlank(expert)) {
			if (StringUtil.isNotBlank(expert.getExpertId())) {
		       hql.append(" and ex.expertId = '").append(expert.getExpertId()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getExpertLoginName())) {
			       hql.append(" and ex.expertLoginName = '").append(expert.getExpertLoginName()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getExpertPassword())) {
			       hql.append(" and ex.expertPassword = '").append(expert.getExpertPassword()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getExpertId())) {
			       hql.append(" and ex.expertId = '").append(expert.getExpertId()).append("'");
				}
			if (StringUtil.isNotBlank(expert.getExpertName())) {
				hql.append(" and ex.expertName like '%").append(expert.getExpertName()).append("%'");
			}
			if (StringUtil.isNotBlank(expert.getStatus())) {
				hql.append(" and ex.status = '").append(expert.getStatus()).append("'");
			}
		}
		hql.append(" order by ex.expertId desc ");
		return this.getObjects(hql.toString());
	}
	
	/**
	 * 获得所有专家表数据集
	 * @param rollPage 分页对象
	 * @param Expert 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getExpertList(RollPage rollPage, Expert expert)
		throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("ex.expertId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		
		StringBuffer hql = new StringBuffer(" from Expert ex where 1 = 1 and ex.isUsable = 0 ");
		if (StringUtil.isNotBlank(expert)) {
			if (StringUtil.isNotBlank(expert.getExpertName())) {
				hql.append(" and ex.expertName like '%").append(expert.getExpertName()).append("%'");
			}
			if (StringUtil.isNotBlank(expert.getDepartment())) {
				hql.append(" and ex.department = '").append(expert.getDepartment()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getStatus())) {
				hql.append(" and ex.status = '").append(expert.getStatus()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getExpertMajor())) {
				hql.append(" and ex.expertMajor like '%").append(expert.getExpertMajor()).append("%'");
			}
			if (StringUtil.isNotBlank(expert.getExpertLoginName())) {
				hql.append(" and ex.expertLoginName = '").append(expert.getExpertLoginName()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getInOut())) {
				hql.append(" and ex.inOut = '").append(expert.getInOut()).append("'");
			}
			if(StringUtil.isNotBlank(expert.getExpertType())){
				hql.append(" and ex.expertType = '").append(expert.getExpertType()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getComId())) {
				hql.append(" and ex.comId = ").append(expert.getComId()).append("");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		//System.out.println(hql);
		
		return this.getObjects(rollPage,hql.toString());
	}
	/**
	 * 获得所有专家表数据集
	 * @param rollPage 分页对象
	 * @param Expert 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getExpertListByRcId(RollPage rollPage, Expert expert,Long rcId)
		throws BaseException {
		//初始化排序
		if(rollPage.getOrderColumn()==null){
			rollPage.setOrderColumn("ex.expertId");
		}
		if( rollPage.getOrderDir()==null){
			rollPage.setOrderDir("desc");
		}
		
		StringBuffer hql = new StringBuffer(" from Expert ex where 1 = 1 and ex.isUsable = 0 ");
		if (StringUtil.isNotBlank(expert)) {
			if (StringUtil.isNotBlank(expert.getExpertName())) {
				hql.append(" and ex.expertName like '%").append(expert.getExpertName()).append("%'");
			}
			if (StringUtil.isNotBlank(expert.getDepartment())) {
				hql.append(" and ex.department = '").append(expert.getDepartment()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getStatus())) {
				hql.append(" and ex.status = '").append(expert.getStatus()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getExpertMajor())) {
				hql.append(" and ex.expertMajor like '%").append(expert.getExpertMajor()).append("%'");
			}
			if (StringUtil.isNotBlank(expert.getExpertLoginName())) {
				hql.append(" and ex.expertLoginName = '").append(expert.getExpertLoginName()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getInOut())) {
				hql.append(" and ex.inOut = '").append(expert.getInOut()).append("'");
			}
			if(StringUtil.isNotBlank(expert.getExpertType())){
				hql.append(" and ex.expertType = '").append(expert.getExpertType()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getComId())) {
				hql.append(" and ex.comId = ").append(expert.getComId()).append("");
			}
		}
		hql.append(" and not exists (select de.expertId from TenderBidJudge de where de.rcId="+rcId+" and de.expertId=ex.expertId) ");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		//System.out.println(hql);
		
		return this.getObjects(rollPage,hql.toString());
	}
	
	/**
	 * 添加专家信息
	 * @param Expert 专家表实例
	 * @throws BaseException 
	 */
	public void saveExpert(Expert expert) throws BaseException {
		this.saveObject(expert);
	}
	
	/**
	 * 删除专家表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteExpert(String id) throws BaseException {
		this.removeObject(this.getExpert(new Long(id)));
	}
	
	/**
	 * 删除专家表实例
	 * @param Expert 专家表实例
	 * @throws BaseException 
	 */
	public void deleteExpert(Expert expert) throws BaseException {
		this.removeObject(expert);
	}
	
	/**
	 * 删除专家表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteExperts(String[] id) throws BaseException {
		this.removeObject(Expert.class, id);
	}

	/**
	 * 更新专家表实例
	 * @param Expert 专家表实例
	 * @throws BaseException 
	 */
	public void updateExpert(Expert expert) throws BaseException {
		this.updateObject(expert);
	}

	/**
	 * 根据专家登录名获取专家中文名
	 * Ushine 2014-11-24
	 * @param expert
	 * @return
	 * @throws BaseException
	 */
	public List getExpertName(Expert expert) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de.expertName from Expert de where 1 = 1 ");
		if (StringUtil.isNotBlank(expert)) {
			if (StringUtil.isNotBlank(expert.getExpertLoginName())) {
				hql.append(" and de.expertLoginName = '").append(expert.getExpertLoginName()).append("'");
			}
			if (StringUtil.isNotBlank(expert.getExpertPassword())) {
				hql.append(" and de.expertPassword = '").append(expert.getExpertLoginName()).append("'");
			}
		}
		return this.getObjects(hql.toString());
	}

}
