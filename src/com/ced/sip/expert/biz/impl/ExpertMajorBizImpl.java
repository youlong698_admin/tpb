package com.ced.sip.expert.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.expert.biz.IExpertMajorBiz;
import com.ced.sip.expert.entity.ExpertMajor;
/** 
 * 类名称：ExpertMajorBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class ExpertMajorBizImpl extends BaseBizImpl implements IExpertMajorBiz  {
	
	/**
	 * 根据主键获得专家专业表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public ExpertMajor getExpertMajor(Long id) throws BaseException {
		return (ExpertMajor)this.getObject(ExpertMajor.class, id);
	}
	
	/**
	 * 获得专家专业表实例
	 * @param expertMajor 专家专业表实例
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public ExpertMajor getExpertMajor(ExpertMajor expertMajor) throws BaseException {
		return (ExpertMajor)this.getObject(ExpertMajor.class, expertMajor.getEmId() );
	}
	
	/**
	 * 添加专家专业信息
	 * @param expertMajor 专家专业表实例
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void saveExpertMajor(ExpertMajor expertMajor) throws BaseException{
		this.saveObject( expertMajor ) ;
	}
	
	/**
	 * 更新专家专业表实例
	 * @param expertMajor 专家专业表实例
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void updateExpertMajor(ExpertMajor expertMajor) throws BaseException{
		this.updateObject( expertMajor ) ;
	}
	
	/**
	 * 删除专家专业表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void deleteExpertMajor(String id) throws BaseException {
		this.removeObject( this.getExpertMajor( new Long(id) ) ) ;
	}
	
	/**
	 * 删除专家专业表实例
	 * @param expertMajor 专家专业表实例
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void deleteExpertMajor(ExpertMajor expertMajor) throws BaseException {
		this.removeObject( expertMajor ) ;
	}
	
	/**
	 * 删除专家专业表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-12
	 * @throws BaseException 
	 */
	public void deleteExpertMajors(String[] id) throws BaseException {
		this.removeBatchObject(ExpertMajor.class, id) ;
	}
	
	/**
	 * 获得专家专业表数据集
	 * expertMajor 专家专业表实例
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public ExpertMajor getExpertMajorByExpertMajor(ExpertMajor expertMajor) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ExpertMajor de where 1 = 1 " );

		hql.append(" order by de.emId desc ");
		List list = this.getObjects( hql.toString() );
		expertMajor = new ExpertMajor();
		if(list!=null&&list.size()>0){
			expertMajor = (ExpertMajor)list.get(0);
		}
		return expertMajor;
	}
	
	/**
	 * 获得所有专家专业表数据集
	 * @param expertMajor 查询参数对象
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public List getExpertMajorList(ExpertMajor expertMajor) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ExpertMajor de where 1 = 1 " );

		hql.append(" order by de.emId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有专家专业表数据集
	 * @param rollPage 分页对象
	 * @param expertMajor 查询参数对象
	 * @author luguanglei 2017-05-12
	 * @return
	 * @throws BaseException 
	 */
	public List getExpertMajorList(RollPage rollPage, ExpertMajor expertMajor) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ExpertMajor de where 1 = 1 " );

		hql.append(" order by de.emId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
