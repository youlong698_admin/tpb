package com.ced.sip.expert.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.expert.entity.ExpertMajor;
/** 
 * 类名称：IExpertMajorBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public interface IExpertMajorBiz {

	/**
	 * 根据主键获得专家专业表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ExpertMajor getExpertMajor(Long id) throws BaseException;

	/**
	 * 添加专家专业信息
	 * @param expertMajor 专家专业表实例
	 * @throws BaseException 
	 */
	abstract void saveExpertMajor(ExpertMajor expertMajor) throws BaseException;

	/**
	 * 更新专家专业表实例
	 * @param expertMajor 专家专业表实例
	 * @throws BaseException 
	 */
	abstract void updateExpertMajor(ExpertMajor expertMajor) throws BaseException;

	/**
	 * 删除专家专业表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteExpertMajor(String id) throws BaseException;

	/**
	 * 删除专家专业表实例
	 * @param expertMajor 专家专业表实例
	 * @throws BaseException 
	 */
	abstract void deleteExpertMajor(ExpertMajor expertMajor) throws BaseException;

	/**
	 * 删除专家专业表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteExpertMajors(String[] id) throws BaseException;

	/**
	 * 获得专家专业表数据集
	 * expertMajor 专家专业表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ExpertMajor getExpertMajorByExpertMajor(ExpertMajor expertMajor) throws BaseException ;
	
	/**
	 * 获得所有专家专业表数据集
	 * @param expertMajor 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getExpertMajorList(ExpertMajor expertMajor) throws BaseException ;
	
	/**
	 * 获得所有专家专业表数据集
	 * @param rollPage 分页对象
	 * @param expertMajor 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getExpertMajorList(RollPage rollPage, ExpertMajor expertMajor)
			throws BaseException;

}