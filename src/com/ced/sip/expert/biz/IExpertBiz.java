package com.ced.sip.expert.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.expert.entity.Expert;

public interface IExpertBiz {

	/**
	 * 根据主键获得专家表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Expert getExpert(Long id) throws BaseException;

	/**
	 * 添加专家信息
	 * @param Expert 专家表实例
	 * @throws BaseException 
	 */
	abstract void saveExpert(Expert expert) throws BaseException;

	/**
	 * 更新专家表实例
	 * @param Expert 专家表实例
	 * @throws BaseException 
	 */
	abstract void updateExpert(Expert expert) throws BaseException;

	/**
	 * 删除专家表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteExpert(String id) throws BaseException;

	/**
	 * 删除专家表实例
	 * @param Expert 专家表实例
	 * @throws BaseException 
	 */
	abstract void deleteExpert(Expert expert) throws BaseException;

	/**
	 * 删除专家表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteExperts(String[] id) throws BaseException;

	/**
	 * 获得所有专家表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getExpertList( RollPage rollPage  ) throws BaseException ;
	
	/**
	 * 获得所有专家表数据集
	 * @param Expert 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getExpertList(  Expert expert ) throws BaseException ;
	
	/**
	 * 获得所有专家表数据集
	 * @param rollPage 分页对象
	 * @param Expert 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getExpertList(RollPage rollPage, Expert expert)
			throws BaseException;
	/**
	 * 获得所有专家表数据集
	 * @param rollPage 分页对象  rcId
	 * @param Expert 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getExpertListByRcId(RollPage rollPage, Expert expert,Long rcId)
			throws BaseException;
	
	/**
	 * 根据专家登录名获取专家中文名
	 * Ushine 2014-11-24
	 * @param expert
	 * @return
	 * @throws BaseException
	 */
	abstract List getExpertName(Expert expert)
			throws BaseException;

}