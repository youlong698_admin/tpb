package com.ced.sip.expert.entity;

import java.util.Date;

/** 
 * 类名称：Expert
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class Expert implements java.io.Serializable {

	// 属性信息
	private Long expertId;     //主键
	private String expertName;	 //姓名
	private String expertSex;	 //性别
	private String identify;	 //身份证号
	private String inOut;	 //系统内外标志
	private String companyName;	 //工作单位
	private String department;	 //工作部门
	private String degree;	 //学历
	private String profession;	 //所学专业
	private String professionTime;	 //从事专业时间（年）
	private String expertMajor;	 //评标专业
	private String expertType;	 //专家类型
	private String titleTech;	 //职称
	private String titleHeadship;	 //职务
	private String phoneNumber;	 //固定电话
	private String mobilNumber;	 //移动电话
	private String faxNumber;	 //传真
	private String address;	 //通信地址
	private String email;	 //邮箱地址
	private String status;	 //当前状态
	private String stutusCn;	 //当前状态中文
	private Date lastEditTime;    //最后修改时间
	private Date cancleDate;    //注销时间
	private String writer;	 //登记人
	private Date writeDate;    //登记时间
	private String isUsable;	 //是否有效
	private String expertLoginName;	 //登录名
	private Long comId;     //
	private String expertPassword;
	
	private String degreeCn;
	private String titleTechCn;
	
	public Expert() {
		super();
	}
	
	public Long getExpertId(){
	   return  expertId;
	} 
	public void setExpertId(Long expertId) {
	   this.expertId = expertId;
    }     
	public String getExpertName(){
	   return  expertName;
	} 
	public void setExpertName(String expertName) {
	   this.expertName = expertName;
    }
	public String getExpertSex(){
	   return  expertSex;
	} 
	public void setExpertSex(String expertSex) {
	   this.expertSex = expertSex;
    }
	public String getIdentify(){
	   return  identify;
	} 
	public void setIdentify(String identify) {
	   this.identify = identify;
    }	    
	public String getInOut(){
	   return  inOut;
	} 
	public void setInOut(String inOut) {
	   this.inOut = inOut;
    }
	public String getCompanyName(){
	   return  companyName;
	} 
	public void setCompanyName(String companyName) {
	   this.companyName = companyName;
    }
	public String getDepartment(){
	   return  department;
	} 
	public void setDepartment(String department) {
	   this.department = department;
    }
	public String getDegree(){
	   return  degree;
	} 
	public void setDegree(String degree) {
	   this.degree = degree;
    }
	public String getProfession(){
	   return  profession;
	} 
	public void setProfession(String profession) {
	   this.profession = profession;
    }
	public String getProfessionTime(){
	   return  professionTime;
	} 
	public void setProfessionTime(String professionTime) {
	   this.professionTime = professionTime;
    }
	public String getExpertMajor(){
	   return  expertMajor;
	} 
	public void setExpertMajor(String expertMajor) {
	   this.expertMajor = expertMajor;
    }
	public String getExpertType(){
	   return  expertType;
	} 
	public void setExpertType(String expertType) {
	   this.expertType = expertType;
    }
	public String getTitleTech(){
	   return  titleTech;
	} 
	public void setTitleTech(String titleTech) {
	   this.titleTech = titleTech;
    }
	public String getTitleHeadship(){
	   return  titleHeadship;
	} 
	public void setTitleHeadship(String titleHeadship) {
	   this.titleHeadship = titleHeadship;
    }
	public String getPhoneNumber(){
	   return  phoneNumber;
	} 
	public void setPhoneNumber(String phoneNumber) {
	   this.phoneNumber = phoneNumber;
    }
	public String getMobilNumber(){
	   return  mobilNumber;
	} 
	public void setMobilNumber(String mobilNumber) {
	   this.mobilNumber = mobilNumber;
    }
	public String getFaxNumber(){
	   return  faxNumber;
	} 
	public void setFaxNumber(String faxNumber) {
	   this.faxNumber = faxNumber;
    }
	public String getAddress(){
	   return  address;
	} 
	public void setAddress(String address) {
	   this.address = address;
    }
	public String getEmail(){
	   return  email;
	} 
	public void setEmail(String email) {
	   this.email = email;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public String getStutusCn(){
	   return  stutusCn;
	} 
	public void setStutusCn(String stutusCn) {
	   this.stutusCn = stutusCn;
    }
	public Date getLastEditTime(){
	   return  lastEditTime;
	} 
	public void setLastEditTime(Date lastEditTime) {
	   this.lastEditTime = lastEditTime;
    }	    
	public Date getCancleDate(){
	   return  cancleDate;
	} 
	public void setCancleDate(Date cancleDate) {
	   this.cancleDate = cancleDate;
    }	    
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getIsUsable(){
	   return  isUsable;
	} 
	public void setIsUsable(String isUsable) {
	   this.isUsable = isUsable;
    }
	public String getExpertLoginName(){
	   return  expertLoginName;
	} 
	public void setExpertLoginName(String expertLoginName) {
	   this.expertLoginName = expertLoginName;
    }
	public Long getComId(){
	   return  comId;
	} 
	public void setComId(Long comId) {
	   this.comId = comId;
    }

	public String getDegreeCn() {
		return degreeCn;
	}

	public void setDegreeCn(String degreeCn) {
		this.degreeCn = degreeCn;
	}

	public String getTitleTechCn() {
		return titleTechCn;
	}

	public void setTitleTechCn(String titleTechCn) {
		this.titleTechCn = titleTechCn;
	}

	public String getExpertPassword() {
		return expertPassword;
	}

	public void setExpertPassword(String expertPassword) {
		this.expertPassword = expertPassword;
	}     
}