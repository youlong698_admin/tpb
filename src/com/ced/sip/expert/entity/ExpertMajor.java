package com.ced.sip.expert.entity;

/** 
 * 类名称：ExpertMajor
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class ExpertMajor implements java.io.Serializable {

	// 属性信息
	private Long emId;     //专业主键
	private Long expertId;     //专家主键
	private Long ejId;     //专业主键
	
	
	public ExpertMajor() {
		super();
	}
	
	public Long getEmId(){
	   return  emId;
	} 
	public void setEmId(Long emId) {
	   this.emId = emId;
    }     
	public Long getExpertId(){
	   return  expertId;
	} 
	public void setExpertId(Long expertId) {
	   this.expertId = expertId;
    }     
	public Long getEjId(){
	   return  ejId;
	} 
	public void setEjId(Long ejId) {
	   this.ejId = ejId;
    }     
}