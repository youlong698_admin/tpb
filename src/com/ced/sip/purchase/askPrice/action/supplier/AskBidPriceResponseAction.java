package com.ced.sip.purchase.askPrice.action.supplier;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.beanutils.BeanUtils;

import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.common.utils.encrypt.Base64;
import com.ced.sip.common.utils.encrypt.RSAEncrypt;
import com.ced.sip.purchase.askPrice.biz.IAskBidListBiz;
import com.ced.sip.purchase.askPrice.biz.IAskReceivedBulletinBiz;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
import com.ced.sip.purchase.askPrice.entity.AskReceivedBulletin;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseBiz;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceHistoryBiz;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
import com.ced.sip.purchase.base.entity.BidBusinessResponseHistory;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
import com.ced.sip.purchase.base.entity.BidPriceDetailHistory;
import com.ced.sip.purchase.base.entity.BidPriceHistory;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Users;
/** 
 * 类名称：BidPriceAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class AskBidPriceResponseAction extends BaseAction {
	//采购计划明细
	private IRequiredCollectBiz iRequiredCollectBiz;
	//商务响应项
	private IBusinessResponseItemBiz iBusinessResponseItemBiz;
	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
    //询价基本信息
	private IAskBidListBiz iAskBidListBiz;
	//回标信息
	private IAskReceivedBulletinBiz iAskReceivedBulletinBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	// 供应商报价明细信息 
	private IBidPriceDetailBiz iBidPriceDetailBiz;
	// 供应商历史报价信息 
	private IBidPriceHistoryBiz iBidPriceHistoryBiz;
	// 供应商历史报价明细信息 
	private IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz;
	// 供应商商务响应 
	private IBidBusinessResponseBiz iBidBusinessResponseBiz;
	// 供应商商务响应 
	private IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz;
		
	// 供应商报价信息
	private BidPrice bidPrice;
	// 供应商报价信息明细
	private BidPriceDetail bidPriceDetail;
	// 供应商报价历史信息
	private BidPriceHistory bidPriceHistory;
	// 供应商报价历史明细信息
	private BidPriceDetailHistory bidPriceDetailHistory;
	// 供应商报价信息
	private List<BidPriceDetail> bpdList;
	// 商务响应项信息
	private BidBusinessResponse bidBusinessResponse;
	// 商务响应项历史信息
	private BidBusinessResponseHistory bidBusinessResponseHistory;
	// 商务响应项信息
	private List<BidBusinessResponse> bbrList;
	// 回标信息
	private AskReceivedBulletin askReceivedBulletin;
	
	private AskBidList askBidList;
	
	private RequiredCollectDetail requiredCollectDetail;
	
	private Long rcId;
	
	
	/**
	 * 查看供应商历史报价信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewAskBidPriceResponseMonitor() throws BaseException {
		String initPage="viewAskBidPriceResponse";
		try{
			String from=this.getRequest().getParameter("from");
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			bidPriceHistory=new BidPriceHistory();
			bidPriceHistory.setRcId(rcId);
			bidPriceHistory.setSupplierId(supplierId);
			List list=this.iBidPriceHistoryBiz.getBidPriceHistoryList(bidPriceHistory);
			this.setListValue(list);
			
			askBidList=this.iAskBidListBiz.getAskBidListByRcId(rcId);
			boolean isReturnTime=false;
			//判断当时时间是否大于回标时间
	        Calendar calendar = Calendar.getInstance(Locale.CHINA);
	        Date date = calendar.getTime();
	        if(askBidList.getReturnDate()!=null){
			if(date.getTime()<askBidList.getReturnDate().getTime()){
				isReturnTime=true;
			}}	
	        this.getRequest().setAttribute("isReturnTime", isReturnTime);
	        
	        InviteSupplier inviteSupplier=new InviteSupplier();
		    inviteSupplier.setRcId(rcId);
		    inviteSupplier.setSupplierId(supplierId);
		    inviteSupplier=this.iInviteSupplierBiz.getInviteSupplierByInviteSupplier(inviteSupplier);
            this.getRequest().setAttribute("isSms", inviteSupplier.getIsSms()); 
          //触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log.error("查看供应商报价信息信息列表错误！", e);
			throw new BaseException("查看供应商报价信息信息列表错误！", e);
		}
		
		return initPage;
		
	}
	
	/**
	 * 保存供应商报价信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitAskBidPriceRespone() throws BaseException {
		String initPage="addInit";
		try{
		  String from=this.getRequest().getParameter("from");
		  Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
		  askBidList=this.iAskBidListBiz.getAskBidListByRcId(rcId);
          askBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		  askBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
		  
		  InviteSupplier inviteSupplier=new InviteSupplier();
		  inviteSupplier.setRcId(rcId);
		  inviteSupplier.setSupplierId(supplierId);
		  inviteSupplier=this.iInviteSupplierBiz.getInviteSupplierByInviteSupplier(inviteSupplier);
		  
		  List<RequiredCollectDetail> rcdlist=this.iRequiredCollectBiz.getRequiredCollectDetailList(rcId);
		  BusinessResponseItem businessResponseItem=new BusinessResponseItem();
		  businessResponseItem.setRcId(rcId);
		  List<BusinessResponseItem> briList=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
		  
		  this.getRequest().setAttribute("rcdlist", rcdlist);
		  this.getRequest().setAttribute("briList", briList);
		  this.getRequest().setAttribute("supplierId", supplierId);
		  this.getRequest().setAttribute("askBidList", askBidList);
		  this.getRequest().setAttribute("publicKey", inviteSupplier.getPublicKey());
          //触屏版使用
          if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("保存供应商报价信息信息初始化错误！", e);
			throw new BaseException("保存供应商报价信息信息初始化错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 加密报价
	 * @return
	 * @throws BaseException 
	 */
	public void encryPrice() throws BaseException {

		JSONObject jsonObj = new JSONObject();
		PrintWriter writer=null;
		try{			
			writer = getResponse().getWriter();  
	        String publicKey=this.getRequest().getParameter("publicKey");
			String prices=this.getRequest().getParameter("prices");
			byte[] cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPublicKeyByStr(publicKey),prices.getBytes());
			String cipher=Base64.encode(cipherData);
			jsonObj.put("data", cipher); 
		} catch (Exception e) {
			log("保存供应商报价信息信息错误！", e);
			jsonObj.put("data", "");
		}	
			writer.print(jsonObj);  
	        writer.flush();  
	        writer.close();	
	}
	/**
	 * 保存供应商报价信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveAskBidPriceRespone() throws BaseException {
		String initPage="success";
		try{
			String from=this.getRequest().getParameter("from");
			SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(getRequest()) ;
			Date date=DateUtil.getCurrentDateTime();	
			//更新邀请供应商表中的是否报价字段
			this.iInviteSupplierBiz.updateIsPriceAaByRcIdAndSupplierId(rcId, supplierInfo.getSupplierId());
			
			//判断回标表,如果没有数据,插入一条
			askReceivedBulletin=new AskReceivedBulletin();
			askReceivedBulletin.setSupplierId(supplierInfo.getSupplierId());
			askReceivedBulletin.setRcId(bidPrice.getRcId());
			List<AskReceivedBulletin> list=this.iAskReceivedBulletinBiz.getAskReceivedBulletinList(askReceivedBulletin);
			if(list.size()==0){
				askReceivedBulletin.setIsReplay(TableStatus.COMMON_0);
				askReceivedBulletin.setReceivedDate(date);
				askReceivedBulletin.setSupplierName(supplierInfo.getSupplierName());
				askReceivedBulletin.setReceivedName(supplierInfo.getContactPerson());
				askReceivedBulletin.setReceivedTel(supplierInfo.getMobilePhone());
				askReceivedBulletin.setWriter(supplierInfo.getSupplierLoginName());
				askReceivedBulletin.setWriteDate(date);
				this.iAskReceivedBulletinBiz.saveAskReceivedBulletin(askReceivedBulletin);
			}
			//删除旧的报价信息
			this.iBidPriceBiz.deleteBidPriceByRcIdAndSupplierId(bidPrice.getRcId(),supplierInfo.getSupplierId());
			
			//保存新的报价信息
			bidPrice.setWriter(supplierInfo.getSupplierLoginName());
			bidPrice.setWriteDate(date);
			this.iBidPriceBiz.saveBidPrice(bidPrice);
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetail.setBpId(bidPrice.getBpId());
					bidPriceDetail.setRcId(bidPrice.getRcId());
					this.iBidPriceDetailBiz.saveBidPriceDetail(bidPriceDetail);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponse.setBpId(bidPrice.getBpId());
					this.iBidBusinessResponseBiz.saveBidBusinessResponse(bidBusinessResponse);
				}
			}
			
			//保存新的报价历史信息
			bidPriceHistory=new BidPriceHistory();
			BeanUtils.copyProperties(bidPriceHistory, bidPrice);
			bidPriceHistory.setTotalPrice(null);
			this.iBidPriceHistoryBiz.saveBidPriceHistory(bidPriceHistory);
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetailHistory=new BidPriceDetailHistory();
					BeanUtils.copyProperties(bidPriceDetailHistory, bidPriceDetail);
					bidPriceDetailHistory.setPrice(null);
					bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
					bidPriceDetailHistory.setRcId(bidPriceHistory.getRcId());
					this.iBidPriceDetailHistoryBiz.saveBidPriceDetailHistory(bidPriceDetailHistory);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponseHistory=new BidBusinessResponseHistory();
					BeanUtils.copyProperties(bidBusinessResponseHistory, bidBusinessResponse);
					bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
					this.iBidBusinessResponseHistoryBiz.saveBidBusinessResponseHistory(bidBusinessResponseHistory);
				}
			}
			this.getRequest().setAttribute("message", "报价成功");
			this.getRequest().setAttribute("operModule", "供应商报价信息");
	         //触屏版使用
	        if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("保存供应商报价信息信息错误！", e);
			throw new BaseException("保存供应商报价信息信息错误！", e);
		}
		
		return initPage;
		
	}
	
	/**
	 * 查看供应商报价信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewAskBidPriceResponeDetail() throws BaseException {
		String initPage="detail";
		try{	

			String from=this.getRequest().getParameter("from");
			bidPriceHistory=this.iBidPriceHistoryBiz.getBidPriceHistory(bidPriceHistory.getBphId() );
			
			askBidList=this.iAskBidListBiz.getAskBidListByRcId(bidPriceHistory.getRcId());
            askBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		    askBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
		    List<BidPriceDetailHistory> bpdhList=new ArrayList<BidPriceDetailHistory>();
			bidPriceDetailHistory=new BidPriceDetailHistory();
			bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
			List<Object[]> objectList=this.iBidPriceDetailHistoryBiz.getBidPriceDetailHistoryListSupplier(bidPriceDetailHistory);
			for(Object[] object:objectList){
				bidPriceDetailHistory=(BidPriceDetailHistory)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				bidPriceDetailHistory.setRequiredCollectDetail(requiredCollectDetail);
				bpdhList.add(bidPriceDetailHistory);
			}
				
				
			bidBusinessResponseHistory=new BidBusinessResponseHistory();
			bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
			List<BidBusinessResponseHistory> bbrhList=this.iBidBusinessResponseHistoryBiz.getBidBusinessResponseHistoryList(bidBusinessResponseHistory);
			
			this.getRequest().setAttribute("bidPriceHistory", bidPriceHistory);			
			this.getRequest().setAttribute("bpdhList", bpdhList);			
			this.getRequest().setAttribute("bbrhList", bbrhList);			
			this.getRequest().setAttribute("askBidList", askBidList);
			//触屏版使用
            if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		} catch (Exception e) {
			log("查看供应商报价信息明细信息错误！", e);
			throw new BaseException("查看供应商报价信息明细信息错误！", e);
		}
		return initPage;
		
	}
	/**
	 * 短信邮件告知项目负责人供应商已报价
	 * @return
	 * @throws BaseException
	 */
    public String saveSmsAskBidPriceRespone() throws BaseException{
    	int result=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(getRequest()) ;
            
            RequiredCollect requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
            
            //更新邀请供应商表中的是否短信发送字段
			this.iInviteSupplierBiz.updateIsSmsByRcIdAndSupplierId(rcId, supplierInfo.getSupplierId());

			askBidList=this.iAskBidListBiz.getAskBidListByRcId(rcId);
			Users user=BaseDataInfosUtil.convertLoginNameToUsers(askBidList.getWriter());
			String subject="邮件提醒："+supplierInfo.getSupplierName()+"对"+requiredCollect.getBuyRemark()+"项目进行报价。",content=""+requiredCollect.getBuyRemark()+"("+requiredCollect.getBidCode()+")供应商已报价，供应商名称："+supplierInfo.getSupplierName()+"";
			this.saveSendMailToUsersSupplier(user.getEmail(), subject, content, supplierInfo.getSupplierName());
			
			String messageContent=""+supplierInfo.getSupplierName()+"对"+requiredCollect.getBuyRemark()+"项目进行报价。";
			this.saveSmsMessageToUsersSupplier(askBidList.getResponsiblePhone(), messageContent, supplierInfo.getSupplierName());
			
			result=1;
			out.print(result);
		} catch (Exception e) {
			log("短信邮件告知项目负责人供应商已投标！", e);
			throw new BaseException("短信邮件告知项目负责人供应商已投标", e);
		}
		return null;    	
    }
	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}

	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}

	public BidPrice getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(BidPrice bidPrice) {
		this.bidPrice = bidPrice;
	}

	public IAskBidListBiz getiAskBidListBiz() {
		return iAskBidListBiz;
	}

	public void setiAskBidListBiz(IAskBidListBiz iAskBidListBiz) {
		this.iAskBidListBiz = iAskBidListBiz;
	}

	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}

	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}

	public IBidPriceHistoryBiz getiBidPriceHistoryBiz() {
		return iBidPriceHistoryBiz;
	}

	public void setiBidPriceHistoryBiz(IBidPriceHistoryBiz iBidPriceHistoryBiz) {
		this.iBidPriceHistoryBiz = iBidPriceHistoryBiz;
	}

	public IBidPriceDetailHistoryBiz getiBidPriceDetailHistoryBiz() {
		return iBidPriceDetailHistoryBiz;
	}

	public void setiBidPriceDetailHistoryBiz(
			IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz) {
		this.iBidPriceDetailHistoryBiz = iBidPriceDetailHistoryBiz;
	}

	public IBidBusinessResponseBiz getiBidBusinessResponseBiz() {
		return iBidBusinessResponseBiz;
	}

	public void setiBidBusinessResponseBiz(
			IBidBusinessResponseBiz iBidBusinessResponseBiz) {
		this.iBidBusinessResponseBiz = iBidBusinessResponseBiz;
	}

	public List<BidPriceDetail> getBpdList() {
		return bpdList;
	}

	public void setBpdList(List<BidPriceDetail> bpdList) {
		this.bpdList = bpdList;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IBusinessResponseItemBiz getiBusinessResponseItemBiz() {
		return iBusinessResponseItemBiz;
	}

	public void setiBusinessResponseItemBiz(
			IBusinessResponseItemBiz iBusinessResponseItemBiz) {
		this.iBusinessResponseItemBiz = iBusinessResponseItemBiz;
	}

	public List<BidBusinessResponse> getBbrList() {
		return bbrList;
	}

	public void setBbrList(List<BidBusinessResponse> bbrList) {
		this.bbrList = bbrList;
	}

	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}

	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}

	public IAskReceivedBulletinBiz getiAskReceivedBulletinBiz() {
		return iAskReceivedBulletinBiz;
	}

	public void setiAskReceivedBulletinBiz(
			IAskReceivedBulletinBiz iAskReceivedBulletinBiz) {
		this.iAskReceivedBulletinBiz = iAskReceivedBulletinBiz;
	}

	public IBidBusinessResponseHistoryBiz getiBidBusinessResponseHistoryBiz() {
		return iBidBusinessResponseHistoryBiz;
	}

	public void setiBidBusinessResponseHistoryBiz(
			IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz) {
		this.iBidBusinessResponseHistoryBiz = iBidBusinessResponseHistoryBiz;
	}

	public BidPriceHistory getBidPriceHistory() {
		return bidPriceHistory;
	}

	public void setBidPriceHistory(BidPriceHistory bidPriceHistory) {
		this.bidPriceHistory = bidPriceHistory;
	}
	
}
