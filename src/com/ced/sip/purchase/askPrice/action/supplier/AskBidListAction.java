package com.ced.sip.purchase.askPrice.action.supplier;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.common.utils.encrypt.RSAEncrypt;
import com.ced.sip.purchase.askPrice.biz.IAskBidListBiz;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Users;

public class AskBidListAction extends BaseAction {
	 //询价信息服务类
	private IAskBidListBiz iAskBidListBiz;
	 //邀请供应商服务类
	private IInviteSupplierBiz iInviteSupplierBiz;
	 //项目信息服务类
	private IRequiredCollectBiz iRequiredCollectBiz;
	private Long rcId;
	
	private RequiredCollect requiredCollect;
	private AskBidList askBidList;
	private InviteSupplier inviteSupplier;
    
	/**
	 * 询价项目报名初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitAskBidListApplication() throws BaseException {
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			askBidList = this.iAskBidListBiz.getAskBidListByRcId(rcId);
			askBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
			askBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
			
			inviteSupplier = new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			inviteSupplier.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
			List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
            boolean isApplication=false;
			if(sulist.size()>0) isApplication=true;
			
			boolean isApplicationDate=false;
			//判断当时时间是否大于截标时间
			Date d=new Date();
			if(askBidList.getReturnDate()!=null){
			if(d.getTime()<=askBidList.getReturnDate().getTime()){
				isApplicationDate=true;
			}}	

			this.getRequest().setAttribute("isApplicationDate", isApplicationDate);
			this.getRequest().setAttribute("isApplication", isApplication);
		} catch (Exception e) {
			log("询价项目报名初始化页面错误！", e);
			throw new BaseException("询价项目报名初始化页面错误！", e);
		}
		return "saveInitAskBidListApplication";
	}
	/**
	 * 询价项目供应商报名
	 * @return
	 * @throws BaseException
	 */
    public String saveAskBidListApplication() throws BaseException{
    	String from="";
    	try {
    		from=this.getRequest().getParameter("from");
    		InviteSupplier inviteSupplier = new InviteSupplier();
    		inviteSupplier.setRcId(rcId);
    		SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(this.getRequest());
			inviteSupplier.setSupplierId(supplierInfo.getSupplierId());
    		List<InviteSupplier> list=this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
    		if(list.size()==0){
			inviteSupplier.setSupplierName(supplierInfo.getSupplierName());
			inviteSupplier.setWriteDate(DateUtil.getCurrentDateTime());
			inviteSupplier.setWriter(supplierInfo.getSupplierLoginName());
			inviteSupplier.setSupplierEmail(supplierInfo.getContactEmail());
			inviteSupplier.setSupplierPhone(supplierInfo.getMobilePhone());
			inviteSupplier.setPriceNum( new Long(0));
			inviteSupplier.setSourceCategory(2);
			Map<String,String> map=RSAEncrypt.genKeyPair();
			String publicKey=map.get("publicKey");
			String privateKey=map.get("privateKey");
			inviteSupplier.setPrivateKey(privateKey);
			inviteSupplier.setPublicKey(publicKey);
			this.iInviteSupplierBiz.saveInviteSupplier(inviteSupplier); 			

			this.iInviteSupplierBiz.updateInviteSupplierSourceCategory(inviteSupplier.getIsId(),inviteSupplier.getSupplierId(),rcId);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			askBidList=this.iAskBidListBiz.getAskBidListByRcId(rcId);
			Users user=BaseDataInfosUtil.convertLoginNameToUsers(askBidList.getWriter());
			String subject="邮件提醒："+supplierInfo.getSupplierName()+"报名"+requiredCollect.getBuyRemark()+"项目。",content=""+requiredCollect.getBuyRemark()+"("+requiredCollect.getBidCode()+")供应商报名，供应商名称："+supplierInfo.getSupplierName()+"";
			this.saveSendMailToUsersSupplier(user.getEmail(), subject, content, supplierInfo.getSupplierName());
			
			String messageContent=""+supplierInfo.getSupplierName()+"报名"+requiredCollect.getBuyRemark()+"项目。";
			this.saveSmsMessageToUsersSupplier(askBidList.getResponsiblePhone(), messageContent, supplierInfo.getSupplierName());
    		}
			if(StringUtil.isNotBlank(from)){
				PrintWriter out = this.getResponse().getWriter();
				out.print("success");
			}
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "供应商报名成功");
		} catch (Exception e) {
			log("询价项目供应商报名错误！", e);
			throw new BaseException("询价项目供应商报名错误！", e);
		}
		if(StringUtil.isNotBlank(from)) return null;
		else return saveInitAskBidListApplication();
    }
    /**
     * 进入我的询价项目页面
     * @return
     * @throws BaseException
     */
    public String viewAskBidListMyMonitor() throws BaseException{
    	try {
    		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
		} catch (Exception e) {
			log("进入我的询价项目页面错误！", e);
			throw new BaseException("进入我的询价项目页面错误！", e);
		}
    	return "viewAskBidListMyMonitor";
    }    
    /**
	 * 项目基本信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewAskBidListDetailMonitor() throws BaseException {
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			askBidList = new AskBidList();
			askBidList.setRcId(rcId);
			List<AskBidList> list=this.iAskBidListBiz.getAskBidListList(askBidList);
			askBidList = list.get(0);
			askBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
			askBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(askBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
			
		} catch (Exception e) {
			log("项目基本信息初始化页面错误！", e);
			throw new BaseException("项目基本信息初始化页面错误！", e);
		}
		return "viewAskBidListDetail";
	}
	public IAskBidListBiz getiAskBidListBiz() {
		return iAskBidListBiz;
	}
	public void setiAskBidListBiz(IAskBidListBiz iAskBidListBiz) {
		this.iAskBidListBiz = iAskBidListBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public AskBidList getAskBidList() {
		return askBidList;
	}

	public void setAskBidList(AskBidList askBidList) {
		this.askBidList = askBidList;
	}

 	
	
}
