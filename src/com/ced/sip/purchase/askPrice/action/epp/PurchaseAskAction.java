package com.ced.sip.purchase.askPrice.action.epp;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.util.AskProgressStatus;
import com.ced.sip.purchase.askPrice.util.AskProgressStatusList;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.requirement.biz.IRequiredMaterialBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.SystemConfiguration;

public class PurchaseAskAction extends BaseAction {
	// 计划
	private IRequiredMaterialBiz iRequiredMaterialBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	//需求汇总分包
	private IRequiredCollectBiz iRequiredCollectBiz;
	//日志服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	// BidMonitor 标段流程记录表
	private IBidProcessLogBiz iBidProcessLogBiz;

	private RequiredCollect requiredCollect;
	private RequiredCollectDetail requiredCollectDetail;
	private List<RequiredCollectDetail> rcdList;
	
	private String ids;
	private Departments departments;
	private PurchaseRecordLog purchaseRecordLog;
	private BidProcessLog bidProcessLog;

	
	/*********************************************************** 询价采购项目开始 **********************************************************/
	/**
	 * 查看询价采购项目列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewRequiredCollect() throws BaseException {
		String page=VIEW;
		try{
			String from=this.getRequest().getParameter("from");
			String buyRemark=this.getRequest().getParameter("buyRemark");
		    if(StringUtil.isNotBlank(from)) page+="Mobile";
		    this.getRequest().setAttribute("buyRemark", buyRemark);
		} catch (Exception e) {
			log.error("查看询价采购项目信息列表错误！", e);
			throw new BaseException("查看询价采购项目信息列表错误！", e);
		}
		
		return page ;
		
	}
	
	/**
	 * 查看询价采购项目信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findRequiredCollect() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			// 汇总 列表信息
			requiredCollect = new RequiredCollect();
			String bidCode=this.getRequest().getParameter("bidCode");
			requiredCollect.setBidCode(bidCode);
			String buyRemark=this.getRequest().getParameter("buyRemark");
			requiredCollect.setBuyRemark(buyRemark);
			
			requiredCollect.setBuyWay(TableStatus.PURCHASE_WAY_01);
			
			String rmCode=this.getRequest().getParameter("rmCode");
			if(StringUtil.isNotBlank(rmCode))
			{
				List<RequiredCollect> list=this.iRequiredCollectBiz.getRequiredCollecListByRmCode(rmCode);
				String condtion=" and (de.rc_id='1' ";
				for(RequiredCollect rc:list){
					condtion+="or de.rc_id='"+rc.getRcId()+"'";
				}
				condtion+=")";
				requiredCollect.setCondition(condtion);
			}
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			
			this.setListValue( this.iRequiredCollectBiz.getRequiredCollectList( this.getRollPageDataTables(), requiredCollect,sqlStr) ) ;
			if(this.getListValue()!=null&&this.getListValue().size()>0){
				for(int i=0;i<this.getListValue().size();i++){
					requiredCollect = (RequiredCollect) this.getListValue().get(i);
					requiredCollect.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
					requiredCollect.setPurchaseDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredCollect
									.getPurchaseDeptId()));
				}
			}
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看询价采购项目信息列表错误！", e);
			throw new BaseException("查看询价采购项目信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 项目立项信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveRequiredCollectInit() throws BaseException {
		try{
			this.getRequest().setAttribute("now",DateUtil.getCurrentDateTime());
			departments=UserRightInfoUtil.getDepartments(this.getRequest());
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(departments.getPurchaseDeptId()));
			this.getRequest().setAttribute("purchaseDeptId",departments.getPurchaseDeptId());
			this.getRequest().setAttribute("writerCn",UserRightInfoUtil.getChineseName(this.getRequest()));
			this.getRequest().setAttribute("writer",UserRightInfoUtil.getUserName(this.getRequest()));
			this.getRequest().setAttribute("deptName",departments.getDeptName());
			this.getRequest().setAttribute("deptId",departments.getDepId());
			this.getRequest().setAttribute("supplierType", TableStatusMap.supplierType);
		} catch (Exception e) {
			log("项目立项信息初始化错误！", e);
			throw new BaseException("项目立项信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	/**
	 * 修改汇总分包信息
	 * @return
	 * @throws BaseException
	 */
	public String saveRequiredCollect() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);	
			if (requiredCollect.getRcId() == null) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
				String bidCodePrefix = systemConfiguration.getRequiredCollectPrefix()+"-"+sdf.format(new Date());
				int floatCode = this.iRequiredCollectBiz.getMaxFloatCodeByPrefixStr(bidCodePrefix);
				requiredCollect.setBidCode(bidCodePrefix+"-" +(new DecimalFormat("0000").format( floatCode + 1 ) ));
				requiredCollect.setWriteDate(new Date());
				requiredCollect.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				requiredCollect.setFloatCode( (long)floatCode + 1);
				requiredCollect.setBuyWay(TableStatus.PURCHASE_WAY_01);
				requiredCollect.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01);
				requiredCollect.setServiceStatus(AskProgressStatus.Progress_Status_20);
				requiredCollect.setServiceStatusCn(AskProgressStatus.Progress_Status_20_Text);
				requiredCollect.setBidStatus(TableStatus.BID_STATUS_1);
				requiredCollect.setBidStatusCn(TableStatus.BID_STATUS_1_TEXT);
				requiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_1);
				requiredCollect.setComId(comId);
				requiredCollect.setOpenStatus(TableStatus.OPEN_STATUS_01);
				this.iRequiredCollectBiz.saveRequiredCollect(requiredCollect);
				
				bidProcessLog=new BidProcessLog();
			    bidProcessLog.setRcId(requiredCollect.getRcId());
				bidProcessLog.setBidNode(AskProgressStatus.Progress_Status_20);
				bidProcessLog.setReceiveDate(new Date());
				this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
				    
			} else {
				iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
				// 删除附件
				iAttachmentBiz
						.deleteAttachments(parseAttachIds(requiredCollect
								.getAttIds()));
			}
			// 保存附件
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					requiredCollect, requiredCollect.getRcId(),
					AttachmentStatus.ATTACHMENT_CODE_103, UserRightInfoUtil
							.getUserName(this.getRequest())));
			//删除老数据
			List<RequiredCollectDetail> oldRcdList = this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId());
			for(RequiredCollectDetail rcd:oldRcdList){
				this.iRequiredMaterialBiz.updateRequiredMaterialYamountByRcdId(rcd.getRmdId(),rcd.getAmount(),0);
                this.iRequiredCollectBiz.deleteRequiredCollectDetail(rcd);
	         }
			
			// 保存新数据
			if (rcdList != null) {
				for (int i = 0; i < rcdList.size(); i++) {
					requiredCollectDetail = rcdList.get(i);
					if (requiredCollectDetail != null
							&& StringUtil.isNotBlank(requiredCollectDetail.getBuyCode().trim())) {
						requiredCollectDetail.setBidCode(requiredCollect.getBidCode());
						requiredCollectDetail.setRcId(requiredCollect.getRcId());
						requiredCollectDetail.setRmdId(requiredCollectDetail.getRmdId());
						requiredCollectDetail.setIsCollect(TableStatus.COLLECT_TYPE_1);
						iRequiredCollectBiz.saveRequiredCollectDetail(requiredCollectDetail);
						
						iRequiredMaterialBiz.updateRequiredMaterialYamountByRcdId(requiredCollectDetail.getRmdId(), requiredCollectDetail.getAmount(),1);
					}
				}
			}
			    
			String operateContent = "询价采购项目成功，项目编号为【"+requiredCollect.getBidCode()+"】";
			purchaseRecordLog = new PurchaseRecordLog(null, null,requiredCollect.getRcId(), UserRightInfoUtil.getUserId(this.getRequest())+"", UserRightInfoUtil.getUserName(this.getRequest()), DateUtil.getCurrentDateTime(), operateContent,"创建询价采购项目");
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			
			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "新增询价采购项目信息");
			
		    this.getRequest().setAttribute("requiredcollect01Workflow", systemConfiguration.getRequiredcollect01Workflow());
		    this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.RC_WorkFlow_Type);
		} catch (Exception e) {
			log("新增询价采购项目信息错误！", e);
			throw new BaseException("新增询价采购项目信息错误！", e);
		}
		return "requiredCollectSuccess";
	}

	/**
	 * 修改询价采购项目初始化
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateRequiredCollectInit() throws BaseException {
		try {
			requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
			// 获取附件
			Map<String, Object> map = iAttachmentBiz
					.getAttachmentMap(new Attachment(
							requiredCollect.getRcId(),
							AttachmentStatus.ATTACHMENT_CODE_103));
			requiredCollect.setUuIdData((String) map.get("uuIdData"));
			requiredCollect.setFileNameData((String) map.get("fileNameData"));
			requiredCollect.setFileTypeData((String) map.get("fileTypeData"));
			requiredCollect.setAttIdData((String) map.get("attIdData"));
			requiredCollect.setAttIds((String) map.get("attIds"));
			
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
			this.getRequest().setAttribute("purchaseWay", TableStatusMap.purchaseWay);
			this.getRequest().setAttribute("supplierType", TableStatusMap.supplierType);
			
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));

		} catch (Exception e) {
			log("修改询价采购项目信息初始化错误！", e);
			throw new BaseException("修改询价采购项目信息初始化错误！", e);
		}
		return MODIFY_INIT;
	}
	/**
	 * 删除立项
	 */
	public String deleteRequiredCollect() throws BaseException {
		try {
			ids = this.getRequest().getParameter("ids");
			// 页面所选汇总明细信息
			String[] idss = ids.split(",");
			for (int i = 0; i < idss.length; i++) {
				if (idss[i] != null) {
					RequiredCollect rc = this.iRequiredCollectBiz
							.getRequiredCollect(new Long(idss[i]));
					requiredCollect = rc;
					List rcdList = this.iRequiredCollectBiz
							.getRequiredCollectDetailList(requiredCollect
									.getRcId());
					for (int j = 0; j < rcdList.size(); j++) {
						RequiredCollectDetail rcd = (RequiredCollectDetail) rcdList.get(j);

						this.iRequiredMaterialBiz.updateRequiredMaterialYamountByRcdId(rcd.getRmdId(),rcd.getAmount(),0);
		                this.iRequiredCollectBiz.deleteRequiredCollectDetail(rcd);

					}
					this.iRequiredCollectBiz.deleteRequiredCollect(rc);
				}
			}
			PrintWriter out = this.getResponse().getWriter();
			String message = "删除成功";
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除立项信息");
			out.print(message);
		} catch (Exception e) {
			log("删除立项信息错误！", e);
			throw new BaseException("删除立项信息错误！", e);
		}

		// return viewRequiredPlan();
		return null;
	}

	/**
	 * 查看计划明细信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewRequiredCollectDetail() throws BaseException {

		try {
			requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
			// 获取附件
			requiredCollect.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									requiredCollect.getRcId(),
									AttachmentStatus.ATTACHMENT_CODE_103)), "0",
							this.getRequest()));
			
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
			this.getRequest().setAttribute("buyWayCn", BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
			this.getRequest().setAttribute("supplierTypeCn", BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
			
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
		
		} catch (Exception e) {
			log("查看计划明细信息错误！", e);
			throw new BaseException("查看计划明细信息错误！", e);
		}
		return DETAIL;

	}

	/**
	 * 采购立项不需要审批的时候提交正式立项
	 * @return
	 * @throws BaseException
	 */
    public String updateProcessRequiredCollect() throws BaseException{
    	
    	try {
			ids = this.getRequest().getParameter("ids");
			// 页面所选汇总明细信息
			String[] idss = ids.split(",");
			for (int i = 0; i < idss.length; i++) {
				if (idss[i] != null) {
					RequiredCollect rc = this.iRequiredCollectBiz
							.getRequiredCollect(new Long(idss[i]));
					rc.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
					rc.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iRequiredCollectBiz.updateRequiredCollect(rc);
				}
			}
			PrintWriter out = this.getResponse().getWriter();
			String message = "提交成功";
			this.getRequest().setAttribute("message", "提交成功");
			this.getRequest().setAttribute("operModule", "提交正式立项信息");
			out.print(message);
		} catch (Exception e) {
			log("提交正式立项信息错误！", e);
			throw new BaseException("提交正式立项信息错误！", e);
		}
		return null;
    }
	/**
	 * 立项信息Excel导出
	 * @return
	 * @throws BaseException 
	 */
	 public void  exportRequiredCollectExcel() throws BaseException{
		try{
		 List<String> titleList = new ArrayList<String>();
		 titleList.add("采购组织");
		 titleList.add("项目编号");
		 titleList.add("项目名称");
		 titleList.add("采购方式");
		 titleList.add("供应商选择方式");
		 titleList.add("立项人");
		 titleList.add("立项日期");
		 
		 
		// 汇总 列表信息
		requiredCollect = new RequiredCollect();
		String bidCode=this.getRequest().getParameter("bidCode");
		requiredCollect.setBidCode(bidCode);
		String buyRemark=this.getRequest().getParameter("buyRemark");
		requiredCollect.setBuyRemark(buyRemark);
		String buyWay=this.getRequest().getParameter("buyWay");
		requiredCollect.setBuyWay(buyWay);
		
		String rmCode=this.getRequest().getParameter("rmCode");
		if(StringUtil.isNotBlank(rmCode))
		{
			List<RequiredCollect> list=this.iRequiredCollectBiz.getRequiredCollecListByRmCode(rmCode);
			String condtion=" and (de.bid_Code='1' ";
			for(RequiredCollect rc:list){
				condtion+="or de.bid_Code='"+rc.getBidCode()+"'";
			}
			condtion+=")";
			requiredCollect.setCondition(condtion);
		}
			
		List<RequiredCollect> rclist =this.iRequiredCollectBiz.getRequiredCollectList( this.getRollPageDataTables(), requiredCollect) ;
			
     	 List<Object[]> objList = new ArrayList<Object[]>();
		 for (int i = 0; i < rclist.size(); i++) {
			 requiredCollect=(RequiredCollect)rclist.get(i);
			 if(StringUtil.isNotBlank(requiredCollect)){
				 requiredCollect.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
				 requiredCollect.setDeptName(BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
				 requiredCollect.setPurchaseDeptName(BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
				 requiredCollect.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
				 requiredCollect.setSupplierTypeCn(BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
				
				 //bidMonitor
				 Object[]  obj=new Object[]{
						 requiredCollect.getPurchaseDeptName(),
						 requiredCollect.getBidCode(),
						 requiredCollect.getBuyRemark(),
						 requiredCollect.getBuyWayCn(),
						 requiredCollect.getSupplierTypeCn(),
						 requiredCollect.getWriterCn(),
						 requiredCollect.getWriteDate()
				 };
				  objList.add(obj);
			 }
		 }
		// 输出的excel文件名
			String file = "询价采购项目信息.xls";
			String targetfile = this.getServletContext().getRealPath(file);
			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("worksheet", "询价采购项目信息");
			map.put("titleList", titleList);
			map.put("valueList", objList);
			excelList.add(map);
			// 输出的excel文件工作表名
			new ExcelUtil().expCommonExcel(targetfile,excelList);
			this.getResponse().setContentType("application/octet-stream; charset=utf-8");
			this.getResponse().setHeader("Content-Disposition", "attachment; filename="+new String(file.getBytes("gbk"),"iso-8859-1") );
			//this.getResponse().setHeader( "Set-Cookie", "name=value; HttpOnly"); 
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			readCount = is.read(buff);
			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		}catch (Exception e) {
		log.error("导出询价采购项目信息列表错误！", e);
		throw new BaseException("导出询价采购项目信息列表错误！", e);
	  }
	}

		
	/**
	 * 进入采购业务管理图形页面
	 * @return
	 * @throws BaseException 
	 */
	public String viewRequiredCollectNode() throws BaseException {
		try{
			if(StringUtil.isNotBlank(requiredCollect.getRcId())){
				requiredCollect = iRequiredCollectBiz.getRequiredCollectInfo(requiredCollect);
				requiredCollect.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
				BidProcessLog bidProcessLog=new BidProcessLog();
				bidProcessLog.setRcId(requiredCollect.getRcId());
				List<BidProcessLog> bidProcessLogList=this.iBidProcessLogBiz.getBidProcessLogList(bidProcessLog);
				Map<Long,Object> map=new HashMap<Long, Object>();
				for(BidProcessLog processLog:bidProcessLogList){
					if(processLog.getCompleteDate()!=null){
					  processLog.setDay(DateUtil.daysBetween(processLog.getReceiveDate(), processLog.getCompleteDate()));
					}
					map.put(processLog.getBidNode(),processLog);
				}
				
				bidProcessLogList=AskProgressStatusList.getbidProcessLogList(map);
				this.getRequest().setAttribute("bidProcessLogList",bidProcessLogList);
				
				 boolean isBidAbnomal=false;
				 if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus()))
				 {
					isBidAbnomal=true;
				 }
				 this.getRequest().setAttribute("isBidAbnomal", isBidAbnomal);
				
			}
			} catch (Exception e) {
			log("进入询价采购业务管理图形页面错误！", e);
			throw new BaseException("进入询价采购业务管理图形页面信息错误！", e);
		}
		return "detail_node";
		
	}

	 /**
	 * 查看流程跟踪信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String followAndViewProcessCollect() throws BaseException {
		try{
			// 流程列表信息
			Long rcId = Long.parseLong(this.getRequest().getParameter("param"));
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRcId(rcId);
			List list=this.iPurchaseRecordLogBiz.getPurchaseRecordLogList(purchaseRecordLog);
			this.setListValue(list);
			if(list.size()>0){
			   purchaseRecordLog=(PurchaseRecordLog)list.get(list.size()-1);
			}else{
				purchaseRecordLog=new PurchaseRecordLog();
			}
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
		} catch (Exception e) {
			log.error("查看流程跟踪信息错误！", e);
			throw new BaseException("查看流程跟踪信息错误！", e);
		}
		
		return "viewProCollect" ;
		
	}
	/*********************************************************** 询价采购项目结束 **********************************************************/
	public IRequiredMaterialBiz getiRequiredMaterialBiz() {
		return iRequiredMaterialBiz;
	}

	public void setiRequiredMaterialBiz(IRequiredMaterialBiz iRequiredMaterialBiz) {
		this.iRequiredMaterialBiz = iRequiredMaterialBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}

	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}

	public List<RequiredCollectDetail> getRcdList() {
		return rcdList;
	}

	public void setRcdList(List<RequiredCollectDetail> rcdList) {
		this.rcdList = rcdList;
	}

	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}

	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	
	
}
