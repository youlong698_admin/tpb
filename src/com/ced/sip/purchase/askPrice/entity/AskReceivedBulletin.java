package com.ced.sip.purchase.askPrice.entity;

import java.util.Date;

/** 
 * 类名称：AskReceivedBulletin
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class AskReceivedBulletin implements java.io.Serializable {

	// 属性信息
	private Long arbId;     //
	private Long rcId;     //
	private Long supplierId;     //
	private String supplierName;	 //
	private Date receivedDate;    //
	private String receivedName;	 //
	private String receivedTel;	 //
	private String writer;	 //
	private Date writeDate;    //
	private String remark;	 //
	private String isReplay;	 //
	
	
	public AskReceivedBulletin() {
		super();
	}
	
	public Long getArbId(){
	   return  arbId;
	} 
	public void setArbId(Long arbId) {
	   this.arbId = arbId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public Date getReceivedDate(){
	   return  receivedDate;
	} 
	public void setReceivedDate(Date receivedDate) {
	   this.receivedDate = receivedDate;
    }	    
	public String getReceivedName(){
	   return  receivedName;
	} 
	public void setReceivedName(String receivedName) {
	   this.receivedName = receivedName;
    }
	public String getReceivedTel(){
	   return  receivedTel;
	} 
	public void setReceivedTel(String receivedTel) {
	   this.receivedTel = receivedTel;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getIsReplay(){
	   return  isReplay;
	} 
	public void setIsReplay(String isReplay) {
	   this.isReplay = isReplay;
    }
}