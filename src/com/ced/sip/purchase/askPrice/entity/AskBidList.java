package com.ced.sip.purchase.askPrice.entity;

import java.util.Date;

/** 
 * 类名称：AskBidList
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public class AskBidList implements java.io.Serializable {

	// 属性信息
	private Long ablId;     //主键
	private Long rcId;     //项目ID
	private Date askDate;    //询价日期
	private Date returnDate;    //报价截止日期
	private Date decryptionDate;    //报价解密日期
	private String priceType;	 //报价类型
	private String priceColumnType;	 //报价列类型
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark1;	 //备用1
	private String remark2;	 //备用2
	private String remark3;	 //备用3
	private String remark4;	 //备用4
	private String remark;	 //备注
	private String responsibleUser;  //项目负责人
	private String responsiblePhone;  //负责人手机号
	private String priceStatus;//报价汇总状态
	

	private String priceTypeCn;	 //报价类型
	private String priceColumnTypeCn;	 //报价列类型
	private String askDateStr;    //询价日期
	private String returnDateStr;    //报价截止日期
	private String decryptionDateStr;    //报价解密日期
	
	
	public AskBidList() {
		super();
	}
	
	public Long getAblId(){
	   return  ablId;
	} 
	public void setAblId(Long ablId) {
	   this.ablId = ablId;
    }
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Date getAskDate(){
	   return  askDate;
	} 
	public void setAskDate(Date askDate) {
	   this.askDate = askDate;
    }	    
	public Date getReturnDate(){
	   return  returnDate;
	} 
	public void setReturnDate(Date returnDate) {
	   this.returnDate = returnDate;
    }	    
	public Date getDecryptionDate(){
	   return  decryptionDate;
	} 
	public void setDecryptionDate(Date decryptionDate) {
	   this.decryptionDate = decryptionDate;
    }	    
	public String getPriceType(){
	   return  priceType;
	} 
	public void setPriceType(String priceType) {
	   this.priceType = priceType;
    }
	public String getPriceColumnType(){
	   return  priceColumnType;
	} 
	public void setPriceColumnType(String priceColumnType) {
	   this.priceColumnType = priceColumnType;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public String getRemark3(){
	   return  remark3;
	} 
	public void setRemark3(String remark3) {
	   this.remark3 = remark3;
    }
	public String getRemark4(){
	   return  remark4;
	} 
	public void setRemark4(String remark4) {
	   this.remark4 = remark4;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getPriceTypeCn() {
		return priceTypeCn;
	}

	public void setPriceTypeCn(String priceTypeCn) {
		this.priceTypeCn = priceTypeCn;
	}

	public String getPriceColumnTypeCn() {
		return priceColumnTypeCn;
	}

	public void setPriceColumnTypeCn(String priceColumnTypeCn) {
		this.priceColumnTypeCn = priceColumnTypeCn;
	}

	public String getAskDateStr() {
		return askDateStr;
	}

	public void setAskDateStr(String askDateStr) {
		this.askDateStr = askDateStr;
	}

	public String getReturnDateStr() {
		return returnDateStr;
	}

	public void setReturnDateStr(String returnDateStr) {
		this.returnDateStr = returnDateStr;
	}

	public String getDecryptionDateStr() {
		return decryptionDateStr;
	}

	public void setDecryptionDateStr(String decryptionDateStr) {
		this.decryptionDateStr = decryptionDateStr;
	}

	public String getResponsibleUser() {
		return responsibleUser;
	}

	public void setResponsibleUser(String responsibleUser) {
		this.responsibleUser = responsibleUser;
	}

	public String getResponsiblePhone() {
		return responsiblePhone;
	}

	public void setResponsiblePhone(String responsiblePhone) {
		this.responsiblePhone = responsiblePhone;
	}

	public String getPriceStatus() {
		return priceStatus;
	}

	public void setPriceStatus(String priceStatus) {
		this.priceStatus = priceStatus;
	}
	
}