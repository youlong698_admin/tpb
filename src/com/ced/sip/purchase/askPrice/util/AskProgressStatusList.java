package com.ced.sip.purchase.askPrice.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ced.sip.purchase.base.entity.BidProcessLog;

/**
 * 此类是描述通过标段监控进入采购流程页需要的节点
 * @author luguanglei
 *
 */
public class AskProgressStatusList {
	
	/**
	 * 采购过程
	 * @param purchaseWay
	 * @param map
	 * @return
	 */
	public static List<BidProcessLog> getbidProcessLogList(Map<Long,Object> map) {
		List<BidProcessLog> bidProcessLogList = new ArrayList<BidProcessLog>();
		BidProcessLog bidProcessLog;
		BidProcessLog bidProcessLogNull=new BidProcessLog();
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_20)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_20);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_20,
					AskProgressStatus.Progress_Status_20_Text,
					AskProgressStatus.Progress_Status_20_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_20_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_21)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_21);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_21,
					AskProgressStatus.Progress_Status_21_Text,
					AskProgressStatus.Progress_Status_21_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_21_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_22)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_22);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_22,
					AskProgressStatus.Progress_Status_22_Text,
					AskProgressStatus.Progress_Status_22_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_22_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_23)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_23);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_23,
					AskProgressStatus.Progress_Status_23_Text,
					AskProgressStatus.Progress_Status_23_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_23_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_24)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_24);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_24,
					AskProgressStatus.Progress_Status_24_Text,
					AskProgressStatus.Progress_Status_24_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_24_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_25)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_25);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_25,
					AskProgressStatus.Progress_Status_25_Text,
					AskProgressStatus.Progress_Status_25_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_25_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_26)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_26);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_26,
					AskProgressStatus.Progress_Status_26_Text,
					AskProgressStatus.Progress_Status_26_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_26_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_27)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_27);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_27,
					AskProgressStatus.Progress_Status_27_Text,
					AskProgressStatus.Progress_Status_27_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_27_IMG));
			bidProcessLog=map.get(AskProgressStatus.Progress_Status_28)==null?bidProcessLogNull:(BidProcessLog)map.get(AskProgressStatus.Progress_Status_28);
			bidProcessLogList.add(new BidProcessLog(
					AskProgressStatus.Progress_Status_28,
					AskProgressStatus.Progress_Status_28_Text,
					AskProgressStatus.Progress_Status_28_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),AskProgressStatus.Progress_Status_28_IMG));
		return bidProcessLogList;
	}
	
}
