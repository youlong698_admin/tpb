package com.ced.sip.purchase.askPrice.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.askPrice.entity.AskReceivedBulletin;
/** 
 * 类名称：IAskReceivedBulletinBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public interface IAskReceivedBulletinBiz {

	/**
	 * 根据主键获得回标表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract AskReceivedBulletin getAskReceivedBulletin(Long id) throws BaseException;

	/**
	 * 添加回标表信息
	 * @param askReceivedBulletin 回标表表实例
	 * @throws BaseException 
	 */
	abstract void saveAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException;

	/**
	 * 更新回标表表实例
	 * @param askReceivedBulletin 回标表表实例
	 * @throws BaseException 
	 */
	abstract void updateAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException;

	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteAskReceivedBulletin(String id) throws BaseException;

	/**
	 * 删除回标表表实例
	 * @param askReceivedBulletin 回标表表实例
	 * @throws BaseException 
	 */
	abstract void deleteAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException;

	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteAskReceivedBulletins(String[] id) throws BaseException;

	/**
	 * 获得所有回标表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAskReceivedBulletinList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有回标表表数据集
	 * @param askReceivedBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAskReceivedBulletinList(AskReceivedBulletin askReceivedBulletin) throws BaseException ;
	
	/**
	 * 获得所有回标表表数据集   总数
	 * @param askReceivedBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int countAskReceivedBulletinList(AskReceivedBulletin askReceivedBulletin) throws BaseException ;
	
	/**
	 * 获得所有回标表表数据集
	 * @param rollPage 分页对象
	 * @param askReceivedBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAskReceivedBulletinList(RollPage rollPage, AskReceivedBulletin askReceivedBulletin)
			throws BaseException;

}