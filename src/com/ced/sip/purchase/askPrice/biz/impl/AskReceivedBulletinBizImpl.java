package com.ced.sip.purchase.askPrice.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.biz.IAskReceivedBulletinBiz;
import com.ced.sip.purchase.askPrice.entity.AskReceivedBulletin;
/** 
 * 类名称：AskReceivedBulletinBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class AskReceivedBulletinBizImpl extends BaseBizImpl implements IAskReceivedBulletinBiz  {
	
	/**
	 * 根据主键获得回标表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public AskReceivedBulletin getAskReceivedBulletin(Long id) throws BaseException {
		return (AskReceivedBulletin)this.getObject(AskReceivedBulletin.class, id);
	}
	
	/**
	 * 获得回标表表实例
	 * @param askReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public AskReceivedBulletin getAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException {
		return (AskReceivedBulletin)this.getObject(AskReceivedBulletin.class, askReceivedBulletin.getArbId() );
	}
	
	/**
	 * 添加回标表信息
	 * @param askReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void saveAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException{
		this.saveObject( askReceivedBulletin ) ;
	}
	
	/**
	 * 更新回标表表实例
	 * @param askReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void updateAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException{
		this.updateObject( askReceivedBulletin ) ;
	}
	
	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteAskReceivedBulletin(String id) throws BaseException {
		this.removeObject( this.getAskReceivedBulletin( new Long(id) ) ) ;
	}
	
	/**
	 * 删除回标表表实例
	 * @param askReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteAskReceivedBulletin(AskReceivedBulletin askReceivedBulletin) throws BaseException {
		this.removeObject( askReceivedBulletin ) ;
	}
	
	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteAskReceivedBulletins(String[] id) throws BaseException {
		this.removeBatchObject(AskReceivedBulletin.class, id) ;
	}
	
	/**
	 * 获得所有回标表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getAskReceivedBulletinList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskReceivedBulletin de where 1 = 1 " );

		hql.append(" order by de.arbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有回标表表数据集
	 * @param askReceivedBulletin 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getAskReceivedBulletinList(AskReceivedBulletin askReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskReceivedBulletin de where 1 = 1 " );
		if(askReceivedBulletin != null){
			if(StringUtil.isNotBlank(askReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(askReceivedBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(askReceivedBulletin.getSupplierId())){
				hql.append(" and de.supplierId =").append(askReceivedBulletin.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.arbId ");
		return this.getObjects( hql.toString() );
	}

	/**
	 * 获得所有回标表表数据集  总数
	 * @param askReceivedBulletin 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public int countAskReceivedBulletinList(AskReceivedBulletin askReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.arbId) from AskReceivedBulletin de where 1 = 1 " );
		if(askReceivedBulletin != null){
			if(StringUtil.isNotBlank(askReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(askReceivedBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(askReceivedBulletin.getSupplierId())){
				hql.append(" and de.supplierId =").append(askReceivedBulletin.getSupplierId()).append("");
			}
		}
		return this.countObjects( hql.toString() );
	}
	/**
	 * 获得所有回标表表数据集
	 * @param rollPage 分页对象
	 * @param askReceivedBulletin 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getAskReceivedBulletinList(RollPage rollPage, AskReceivedBulletin askReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskReceivedBulletin de where 1 = 1 " );
		if(askReceivedBulletin != null){
			if(StringUtil.isNotBlank(askReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(askReceivedBulletin.getRcId()).append("");
			}
		}
		hql.append(" order by de.arbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
