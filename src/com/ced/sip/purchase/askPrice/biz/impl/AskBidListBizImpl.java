package com.ced.sip.purchase.askPrice.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.biz.IAskBidListBiz;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
/** 
 * 类名称：AskBidListBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public class AskBidListBizImpl extends BaseBizImpl implements IAskBidListBiz  {
	
	/**
	 * 根据主键获得询价_询价信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public AskBidList getAskBidList(Long id) throws BaseException {
		return (AskBidList)this.getObject(AskBidList.class, id);
	}
	
	/**
	 * 获得询价_询价信息表实例
	 * @param askBidList 询价_询价信息表实例
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public AskBidList getAskBidList(AskBidList askBidList) throws BaseException {
		return (AskBidList)this.getObject(AskBidList.class, askBidList.getAblId() );
	}
	
	/**
	 * 添加询价_询价信息信息
	 * @param askBidList 询价_询价信息表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void saveAskBidList(AskBidList askBidList) throws BaseException{
		this.saveObject( askBidList ) ;
	}
	
	/**
	 * 更新询价_询价信息表实例
	 * @param askBidList 询价_询价信息表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void updateAskBidList(AskBidList askBidList) throws BaseException{
		this.updateObject( askBidList ) ;
	}
	
	/**
	 * 删除询价_询价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteAskBidList(String id) throws BaseException {
		this.removeObject( this.getAskBidList( new Long(id) ) ) ;
	}
	
	/**
	 * 删除询价_询价信息表实例
	 * @param askBidList 询价_询价信息表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteAskBidList(AskBidList askBidList) throws BaseException {
		this.removeObject( askBidList ) ;
	}
	
	/**
	 * 删除询价_询价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteAskBidLists(String[] id) throws BaseException {
		this.removeBatchObject(AskBidList.class, id) ;
	}
	
	/**
	 * 获得所有询价_询价信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getAskBidListList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskBidList de where 1 = 1 " );

		hql.append(" order by de.ablId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有询价_询价信息表数据集
	 * @param askBidList 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getAskBidListList(AskBidList askBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskBidList de where 1 = 1 " );
		if(askBidList != null){
			if(StringUtil.isNotBlank(askBidList.getRcId())){
				hql.append(" and de.rcId =").append(askBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.ablId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有询价_询价信息表数据集
	 * @param rollPage 分页对象
	 * @param askBidList 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getAskBidListList(RollPage rollPage, AskBidList askBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskBidList de where 1 = 1 " );
		if(askBidList != null){
			if(StringUtil.isNotBlank(askBidList.getRcId())){
				hql.append(" and de.rcId =").append(askBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.ablId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 根据rcId获得询价_询价信息表实例
	 * @param rcId 项目ID
	 * @return
	 * @throws BaseException 
	 */
	public AskBidList getAskBidListByRcId(Long rcId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from AskBidList de where de.rcId =").append(rcId);
        List list = this.getObjects( hql.toString() );
		AskBidList askBidList = new AskBidList();
		if(list!=null&&list.size()>0){
			askBidList = (AskBidList)list.get(0);
		}
		return askBidList;
	}
}
