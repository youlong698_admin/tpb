package com.ced.sip.purchase.askPrice.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
/** 
 * 类名称：IAskBidListBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public interface IAskBidListBiz {

	/**
	 * 根据主键获得询价_询价信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract AskBidList getAskBidList(Long id) throws BaseException;

	/**
	 * 添加询价_询价信息信息
	 * @param askBidList 询价_询价信息表实例
	 * @throws BaseException 
	 */
	abstract void saveAskBidList(AskBidList askBidList) throws BaseException;

	/**
	 * 更新询价_询价信息表实例
	 * @param askBidList 询价_询价信息表实例
	 * @throws BaseException 
	 */
	abstract void updateAskBidList(AskBidList askBidList) throws BaseException;

	/**
	 * 删除询价_询价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteAskBidList(String id) throws BaseException;

	/**
	 * 删除询价_询价信息表实例
	 * @param askBidList 询价_询价信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteAskBidList(AskBidList askBidList) throws BaseException;

	/**
	 * 删除询价_询价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteAskBidLists(String[] id) throws BaseException;

	/**
	 * 获得所有询价_询价信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAskBidListList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有询价_询价信息表数据集
	 * @param askBidList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAskBidListList(AskBidList askBidList) throws BaseException ;
	
	/**
	 * 获得所有询价_询价信息表数据集
	 * @param rollPage 分页对象
	 * @param askBidList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAskBidListList(RollPage rollPage, AskBidList askBidList)
			throws BaseException;

	/**
	 * 根据rcId获得询价_询价信息表实例
	 * @param rcId 项目ID
	 * @return
	 * @throws BaseException 
	 */
	abstract AskBidList getAskBidListByRcId(Long rcId) throws BaseException;
}