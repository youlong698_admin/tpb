package com.ced.sip.purchase.tender.entity;

public class TenderJudgeSum {
   private Long supplierId;
   private Double sumPoints;
public Long getSupplierId() {
	return supplierId;
}
public void setSupplierId(Long supplierId) {
	this.supplierId = supplierId;
}
public Double getSumPoints() {
	return sumPoints;
}
public void setSumPoints(Double sumPoints) {
	this.sumPoints = sumPoints;
}
   
}
