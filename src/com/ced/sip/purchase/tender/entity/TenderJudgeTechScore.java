package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderJudgeTechScore
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderJudgeTechScore implements java.io.Serializable {

	// 属性信息
	private Long tjstId;     //主键
	private Long rcId;     //项目ID
	private Long itemId;     //评分项ID
	private Long dispNumber;     //显示序号
	private Double judgePoints;	//评委的评分
	private Long supplierId;     //供应商ID
	private String supplierName; //供应商名称
	private Date scoreTime;    //评分时间
	private Long expertId;     //专家ID
	private String status;	 //状态
	private Double supPoints;	//供应商得分
	
	
	public TenderJudgeTechScore() {
		super();
	}
	
	public Long getTjstId(){
	   return  tjstId;
	} 
	public void setTjstId(Long tjstId) {
	   this.tjstId = tjstId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getItemId(){
	   return  itemId;
	} 
	public void setItemId(Long itemId) {
	   this.itemId = itemId;
    }     
	public Long getDispNumber(){
	   return  dispNumber;
	} 
	public void setDispNumber(Long dispNumber) {
	   this.dispNumber = dispNumber;
    }     
	public Double getJudgePoints(){
	   return  judgePoints;
	} 
	public void setJudgePoints(Double judgePoints) {
	   this.judgePoints = judgePoints;
    }	
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Date getScoreTime(){
	   return  scoreTime;
	} 
	public void setScoreTime(Date scoreTime) {
	   this.scoreTime = scoreTime;
    }	    
	public Long getExpertId(){
	   return  expertId;
	} 
	public void setExpertId(Long expertId) {
	   this.expertId = expertId;
    }     
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public Double getSupPoints(){
	   return  supPoints;
	} 
	public void setSupPoints(Double supPoints) {
	   this.supPoints = supPoints;
    }

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
}