package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderBidList
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidList implements java.io.Serializable {

	// 属性信息
	private Long tblId;     //主键
	private Long rcId;     //项目ID
	private Date openDate;    //开标日期
	private Date salesDate;    //招标文件领购开始日期
	private Date saleeDate;    //招标文件领购截止日期
	private Date returnDate;    //回标截止日期
	private Double tenderMoney;	//标书费金额
	private Double bondMoney;	//保证金金额
	private Long minBidAmount;     //
	private String biddingMode;	 //专家决标 价格决标
	private String priceType;	 //报价类型
	private String priceColumnType;	 //报价列类型
	private String responsibleUser;	 //项目负责人
	private String responsiblePhone;	 //负责人手机号
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark1;	 //备用1
	private String remark2;	 //备用2
	private String remark3;	 //备用3
	private String remark4;	 //备用4
	private String remark;	 //备注
	private Long bidOpenAdmin;//开标管理员
	private String bidOpenPassword;//开标密码
	private String priceStatus;//报价汇总状态
	private String calibtationStatus;//定标状态
	private String openStatus;//开标状态
	private String priceScoreType;//报价评分方式
	
	private String priceTypeCn;	 //报价类型
	private String priceColumnTypeCn;	 //报价列类型
	private String openDateStr;    //开标日期
	private String salesDateStr;    //招标文件领购开始日期
	private String saleeDateStr;    //招标文件领购截止日期
	private String returnDateStr;    //回标截止日期
	private String bidOpenAdminCn;//开标管理员
	
	
	public TenderBidList() {
		super();
	}
	
	public Long getTblId(){
	   return  tblId;
	} 
	public void setTblId(Long tblId) {
	   this.tblId = tblId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Date getOpenDate(){
	   return  openDate;
	} 
	public void setOpenDate(Date openDate) {
	   this.openDate = openDate;
    }	 	    
	public Date getReturnDate(){
	   return  returnDate;
	} 
	public void setReturnDate(Date returnDate) {
	   this.returnDate = returnDate;
    }	    
	public Double getTenderMoney(){
	   return  tenderMoney;
	} 
	public void setTenderMoney(Double tenderMoney) {
	   this.tenderMoney = tenderMoney;
    }	
	public Double getBondMoney(){
	   return  bondMoney;
	} 
	public void setBondMoney(Double bondMoney) {
	   this.bondMoney = bondMoney;
    }	
	public Long getMinBidAmount(){
	   return  minBidAmount;
	} 
	public void setMinBidAmount(Long minBidAmount) {
	   this.minBidAmount = minBidAmount;
    }     
	public String getBiddingMode(){
	   return  biddingMode;
	} 
	public void setBiddingMode(String biddingMode) {
	   this.biddingMode = biddingMode;
    }
	public String getPriceType(){
	   return  priceType;
	} 
	public void setPriceType(String priceType) {
	   this.priceType = priceType;
    }
	public String getPriceColumnType(){
	   return  priceColumnType;
	} 
	public void setPriceColumnType(String priceColumnType) {
	   this.priceColumnType = priceColumnType;
    }
	public String getResponsibleUser(){
	   return  responsibleUser;
	} 
	public void setResponsibleUser(String responsibleUser) {
	   this.responsibleUser = responsibleUser;
    }
	public String getResponsiblePhone(){
	   return  responsiblePhone;
	} 
	public void setResponsiblePhone(String responsiblePhone) {
	   this.responsiblePhone = responsiblePhone;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public String getRemark3(){
	   return  remark3;
	} 
	public void setRemark3(String remark3) {
	   this.remark3 = remark3;
    }
	public String getRemark4(){
	   return  remark4;
	} 
	public void setRemark4(String remark4) {
	   this.remark4 = remark4;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getPriceTypeCn() {
		return priceTypeCn;
	}

	public void setPriceTypeCn(String priceTypeCn) {
		this.priceTypeCn = priceTypeCn;
	}

	public String getPriceColumnTypeCn() {
		return priceColumnTypeCn;
	}

	public void setPriceColumnTypeCn(String priceColumnTypeCn) {
		this.priceColumnTypeCn = priceColumnTypeCn;
	}

	public String getOpenDateStr() {
		return openDateStr;
	}

	public void setOpenDateStr(String openDateStr) {
		this.openDateStr = openDateStr;
	}

	public Date getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Date salesDate) {
		this.salesDate = salesDate;
	}

	public Date getSaleeDate() {
		return saleeDate;
	}

	public void setSaleeDate(Date saleeDate) {
		this.saleeDate = saleeDate;
	}

	public String getSalesDateStr() {
		return salesDateStr;
	}

	public void setSalesDateStr(String salesDateStr) {
		this.salesDateStr = salesDateStr;
	}

	public String getSaleeDateStr() {
		return saleeDateStr;
	}

	public void setSaleeDateStr(String saleeDateStr) {
		this.saleeDateStr = saleeDateStr;
	}

	public String getReturnDateStr() {
		return returnDateStr;
	}

	public void setReturnDateStr(String returnDateStr) {
		this.returnDateStr = returnDateStr;
	}

	public Long getBidOpenAdmin() {
		return bidOpenAdmin;
	}

	public void setBidOpenAdmin(Long bidOpenAdmin) {
		this.bidOpenAdmin = bidOpenAdmin;
	}

	public String getBidOpenPassword() {
		return bidOpenPassword;
	}

	public void setBidOpenPassword(String bidOpenPassword) {
		this.bidOpenPassword = bidOpenPassword;
	}

	public String getBidOpenAdminCn() {
		return bidOpenAdminCn;
	}

	public void setBidOpenAdminCn(String bidOpenAdminCn) {
		this.bidOpenAdminCn = bidOpenAdminCn;
	}

	public String getPriceStatus() {
		return priceStatus;
	}

	public void setPriceStatus(String priceStatus) {
		this.priceStatus = priceStatus;
	}

	public String getCalibtationStatus() {
		return calibtationStatus;
	}

	public void setCalibtationStatus(String calibtationStatus) {
		this.calibtationStatus = calibtationStatus;
	}

	public String getOpenStatus() {
		return openStatus;
	}

	public void setOpenStatus(String openStatus) {
		this.openStatus = openStatus;
	}

	public String getPriceScoreType() {
		return priceScoreType;
	}

	public void setPriceScoreType(String priceScoreType) {
		this.priceScoreType = priceScoreType;
	}
	
}