package com.ced.sip.purchase.tender.entity;

import java.util.Date;
import java.util.List;

/** 
 * 类名称：TenderBidJudge
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidJudge implements java.io.Serializable {

	// 属性信息
	private Long tbjId;     //主键
	private Long rcId;     //项目ID
	private String selectType;	 //
	private int selectNumber;     //抽取批次
	private String selectProfession;	 //抽取专业
	private Long selectCount;     //抽取数量
	private Long expertId;     //评委主键
	private String expertPassword;	 //评委密码
	private String expertRole;	 //评委角色（0技术,1商务,2综合）
	private String expertSign;	 //评委签到
	private Date expertSignDate;    //评委签到时间
	private Date selectDate;    //抽取时间
	private String remark;	 //备注
	private String writer;     //添加人
	private Date writeDate;    //添加时间
	
	private String expertName;
	private String companyName;
	private String expertMajor;
	private String mobilNumber;
	private String bidCode;
	private String buyRemark;
	private Date openDate;
	private List<TenderBidJudgeType> bidJudgeTypes;
	private String openStatus;
	
	
	public TenderBidJudge() {
		super();
	}
	
	public Long getTbjId(){
	   return  tbjId;
	} 
	public void setTbjId(Long tbjId) {
	   this.tbjId = tbjId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getSelectType(){
	   return  selectType;
	} 
	public void setSelectType(String selectType) {
	   this.selectType = selectType;
    }
	public int getSelectNumber(){
	   return  selectNumber;
	} 
	public void setSelectNumber(int selectNumber) {
	   this.selectNumber = selectNumber;
    }     
	public String getSelectProfession(){
	   return  selectProfession;
	} 
	public void setSelectProfession(String selectProfession) {
	   this.selectProfession = selectProfession;
    }
	public Long getSelectCount(){
	   return  selectCount;
	} 
	public void setSelectCount(Long selectCount) {
	   this.selectCount = selectCount;
    }     
	public Long getExpertId(){
	   return  expertId;
	} 
	public void setExpertId(Long expertId) {
	   this.expertId = expertId;
    }     
	public String getExpertPassword(){
	   return  expertPassword;
	} 
	public void setExpertPassword(String expertPassword) {
	   this.expertPassword = expertPassword;
    }
	public String getExpertRole(){
	   return  expertRole;
	} 
	public void setExpertRole(String expertRole) {
	   this.expertRole = expertRole;
    }
	public String getExpertSign(){
	   return  expertSign;
	} 
	public void setExpertSign(String expertSign) {
	   this.expertSign = expertSign;
    }
	public Date getExpertSignDate(){
	   return  expertSignDate;
	} 
	public void setExpertSignDate(Date expertSignDate) {
	   this.expertSignDate = expertSignDate;
    }	
	public Date getSelectDate(){
	   return  selectDate;
	} 
	public void setSelectDate(Date selectDate) {
	   this.selectDate = selectDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }     
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }

	public String getExpertName() {
		return expertName;
	}

	public void setExpertName(String expertName) {
		this.expertName = expertName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getExpertMajor() {
		return expertMajor;
	}

	public void setExpertMajor(String expertMajor) {
		this.expertMajor = expertMajor;
	}

	public String getMobilNumber() {
		return mobilNumber;
	}

	public void setMobilNumber(String mobilNumber) {
		this.mobilNumber = mobilNumber;
	}

	public String getBidCode() {
		return bidCode;
	}

	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}

	public String getBuyRemark() {
		return buyRemark;
	}

	public void setBuyRemark(String buyRemark) {
		this.buyRemark = buyRemark;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public List<TenderBidJudgeType> getBidJudgeTypes() {
		return bidJudgeTypes;
	}

	public void setBidJudgeTypes(List<TenderBidJudgeType> bidJudgeTypes) {
		this.bidJudgeTypes = bidJudgeTypes;
	}

	public String getOpenStatus() {
		return openStatus;
	}

	public void setOpenStatus(String openStatus) {
		this.openStatus = openStatus;
	}	 
	
}