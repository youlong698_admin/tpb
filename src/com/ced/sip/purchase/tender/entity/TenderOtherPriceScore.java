package com.ced.sip.purchase.tender.entity;

/** 
 * 类名称：TenderOtherPriceScore
 * 创建人：luguanglei 
 * 创建时间：2017-05-13
 */
public class TenderOtherPriceScore implements java.io.Serializable {

	// 属性信息
	private Long topsId;     //主键
	private Long rcId;     //项目ID
	private Long supplierId;     //供应商ID
	private Double priceScore;	//供应商得分
	
	
	public TenderOtherPriceScore() {
		super();
	}
	
	public Long getTopsId(){
	   return  topsId;
	} 
	public void setTopsId(Long topsId) {
	   this.topsId = topsId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Double getPriceScore(){
	   return  priceScore;
	} 
	public void setPriceScore(Double priceScore) {
	   this.priceScore = priceScore;
    }	
}