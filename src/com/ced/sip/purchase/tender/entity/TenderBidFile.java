package com.ced.sip.purchase.tender.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：TenderBidFile
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidFile extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long tbfId;     //主键
	private Long rcId;     //项目ID
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark1;	 //备用1
	private String remark2;	 //备用2
	private String remark3;	 //备用3
	private String remark4;	 //备用4
	private String remark;	 //备注
	
	private String writerCn;
	
	public TenderBidFile() {
		super();
	}
	
	public Long getTbfId(){
	   return  tbfId;
	} 
	public void setTbfId(Long tbfId) {
	   this.tbfId = tbfId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public String getRemark3(){
	   return  remark3;
	} 
	public void setRemark3(String remark3) {
	   this.remark3 = remark3;
    }
	public String getRemark4(){
	   return  remark4;
	} 
	public void setRemark4(String remark4) {
	   this.remark4 = remark4;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getWriterCn() {
		return writerCn;
	}

	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}
}