package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderBidPriceSet
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidPriceSet implements java.io.Serializable {

	// 属性信息
	private Long tbpsId;     //主键
	private Long rcId;     //项目ID
	private String brsType;	 //评分规则类型
	private Long rate;     //计算比率
	private Long upPoint;     //高出比率扣分
	private Long lowPoint;     //低出比率扣分
	private String writer;	 //编辑人
	private Date writeDate;    //编辑日期
	
	
	public TenderBidPriceSet() {
		super();
	}
	
	public Long getTbpsId(){
	   return  tbpsId;
	} 
	public void setTbpsId(Long tbpsId) {
	   this.tbpsId = tbpsId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getBrsType(){
	   return  brsType;
	} 
	public void setBrsType(String brsType) {
	   this.brsType = brsType;
    }
	public Long getRate(){
	   return  rate;
	} 
	public void setRate(Long rate) {
	   this.rate = rate;
    }     
	public Long getUpPoint(){
	   return  upPoint;
	} 
	public void setUpPoint(Long upPoint) {
	   this.upPoint = upPoint;
    }     
	public Long getLowPoint(){
	   return  lowPoint;
	} 
	public void setLowPoint(Long lowPoint) {
	   this.lowPoint = lowPoint;
    }     
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
}