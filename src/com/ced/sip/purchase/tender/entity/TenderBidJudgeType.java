package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderBidJudgeType
 * 创建人：luguanglei 
 * 创建时间：2017-05-13
 */
public class TenderBidJudgeType implements java.io.Serializable {

	// 属性信息
	private Long tbjtId;     //主键
	private Long rcId;     //项目ID
	private Long tbjId;	 //
	private String tableName;	 //评分表名（技术评分表，商务评分表）
	private String tableStatu;	 //评分表状态
	private Date tableSubmitTime;    //评分表提交时间
	private Long expertId;     //专家ID
	
	private String expertName;
	
	
	public TenderBidJudgeType() {
		super();
	}
	
	public Long getTbjtId(){
	   return  tbjtId;
	} 
	public void setTbjtId(Long tbjtId) {
	   this.tbjtId = tbjtId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getTbjId(){
	   return  tbjId;
	} 
	public void setTbjId(Long tbjId) {
	   this.tbjId = tbjId;
    }
	public String getTableName(){
	   return  tableName;
	} 
	public void setTableName(String tableName) {
	   this.tableName = tableName;
    }
	public String getTableStatu(){
	   return  tableStatu;
	} 
	public void setTableStatu(String tableStatu) {
	   this.tableStatu = tableStatu;
    }
	public Date getTableSubmitTime(){
	   return  tableSubmitTime;
	} 
	public void setTableSubmitTime(Date tableSubmitTime) {
	   this.tableSubmitTime = tableSubmitTime;
    }	    
	public Long getExpertId(){
	   return  expertId;
	} 
	public void setExpertId(Long expertId) {
	   this.expertId = expertId;
    }

	public String getExpertName() {
		return expertName;
	}

	public void setExpertName(String expertName) {
		this.expertName = expertName;
	}     
}