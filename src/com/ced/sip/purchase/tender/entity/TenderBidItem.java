package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderBidItem
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidItem implements java.io.Serializable {

	// 属性信息
	private Long tbiId;     //主键
	private Long rcId;     //项目ID
	private Long itemId;     //评分项ID
	private String itemName;	 //评分项
	private Long dispNumber;     //显示序号
	private Double points;	//标准分值
	private String itemType;	 //类别（0技术，1商务）
	private String itemDetail;	 //评分说明
	private String remark;	 //备注
	private String writer;     //添加人
	private Date writeDate;    //添加时间
	
	
	public TenderBidItem() {
		super();
	}
	
	public Long getTbiId(){
	   return  tbiId;
	} 
	public void setTbiId(Long tbiId) {
	   this.tbiId = tbiId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getItemId(){
	   return  itemId;
	} 
	public void setItemId(Long itemId) {
	   this.itemId = itemId;
    }     
	public String getItemName(){
	   return  itemName;
	} 
	public void setItemName(String itemName) {
	   this.itemName = itemName;
    }
	public Long getDispNumber(){
	   return  dispNumber;
	} 
	public void setDispNumber(Long dispNumber) {
	   this.dispNumber = dispNumber;
    }     
	public Double getPoints(){
	   return  points;
	} 
	public void setPoints(Double points) {
	   this.points = points;
    }	
	public String getItemType(){
	   return  itemType;
	} 
	public void setItemType(String itemType) {
	   this.itemType = itemType;
    }
	public String getItemDetail(){
	   return  itemDetail;
	} 
	public void setItemDetail(String itemDetail) {
	   this.itemDetail = itemDetail;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }     
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
}