package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderBidRate
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidRate implements java.io.Serializable {

	// 属性信息
	private Long tbrId;     //主键
	private Long rcId;     //项目ID
	private Double priceRate;	//价格权重
	private Double businessRate;	//商务权重
	private Double techRate;	//技术权重
	private String writer;	 //编辑人
	private Date writeDate;    //编辑日期
	private String remark;	 //备注
	

	private String totalRate;
	
	public TenderBidRate() {
		super();
	}
	
	public Long getTbrId(){
	   return  tbrId;
	} 
	public void setTbrId(Long tbrId) {
	   this.tbrId = tbrId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Double getPriceRate(){
	   return  priceRate;
	} 
	public void setPriceRate(Double priceRate) {
	   this.priceRate = priceRate;
    }	
	public Double getBusinessRate(){
	   return  businessRate;
	} 
	public void setBusinessRate(Double businessRate) {
	   this.businessRate = businessRate;
    }	
	public Double getTechRate(){
	   return  techRate;
	} 
	public void setTechRate(Double techRate) {
	   this.techRate = techRate;
    }	
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(String totalRate) {
		this.totalRate = totalRate;
	}
}