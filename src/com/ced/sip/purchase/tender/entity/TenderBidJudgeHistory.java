package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderBidJudgeHistory
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidJudgeHistory implements java.io.Serializable {

	// 属性信息
	private Long tbjhId;     //主键
	private Long rcId;     //项目ID
	private String selectType;	 //
	private int selectNumber;     //抽取批次
	private String selectProfession;	 //抽取专业
	private Long selectCount;     //抽取数量
	private Long expertId;     //评委主键
	private String expertPassword;	 //评委密码
	private String expertRole;	 //评委角色（0技术,1商务,2综合）
	private String ifJoinFinal;	 //是否为终选评委
	private Date selectDate;    //抽取时间
	private String remark;	 //备注
	private String writer;     //添加人
	private Date writeDate;    //添加时间
	
	
	public TenderBidJudgeHistory() {
		super();
	}
	
	public Long getTbjhId(){
	   return  tbjhId;
	} 
	public void setTbjhId(Long tbjhId) {
	   this.tbjhId = tbjhId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getSelectType(){
	   return  selectType;
	} 
	public void setSelectType(String selectType) {
	   this.selectType = selectType;
    }
	public int getSelectNumber(){
	   return  selectNumber;
	} 
	public void setSelectNumber(int selectNumber) {
	   this.selectNumber = selectNumber;
    }     
	public String getSelectProfession(){
	   return  selectProfession;
	} 
	public void setSelectProfession(String selectProfession) {
	   this.selectProfession = selectProfession;
    }
	public Long getSelectCount(){
	   return  selectCount;
	} 
	public void setSelectCount(Long selectCount) {
	   this.selectCount = selectCount;
    }     
	public Long getExpertId(){
	   return  expertId;
	} 
	public void setExpertId(Long expertId) {
	   this.expertId = expertId;
    }     
	public String getExpertPassword(){
	   return  expertPassword;
	} 
	public void setExpertPassword(String expertPassword) {
	   this.expertPassword = expertPassword;
    }
	public String getExpertRole(){
	   return  expertRole;
	} 
	public void setExpertRole(String expertRole) {
	   this.expertRole = expertRole;
    }
	public String getIfJoinFinal(){
	   return  ifJoinFinal;
	} 
	public void setIfJoinFinal(String ifJoinFinal) {
	   this.ifJoinFinal = ifJoinFinal;
    }
	public Date getSelectDate(){
	   return  selectDate;
	} 
	public void setSelectDate(Date selectDate) {
	   this.selectDate = selectDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }     
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
}