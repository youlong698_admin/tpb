package com.ced.sip.purchase.tender.entity;

public class TenderSupplierScore {
	private Long rcId;
	private Long supplierId;
	private String supplierName;
	
	//技术专家打总分
	private String techSumPoints;
	//商务专家打总分
	private String busSumPoints;
	//综合评分总得分
	private String compreSumPoints;
	//总价合计
	private String sumPrice;
	//报价平均价
	private String avgPrice;
	//报价得分
	private String priceScore;
	
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getAvgPrice() {
		return avgPrice;
	}
	public void setAvgPrice(String avgPrice) {
		this.avgPrice = avgPrice;
	}
	public String getPriceScore() {
		return priceScore;
	}
	public void setPriceScore(String priceScore) {
		this.priceScore = priceScore;
	}
	public String getTechSumPoints() {
		return techSumPoints;
	}
	public void setTechSumPoints(String techSumPoints) {
		this.techSumPoints = techSumPoints;
	}
	public String getBusSumPoints() {
		return busSumPoints;
	}
	public void setBusSumPoints(String busSumPoints) {
		this.busSumPoints = busSumPoints;
	}
	public String getCompreSumPoints() {
		return compreSumPoints;
	}
	public void setCompreSumPoints(String compreSumPoints) {
		this.compreSumPoints = compreSumPoints;
	}
	public String getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}
	
}
