package com.ced.sip.purchase.tender.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：TenderReceivedBulletin
 * 创建人：luguanglei 
 * 创建时间：2017-05-11
 */
public class TenderReceivedBulletin extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long trbId;     //主键
	private Long rcId;     //项目ID
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private Date receivedDate;    //应标日期
	private String receivedName;	 //应标人
	private String receivedTel;	 //应标人电话
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark;	 //备注
	private String isReplay;	 //是否回标
	

	private String judgePoints;//评委评标项得分
	private String supPoints;//评委给供应商打分
	
	
	public TenderReceivedBulletin() {
		super();
	}
	
	public Long getTrbId(){
	   return  trbId;
	} 
	public void setTrbId(Long trbId) {
	   this.trbId = trbId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public Date getReceivedDate(){
	   return  receivedDate;
	} 
	public void setReceivedDate(Date receivedDate) {
	   this.receivedDate = receivedDate;
    }	    
	public String getReceivedName(){
	   return  receivedName;
	} 
	public void setReceivedName(String receivedName) {
	   this.receivedName = receivedName;
    }
	public String getReceivedTel(){
	   return  receivedTel;
	} 
	public void setReceivedTel(String receivedTel) {
	   this.receivedTel = receivedTel;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getIsReplay(){
	   return  isReplay;
	} 
	public void setIsReplay(String isReplay) {
	   this.isReplay = isReplay;
    }

	public String getJudgePoints() {
		return judgePoints;
	}

	public void setJudgePoints(String judgePoints) {
		this.judgePoints = judgePoints;
	}

	public String getSupPoints() {
		return supPoints;
	}

	public void setSupPoints(String supPoints) {
		this.supPoints = supPoints;
	}
}