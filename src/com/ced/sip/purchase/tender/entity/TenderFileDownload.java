package com.ced.sip.purchase.tender.entity;

import java.util.Date;

/** 
 * 类名称：TenderFileDownload
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderFileDownload implements java.io.Serializable {

	// 属性信息
	private Long tfdId;     //主键
	private Long rcId;     //采购立项ID
	private Long supplierId;     //投标供应商主键
	private String supplierName;	 //投标供应商名称
	private Date fileDownDate;    //标书下载时间
	private String fileDownName;	 //标书下载人姓名
	private String fileDownTel;	 //标书下载人电话
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark;	 //备注
	private String remark1;	 //备用1
	private String remark2;	 //备用2
	private String remark3;	 //备用3
	private String remark4;	 //备用4
	
	
	public TenderFileDownload() {
		super();
	}
	
	public Long getTfdId(){
	   return  tfdId;
	} 
	public void setTfdId(Long tfdId) {
	   this.tfdId = tfdId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public Date getFileDownDate(){
	   return  fileDownDate;
	} 
	public void setFileDownDate(Date fileDownDate) {
	   this.fileDownDate = fileDownDate;
    }	    
	public String getFileDownName(){
	   return  fileDownName;
	} 
	public void setFileDownName(String fileDownName) {
	   this.fileDownName = fileDownName;
    }
	public String getFileDownTel(){
	   return  fileDownTel;
	} 
	public void setFileDownTel(String fileDownTel) {
	   this.fileDownTel = fileDownTel;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public String getRemark3(){
	   return  remark3;
	} 
	public void setRemark3(String remark3) {
	   this.remark3 = remark3;
    }
	public String getRemark4(){
	   return  remark4;
	} 
	public void setRemark4(String remark4) {
	   this.remark4 = remark4;
    }
}