package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
import com.ced.sip.purchase.tender.entity.TenderOtherPriceScore;

public interface ITenderPriceScoreBiz {

	/**
	 * 取报价汇总 净总价、含税总价
	 * @param rcId 项目id
	 * @param supplierId 供应商ID
	 * @return
	 * @throws BaseException
	 */
	abstract List getSumBidPriceAa( Long rcId, String supplierId ) throws BaseException ;
	/**
	 * 根据评分规则类型进行 报价评分计算
	 * 计算各供应商的总报价 计算最高价、最低价、平均价
	 * @param rcId
	 * @param sumpList
	 * @param bpSet
	 * @param bidRate
	 * @return
	 */
	abstract List getSupplierPriceScore(Long rcId, List<Object[]> sumpList, TenderBidPriceSet bpSet,TenderBidRate bidRate ) ;

	/**
	 * 获得所有其他价格规则分值表数据集
	 * @param tenderOtherPriceScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderOtherPriceScoreList(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException;
	/**
	 * 添加其他价格评分信息
	 * @param tenderOtherPriceScore 其他价格评分表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderOtherPriceScore(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException;

	/**
	 * 更新其他价格评分表实例
	 * @param tenderOtherPriceScore 其他价格评分表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderOtherPriceScore(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException;
	/**
	 * 获得其他价格评分表数据集
	 * tenderOtherPriceScore 其他价格评分表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderOtherPriceScore getTenderOtherPriceScoreByTenderOtherPriceScore(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException ;
	
}
