package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidFile;
/** 
 * 类名称：ITenderBidFileBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidFileBiz {

	/**
	 * 根据主键获得招标文件表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidFile getTenderBidFile(Long id) throws BaseException;

	/**
	 * 添加招标文件信息
	 * @param tenderBidFile 招标文件表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidFile(TenderBidFile tenderBidFile) throws BaseException;

	/**
	 * 更新招标文件表实例
	 * @param tenderBidFile 招标文件表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidFile(TenderBidFile tenderBidFile) throws BaseException;

	/**
	 * 删除招标文件表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidFile(String id) throws BaseException;

	/**
	 * 删除招标文件表实例
	 * @param tenderBidFile 招标文件表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidFile(TenderBidFile tenderBidFile) throws BaseException;

	/**
	 * 删除招标文件表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidFiles(String[] id) throws BaseException;

	/**
	 * 获得招标文件表数据集
	 * tenderBidFile 招标文件表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidFile getTenderBidFileByTenderBidFile(TenderBidFile tenderBidFile) throws BaseException ;
	
	/**
	 * 获得所有招标文件表数据集
	 * @param tenderBidFile 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidFileList(TenderBidFile tenderBidFile) throws BaseException ;
	
	/**
	 * 获得所有招标文件表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidFile 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidFileList(RollPage rollPage, TenderBidFile tenderBidFile)
			throws BaseException;

}