package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidFileBiz;
import com.ced.sip.purchase.tender.entity.TenderBidFile;
/** 
 * 类名称：TenderBidFileBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidFileBizImpl extends BaseBizImpl implements ITenderBidFileBiz  {
	
	/**
	 * 根据主键获得招标文件表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidFile getTenderBidFile(Long id) throws BaseException {
		return (TenderBidFile)this.getObject(TenderBidFile.class, id);
	}
	
	/**
	 * 获得招标文件表实例
	 * @param tenderBidFile 招标文件表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidFile getTenderBidFile(TenderBidFile tenderBidFile) throws BaseException {
		return (TenderBidFile)this.getObject(TenderBidFile.class, tenderBidFile.getTbfId() );
	}
	
	/**
	 * 添加招标文件信息
	 * @param tenderBidFile 招标文件表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidFile(TenderBidFile tenderBidFile) throws BaseException{
		this.saveObject( tenderBidFile ) ;
	}
	
	/**
	 * 更新招标文件表实例
	 * @param tenderBidFile 招标文件表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidFile(TenderBidFile tenderBidFile) throws BaseException{
		this.updateObject( tenderBidFile ) ;
	}
	
	/**
	 * 删除招标文件表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidFile(String id) throws BaseException {
		this.removeObject( this.getTenderBidFile( new Long(id) ) ) ;
	}
	
	/**
	 * 删除招标文件表实例
	 * @param tenderBidFile 招标文件表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidFile(TenderBidFile tenderBidFile) throws BaseException {
		this.removeObject( tenderBidFile ) ;
	}
	
	/**
	 * 删除招标文件表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidFiles(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidFile.class, id) ;
	}
	
	/**
	 * 获得招标文件表数据集
	 * tenderBidFile 招标文件表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidFile getTenderBidFileByTenderBidFile(TenderBidFile tenderBidFile) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidFile de where 1 = 1 " );
		if(tenderBidFile != null){
			if(StringUtil.isNotBlank(tenderBidFile.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidFile.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbfId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidFile = new TenderBidFile();
		if(list!=null&&list.size()>0){
			tenderBidFile = (TenderBidFile)list.get(0);
		}
		return tenderBidFile;
	}
	
	/**
	 * 获得所有招标文件表数据集
	 * @param tenderBidFile 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidFileList(TenderBidFile tenderBidFile) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidFile de where 1 = 1 " );
		if(tenderBidFile != null){
			if(StringUtil.isNotBlank(tenderBidFile.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidFile.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbfId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有招标文件表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidFile 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidFileList(RollPage rollPage, TenderBidFile tenderBidFile) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidFile de where 1 = 1 " );
		if(tenderBidFile != null){
			if(StringUtil.isNotBlank(tenderBidFile.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidFile.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbfId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
