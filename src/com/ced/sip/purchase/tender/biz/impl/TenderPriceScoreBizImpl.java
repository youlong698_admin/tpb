package com.ced.sip.purchase.tender.biz.impl;

import java.util.ArrayList;
import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderPriceScoreBiz;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
import com.ced.sip.purchase.tender.entity.TenderOtherPriceScore;
import com.ced.sip.purchase.tender.entity.TenderSupplierScore;

public class TenderPriceScoreBizImpl extends BaseBizImpl implements ITenderPriceScoreBiz {

	/**
	 * 取报价汇总 净总价、含税总价
	 * @param rcId 项目id
	 * @param supplierId 供应商ID
	 * @return
	 * @throws BaseException
	 */
	public List getSumBidPriceAa(Long rcId, String supplierId)
			throws BaseException {
        List rList = new ArrayList() ;
			
			StringBuffer hql = new StringBuffer(" select bp.totalPrice, bp.supplierId,si.supplierName  from BidPrice bp,SupplierInfo si where si.supplierId=bp.supplierId " ); 
			hql.append(" and bp.rcId = '").append(rcId).append("' ");
			
			if( StringUtil.isNotBlank( supplierId ) ) {
				hql.append(" and bp.supplierId = ").append( supplierId );
			}
			
			hql.append(" order by bp.totalPrice " ); 
			
			rList = this.getObjects(hql.toString() ); 
		
		return  rList ;
	}
	/**
	 * 根据评分规则类型进行 报价评分计算
	 * 计算各供应商的总报价 计算最高价、最低价、平均价、其他价格值
	 * @param rcId
	 * @param sumpList
	 * @param bpSet
	 * @param bidRate
	 * @return
	 */
	public List getSupplierPriceScore(Long rcId,List<Object[]> sumpList, TenderBidPriceSet bpSet,TenderBidRate bidRate ) {
		List rList = new ArrayList() ;
		Double rate = bidRate.getPriceRate()/100;
		// 最高价
		double maxPrice = 0 ;
		// 平均价
		double avgPrice = 0 ;
		// 最低价
		double minPrice = 0 ;
		
		double sumPrice = 0 ;
		double tmpPrice = 0 ;
		if( sumpList != null && sumpList.size()>0 ) {
			int supCount = 0 ;
			
			// 计算最高价、最低价、平均价
			for( Object[] obj : sumpList ) {
				tmpPrice = (Double) obj[0] ; 
				if( tmpPrice > 0 ) {
					if( supCount == 0 ) {
						maxPrice = avgPrice = minPrice = tmpPrice ;
					}else {
						if( tmpPrice > maxPrice )
							maxPrice = tmpPrice ;
						if( tmpPrice < minPrice )
							minPrice = tmpPrice ;
					}
					sumPrice += tmpPrice ;
					supCount ++ ;
				}
			}
			
			avgPrice = sumPrice / supCount ;
			
			double sumScore 	= 100 ;
			double midScore 	= 0;
			double pointScore 	= 100 * bpSet.getUpPoint();
			double priceScore 	= 0 ;
			for( Object[] obj : sumpList ) {
				TenderSupplierScore tss = new TenderSupplierScore();
				tmpPrice = (Double) obj[0] ; 
				if( tmpPrice > 0 ) {
					if( TableStatus.Bid_Price_Type_01.equals( bpSet.getBrsType() ) ) {
						// 最低价满分
						midScore = ( tmpPrice - minPrice ) / minPrice  ;
						priceScore = sumScore -  ( midScore * pointScore )/rate ;
					}else if( TableStatus.Bid_Price_Type_02.equals( bpSet.getBrsType() ) ) {
						// 平均价满分
						midScore = ( tmpPrice - avgPrice ) / avgPrice ;
						priceScore = sumScore -  Math.abs( midScore * pointScore )/rate ;
					}else if( TableStatus.Bid_Price_Type_03.equals( bpSet.getBrsType() ) ) {
						// 最高价满分
						midScore = ( tmpPrice - maxPrice ) / maxPrice ;
						priceScore = sumScore -  Math.abs( midScore * pointScore )/rate ;
					}else {
						TenderOtherPriceScore bops = new TenderOtherPriceScore();
						bops.setRcId(rcId);
						bops.setSupplierId((Long) obj[1]);
						try {
							List<TenderOtherPriceScore> bopsList = this.getTenderOtherPriceScoreList(bops);
							if(bopsList!=null&&bopsList.size()>0){
								if(StringUtil.isNotBlank(bopsList.get(0).getPriceScore())){
									priceScore = bopsList.get(0).getPriceScore();
								}else{
									priceScore = 0;
								}
							}else{
								priceScore = bpSet.getUpPoint();
							}
						} catch (BaseException e) {
							e.printStackTrace();
						}
					}
					
					tss.setSumPrice( StringUtil.formateNumber(Double.parseDouble(obj[0].toString())) ) ;
					tss.setAvgPrice( StringUtil.formateNumber( avgPrice ) ) ;
					
				}else {
					priceScore = 0 ;
					tss.setSumPrice( "0" ) ;
					tss.setAvgPrice( "0" ) ;
				}
				
				if( priceScore <=0 )
					tss.setPriceScore( "0" );
				else 
					tss.setPriceScore( StringUtil.formateNumber( priceScore ) ) ;
				
				tss.setSupplierId( (Long) obj[1]  ) ;
				tss.setSupplierName( obj[2].toString() ) ;
				
				rList.add( tss ) ;
			}
		}
		
		return rList ;
	}
	/**
	 * 获得所有其他价格规则分值表数据集
	 * @param tenderOtherPriceScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderOtherPriceScoreList(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderOtherPriceScore de where 1 = 1 " );
		if(tenderOtherPriceScore!=null){
			if(StringUtil.isNotBlank(tenderOtherPriceScore.getRcId())){
				hql.append(" and de.rcId=").append(tenderOtherPriceScore.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderOtherPriceScore.getSupplierId())){
				hql.append(" and de.supplierId=").append(tenderOtherPriceScore.getSupplierId());
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 添加其他价格评分信息
	 * @param tenderOtherPriceScore 其他价格评分表实例
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void saveTenderOtherPriceScore(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException{
		this.saveObject( tenderOtherPriceScore ) ;
	}
	
	/**
	 * 更新其他价格评分表实例
	 * @param tenderOtherPriceScore 其他价格评分表实例
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void updateTenderOtherPriceScore(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException{
		this.updateObject( tenderOtherPriceScore ) ;
	}
	/**
	 * 获得其他价格评分表数据集
	 * tenderOtherPriceScore 其他价格评分表实例
	 * @author luguanglei 2017-05-13
	 * @return
	 * @throws BaseException 
	 */
	public TenderOtherPriceScore getTenderOtherPriceScoreByTenderOtherPriceScore(TenderOtherPriceScore tenderOtherPriceScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderOtherPriceScore de where 1 = 1 " );
		if(tenderOtherPriceScore != null){
			if(StringUtil.isNotBlank(tenderOtherPriceScore.getRcId())){
				hql.append(" and de.rcId =").append(tenderOtherPriceScore.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderOtherPriceScore.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderOtherPriceScore.getSupplierId()).append("");
			}
			if(StringUtil.isNotBlank(tenderOtherPriceScore.getTopsId())){
				hql.append(" and de.topsId =").append(tenderOtherPriceScore.getTopsId()).append("");
			}
		}
		hql.append(" order by de.topsId desc ");
		List list = this.getObjects( hql.toString() );
		tenderOtherPriceScore = new TenderOtherPriceScore();
		if(list!=null&&list.size()>0){
			tenderOtherPriceScore = (TenderOtherPriceScore)list.get(0);
		}
		return tenderOtherPriceScore;
	}

}
