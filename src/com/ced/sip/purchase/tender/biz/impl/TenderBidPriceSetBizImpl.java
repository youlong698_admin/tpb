package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidPriceSetBiz;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
/** 
 * 类名称：TenderBidPriceSetBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidPriceSetBizImpl extends BaseBizImpl implements ITenderBidPriceSetBiz  {
	
	/**
	 * 根据主键获得价格评分规则表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidPriceSet getTenderBidPriceSet(Long id) throws BaseException {
		return (TenderBidPriceSet)this.getObject(TenderBidPriceSet.class, id);
	}
	
	/**
	 * 获得价格评分规则表实例
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidPriceSet getTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException {
		return (TenderBidPriceSet)this.getObject(TenderBidPriceSet.class, tenderBidPriceSet.getTbpsId() );
	}
	
	/**
	 * 添加价格评分规则信息
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException{
		this.saveObject( tenderBidPriceSet ) ;
	}
	
	/**
	 * 更新价格评分规则表实例
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException{
		this.updateObject( tenderBidPriceSet ) ;
	}
	
	/**
	 * 删除价格评分规则表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidPriceSet(String id) throws BaseException {
		this.removeObject( this.getTenderBidPriceSet( new Long(id) ) ) ;
	}
	
	/**
	 * 删除价格评分规则表实例
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException {
		this.removeObject( tenderBidPriceSet ) ;
	}
	
	/**
	 * 删除价格评分规则表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidPriceSets(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidPriceSet.class, id) ;
	}
	
	/**
	 * 获得价格评分规则表数据集
	 * tenderBidPriceSet 价格评分规则表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidPriceSet getTenderBidPriceSetByTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidPriceSet de where 1 = 1 " );
		if(tenderBidPriceSet != null){
			if(StringUtil.isNotBlank(tenderBidPriceSet.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidPriceSet.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbpsId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidPriceSet = new TenderBidPriceSet();
		if(list!=null&&list.size()>0){
			tenderBidPriceSet = (TenderBidPriceSet)list.get(0);
		}
		return tenderBidPriceSet;
	}
	
	/**
	 * 获得所有价格评分规则表数据集
	 * @param tenderBidPriceSet 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidPriceSetList(TenderBidPriceSet tenderBidPriceSet) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidPriceSet de where 1 = 1 " );
		if(tenderBidPriceSet != null){
			if(StringUtil.isNotBlank(tenderBidPriceSet.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidPriceSet.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbpsId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有价格评分规则表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidPriceSet 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidPriceSetList(RollPage rollPage, TenderBidPriceSet tenderBidPriceSet) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidPriceSet de where 1 = 1 " );
		if(tenderBidPriceSet != null){
			if(StringUtil.isNotBlank(tenderBidPriceSet.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidPriceSet.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbpsId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
