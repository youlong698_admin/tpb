package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderReceivedBulletinBiz;
import com.ced.sip.purchase.tender.entity.TenderReceivedBulletin;
/** 
 * 类名称：TenderReceivedBulletinBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-11
 */
public class TenderReceivedBulletinBizImpl extends BaseBizImpl implements ITenderReceivedBulletinBiz  {
	
	/**
	 * 根据主键获得回标表表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-11
	 * @return
	 * @throws BaseException 
	 */
	public TenderReceivedBulletin getTenderReceivedBulletin(Long id) throws BaseException {
		return (TenderReceivedBulletin)this.getObject(TenderReceivedBulletin.class, id);
	}
	
	/**
	 * 获得回标表表实例
	 * @param tenderReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-05-11
	 * @return
	 * @throws BaseException 
	 */
	public TenderReceivedBulletin getTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException {
		return (TenderReceivedBulletin)this.getObject(TenderReceivedBulletin.class, tenderReceivedBulletin.getTrbId() );
	}
	
	/**
	 * 添加回标表信息
	 * @param tenderReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-05-11
	 * @throws BaseException 
	 */
	public void saveTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException{
		this.saveObject( tenderReceivedBulletin ) ;
	}
	
	/**
	 * 更新回标表表实例
	 * @param tenderReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-05-11
	 * @throws BaseException 
	 */
	public void updateTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException{
		this.updateObject( tenderReceivedBulletin ) ;
	}
	
	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-11
	 * @throws BaseException 
	 */
	public void deleteTenderReceivedBulletin(String id) throws BaseException {
		this.removeObject( this.getTenderReceivedBulletin( new Long(id) ) ) ;
	}
	
	/**
	 * 删除回标表表实例
	 * @param tenderReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-05-11
	 * @throws BaseException 
	 */
	public void deleteTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException {
		this.removeObject( tenderReceivedBulletin ) ;
	}
	
	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-11
	 * @throws BaseException 
	 */
	public void deleteTenderReceivedBulletins(String[] id) throws BaseException {
		this.removeBatchObject(TenderReceivedBulletin.class, id) ;
	}
	
	/**
	 * 获得回标表表数据集
	 * tenderReceivedBulletin 回标表表实例
	 * @author luguanglei 2017-05-11
	 * @return
	 * @throws BaseException 
	 */
	public TenderReceivedBulletin getTenderReceivedBulletinByTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderReceivedBulletin de where 1 = 1 " );
		if(tenderReceivedBulletin != null){
			if(StringUtil.isNotBlank(tenderReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(tenderReceivedBulletin.getRcId()).append("");
			}if(StringUtil.isNotBlank(tenderReceivedBulletin.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderReceivedBulletin.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.trbId desc ");
		List list = this.getObjects( hql.toString() );
		tenderReceivedBulletin = new TenderReceivedBulletin();
		if(list!=null&&list.size()>0){
			tenderReceivedBulletin = (TenderReceivedBulletin)list.get(0);
		}
		return tenderReceivedBulletin;
	}
	
	/**
	 * 获得所有回标表表数据集
	 * @param tenderReceivedBulletin 查询参数对象
	 * @author luguanglei 2017-05-11
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderReceivedBulletinList(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderReceivedBulletin de where 1 = 1 " );
		if(tenderReceivedBulletin != null){
			if(StringUtil.isNotBlank(tenderReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(tenderReceivedBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderReceivedBulletin.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderReceivedBulletin.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.trbId ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有回标表表数据  总数
	 * @param tenderReceivedBulletin 查询参数对象
	 * @author luguanglei 2017-05-11
	 * @return
	 * @throws BaseException 
	 */
	public int countTenderReceivedBulletinList(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.trbId) from TenderReceivedBulletin de where 1 = 1 " );
		if(tenderReceivedBulletin != null){
			if(StringUtil.isNotBlank(tenderReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(tenderReceivedBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderReceivedBulletin.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderReceivedBulletin.getSupplierId()).append("");
			}
		}
		return this.countObjects( hql.toString() );
	}
	/**
	 * 获得所有回标表表数据集
	 * @param rollPage 分页对象
	 * @param tenderReceivedBulletin 查询参数对象
	 * @author luguanglei 2017-05-11
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderReceivedBulletinList(RollPage rollPage, TenderReceivedBulletin tenderReceivedBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderReceivedBulletin de where 1 = 1 " );
		if(tenderReceivedBulletin != null){
			if(StringUtil.isNotBlank(tenderReceivedBulletin.getRcId())){
				hql.append(" and de.rcId =").append(tenderReceivedBulletin.getRcId()).append("");
			}if(StringUtil.isNotBlank(tenderReceivedBulletin.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderReceivedBulletin.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.trbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
