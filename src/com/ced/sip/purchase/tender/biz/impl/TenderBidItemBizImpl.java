package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidItemBiz;
import com.ced.sip.purchase.tender.entity.TenderBidItem;
/** 
 * 类名称：TenderBidItemBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidItemBizImpl extends BaseBizImpl implements ITenderBidItemBiz  {
	
	/**
	 * 根据主键获得招标项目评分设置表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidItem getTenderBidItem(Long id) throws BaseException {
		return (TenderBidItem)this.getObject(TenderBidItem.class, id);
	}
	
	/**
	 * 获得招标项目评分设置表实例
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidItem getTenderBidItem(TenderBidItem tenderBidItem) throws BaseException {
		return (TenderBidItem)this.getObject(TenderBidItem.class, tenderBidItem.getTbiId() );
	}
	
	/**
	 * 添加招标项目评分设置信息
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidItem(TenderBidItem tenderBidItem) throws BaseException{
		this.saveObject( tenderBidItem ) ;
	}
	
	/**
	 * 更新招标项目评分设置表实例
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidItem(TenderBidItem tenderBidItem) throws BaseException{
		this.updateObject( tenderBidItem ) ;
	}
	
	/**
	 * 删除招标项目评分设置表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidItem(String id) throws BaseException {
		this.removeObject( this.getTenderBidItem( new Long(id) ) ) ;
	}
	
	/**
	 * 删除招标项目评分设置表实例
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidItem(TenderBidItem tenderBidItem) throws BaseException {
		this.removeObject( tenderBidItem ) ;
	}
	
	/**
	 * 删除招标项目评分设置表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidItems(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidItem.class, id) ;
	}
	
	/**
	 * 获得招标项目评分设置表数据集
	 * tenderBidItem 招标项目评分设置表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidItem getTenderBidItemByTenderBidItem(TenderBidItem tenderBidItem) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidItem de where 1 = 1 " );
		if(tenderBidItem != null){
			if(StringUtil.isNotBlank(tenderBidItem.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidItem.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderBidItem.getItemType())){
				hql.append(" and de.itemType ='").append(tenderBidItem.getItemType()).append("'");
			}
		}
		hql.append(" order by de.tbiId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidItem = new TenderBidItem();
		if(list!=null&&list.size()>0){
			tenderBidItem = (TenderBidItem)list.get(0);
		}
		return tenderBidItem;
	}
	
	/**
	 * 获得所有招标项目评分设置表数据集
	 * @param tenderBidItem 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidItemList(TenderBidItem tenderBidItem) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidItem de where 1 = 1 " );
		if(tenderBidItem != null){
			if(StringUtil.isNotBlank(tenderBidItem.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidItem.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderBidItem.getItemType())){
				hql.append(" and de.itemType ='").append(tenderBidItem.getItemType()).append("'");
			}
		}
		hql.append(" order by de.tbiId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有招标项目评分设置表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidItem 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidItemList(RollPage rollPage, TenderBidItem tenderBidItem) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidItem de where 1 = 1 " );
		if(tenderBidItem != null){
			if(StringUtil.isNotBlank(tenderBidItem.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidItem.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderBidItem.getItemType())){
				hql.append(" and de.itemType ='").append(tenderBidItem.getItemType()).append("'");
			}
		}
		hql.append(" order by de.tbiId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
