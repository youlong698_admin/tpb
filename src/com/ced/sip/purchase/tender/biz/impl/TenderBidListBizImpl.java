package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
/** 
 * 类名称：TenderBidListBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidListBizImpl extends BaseBizImpl implements ITenderBidListBiz  {
	
	/**
	 * 根据主键获得招标信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidList getTenderBidList(Long id) throws BaseException {
		return (TenderBidList)this.getObject(TenderBidList.class, id);
	}
	
	/**
	 * 获得招标信息表实例
	 * @param tenderBidList 招标信息表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidList getTenderBidList(TenderBidList tenderBidList) throws BaseException {
		return (TenderBidList)this.getObject(TenderBidList.class, tenderBidList.getTblId() );
	}
	
	/**
	 * 添加招标信息信息
	 * @param tenderBidList 招标信息表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidList(TenderBidList tenderBidList) throws BaseException{
		this.saveObject( tenderBidList ) ;
	}
	
	/**
	 * 更新招标信息表实例
	 * @param tenderBidList 招标信息表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidList(TenderBidList tenderBidList) throws BaseException{
		this.updateObject( tenderBidList ) ;
	}
	
	/**
	 * 删除招标信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidList(String id) throws BaseException {
		this.removeObject( this.getTenderBidList( new Long(id) ) ) ;
	}
	
	/**
	 * 删除招标信息表实例
	 * @param tenderBidList 招标信息表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidList(TenderBidList tenderBidList) throws BaseException {
		this.removeObject( tenderBidList ) ;
	}
	
	/**
	 * 删除招标信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidLists(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidList.class, id) ;
	}
	
	/**
	 * 获得招标信息表数据集
	 * tenderBidList 招标信息表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidList getTenderBidListByTenderBid(TenderBidList tenderBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidList de where 1 = 1 " );
		if(tenderBidList != null){
			if(StringUtil.isNotBlank(tenderBidList.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.tblId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidList = new TenderBidList();
		if(list!=null&&list.size()>0){
			tenderBidList = (TenderBidList)list.get(0);
		}
		return tenderBidList;
	}
	
	/**
	 * 获得所有招标信息表数据集
	 * @param tenderBidList 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidListList(TenderBidList tenderBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidList de where 1 = 1 " );
		if(tenderBidList != null){
			if(StringUtil.isNotBlank(tenderBidList.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.tblId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有招标信息表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidList 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidListList(RollPage rollPage, TenderBidList tenderBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidList de where 1 = 1 " );
		if(tenderBidList != null){
			if(StringUtil.isNotBlank(tenderBidList.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.tblId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 根据rcId获得招标计划信息表实例
	 * @param rcId 项目ID
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidList getTenderBidListByRcId(Long rcId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidList de where de.rcId =").append(rcId);
        List list = this.getObjects( hql.toString() );
        TenderBidList tenderBidList = new TenderBidList();
		if(list!=null&&list.size()>0){
			tenderBidList = (TenderBidList)list.get(0);
		}
		return tenderBidList;
	}

	/**
	 * 根据项目id更新评标状态 专家评标时候更新状态
	 * @param rcId
	 * @throws BaseException
	 */
	public void updateCalibtationStatus(Long rcId) throws BaseException {
		String sql="update tender_bid_list t set t.calibtation_status='02' where t.rc_id="+rcId;
		this.updateJdbcSql(sql);
		
	}
	
}
