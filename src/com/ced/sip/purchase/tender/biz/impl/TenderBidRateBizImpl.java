package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidRateBiz;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
/** 
 * 类名称：TenderBidRateBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidRateBizImpl extends BaseBizImpl implements ITenderBidRateBiz  {
	
	/**
	 * 根据主键获得评标权重表表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidRate getTenderBidRate(Long id) throws BaseException {
		return (TenderBidRate)this.getObject(TenderBidRate.class, id);
	}
	
	/**
	 * 获得评标权重表表实例
	 * @param tenderBidRate 评标权重表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidRate getTenderBidRate(TenderBidRate tenderBidRate) throws BaseException {
		return (TenderBidRate)this.getObject(TenderBidRate.class, tenderBidRate.getTbrId() );
	}
	
	/**
	 * 添加评标权重表信息
	 * @param tenderBidRate 评标权重表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidRate(TenderBidRate tenderBidRate) throws BaseException{
		this.saveObject( tenderBidRate ) ;
	}
	
	/**
	 * 更新评标权重表表实例
	 * @param tenderBidRate 评标权重表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidRate(TenderBidRate tenderBidRate) throws BaseException{
		this.updateObject( tenderBidRate ) ;
	}
	
	/**
	 * 删除评标权重表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidRate(String id) throws BaseException {
		this.removeObject( this.getTenderBidRate( new Long(id) ) ) ;
	}
	
	/**
	 * 删除评标权重表表实例
	 * @param tenderBidRate 评标权重表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidRate(TenderBidRate tenderBidRate) throws BaseException {
		this.removeObject( tenderBidRate ) ;
	}
	
	/**
	 * 删除评标权重表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidRates(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidRate.class, id) ;
	}
	
	/**
	 * 获得评标权重表表数据集
	 * tenderBidRate 评标权重表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidRate getTenderBidRateByTenderBidRate(TenderBidRate tenderBidRate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidRate de where 1 = 1 " );
		if(tenderBidRate != null){
			if(StringUtil.isNotBlank(tenderBidRate.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidRate.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbrId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidRate = new TenderBidRate();
		if(list!=null&&list.size()>0){
			tenderBidRate = (TenderBidRate)list.get(0);
		}
		return tenderBidRate;
	}
	
	/**
	 * 获得所有评标权重表表数据集
	 * @param tenderBidRate 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidRateList(TenderBidRate tenderBidRate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidRate de where 1 = 1 " );
		if(tenderBidRate != null){
			if(StringUtil.isNotBlank(tenderBidRate.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidRate.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbrId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有评标权重表表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidRate 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidRateList(RollPage rollPage, TenderBidRate tenderBidRate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidRate de where 1 = 1 " );
		if(tenderBidRate != null){
			if(StringUtil.isNotBlank(tenderBidRate.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidRate.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbrId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
