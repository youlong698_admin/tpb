package com.ced.sip.purchase.tender.biz.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderJudgeTechScoreBiz;
import com.ced.sip.purchase.tender.entity.TenderJudgeSum;
import com.ced.sip.purchase.tender.entity.TenderJudgeTechScore;
/** 
 * 类名称：TenderJudgeTechScoreBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderJudgeTechScoreBizImpl extends BaseBizImpl implements ITenderJudgeTechScoreBiz  {
	
	/**
	 * 根据主键获得技术评分表表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderJudgeTechScore getTenderJudgeTechScore(Long id) throws BaseException {
		return (TenderJudgeTechScore)this.getObject(TenderJudgeTechScore.class, id);
	}
	
	/**
	 * 获得技术评分表表实例
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderJudgeTechScore getTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException {
		return (TenderJudgeTechScore)this.getObject(TenderJudgeTechScore.class, tenderJudgeTechScore.getTjstId() );
	}
	
	/**
	 * 添加技术评分表信息
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException{
		this.saveObject( tenderJudgeTechScore ) ;
	}
	
	/**
	 * 更新技术评分表表实例
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException{
		this.updateObject( tenderJudgeTechScore ) ;
	}
	
	/**
	 * 删除技术评分表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderJudgeTechScore(String id) throws BaseException {
		this.removeObject( this.getTenderJudgeTechScore( new Long(id) ) ) ;
	}
	
	/**
	 * 删除技术评分表表实例
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException {
		this.removeObject( tenderJudgeTechScore ) ;
	}
	
	/**
	 * 删除技术评分表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderJudgeTechScores(String[] id) throws BaseException {
		this.removeBatchObject(TenderJudgeTechScore.class, id) ;
	}
	
	/**
	 * 获得技术评分表表数据集
	 * tenderJudgeTechScore 技术评分表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderJudgeTechScore getTenderJudgeTechScoreByTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderJudgeTechScore de where 1 = 1 " );

		hql.append(" order by de.tjstId desc ");
		List list = this.getObjects( hql.toString() );
		tenderJudgeTechScore = new TenderJudgeTechScore();
		if(list!=null&&list.size()>0){
			tenderJudgeTechScore = (TenderJudgeTechScore)list.get(0);
		}
		return tenderJudgeTechScore;
	}
	
	/**
	 * 获得所有技术评分表表数据集
	 * @param tenderJudgeTechScore 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderJudgeTechScoreList(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderJudgeTechScore de where 1 = 1 " );
		if(tenderJudgeTechScore!=null){
			if(StringUtil.isNotBlank(tenderJudgeTechScore.getRcId())){
				hql.append(" and de.rcId = '").append(tenderJudgeTechScore.getRcId()).append("'");
			}
			if(!StringUtil.isBlank(tenderJudgeTechScore.getSupplierId())){
				hql.append(" and de.supplierId = ").append(tenderJudgeTechScore.getSupplierId());
			}
			if(!StringUtil.isBlank(tenderJudgeTechScore.getExpertId())){
				hql.append(" and de.expertId = ").append(tenderJudgeTechScore.getExpertId());
			}
		}
		hql.append(" order by de.tjstId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有技术评分表表数据集
	 * @param rollPage 分页对象
	 * @param tenderJudgeTechScore 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderJudgeTechScoreList(RollPage rollPage, TenderJudgeTechScore tenderJudgeTechScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderJudgeTechScore de where 1 = 1 " );

		hql.append(" order by de.tjstId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获取技术评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	public List getTenderJudgeTechScoreListForGatherTab(Long[] supplierIdStr,Long rcId) throws BaseException{
		int columnCount=2+supplierIdStr.length;
		StringBuffer hql = new StringBuffer(" select tbi.item_name,tbi.points" );
		for(Long supplierId:supplierIdStr){
			hql.append(",tjtsv"+supplierId+".avg_points avg_points"+supplierId);
		}
		hql.append("  from tender_bid_item tbi ");
		for(Long supplierId:supplierIdStr){
			hql.append(" left join (select item_Id,avg_points from tender_judge_tech_score_view where supplier_id="+supplierId+" and rc_id="+rcId+" ) tjtsv"+supplierId+" on tbi.item_id=tjtsv"+supplierId+".item_id");
		}
		hql.append("  where tbi.rc_id="+rcId+" and tbi.item_type='0'");
		return this.getObjects(hql.toString(), columnCount);
	}
	/**
	  * 获取单个专家技术评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */	
	public List getTenderJudgeTechScoreListForGatherTabByExpertId(Long[] supplierIdStr,Long rcId,Long expertId) throws BaseException{
		int columnCount=2+supplierIdStr.length;
		StringBuffer hql = new StringBuffer(" select tbi.item_name,tbi.points" );
		for(Long supplierId:supplierIdStr){
			hql.append(",tjbsv"+supplierId+".judge_points judge_points"+supplierId);
		}
		hql.append("  from tender_bid_item tbi ");
		for(Long supplierId:supplierIdStr){
			hql.append(" left join (select item_Id,judge_points from tender_judge_tech_score where supplier_id="+supplierId+" and rc_id="+rcId+" and expert_id="+expertId+"  ) tjbsv"+supplierId+" on tbi.item_id=tjbsv"+supplierId+".item_id");
		}
		hql.append("  where tbi.rc_id="+rcId+" and tbi.item_type='0'");
		return this.getObjects(hql.toString(), columnCount);
	}
	/***
	 * 根据项目Id 查询每个供应商的技术评分综合
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	public Map<Long,Double> getTenderJudgeTechScoreMap(Long rcId) throws BaseException{
		String sql="select supplier_id,sum(avg_points) sumPoints from tender_judge_tech_score_view where rc_Id="+rcId+" group by supplier_id";
		Map<Long,Double> map=new HashMap<Long, Double>();
		List list=this.getObjectsList(TenderJudgeSum.class,sql);
		TenderJudgeSum tenderJudgeSum=null;
		for(int i=0;i<list.size();i++){
			tenderJudgeSum=(TenderJudgeSum)list.get(i);
			map.put(tenderJudgeSum.getSupplierId(), tenderJudgeSum.getSumPoints());
		}
		return map;
	}
	/***
	 * 根据项目Id 和专家Id 查询每个供应商的单个专家技术评分之和
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */
	public Map<Long,Double> getTenderJudgeTechScoreMapByExpertId(Long rcId,Long expertId) throws BaseException{
		String sql="select supplier_id,sum(judge_points) sumPoints from tender_judge_tech_score where rc_Id="+rcId+" and expert_id="+expertId+" group by supplier_id";
		Map<Long,Double> map=new HashMap<Long, Double>();
		List list=this.getObjectsList(TenderJudgeSum.class,sql);
		TenderJudgeSum tenderJudgeSum=null;
		for(int i=0;i<list.size();i++){
			tenderJudgeSum=(TenderJudgeSum)list.get(i);
			map.put(tenderJudgeSum.getSupplierId(), tenderJudgeSum.getSumPoints());
		}
		return map;
	}
	/**
	 * 获得不同字段不重复值的个数数据集
	 * @param judgeTechScore 查询参数对象
	 * @author zhuyu 2014-08-25
	 * @return
	 * @throws BaseException 
	 */
	public List getDiffrentTechJudgeList(  String cloumns ,TenderJudgeTechScore judgeTechScore,String hqlString) throws BaseException {
		StringBuffer hql = new StringBuffer(" select distinct "+cloumns+" from TenderJudgeTechScore de where 1 = 1 " );
		if(judgeTechScore!=null){
			if(StringUtil.isNotBlank(judgeTechScore.getRcId())){
				hql.append(" and de.rcId = '").append(judgeTechScore.getRcId()).append("'");
			}
			if(!StringUtil.isBlank(judgeTechScore.getExpertId())){
				hql.append(" and de.expertId = ").append(judgeTechScore.getExpertId());
			}
		}
		if(StringUtil.isNotBlank(hqlString)){
			hql.append(hqlString);
		}
		hql.append(" order by "+cloumns+" desc ");
		return this.getObjects( hql.toString() );
	}

	/**
	 * 根据项目id和专家id删除专家的评标信息
	 * @param rcId
	 * @param expertId
	 * @throws BaseException
	 */
	public void deleteTenderJudgeTechScoreByRcIdAndExpertId(Long rcId,
			Long expertId) throws BaseException {
		String sql="delete from tender_judge_tech_score where rc_id="+rcId+" and expert_id="+expertId;
		this.updateJdbcSql(sql);
		
	}
	
}
