package com.ced.sip.purchase.tender.biz.impl;

import java.util.Date;
import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeBiz;
import com.ced.sip.purchase.tender.entity.TenderBidJudge;
/** 
 * 类名称：TenderBidJudgeBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidJudgeBizImpl extends BaseBizImpl implements ITenderBidJudgeBiz  {
	
	/**
	 * 根据主键获得评标专家表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudge getTenderBidJudge(Long id) throws BaseException {
		return (TenderBidJudge)this.getObject(TenderBidJudge.class, id);
	}
	
	/**
	 * 获得评标专家表实例
	 * @param tenderBidJudge 评标专家表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudge getTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException {
		return (TenderBidJudge)this.getObject(TenderBidJudge.class, tenderBidJudge.getTbjId() );
	}
	
	/**
	 * 添加评标专家信息
	 * @param tenderBidJudge 评标专家表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException{
		this.saveObject( tenderBidJudge ) ;
	}
	
	/**
	 * 更新评标专家表实例
	 * @param tenderBidJudge 评标专家表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException{
		this.updateObject( tenderBidJudge ) ;
	}
	
	/**
	 * 删除评标专家表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudge(String id) throws BaseException {
		this.removeObject( this.getTenderBidJudge( new Long(id) ) ) ;
	}
	
	/**
	 * 删除评标专家表实例
	 * @param tenderBidJudge 评标专家表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException {
		this.removeObject( tenderBidJudge ) ;
	}
	
	/**
	 * 删除评标专家表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudges(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidJudge.class, id) ;
	}
	
	/**
	 * 获得评标专家表数据集
	 * tenderBidJudge 评标专家表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudge getTenderBidJudgeByTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudge de where 1 = 1 " );

		hql.append(" order by de.tbjId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidJudge = new TenderBidJudge();
		if(list!=null&&list.size()>0){
			tenderBidJudge = (TenderBidJudge)list.get(0);
		}
		return tenderBidJudge;
	}
	
	/**
	 * 获得所有评标专家表数据集
	 * @param tenderBidJudge 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidJudgeList(TenderBidJudge tenderBidJudge) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,ex.expertName,ex.companyName,ex.expertMajor,ex.mobilNumber from TenderBidJudge de,Expert ex where de.expertId=ex.expertId " );
		if(tenderBidJudge != null){
			if(StringUtil.isNotBlank(tenderBidJudge.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidJudge.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbjId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有评标专家表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidJudge 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidJudgeList(RollPage rollPage, TenderBidJudge tenderBidJudge) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudge de where 1 = 1 " );
		if(tenderBidJudge != null){
			if(StringUtil.isNotBlank(tenderBidJudge.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidJudge.getRcId()).append("");
			}
		}
		hql.append(" order by de.tbjId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 查询今日开标的项目信息
	 * @param experId 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getIsTenderBidJudgeList(Long experId) throws BaseException {
		StringBuffer hql = new StringBuffer(" select bj,rc.buyRemark,rc.bidCode,bl.openDate from TenderBidJudge bj,TenderBidList bl,RequiredCollect rc where rc.rcId=bj.rcId and bl.rcId=rc.rcId ");
		hql.append(" and bj.expertId = ").append(experId).append("");
		hql.append(" and trunc(bl.openDate,'dd') <= to_date('").append(DateUtil.getDefaultDateFormat(new Date())).append("','yyyy-MM-dd')");
		hql.append(" and trunc(bl.openDate,'dd') >= to_date('").append(DateUtil.getDefaultDateFormat(new Date())).append("','yyyy-MM-dd')");
			
		return this.getObjects(hql.toString());
	}
	/**
	 * 查询今日开标的项目信息
	 * @param experId bidCode buyRemark查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getHistoryTenderBidJudgeList(Long experId,String bidCode,String buyRemark) throws BaseException {
		StringBuffer hql = new StringBuffer(" select bj,rc.buyRemark,rc.bidCode,bl.openDate from TenderBidJudge bj,TenderBidList bl,RequiredCollect rc where rc.rcId=bj.rcId and bl.rcId=rc.rcId  ");
		hql.append(" and bj.expertId = ").append(experId).append("");
		if(StringUtil.isNotBlank(bidCode)){
		   hql.append(" and rc.bodCode like '%").append(bidCode).append("%'");
		}
		if(StringUtil.isNotBlank(buyRemark)){
		    hql.append(" and rc.buyRemark like '%").append(buyRemark).append("%'");
		}
			
		return this.getObjects(hql.toString());
	}
	
}
