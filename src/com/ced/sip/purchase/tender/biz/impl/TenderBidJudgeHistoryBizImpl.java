package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeHistoryBiz;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeHistory;
/** 
 * 类名称：TenderBidJudgeHistoryBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidJudgeHistoryBizImpl extends BaseBizImpl implements ITenderBidJudgeHistoryBiz  {
	
	/**
	 * 根据主键获得评标专家历史表表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudgeHistory getTenderBidJudgeHistory(Long id) throws BaseException {
		return (TenderBidJudgeHistory)this.getObject(TenderBidJudgeHistory.class, id);
	}
	
	/**
	 * 获得评标专家历史表表实例
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudgeHistory getTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException {
		return (TenderBidJudgeHistory)this.getObject(TenderBidJudgeHistory.class, tenderBidJudgeHistory.getTbjhId() );
	}
	
	/**
	 * 添加评标专家历史表信息
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException{
		this.saveObject( tenderBidJudgeHistory ) ;
	}
	
	/**
	 * 更新评标专家历史表表实例
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException{
		this.updateObject( tenderBidJudgeHistory ) ;
	}
	
	/**
	 * 删除评标专家历史表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudgeHistory(String id) throws BaseException {
		this.removeObject( this.getTenderBidJudgeHistory( new Long(id) ) ) ;
	}
	
	/**
	 * 删除评标专家历史表表实例
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException {
		this.removeObject( tenderBidJudgeHistory ) ;
	}
	
	/**
	 * 删除评标专家历史表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudgeHistorys(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidJudgeHistory.class, id) ;
	}
	
	/**
	 * 获得评标专家历史表表数据集
	 * tenderBidJudgeHistory 评标专家历史表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudgeHistory getTenderBidJudgeHistoryByTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudgeHistory de where 1 = 1 " );

		hql.append(" order by de.tbjhId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidJudgeHistory = new TenderBidJudgeHistory();
		if(list!=null&&list.size()>0){
			tenderBidJudgeHistory = (TenderBidJudgeHistory)list.get(0);
		}
		return tenderBidJudgeHistory;
	}
	
	/**
	 * 获得所有评标专家历史表表数据集
	 * @param tenderBidJudgeHistory 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidJudgeHistoryList(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudgeHistory de where 1 = 1 " );

		hql.append(" order by de.tbjhId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有评标专家历史表表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidJudgeHistory 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidJudgeHistoryList(RollPage rollPage, TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudgeHistory de where 1 = 1 " );

		hql.append(" order by de.tbjhId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 取最大抽取次数
	 * @param bidCode 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public int getMaxExpertSelectNumber(Long rcId) throws BaseException {
		int iMaxSelectTimes = 0;

		StringBuffer sHql = new StringBuffer();
		sHql.append("select Max( tbjdh.selectNumber ) From TenderBidJudgeHistory tbjdh ");
		sHql.append("	where tbjdh.rcId = ").append(rcId).append("");
		
		List lTemp = this.getObjects( sHql.toString() );
		//取得最大次数
		if( lTemp != null && lTemp.size() > 0 ){
			if( lTemp.get(0) != null ){
				iMaxSelectTimes = ((Short)lTemp.get(0)).intValue();
			}
		}
		
		return iMaxSelectTimes;
	}
	
}
