package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeTypeBiz;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeType;
/** 
 * 类名称：TenderBidJudgeTypeBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-13
 */
public class TenderBidJudgeTypeBizImpl extends BaseBizImpl implements ITenderBidJudgeTypeBiz  {
	
	/**
	 * 根据主键获得评标专家类别表表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-13
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudgeType getTenderBidJudgeType(Long id) throws BaseException {
		return (TenderBidJudgeType)this.getObject(TenderBidJudgeType.class, id);
	}
	
	/**
	 * 获得评标专家类别表表实例
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @author luguanglei 2017-05-13
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudgeType getTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException {
		return (TenderBidJudgeType)this.getObject(TenderBidJudgeType.class, tenderBidJudgeType.getTbjtId() );
	}
	
	/**
	 * 添加评标专家类别表信息
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void saveTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException{
		this.saveObject( tenderBidJudgeType ) ;
	}
	
	/**
	 * 更新评标专家类别表表实例
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void updateTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException{
		this.updateObject( tenderBidJudgeType ) ;
	}
	
	/**
	 * 删除评标专家类别表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudgeType(String id) throws BaseException {
		this.removeObject( this.getTenderBidJudgeType( new Long(id) ) ) ;
	}
	
	/**
	 * 删除评标专家类别表表实例
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException {
		this.removeObject( tenderBidJudgeType ) ;
	}
	
	/**
	 * 删除评标专家类别表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-13
	 * @throws BaseException 
	 */
	public void deleteTenderBidJudgeTypes(String[] id) throws BaseException {
		this.removeBatchObject(TenderBidJudgeType.class, id) ;
	}
	
	/**
	 * 获得评标专家类别表表数据集
	 * tenderBidJudgeType 评标专家类别表表实例
	 * @author luguanglei 2017-05-13
	 * @return
	 * @throws BaseException 
	 */
	public TenderBidJudgeType getTenderBidJudgeTypeByTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudgeType de where 1 = 1 " );
		if(tenderBidJudgeType != null){
			if(StringUtil.isNotBlank(tenderBidJudgeType.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidJudgeType.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderBidJudgeType.getTableName())){
				hql.append(" and de.tableName ='").append(tenderBidJudgeType.getTableName()).append("'");
			}
		}
		hql.append(" order by de.tbjtId desc ");
		List list = this.getObjects( hql.toString() );
		tenderBidJudgeType = new TenderBidJudgeType();
		if(list!=null&&list.size()>0){
			tenderBidJudgeType = (TenderBidJudgeType)list.get(0);
		}
		return tenderBidJudgeType;
	}
	
	/**
	 * 获得所有评标专家类别表表数据集
	 * @param tenderBidJudgeType 查询参数对象
	 * @author luguanglei 2017-05-13
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidJudgeTypeList(TenderBidJudgeType tenderBidJudgeType) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,ex.expertName from TenderBidJudgeType de,Expert ex where de.expertId=ex.expertId " );
		if(tenderBidJudgeType != null){
			if(StringUtil.isNotBlank(tenderBidJudgeType.getRcId())){
				hql.append(" and de.rcId =").append(tenderBidJudgeType.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderBidJudgeType.getTableName())){
				hql.append(" and de.tableName ='").append(tenderBidJudgeType.getTableName()).append("'");
			}
			if(StringUtil.isNotBlank(tenderBidJudgeType.getTbjtId())){
				hql.append(" and de.tbjtId =").append(tenderBidJudgeType.getTbjtId()).append("");
			}
		}
		hql.append(" order by de.tbjtId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有评标专家类别表表数据集
	 * @param tbjId 查询参数对象
	 * @author luguanglei 2017-05-13
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderBidJudgeTypeListByTbjId(Long tbjId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderBidJudgeType de where 1 = 1 " );
		hql.append(" and de.tbjId =").append(tbjId).append("");
		
		hql.append(" order by de.tableName desc ");
		return this.getObjects(hql.toString() );
	}

	/**
	 * 根据评标专家表主键删除评标专家类别
	 * @param tbjId
	 * @throws BaseException
	 */
	public void deleTenderBidJudgeType(Long tbjId) throws BaseException {
		String sql="delete from tender_bid_judge_type where tbj_id="+tbjId;
		this.updateJdbcSql(sql);
		
	}
	
}
