package com.ced.sip.purchase.tender.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderFileDownloadBiz;
import com.ced.sip.purchase.tender.entity.TenderFileDownload;
/** 
 * 类名称：TenderFileDownloadBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderFileDownloadBizImpl extends BaseBizImpl implements ITenderFileDownloadBiz  {
	
	/**
	 * 根据主键获得标书下载表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderFileDownload getTenderFileDownload(Long id) throws BaseException {
		return (TenderFileDownload)this.getObject(TenderFileDownload.class, id);
	}
	
	/**
	 * 获得标书下载表实例
	 * @param tenderFileDownload 标书下载表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderFileDownload getTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException {
		return (TenderFileDownload)this.getObject(TenderFileDownload.class, tenderFileDownload.getTfdId() );
	}
	
	/**
	 * 添加标书下载信息
	 * @param tenderFileDownload 标书下载表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException{
		this.saveObject( tenderFileDownload ) ;
	}
	
	/**
	 * 更新标书下载表实例
	 * @param tenderFileDownload 标书下载表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException{
		this.updateObject( tenderFileDownload ) ;
	}
	
	/**
	 * 删除标书下载表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderFileDownload(String id) throws BaseException {
		this.removeObject( this.getTenderFileDownload( new Long(id) ) ) ;
	}
	
	/**
	 * 删除标书下载表实例
	 * @param tenderFileDownload 标书下载表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException {
		this.removeObject( tenderFileDownload ) ;
	}
	
	/**
	 * 删除标书下载表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderFileDownloads(String[] id) throws BaseException {
		this.removeBatchObject(TenderFileDownload.class, id) ;
	}
	
	/**
	 * 获得标书下载表数据集
	 * tenderFileDownload 标书下载表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderFileDownload getTenderFileDownloadByTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderFileDownload de where 1 = 1 " );
		if(tenderFileDownload != null){
			if(StringUtil.isNotBlank(tenderFileDownload.getRcId())){
				hql.append(" and de.rcId =").append(tenderFileDownload.getRcId()).append("");
			}
		}
		hql.append(" order by de.tfdId desc ");
		List list = this.getObjects( hql.toString() );
		tenderFileDownload = new TenderFileDownload();
		if(list!=null&&list.size()>0){
			tenderFileDownload = (TenderFileDownload)list.get(0);
		}
		return tenderFileDownload;
	}
	
	/**
	 * 获得所有标书下载表数据集
	 * @param tenderFileDownload 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderFileDownloadList(TenderFileDownload tenderFileDownload) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderFileDownload de where 1 = 1 " );
		if(tenderFileDownload != null){
			if(StringUtil.isNotBlank(tenderFileDownload.getRcId())){
				hql.append(" and de.rcId =").append(tenderFileDownload.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderFileDownload.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderFileDownload.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.tfdId ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有标书下载表数据的总数
	 * @param tenderFileDownload 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public int countTenderFileDownloadList(TenderFileDownload tenderFileDownload) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.tfdId) from TenderFileDownload de where 1 = 1 " );
		if(tenderFileDownload != null){
			if(StringUtil.isNotBlank(tenderFileDownload.getRcId())){
				hql.append(" and de.rcId =").append(tenderFileDownload.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(tenderFileDownload.getSupplierId())){
				hql.append(" and de.supplierId =").append(tenderFileDownload.getSupplierId()).append("");
			}
		}
		return this.countObjects( hql.toString() );
	}
	/**
	 * 获得所有标书下载表数据集
	 * @param rollPage 分页对象
	 * @param tenderFileDownload 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderFileDownloadList(RollPage rollPage, TenderFileDownload tenderFileDownload) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderFileDownload de where 1 = 1 " );
		if(tenderFileDownload != null){
			if(StringUtil.isNotBlank(tenderFileDownload.getRcId())){
				hql.append(" and de.rcId =").append(tenderFileDownload.getRcId()).append("");
			}
		}
		hql.append(" order by de.tfdId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
