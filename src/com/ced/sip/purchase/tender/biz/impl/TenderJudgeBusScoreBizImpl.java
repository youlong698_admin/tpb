package com.ced.sip.purchase.tender.biz.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderJudgeBusScoreBiz;
import com.ced.sip.purchase.tender.entity.TenderJudgeBusScore;
import com.ced.sip.purchase.tender.entity.TenderJudgeSum;
/** 
 * 类名称：TenderJudgeBusScoreBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderJudgeBusScoreBizImpl extends BaseBizImpl implements ITenderJudgeBusScoreBiz  {
	
	/**
	 * 根据主键获得评委商务评分表表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderJudgeBusScore getTenderJudgeBusScore(Long id) throws BaseException {
		return (TenderJudgeBusScore)this.getObject(TenderJudgeBusScore.class, id);
	}
	
	/**
	 * 获得评委商务评分表表实例
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderJudgeBusScore getTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException {
		return (TenderJudgeBusScore)this.getObject(TenderJudgeBusScore.class, tenderJudgeBusScore.getTjbsId() );
	}
	
	/**
	 * 添加评委商务评分表信息
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void saveTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException{
		this.saveObject( tenderJudgeBusScore ) ;
	}
	
	/**
	 * 更新评委商务评分表表实例
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void updateTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException{
		this.updateObject( tenderJudgeBusScore ) ;
	}
	
	/**
	 * 删除评委商务评分表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderJudgeBusScore(String id) throws BaseException {
		this.removeObject( this.getTenderJudgeBusScore( new Long(id) ) ) ;
	}
	
	/**
	 * 删除评委商务评分表表实例
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException {
		this.removeObject( tenderJudgeBusScore ) ;
	}
	
	/**
	 * 删除评委商务评分表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-10
	 * @throws BaseException 
	 */
	public void deleteTenderJudgeBusScores(String[] id) throws BaseException {
		this.removeBatchObject(TenderJudgeBusScore.class, id) ;
	}
	
	/**
	 * 获得评委商务评分表表数据集
	 * tenderJudgeBusScore 评委商务评分表表实例
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public TenderJudgeBusScore getTenderJudgeBusScoreByTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderJudgeBusScore de where 1 = 1 " );

		hql.append(" order by de.tjbsId desc ");
		List list = this.getObjects( hql.toString() );
		tenderJudgeBusScore = new TenderJudgeBusScore();
		if(list!=null&&list.size()>0){
			tenderJudgeBusScore = (TenderJudgeBusScore)list.get(0);
		}
		return tenderJudgeBusScore;
	}
	
	/**
	 * 获得所有评委商务评分表表数据集
	 * @param tenderJudgeBusScore 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderJudgeBusScoreList(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderJudgeBusScore de where 1 = 1 " );
		if(tenderJudgeBusScore!=null){
			if(StringUtil.isNotBlank(tenderJudgeBusScore.getRcId())){
				hql.append(" and de.rcId = '").append(tenderJudgeBusScore.getRcId()).append("'");
			}
			if(!StringUtil.isBlank(tenderJudgeBusScore.getSupplierId())){
				hql.append(" and de.supplierId = ").append(tenderJudgeBusScore.getSupplierId());
			}
			if(!StringUtil.isBlank(tenderJudgeBusScore.getExpertId())){
				hql.append(" and de.expertId = ").append(tenderJudgeBusScore.getExpertId());
			}
		}
		hql.append(" order by de.tjbsId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有评委商务评分表表数据集
	 * @param rollPage 分页对象
	 * @param tenderJudgeBusScore 查询参数对象
	 * @author luguanglei 2017-05-10
	 * @return
	 * @throws BaseException 
	 */
	public List getTenderJudgeBusScoreList(RollPage rollPage, TenderJudgeBusScore tenderJudgeBusScore) throws BaseException {
		StringBuffer hql = new StringBuffer(" from TenderJudgeBusScore de where 1 = 1 " );

		hql.append(" order by de.tjbsId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获取商务评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	public List getTenderJudgeBusScoreListForGatherTab(Long[] supplierIdStr,Long rcId) throws BaseException{
		int columnCount=2+supplierIdStr.length;
		StringBuffer hql = new StringBuffer(" select tbi.item_name,tbi.points" );
		for(Long supplierId:supplierIdStr){
			hql.append(",tjbsv"+supplierId+".avg_points avg_points"+supplierId);
		}
		hql.append("  from tender_bid_item tbi ");
		for(Long supplierId:supplierIdStr){
			hql.append(" left join (select item_Id,avg_points from tender_judge_bus_score_view where supplier_id="+supplierId+" and rc_id="+rcId+" ) tjbsv"+supplierId+" on tbi.item_id=tjbsv"+supplierId+".item_id");
		}
		hql.append("  where tbi.rc_id="+rcId+" and tbi.item_type='1'");
		return this.getObjects(hql.toString(), columnCount);
	}
	/**
	  * 获取单个专家商务评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */	
	public List getTenderJudgeBusScoreListForGatherTabByExpertId(Long[] supplierIdStr,Long rcId,Long expertId) throws BaseException{
		int columnCount=2+supplierIdStr.length;
		StringBuffer hql = new StringBuffer(" select tbi.item_name,tbi.points" );
		for(Long supplierId:supplierIdStr){
			hql.append(",tjbsv"+supplierId+".judge_points judge_points"+supplierId);
		}
		hql.append("  from tender_bid_item tbi ");
		for(Long supplierId:supplierIdStr){
			hql.append(" left join (select item_Id,judge_points from tender_judge_bus_score where supplier_id="+supplierId+" and rc_id="+rcId+" and expert_id="+expertId+"  ) tjbsv"+supplierId+" on tbi.item_id=tjbsv"+supplierId+".item_id");
		}
		hql.append("  where tbi.rc_id="+rcId+" and tbi.item_type='1'");
		return this.getObjects(hql.toString(), columnCount);
	}
	/***
	 * 根据项目Id 查询每个供应商的技术评分综合
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	public Map<Long,Double> getTenderJudgeBusScoreMap(Long rcId) throws BaseException{
		String sql="select supplier_id,sum(avg_points) sumPoints from tender_judge_bus_score_view where rc_Id="+rcId+" group by supplier_id";
		Map<Long,Double> map=new HashMap<Long, Double>();
		List list=this.getObjectsList(TenderJudgeSum.class,sql);
		TenderJudgeSum tenderJudgeSum=null;
		for(int i=0;i<list.size();i++){
			tenderJudgeSum=(TenderJudgeSum)list.get(i);
			map.put(tenderJudgeSum.getSupplierId(), tenderJudgeSum.getSumPoints());
		}
		return map;
	}
	/***
	 * 根据项目Id 和专家Id 查询每个供应商的单个专家商务评分之和
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */
	public Map<Long,Double> getTenderJudgeBusScoreMapByExpertId(Long rcId,Long expertId) throws BaseException{
		String sql="select supplier_id,sum(judge_points) sumPoints from tender_judge_bus_score where rc_Id="+rcId+" and expert_id="+expertId+" group by supplier_id";
		Map<Long,Double> map=new HashMap<Long, Double>();
		List list=this.getObjectsList(TenderJudgeSum.class,sql);
		TenderJudgeSum tenderJudgeSum=null;
		for(int i=0;i<list.size();i++){
			tenderJudgeSum=(TenderJudgeSum)list.get(i);
			map.put(tenderJudgeSum.getSupplierId(), tenderJudgeSum.getSumPoints());
		}
		return map;
	}

	/**
	 * 获得不同字段不重复值的个数数据集
	 * @param judgeBusScore 查询参数对象
	 * @author zhuyu 2014-08-25
	 * @return
	 * @throws BaseException 
	 */
	public List getDiffrentBusJudgeList(  String cloumns ,TenderJudgeBusScore judgeBusScore,String hqlString) throws BaseException {
		StringBuffer hql = new StringBuffer(" select distinct "+cloumns+" from TenderJudgeBusScore de where 1 = 1 " );
		if(judgeBusScore!=null){
			if(StringUtil.isNotBlank(judgeBusScore.getRcId())){
				hql.append(" and de.rcId = '").append(judgeBusScore.getRcId()).append("'");
			}
			if(!StringUtil.isBlank(judgeBusScore.getExpertId())){
				hql.append(" and de.expertId = ").append(judgeBusScore.getExpertId());
			}
		}
		if(StringUtil.isNotBlank(hqlString)){
			hql.append(hqlString);
		}
		hql.append(" order by "+cloumns+" desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 根据项目id和专家id删除专家的评标信息
	 * @param rcId
	 * @param expertId
	 * @throws BaseException
	 */
	public void deleteTenderJudgeBusScoreByRcIdAndExpertId(Long rcId,
			Long expertId) throws BaseException {
		String sql="delete from tender_judge_bus_score where rc_id="+rcId+" and expert_id="+expertId;
		this.updateJdbcSql(sql);
		
	}
}
