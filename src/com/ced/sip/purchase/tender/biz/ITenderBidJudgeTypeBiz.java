package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeType;
/** 
 * 类名称：ITenderBidJudgeTypeBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-13
 */
public interface ITenderBidJudgeTypeBiz {

	/**
	 * 根据主键获得评标专家类别表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidJudgeType getTenderBidJudgeType(Long id) throws BaseException;

	/**
	 * 添加评标专家类别表信息
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException;

	/**
	 * 更新评标专家类别表表实例
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException;

	/**
	 * 删除评标专家类别表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudgeType(String id) throws BaseException;

	/**
	 * 删除评标专家类别表表实例
	 * @param tenderBidJudgeType 评标专家类别表表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException;

	/**
	 * 删除评标专家类别表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudgeTypes(String[] id) throws BaseException;

	/**
	 * 获得评标专家类别表表数据集
	 * tenderBidJudgeType 评标专家类别表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidJudgeType getTenderBidJudgeTypeByTenderBidJudgeType(TenderBidJudgeType tenderBidJudgeType) throws BaseException ;
	
	/**
	 * 获得所有评标专家类别表表数据集
	 * @param tenderBidJudgeType 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidJudgeTypeList(TenderBidJudgeType tenderBidJudgeType) throws BaseException ;
	
	/**
	 * 获得所有评标专家类别表表数据集
	 * @param tbjId 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidJudgeTypeListByTbjId(Long tbjId)
			throws BaseException;
	/**
	 * 根据评标专家表主键删除评标专家类别
	 * @param tbjId
	 * @throws BaseException
	 */
    abstract void deleTenderBidJudgeType(Long tbjId) throws BaseException;
}