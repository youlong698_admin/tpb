package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeHistory;
/** 
 * 类名称：ITenderBidJudgeHistoryBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidJudgeHistoryBiz {

	/**
	 * 根据主键获得评标专家历史表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidJudgeHistory getTenderBidJudgeHistory(Long id) throws BaseException;

	/**
	 * 添加评标专家历史表信息
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException;

	/**
	 * 更新评标专家历史表表实例
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException;

	/**
	 * 删除评标专家历史表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudgeHistory(String id) throws BaseException;

	/**
	 * 删除评标专家历史表表实例
	 * @param tenderBidJudgeHistory 评标专家历史表表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException;

	/**
	 * 删除评标专家历史表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudgeHistorys(String[] id) throws BaseException;

	/**
	 * 获得评标专家历史表表数据集
	 * tenderBidJudgeHistory 评标专家历史表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidJudgeHistory getTenderBidJudgeHistoryByTenderBidJudgeHistory(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException ;
	
	/**
	 * 获得所有评标专家历史表表数据集
	 * @param tenderBidJudgeHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidJudgeHistoryList(TenderBidJudgeHistory tenderBidJudgeHistory) throws BaseException ;
	
	/**
	 * 获得所有评标专家历史表表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidJudgeHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidJudgeHistoryList(RollPage rollPage, TenderBidJudgeHistory tenderBidJudgeHistory)
			throws BaseException;
	/**
	 * 取最大抽取次数
	 * @param bidCode 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int getMaxExpertSelectNumber(Long rcId) throws BaseException;

}