package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderReceivedBulletin;
/** 
 * 类名称：ITenderReceivedBulletinBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-11
 */
public interface ITenderReceivedBulletinBiz {

	/**
	 * 根据主键获得回标表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderReceivedBulletin getTenderReceivedBulletin(Long id) throws BaseException;

	/**
	 * 添加回标表信息
	 * @param tenderReceivedBulletin 回标表表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException;

	/**
	 * 更新回标表表实例
	 * @param tenderReceivedBulletin 回标表表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException;

	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderReceivedBulletin(String id) throws BaseException;

	/**
	 * 删除回标表表实例
	 * @param tenderReceivedBulletin 回标表表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException;

	/**
	 * 删除回标表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderReceivedBulletins(String[] id) throws BaseException;

	/**
	 * 获得回标表表数据集
	 * tenderReceivedBulletin 回标表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderReceivedBulletin getTenderReceivedBulletinByTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException ;
	
	/**
	 * 获得所有回标表表数据集
	 * @param tenderReceivedBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderReceivedBulletinList(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException ;

	/**
	 * 获得所有回标表表  总数
	 * @param tenderReceivedBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int countTenderReceivedBulletinList(TenderReceivedBulletin tenderReceivedBulletin) throws BaseException ;
	
	/**
	 * 获得所有回标表表数据集
	 * @param rollPage 分页对象
	 * @param tenderReceivedBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderReceivedBulletinList(RollPage rollPage, TenderReceivedBulletin tenderReceivedBulletin)
			throws BaseException;

}