package com.ced.sip.purchase.tender.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderJudgeBusScore;
/** 
 * 类名称：ITenderJudgeBusScoreBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderJudgeBusScoreBiz {

	/**
	 * 根据主键获得评委商务评分表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderJudgeBusScore getTenderJudgeBusScore(Long id) throws BaseException;

	/**
	 * 添加评委商务评分表信息
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException;

	/**
	 * 更新评委商务评分表表实例
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException;

	/**
	 * 删除评委商务评分表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderJudgeBusScore(String id) throws BaseException;

	/**
	 * 删除评委商务评分表表实例
	 * @param tenderJudgeBusScore 评委商务评分表表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException;

	/**
	 * 删除评委商务评分表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderJudgeBusScores(String[] id) throws BaseException;

	/**
	 * 获得评委商务评分表表数据集
	 * tenderJudgeBusScore 评委商务评分表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderJudgeBusScore getTenderJudgeBusScoreByTenderJudgeBusScore(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException ;
	
	/**
	 * 获得所有评委商务评分表表数据集
	 * @param tenderJudgeBusScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderJudgeBusScoreList(TenderJudgeBusScore tenderJudgeBusScore) throws BaseException ;
	
	/**
	 * 获得所有评委商务评分表表数据集
	 * @param rollPage 分页对象
	 * @param tenderJudgeBusScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderJudgeBusScoreList(RollPage rollPage, TenderJudgeBusScore tenderJudgeBusScore)
			throws BaseException;
	/**
	 * 获取商务评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	abstract List getTenderJudgeBusScoreListForGatherTab(Long[] supplierIdStr,Long rcId) throws BaseException;
	/**
	 * 获取单个专家商务评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */	
	abstract List getTenderJudgeBusScoreListForGatherTabByExpertId(Long[] supplierIdStr,Long rcId,Long expertId) throws BaseException;
	/***
	 * 根据项目Id 查询每个供应商的商务评分综合
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	abstract Map<Long,Double> getTenderJudgeBusScoreMap(Long rcId) throws BaseException;
	/***
	 * 根据项目Id 和专家Id 查询每个供应商的单个专家商务评分之和
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */
	abstract Map<Long,Double> getTenderJudgeBusScoreMapByExpertId(Long rcId,Long expertId) throws BaseException;

	/**
	 * 获得不同字段不重复值的个数数据集
	 * @param judgeBusScore 查询参数对象
	 * @author zhuyu 2014-08-25
	 * @return
	 * @throws BaseException 
	 */
	abstract List getDiffrentBusJudgeList(  String cloumns ,TenderJudgeBusScore judgeBusScore,String hqlString) throws BaseException;
	/**
	 * 根据项目id和专家id删除专家的评标信息
	 * @param rcId
	 * @param expertId
	 * @throws BaseException
	 */
	abstract void deleteTenderJudgeBusScoreByRcIdAndExpertId(Long rcId,Long expertId) throws BaseException;
}