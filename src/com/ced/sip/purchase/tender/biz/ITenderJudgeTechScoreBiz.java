package com.ced.sip.purchase.tender.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderJudgeTechScore;
/** 
 * 类名称：ITenderJudgeTechScoreBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderJudgeTechScoreBiz {

	/**
	 * 根据主键获得技术评分表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderJudgeTechScore getTenderJudgeTechScore(Long id) throws BaseException;

	/**
	 * 添加技术评分表信息
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException;

	/**
	 * 更新技术评分表表实例
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException;

	/**
	 * 删除技术评分表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderJudgeTechScore(String id) throws BaseException;

	/**
	 * 删除技术评分表表实例
	 * @param tenderJudgeTechScore 技术评分表表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException;

	/**
	 * 删除技术评分表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderJudgeTechScores(String[] id) throws BaseException;

	/**
	 * 获得技术评分表表数据集
	 * tenderJudgeTechScore 技术评分表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderJudgeTechScore getTenderJudgeTechScoreByTenderJudgeTechScore(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException ;
	
	/**
	 * 获得所有技术评分表表数据集
	 * @param tenderJudgeTechScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderJudgeTechScoreList(TenderJudgeTechScore tenderJudgeTechScore) throws BaseException ;
	
	/**
	 * 获得所有技术评分表表数据集
	 * @param rollPage 分页对象
	 * @param tenderJudgeTechScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderJudgeTechScoreList(RollPage rollPage, TenderJudgeTechScore tenderJudgeTechScore)
			throws BaseException;

	/**
	 * 获取技术评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	abstract List getTenderJudgeTechScoreListForGatherTab(Long[] supplierIdStr,Long rcId) throws BaseException;
	/**
	 * 获取单个专家技术评分汇总表数据集
	 * @param supplierIdStr
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */	
	abstract List getTenderJudgeTechScoreListForGatherTabByExpertId(Long[] supplierIdStr,Long rcId,Long expertId) throws BaseException;
	
	/***
	 * 根据项目Id 查询每个供应商的技术评分综合
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	abstract Map<Long,Double> getTenderJudgeTechScoreMap(Long rcId) throws BaseException;
	/***
	 * 根据项目Id 和专家Id 查询每个供应商的单个专家技术评分之和
	 * @param rcId
	 * @param expertId
	 * @return
	 * @throws BaseException
	 */
	abstract Map<Long,Double> getTenderJudgeTechScoreMapByExpertId(Long rcId,Long expertId) throws BaseException;
	/**
	 * 获得不同字段不重复值的个数数据集
	 * @param judgeTechScore 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getDiffrentTechJudgeList(  String cloumns ,TenderJudgeTechScore judgeTechScore,String hqlString) throws BaseException;
	/**
	 * 根据项目id和专家id删除专家的评标信息
	 * @param rcId
	 * @param expertId
	 * @throws BaseException
	 */
	abstract void deleteTenderJudgeTechScoreByRcIdAndExpertId(Long rcId,Long expertId) throws BaseException;

}