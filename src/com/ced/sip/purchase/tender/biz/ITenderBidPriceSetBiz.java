package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
/** 
 * 类名称：ITenderBidPriceSetBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidPriceSetBiz {

	/**
	 * 根据主键获得价格评分规则表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidPriceSet getTenderBidPriceSet(Long id) throws BaseException;

	/**
	 * 添加价格评分规则信息
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException;

	/**
	 * 更新价格评分规则表实例
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException;

	/**
	 * 删除价格评分规则表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidPriceSet(String id) throws BaseException;

	/**
	 * 删除价格评分规则表实例
	 * @param tenderBidPriceSet 价格评分规则表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException;

	/**
	 * 删除价格评分规则表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidPriceSets(String[] id) throws BaseException;

	/**
	 * 获得价格评分规则表数据集
	 * tenderBidPriceSet 价格评分规则表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidPriceSet getTenderBidPriceSetByTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) throws BaseException ;
	
	/**
	 * 获得所有价格评分规则表数据集
	 * @param tenderBidPriceSet 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidPriceSetList(TenderBidPriceSet tenderBidPriceSet) throws BaseException ;
	
	/**
	 * 获得所有价格评分规则表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidPriceSet 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidPriceSetList(RollPage rollPage, TenderBidPriceSet tenderBidPriceSet)
			throws BaseException;

}