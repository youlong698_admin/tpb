package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidList;
/** 
 * 类名称：ITenderBidListBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidListBiz {

	/**
	 * 根据主键获得招标信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidList getTenderBidList(Long id) throws BaseException;

	/**
	 * 添加招标信息信息
	 * @param tenderBidList 招标信息表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidList(TenderBidList tenderBidList) throws BaseException;

	/**
	 * 更新招标信息表实例
	 * @param tenderBidList 招标信息表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidList(TenderBidList tenderBidList) throws BaseException;

	/**
	 * 删除招标信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidList(String id) throws BaseException;

	/**
	 * 删除招标信息表实例
	 * @param tenderBidList 招标信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidList(TenderBidList tenderBidList) throws BaseException;

	/**
	 * 删除招标信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidLists(String[] id) throws BaseException;

	/**
	 * 获得招标信息表数据集
	 * tenderBidList 招标信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidList getTenderBidListByTenderBid(TenderBidList tenderBidList) throws BaseException ;
	
	/**
	 * 获得所有招标信息表数据集
	 * @param tenderBidList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidListList(TenderBidList tenderBidList) throws BaseException ;
	
	/**
	 * 获得所有招标信息表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidListList(RollPage rollPage, TenderBidList tenderBidList)
			throws BaseException;
	/**
	 * 根据rcId获得招标计划信息表实例
	 * @param rcId 项目ID
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidList getTenderBidListByRcId(Long rcId) throws BaseException;
	/**
	 * 根据项目id更新评标状态 专家评标时候更新状态
	 * @param rcId
	 * @throws BaseException
	 */
	abstract void updateCalibtationStatus(Long rcId) throws BaseException;
}