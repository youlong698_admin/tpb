package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderFileDownload;
/** 
 * 类名称：ITenderFileDownloadBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderFileDownloadBiz {

	/**
	 * 根据主键获得标书下载表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderFileDownload getTenderFileDownload(Long id) throws BaseException;

	/**
	 * 添加标书下载信息
	 * @param tenderFileDownload 标书下载表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException;

	/**
	 * 更新标书下载表实例
	 * @param tenderFileDownload 标书下载表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException;

	/**
	 * 删除标书下载表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderFileDownload(String id) throws BaseException;

	/**
	 * 删除标书下载表实例
	 * @param tenderFileDownload 标书下载表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException;

	/**
	 * 删除标书下载表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderFileDownloads(String[] id) throws BaseException;

	/**
	 * 获得标书下载表数据集
	 * tenderFileDownload 标书下载表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderFileDownload getTenderFileDownloadByTenderFileDownload(TenderFileDownload tenderFileDownload) throws BaseException ;
	
	/**
	 * 获得所有标书下载表数据集
	 * @param tenderFileDownload 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderFileDownloadList(TenderFileDownload tenderFileDownload) throws BaseException ;
	/**
	 * 获得所有标书下载表数据的总数
	 * @param tenderFileDownload 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int countTenderFileDownloadList(TenderFileDownload tenderFileDownload) throws BaseException ;
	/**
	 * 获得所有标书下载表数据集
	 * @param rollPage 分页对象
	 * @param tenderFileDownload 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderFileDownloadList(RollPage rollPage, TenderFileDownload tenderFileDownload)
			throws BaseException;

}