package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidJudge;
/** 
 * 类名称：ITenderBidJudgeBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidJudgeBiz {

	/**
	 * 根据主键获得评标专家表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidJudge getTenderBidJudge(Long id) throws BaseException;

	/**
	 * 添加评标专家信息
	 * @param tenderBidJudge 评标专家表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException;

	/**
	 * 更新评标专家表实例
	 * @param tenderBidJudge 评标专家表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException;

	/**
	 * 删除评标专家表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudge(String id) throws BaseException;

	/**
	 * 删除评标专家表实例
	 * @param tenderBidJudge 评标专家表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException;

	/**
	 * 删除评标专家表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidJudges(String[] id) throws BaseException;

	/**
	 * 获得评标专家表数据集
	 * tenderBidJudge 评标专家表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidJudge getTenderBidJudgeByTenderBidJudge(TenderBidJudge tenderBidJudge) throws BaseException ;
	
	/**
	 * 获得所有评标专家表数据集
	 * @param tenderBidJudge 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidJudgeList(TenderBidJudge tenderBidJudge) throws BaseException ;
	
	/**
	 * 获得所有评标专家表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidJudge 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidJudgeList(RollPage rollPage, TenderBidJudge tenderBidJudge)
			throws BaseException;
	/**
	 * 查询今日开标的项目信息
	 * @param experId 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getIsTenderBidJudgeList(Long experId)
			throws BaseException;
	/**
	 * 查询历史开标的项目信息
	 * @param experId bidCode buyRemark查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getHistoryTenderBidJudgeList(Long experId,String bidCode,String buyRemark)
			throws BaseException;

}