package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
/** 
 * 类名称：ITenderBidRateBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidRateBiz {

	/**
	 * 根据主键获得评标权重表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidRate getTenderBidRate(Long id) throws BaseException;

	/**
	 * 添加评标权重表信息
	 * @param tenderBidRate 评标权重表表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidRate(TenderBidRate tenderBidRate) throws BaseException;

	/**
	 * 更新评标权重表表实例
	 * @param tenderBidRate 评标权重表表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidRate(TenderBidRate tenderBidRate) throws BaseException;

	/**
	 * 删除评标权重表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidRate(String id) throws BaseException;

	/**
	 * 删除评标权重表表实例
	 * @param tenderBidRate 评标权重表表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidRate(TenderBidRate tenderBidRate) throws BaseException;

	/**
	 * 删除评标权重表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidRates(String[] id) throws BaseException;

	/**
	 * 获得评标权重表表数据集
	 * tenderBidRate 评标权重表表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidRate getTenderBidRateByTenderBidRate(TenderBidRate tenderBidRate) throws BaseException ;
	
	/**
	 * 获得所有评标权重表表数据集
	 * @param tenderBidRate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidRateList(TenderBidRate tenderBidRate) throws BaseException ;
	
	/**
	 * 获得所有评标权重表表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidRate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidRateList(RollPage rollPage, TenderBidRate tenderBidRate)
			throws BaseException;

}