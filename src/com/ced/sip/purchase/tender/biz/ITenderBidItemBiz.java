package com.ced.sip.purchase.tender.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.tender.entity.TenderBidItem;
/** 
 * 类名称：ITenderBidItemBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public interface ITenderBidItemBiz {

	/**
	 * 根据主键获得招标项目评分设置表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidItem getTenderBidItem(Long id) throws BaseException;

	/**
	 * 添加招标项目评分设置信息
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @throws BaseException 
	 */
	abstract void saveTenderBidItem(TenderBidItem tenderBidItem) throws BaseException;

	/**
	 * 更新招标项目评分设置表实例
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @throws BaseException 
	 */
	abstract void updateTenderBidItem(TenderBidItem tenderBidItem) throws BaseException;

	/**
	 * 删除招标项目评分设置表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidItem(String id) throws BaseException;

	/**
	 * 删除招标项目评分设置表实例
	 * @param tenderBidItem 招标项目评分设置表实例
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidItem(TenderBidItem tenderBidItem) throws BaseException;

	/**
	 * 删除招标项目评分设置表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteTenderBidItems(String[] id) throws BaseException;

	/**
	 * 获得招标项目评分设置表数据集
	 * tenderBidItem 招标项目评分设置表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract TenderBidItem getTenderBidItemByTenderBidItem(TenderBidItem tenderBidItem) throws BaseException ;
	
	/**
	 * 获得所有招标项目评分设置表数据集
	 * @param tenderBidItem 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidItemList(TenderBidItem tenderBidItem) throws BaseException ;
	
	/**
	 * 获得所有招标项目评分设置表数据集
	 * @param rollPage 分页对象
	 * @param tenderBidItem 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getTenderBidItemList(RollPage rollPage, TenderBidItem tenderBidItem)
			throws BaseException;

}