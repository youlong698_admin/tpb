package com.ced.sip.purchase.tender.action.supplier;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.beanutils.BeanUtils;

import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.encrypt.Base64;
import com.ced.sip.common.utils.encrypt.RSAEncrypt;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.biz.ITenderReceivedBulletinBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.entity.TenderReceivedBulletin;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseBiz;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceHistoryBiz;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
import com.ced.sip.purchase.base.entity.BidBusinessResponseHistory;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
import com.ced.sip.purchase.base.entity.BidPriceDetailHistory;
import com.ced.sip.purchase.base.entity.BidPriceHistory;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Users;
/** 
 * 类名称：BidPriceAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-15
 */
public class TenderBidPriceResponseAction extends BaseAction {
	//采购计划明细
	private IRequiredCollectBiz iRequiredCollectBiz;
	//商务响应项
	private IBusinessResponseItemBiz iBusinessResponseItemBiz;
	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
    //招标基本信息
	private ITenderBidListBiz iTenderBidListBiz;
	//回标信息
	private ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	// 供应商报价明细信息 
	private IBidPriceDetailBiz iBidPriceDetailBiz;
	// 供应商历史报价信息 
	private IBidPriceHistoryBiz iBidPriceHistoryBiz;
	// 供应商历史报价明细信息 
	private IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz;
	// 供应商商务响应 
	private IBidBusinessResponseBiz iBidBusinessResponseBiz;
	// 供应商商务响应 
	private IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz;

    private IAttachmentBiz iAttachmentBiz;
    
	// 供应商报价信息
	private BidPrice bidPrice;
	// 供应商报价信息明细
	private BidPriceDetail bidPriceDetail;
	// 供应商报价历史信息
	private BidPriceHistory bidPriceHistory;
	// 供应商报价历史明细信息
	private BidPriceDetailHistory bidPriceDetailHistory;
	// 供应商报价信息
	private List<BidPriceDetail> bpdList;
	// 商务响应项信息
	private BidBusinessResponse bidBusinessResponse;
	// 商务响应项历史信息
	private BidBusinessResponseHistory bidBusinessResponseHistory;
	// 商务响应项信息
	private List<BidBusinessResponse> bbrList;
	// 回标信息
	private TenderReceivedBulletin tenderReceivedBulletin;
	
	private TenderBidList tenderBidList;
	
	private RequiredCollectDetail requiredCollectDetail;
	
	private Long rcId;
	
	
	/**
	 * 查看供应商历史报价信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTenderBidPriceResponseMonitor() throws BaseException {
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			bidPriceHistory=new BidPriceHistory();
			bidPriceHistory.setRcId(rcId);
			bidPriceHistory.setSupplierId(supplierId);
			List list=this.iBidPriceHistoryBiz.getBidPriceHistoryList(bidPriceHistory);
			this.setListValue(list);
			
			tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			boolean isReturnTime=false;
			//判断当时时间是否大于回标时间
	        Calendar calendar = Calendar.getInstance(Locale.CHINA);
	        Date date = calendar.getTime();
	        if(tenderBidList.getReturnDate()!=null){
			if(date.getTime()<tenderBidList.getReturnDate().getTime()){
				isReturnTime=true;
			}}	
	        this.getRequest().setAttribute("isReturnTime", isReturnTime);
	        
	        InviteSupplier inviteSupplier=new InviteSupplier();
		    inviteSupplier.setRcId(rcId);
		    inviteSupplier.setSupplierId(supplierId);
		    inviteSupplier=this.iInviteSupplierBiz.getInviteSupplierByInviteSupplier(inviteSupplier);
            this.getRequest().setAttribute("isSms", inviteSupplier.getIsSms());  
		} catch (Exception e) {
			log.error("查看供应商报价信息信息列表错误！", e);
			throw new BaseException("查看供应商报价信息信息列表错误！", e);
		}
		
		return "viewTenderBidPriceResponse" ;
		
	}
	
	/**
	 * 保存供应商报价信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitTenderBidPriceRespone() throws BaseException {
		try{
		  Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
		  tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
          tenderBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		  tenderBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
		  
		  InviteSupplier inviteSupplier=new InviteSupplier();
		  inviteSupplier.setRcId(rcId);
		  inviteSupplier.setSupplierId(supplierId);
		  inviteSupplier=this.iInviteSupplierBiz.getInviteSupplierByInviteSupplier(inviteSupplier);
		  
		  List<RequiredCollectDetail> rcdlist=this.iRequiredCollectBiz.getRequiredCollectDetailList(rcId);
		  BusinessResponseItem businessResponseItem=new BusinessResponseItem();
		  businessResponseItem.setRcId(rcId);
		  List<BusinessResponseItem> briList=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
		  
		  this.getRequest().setAttribute("rcdlist", rcdlist);
		  this.getRequest().setAttribute("briList", briList);
		  this.getRequest().setAttribute("supplierId", supplierId);
		  this.getRequest().setAttribute("tenderBidList", tenderBidList);
		  this.getRequest().setAttribute("publicKey", inviteSupplier.getPublicKey());
		} catch (Exception e) {
			log("保存供应商报价信息信息初始化错误！", e);
			throw new BaseException("保存供应商报价信息信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	/**
	 * 加密报价
	 * @return
	 * @throws BaseException 
	 */
	public void encryPrice() throws BaseException {
		JSONObject jsonObj = new JSONObject();
		PrintWriter writer=null;
		try{
			writer = getResponse().getWriter();  
			String publicKey=this.getRequest().getParameter("publicKey");
			String prices=this.getRequest().getParameter("prices");
			//System.out.println(prices);
			byte[] cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPublicKeyByStr(publicKey),prices.getBytes());
			String cipher=Base64.encode(cipherData);
			jsonObj.put("data", cipher); 
		} catch (Exception e) {
			log("保存供应商报价信息信息错误！", e);
			jsonObj.put("data", ""); 
		}
        writer.print(jsonObj);  
        writer.flush();  
        writer.close();		
	}
	/**
	 * 保存供应商报价信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveTenderBidPriceRespone() throws BaseException {
		
		try{
			SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(getRequest()) ;
			Date date=DateUtil.getCurrentDateTime();	
			//更新邀请供应商表中的是否报价字段
			this.iInviteSupplierBiz.updateIsPriceAaByRcIdAndSupplierId(rcId, supplierInfo.getSupplierId());
			
			//判断回标表,如果没有数据,插入一条
			TenderReceivedBulletin tenderReceivedBulletinTmp=new TenderReceivedBulletin();
			tenderReceivedBulletinTmp.setSupplierId(supplierInfo.getSupplierId());
			tenderReceivedBulletinTmp.setRcId(bidPrice.getRcId());
			List<TenderReceivedBulletin> list=this.iTenderReceivedBulletinBiz.getTenderReceivedBulletinList(tenderReceivedBulletinTmp);
			if(list.size()==0){
				tenderReceivedBulletinTmp=new TenderReceivedBulletin();
				tenderReceivedBulletinTmp.setSupplierId(supplierInfo.getSupplierId());
				tenderReceivedBulletinTmp.setRcId(bidPrice.getRcId());
				tenderReceivedBulletinTmp.setIsReplay(TableStatus.COMMON_0);
				tenderReceivedBulletinTmp.setReceivedDate(date);
				tenderReceivedBulletinTmp.setSupplierName(supplierInfo.getSupplierName());
				tenderReceivedBulletinTmp.setReceivedName(supplierInfo.getContactPerson());
				tenderReceivedBulletinTmp.setReceivedTel(supplierInfo.getMobilePhone());
				tenderReceivedBulletinTmp.setWriter(supplierInfo.getSupplierLoginName());
				tenderReceivedBulletinTmp.setWriteDate(date);
				this.iTenderReceivedBulletinBiz.saveTenderReceivedBulletin(tenderReceivedBulletinTmp);
			}else{
				tenderReceivedBulletinTmp=list.get(0);
			}
			//删除旧的报价信息
			this.iBidPriceBiz.deleteBidPriceByRcIdAndSupplierId(bidPrice.getRcId(),supplierInfo.getSupplierId());
			
			//保存新的报价信息
			bidPrice.setWriter(supplierInfo.getSupplierLoginName());
			bidPrice.setWriteDate(date);
			this.iBidPriceBiz.saveBidPrice(bidPrice);
			
			this.iAttachmentBiz.deleteAttachments("TenderReceivedBulletin",tenderReceivedBulletinTmp.getTrbId(),
					AttachmentStatus.ATTACHMENT_CODE_205);
			
			// 保存文件
			tenderReceivedBulletinTmp.setAttIdData(tenderReceivedBulletin.getAttIdData());
			tenderReceivedBulletinTmp.setUuIdData(tenderReceivedBulletin.getUuIdData());
			tenderReceivedBulletinTmp.setFileNameData(tenderReceivedBulletin.getFileNameData());
			tenderReceivedBulletinTmp.setFileTypeData(tenderReceivedBulletin.getFileTypeData());
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					tenderReceivedBulletinTmp, tenderReceivedBulletinTmp.getTrbId(),
					AttachmentStatus.ATTACHMENT_CODE_205, UserRightInfoUtil
							.getSupplierName(this.getRequest())));
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetail.setBpId(bidPrice.getBpId());
					bidPriceDetail.setRcId(bidPrice.getRcId());
					this.iBidPriceDetailBiz.saveBidPriceDetail(bidPriceDetail);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponse.setBpId(bidPrice.getBpId());
					this.iBidBusinessResponseBiz.saveBidBusinessResponse(bidBusinessResponse);
				}
			}
			
			//保存新的报价历史信息
			bidPriceHistory=new BidPriceHistory();
			BeanUtils.copyProperties(bidPriceHistory, bidPrice);
			bidPriceHistory.setTotalPrice(null);
			this.iBidPriceHistoryBiz.saveBidPriceHistory(bidPriceHistory);
			
			// 保存文件
			bidPriceHistory.setAttIdData(tenderReceivedBulletin.getAttIdData());
			bidPriceHistory.setUuIdData(tenderReceivedBulletin.getUuIdData());
			bidPriceHistory.setFileNameData(tenderReceivedBulletin.getFileNameData());
			bidPriceHistory.setFileTypeData(tenderReceivedBulletin.getFileTypeData());
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					bidPriceHistory, bidPriceHistory.getBphId(),
					AttachmentStatus.ATTACHMENT_CODE_206, UserRightInfoUtil
							.getSupplierName(this.getRequest())));
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetailHistory=new BidPriceDetailHistory();
					BeanUtils.copyProperties(bidPriceDetailHistory, bidPriceDetail);
					bidPriceDetailHistory.setPrice(null);
					bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
					bidPriceDetailHistory.setRcId(bidPriceHistory.getRcId());
					this.iBidPriceDetailHistoryBiz.saveBidPriceDetailHistory(bidPriceDetailHistory);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponseHistory=new BidBusinessResponseHistory();
					BeanUtils.copyProperties(bidBusinessResponseHistory, bidBusinessResponse);
					bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
					this.iBidBusinessResponseHistoryBiz.saveBidBusinessResponseHistory(bidBusinessResponseHistory);
				}
			}
			this.getRequest().setAttribute("message", "报价成功");
			this.getRequest().setAttribute("operModule", "供应商报价信息");
		} catch (Exception e) {
			log("保存供应商报价信息信息错误！", e);
			throw new BaseException("保存供应商报价信息信息错误！", e);
		}
		
		return SUCCESS;
		
	}
	
	/**
	 * 查看供应商报价信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewTenderBidPriceResponeDetail() throws BaseException {
		
		try{

		      
			bidPriceHistory=this.iBidPriceHistoryBiz.getBidPriceHistory(bidPriceHistory.getBphId() );
			
			// 获取文件
			bidPriceHistory.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(iAttachmentBiz
							.getAttachmentList(new Attachment(bidPriceHistory
									.getBphId(),
									AttachmentStatus.ATTACHMENT_CODE_206)),
							"0", this.getRequest()));
			
			tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(bidPriceHistory.getRcId());
            tenderBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		    tenderBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
		    List<BidPriceDetailHistory> bpdhList=new ArrayList<BidPriceDetailHistory>();
			bidPriceDetailHistory=new BidPriceDetailHistory();
			bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
			List<Object[]> objectList=this.iBidPriceDetailHistoryBiz.getBidPriceDetailHistoryListSupplier(bidPriceDetailHistory);
			for(Object[] object:objectList){
				bidPriceDetailHistory=(BidPriceDetailHistory)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				bidPriceDetailHistory.setRequiredCollectDetail(requiredCollectDetail);
				bpdhList.add(bidPriceDetailHistory);
			}
				
				
			bidBusinessResponseHistory=new BidBusinessResponseHistory();
			bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
			List<BidBusinessResponseHistory> bbrhList=this.iBidBusinessResponseHistoryBiz.getBidBusinessResponseHistoryList(bidBusinessResponseHistory);
			
			
			
			this.getRequest().setAttribute("bidPriceHistory", bidPriceHistory);			
			this.getRequest().setAttribute("bpdhList", bpdhList);			
			this.getRequest().setAttribute("bbrhList", bbrhList);			
			this.getRequest().setAttribute("tenderBidList", tenderBidList);	
		} catch (Exception e) {
			log("查看供应商报价信息明细信息错误！", e);
			throw new BaseException("查看供应商报价信息明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 短信邮件告知项目负责人供应商已投标
	 * @return
	 * @throws BaseException
	 */
    public String saveSmsTenderBidPriceRespone() throws BaseException{
    	int result=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(getRequest()) ;
            
            RequiredCollect requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
            
            //更新邀请供应商表中的是否短信发送字段
			this.iInviteSupplierBiz.updateIsSmsByRcIdAndSupplierId(rcId, supplierInfo.getSupplierId());

			tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			Users user=BaseDataInfosUtil.convertLoginNameToUsers(tenderBidList.getWriter());
			String subject="邮件提醒："+supplierInfo.getSupplierName()+"对"+requiredCollect.getBuyRemark()+"项目进行报价投标。",content=""+requiredCollect.getBuyRemark()+"("+requiredCollect.getBidCode()+")供应商已回标，供应商名称："+supplierInfo.getSupplierName()+"";
			this.saveSendMailToUsersSupplier(user.getEmail(), subject, content, supplierInfo.getSupplierName());
			
			String messageContent=""+supplierInfo.getSupplierName()+"对"+requiredCollect.getBuyRemark()+"项目进行报价投标。";
			this.saveSmsMessageToUsersSupplier(tenderBidList.getResponsiblePhone(), messageContent, supplierInfo.getSupplierName());
			
			result=1;
			out.print(result);
		} catch (Exception e) {
			log("短信邮件告知项目负责人供应商已投标！", e);
			throw new BaseException("短信邮件告知项目负责人供应商已投标", e);
		}
		return null;    	
    }
	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}

	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}

	public BidPrice getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(BidPrice bidPrice) {
		this.bidPrice = bidPrice;
	}

	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}

	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}

	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}

	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}

	public IBidPriceHistoryBiz getiBidPriceHistoryBiz() {
		return iBidPriceHistoryBiz;
	}

	public void setiBidPriceHistoryBiz(IBidPriceHistoryBiz iBidPriceHistoryBiz) {
		this.iBidPriceHistoryBiz = iBidPriceHistoryBiz;
	}

	public IBidPriceDetailHistoryBiz getiBidPriceDetailHistoryBiz() {
		return iBidPriceDetailHistoryBiz;
	}

	public void setiBidPriceDetailHistoryBiz(
			IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz) {
		this.iBidPriceDetailHistoryBiz = iBidPriceDetailHistoryBiz;
	}

	public IBidBusinessResponseBiz getiBidBusinessResponseBiz() {
		return iBidBusinessResponseBiz;
	}

	public void setiBidBusinessResponseBiz(
			IBidBusinessResponseBiz iBidBusinessResponseBiz) {
		this.iBidBusinessResponseBiz = iBidBusinessResponseBiz;
	}

	public List<BidPriceDetail> getBpdList() {
		return bpdList;
	}

	public void setBpdList(List<BidPriceDetail> bpdList) {
		this.bpdList = bpdList;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IBusinessResponseItemBiz getiBusinessResponseItemBiz() {
		return iBusinessResponseItemBiz;
	}

	public void setiBusinessResponseItemBiz(
			IBusinessResponseItemBiz iBusinessResponseItemBiz) {
		this.iBusinessResponseItemBiz = iBusinessResponseItemBiz;
	}

	public List<BidBusinessResponse> getBbrList() {
		return bbrList;
	}

	public void setBbrList(List<BidBusinessResponse> bbrList) {
		this.bbrList = bbrList;
	}

	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}

	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}

	public ITenderReceivedBulletinBiz getiTenderReceivedBulletinBiz() {
		return iTenderReceivedBulletinBiz;
	}

	public void setiTenderReceivedBulletinBiz(
			ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz) {
		this.iTenderReceivedBulletinBiz = iTenderReceivedBulletinBiz;
	}

	public IBidBusinessResponseHistoryBiz getiBidBusinessResponseHistoryBiz() {
		return iBidBusinessResponseHistoryBiz;
	}

	public void setiBidBusinessResponseHistoryBiz(
			IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz) {
		this.iBidBusinessResponseHistoryBiz = iBidBusinessResponseHistoryBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public TenderReceivedBulletin getTenderReceivedBulletin() {
		return tenderReceivedBulletin;
	}

	public void setTenderReceivedBulletin(
			TenderReceivedBulletin tenderReceivedBulletin) {
		this.tenderReceivedBulletin = tenderReceivedBulletin;
	}

	public BidPriceHistory getBidPriceHistory() {
		return bidPriceHistory;
	}

	public void setBidPriceHistory(BidPriceHistory bidPriceHistory) {
		this.bidPriceHistory = bidPriceHistory;
	}
	
}
