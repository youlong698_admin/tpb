package com.ced.sip.purchase.tender.action.supplier;

import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.base.biz.IBidCommunicationInfoBiz;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
/** 
 * 类名称：TenderBidCommunicationInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-15
 */
public class TenderBidCommunicationInfoAction extends BaseAction {

	// 标中质询
	private IBidCommunicationInfoBiz iBidCommunicationInfoBiz;
	
	private Long rcId;
	// 标中质询
	private BidCommunicationInfo bidCommunicationInfo;

	/**
	 * 查看标中质询列表 标段监控节点
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTenderBidCommunicationInfoMonitor() throws BaseException {
		
		try{
			long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setRcId(rcId);
			bidCommunicationInfo.setAnswerId(supplierId);
			List<BidCommunicationInfo> bidCommunicationInfoList=this.iBidCommunicationInfoBiz.getBidCommunicationInfoList(bidCommunicationInfo);
			this.getRequest().setAttribute("bidCommunicationInfoList", bidCommunicationInfoList);
			
		} catch (Exception e) {
			log.error("查看标中质询列表标段监控错误！", e);
			throw new BaseException("查看标中质询列表标段监控错误！", e);
		}	
			
		return "bidCommunicationInfoDetial" ;
				
	}
	/**
	 * 修改标中质询信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidCommunicationInfoInit() throws BaseException {
		
		try{
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
		} catch (Exception e) {
			log("修改标中质询信息初始化错误！", e);
			throw new BaseException("修改标中质询信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改标中质询信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidCommunicationInfo() throws BaseException {
		
		try{
			bidCommunicationInfo.setAnswerDate(DateUtil.getCurrentDateTime());			
			this.iBidCommunicationInfoBiz.updateBidCommunicationInfo(bidCommunicationInfo);			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改标中质询");
		} catch (Exception e) {
			log("修改标中质询信息错误！", e);
			throw new BaseException("修改标中质询信息错误！", e);
		}
		return MODIFY_INIT;
		
	}
	/**
	 * 查看标中质询明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidCommunicationInfoDetail() throws BaseException {
		
		try{
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
		} catch (Exception e) {
			log("查看标中质询明细信息错误！", e);
			throw new BaseException("查看标中质询明细信息错误！", e);
		}
		return DETAIL;
		
	}
	public BidCommunicationInfo getBidCommunicationInfo() {
		return bidCommunicationInfo;
	}

	public void setBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) {
		this.bidCommunicationInfo = bidCommunicationInfo;
	}
	public IBidCommunicationInfoBiz getiBidCommunicationInfoBiz() {
		return iBidCommunicationInfoBiz;
	}
	public void setiBidCommunicationInfoBiz(
			IBidCommunicationInfoBiz iBidCommunicationInfoBiz) {
		this.iBidCommunicationInfoBiz = iBidCommunicationInfoBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

}
