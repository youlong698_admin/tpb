package com.ced.sip.purchase.tender.action.supplier;

import java.util.Date;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidFileBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.biz.ITenderFileDownloadBiz;
import com.ced.sip.purchase.tender.entity.TenderBidFile;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.entity.TenderFileDownload;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;

public class TenderFileDownloadAction extends BaseAction {
	//招标计划服务类
	private ITenderBidListBiz iTenderBidListBiz;
	//招标文件
	private ITenderBidFileBiz iTenderBidFileBiz;
	//招标文件下载
	private ITenderFileDownloadBiz iTenderFileDownloadBiz;
	//供应商服务类
	private ISupplierInfoBiz iSupplierInfoBiz;
	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
	 
	private IAttachmentBiz iAttachmentBiz;
	
	private Long rcId;
	
	
	private TenderBidFile tenderBidFile;
	
	private TenderFileDownload tenderFileDownload;
    
	private TenderBidList tenderBidList;
	
	private InviteSupplier inviteSupplier;
	/**
	 * 招标文件下载页面
	 * @return
	 * @throws BaseException 
	 */
	public String viewTenderFileDownloadDetialBidMonitor() throws BaseException {
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			
			
			inviteSupplier=new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			inviteSupplier.setSupplierId(supplierId);
			inviteSupplier=this.iInviteSupplierBiz.getInviteSupplierByInviteSupplier(inviteSupplier);
			

			boolean isApplicationDate=false;
			
			tenderBidList = this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			
			tenderBidFile = new TenderBidFile();
			tenderBidFile.setRcId(rcId);
			List<TenderBidFile> list = this.iTenderBidFileBiz.getTenderBidFileList(tenderBidFile);
			if (list.size() > 0) {
				tenderBidFile = list.get(0);
				//判断当时时间是否在招标文件允许下载时间范围内
				Date d=new Date();
				if(d.getTime()>=tenderBidList.getSalesDate().getTime()&&d.getTime()<=tenderBidList.getSaleeDate().getTime()){
					isApplicationDate=true;
				}
				if(isApplicationDate){
				// 获取文件
				tenderBidFile.setAttachmentUrl(iAttachmentBiz
						.getAttachmentPageUrl(iAttachmentBiz
								.getAttachmentList(new Attachment(tenderBidFile
										.getTbfId(),
										AttachmentStatus.ATTACHMENT_CODE_204)),
								"0", this.getRequest()));
				}
				
			 }
			
			tenderFileDownload=new TenderFileDownload();
			tenderFileDownload.setRcId(rcId);
			tenderFileDownload.setSupplierId(supplierId);
			List<TenderFileDownload> tenderFileDownloadList=this.iTenderFileDownloadBiz.getTenderFileDownloadList(tenderFileDownload); 


			this.getRequest().setAttribute("isTenderBidFile", inviteSupplier.getIsTenderBidFile());
			this.getRequest().setAttribute("isApplicationDate", isApplicationDate);
			this.getRequest().setAttribute("tenderBidList", tenderBidList);
			this.getRequest().setAttribute("tenderBidFile", tenderBidFile);
			this.getRequest().setAttribute("tenderFileDownloadList", tenderFileDownloadList);
		} catch (Exception e) {
			log("招标文件下载初始化页面错误！", e);
			throw new BaseException("招标文件下载初始化页面错误！", e);
		}
		return "tenderFileDownloadDetial";
	}
	/**
	 * 保存供应商下载招标文件
	 * @return
	 * @throws BaseException
	 */
    public String saveTenderFileDown() throws BaseException{
    	try {
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			SupplierInfo supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(supplierId);
    		tenderFileDownload=new TenderFileDownload();
			tenderFileDownload.setRcId(rcId);
			tenderFileDownload.setSupplierId(supplierId);
			tenderFileDownload.setFileDownName(supplierInfo.getContactPerson());
			tenderFileDownload.setFileDownTel(supplierInfo.getMobilePhone());
			tenderFileDownload.setFileDownDate(DateUtil.getCurrentDateTime());
			tenderFileDownload.setSupplierName(supplierInfo.getSupplierName());
			tenderFileDownload.setWriteDate(DateUtil.getCurrentDateTime());
			tenderFileDownload.setWriter(UserRightInfoUtil.getSupplierLoginName(this.getRequest()));
			this.iTenderFileDownloadBiz.saveTenderFileDownload(tenderFileDownload);
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "供应商下载招标文件成功");
		} catch (Exception e) {
			log("供应商下载招标文件错误！", e);
			throw new BaseException("供应商下载招标文件错误！", e);
		}
		return null;
    }
	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	public ITenderBidFileBiz getiTenderBidFileBiz() {
		return iTenderBidFileBiz;
	}
	public void setiTenderBidFileBiz(ITenderBidFileBiz iTenderBidFileBiz) {
		this.iTenderBidFileBiz = iTenderBidFileBiz;
	}
	public ITenderFileDownloadBiz getiTenderFileDownloadBiz() {
		return iTenderFileDownloadBiz;
	}
	public void setiTenderFileDownloadBiz(
			ITenderFileDownloadBiz iTenderFileDownloadBiz) {
		this.iTenderFileDownloadBiz = iTenderFileDownloadBiz;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}

 	
	
}
