package com.ced.sip.purchase.tender.action.supplier;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.common.utils.encrypt.RSAEncrypt;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Users;

public class TenderBidListAction extends BaseAction {
	 //招标计划服务类
	private ITenderBidListBiz iTenderBidListBiz;
	 //邀请供应商服务类
	private IInviteSupplierBiz iInviteSupplierBiz;
	 //项目信息服务类
	private IRequiredCollectBiz iRequiredCollectBiz;
	
	private Long rcId;
	
	private RequiredCollect requiredCollect;
	private TenderBidList tenderBidList;
	private InviteSupplier inviteSupplier;
    
	/**
	 * 招标项目报名初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitTenderBidListApplication() throws BaseException {
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			tenderBidList = this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			tenderBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
			tenderBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
			
			inviteSupplier = new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			inviteSupplier.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
			List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
            boolean isApplication=false;
			if(sulist.size()>0) isApplication=true;
			
			boolean isApplicationDate=false;
			//判断当时时间是否大于截标时间
			Date d=new Date();
			if(tenderBidList.getReturnDate()!=null){
			if(d.getTime()<=tenderBidList.getReturnDate().getTime()){
				isApplicationDate=true;
			}}	

			this.getRequest().setAttribute("isApplicationDate", isApplicationDate);
			this.getRequest().setAttribute("isApplication", isApplication);
		} catch (Exception e) {
			log("招标项目报名初始化页面错误！", e);
			throw new BaseException("招标项目报名初始化页面错误！", e);
		}
		return "saveInitTenderBidListApplication";
	}
	/**
	 * 招标计划项目供应商报名
	 * @return
	 * @throws BaseException
	 */
    public String saveTenderBidListApplication() throws BaseException{
    	String from="";
    	try {
    		from=this.getRequest().getParameter("from");
    		InviteSupplier inviteSupplier = new InviteSupplier();
    		inviteSupplier.setRcId(rcId);
    		SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(this.getRequest());
			inviteSupplier.setSupplierId(supplierInfo.getSupplierId());
    		List<InviteSupplier> list=this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
    		if(list.size()==0){
			inviteSupplier.setSupplierName(supplierInfo.getSupplierName());
			inviteSupplier.setWriteDate(DateUtil.getCurrentDateTime());
			inviteSupplier.setWriter(supplierInfo.getSupplierLoginName());
			inviteSupplier.setSupplierEmail(supplierInfo.getContactEmail());
			inviteSupplier.setSupplierPhone(supplierInfo.getMobilePhone());
			inviteSupplier.setPriceNum( new Long(0));
			inviteSupplier.setSourceCategory(2);
			Map<String,String> map=RSAEncrypt.genKeyPair();
			String publicKey=map.get("publicKey");
			String privateKey=map.get("privateKey");
			inviteSupplier.setPrivateKey(privateKey);
			inviteSupplier.setPublicKey(publicKey);
			this.iInviteSupplierBiz.saveInviteSupplier(inviteSupplier); 

			this.iInviteSupplierBiz.updateInviteSupplierSourceCategory(inviteSupplier.getIsId(),inviteSupplier.getSupplierId(),rcId);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			Users user=BaseDataInfosUtil.convertLoginNameToUsers(tenderBidList.getWriter());
			String subject="邮件提醒："+supplierInfo.getSupplierName()+"报名"+requiredCollect.getBuyRemark()+"项目。",content=""+requiredCollect.getBuyRemark()+"("+requiredCollect.getBidCode()+")供应商报名，供应商名称："+supplierInfo.getSupplierName()+"";
			this.saveSendMailToUsersSupplier(user.getEmail(), subject, content, supplierInfo.getSupplierName());
			
			String messageContent=""+supplierInfo.getSupplierName()+"报名"+requiredCollect.getBuyRemark()+"项目。";
			this.saveSmsMessageToUsersSupplier(tenderBidList.getResponsiblePhone(), messageContent, supplierInfo.getSupplierName());
    		}
			if(StringUtil.isNotBlank(from)){
				PrintWriter out = this.getResponse().getWriter();
				out.print("success");
			}
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "供应商报名成功");
		} catch (Exception e) {
			log("招标项目供应商报名错误！", e);
			throw new BaseException("招标项目供应商报名错误！", e);
		}
		if(StringUtil.isNotBlank(from)) return null;
		else return saveInitTenderBidListApplication();
    }
    /**
     * 进入我的招标项目页面
     * @return
     * @throws BaseException
     */
    public String viewTenderBidListMyMonitor() throws BaseException{
    	try {
    		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
		} catch (Exception e) {
			log("进入我的招标项目页面错误！", e);
			throw new BaseException("进入我的招标项目页面错误！", e);
		}
    	return "viewTenderBidListMyMonitor";
    }    
    /**
	 * 项目基本信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewTenderBidListDetailMonitor() throws BaseException {
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			tenderBidList = new TenderBidList();
			tenderBidList.setRcId(rcId);
			List<TenderBidList> list=this.iTenderBidListBiz.getTenderBidListList(tenderBidList);
			tenderBidList = list.get(0);
			tenderBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
			tenderBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(tenderBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
			
		} catch (Exception e) {
			log("项目基本信息初始化页面错误！", e);
			throw new BaseException("项目基本信息初始化页面错误！", e);
		}
		return "viewTenderBidListDetail";
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public TenderBidList getTenderBidList() {
		return tenderBidList;
	}

	public void setTenderBidList(TenderBidList tenderBidList) {
		this.tenderBidList = tenderBidList;
	}

 	
	
}
