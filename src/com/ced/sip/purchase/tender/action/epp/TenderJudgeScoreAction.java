package com.ced.sip.purchase.tender.action.epp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.RandomColor;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeTypeBiz;
import com.ced.sip.purchase.tender.biz.ITenderJudgeBusScoreBiz;
import com.ced.sip.purchase.tender.biz.ITenderJudgeTechScoreBiz;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeType;

public class TenderJudgeScoreAction extends BaseAction{

	// 评标专家评标方式
	private ITenderBidJudgeTypeBiz iTenderBidJudgeTypeBiz;
	//技术评分汇总
	private ITenderJudgeTechScoreBiz iTenderJudgeTechScoreBiz;
	//商务评分汇总
	private ITenderJudgeBusScoreBiz iTenderJudgeBusScoreBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	
	

    private Long rcId;
    private Long tbjtId;
	// 供应商报价信息
	private BidPrice bidPrice;
	//评标专家评标方式
	private TenderBidJudgeType tenderBidJudgeType;
	/**
	 * 查看技术评分汇总表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTenderJudTechGatherTab() throws BaseException {
		try{
			bidPrice=new BidPrice();
			bidPrice.setRcId(rcId);
			List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);
			
			Map<Long,Double> map=this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreMap(rcId);
			
			String[] supplierStr=new String[objectList.size()];
			Long[] supplierIdStr=new Long[objectList.size()];
			String[] colorStr=new String[objectList.size()];
			Double[] totalSumStr=new Double[objectList.size()];
			int i=0;
			for(Object[] object:objectList){
				bidPrice=(BidPrice)object[0];
				supplierIdStr[i]=bidPrice.getSupplierId();
				supplierStr[i]=(String)object[1];
				colorStr[i]=RandomColor.generateColor(i);
				totalSumStr[i]=map.get(bidPrice.getSupplierId());
				i++;
			}
			
			List tjtsList=this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreListForGatherTab(supplierIdStr, rcId);
						
            //评委人员查询
			TenderBidJudgeType tenderBidJudgeType = new TenderBidJudgeType();
			tenderBidJudgeType.setRcId(rcId);
			tenderBidJudgeType.setTableName(TableStatus.Judge_Table_Type_Tech);
			List<TenderBidJudgeType> judgeList=new ArrayList<TenderBidJudgeType>();
			List<Object[]> objList = this.iTenderBidJudgeTypeBiz.getTenderBidJudgeTypeList(tenderBidJudgeType);
			for(Object[] obj:objList){
				tenderBidJudgeType=(TenderBidJudgeType)obj[0];
				tenderBidJudgeType.setExpertName((String)obj[1]);
				judgeList.add(tenderBidJudgeType);
			}
			this.getRequest().setAttribute("judgeList", judgeList);
			this.getRequest().setAttribute("tjtsList", tjtsList);
			this.getRequest().setAttribute("colorStr", colorStr);
			this.getRequest().setAttribute("supplierStr", supplierStr);
			this.getRequest().setAttribute("totalSumStr", totalSumStr);
			
			
		} catch (Exception e) {
			log.error("查看技术评分汇总信息列表错误！", e);
			throw new BaseException("查看技术评分汇总信息列表错误！", e);
		}
		
		return "tenderJudTechGatherTab" ;
		
	}
	/**
	 * 查看评委技术评分表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewJudgeTechTab() throws BaseException {
		
		try{
			tenderBidJudgeType=new TenderBidJudgeType();
			tenderBidJudgeType.setTbjtId(tbjtId);
			List<Object[]> objlist=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeTypeList(tenderBidJudgeType);
			for(Object[] obj:objlist){
				tenderBidJudgeType=(TenderBidJudgeType)obj[0];
				tenderBidJudgeType.setExpertName((String)obj[1]);
			}
			
			bidPrice=new BidPrice();
			bidPrice.setRcId(rcId);
			List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);

			Map<Long,Double> map=this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreMapByExpertId(rcId, tenderBidJudgeType.getExpertId());
			
			String[] supplierStr=new String[objectList.size()];
			Long[] supplierIdStr=new Long[objectList.size()];
			String[] colorStr=new String[objectList.size()];
			Double[] totalSumStr=new Double[objectList.size()];
			int i=0;
			for(Object[] object:objectList){
				bidPrice=(BidPrice)object[0];
				supplierIdStr[i]=bidPrice.getSupplierId();
				supplierStr[i]=(String)object[1];
				colorStr[i]=RandomColor.generateColor(i);
				totalSumStr[i]=map.get(bidPrice.getSupplierId());
				i++;
			}
			
			List tjtsList=this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreListForGatherTabByExpertId(supplierIdStr, rcId,tenderBidJudgeType.getExpertId());
			
			
			this.getRequest().setAttribute("tenderBidJudgeType", tenderBidJudgeType);
			this.getRequest().setAttribute("tjtsList", tjtsList);
			this.getRequest().setAttribute("colorStr", colorStr);
			this.getRequest().setAttribute("supplierStr", supplierStr);
			this.getRequest().setAttribute("totalSumStr", totalSumStr);
			
		} catch (Exception e) {
			log.error("查看单个专家技术评标信息列表错误！", e);
			throw new BaseException("查看单个专家技术评标信息列表错误！", e);
		}
		
		return "view_judgeTechTab" ;
		
	}
	/**
	 * 查看商务评分汇总表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTenderJudBusGatherTab() throws BaseException {
		try{
			
			bidPrice=new BidPrice();
			bidPrice.setRcId(rcId);
			List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);

			Map<Long,Double> map=this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreMap(rcId);
			
			String[] supplierStr=new String[objectList.size()];
			Long[] supplierIdStr=new Long[objectList.size()];
			String[] colorStr=new String[objectList.size()];
			Double[] totalSumStr=new Double[objectList.size()];
			int i=0;
			for(Object[] object:objectList){
				bidPrice=(BidPrice)object[0];
				supplierIdStr[i]=bidPrice.getSupplierId();
				supplierStr[i]=(String)object[1];
				colorStr[i]=RandomColor.generateColor(i);
				totalSumStr[i]=map.get(bidPrice.getSupplierId());
				i++;
			}
			
			List tjtsList=this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreListForGatherTab(supplierIdStr, rcId);
						
            //评委人员查询
			TenderBidJudgeType tenderBidJudgeType = new TenderBidJudgeType();
			tenderBidJudgeType.setRcId(rcId);
			tenderBidJudgeType.setTableName(TableStatus.Judge_Table_Type_Business);
			List<TenderBidJudgeType> judgeList=new ArrayList<TenderBidJudgeType>();
			List<Object[]> objList = this.iTenderBidJudgeTypeBiz.getTenderBidJudgeTypeList(tenderBidJudgeType);
			for(Object[] obj:objList){
				tenderBidJudgeType=(TenderBidJudgeType)obj[0];
				tenderBidJudgeType.setExpertName((String)obj[1]);
				judgeList.add(tenderBidJudgeType);
			}
			this.getRequest().setAttribute("judgeList", judgeList);
			this.getRequest().setAttribute("tjtsList", tjtsList);
			this.getRequest().setAttribute("colorStr", colorStr);
			this.getRequest().setAttribute("supplierStr", supplierStr);
			this.getRequest().setAttribute("totalSumStr", totalSumStr);
			
		} catch (Exception e) {
			log.error("查看商务评分汇总表信息列表错误！", e);
			throw new BaseException("查看商务评分汇总表信息列表错误！", e);
		}
		
		return "tenderJudBusGatherTab" ;
		
	}
	/**
	 * 查看评委商务评分表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewJudgeBusTab() throws BaseException {
		
		try{
			tenderBidJudgeType=new TenderBidJudgeType();
			tenderBidJudgeType.setTbjtId(tbjtId);
			List<Object[]> objlist=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeTypeList(tenderBidJudgeType);
			for(Object[] obj:objlist){
				tenderBidJudgeType=(TenderBidJudgeType)obj[0];
				tenderBidJudgeType.setExpertName((String)obj[1]);
			}
			
			bidPrice=new BidPrice();
			bidPrice.setRcId(rcId);
			List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);

			Map<Long,Double> map=this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreMapByExpertId(rcId, tenderBidJudgeType.getExpertId());
			
			String[] supplierStr=new String[objectList.size()];
			Long[] supplierIdStr=new Long[objectList.size()];
			String[] colorStr=new String[objectList.size()];
			Double[] totalSumStr=new Double[objectList.size()];
			int i=0;
			for(Object[] object:objectList){
				bidPrice=(BidPrice)object[0];
				supplierIdStr[i]=bidPrice.getSupplierId();
				supplierStr[i]=(String)object[1];
				colorStr[i]=RandomColor.generateColor(i);
				totalSumStr[i]=map.get(bidPrice.getSupplierId());
				i++;
			}
			
			List tjtsList=this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreListForGatherTabByExpertId(supplierIdStr, rcId,tenderBidJudgeType.getExpertId());
			
			
			this.getRequest().setAttribute("tenderBidJudgeType", tenderBidJudgeType);
			this.getRequest().setAttribute("tjtsList", tjtsList);
			this.getRequest().setAttribute("colorStr", colorStr);
			this.getRequest().setAttribute("supplierStr", supplierStr);
			this.getRequest().setAttribute("totalSumStr", totalSumStr);
		} catch (Exception e) {
			log.error("查看评委商务评分信息列表错误！", e);
			throw new BaseException("查看评委商务评分信息列表错误！", e);
		}
		
		return "view_judgeBusTab" ;
		
	}
	public ITenderBidJudgeTypeBiz getiTenderBidJudgeTypeBiz() {
		return iTenderBidJudgeTypeBiz;
	}
	public void setiTenderBidJudgeTypeBiz(
			ITenderBidJudgeTypeBiz iTenderBidJudgeTypeBiz) {
		this.iTenderBidJudgeTypeBiz = iTenderBidJudgeTypeBiz;
	}
	public ITenderJudgeTechScoreBiz getiTenderJudgeTechScoreBiz() {
		return iTenderJudgeTechScoreBiz;
	}
	public void setiTenderJudgeTechScoreBiz(
			ITenderJudgeTechScoreBiz iTenderJudgeTechScoreBiz) {
		this.iTenderJudgeTechScoreBiz = iTenderJudgeTechScoreBiz;
	}
	public ITenderJudgeBusScoreBiz getiTenderJudgeBusScoreBiz() {
		return iTenderJudgeBusScoreBiz;
	}
	public void setiTenderJudgeBusScoreBiz(
			ITenderJudgeBusScoreBiz iTenderJudgeBusScoreBiz) {
		this.iTenderJudgeBusScoreBiz = iTenderJudgeBusScoreBiz;
	}
	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}
	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public Long getTbjtId() {
		return tbjtId;
	}
	public void setTbjtId(Long tbjtId) {
		this.tbjtId = tbjtId;
	}
	
}
