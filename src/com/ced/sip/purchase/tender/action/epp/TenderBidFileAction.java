package com.ced.sip.purchase.tender.action.epp;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.tender.biz.ITenderBidFileBiz;
import com.ced.sip.purchase.tender.entity.TenderBidFile;
import com.ced.sip.purchase.tender.util.TenderProgressStatus;
/** 
 * 类名称：TenderBidFileAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-10
 */
public class TenderBidFileAction extends BaseAction {

	// 招标文件 
	private ITenderBidFileBiz iTenderBidFileBiz;
	 //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
	 //标段流程记录实例表
	 private IBidProcessLogBiz iBidProcessLogBiz;
	 //项目日志记录服务类
	 private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	 
	 private IAttachmentBiz iAttachmentBiz;
	 
	 
	// 招标文件
	private TenderBidFile tenderBidFile; 
	
	private Long rcId;
    private String isDetail;
    
    private RequiredCollect requiredCollect; 
    private PurchaseRecordLog purchaseRecordLog;
	private BidProcessLog bidProcessLog;
	
	/**
	 * 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
	 * 
	 * @param rcId
	 *            项目id
	 * @param currProgress_Status
	 *            当前节点
	 * @param nextProgress_Status
	 *            下一个节点
	 * @param nextProgress_Status_Text
	 *            下一个节点的节点名称
	 * @author luguanglei
	 * @throws BaseException
	 */
	private void updateBidMonitorAndBidProcessLog(Long rcId,
			Long currProgress_Status, Long nextProgress_Status,
			String nextProgress_Status_Text) throws BaseException {
		// 修改项目监控至下一个节点
		requiredCollect = new RequiredCollect();
		requiredCollect.setRcId(rcId);
		requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(rcId);
		if (requiredCollect.getServiceStatus() < nextProgress_Status) {
			requiredCollect.setServiceStatus(nextProgress_Status);
			requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
			this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
		}
		// 修改项目日志表的当前节点的完成时间
		bidProcessLog = new BidProcessLog();
		bidProcessLog.setRcId(rcId);
		bidProcessLog.setBidNode(currProgress_Status);
		bidProcessLog = this.iBidProcessLogBiz
				.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
		bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
		this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
		// 新增项目日志表的下一个节点的接收时间
		bidProcessLog = new BidProcessLog();
		bidProcessLog.setRcId(rcId);
		bidProcessLog.setBidNode(nextProgress_Status);
		bidProcessLog = this.iBidProcessLogBiz
				.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
		if (bidProcessLog.getBplId() == null) {
			bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
			this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
		}

	}

	/**
	 * 判断是否是编制人员
	 * 
	 * @param bidMonitor
	 * @return
	 */
	private boolean isWriter(String writer) {
		boolean isWriter = false;
		String username = UserRightInfoUtil.getUserName(this.getRequest());
		// 当前登录人是不是编制人员
		if (username.equals(writer))
			isWriter = true;
		return isWriter;
	}

	/**
	 * 招标项目招标文件项目监控
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewTenderBidFileMonitor() throws BaseException {
		String view = "tenderBidFileMonitorDetail";
		try {

			requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(rcId);

			if (!isWriter(requiredCollect.getWriter()))
				isDetail = "detail";

			tenderBidFile = new TenderBidFile();
			tenderBidFile.setRcId(rcId);
			List<TenderBidFile> list = this.iTenderBidFileBiz.getTenderBidFileList(tenderBidFile);
			if (list.size() > 0) {
				tenderBidFile = list.get(0);
			} else {
				tenderBidFile.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
				tenderBidFile.setWriteDate(DateUtil.getCurrentDateTime());
				tenderBidFile.setRcId(rcId);
			}

			if (TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())
					&& TableStatus.NOTICE_TYPE_1.equals(requiredCollect
							.getIsSendNotice()) && StringUtil.isBlank(isDetail)&&(requiredCollect.getStatus().equals(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01)||requiredCollect.getStatus().equals(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3))) {
				tenderBidFile.setWriterCn(BaseDataInfosUtil
						.convertLoginNameToChnName(tenderBidFile.getWriter()));
				// 获取文件
				Map<String, Object> map = iAttachmentBiz
						.getAttachmentMap(new Attachment(tenderBidFile
								.getTbfId(),
								AttachmentStatus.ATTACHMENT_CODE_204));
				tenderBidFile.setUuIdData((String) map.get("uuIdData"));
				tenderBidFile.setFileNameData((String) map.get("fileNameData"));
				tenderBidFile.setFileTypeData((String) map.get("fileTypeData"));
				tenderBidFile.setAttIdData((String) map.get("attIdData"));
				view = "tenderBidFileMonitorUpdate";
			} else {
				tenderBidFile.setWriterCn(BaseDataInfosUtil
						.convertLoginNameToChnName(tenderBidFile.getWriter()));
				// 获取文件
				tenderBidFile.setAttachmentUrl(iAttachmentBiz
						.getAttachmentPageUrl(iAttachmentBiz
								.getAttachmentList(new Attachment(tenderBidFile
										.getTbfId(),
										AttachmentStatus.ATTACHMENT_CODE_204)),
								"0", this.getRequest()));
			}

		} catch (Exception e) {
			log("通过项目进入招标文件错误！", e);
			throw new BaseException("通过项目进入招标文件错误！", e);
		}
		return view;
	}

	/**
	 * 保存招标文件
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateTenderBidFile() throws BaseException {
		String view = "success";
		try {
			if (tenderBidFile.getTbfId() == null) {
				tenderBidFile.setWriteDate(new Date());
				tenderBidFile.setWriter(UserRightInfoUtil
						.getUserName(getRequest()));
				this.iTenderBidFileBiz.saveTenderBidFile(tenderBidFile);
			} else {
				this.iTenderBidFileBiz.updateTenderBidFile(tenderBidFile);
			}
			// 保存文件
			tenderBidFile.setAttIdData(tenderBidFile.getAttIdData());
			tenderBidFile.setUuIdData(tenderBidFile.getUuIdData());
			tenderBidFile.setFileNameData(tenderBidFile.getFileNameData());
			tenderBidFile.setFileTypeData(tenderBidFile.getFileTypeData());
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					tenderBidFile, tenderBidFile.getTbfId(),
					AttachmentStatus.ATTACHMENT_CODE_204, UserRightInfoUtil
							.getUserName(this.getRequest())));

			// 删除文件
			iAttachmentBiz.deleteAttachments(parseAttachIds(tenderBidFile
					.getAttIds()));

			String operateContent = "招标文件已上传，上传日期为【"
					+ DateUtil.getStringFromDate(tenderBidFile.getWriteDate()) + "】";
			updateBidMonitorAndBidProcessLog(tenderBidFile.getRcId(),
					TenderProgressStatus.Progress_Status_21,
					TenderProgressStatus.Progress_Status_22,
					TenderProgressStatus.Progress_Status_22_Text);

			// 保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this
					.getRequest())
					+ "");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil
					.getUserName(this.getRequest()));
			purchaseRecordLog.setRcId(tenderBidFile.getRcId());
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog
					.setBidNode(TenderProgressStatus.Progress_Status_21_Text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);

			this.getRequest().setAttribute("message", "操作成功");
			this.getRequest().setAttribute("operModule", "上传招标文件成功");
		} catch (Exception e) {
			log("上传招标文件错误！", e);
			throw new BaseException("上传招标文件错误！", e);
		}
		return view;

	}
	/**
	 * 招标项目招标文件查看
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewTenderBidFileDetail() throws BaseException {
		tenderBidFile = new TenderBidFile();
		tenderBidFile.setRcId(rcId);
		List<TenderBidFile> list = this.iTenderBidFileBiz.getTenderBidFileList(tenderBidFile);
		if (list.size() > 0) {
			tenderBidFile = list.get(0);
			tenderBidFile.setWriterCn(BaseDataInfosUtil
					.convertLoginNameToChnName(tenderBidFile.getWriter()));
			// 获取文件
			tenderBidFile.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(iAttachmentBiz
							.getAttachmentList(new Attachment(tenderBidFile
									.getTbfId(),
									AttachmentStatus.ATTACHMENT_CODE_204)),
							"0", this.getRequest()));
		}
		return "tenderBidFileMonitorDetail";
	}
	public ITenderBidFileBiz getiTenderBidFileBiz() {
		return iTenderBidFileBiz;
	}

	public void setiTenderBidFileBiz(ITenderBidFileBiz iTenderBidFileBiz) {
		this.iTenderBidFileBiz = iTenderBidFileBiz;
	}

	public TenderBidFile getTenderBidFile() {
		return tenderBidFile;
	}

	public void setTenderBidFile(TenderBidFile tenderBidFile) {
		this.tenderBidFile = tenderBidFile;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	
	
}
