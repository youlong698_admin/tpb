package com.ced.sip.purchase.tender.action.epp;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.tender.biz.ITenderReceivedBulletinBiz;
import com.ced.sip.purchase.tender.entity.TenderReceivedBulletin;
/** 
 * 类名称：TenderReceivedBulletinAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-11
 */
public class TenderReceivedBulletinAction extends BaseAction {

	// 回标表 
	private ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz;
	
	 //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;

	private IAttachmentBiz iAttachmentBiz;
	
	 
	 private Long rcId;
     private String isDetail;
     
     private RequiredCollect requiredCollect;
 	// 回标表
 	private TenderReceivedBulletin tenderReceivedBulletin;
	
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 回标响回标段监控
	 * @return
	 * @throws BaseException 
	 */
	public String viewTenderReceivedBulletinMonitor() throws BaseException {
		
		String view="tenderReceivedBulletinMonitorDetail";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			tenderReceivedBulletin=new TenderReceivedBulletin();
			tenderReceivedBulletin.setRcId(rcId);
			List<TenderReceivedBulletin> tenderReceivedBulletinList=this.iTenderReceivedBulletinBiz.getTenderReceivedBulletinList(tenderReceivedBulletin);
			this.getRequest().setAttribute("tenderReceivedBulletinList", tenderReceivedBulletinList);
			
			//当前流程进度为回标响应 且项目状态为正常 且非查看页面
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.OPEN_STATUS_01.equals(requiredCollect.getOpenStatus())&&StringUtil.isBlank(isDetail))
			{   
            	view="tenderReceivedBulletinMonitorUpdate";
			}
            
		} catch (Exception e) {
			log("回标响应监控初始化错误！", e);
			throw new BaseException("回标响应监控初始化错误！", e);
		}
		return view;
		
	}
	/**
	 * 开标过程中供方投标文件查看
	 * @return
	 * @throws BaseException 
	 */
	public String viewTenderReceiveBulletinTab() throws BaseException {
		
		try{
			tenderReceivedBulletin=new TenderReceivedBulletin();
			tenderReceivedBulletin.setRcId(rcId);
			List<TenderReceivedBulletin> tenderReceivedBulletinList=this.iTenderReceivedBulletinBiz.getTenderReceivedBulletinList(tenderReceivedBulletin);
			for(TenderReceivedBulletin tenderReceivedBulletin:tenderReceivedBulletinList){
				// 获取文件
				tenderReceivedBulletin.setAttachmentUrl(iAttachmentBiz
						.getAttachmentPageUrl(iAttachmentBiz
								.getAttachmentList(new Attachment(tenderReceivedBulletin.getTrbId(),AttachmentStatus.ATTACHMENT_CODE_205)),
								"0", this.getRequest()));
			}
			this.getRequest().setAttribute("tenderReceivedBulletinList", tenderReceivedBulletinList);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.getRequest().setAttribute("requiredCollect", requiredCollect);
            
		} catch (Exception e) {
			log("开标过程中供方投标文件查看错误！", e);
			throw new BaseException("开标过程中供方投标文件查看错误！", e);
		}
		return "tenderReceiveBulletinTab";
		
	}
	/**
	 * 获取回标响应数
	 * @return
	 * @throws BaseException
	 */
	public String getTenderReceiveBulletinForNode() throws BaseException{
		int count=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            tenderReceivedBulletin=new TenderReceivedBulletin();
			tenderReceivedBulletin.setRcId(rcId);
			count=this.iTenderReceivedBulletinBiz.countTenderReceivedBulletinList(tenderReceivedBulletin);
			
			out.print(count);
		} catch (Exception e) {
			log("获取回标响应厂家数！", e);
			throw new BaseException("获取回标响应厂家数！", e);
		}
		return null;
	}
	public ITenderReceivedBulletinBiz getiTenderReceivedBulletinBiz() {
		return iTenderReceivedBulletinBiz;
	}

	public void setiTenderReceivedBulletinBiz(ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz) {
		this.iTenderReceivedBulletinBiz = iTenderReceivedBulletinBiz;
	}

	public TenderReceivedBulletin getTenderReceivedBulletin() {
		return tenderReceivedBulletin;
	}

	public void setTenderReceivedBulletin(TenderReceivedBulletin tenderReceivedBulletin) {
		this.tenderReceivedBulletin = tenderReceivedBulletin;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	
}
