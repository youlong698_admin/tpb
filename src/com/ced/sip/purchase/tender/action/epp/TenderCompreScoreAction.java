package com.ced.sip.purchase.tender.action.epp;

import java.util.ArrayList;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidPriceSetBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidRateBiz;
import com.ced.sip.purchase.tender.biz.ITenderJudgeBusScoreBiz;
import com.ced.sip.purchase.tender.biz.ITenderJudgeTechScoreBiz;
import com.ced.sip.purchase.tender.biz.ITenderPriceScoreBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
import com.ced.sip.purchase.tender.entity.TenderJudgeBusScore;
import com.ced.sip.purchase.tender.entity.TenderJudgeTechScore;
import com.ced.sip.purchase.tender.entity.TenderSupplierScore;

public class TenderCompreScoreAction extends BaseAction {
	//评标权重
	private ITenderBidRateBiz iTenderBidRateBiz;
	//价格评标权重
	private ITenderBidPriceSetBiz iTenderBidPriceSetBiz;
	//价格评分服务类
	private ITenderPriceScoreBiz iTenderPriceScoreBiz;
	//技术评分汇总
	private ITenderJudgeTechScoreBiz iTenderJudgeTechScoreBiz;
	//商务评分汇总
	private ITenderJudgeBusScoreBiz iTenderJudgeBusScoreBiz;
	//招标计划
	private ITenderBidListBiz iTenderBidListBiz;
	
	
	private Long rcId;
	private TenderJudgeTechScore judgeTechScore;
	private TenderJudgeBusScore judgeBusScore;
	private List<TenderJudgeBusScore> jbsList;
	private List<TenderJudgeTechScore> jthList;
	/**
	 * 查看综合评标汇总明细信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String vewTenderCompreScore() throws BaseException {
		String message="";
		try{			
           TenderBidList tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			
			if(TableStatus.PRICE_STATUS_04.equals(tenderBidList.getPriceStatus())){
				
			List compPointList = new ArrayList();
			//获得评标权重
			TenderBidRate tenderBidRate = new TenderBidRate();
			tenderBidRate.setRcId(rcId);
			tenderBidRate=this.iTenderBidRateBiz.getTenderBidRateByTenderBidRate(tenderBidRate);
			
			//取得标段价格评分规则信息
			TenderBidPriceSet tenderBidPriceSet = new TenderBidPriceSet() ;
			tenderBidPriceSet.setRcId(rcId) ;
			tenderBidPriceSet = this.iTenderBidPriceSetBiz.getTenderBidPriceSetByTenderBidPriceSet(tenderBidPriceSet);
			
			//获得邀请供应商
			List sumpList = iTenderPriceScoreBiz.getSumBidPriceAa( rcId , null ) ;
			List tssList = this.iTenderPriceScoreBiz.getSupplierPriceScore(rcId,sumpList, tenderBidPriceSet, tenderBidRate ) ;
			//isList = this.orderInviteSupplierBySumPrice(isList);
			TenderSupplierScore tenderSupplierScore=null;
			for(int i=0;i<tssList.size();i++){
				tenderSupplierScore = (TenderSupplierScore)tssList.get(i);
				//综合评分加权值
				Double compreSumPoints = 0.0;
				//技术评分加权值
				Double tSumPoints = 0.0;
				//商务评分加权值
				Double bSumPoints = 0.0;
				//技术评分List
				judgeTechScore = new TenderJudgeTechScore();
				judgeTechScore.setRcId(rcId);
				judgeTechScore.setSupplierId(tenderSupplierScore.getSupplierId());
				jthList = this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreList(judgeTechScore);
				//获得技术评委数量
				Integer jdNum = this.iTenderJudgeTechScoreBiz.getDiffrentTechJudgeList("expertId", judgeTechScore, "").size();
				Double techSumPoints = 0.0;
				for(int j=0;j<jthList.size();j++){
					TenderJudgeTechScore jts = jthList.get(j);
					techSumPoints += jts.getJudgePoints();
				}
				if(jdNum!=0){
					tSumPoints = (techSumPoints/jdNum)*(tenderBidRate.getTechRate()/100);
					tenderSupplierScore.setTechSumPoints(StringUtil.formateNumber((tSumPoints)));
				}else{
					tSumPoints = techSumPoints*(tenderBidRate.getTechRate()/100);
					tenderSupplierScore.setTechSumPoints(StringUtil.formateNumber((tSumPoints)));
				}
				//商务评分List
				judgeBusScore = new TenderJudgeBusScore();
				judgeBusScore.setRcId(rcId);
				judgeBusScore.setSupplierId(tenderSupplierScore.getSupplierId());
				jbsList = this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreList(judgeBusScore);
				//获得商务评委数量
				Integer jbNum = this.iTenderJudgeBusScoreBiz.getDiffrentBusJudgeList("expertId", judgeBusScore, "").size();
				Double busSumPoints = 0.0;
				for(int j=0;j<jbsList.size();j++){
					TenderJudgeBusScore jbs = jbsList.get(j);
					busSumPoints += jbs.getJudgePoints();
				}
				if(jbNum!=0){
					bSumPoints = (busSumPoints/jbNum)*(tenderBidRate.getBusinessRate()/100);
					tenderSupplierScore.setBusSumPoints(StringUtil.formateNumberTo((bSumPoints)));
				}else{
					bSumPoints = busSumPoints*(tenderBidRate.getBusinessRate()/100);
					tenderSupplierScore.setBusSumPoints(StringUtil.formateNumberTo((bSumPoints)));
				}
				compreSumPoints = Double.parseDouble(tenderSupplierScore.getPriceScore())*(tenderBidRate.getPriceRate()/100);
				tenderSupplierScore.setCompreSumPoints(StringUtil.formateNumberTo(tSumPoints+bSumPoints+compreSumPoints));
				tenderSupplierScore.setPriceScore(StringUtil.formateNumberTo(Double.parseDouble(tenderSupplierScore.getPriceScore())*(tenderBidRate.getPriceRate()/100)));
				if(StringUtil.isNotBlank((tenderSupplierScore.getSumPrice()))){
					compPointList.add(tenderSupplierScore);
				}
			}
			compPointList = this.orderInviteSupplierByCompreSumPoints(compPointList);
			this.getRequest().setAttribute("compPointList", compPointList);
			this.getRequest().setAttribute("tenderBidRate", tenderBidRate);
			}else{
				message="温馨提示：报价最终提交之后,才能查看综合评分汇总！";
			}
			this.getRequest().setAttribute("message", message );
			
		} catch (Exception e) {
			log.error("根据标段监控查看综合评标汇总明细信息！", e);
			throw new BaseException("根据标段监控查看综合评标汇总明细信息！", e);
		}
		return "tenderCompreScore" ;
		
	}
	/**
	 * 供应商按综合评分信息排序
	 * @return
	 * @throws BaseException 
	 */
	public List orderInviteSupplierByCompreSumPoints(List invSupList) {
		TenderSupplierScore inSup=null;
		TenderSupplierScore invSup=null;
		List orderSupList = new ArrayList();
		String[] sumPriceArray = new String[invSupList.size()];
		for(int i=0;i<invSupList.size();i++){
			inSup = (TenderSupplierScore) invSupList.get(i);
			sumPriceArray[i] = inSup.getCompreSumPoints();
		}
		String temp = null;
		if(sumPriceArray!=null){
			for(int i=0;i<sumPriceArray.length;i++){
				for(int j=0;j<sumPriceArray.length;j++){
					if(sumPriceArray[i] != null && sumPriceArray[j] != null){
						if(Double.parseDouble(sumPriceArray[i])>Double.parseDouble(sumPriceArray[j])){
							temp = sumPriceArray[i];
							sumPriceArray[i] = sumPriceArray[j];
							sumPriceArray[j] = temp;
						}
					}
				}
			}
			for(int i=0;i<sumPriceArray.length;i++){
				for(int j=0;j<sumPriceArray.length;j++){
					invSup = (TenderSupplierScore) invSupList.get(j);
					if(sumPriceArray[i].equals(invSup.getCompreSumPoints())){
						orderSupList.add(invSup);
						break;
					}
				}
			}
		}
		return orderSupList;
	}
	public ITenderBidRateBiz getiTenderBidRateBiz() {
		return iTenderBidRateBiz;
	}
	public void setiTenderBidRateBiz(ITenderBidRateBiz iTenderBidRateBiz) {
		this.iTenderBidRateBiz = iTenderBidRateBiz;
	}
	public ITenderBidPriceSetBiz getiTenderBidPriceSetBiz() {
		return iTenderBidPriceSetBiz;
	}
	public void setiTenderBidPriceSetBiz(ITenderBidPriceSetBiz iTenderBidPriceSetBiz) {
		this.iTenderBidPriceSetBiz = iTenderBidPriceSetBiz;
	}
	public ITenderPriceScoreBiz getiTenderPriceScoreBiz() {
		return iTenderPriceScoreBiz;
	}
	public void setiTenderPriceScoreBiz(ITenderPriceScoreBiz iTenderPriceScoreBiz) {
		this.iTenderPriceScoreBiz = iTenderPriceScoreBiz;
	}
	public ITenderJudgeTechScoreBiz getiTenderJudgeTechScoreBiz() {
		return iTenderJudgeTechScoreBiz;
	}
	public void setiTenderJudgeTechScoreBiz(
			ITenderJudgeTechScoreBiz iTenderJudgeTechScoreBiz) {
		this.iTenderJudgeTechScoreBiz = iTenderJudgeTechScoreBiz;
	}
	public ITenderJudgeBusScoreBiz getiTenderJudgeBusScoreBiz() {
		return iTenderJudgeBusScoreBiz;
	}
	public void setiTenderJudgeBusScoreBiz(
			ITenderJudgeBusScoreBiz iTenderJudgeBusScoreBiz) {
		this.iTenderJudgeBusScoreBiz = iTenderJudgeBusScoreBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	
}
