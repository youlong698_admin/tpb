package com.ced.sip.purchase.tender.action.epp;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.tender.biz.ITenderBidItemBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidPriceSetBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidRateBiz;
import com.ced.sip.purchase.tender.entity.TenderBidItem;
import com.ced.sip.purchase.tender.entity.TenderBidJudge;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
import com.ced.sip.purchase.tender.util.TenderProgressStatus;
import com.ced.sip.system.biz.IBidItemBiz;
import com.ced.sip.system.entity.BidItem;

/**
 * 类名称：TenderBidRateAction 创建人：luguanglei 创建时间：2017-05-10
 */
public class TenderBidPreAction extends BaseAction {

	// 评标权重表
	private ITenderBidRateBiz iTenderBidRateBiz;
	// 评分设置表
	private ITenderBidItemBiz iTenderBidItemBiz;
	//评分标准表
	private IBidItemBiz iBidItemBiz;
	// 价格评分设置表
	private ITenderBidPriceSetBiz iTenderBidPriceSetBiz;
	// 评标专家 
	private ITenderBidJudgeBiz iTenderBidJudgeBiz;	
	// 项目信息服务类
	private IRequiredCollectBiz iRequiredCollectBiz;
	// 标段流程记录实例表
	private IBidProcessLogBiz iBidProcessLogBiz;
	// 项目日志记录服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	//招标计划
	private ITenderBidListBiz iTenderBidListBiz;

	// 评标权重表
	private TenderBidRate tenderBidRate;
	//评分设置
	private TenderBidItem tenderBidItem;
	//价格评分规则
	private TenderBidPriceSet tenderBidPriceSet;
	private List<TenderBidItem> tbiList;
	
	private Long rcId;
	private String isDetail;

	private RequiredCollect requiredCollect;
	private PurchaseRecordLog purchaseRecordLog;
	private BidProcessLog bidProcessLog;
	

	private File file;
	//导入上传模块完成alert的内容
	private String contentImport;
	//导入上传模板完成后判断是否成功
	private boolean boolImport;

	/**
	 * 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
	 * 
	 * @param rcId
	 *            项目id
	 * @param currProgress_Status
	 *            当前节点
	 * @param nextProgress_Status
	 *            下一个节点
	 * @param nextProgress_Status_Text
	 *            下一个节点的节点名称
	 * @author luguanglei
	 * @throws BaseException
	 */
	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text) throws BaseException {
		// 修改项目监控至下一个节点
		requiredCollect = new RequiredCollect();
		requiredCollect.setRcId(rcId);
		requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(rcId);
		if (requiredCollect.getServiceStatus() < nextProgress_Status) {
			requiredCollect.setServiceStatus(nextProgress_Status);
			requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
			this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
		}
		// 修改项目日志表的当前节点的完成时间
		bidProcessLog = new BidProcessLog();
		bidProcessLog.setRcId(rcId);
		bidProcessLog.setBidNode(currProgress_Status);
		bidProcessLog = this.iBidProcessLogBiz
				.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
		bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
		this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
		// 新增项目日志表的下一个节点的接收时间
		bidProcessLog = new BidProcessLog();
		bidProcessLog.setRcId(rcId);
		bidProcessLog.setBidNode(nextProgress_Status);
		bidProcessLog = this.iBidProcessLogBiz
				.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
		if (bidProcessLog.getBplId() == null) {
			bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
			this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
		}

	}
	/**
	 * 当前节点的完成时间
	 * 
	 * @param rcId
	 *            项目id
	 * @param currProgress_Status
	 *            当前节点
	 * @author luguanglei
	 * @throws BaseException
	 */
	private void updateBidMonitorAndBidProcessLogNoInsert(Long rcId,Long currProgress_Status) throws BaseException {
		// 修改项目日志表的当前节点的完成时间
		bidProcessLog = new BidProcessLog();
		bidProcessLog.setRcId(rcId);
		bidProcessLog.setBidNode(currProgress_Status);
		bidProcessLog = this.iBidProcessLogBiz
				.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
		bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
		this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);

	}
	/**
	 * 判断是否是编制人员
	 * 
	 * @param bidMonitor
	 * @return
	 */
	private boolean isWriter(String writer) {
		boolean isWriter = false;
		String username = UserRightInfoUtil.getUserName(this.getRequest());
		// 当前登录人是不是编制人员
		if (username.equals(writer))
			isWriter = true;
		return isWriter;
	}

	/**
	 * 通过项目监控进入标前准备信息
	 * 
	 * @return
	 * @throws BaseException
	 * @author luguanglei
	 */
	public String viewTenderBidPreMonitor() throws BaseException {
		String view = "tenderBidPreMonitorDetail";
		try {
			requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(rcId);

			if (!isWriter(requiredCollect.getWriter()))
				isDetail = "detail";

			TenderBidList tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			/*boolean isOpen=false;
			//判断当时时间是否大于开标时间
			Date d=new Date();
			if(tenderBidList.getOpenDate()!=null){
			if(d.getTime()>=tenderBidList.getOpenDate().getTime()){
				isOpen=true;
			}
			}*/	
			if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.OPEN_STATUS_01.equals(requiredCollect.getOpenStatus())&& StringUtil.isBlank(isDetail))
			{
				this.getRequest().setAttribute("isNext", requiredCollect.getServiceStatus()==TenderProgressStatus.Progress_Status_26);
				view="tenderBidPreMonitorUpdate";
			}else{
				tenderBidRate=new TenderBidRate();
				tenderBidRate.setRcId(rcId);
				tenderBidRate=this.iTenderBidRateBiz.getTenderBidRateByTenderBidRate(tenderBidRate);
				tenderBidRate.setTotalRate(((int)(tenderBidRate.getTechRate()==null?0:tenderBidRate.getTechRate()) + (int)(tenderBidRate.getBusinessRate()==null?0:tenderBidRate.getBusinessRate())+(int)(tenderBidRate.getPriceRate()==null?0:tenderBidRate.getPriceRate()))+"");
			
				TenderBidItem tenderBidItem =new TenderBidItem();
				tenderBidItem.setRcId(rcId);
				tenderBidItem.setItemType("1");
				List<TenderBidItem> itemsOne= this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem);
                this.getRequest().setAttribute("itemsOne", itemsOne);
				
                tenderBidItem =new TenderBidItem();
				tenderBidItem.setRcId(rcId);
				tenderBidItem.setItemType("0");
				List<TenderBidItem> itemsTwo= this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem);
                this.getRequest().setAttribute("itemsTwo", itemsTwo);
                
				TenderBidPriceSet tenderBidPriceSet=new TenderBidPriceSet();
				tenderBidPriceSet.setRcId(rcId);
				tenderBidPriceSet=this.iTenderBidPriceSetBiz.getTenderBidPriceSetByTenderBidPriceSet(tenderBidPriceSet);
				
				this.getRequest().setAttribute("tenderBidPriceSet", tenderBidPriceSet);
			
				Map judgeType = TableStatusMap.judgeType;
				//取已确定为最终参加的专家列表
				TenderBidJudge tenderBidJudge=new TenderBidJudge();
				tenderBidJudge.setRcId(rcId);
				List<TenderBidJudge> tbjList=new ArrayList<TenderBidJudge>();
				List<Object[]> objList= this.iTenderBidJudgeBiz.getTenderBidJudgeList(tenderBidJudge);
				for(Object[] obj:objList){
					tenderBidJudge=(TenderBidJudge)obj[0];
					tenderBidJudge.setExpertName((String)obj[1]);
					tenderBidJudge.setCompanyName((String)obj[2]);
					tenderBidJudge.setExpertMajor((String)obj[3]);
					tenderBidJudge.setMobilNumber((String)obj[4]);
					tbjList.add(tenderBidJudge);
				}
				this.setListValue(tbjList);
				
				this.getRequest().setAttribute("judgeType", judgeType);
				
				this.getRequest().setAttribute("isNext", requiredCollect.getServiceStatus()==TenderProgressStatus.Progress_Status_26&&StringUtil.isBlank(isDetail));
				
			}
		} catch (Exception e) {
			log("通过项目监控进入标前准备信息错误！", e);
			throw new BaseException("通过项目监控进入标前准备信息错误！", e);
		}
		return view;

	}
	/**
	 * 标前准备执行下一步
	 * @return
	 * @throws BaseException 
	 */
	public String updateTenderBidPre() throws BaseException {
		String view="success";
		try{
			Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
			String operateContent="标前准备执行下一步";
			updateBidMonitorAndBidProcessLogNoInsert(rcId, TenderProgressStatus.Progress_Status_23);
			updateBidMonitorAndBidProcessLogNoInsert(rcId, TenderProgressStatus.Progress_Status_24);
			updateBidMonitorAndBidProcessLogNoInsert(rcId, TenderProgressStatus.Progress_Status_25);
			updateBidMonitorAndBidProcessLog(rcId, TenderProgressStatus.Progress_Status_26, TenderProgressStatus.Progress_Status_27, TenderProgressStatus.Progress_Status_27_Text);
				
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setRcId(rcId);
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog.setBidNode(TenderProgressStatus.Progress_Status_26_Text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", operateContent);
		} catch (Exception e) {
			log("标前准备执行下一步错误！", e);
			throw new BaseException("标前准备执行下一步错误！", e);
		}
		return view;
		
	}
	/**
	 * 设置评标权重初始化
	 * @return
	 * @throws BaseException
	 * 
	 */
	public String updateTenderBidRateInit() throws BaseException{
		
		try{
			rcId=tenderBidRate.getRcId();
			tenderBidRate=this.iTenderBidRateBiz.getTenderBidRateByTenderBidRate(tenderBidRate);
			if (tenderBidRate.getTbrId()!=null) {
				tenderBidRate.setTotalRate(((int)(tenderBidRate.getTechRate()==null?0:tenderBidRate.getTechRate()) + (int)(tenderBidRate.getBusinessRate()==null?0:tenderBidRate.getBusinessRate())+(int)(tenderBidRate.getPriceRate()==null?0:tenderBidRate.getPriceRate()))+"");
			}else{
				tenderBidRate.setTbrId(0L);
				tenderBidRate.setRcId(rcId);
			}
			
		} catch (Exception e) {
			log("设置评标权重初始化错误！", e);
			throw new BaseException("设置评标权重初始化错误！", e);
		}
		return "tenderBidRateInit";
	}
	
	/**
	 * 设置评标权重
	 * @return
	 * @throws BaseException	
	 */
	public String updateTenderBidRate() throws BaseException{
		
		try{
			if(tenderBidRate.getTbrId()==0) {
				tenderBidRate.setWriteDate(DateUtil.getCurrentDateTime());
				tenderBidRate.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				this.iTenderBidRateBiz.saveTenderBidRate(tenderBidRate);
			}else{
				tenderBidRate.setWriteDate(DateUtil.getCurrentDateTime());
				tenderBidRate.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				this.iTenderBidRateBiz.updateTenderBidRate(tenderBidRate);
			}
			
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRcId(tenderBidRate.getRcId());
			purchaseRecordLog.setOperateContent("评标权重已设置");
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setBidNode("评标权重设置");
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			this.getRequest().setAttribute("message", "操作成功");
			this.getRequest().setAttribute("operModule", "评标权重设置");
			} catch (Exception e) {
			log("设置评标权重错误！", e);
			throw new BaseException("设置评标权重错误！", e);
		}
		return "tenderBidRateInit";
	}
	
	
	/**
	 * 设置价格评分规则初始化
	 * @return
	 * @throws BaseException
	 */
	public String updateTenderBidPriceInit() throws BaseException{
		
		try{
			rcId=tenderBidPriceSet.getRcId();
			tenderBidPriceSet=this.iTenderBidPriceSetBiz.getTenderBidPriceSetByTenderBidPriceSet(tenderBidPriceSet);
			if(tenderBidPriceSet.getTbpsId()==null){
				tenderBidPriceSet.setWriteDate(DateUtil.getCurrentDateTime());
				tenderBidPriceSet.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				tenderBidPriceSet.setRcId(rcId);
				this.iTenderBidPriceSetBiz.saveTenderBidPriceSet(tenderBidPriceSet);
			}			
		} catch (Exception e) {
			log("设置价格评分规则初始化错误！", e);
			throw new BaseException("设置价格评分规则初始化错误！", e);
		}
		return "tenderBidPriceTypeInit";
	}
	
	/**
	 * 设置价格评分规则
	 * @return
	 * @throws BaseException
	 */
	public String updateTenderBidPrice() throws BaseException{
		
		try{
			this.iTenderBidPriceSetBiz.updateTenderBidPriceSet(tenderBidPriceSet);
			
			//更新招标计划表中的报价评分方式，开评标需要用到
			TenderBidList tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(tenderBidPriceSet.getRcId());
			tenderBidList.setPriceScoreType(tenderBidPriceSet.getBrsType());
			this.iTenderBidListBiz.updateTenderBidList(tenderBidList);
			
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRcId(tenderBidPriceSet.getRcId());
			purchaseRecordLog.setOperateContent("价格评分规则已设置");
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setBidNode("价格评分规则设置");
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			
			this.getRequest().setAttribute("message", "操作成功");
			this.getRequest().setAttribute("operModule", "设置价格评分规则");
		} catch (Exception e) {
			log("设置价格评分规则错误！", e);
			throw new BaseException("设置价格评分规则错误！", e);
		}
		return "tenderBidPriceTypeInit";
	}

	/**
	 * 设置评分项初始化
	 * @return
	 * @throws BaseException
	 */
	public String updateTenderBidItemInit() throws BaseException{
		
		try{
			this.setListValue(this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem));
		} catch (Exception e) {
			log("设置评分项初始化错误！", e);
			throw new BaseException("设置评分项初始化错误！", e);
		}
		return "tenderBidItemInit";
	}
	
	/**
	 * 设置评分项
	 * @return
	 * @throws BaseException
	 */
	public String saveTenderBidItemSet() throws BaseException{
		String ids=this.getRequest().getParameter("ids");
		try{
			//页面所选评分项
			String[] idss = ids.split(",");
			for(int i=0;i<idss.length;i++){
				String biId = idss[i];
				BidItem bi = this.iBidItemBiz.getBidItem(new Long(biId));
				TenderBidItem bis = new TenderBidItem();
				bis.setRcId(tenderBidItem.getRcId());
				bis.setItemType(tenderBidItem.getItemType());
				bis.setItemId(bi.getBiId());
				bis.setItemName(bi.getItemName());
				bis.setItemDetail(bi.getItemDetail());
				bis.setPoints(bi.getPoints());
				bis.setWriteDate(DateUtil.getCurrentDateTime());
				bis.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				this.iTenderBidItemBiz.saveTenderBidItem(bis);
			}
			//查询需求计划编码
			String operateContent="";
			String bidNode="";
			if("0".equals(tenderBidItem.getItemType())){
				operateContent="技术评分项已设置";
				bidNode="技术评分项设置";
			}else if("1".equals(tenderBidItem.getItemType())){
				operateContent="商务评分项已设置";
				bidNode="商务评分项设置";
			}
			
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRcId(tenderBidItem.getRcId());
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setBidNode(bidNode);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);

			this.setListValue(this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem));

			this.getRequest().setAttribute("message", "设置成功");
			this.getRequest().setAttribute("operModule", "设置评分项");
		} catch (Exception e) {
			log("设置评分项错误！", e);
			throw new BaseException("设置评分项错误！", e);
		}
		return "tenderBidItemInit";
	}
	
	/**
	 * 更新评分项
	 * @return
	 * @throws BaseException
	 */
	public String updateTenderBidItemSet() throws BaseException{
		String closeMsg=this.getRequest().getParameter("closeMsg");
		try{
			//页面所选评分项
			if(tbiList != null){
				for(int i=0;i<tbiList.size();i++){
					TenderBidItem tbi = tbiList.get(i);
					this.iTenderBidItemBiz.updateTenderBidItem(tbi);
				}
			}
			this.getRequest().setAttribute("message", "设置成功");
			
			this.setListValue(this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem));
			
			this.getRequest().setAttribute("operModule", "更新评分项");
			this.getRequest().setAttribute("closeMsg", closeMsg);
		} catch (Exception e) {
			log("设置评分项错误！", e);
			throw new BaseException("设置评分项错误！", e);
		}
		return "tenderBidItemInit";
	}
	/**
	 * 删除评分项
	 * @return
	 * @throws BaseException
	 */
	public String deleteTenderBidItemSet() throws BaseException{

		String ids=this.getRequest().getParameter("ids");
		try{
			//页面所选评分项
			String[] idss = ids.split(",");
			for(int i=0;i<idss.length;i++){
				String bjId = idss[i];
				TenderBidItem bis = this.iTenderBidItemBiz.getTenderBidItem(new Long(bjId));
				if(i == 0){
					tenderBidItem = new TenderBidItem();
					tenderBidItem.setRcId(bis.getRcId());
					tenderBidItem.setItemType(bis.getItemType());
				}
				this.iTenderBidItemBiz.deleteTenderBidItem(bis);
			}
			this.setListValue(this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem));
			
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除评分项");
			
		} catch (Exception e) {
			log("删除评分项错误！", e);
			throw new BaseException("删除评分项错误！", e);
		}
		return "bidItemInit";
	}
	
	/**
	 * 选择评分项信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidItemInit() throws BaseException {
		
		try{
			String itemType=this.getRequest().getParameter("itemType");
			this.getRequest().setAttribute("itemType", itemType);
			//this.setListValue(this.iPurchaseBiz.getBidItemSetList(bidItemSet));
		
		} catch (Exception e) {
			log.error("选择评分项信息列表错误！", e);
			throw new BaseException("选择评分项信息列表错误！", e);
		}
		
		return "tenderBidItemSelect" ;
		
	}
	/**
	 * 查看评分项信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findBidItem() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			
			BidItem bidItem = new BidItem();
			String itemName=this.getRequest().getParameter("itemName");
			bidItem.setItemName(itemName);
			String itemType=this.getRequest().getParameter("itemType");
			bidItem.setItemType(itemType);
			
			bidItem.setComId(comId);
			tenderBidItem=new TenderBidItem();
			tenderBidItem.setRcId(rcId);
			this.setListValue(this.iBidItemBiz.getBidItemList(getRollPageDataTables(),bidItem,tenderBidItem));	
			this.getPagejsonDataTables(this.getListValue());

			this.getRequest().setAttribute("itemType", itemType);
		} catch (Exception e) {
			log.error("查看评分项信息列表错误！", e);
			throw new BaseException("查看评分项信息列表错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 评分项信息Excel导入
	 * @return
	 * @throws BaseException 
	 */
	public String saveTenderBidItemsExcel() throws BaseException {
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			//删除原有数据
			String itemType = this.getRequest().getParameter("itemType");
			TenderBidItem biSet = new TenderBidItem();
			biSet.setRcId(rcId);
			biSet.setItemType(itemType);
			List<TenderBidItem> bisList = this.iTenderBidItemBiz.getTenderBidItemList(biSet);
			for(TenderBidItem bidItemSet:bisList){
				this.iTenderBidItemBiz.deleteTenderBidItem(bidItemSet);
			}
			BidItem bidItem=null;
			//保存导入数据
			ExcelUtil eu = new ExcelUtil();
			String[][] itemsExcel = eu.readExcel(file.getPath());
			List itemsList = getBidItemsExcel(itemsExcel,itemType);
			Double sumPoints = 0.0;
			if(boolImport){
				for(int i=0;i<itemsList.size();i++){
					//保存评分项
					bidItem = (BidItem) itemsList.get(i);
					bidItem.setWriter(UserRightInfoUtil.getUserName(getRequest()));
					bidItem.setWriteDate(DateUtil.getCurrentDateTime());
					bidItem.setIsUsable("0");
					this.iBidItemBiz.saveBidItem(bidItem);
					//保存评分项设置信息
					tenderBidItem = new TenderBidItem();
					tenderBidItem.setRcId(rcId);
					tenderBidItem.setItemId(bidItem.getBiId());
					tenderBidItem.setItemName(bidItem.getItemName());
					tenderBidItem.setItemType(bidItem.getItemType());
					tenderBidItem.setItemDetail(bidItem.getItemDetail());
					tenderBidItem.setPoints(bidItem.getPoints());
					tenderBidItem.setWriter(UserRightInfoUtil.getUserName(getRequest()));
					tenderBidItem.setWriteDate(DateUtil.getCurrentDateTime());
					this.iTenderBidItemBiz.saveTenderBidItem(tenderBidItem);
					sumPoints += bidItem.getPoints();
				}
				if(sumPoints!=100){
					contentImport = "导入失败！评分值之和必须等于100";
				}else{
					contentImport = "成功";
				}
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("contentImport", contentImport);
			this.getRequest().setAttribute("operModule", "导入评分项信息");
			this.getRequest().setAttribute("message", contentImport);
			out.print(jsonObject.toString());
		} catch (Exception e) {			
			contentImport="导入失败！（模板信息有误，请检查模板信息无误后再导入）";
			log("导入评分项信息错误！", e);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("contentImport", contentImport);
			out.print(jsonObject.toString());
			//throw new BaseException("导入评分项信息错误！", e);
		}	
		return null;
		
	}
	
	/**
	 * 评分项信息Excel导入
	 * @return
	 * @throws BaseException 
	 */
	public List getBidItemsExcel(String[][] itemsExcel,String type) throws Exception{
		List itemsList = new ArrayList();
		boolImport = true;
		contentImport = "";
		String itemType = "";
		String itemName = "";
		String itemDetail = "";
		String points = "";
		for(int i=3;i<itemsExcel.length;i++){
			BidItem bidItem = new BidItem();
			if(StringUtil.isNotBlank(itemsExcel[i][0])){
				itemType = StringUtil.convertNullToBlankAndTrim(itemsExcel[i][0]);//*
				itemName = StringUtil.convertNullToBlankAndTrim(itemsExcel[i][1]);//*
				itemDetail = StringUtil.convertNullToBlankAndTrim(itemsExcel[i][2]);//*
				points = StringUtil.convertNullToBlankAndTrim(itemsExcel[i][3]);//*
				//校验评分项类型
				if(StringUtil.isNotBlank(itemType)){
					if(!itemType.equals("0")&&!itemType.equals("1")){
						boolImport = false;
						contentImport = "导入失败！第"+(i+1)+"行，评分项类别只能全是0或全是1";
					}else{
						if(!"0".equals(itemType)&&!type.equals(itemType)){
							boolImport = false;
							contentImport = "导入失败！第"+(i+1)+"行，技术评分项类别只能全是0";
						}else{
							bidItem.setItemType(itemType);
						}
						if(!"1".equals(itemType)&&!type.equals(itemType)){
							boolImport = false;
							contentImport = "导入失败！第"+(i+1)+"行，商务评分项类别只能全是1";
						}else{
							bidItem.setItemType(itemType);
						}
					}
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，评分项类别不能为空";
				}
				//校验评分项名称
				if(StringUtil.isNotBlank(itemName)){
					bidItem.setItemName(itemName);
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，评分项名称不能为空";
				}
				//校验评分项分值
				if(StringUtil.isNotBlank(points)){
					bidItem.setPoints(Double.parseDouble(points));
				}else{
					boolImport = false;
					contentImport = "导入失败！第"+(i+1)+"行，评分项分值不能为空";
				}
				bidItem.setItemDetail(itemDetail);
				itemsList.add(bidItem);
			}
		}
		return itemsList;
	}
	public ITenderBidRateBiz getiTenderBidRateBiz() {
		return iTenderBidRateBiz;
	}

	public void setiTenderBidRateBiz(ITenderBidRateBiz iTenderBidRateBiz) {
		this.iTenderBidRateBiz = iTenderBidRateBiz;
	}

	public TenderBidRate getTenderBidRate() {
		return tenderBidRate;
	}

	public void setTenderBidRate(TenderBidRate tenderBidRate) {
		this.tenderBidRate = tenderBidRate;
	}

	public ITenderBidItemBiz getiTenderBidItemBiz() {
		return iTenderBidItemBiz;
	}

	public void setiTenderBidItemBiz(ITenderBidItemBiz iTenderBidItemBiz) {
		this.iTenderBidItemBiz = iTenderBidItemBiz;
	}

	public ITenderBidPriceSetBiz getiTenderBidPriceSetBiz() {
		return iTenderBidPriceSetBiz;
	}

	public void setiTenderBidPriceSetBiz(
			ITenderBidPriceSetBiz iTenderBidPriceSetBiz) {
		this.iTenderBidPriceSetBiz = iTenderBidPriceSetBiz;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}

	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}

	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}

	public void setiPurchaseRecordLogBiz(
			IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public String getIsDetail() {
		return isDetail;
	}

	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}

	public IBidItemBiz getiBidItemBiz() {
		return iBidItemBiz;
	}

	public void setiBidItemBiz(IBidItemBiz iBidItemBiz) {
		this.iBidItemBiz = iBidItemBiz;
	}

	public ITenderBidJudgeBiz getiTenderBidJudgeBiz() {
		return iTenderBidJudgeBiz;
	}

	public void setiTenderBidJudgeBiz(ITenderBidJudgeBiz iTenderBidJudgeBiz) {
		this.iTenderBidJudgeBiz = iTenderBidJudgeBiz;
	}

	public TenderBidPriceSet getTenderBidPriceSet() {
		return tenderBidPriceSet;
	}

	public void setTenderBidPriceSet(TenderBidPriceSet tenderBidPriceSet) {
		this.tenderBidPriceSet = tenderBidPriceSet;
	}

	public TenderBidItem getTenderBidItem() {
		return tenderBidItem;
	}

	public void setTenderBidItem(TenderBidItem tenderBidItem) {
		this.tenderBidItem = tenderBidItem;
	}

	public List<TenderBidItem> getTbiList() {
		return tbiList;
	}

	public void setTbiList(List<TenderBidItem> tbiList) {
		this.tbiList = tbiList;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}

	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}

}
