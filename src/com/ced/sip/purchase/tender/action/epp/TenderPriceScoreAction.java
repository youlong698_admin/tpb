package com.ced.sip.purchase.tender.action.epp;

import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidPriceSetBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidRateBiz;
import com.ced.sip.purchase.tender.biz.ITenderPriceScoreBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.entity.TenderBidPriceSet;
import com.ced.sip.purchase.tender.entity.TenderBidRate;
import com.ced.sip.purchase.tender.entity.TenderOtherPriceScore;

public class TenderPriceScoreAction extends BaseAction{
	//价格评分服务类
	private ITenderPriceScoreBiz iTenderPriceScoreBiz;

	// 价格评分设置表
	private ITenderBidPriceSetBiz iTenderBidPriceSetBiz;
	// 评标权重表
	private ITenderBidRateBiz iTenderBidRateBiz;
	
	//招标计划
	private ITenderBidListBiz iTenderBidListBiz;
	
	private Long rcId;

	//特殊价格规则 集合
	private List<TenderOtherPriceScore> bopsObjList;
	/**
	 * 修改报价评分汇总信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String viewTenderPriceGatherTab() throws BaseException {
		
		String message="";
		try{
			
			TenderBidList tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
			
			if(TableStatus.PRICE_STATUS_04.equals(tenderBidList.getPriceStatus())){
			
			// 2.1取得标段价格评分规则信息
			TenderBidPriceSet tenderBidPriceSet = new TenderBidPriceSet() ;
			tenderBidPriceSet.setRcId(rcId) ;
			tenderBidPriceSet = this.iTenderBidPriceSetBiz.getTenderBidPriceSetByTenderBidPriceSet(tenderBidPriceSet) ;
			// 2.2判断是否设置价格评分规则
			if( StringUtil.isBlank(tenderBidPriceSet.getTbpsId())||StringUtil.isBlank(tenderBidPriceSet.getBrsType())) {
				message="温馨提示：标段价格评分规则未设置， 不能进行报价评分汇总！";
			}
			
			// 2.3判断是否设置评标权重
			TenderBidRate tenderBidRate = new TenderBidRate();
			tenderBidRate.setRcId(rcId);
			tenderBidRate=this.iTenderBidRateBiz.getTenderBidRateByTenderBidRate(tenderBidRate);
			// 2.2判断是否设置价格评分规则
			if( StringUtil.isBlank(tenderBidRate.getTbrId())||StringUtil.isBlank(tenderBidRate.getPriceRate())) {
				message="温馨提示：评标权重未设置， 不能进行报价评分汇总！";
			}
			
			// 3、根据评分规则类型进行 报价评分计算  TableStatus.Bid_Price_Type_01
			// 计算各供应商的总报价 计算最高价、最低价、平均价、其他价格值
			List sumpList = iTenderPriceScoreBiz.getSumBidPriceAa( rcId , null ) ;
			List tssList = this.iTenderPriceScoreBiz.getSupplierPriceScore(rcId,sumpList, tenderBidPriceSet, tenderBidRate ) ;
			
			this.getRequest().setAttribute("tenderBidPriceSet", tenderBidPriceSet );
			this.getRequest().setAttribute("tssList", tssList );
			}else{
				message="温馨提示：报价最终提交之后,才能计算报价评分！";
			}
			this.getRequest().setAttribute("message", message );
			
			
		} catch (Exception e) {
			log("查看报价评分汇总信息初始化错误！", e);
			throw new BaseException("查看报价评分汇总信息初始化错误！", e);
		}
		return "tenderPriceGatherTab";
		
	}
	/**
	 * 修改报价评分汇总信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateTenderPriceScoreInit() throws BaseException {
		
		String message="";
		try{
			
			// 2.1取得标段价格评分规则信息
			TenderBidPriceSet tenderBidPriceSet = new TenderBidPriceSet() ;
			tenderBidPriceSet.setRcId(rcId) ;
			tenderBidPriceSet = this.iTenderBidPriceSetBiz.getTenderBidPriceSetByTenderBidPriceSet(tenderBidPriceSet) ;
			// 2.2判断是否设置价格评分规则
			if( StringUtil.isBlank(tenderBidPriceSet.getTbpsId())||StringUtil.isBlank(tenderBidPriceSet.getBrsType())) {
				message="温馨提示：标段价格评分规则未设置， 不能进行报价评分汇总！";
			}
			
			// 2.3判断是否设置评标权重
			TenderBidRate tenderBidRate = new TenderBidRate();
			tenderBidRate.setRcId(rcId);
			tenderBidRate=this.iTenderBidRateBiz.getTenderBidRateByTenderBidRate(tenderBidRate);
			// 2.2判断是否设置价格评分规则
			if( StringUtil.isBlank(tenderBidRate.getTbrId())||StringUtil.isBlank(tenderBidRate.getPriceRate())) {
				message="温馨提示：评标权重未设置， 不能进行报价评分汇总！";
			}
			
			// 3、根据评分规则类型进行 报价评分计算  TableStatus.Bid_Price_Type_01
			// 计算各供应商的总报价 计算最高价、最低价、平均价、其他价格值
			List sumpList = iTenderPriceScoreBiz.getSumBidPriceAa( rcId , null ) ;
			List tssList = this.iTenderPriceScoreBiz.getSupplierPriceScore(rcId,sumpList, tenderBidPriceSet, tenderBidRate ) ;
			
			this.getRequest().setAttribute("tenderBidPriceSet", tenderBidPriceSet );
			this.getRequest().setAttribute("tssList", tssList );
			this.getRequest().setAttribute("message", message );
			
			
		} catch (Exception e) {
			log("修改报价评分汇总信息初始化错误！", e);
			throw new BaseException("修改报价评分汇总信息初始化错误！", e);
		}
		return "updateTenderPriceScoreInit";
		
	}
	/**
	 * 保存特殊规则报价评分信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateTenderPriceScore() throws BaseException {
		int sign=Integer.parseInt(this.getRequest().getParameter("sign"));
		try{
			if(bopsObjList!=null){
				for(TenderOtherPriceScore bops:bopsObjList){
					TenderOtherPriceScore bopScore = this.iTenderPriceScoreBiz.getTenderOtherPriceScoreByTenderOtherPriceScore(bops);
					if(bopScore.getTopsId()!=null){
						this.iTenderPriceScoreBiz.updateTenderOtherPriceScore(bopScore);
					}else{
						this.iTenderPriceScoreBiz.saveTenderOtherPriceScore(bops);
					}
				}
			}
			if(sign==2){
				TenderBidList tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
				tenderBidList.setPriceStatus(TableStatus.PRICE_STATUS_04);
				this.iTenderBidListBiz.updateTenderBidList(tenderBidList);
			}
			
			this.getRequest().setAttribute("operModule", "保存特殊规则报价评分信息");
			this.getRequest().setAttribute("message", "保存成功");	
		} catch (Exception e) {
			log("保存特殊规则报价评分信息错误！", e);
			throw new BaseException("保存特殊规则报价评分信息错误！", e);
		}
		if(sign==2){
		   return "success";
		}else{
		   return updateTenderPriceScoreInit();
		}
		
	}
	public ITenderPriceScoreBiz getiTenderPriceScoreBiz() {
		return iTenderPriceScoreBiz;
	}
	public void setiTenderPriceScoreBiz(ITenderPriceScoreBiz iTenderPriceScoreBiz) {
		this.iTenderPriceScoreBiz = iTenderPriceScoreBiz;
	}
	public ITenderBidPriceSetBiz getiTenderBidPriceSetBiz() {
		return iTenderBidPriceSetBiz;
	}
	public void setiTenderBidPriceSetBiz(ITenderBidPriceSetBiz iTenderBidPriceSetBiz) {
		this.iTenderBidPriceSetBiz = iTenderBidPriceSetBiz;
	}
	public ITenderBidRateBiz getiTenderBidRateBiz() {
		return iTenderBidRateBiz;
	}
	public void setiTenderBidRateBiz(ITenderBidRateBiz iTenderBidRateBiz) {
		this.iTenderBidRateBiz = iTenderBidRateBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public List<TenderOtherPriceScore> getBopsObjList() {
		return bopsObjList;
	}
	public void setBopsObjList(List<TenderOtherPriceScore> bopsObjList) {
		this.bopsObjList = bopsObjList;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	
}
