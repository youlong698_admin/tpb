package com.ced.sip.purchase.tender.action.epp;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.expert.biz.IExpertBiz;
import com.ced.sip.expert.entity.Expert;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeHistoryBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeTypeBiz;
import com.ced.sip.purchase.tender.entity.TenderBidJudge;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeHistory;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeType;
import com.sun.org.apache.commons.beanutils.BeanUtils;
/** 
 * 类名称：TenderBidJudgeAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-12
 */
public class TenderBidJudgeAction extends BaseAction {

	// 评标专家 
	private ITenderBidJudgeBiz iTenderBidJudgeBiz;
	
	// 评标专家历史表 
	private ITenderBidJudgeHistoryBiz iTenderBidJudgeHistoryBiz;
	//专家库专家
	private IExpertBiz iExpertBiz;
	//评标专家评标类别
	private ITenderBidJudgeTypeBiz iTenderBidJudgeTypeBiz;
	
	private Long rcId;
	// 评标专家
	private TenderBidJudge tenderBidJudge;
	// 评标专家历史表
	private TenderBidJudgeHistory tenderBidJudgeHistory;
	
	/**
	 * 查看评标专家信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTenderBidJudge() throws BaseException {
		
		try{
			Map judgeType = TableStatusMap.judgeType;
			//取已确定为最终参加的专家列表
			tenderBidJudge=new TenderBidJudge();
			tenderBidJudge.setRcId(rcId);
			List<TenderBidJudge> tbjList=new ArrayList<TenderBidJudge>();
			List<Object[]> objList= this.iTenderBidJudgeBiz.getTenderBidJudgeList(tenderBidJudge);
			for(Object[] obj:objList){
				tenderBidJudge=(TenderBidJudge)obj[0];
				tenderBidJudge.setExpertName((String)obj[1]);
				tenderBidJudge.setCompanyName((String)obj[2]);
				tenderBidJudge.setExpertMajor((String)obj[3]);
				tenderBidJudge.setMobilNumber((String)obj[4]);
				tbjList.add(tenderBidJudge);
			}
			this.setListValue(tbjList);
			
			this.getRequest().setAttribute("judgeType", judgeType);
		} catch (Exception e) {
			log.error("查看评标专家信息列表错误！", e);
			throw new BaseException("查看评标专家信息列表错误！", e);
		}	
			
		return VIEW ;
	}
	/**
	 * 抽取的专家进行保存
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateBidJudgeSave() throws BaseException {
		try{
			String expertIds=this.getRequest().getParameter("expertIds");
			String[] selectedList=expertIds.split(","); 
			int selectNumber = this.iTenderBidJudgeHistoryBiz.getMaxExpertSelectNumber(rcId)+1;
			
			//保存抽取数据到抽取历史表中
			for(int i=1;i<selectedList.length;i++){
				Long expertId = Long.parseLong(selectedList[i]);
				tenderBidJudgeHistory = new TenderBidJudgeHistory();
				tenderBidJudgeHistory.setRcId(rcId);
				tenderBidJudgeHistory.setExpertId(expertId);
				tenderBidJudgeHistory.setSelectNumber(selectNumber);//抽取批次
				tenderBidJudgeHistory.setSelectDate(DateUtil.getCurrentDateTime());
				tenderBidJudgeHistory.setWriteDate(DateUtil.getCurrentDateTime());
				tenderBidJudgeHistory.setIfJoinFinal(TableStatus.Common_Jion_N);//默认为否
				tenderBidJudgeHistory.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				this.iTenderBidJudgeHistoryBiz.saveTenderBidJudgeHistory(tenderBidJudgeHistory);
				
				tenderBidJudge=new TenderBidJudge();
				BeanUtils.copyProperties(tenderBidJudge, tenderBidJudgeHistory);
				
				this.iTenderBidJudgeBiz.saveTenderBidJudge(tenderBidJudge);
			}
		} catch (Exception e) {
			log.error("抽取专家进行保存错误！", e);
			throw new BaseException("抽取专家进行保存错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 抽取的专家进行选择评标类别保存
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateTerderBidJudgeType() throws BaseException {
		try{
			Long tbjId=Long.parseLong(this.getRequest().getParameter("tbjId"));
			String key=this.getRequest().getParameter("key");
			tenderBidJudge=this.iTenderBidJudgeBiz.getTenderBidJudge(tbjId);
			
			tenderBidJudge.setExpertRole(key);
			this.iTenderBidJudgeBiz.updateTenderBidJudge(tenderBidJudge);
			
			this.iTenderBidJudgeTypeBiz.deleTenderBidJudgeType(tbjId);
			TenderBidJudgeType tenderBidJudgeType=new TenderBidJudgeType();
			tenderBidJudgeType.setExpertId(tenderBidJudge.getExpertId());
			tenderBidJudgeType.setRcId(tenderBidJudge.getRcId());
			tenderBidJudgeType.setTbjId(tbjId);
			//更新专家的评标类别
			if(TableStatus.Expert_Judge_Type_Business.equals(key)){//商务评委
				tenderBidJudgeType.setTableName(TableStatus.Judge_Table_Type_Business);
				this.iTenderBidJudgeTypeBiz.saveTenderBidJudgeType(tenderBidJudgeType);
			}else if(TableStatus.Expert_Judge_Type_Tech.equals(key)){//技术评委
				tenderBidJudgeType.setTableName(TableStatus.Judge_Table_Type_Tech);
				this.iTenderBidJudgeTypeBiz.saveTenderBidJudgeType(tenderBidJudgeType);
			}else{//综合评委
				tenderBidJudgeType.setTableName(TableStatus.Judge_Table_Type_Business);
				this.iTenderBidJudgeTypeBiz.saveTenderBidJudgeType(tenderBidJudgeType);
				
				tenderBidJudgeType=new TenderBidJudgeType();
				tenderBidJudgeType.setExpertId(tenderBidJudge.getExpertId());
				tenderBidJudgeType.setRcId(tenderBidJudge.getRcId());
				tenderBidJudgeType.setTbjId(tbjId);
				tenderBidJudgeType.setTableName(TableStatus.Judge_Table_Type_Tech);
				this.iTenderBidJudgeTypeBiz.saveTenderBidJudgeType(tenderBidJudgeType);
			}
			this.getRequest().setAttribute("message", "更改成功");
			this.getRequest().setAttribute("operModule", "更改抽取的专家进行评标类别");
		} catch (Exception e) {
			log.error("抽取的专家进行选择评标类别保存错误！", e);
			throw new BaseException("抽取的专家进行选择评标类别保存错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 删除抽取的专家
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateTenderBidJudgeDelete() throws BaseException {
		try{
			String tbjId=this.getRequest().getParameter("tbjId");
			this.iTenderBidJudgeBiz.deleteTenderBidJudge(tbjId);
			PrintWriter out = this.getResponse().getWriter();
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除抽取专家");
			out.print("success");
		} catch (Exception e) {
			log.error("删除抽取的专家错误！", e);
			throw new BaseException("删除抽取的专家错误！", e);
		}
		
		return null ;
		
	}
	/**
	 * 选择专家器--抽取专家列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewExpertSelect() throws BaseException {
		try{
            String ul=this.getRequest().getParameter("ul");;
			this.getRequest().setAttribute("ul", ul);
		} catch (Exception e) {
			log.error("选择专家器--抽取专家列表错误！", e);
			throw new BaseException("选择专家器--抽取专家列表 错误！", e);
		}
		return "expertIndex" ;		
	}
	/**
	 * 选择专家器 左侧页面
	 * @return
	 * @throws Exception
	 */
	public String viewExpertLeft() throws BaseException {
		String ul=this.getRequest().getParameter("ul");
        String rcId=this.getRequest().getParameter("rcId");
		this.getRequest().setAttribute("ul", ul);
		this.getRequest().setAttribute("rcId", rcId);		
		return "expertLeft" ;
	}
    /**
     * 选择专家器 上部页面
     * @return
     * @throws Exception
     */
	public String viewExpert() throws BaseException {
	    String ul=this.getRequest().getParameter("ul");
		this.getRequest().setAttribute("ul", ul);
		return "viewExpert" ;
	}
    /**
     * 选择专家器----专家库专家
     * @return
     * @throws BaseException
     */
	public String findExpert() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			Expert expert=new Expert();
			String expertName=this.getRequest().getParameter("expertName");
			expert.setExpertName(expertName);
			String expertMajor=this.getRequest().getParameter("expertMajor");
			expert.setExpertMajor(expertMajor);
			String status=this.getRequest().getParameter("status");
			expert.setStatus(status);
			expert.setComId(comId);
			this.setListValue(this.iExpertBiz.getExpertListByRcId(this.getRollPageDataTables(), expert,rcId));
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("选择专家器--查看专家库专家信息列表错误！", e);
			throw new BaseException("选择专家器--查看专家库专家信息列表错误！", e);
		}
	    return null ;
	}
    /**
     * 选择专家器-----选择下部页面
     * @return
     * @throws BaseException
     */
	public String viewExpertBotton() throws BaseException {
		String ul = this.getRequest().getParameter("ul");

		String m="";
		if (ul == "-1" || "-1".equals(ul)) {
			ul = "";
		}
		if (StringUtil.isNotBlank(ul)) {
			
			String [] mn = ul.split(",");
			String mc ="";
			for(int i=0;i<mn.length;i++){
				m=this.iExpertBiz.getExpert(Long.parseLong(mn[i])).getExpertName();
				mc+=mn[i]+":"+m+",";
			}
			this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
		}else{
		    this.getRequest().setAttribute("ul", "-1");
		}
		return "expertBotton" ;
	}
	

	public ITenderBidJudgeBiz getiTenderBidJudgeBiz() {
		return iTenderBidJudgeBiz;
	}

	public void setiTenderBidJudgeBiz(ITenderBidJudgeBiz iTenderBidJudgeBiz) {
		this.iTenderBidJudgeBiz = iTenderBidJudgeBiz;
	}

	public TenderBidJudge getTenderBidJudge() {
		return tenderBidJudge;
	}

	public void setTenderBidJudge(TenderBidJudge tenderBidJudge) {
		this.tenderBidJudge = tenderBidJudge;
	}

	public ITenderBidJudgeHistoryBiz getiTenderBidJudgeHistoryBiz() {
		return iTenderBidJudgeHistoryBiz;
	}

	public void setiTenderBidJudgeHistoryBiz(
			ITenderBidJudgeHistoryBiz iTenderBidJudgeHistoryBiz) {
		this.iTenderBidJudgeHistoryBiz = iTenderBidJudgeHistoryBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public IExpertBiz getiExpertBiz() {
		return iExpertBiz;
	}
	public void setiExpertBiz(IExpertBiz iExpertBiz) {
		this.iExpertBiz = iExpertBiz;
	}
	public ITenderBidJudgeTypeBiz getiTenderBidJudgeTypeBiz() {
		return iTenderBidJudgeTypeBiz;
	}
	public void setiTenderBidJudgeTypeBiz(
			ITenderBidJudgeTypeBiz iTenderBidJudgeTypeBiz) {
		this.iTenderBidJudgeTypeBiz = iTenderBidJudgeTypeBiz;
	}
	
}
