package com.ced.sip.purchase.tender.action.epp;

import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.base.biz.IBidCommunicationInfoBiz;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
import com.ced.sip.purchase.tender.biz.ITenderReceivedBulletinBiz;
import com.ced.sip.purchase.tender.entity.TenderReceivedBulletin;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
/** 
 * 类名称：TenderBidCommunicationInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-15
 */
public class TenderBidCommunicationInfoAction extends BaseAction {

	// 标中质询
	private IBidCommunicationInfoBiz iBidCommunicationInfoBiz;
	
	//回标供应商
	private ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz;
	
	private ISupplierInfoBiz iSupplierInfoBiz;
	
	private Long rcId;
	// 交流信息
	private BidCommunicationInfo bidCommunicationInfo;

	/**
	 * 查看交流信息列表 标段监控节点
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidCommunicationInfoTab() throws BaseException {
		
		try{
			bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setRcId(rcId);
			List<BidCommunicationInfo> bidCommunicationInfoList=this.iBidCommunicationInfoBiz.getBidCommunicationInfoList(bidCommunicationInfo);
			this.getRequest().setAttribute("bidCommunicationInfoList", bidCommunicationInfoList);
			
		} catch (Exception e) {
			log.error("查看交流信息信息列表标段监控错误！", e);
			throw new BaseException("查看交流信息信息列表标段监控错误！", e);
		}	
			
		return "bidCommunicationInfoTab" ;
				
	}

	/**
	 * 新增标中质询信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidCommunicationInfoInit() throws BaseException {
		
		try{
			bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setType("2");
			bidCommunicationInfo.setIdentification("2");
			bidCommunicationInfo.setRcId(rcId);
			TenderReceivedBulletin tenderReceivedBulletin=new TenderReceivedBulletin();
			tenderReceivedBulletin.setRcId(rcId);
			List<TenderReceivedBulletin> list=this.iTenderReceivedBulletinBiz.getTenderReceivedBulletinList(tenderReceivedBulletin);
			this.getRequest().setAttribute("now", DateUtil.getCurrentDateTime());
			this.getRequest().setAttribute("list", list);
		} catch (Exception e) {
			log("新增标中质询信息初始化错误！", e);
			throw new BaseException("新增标中质询信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 新增标中质询信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidCommunicationInfo() throws BaseException {
		
		try{
			String[] supplierIds=this.getRequest().getParameterValues("supplierId");
			SupplierInfo supplierInfo=null;
			BidCommunicationInfo info=null;
			for(String str:supplierIds){
				if(!"0".equals(str)){
				info=new BidCommunicationInfo();
				info.setQuestionContent(bidCommunicationInfo.getQuestionContent());
				info.setRcId(bidCommunicationInfo.getRcId());
				info.setType(bidCommunicationInfo.getType());
				info.setIdentification(bidCommunicationInfo.getIdentification());
				String userName=UserRightInfoUtil.getChineseName(getRequest());
				Long userId=UserRightInfoUtil.getUserId(getRequest());
				info.setQuestionerId(userId);
				info.setQuestionerName(userName);
				info.setQuestionDate(DateUtil.getCurrentDateTime());
				info.setAnswerId(Long.parseLong(str));
				supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(Long.parseLong(str));
				info.setAnswerName(supplierInfo.getSupplierName());
				this.iBidCommunicationInfoBiz.saveBidCommunicationInfo(info);
				}
			}
			rcId=bidCommunicationInfo.getRcId();
			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "新增标中质询");
		} catch (Exception e) {
			log("新增标中质询信息错误！", e);
			throw new BaseException("新增标中质询信息错误！", e);
		}
		return ADD_INIT;
		
	}
	/**
	 * 查看交流信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidCommunicationInfoDetail() throws BaseException {
		
		try{
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
		} catch (Exception e) {
			log("查看标中质询明细信息错误！", e);
			throw new BaseException("查看标中质询明细信息错误！", e);
		}
		return DETAIL;
		
	}
	public BidCommunicationInfo getBidCommunicationInfo() {
		return bidCommunicationInfo;
	}

	public void setBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) {
		this.bidCommunicationInfo = bidCommunicationInfo;
	}
	public IBidCommunicationInfoBiz getiBidCommunicationInfoBiz() {
		return iBidCommunicationInfoBiz;
	}
	public void setiBidCommunicationInfoBiz(
			IBidCommunicationInfoBiz iBidCommunicationInfoBiz) {
		this.iBidCommunicationInfoBiz = iBidCommunicationInfoBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public ITenderReceivedBulletinBiz getiTenderReceivedBulletinBiz() {
		return iTenderReceivedBulletinBiz;
	}

	public void setiTenderReceivedBulletinBiz(
			ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz) {
		this.iTenderReceivedBulletinBiz = iTenderReceivedBulletinBiz;
	}

	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	
}
