package com.ced.sip.purchase.tender.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ced.sip.purchase.base.entity.BidProcessLog;

/**
 * 此类是描述通过标段监控进入采购流程页需要的节点
 * @author luguanglei
 *
 */
public class TenderProgressStatusList {
	
	/**
	 * 采购过程
	 * @param purchaseWay
	 * @param map
	 * @return
	 */
	public static List<BidProcessLog> getbidProcessLogList(Map<Long,Object> map) {
		List<BidProcessLog> bidProcessLogList = new ArrayList<BidProcessLog>();
		BidProcessLog bidProcessLog;
		BidProcessLog bidProcessLogNull=new BidProcessLog();
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_20)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_20);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_20,
					TenderProgressStatus.Progress_Status_20_Text,
					TenderProgressStatus.Progress_Status_20_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_20_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_21)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_21);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_21,
					TenderProgressStatus.Progress_Status_21_Text,
					TenderProgressStatus.Progress_Status_21_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_21_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_22)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_22);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_22,
					TenderProgressStatus.Progress_Status_22_Text,
					TenderProgressStatus.Progress_Status_22_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_22_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_23)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_23);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_23,
					TenderProgressStatus.Progress_Status_23_Text,
					TenderProgressStatus.Progress_Status_23_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_23_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_24)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_24);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_24,
					TenderProgressStatus.Progress_Status_24_Text,
					TenderProgressStatus.Progress_Status_24_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_24_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_25)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_25);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_25,
					TenderProgressStatus.Progress_Status_25_Text,
					TenderProgressStatus.Progress_Status_25_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_25_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_26)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_26);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_26,
					TenderProgressStatus.Progress_Status_26_Text,
					TenderProgressStatus.Progress_Status_26_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_26_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_27)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_27);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_27,
					TenderProgressStatus.Progress_Status_27_Text,
					TenderProgressStatus.Progress_Status_27_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_27_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_28)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_28);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_28,
					TenderProgressStatus.Progress_Status_28_Text,
					TenderProgressStatus.Progress_Status_28_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_28_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_29)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_29);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_29,
					TenderProgressStatus.Progress_Status_29_Text,
					TenderProgressStatus.Progress_Status_29_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_29_IMG));
			bidProcessLog=map.get(TenderProgressStatus.Progress_Status_30)==null?bidProcessLogNull:(BidProcessLog)map.get(TenderProgressStatus.Progress_Status_30);
			bidProcessLogList.add(new BidProcessLog(
					TenderProgressStatus.Progress_Status_30,
					TenderProgressStatus.Progress_Status_30_Text,
					TenderProgressStatus.Progress_Status_30_URL,bidProcessLog.getReceiveDate(),bidProcessLog.getCompleteDate(),bidProcessLog.getBplId(),bidProcessLog.getDay(),TenderProgressStatus.Progress_Status_30_IMG));
			return bidProcessLogList;
	}
	
}
