package com.ced.sip.purchase.tender.util;

public class TenderProgressStatus {
//	--------------------------- 招标采购流程节点状态  begin---------------------------
//   采购流程节点状态以 1012 显示，前两位为大的流程节点  后两位为小的流程节点
	/** 招标信息 */
	public static long Progress_Status_20 = 20 ;
	/** 招标信息 */
	public static String Progress_Status_20_Text = "招标计划" ;	
	/** 招标信息 url */
	public static String Progress_Status_20_URL = "viewTenderBidListMonitor_tenderBidList.action" ;
	/** 招标信息 IMG */
	public static String Progress_Status_20_IMG = "images/bidImages/swhz.png" ;
	
	/** 招标文件*/
	public static long Progress_Status_21 = 21 ;
	/** 招标文件 */
	public static String Progress_Status_21_Text = "招标文件" ;	
	/** 招标文件 url */
	public static String Progress_Status_21_URL = "viewTenderBidFileMonitor_tenderBidFile.action" ;
	/** 招标文件 IMG */
	public static String Progress_Status_21_IMG = "images/bidImages/wjsh.png" ;
	
	/** 招标公告 */
	public static long Progress_Status_22 = 22 ;
	/** 招标公告 */
	public static String Progress_Status_22_Text = "招标公告" ;	
	/** 招标公告 url */
	public static String Progress_Status_22_URL = "viewBidBulletinBidMonitor_bidBulletin.action" ;
	/** 招标公告 IMG */
	public static String Progress_Status_22_IMG = "images/bidImages/gg.png" ;
	
	
	/** 应标响应 */
	public static long Progress_Status_23 = 23 ;
	/** 应标响应 */
	public static String Progress_Status_23_Text = "应标响应" ;	
	/** 应标响应 url */
	public static String Progress_Status_23_URL = "viewTenderFileDownloadMonitor_tenderFileDownload.action" ;
	/** 应标响应 IMG */
	public static String Progress_Status_23_IMG = "images/bidImages/ybgl.png" ;
	
	/** 回标响应 */
	public static long Progress_Status_24 = 24 ;
	/** 回标响应 */
	public static String Progress_Status_24_Text = "回标响应" ;	
	/** 回标响应 url */
	public static String Progress_Status_24_URL = "viewTenderReceivedBulletinMonitor_tenderReceivedBulletin.action" ;
	/** 回标响应 IMG */
	public static String Progress_Status_24_IMG = "images/bidImages/ybgl.png" ;
	
	/** 标前澄清 */
	public static long Progress_Status_25 = 25 ;
	/** 标前澄清 */
	public static String Progress_Status_25_Text = "标前澄清" ;	
	/** 标前澄清 url */
	public static String Progress_Status_25_URL = "viewBidClarifyMonitor_bidClarify.action" ;
	/** 标前澄清 IMG */
	public static String Progress_Status_25_IMG = "images/bidImages/wtcq.png" ;
	
	
	/** 标前准备 */
	public static long Progress_Status_26 = 26 ;
	/** 标前准备 */
	public static String Progress_Status_26_Text = "标前准备" ;	
	/** 标前准备 url */
	public static String Progress_Status_26_URL = "viewTenderBidPreMonitor_tenderBidPre.action" ;
	/** 标前准备 IMG */
	public static String Progress_Status_26_IMG = "images/bidImages/sr.png" ;
	
	/** 开评标*/
	public static long Progress_Status_27 = 27 ;
	/** 开评标 */
	public static String Progress_Status_27_Text = "开评标" ;	
	/** 开评标url */
	public static String Progress_Status_27_URL = "viewTenderBidOpenMonitor_tenderBidOpen.action" ;
	/** 开评标 IMG */
	public static String Progress_Status_27_IMG = "images/bidImages/wtcq.png" ;
	
	
	/** 授标 */
	public static long Progress_Status_28 = 28 ;
	/** 授标 */
	public static String Progress_Status_28_Text = "授标" ;	
	/** 授标 url */
	public static String Progress_Status_28_URL = "viewBidAwardMonitor_bidAward.action" ;
	/** 授标 IMG */
	public static String Progress_Status_28_IMG = "images/bidImages/sr.png" ;
	
	/** 结果公示 */
	public static long Progress_Status_29 = 29 ;
	/** 结果公示 */
	public static String Progress_Status_29_Text = "结果公示" ;	
	/** 结果公示 url */
	public static String Progress_Status_29_URL = "viewBidWinningBidMonitor_bidWinning.action" ;
	/** 结果公示 IMG */
	public static String Progress_Status_29_IMG = "images/bidImages/fqht.png" ;
	
	
	/** 中标通知 */
	public static long Progress_Status_30 = 30 ;
	/** 中标通知 */
	public static String Progress_Status_30_Text = "中标通知" ;	
	/** 中标通知 url */
	public static String Progress_Status_30_URL = "viewBidResultNoticeMonitor_bidResultNotice.action" ;
	/** 中标通知 IMG */
	public static String Progress_Status_30_IMG = "images/bidImages/zbtzs.png" ;
	
	/** 采购完成 */
	public static long Progress_Status_31 = 31;
	/** 采购完成 */
	public static String Progress_Status_31_Text = "采购完成" ;	
	
//  ---------------------------  整个采购流程节点状态  end---------------------------
}
