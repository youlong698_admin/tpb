package com.ced.sip.purchase.base.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：BidPriceHistory
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceHistory extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long bphId;     //主键
	private Long supplierId;     //供应商ID
	private Long rcId;     //项目ID
	private String encryTotalPrice;	 //加密总价
	private Double totalPrice;	//总价
	private String writer;	 //编制人
	private Date writeDate;    //编制时间
	private String remark;	 //备注
	private String remark1;	 //备注1
	private String remark2;	 //备注2
	private Double taxRate;	//税率
	
	
	public BidPriceHistory() {
		super();
	}
	
	public Long getBphId(){
	   return  bphId;
	} 
	public void setBphId(Long bphId) {
	   this.bphId = bphId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getEncryTotalPrice(){
	   return  encryTotalPrice;
	} 
	public void setEncryTotalPrice(String encryTotalPrice) {
	   this.encryTotalPrice = encryTotalPrice;
    }
	public Double getTotalPrice(){
	   return  totalPrice;
	} 
	public void setTotalPrice(Double totalPrice) {
	   this.totalPrice = totalPrice;
    }	
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public Double getTaxRate(){
	   return  taxRate;
	} 
	public void setTaxRate(Double taxRate) {
	   this.taxRate = taxRate;
    }	
}