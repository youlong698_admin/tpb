package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidCommunicationInfo
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidCommunicationInfo implements java.io.Serializable {

	// 属性信息
	private Long bciId;     //主键
	private Long rcId;     //项目ID
	private Long questionerId;     //提问者ID
	private String questionerName;	 //提问者名称
	private String questionContent;	 //提问内容
	private Date questionDate;    //提问日期
	private Long answerId;     //回答者ID
	private String answerName;	 //回答者名称
	private Date answerDate;    //回答日期
	private String answerContent;	 //回答内容
	private String type;	 //类型 1 供应商发问  2项目负责人发问
	private String identification;	 //标识 1标前交流  2 标中交流
	private String readStatus;//已读状态
	
	
	public BidCommunicationInfo() {
		super();
	}
	
	public Long getBciId(){
	   return  bciId;
	} 
	public void setBciId(Long bciId) {
	   this.bciId = bciId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getQuestionerId(){
	   return  questionerId;
	} 
	public void setQuestionerId(Long questionerId) {
	   this.questionerId = questionerId;
    }     
	public String getQuestionerName(){
	   return  questionerName;
	} 
	public void setQuestionerName(String questionerName) {
	   this.questionerName = questionerName;
    }
	public String getQuestionContent(){
	   return  questionContent;
	} 
	public void setQuestionContent(String questionContent) {
	   this.questionContent = questionContent;
    }
	public Date getQuestionDate(){
	   return  questionDate;
	} 
	public void setQuestionDate(Date questionDate) {
	   this.questionDate = questionDate;
    }	    
	public Long getAnswerId(){
	   return  answerId;
	} 
	public void setAnswerId(Long answerId) {
	   this.answerId = answerId;
    }     
	public String getAnswerName(){
	   return  answerName;
	} 
	public void setAnswerName(String answerName) {
	   this.answerName = answerName;
    }
	public Date getAnswerDate(){
	   return  answerDate;
	} 
	public void setAnswerDate(Date answerDate) {
	   this.answerDate = answerDate;
    }	    
	public String getAnswerContent(){
	   return  answerContent;
	} 
	public void setAnswerContent(String answerContent) {
	   this.answerContent = answerContent;
    }
	public String getType(){
	   return  type;
	} 
	public void setType(String type) {
	   this.type = type;
    }
	public String getIdentification(){
	   return  identification;
	} 
	public void setIdentification(String identification) {
	   this.identification = identification;
    }
	public String getReadStatus() {
		return readStatus;
	}
	public void setReadStatus(String readStatus) {
		this.readStatus = readStatus;
	}
}