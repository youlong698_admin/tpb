package com.ced.sip.purchase.base.entity;

/** 
 * 类名称：BidPriceNegotiate
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceNegotiate implements java.io.Serializable {

	// 属性信息
	private Long bpnId;     //主键
	private Long bnId;     //磋商信息ID
	private Long supplierId;     //供应商ID
	private Long rcdId;     //采购计划明细ID
	private Double lowestPrice;	 //当前报价的最低价
	private Double expectPrice;     //期望价格
	private String remark;	 //备注
	
	
	public BidPriceNegotiate() {
		super();
	}
	
	public Long getBpnId(){
	   return  bpnId;
	} 
	public void setBpnId(Long bpnId) {
	   this.bpnId = bpnId;
    }     
	public Long getBnId(){
	   return  bnId;
	} 
	public void setBnId(Long bnId) {
	   this.bnId = bnId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getRcdId(){
	   return  rcdId;
	} 
	public void setRcdId(Long rcdId) {
	   this.rcdId = rcdId;
    }     
	public Double getLowestPrice(){
	   return  lowestPrice;
	} 
	public void setLowestPrice(Double lowestPrice) {
	   this.lowestPrice = lowestPrice;
    }
	public Double getExpectPrice(){
	   return  expectPrice;
	} 
	public void setExpectPrice(Double expectPrice) {
	   this.expectPrice = expectPrice;
    }     
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
}