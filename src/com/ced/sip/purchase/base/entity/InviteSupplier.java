package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：InviteSupplier
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public class InviteSupplier implements java.io.Serializable {

	// 属性信息
	private Long isId;     //主键
	private Long rcId;     //项目ID
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String isPriceAa;	 //是否已回标
	private String isWinBiding;	 //是否中标
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Long priceNum;     //报价次数
	private String publicKey; //公钥
	private String privateKey; //私钥
	private String isTenderBidFile;//是否允许下载招标文件
	private String supplierPhone;//供应商手机号
	private String supplierEmail;//供应商Email;
	private String isSms;//短信是否通知
	private int sourceCategory;//来源类别
	
	
	public InviteSupplier() {
		super();
	}
	
	public Long getIsId(){
	   return  isId;
	} 
	public void setIsId(Long isId) {
	   this.isId = isId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getIsPriceAa(){
	   return  isPriceAa;
	} 
	public void setIsPriceAa(String isPriceAa) {
	   this.isPriceAa = isPriceAa;
    }
	public String getIsWinBiding(){
	   return  isWinBiding;
	} 
	public void setIsWinBiding(String isWinBiding) {
	   this.isWinBiding = isWinBiding;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getPriceNum(){
	   return  priceNum;
	} 
	public void setPriceNum(Long priceNum) {
	   this.priceNum = priceNum;
    }

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getIsTenderBidFile() {
		return isTenderBidFile;
	}

	public void setIsTenderBidFile(String isTenderBidFile) {
		this.isTenderBidFile = isTenderBidFile;
	}

	public String getSupplierPhone() {
		return supplierPhone;
	}

	public void setSupplierPhone(String supplierPhone) {
		this.supplierPhone = supplierPhone;
	}

	public String getSupplierEmail() {
		return supplierEmail;
	}

	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}

	public String getIsSms() {
		return isSms;
	}

	public void setIsSms(String isSms) {
		this.isSms = isSms;
	}

	public int getSourceCategory() {
		return sourceCategory;
	}

	public void setSourceCategory(int sourceCategory) {
		this.sourceCategory = sourceCategory;
	}     
}