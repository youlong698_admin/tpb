package com.ced.sip.purchase.base.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：BidClarify
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidClarify extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long bcId;     //主键
	private Long rcId;     //项目ID
	private String clarifyTitle;	 //澄清标题
	private String clarifyContent;	 //澄清内容
	private String writer;	 //编制人
	private Date writeDate;    //编制时间
	private String remark;	 //备注
	private String status;	 //状态
	private String statusCn;
	
	private int isView; //是否已阅  0是  1否
	
	
	public BidClarify() {
		super();
	}
	
	public Long getBcId(){
	   return  bcId;
	} 
	public void setBcId(Long bcId) {
	   this.bcId = bcId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getClarifyTitle(){
	   return  clarifyTitle;
	} 
	public void setClarifyTitle(String clarifyTitle) {
	   this.clarifyTitle = clarifyTitle;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getClarifyContent() {
		return clarifyContent;
	}

	public void setClarifyContent(String clarifyContent) {
		this.clarifyContent = clarifyContent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCn() {
		return statusCn;
	}

	public void setStatusCn(String statusCn) {
		this.statusCn = statusCn;
	}

	public int getIsView() {
		return isView;
	}

	public void setIsView(int isView) {
		this.isView = isView;
	}
	
}