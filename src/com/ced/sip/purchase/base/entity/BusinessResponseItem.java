package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BusinessResponseItem
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public class BusinessResponseItem implements java.io.Serializable {

	// 属性信息
	private Long briId;     //主键
	private Long rcId;     //项目ID
	private String responseItemName;	 //响应项名称
	private String responseRequirements;	 //响应项要求
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	
	
	public BusinessResponseItem() {
		super();
	}
	
	public Long getBriId(){
	   return  briId;
	} 
	public void setBriId(Long briId) {
	   this.briId = briId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    } 
	public String getResponseItemName(){
	   return  responseItemName;
	} 
	public void setResponseItemName(String responseItemName) {
	   this.responseItemName = responseItemName;
    }
	public String getResponseRequirements(){
	   return  responseRequirements;
	} 
	public void setResponseRequirements(String responseRequirements) {
	   this.responseRequirements = responseRequirements;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
}