package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidPrice
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPrice implements java.io.Serializable {

	// 属性信息
	private Long bpId;     //主键
	private Long supplierId;     //供应商ID
	private Long rcId;     //项目ID
	private String encryTotalPrice;	 //加密总价
	private Double totalPrice;	//总价
	private String writer;	 //编制人
	private Date writeDate;    //编制时间
	private String remark;	 //备注
	private String remark1;	 //备注1
	private String remark2;	 //备注2
	private Double taxRate;	//税率
	
	private String supplierName;
	private String isResponseNegotiate; //是否有磋商信息 0有磋商但是供方没响应    1有磋商供方也响应了  2 没有磋商信息
	
	private Long baId; //授标ID
	private Double bidPrice;	//授标总价
	
	
	public BidPrice() {
		super();
	}
	
	public Long getBpId(){
	   return  bpId;
	} 
	public void setBpId(Long bpId) {
	   this.bpId = bpId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getEncryTotalPrice(){
	   return  encryTotalPrice;
	} 
	public void setEncryTotalPrice(String encryTotalPrice) {
	   this.encryTotalPrice = encryTotalPrice;
    }
	public Double getTotalPrice(){
	   return  totalPrice;
	} 
	public void setTotalPrice(Double totalPrice) {
	   this.totalPrice = totalPrice;
    }	
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public Double getTaxRate(){
	   return  taxRate;
	} 
	public void setTaxRate(Double taxRate) {
	   this.taxRate = taxRate;
    }

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getIsResponseNegotiate() {
		return isResponseNegotiate;
	}

	public void setIsResponseNegotiate(String isResponseNegotiate) {
		this.isResponseNegotiate = isResponseNegotiate;
	}

	public Long getBaId() {
		return baId;
	}

	public void setBaId(Long baId) {
		this.baId = baId;
	}

	public Double getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(Double bidPrice) {
		this.bidPrice = bidPrice;
	}
	
}