package com.ced.sip.purchase.base.entity;


/** 
 * 类名称：BidAwardDetail
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidAwardDetail implements java.io.Serializable {

	// 属性信息
	private Long badId;     //主键
	private Long baId;     //授标ID
	private Long rcdId;     //采购计划明细ID
	private Double price;	//授标价
	private Double awardAmount;	//授标数量
	private String remark;	 //备注
	private String remark1;	 //备注1
	private String remark2;	 //备注2
	private int isOrder;//是否生成订单
	

	private RequiredCollectDetail requiredCollectDetail;
	
	
	public BidAwardDetail() {
		super();
	}
	
	public Long getBadId(){
	   return  badId;
	} 
	public void setBadId(Long badId) {
	   this.badId = badId;
    }     
	public Long getBaId(){
	   return  baId;
	} 
	public void setBaId(Long baId) {
	   this.baId = baId;
    }     
	public Long getRcdId(){
	   return  rcdId;
	} 
	public void setRcdId(Long rcdId) {
	   this.rcdId = rcdId;
    }     
	public Double getPrice(){
	   return  price;
	} 
	public void setPrice(Double price) {
	   this.price = price;
    }		
	public Double getAwardAmount(){
	   return  awardAmount;
	} 
	public void setAwardAmount(Double awardAmount) {
	   this.awardAmount = awardAmount;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }

	public int getIsOrder() {
		return isOrder;
	}

	public void setIsOrder(int isOrder) {
		this.isOrder = isOrder;
	}

	public RequiredCollectDetail getRequiredCollectDetail() {
		return requiredCollectDetail;
	}

	public void setRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) {
		this.requiredCollectDetail = requiredCollectDetail;
	}
	
}