package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidNegotiate
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidNegotiate implements java.io.Serializable {

	// 属性信息
	private Long bnId;     //主键
	private Long supplierId;     //供应商ID
	private Long rcId;     //项目ID
	private Date negotiateReturnDate; //磋商回应时间
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark;	 //备注
	private String remark1;	 //备注1
	private String remark2;	 //备注2
	private String responseNegotiate;	 //供方是否完成磋商
	private String status; //状态
	
	private String supplierName;
	
	
	public BidNegotiate() {
		super();
	}
	
	public Long getBnId(){
	   return  bnId;
	} 
	public void setBnId(Long bnId) {
	   this.bnId = bnId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }       
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public String getResponseNegotiate(){
	   return  responseNegotiate;
	} 
	public void setResponseNegotiate(String responseNegotiate) {
	   this.responseNegotiate = responseNegotiate;
    }
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Date getNegotiateReturnDate() {
		return negotiateReturnDate;
	}

	public void setNegotiateReturnDate(Date negotiateReturnDate) {
		this.negotiateReturnDate = negotiateReturnDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}