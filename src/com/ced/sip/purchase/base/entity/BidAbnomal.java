package com.ced.sip.purchase.base.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/**
 * BidAbnomal entity. @author MyEclipse Persistence Tools
 */

public class BidAbnomal extends BaseObject implements java.io.Serializable {

	// Fields

	private Long baId;
	private Long rcId;
	private String bidCode;
	private String buyRemark;
	private String buyWay;
	private String abnomalType;
	private String abnomalReason;
	private Date submitDate;
	private String status;
	private String statusCn;
	private Long writerId;
	private String writer;
	private Date writeDete;
	private Long deptId;

	//临时
	private String writerCn;
	private String abnomalTypeCn;
	private String buyWayCn;
	// Constructors

	/** default constructor */
	public BidAbnomal() {
	}

	/** minimal constructor */
	public BidAbnomal(Long baId) {
		this.baId = baId;
	}

	/** full constructor */
	public BidAbnomal(Long baId, String bidCode, String buyRemark,
			String buyWay, String abnomalType, String abnomalReason,
			Date submitDate, String status, String statusCn, Long writerId,
			String writer, Date writeDete) {
		this.baId = baId;
		this.bidCode = bidCode;
		this.buyRemark = buyRemark;
		this.buyWay = buyWay;
		this.abnomalType = abnomalType;
		this.abnomalReason = abnomalReason;
		this.submitDate = submitDate;
		this.status = status;
		this.statusCn = statusCn;
		this.writerId = writerId;
		this.writer = writer;
		this.writeDete = writeDete;
	}

	// Property accessors

	public Long getBaId() {
		return this.baId;
	}

	public void setBaId(Long baId) {
		this.baId = baId;
	}

	public String getBidCode() {
		return this.bidCode;
	}

	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}

	public String getBuyRemark() {
		return this.buyRemark;
	}

	public void setBuyRemark(String buyRemark) {
		this.buyRemark = buyRemark;
	}

	public String getBuyWay() {
		return buyWay;
	}

	public void setBuyWay(String buyWay) {
		this.buyWay = buyWay;
	}

	public String getAbnomalType() {
		return this.abnomalType;
	}

	public void setAbnomalType(String abnomalType) {
		this.abnomalType = abnomalType;
	}

	public String getAbnomalReason() {
		return this.abnomalReason;
	}

	public void setAbnomalReason(String abnomalReason) {
		this.abnomalReason = abnomalReason;
	}

	public Date getSubmitDate() {
		return this.submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCn() {
		return this.statusCn;
	}

	public void setStatusCn(String statusCn) {
		this.statusCn = statusCn;
	}

	public Long getWriterId() {
		return this.writerId;
	}

	public void setWriterId(Long writerId) {
		this.writerId = writerId;
	}

	public String getWriter() {
		return this.writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDete() {
		return this.writeDete;
	}

	public void setWriteDete(Date writeDete) {
		this.writeDete = writeDete;
	}

	public String getWriterCn() {
		return writerCn;
	}

	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}

	public String getAbnomalTypeCn() {
		return abnomalTypeCn;
	}

	public void setAbnomalTypeCn(String abnomalTypeCn) {
		this.abnomalTypeCn = abnomalTypeCn;
	}

	public String getBuyWayCn() {
		return buyWayCn;
	}

	public void setBuyWayCn(String buyWayCn) {
		this.buyWayCn = buyWayCn;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	
}