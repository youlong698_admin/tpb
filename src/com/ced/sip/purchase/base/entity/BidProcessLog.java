package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidProcessLog
 * 创建人：luguanglei 
 * 创建时间：2017-04-03
 */
public class BidProcessLog implements java.io.Serializable {

	// 属性信息
	private Long bplId;     //主键
	private Long rcId;     //项目ID
	private Long bidNode;     //项目节点
	private Date receiveDate;    //接收日期
	private Date completeDate;    //完成日期

	private String nodeName;
	private String url;
	private String img;
	private int day;
	
	public BidProcessLog( Long bidNode, String nodeName, String url,
			Date receiveDate, Date completeDate,Long bplId,int day,String img) {
		this.bplId = bplId;
		this.bidNode = bidNode;
		this.receiveDate = receiveDate;
		this.completeDate = completeDate;
		this.nodeName = nodeName;
		this.url = url;
		this.day=day;
		this.img = img;
	}
	
	public BidProcessLog() {
		super();
	}
	
	public Long getBplId(){
	   return  bplId;
	} 
	public void setBplId(Long bplId) {
	   this.bplId = bplId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }
	public Long getBidNode(){
	   return  bidNode;
	} 
	public void setBidNode(Long bidNode) {
	   this.bidNode = bidNode;
    }     
	public Date getReceiveDate(){
	   return  receiveDate;
	} 
	public void setReceiveDate(Date receiveDate) {
	   this.receiveDate = receiveDate;
    }	    
	public Date getCompleteDate(){
	   return  completeDate;
	} 
	public void setCompleteDate(Date completeDate) {
	   this.completeDate = completeDate;
    }

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
}