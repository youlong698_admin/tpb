package com.ced.sip.purchase.base.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/**
 * RequiredCollect entity. @author MyEclipse Persistence Tools
 */

public class RequiredCollect  extends BaseObject implements java.io.Serializable {

	// Fields

	private Long rcId;
	private String bidCode;
	private String buyRemark;
	private String buyWay;
	private String supplierType;
	private String writer;
	private Date writeDate;
	private Date auditDate;
	private String status;
	private Long deptId;//立项人部门id
	private Long floatCode;
	private Long purchaseDeptId;//采购组织
	private Double totalBudget;
	private String remark;
	private Long serviceStatus;
	private String serviceStatusCn;
	private String bidStatus;
	private String bidStatusCn;
	private String isSendNotice;	 //是否发布公告
	private Long comId;
	private String changeReason;//变更原因
	private String openStatus;//开标状态
    private Date noticePublishDate;
    private Date bidReturnDate;
    private Date bidOpenDate;
    private Date bidAwardDate;
    private Long changeNum;
    private Double bidWinningPrice;
	
	private String buyWayCn;
	private String supplierTypeCn;
	private String condition;
	private String writerCn;
	private String purchaseDeptName;
	private String deptName;
	
	private String compName;
	private String writeDateStart;
	private String writeDateEnd;
	
	

	// Constructors
	private String orderId;//流程实例标示
	private String processId;//流程定义标示
	private String processName;//流程定义名称
	private String orderState;//流程状态
	private String orderStateName;//流程状态名称
	private String instanceUrl;//实例启动页面
	
	
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getBidCode() {
		return bidCode;
	}
	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}
	public String getBuyRemark() {
		return buyRemark;
	}
	public void setBuyRemark(String buyRemark) {
		this.buyRemark = buyRemark;
	}
	public String getBuyWay() {
		return buyWay;
	}
	public void setBuyWay(String buyWay) {
		this.buyWay = buyWay;
	}
	public String getSupplierType() {
		return supplierType;
	}
	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public Date getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Long getFloatCode() {
		return floatCode;
	}
	public void setFloatCode(Long floatCode) {
		this.floatCode = floatCode;
	}
	public String getBuyWayCn() {
		return buyWayCn;
	}
	public void setBuyWayCn(String buyWayCn) {
		this.buyWayCn = buyWayCn;
	}
	public String getSupplierTypeCn() {
		return supplierTypeCn;
	}
	public void setSupplierTypeCn(String supplierTypeCn) {
		this.supplierTypeCn = supplierTypeCn;
	}
	public Long getPurchaseDeptId() {
		return purchaseDeptId;
	}
	public void setPurchaseDeptId(Long purchaseDeptId) {
		this.purchaseDeptId = purchaseDeptId;
	}
	public Double getTotalBudget() {
		return totalBudget;
	}
	public void setTotalBudget(Double totalBudget) {
		this.totalBudget = totalBudget;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(Long serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	public String getBidStatus() {
		return bidStatus;
	}
	public void setBidStatus(String bidStatus) {
		this.bidStatus = bidStatus;
	}
	public String getServiceStatusCn() {
		return serviceStatusCn;
	}
	public void setServiceStatusCn(String serviceStatusCn) {
		this.serviceStatusCn = serviceStatusCn;
	}
	public String getBidStatusCn() {
		return bidStatusCn;
	}
	public void setBidStatusCn(String bidStatusCn) {
		this.bidStatusCn = bidStatusCn;
	}
	public String getIsSendNotice() {
		return isSendNotice;
	}
	public void setIsSendNotice(String isSendNotice) {
		this.isSendNotice = isSendNotice;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getWriterCn() {
		return writerCn;
	}
	public void setWriterCn(String writerCn) {
		this.writerCn = writerCn;
	}
	public String getPurchaseDeptName() {
		return purchaseDeptName;
	}
	public void setPurchaseDeptName(String purchaseDeptName) {
		this.purchaseDeptName = purchaseDeptName;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getOrderState() {
		return orderState;
	}
	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	public String getOrderStateName() {
		return orderStateName;
	}
	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}
	public String getInstanceUrl() {
		return instanceUrl;
	}
	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	public String getChangeReason() {
		return changeReason;
	}
	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}
	public String getOpenStatus() {
		return openStatus;
	}
	public void setOpenStatus(String openStatus) {
		this.openStatus = openStatus;
	}
	public Date getNoticePublishDate() {
		return noticePublishDate;
	}
	public void setNoticePublishDate(Date noticePublishDate) {
		this.noticePublishDate = noticePublishDate;
	}
	public Date getBidReturnDate() {
		return bidReturnDate;
	}
	public void setBidReturnDate(Date bidReturnDate) {
		this.bidReturnDate = bidReturnDate;
	}
	public Date getBidOpenDate() {
		return bidOpenDate;
	}
	public void setBidOpenDate(Date bidOpenDate) {
		this.bidOpenDate = bidOpenDate;
	}
	public Date getBidAwardDate() {
		return bidAwardDate;
	}
	public void setBidAwardDate(Date bidAwardDate) {
		this.bidAwardDate = bidAwardDate;
	}
	public Long getChangeNum() {
		return changeNum;
	}
	public void setChangeNum(Long changeNum) {
		this.changeNum = changeNum;
	}
	public Double getBidWinningPrice() {
		return bidWinningPrice;
	}
	public void setBidWinningPrice(Double bidWinningPrice) {
		this.bidWinningPrice = bidWinningPrice;
	}
	public String getWriteDateStart() {
		return writeDateStart;
	}
	public void setWriteDateStart(String writeDateStart) {
		this.writeDateStart = writeDateStart;
	}
	public String getWriteDateEnd() {
		return writeDateEnd;
	}
	public void setWriteDateEnd(String writeDateEnd) {
		this.writeDateEnd = writeDateEnd;
	}		
}