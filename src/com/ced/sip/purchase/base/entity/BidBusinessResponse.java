package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidBusinessResponse
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidBusinessResponse implements java.io.Serializable {

	// 属性信息
	private Long bbrId;     //主键
	private Long rcId;     //项目ID
	private Long bpId;     //报价信息ID
	private Long supplierId;     //供应商ID
	private Long briId;     //商务响应项ID
	private String responseItemName;	 //响应项名称
	private String responseRequirements;	 //响应项要求
	private String myResponse;	 //我的响应
	private String writer;	 //编制人
	private Date writeDate;    //编制时间
	private Long brnId;     //商务磋商主键
	

	private BidResponseNegotiate bidResponseNegotiate;
	
	public BidBusinessResponse() {
		super();
	}
	
	public Long getBbrId(){
	   return  bbrId;
	} 
	public void setBbrId(Long bbrId) {
	   this.bbrId = bbrId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getBriId(){
	   return  briId;
	} 
	public void setBriId(Long briId) {
	   this.briId = briId;
    }     
	public String getResponseItemName(){
	   return  responseItemName;
	} 
	public void setResponseItemName(String responseItemName) {
	   this.responseItemName = responseItemName;
    }
	public String getResponseRequirements(){
	   return  responseRequirements;
	} 
	public void setResponseRequirements(String responseRequirements) {
	   this.responseRequirements = responseRequirements;
    }
	public String getMyResponse(){
	   return  myResponse;
	} 
	public void setMyResponse(String myResponse) {
	   this.myResponse = myResponse;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public Long getBrnId(){
	   return  brnId;
	} 
	public void setBrnId(Long brnId) {
	   this.brnId = brnId;
    }

	public Long getBpId() {
		return bpId;
	}

	public void setBpId(Long bpId) {
		this.bpId = bpId;
	}

	public BidResponseNegotiate getBidResponseNegotiate() {
		return bidResponseNegotiate;
	}

	public void setBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) {
		this.bidResponseNegotiate = bidResponseNegotiate;
	}     
}