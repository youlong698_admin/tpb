package com.ced.sip.purchase.base.entity;

/** 
 * 类名称：BidResponseNegotiate
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidResponseNegotiate implements java.io.Serializable {

	// 属性信息
	private Long brnId;     //主键
	private Long bnId;     //磋商信息ID
	private Long supplierId;     //供应商ID
	private Long briId;     //商务响应项ID
	private String responseItemName;	 //响应项名称
	private String responseRequirements;	 //响应项要求
	private String expectResponse;	 //期望响应
	private String remark;	 //备注
	
	
	public BidResponseNegotiate() {
		super();
	}
	
	public Long getBrnId(){
	   return  brnId;
	} 
	public void setBrnId(Long brnId) {
	   this.brnId = brnId;
    }     
	public Long getBnId(){
	   return  bnId;
	} 
	public void setBnId(Long bnId) {
	   this.bnId = bnId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getBriId(){
	   return  briId;
	} 
	public void setBriId(Long briId) {
	   this.briId = briId;
    }     
	public String getResponseItemName(){
	   return  responseItemName;
	} 
	public void setResponseItemName(String responseItemName) {
	   this.responseItemName = responseItemName;
    }
	public String getResponseRequirements(){
	   return  responseRequirements;
	} 
	public void setResponseRequirements(String responseRequirements) {
	   this.responseRequirements = responseRequirements;
    }
	public String getExpectResponse(){
	   return  expectResponse;
	} 
	public void setExpectResponse(String expectResponse) {
	   this.expectResponse = expectResponse;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
}