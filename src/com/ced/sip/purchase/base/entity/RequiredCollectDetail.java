package com.ced.sip.purchase.base.entity;

import java.util.Date;

/**
 * RequiredCollectDetail entity. @author MyEclipse Persistence Tools
 */

public class RequiredCollectDetail implements java.io.Serializable {

	// Fields

	private Long rcdId;
	private Long rcId;
	private String bidCode;
	private String buyCode;
	private String buyName;
	private String materialType;
	private String unit;
	private Double amount;
	private Double estimatePrice;
	private Double estimateSumPrice;
	private Double newPrice;
	private Date deliverDate;
	private String remark;
	private String remark1;
	private String remark2;
	private String remark3;
	private String remark4;
	private Long rmdId;
	private String isCollect;
	private Long comId;
	private String mnemonicCode;
	private Long materialId;     //采购对象ID
	
	private Double lowestPrice; //当前报价的最低价
	
	public Long getRcdId() {
		return rcdId;
	}
	public void setRcdId(Long rcdId) {
		this.rcdId = rcdId;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getBidCode() {
		return bidCode;
	}
	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}
	public String getBuyCode() {
		return buyCode;
	}
	public void setBuyCode(String buyCode) {
		this.buyCode = buyCode;
	}
	public String getBuyName() {
		return buyName;
	}
	public void setBuyName(String buyName) {
		this.buyName = buyName;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getDeliverDate() {
		return deliverDate;
	}
	public void setDeliverDate(Date deliverDate) {
		this.deliverDate = deliverDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getRemark3() {
		return remark3;
	}
	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	public String getRemark4() {
		return remark4;
	}
	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}
	public Long getRmdId() {
		return rmdId;
	}
	public void setRmdId(Long rmdId) {
		this.rmdId = rmdId;
	}
	public String getIsCollect() {
		return isCollect;
	}
	public void setIsCollect(String isCollect) {
		this.isCollect = isCollect;
	}
	public Double getLowestPrice() {
		return lowestPrice;
	}
	public void setLowestPrice(Double lowestPrice) {
		this.lowestPrice = lowestPrice;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public String getMnemonicCode() {
		return mnemonicCode;
	}
	public void setMnemonicCode(String mnemonicCode) {
		this.mnemonicCode = mnemonicCode;
	}
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	public Double getEstimatePrice() {
		return estimatePrice;
	}
	public void setEstimatePrice(Double estimatePrice) {
		this.estimatePrice = estimatePrice;
	}
	public Double getEstimateSumPrice() {
		return estimateSumPrice;
	}
	public void setEstimateSumPrice(Double estimateSumPrice) {
		this.estimateSumPrice = estimateSumPrice;
	}
	public Double getNewPrice() {
		return newPrice;
	}
	public void setNewPrice(Double newPrice) {
		this.newPrice = newPrice;
	}
	
}