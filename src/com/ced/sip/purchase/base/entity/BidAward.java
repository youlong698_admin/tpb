package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidAward
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidAward implements java.io.Serializable {

	// 属性信息
	private Long baId;     //主键
	private Long rcId;     //项目ID
	private Long supplierId;     //供应商ID
	private Double bidPrice;	//授标总价
	private Double taxRate;	//税率
	private String writer;	 //编制人
	private Date writeDate;    //编制时间
	private String remark;	 //备注
	private String remark1;	 //备注1
	private String remark2;	 //备注2
	private String buyWay;
	private Long deptId;//立项人部门id
	private Long comId;
	private int isContract;//是否生成合同
	private int isOrder;//是否生成订单
	private String status;//状态
	
	private String supplierName;
	private String sumPrice;
	
	public BidAward() {
		super();
	}
	
	public Long getBaId(){
	   return  baId;
	} 
	public void setBaId(Long baId) {
	   this.baId = baId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Double getBidPrice(){
	   return  bidPrice;
	} 
	public Double getTaxRate(){
		   return  taxRate;
	} 
	public void setTaxRate(Double taxRate) {
	   this.taxRate = taxRate;
    }
	public void setBidPrice(Double bidPrice) {
	   this.bidPrice = bidPrice;
    }	
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getBuyWay() {
		return buyWay;
	}

	public void setBuyWay(String buyWay) {
		this.buyWay = buyWay;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	public int getIsContract() {
		return isContract;
	}

	public void setIsContract(int isContract) {
		this.isContract = isContract;
	}

	public int getIsOrder() {
		return isOrder;
	}

	public void setIsOrder(int isOrder) {
		this.isOrder = isOrder;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}