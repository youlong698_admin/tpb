package com.ced.sip.purchase.base.entity;

import java.util.Date;

/** 
 * 类名称：BidClarifyResponse
 * 创建人：luguanglei 
 * 创建时间：2017-04-08
 */
public class BidClarifyResponse implements java.io.Serializable {

	// 属性信息
	private Long bcrId;     //主键
	private Long bcId;     //标前澄清ID
	private Long supplierId;     //供应商ID
	private String supplierName;	 //供应商名称
	private String viewr;	 //查看人
	private Date viewDate;    //查看时间
	private String remark;	 //备注
	
	
	public BidClarifyResponse() {
		super();
	}
	
	public Long getBcrId(){
	   return  bcrId;
	} 
	public void setBcrId(Long bcrId) {
	   this.bcrId = bcrId;
    }     
	public Long getBcId(){
	   return  bcId;
	} 
	public void setBcId(Long bcId) {
	   this.bcId = bcId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getViewr(){
	   return  viewr;
	} 
	public void setViewr(String viewr) {
	   this.viewr = viewr;
    }
	public Date getViewDate(){
	   return  viewDate;
	} 
	public void setViewDate(Date viewDate) {
	   this.viewDate = viewDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
}