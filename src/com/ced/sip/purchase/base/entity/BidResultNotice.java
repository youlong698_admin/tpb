package com.ced.sip.purchase.base.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：BidResultNotice
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidResultNotice extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long brnId;     //主键
	private Long supplierId;     //供应商主键
	private String supplierName;	 //供应商名称
	private Double bidPrice;	//中标金额
	private String publisher;	 //发布人
	private String contactPhone;	 //联系电话
	private String contactEmail;	 //邮箱
	private String contactAddress;	 //地址
	private Long rcId;     //项目ID
	private Date publishDate;    //发布日期
	private String remark;	 //备注
	private String contactPerson;	 //联系人
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	
	private String bidCode;
	private String buyRemark;
	private String buyWayCn;
	
	
	public BidResultNotice() {
		super();
	}
	
	public Long getBrnId(){
	   return  brnId;
	} 
	public void setBrnId(Long brnId) {
	   this.brnId = brnId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public String getSupplierName(){
	   return  supplierName;
	} 
	public void setSupplierName(String supplierName) {
	   this.supplierName = supplierName;
    }
	public String getPublisher(){
	   return  publisher;
	} 
	public void setPublisher(String publisher) {
	   this.publisher = publisher;
    }
	public String getContactPhone(){
	   return  contactPhone;
	} 
	public void setContactPhone(String contactPhone) {
	   this.contactPhone = contactPhone;
    }
	public String getContactEmail(){
	   return  contactEmail;
	} 
	public void setContactEmail(String contactEmail) {
	   this.contactEmail = contactEmail;
    }
	public String getContactAddress(){
	   return  contactAddress;
	} 
	public void setContactAddress(String contactAddress) {
	   this.contactAddress = contactAddress;
    }
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Date getPublishDate(){
	   return  publishDate;
	} 
	public void setPublishDate(Date publishDate) {
	   this.publishDate = publishDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getContactPerson(){
	   return  contactPerson;
	} 
	public void setContactPerson(String contactPerson) {
	   this.contactPerson = contactPerson;
    }

	public Double getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(Double bidPrice) {
		this.bidPrice = bidPrice;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getBidCode() {
		return bidCode;
	}

	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}

	public String getBuyRemark() {
		return buyRemark;
	}

	public void setBuyRemark(String buyRemark) {
		this.buyRemark = buyRemark;
	}

	public String getBuyWayCn() {
		return buyWayCn;
	}

	public void setBuyWayCn(String buyWayCn) {
		this.buyWayCn = buyWayCn;
	}
	
}