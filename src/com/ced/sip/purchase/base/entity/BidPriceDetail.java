package com.ced.sip.purchase.base.entity;

/** 
 * 类名称：BidPriceDetail
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceDetail implements java.io.Serializable {

	// 属性信息
	private Long bpdId;     //主键
	private Long supplierId;     //供应商ID
	private Long bpId;     //供应商报价ID
	private Long rcdId;     //采购计划明细ID
	private String encryPrice;	 //加密报价
	private Double price;	//报价
	private Long bpnId;     //报价磋商主键
	private Long rcId;     //项目ID
	
	private RequiredCollectDetail requiredCollectDetail;
	private BidPriceNegotiate bidPriceNegotiate;
	
	
	public BidPriceDetail() {
		super();
	}
	
	public Long getBpdId(){
	   return  bpdId;
	} 
	public void setBpdId(Long bpdId) {
	   this.bpdId = bpdId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getBpId(){
	   return  bpId;
	} 
	public void setBpId(Long bpId) {
	   this.bpId = bpId;
    }     
	public Long getRcdId(){
	   return  rcdId;
	} 
	public void setRcdId(Long rcdId) {
	   this.rcdId = rcdId;
    }     
	public String getEncryPrice(){
	   return  encryPrice;
	} 
	public void setEncryPrice(String encryPrice) {
	   this.encryPrice = encryPrice;
    }
	public Double getPrice(){
	   return  price;
	} 
	public void setPrice(Double price) {
	   this.price = price;
    }	
	public Long getBpnId(){
	   return  bpnId;
	} 
	public void setBpnId(Long bpnId) {
	   this.bpnId = bpnId;
    }

	public RequiredCollectDetail getRequiredCollectDetail() {
		return requiredCollectDetail;
	}

	public void setRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) {
		this.requiredCollectDetail = requiredCollectDetail;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public BidPriceNegotiate getBidPriceNegotiate() {
		return bidPriceNegotiate;
	}

	public void setBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) {
		this.bidPriceNegotiate = bidPriceNegotiate;
	}     
}