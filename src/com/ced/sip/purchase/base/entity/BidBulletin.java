package com.ced.sip.purchase.base.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/** 
 * 类名称：BidBulletin
 * 创建人：luguanglei 
 * 创建时间：2017-04-05
 */
public class BidBulletin extends BaseObject implements java.io.Serializable {

	// 属性信息
	private Long bbId;     //主键
	private Long rcId;     //项目ID
	private String bidCode;	 //项目编号
	private String bulletinTitle;	 //公告标题
	private String bulletinContent;	 //公告内容
	private String buyWay;         //采购方式
	private String contacts;	 //联系人
	private String contactTelephone;	 //联系电话
	private String publisher;	 //发布人
	private Date publishDate;    //发布日期
	private String remark;	 //备注
	private String status;	 //状态
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private Date returnDate;    //截止日期
	private String sysCompany; //采购单位
	private Long comId;
	

	private String publisherCn;	 //发布人
	private String statusCn;	 //状态
	
	public BidBulletin() {
		super();
	}
	
	public Long getBbId(){
	   return  bbId;
	} 
	public void setBbId(Long bbId) {
	   this.bbId = bbId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getBidCode(){
	   return  bidCode;
	} 
	public void setBidCode(String bidCode) {
	   this.bidCode = bidCode;
    }
	public String getBulletinTitle(){
	   return  bulletinTitle;
	} 
	public void setBulletinTitle(String bulletinTitle) {
	   this.bulletinTitle = bulletinTitle;
    }
	public String getBulletinContent(){
	   return  bulletinContent;
	} 
	public void setBulletinContent(String bulletinContent) {
	   this.bulletinContent = bulletinContent;
    }
	public String getContacts(){
	   return  contacts;
	} 
	public void setContacts(String contacts) {
	   this.contacts = contacts;
    }
	public String getContactTelephone(){
	   return  contactTelephone;
	} 
	public void setContactTelephone(String contactTelephone) {
	   this.contactTelephone = contactTelephone;
    }
	public String getPublisher(){
	   return  publisher;
	} 
	public void setPublisher(String publisher) {
	   this.publisher = publisher;
    }
	public Date getPublishDate(){
	   return  publishDate;
	} 
	public void setPublishDate(Date publishDate) {
	   this.publishDate = publishDate;
    }	    
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }
	public String getStatus(){
	   return  status;
	} 
	public void setStatus(String status) {
	   this.status = status;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }

	public String getPublisherCn() {
		return publisherCn;
	}

	public void setPublisherCn(String publisherCn) {
		this.publisherCn = publisherCn;
	}

	public String getStatusCn() {
		return statusCn;
	}

	public void setStatusCn(String statusCn) {
		this.statusCn = statusCn;
	}

	public String getBuyWay() {
		return buyWay;
	}

	public void setBuyWay(String buyWay) {
		this.buyWay = buyWay;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getSysCompany() {
		return sysCompany;
	}

	public void setSysCompany(String sysCompany) {
		this.sysCompany = sysCompany;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}
	
}