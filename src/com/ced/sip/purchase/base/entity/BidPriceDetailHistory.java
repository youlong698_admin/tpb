package com.ced.sip.purchase.base.entity;

/** 
 * 类名称：BidPriceDetailHistory
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceDetailHistory implements java.io.Serializable {

	// 属性信息
	private Long bpdhId;     //主键
	private Long supplierId;     //供应商ID
	private Long bphId;     //供应商报价历史ID
	private Long rcdId;     //采购计划明细ID
	private String encryPrice;	 //加密报价
	private Double price;	//报价
	private Long bpnId;     //报价磋商主键
	private Long rcId;     //项目ID

	private RequiredCollectDetail requiredCollectDetail;
	
	public BidPriceDetailHistory() {
		super();
	}
	
	public Long getBpdhId(){
	   return  bpdhId;
	} 
	public void setBpdhId(Long bpdhId) {
	   this.bpdhId = bpdhId;
    }     
	public Long getSupplierId(){
	   return  supplierId;
	} 
	public void setSupplierId(Long supplierId) {
	   this.supplierId = supplierId;
    }     
	public Long getBphId(){
	   return  bphId;
	} 
	public void setBphId(Long bphId) {
	   this.bphId = bphId;
    }     
	public Long getRcdId(){
	   return  rcdId;
	} 
	public void setRcdId(Long rcdId) {
	   this.rcdId = rcdId;
    }     
	public String getEncryPrice(){
	   return  encryPrice;
	} 
	public void setEncryPrice(String encryPrice) {
	   this.encryPrice = encryPrice;
    }
	public Double getPrice(){
	   return  price;
	} 
	public void setPrice(Double price) {
	   this.price = price;
    }	
	public Long getBpnId(){
	   return  bpnId;
	} 
	public void setBpnId(Long bpnId) {
	   this.bpnId = bpnId;
    }

	public RequiredCollectDetail getRequiredCollectDetail() {
		return requiredCollectDetail;
	}

	public void setRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) {
		this.requiredCollectDetail = requiredCollectDetail;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}     
}