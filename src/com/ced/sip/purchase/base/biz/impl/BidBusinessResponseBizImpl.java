package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
/** 
 * 类名称：BidBusinessResponseBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidBusinessResponseBizImpl extends BaseBizImpl implements IBidBusinessResponseBiz  {
	
	/**
	 * 根据主键获得供应商商务响应表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidBusinessResponse getBidBusinessResponse(Long id) throws BaseException {
		return (BidBusinessResponse)this.getObject(BidBusinessResponse.class, id);
	}
	
	/**
	 * 获得供应商商务响应表实例
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidBusinessResponse getBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException {
		return (BidBusinessResponse)this.getObject(BidBusinessResponse.class, bidBusinessResponse.getBbrId() );
	}
	
	/**
	 * 添加供应商商务响应信息
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException{
		this.saveObject( bidBusinessResponse ) ;
	}
	
	/**
	 * 更新供应商商务响应表实例
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException{
		this.updateObject( bidBusinessResponse ) ;
	}
	
	/**
	 * 删除供应商商务响应表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidBusinessResponse(String id) throws BaseException {
		this.removeObject( this.getBidBusinessResponse( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商商务响应表实例
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException {
		this.removeObject( bidBusinessResponse ) ;
	}
	
	/**
	 * 删除供应商商务响应表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidBusinessResponses(String[] id) throws BaseException {
		this.removeBatchObject(BidBusinessResponse.class, id) ;
	}
	
	/**
	 * 获得所有供应商商务响应表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBusinessResponseList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBusinessResponse de where 1 = 1 " );

		hql.append(" order by de.bbrId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商商务响应表数据集
	 * @param bidBusinessResponse 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBusinessResponseList(BidBusinessResponse bidBusinessResponse) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBusinessResponse de where 1 = 1 " );
		if(bidBusinessResponse != null){
			if(StringUtil.isNotBlank(bidBusinessResponse.getBpId())){
				hql.append(" and de.bpId =").append(bidBusinessResponse.getBpId()).append("");
			}
		}
		hql.append(" order by de.bbrId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商商务响应表数据集
	 * @param rollPage 分页对象
	 * @param bidBusinessResponse 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBusinessResponseList(RollPage rollPage, BidBusinessResponse bidBusinessResponse) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBusinessResponse de where 1 = 1 " );

		hql.append(" order by de.bbrId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获取供应商商务响应比对数据集
	 * @param bpIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	public List getBidBusinessResponseForParityPrice(Long[] bpIdStr,Long rcId) throws BaseException{
		int columnCount=2+bpIdStr.length;
		StringBuffer hql = new StringBuffer(" select bri.response_item_name,bri.response_requirements" );
		for(Long bpId:bpIdStr){
			hql.append(",bbr"+bpId+".my_response myResponse"+bpId);
		}
		hql.append("  from business_response_item bri ");
		for(Long bpId:bpIdStr){
			hql.append(" left join (select bri_id,my_response from bid_business_response where bp_id="+bpId+" ) bbr"+bpId+" on bri.bri_id=bbr"+bpId+".bri_id");
		}
		hql.append("  where bri.rc_id="+rcId);
		return this.getObjects(hql.toString(), columnCount);
	}
}
