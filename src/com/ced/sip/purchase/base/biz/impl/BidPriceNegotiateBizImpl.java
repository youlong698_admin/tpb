package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidPriceNegotiateBiz;
import com.ced.sip.purchase.base.entity.BidPriceNegotiate;
/** 
 * 类名称：BidPriceNegotiateBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceNegotiateBizImpl extends BaseBizImpl implements IBidPriceNegotiateBiz  {
	
	/**
	 * 根据主键获得价格磋商表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceNegotiate getBidPriceNegotiate(Long id) throws BaseException {
		return (BidPriceNegotiate)this.getObject(BidPriceNegotiate.class, id);
	}
	
	/**
	 * 获得价格磋商表实例
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceNegotiate getBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException {
		return (BidPriceNegotiate)this.getObject(BidPriceNegotiate.class, bidPriceNegotiate.getBpnId() );
	}
	
	/**
	 * 添加价格磋商信息
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException{
		this.saveObject( bidPriceNegotiate ) ;
	}
	
	/**
	 * 更新价格磋商表实例
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException{
		this.updateObject( bidPriceNegotiate ) ;
	}
	
	/**
	 * 删除价格磋商表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceNegotiate(String id) throws BaseException {
		this.removeObject( this.getBidPriceNegotiate( new Long(id) ) ) ;
	}
	
	/**
	 * 删除价格磋商表实例
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException {
		this.removeObject( bidPriceNegotiate ) ;
	}
	
	/**
	 * 删除价格磋商表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceNegotiates(String[] id) throws BaseException {
		this.removeBatchObject(BidPriceNegotiate.class, id) ;
	}
	
	/**
	 * 获得所有价格磋商表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceNegotiateList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceNegotiate de where 1 = 1 " );

		hql.append(" order by de.bpnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有价格磋商表数据集
	 * @param bidPriceNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceNegotiateList(BidPriceNegotiate bidPriceNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceNegotiate de where 1 = 1 " );
		if(bidPriceNegotiate != null){
			if(StringUtil.isNotBlank(bidPriceNegotiate.getBnId())){
				hql.append(" and de.bnId =").append(bidPriceNegotiate.getBnId()).append("");
			}
		}
		hql.append(" order by de.bpnId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有价格磋商表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceNegotiateList(RollPage rollPage, BidPriceNegotiate bidPriceNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceNegotiate de where 1 = 1 " );
		if(bidPriceNegotiate != null){
			if(StringUtil.isNotBlank(bidPriceNegotiate.getBnId())){
				hql.append(" and de.bnId =").append(bidPriceNegotiate.getBnId()).append("");
			}
		}
		hql.append(" order by de.bpnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
