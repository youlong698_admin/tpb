package com.ced.sip.purchase.base.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidBulletinBiz;
import com.ced.sip.purchase.base.entity.BidBulletin;
/** 
 * 类名称：BidBulletinBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-05
 */
public class BidBulletinBizImpl extends BaseBizImpl implements IBidBulletinBiz  {
	
	/**
	 * 根据主键获得公告表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-05
	 * @return
	 * @throws BaseException 
	 */
	public BidBulletin getBidBulletin(Long id) throws BaseException {
		return (BidBulletin)this.getObject(BidBulletin.class, id);
	}
	
	/**
	 * 获得公告表表实例
	 * @param bidBulletin 公告表表实例
	 * @author luguanglei 2017-04-05
	 * @return
	 * @throws BaseException 
	 */
	public BidBulletin getBidBulletin(BidBulletin bidBulletin) throws BaseException {
		return (BidBulletin)this.getObject(BidBulletin.class, bidBulletin.getBbId() );
	}
	
	/**
	 * 添加公告表信息
	 * @param bidBulletin 公告表表实例
	 * @author luguanglei 2017-04-05
	 * @throws BaseException 
	 */
	public void saveBidBulletin(BidBulletin bidBulletin) throws BaseException{
		this.saveObject( bidBulletin ) ;
	}
	
	/**
	 * 更新公告表表实例
	 * @param bidBulletin 公告表表实例
	 * @author luguanglei 2017-04-05
	 * @throws BaseException 
	 */
	public void updateBidBulletin(BidBulletin bidBulletin) throws BaseException{
		this.updateObject( bidBulletin ) ;
	}
	
	/**
	 * 删除公告表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-05
	 * @throws BaseException 
	 */
	public void deleteBidBulletin(String id) throws BaseException {
		this.removeObject( this.getBidBulletin( new Long(id) ) ) ;
	}
	
	/**
	 * 删除公告表表实例
	 * @param bidBulletin 公告表表实例
	 * @author luguanglei 2017-04-05
	 * @throws BaseException 
	 */
	public void deleteBidBulletin(BidBulletin bidBulletin) throws BaseException {
		this.removeObject( bidBulletin ) ;
	}
	
	/**
	 * 删除公告表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-05
	 * @throws BaseException 
	 */
	public void deleteBidBulletins(String[] id) throws BaseException {
		this.removeBatchObject(BidBulletin.class, id) ;
	}
	
	/**
	 * 获得所有公告表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-05
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBulletinList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBulletin de where 1 = 1 " );
		
		hql.append(" order by de.bbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有公告表表数据集
	 * @param bidBulletin 查询参数对象
	 * @author luguanglei 2017-04-05
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBulletinList(BidBulletin bidBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBulletin de where 1 = 1 " );
		if(bidBulletin != null){
			if(StringUtil.isNotBlank(bidBulletin.getRcId())){
				hql.append(" and de.rcId =").append(bidBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidBulletin.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidBulletin.getBuyWay()).append("'");
			}
		}
		hql.append(" order by de.bbId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有公告表表数据集
	 * @param rollPage 分页对象
	 * @param bidBulletin 查询参数对象
	 * @author luguanglei 2017-04-05
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBulletinList(RollPage rollPage, BidBulletin bidBulletin) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBulletin de where 1 = 1 and de.status='0'" );
		if(bidBulletin != null){
			if(StringUtil.isNotBlank(bidBulletin.getRcId())){
				hql.append(" and de.rcId =").append(bidBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidBulletin.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidBulletin.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(bidBulletin.getStatus())){
				hql.append(" and de.status ='").append(bidBulletin.getStatus()).append("'");
			}
			if(StringUtil.isNotBlank(bidBulletin.getComId())){
				hql.append(" and de.comId =").append(bidBulletin.getComId()).append("");
			}
		}
		hql.append(" order by de.bbId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有公告表表数据集
	 * @param rollPage 分页对象   type3触屏版首页
	 * @param bidBulletin 查询参数对象
	 * @author luguanglei 2017-04-05
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBulletinList(RollPage rollPage, BidBulletin bidBulletin,int type) throws BaseException {
		StringBuffer hql = new StringBuffer("select de from BidBulletin de where  de.status='0'" );
		if(bidBulletin != null){
			if(StringUtil.isNotBlank(bidBulletin.getRcId())){
				hql.append(" and de.rcId =").append(bidBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidBulletin.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidBulletin.getBuyWay()).append("'");
			}
		}
		if(type==1){
			hql.append(" and de.buyWay ='00'");
		}else if(type==2){
			hql.append(" and de.buyWay !='00'");
		}else if(type==3){
			hql.append(" and de.returnDate>sysdate ");
		}
		hql.append(" order by de.bbId desc ");
		return this.getObjects(rollPage,hql.toString() );
	}

	/**
	 * 获得所有公告表表总数数据集
	 * @param rollPage 分页对象
	 * @param bidBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public int countBidBulletinList(BidBulletin bidBulletin, int type)
			throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.bbId) from BidBulletin de where 1 = 1 and de.status='0'" );
		if(bidBulletin != null){
			if(StringUtil.isNotBlank(bidBulletin.getRcId())){
				hql.append(" and de.rcId =").append(bidBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidBulletin.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidBulletin.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(bidBulletin.getStatus())){
				hql.append(" and de.status ='").append(bidBulletin.getStatus()).append("'");
			}
			if(StringUtil.isNotBlank(bidBulletin.getComId())){
				hql.append(" and de.comId =").append(bidBulletin.getComId()).append("");
			}
		}
		if(type==1){
			hql.append(" and de.buyWay ='00'");
		}else if(type==2){
			hql.append(" and (de.buyWay ='01' or de.buyWay ='02')");
		}else if(type==3){
			hql.append(" and de.returnDate>sysdate ");
		}
		return this.countObjects(hql.toString());
	}

	/**
	 * 获得所有公告表表数据集   触屏版项目信息搜索用
	 * @param bidBulletin 查询参数对象  type3触屏版首页
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBulletinListByWebApp(RollPage rollPage,
			BidBulletin bidBulletin, Map<String, Object> map)
			throws BaseException {
		StringBuffer hql = new StringBuffer("select de from BidBulletin de where  de.status='0'" );
		if(bidBulletin != null){
			if(StringUtil.isNotBlank(bidBulletin.getRcId())){
				hql.append(" and de.rcId =").append(bidBulletin.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidBulletin.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidBulletin.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(map.get("startDate"))){
				hql.append(" and de.publishDate >= to_date('").append(map.get("startDate")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(map.get("endDate"))){
				hql.append(" and de.publishDate <= to_date('").append(map.get("endDate")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(bidBulletin.getComId())){
				hql.append(" and de.comId =").append(bidBulletin.getComId()).append("");
			}
			if(StringUtil.isNotBlank(bidBulletin.getBulletinTitle())){
				hql.append(" and de.bulletinTitle  like '%").append(bidBulletin.getComId()).append("%'");
			}
		}
		int bidderType=(Integer)map.get("bidderType");
		if(bidderType==1){
			hql.append(" and de.buyWay = '00'");
		}else if(bidderType==2){
			hql.append(" and de.buyWay = '01'");
		}else if(bidderType==3){
			hql.append(" and de.buyWay = '02'");
		}else if(bidderType==4){
			hql.append(" and de.returnDate>sysdate");
		}//4为允许报名的
		hql.append(" order by de.bbId desc ");
		return this.getObjects(rollPage,hql.toString() );
	}
	
}
