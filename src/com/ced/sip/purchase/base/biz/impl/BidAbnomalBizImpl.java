package com.ced.sip.purchase.base.biz.impl;


import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidAbnomalBiz;
import com.ced.sip.purchase.base.entity.BidAbnomal;

public class BidAbnomalBizImpl  extends BaseBizImpl implements IBidAbnomalBiz {

	/**
	 * 根据主键获得采购异常表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public BidAbnomal getBidAbnomal(Long id) throws BaseException {
		return (BidAbnomal)this.getObject(BidAbnomal.class, id);
	}
	
	/**
	 * 获得采购异常表实例
	 * @param bidMonitor 采购异常表实例
	 * @return
	 * @throws BaseException 
	 */
	public BidAbnomal getBidAbnomal( BidAbnomal bidAbnomal ) throws BaseException {
		return (BidAbnomal)this.getObject(BidAbnomal.class, bidAbnomal.getBaId() );
	}
	
	/**
	 * 添加采购异常表信息
	 * @param BidAbnomal 采购异常表实例
	 * @throws BaseException 
	 */
	public void saveBidAbnomal(BidAbnomal bidAbnomal) throws BaseException{
		this.saveObject( bidAbnomal ) ;
	}
	
	/**
	 * 更新采购异常表实例
	 * @param BidAbnomal 采购异常表实例
	 * @throws BaseException 
	 */
	public void updateBidAbnomal(BidAbnomal bidAbnomal) throws BaseException{
		this.updateObject( bidAbnomal ) ;
	}
	
	/**
	 * 删除采购异常表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteBidAbnomal(String id) throws BaseException {
		this.removeObject( this.getBidAbnomal( new Long(id) ) ) ;
	}
	
	/**
	 * 删除采购异常表实例
	 * @param BidAbnomal 采购异常表实例
	 * @throws BaseException 
	 */
	public void deleteBidAbnomal(BidAbnomal bidAbnomal) throws BaseException {
		this.removeObject( bidAbnomal ) ;
	}
	
	/**
	 * 删除采购异常表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteBidAbnomals(String[] id) throws BaseException {
		this.removeBatchObject(BidAbnomal.class, id) ;
	}
	
	
	/**
	 * 获得所有采购异常表数据集
	 * @param BidAbnomal 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List<BidAbnomal> getBidAbnomalWithTreeList(  BidAbnomal bidAbnomal ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAbnomal de where 1 = 1 " );
		hql.append(" order by baId desc");
		return this.getObjects( hql.toString() );
	}
	
	
	/**
	 * 获得所有采购异常表数据集
	 * @param BidAbnomal 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAbnomalList(  BidAbnomal bidAbnomal ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAbnomal de where 1 = 1 " );
		if(bidAbnomal != null){
			if(StringUtil.isNotBlank(bidAbnomal.getBidCode())){
				hql.append(" and de.bidCode ='").append(bidAbnomal.getBidCode()).append("'");
			}
		}
		hql.append(" order by de.baId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有采购异常表数据集
	 * @param rollPage 分页对象
	 * @param BidAbnomal 查询参数对象
	 * @param sql 权限sql
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAbnomalList( RollPage rollPage, BidAbnomal bidAbnomal) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAbnomal de where 1 = 1 " );
		
		if(bidAbnomal != null){
			if(StringUtil.isNotBlank(bidAbnomal.getBaId())){
				hql.append(" and de.baId ='").append(bidAbnomal.getBaId()).append("'");
			}
			
			
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	
	}
	/**
	 * 获得所有异常采购采购项目信息
	 * @param rollPage 分页对象
	 * @param map 查询参数对象
	 * @author lgl 2017-10-20
	 * @return
	 * @throws BaseException 
	 */
	public List getAbnomalBidList(RollPage rollPage,
			Map<String,Object> map,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAbnomal de where 1 = 1 ");
		if(StringUtil.isNotBlank(map)){
			
			if(StringUtil.isNotBlank(map.get("bidCode"))){
				hql.append(" and de.bidCode like  '%").append(map.get("bidCode")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("buyRemark"))){
				hql.append(" and de.buyRemark like '%").append(map.get("buyRemark")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("bidAbnomal"))){
				hql.append(" and de.abnomalType = '").append(map.get("bidAbnomal")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("buyWay"))){
				hql.append(" and de.buyWay = '").append(map.get("buyWay")).append("' ");
			}
			if(StringUtil.isNotBlank(map.get("writeDateStart"))){
				hql.append(" and de.submitDate>=do_date('").append(map.get("writeDateStart")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(map.get("writeDateEnd"))){
				hql.append(" and  de.submitDate>=do_date('").append(map.get("writeDateEnd")).append("','yyyy-MM-dd')");
			}
		}
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		
		return this.getObjects(rollPage,hql.toString()); 
	}
	/**
	 * 获得所有异常采购采购项目信息  导出EXCEL
	 * @param map 查询参数对象
	 * @author lgl 2017-10-20
	 * @return
	 * @throws BaseException 
	 */
	public List getAbnomalBidListAll(Map<String,Object> map,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer("from BidAbnomal de where 1 = 1 ");
		if(StringUtil.isNotBlank(map)){
			
			if(StringUtil.isNotBlank(map.get("bidCode"))){
				hql.append(" and de.bidCode like  '%").append(map.get("bidCode")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("buyRemark"))){
				hql.append(" and de.buyRemark like '%").append(map.get("buyRemark")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("bidAbnomal"))){
				hql.append(" and de.abnomalType = '").append(map.get("bidAbnomal")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("buyWay"))){
				hql.append(" and de.buyWay = '").append(map.get("buyWay")).append("' ");
			}
			if(StringUtil.isNotBlank(map.get("writeDateStart"))){
				hql.append(" and de.submitDate>=do_date('").append(map.get("writeDateStart")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(map.get("writeDateEnd"))){
				hql.append(" and  de.submitDate>=do_date('").append(map.get("writeDateEnd")).append("','yyyy-MM-dd')");
			}
		}
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		
		return this.getObjects(hql.toString()); 
	}
	/**
	 * 获得异常采购项目量
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  int countBidAbnomal(Map<String,Object> mapParams,String sqlStr) throws BaseException {		
		StringBuffer hql = new StringBuffer("select count(de.baId) from BidAbnomal de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDete,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDete,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		return this.countObjects(hql.toString() );
	}
}
