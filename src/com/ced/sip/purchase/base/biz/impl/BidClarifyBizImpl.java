package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidClarifyBiz;
import com.ced.sip.purchase.base.entity.BidClarify;
import com.ced.sip.purchase.base.entity.BidClarifyResponse;
/** 
 * 类名称：BidClarifyBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidClarifyBizImpl extends BaseBizImpl implements IBidClarifyBiz  {
	
	/**
	 * 根据主键获得标前澄清表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public BidClarify getBidClarify(Long id) throws BaseException {
		return (BidClarify)this.getObject(BidClarify.class, id);
	}
	
	/**
	 * 获得标前澄清表实例
	 * @param bidClarify 标前澄清表实例
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public BidClarify getBidClarify(BidClarify bidClarify) throws BaseException {
		return (BidClarify)this.getObject(BidClarify.class, bidClarify.getBcId() );
	}
	
	/**
	 * 添加标前澄清信息
	 * @param bidClarify 标前澄清表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void saveBidClarify(BidClarify bidClarify) throws BaseException{
		this.saveObject( bidClarify ) ;
	}
	
	/**
	 * 更新标前澄清表实例
	 * @param bidClarify 标前澄清表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void updateBidClarify(BidClarify bidClarify) throws BaseException{
		this.updateObject( bidClarify ) ;
	}
	
	/**
	 * 删除标前澄清表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteBidClarify(String id) throws BaseException {
		this.removeObject( this.getBidClarify( new Long(id) ) ) ;
	}
	
	/**
	 * 删除标前澄清表实例
	 * @param bidClarify 标前澄清表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteBidClarify(BidClarify bidClarify) throws BaseException {
		this.removeObject( bidClarify ) ;
	}
	
	/**
	 * 删除标前澄清表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteBidClarifys(String[] id) throws BaseException {
		this.removeBatchObject(BidClarify.class, id) ;
	}
	
	/**
	 * 获得所有标前澄清表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getBidClarifyList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidClarify de where 1 = 1 " );

		hql.append(" order by de.bcId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有标前澄清表数据集
	 * @param bidClarify 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getBidClarifyList(BidClarify bidClarify) throws BaseException {
		StringBuffer hql = new StringBuffer("from BidClarify de where 1 = 1 " );
		if(bidClarify != null){
			if(StringUtil.isNotBlank(bidClarify.getRcId())){
				hql.append(" and de.rcId =").append(bidClarify.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidClarify.getStatus())){
				hql.append(" and de.status ='").append(bidClarify.getStatus()).append("'");
			}
		}
		hql.append(" order by de.bcId ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有标前澄清表数据集
	 * @param rollPage 分页对象
	 * @param bidClarify 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getBidClarifyList(RollPage rollPage, BidClarify bidClarify) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidClarify de where 1 = 1 " );
		if(bidClarify != null){
			if(StringUtil.isNotBlank(bidClarify.getRcId())){
				hql.append(" and de.rcId =").append(bidClarify.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidClarify.getStatus())){
				hql.append(" and de.status ='").append(bidClarify.getStatus()).append("'");
			}
		}
		hql.append(" order by de.bcId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
