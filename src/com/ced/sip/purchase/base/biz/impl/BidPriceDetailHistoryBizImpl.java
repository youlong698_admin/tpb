package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidPriceDetailHistoryBiz;
import com.ced.sip.purchase.base.entity.BidPriceDetailHistory;
/** 
 * 类名称：BidPriceDetailHistoryBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceDetailHistoryBizImpl extends BaseBizImpl implements IBidPriceDetailHistoryBiz  {
	
	/**
	 * 根据主键获得供应商历史报价明细信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceDetailHistory getBidPriceDetailHistory(Long id) throws BaseException {
		return (BidPriceDetailHistory)this.getObject(BidPriceDetailHistory.class, id);
	}
	
	/**
	 * 获得供应商历史报价明细信息表实例
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceDetailHistory getBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException {
		return (BidPriceDetailHistory)this.getObject(BidPriceDetailHistory.class, bidPriceDetailHistory.getBpdhId() );
	}
	
	/**
	 * 添加供应商历史报价明细信息信息
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException{
		this.saveObject( bidPriceDetailHistory ) ;
	}
	
	/**
	 * 更新供应商历史报价明细信息表实例
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException{
		this.updateObject( bidPriceDetailHistory ) ;
	}
	
	/**
	 * 删除供应商历史报价明细信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceDetailHistory(String id) throws BaseException {
		this.removeObject( this.getBidPriceDetailHistory( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商历史报价明细信息表实例
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException {
		this.removeObject( bidPriceDetailHistory ) ;
	}
	
	/**
	 * 删除供应商历史报价明细信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceDetailHistorys(String[] id) throws BaseException {
		this.removeBatchObject(BidPriceDetailHistory.class, id) ;
	}
	
	/**
	 * 获得所有供应商历史报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailHistoryList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceDetailHistory de where 1 = 1 " );

		hql.append(" order by de.bpdhId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商历史报价明细信息表数据集   关联RequiredCollectDetail表
	 * @param bidPriceDetailHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailHistoryListRequiredCollectDetail(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,rcd from BidPriceDetailHistory de,RequiredCollectDetail rcd where rcd.rcdId=de.rcdId " );
		if(bidPriceDetailHistory != null){
			if(StringUtil.isNotBlank(bidPriceDetailHistory.getBphId())){
				hql.append(" and de.bphId =").append(bidPriceDetailHistory.getBphId()).append("");
			}
		}
		hql.append(" order by de.bpdhId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商历史报价明细信息表数据集
	 * @param bidPriceDetailHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailHistoryList(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException {
		StringBuffer hql = new StringBuffer("from BidPriceDetailHistory de where 1=1 " );
		if(bidPriceDetailHistory != null){
			if(StringUtil.isNotBlank(bidPriceDetailHistory.getBphId())){
				hql.append(" and de.bphId =").append(bidPriceDetailHistory.getBphId()).append("");
			}
		}
		hql.append(" order by de.bpdhId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商历史报价明细信息表数据集  供应商端
	 * @param bidPriceDetailHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailHistoryListSupplier(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,rcd from BidPriceDetailHistory de,RequiredCollectDetail rcd where rcd.rcdId=de.rcdId " );
		if(bidPriceDetailHistory != null){
			if(StringUtil.isNotBlank(bidPriceDetailHistory.getBphId())){
				hql.append(" and de.bphId =").append(bidPriceDetailHistory.getBphId()).append("");
			}
		}
		hql.append(" order by de.bpdhId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商历史报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceDetailHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailHistoryList(RollPage rollPage, BidPriceDetailHistory bidPriceDetailHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceDetailHistory de where 1 = 1 " );

		hql.append(" order by de.bpdhId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
