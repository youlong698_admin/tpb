package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidPriceHistoryBiz;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceHistory;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
/** 
 * 类名称：BidPriceHistoryBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceHistoryBizImpl extends BaseBizImpl implements IBidPriceHistoryBiz  {
	
	/**
	 * 根据主键获得供应商历史报价信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceHistory getBidPriceHistory(Long id) throws BaseException {
		return (BidPriceHistory)this.getObject(BidPriceHistory.class, id);
	}
	
	/**
	 * 获得供应商历史报价信息表实例
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceHistory getBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException {
		return (BidPriceHistory)this.getObject(BidPriceHistory.class, bidPriceHistory.getBphId() );
	}
	
	/**
	 * 添加供应商历史报价信息信息
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException{
		this.saveObject( bidPriceHistory ) ;
	}
	
	/**
	 * 更新供应商历史报价信息表实例
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException{
		this.updateObject( bidPriceHistory ) ;
	}
	
	/**
	 * 删除供应商历史报价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceHistory(String id) throws BaseException {
		this.removeObject( this.getBidPriceHistory( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商历史报价信息表实例
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException {
		this.removeObject( bidPriceHistory ) ;
	}
	
	/**
	 * 删除供应商历史报价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceHistorys(String[] id) throws BaseException {
		this.removeBatchObject(BidPriceHistory.class, id) ;
	}
	
	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceHistoryList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceHistory de where 1 = 1 " );

		hql.append(" order by de.bphId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param bidPriceHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceHistoryList(BidPriceHistory bidPriceHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceHistory de where 1 = 1 " );
		if(bidPriceHistory != null){
			if(StringUtil.isNotBlank(bidPriceHistory.getRcId())){
				hql.append(" and de.rcId =").append(bidPriceHistory.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidPriceHistory.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidPriceHistory.getSupplierId()).append("");
			}
			if(StringUtil.isNotBlank(bidPriceHistory.getRemark())){
				hql.append(" and de.remark ='").append(bidPriceHistory.getRemark()).append("'");
			}
			if(StringUtil.isNotBlank(bidPriceHistory.getRemark1())){
				hql.append(" and de.remark1 ='").append(bidPriceHistory.getRemark1()).append("'");
			}
		}
		hql.append(" order by de.bphId desc ");
		//System.out.println(hql.toString());
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param bidPriceHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceHistoryListForDecrypt(BidPriceHistory bidPriceHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceHistory de where de.totalPrice is null " );
		if(bidPriceHistory != null){
			if(StringUtil.isNotBlank(bidPriceHistory.getRcId())){
				hql.append(" and de.rcId =").append(bidPriceHistory.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidPriceHistory.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidPriceHistory.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.bphId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceHistoryList(RollPage rollPage, BidPriceHistory bidPriceHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceHistory de where 1 = 1 " );

		hql.append(" order by de.bphId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 竞价查询每轮次的竞价信息，竞买查询最低，竞卖查询最高的
	 * @param rcId
	 * @param biddingType
	 * @return
	 * @throws BaseException
	 */
	public List getBidPriceGroupByBiddingRound(Long rcId, String biddingType)
			throws BaseException {
		String hql="";
		if(biddingType.equals(BiddingStatus.BIDDING_TYPE_0)){
			hql="select min(de.totalPrice),si.supplierName,de.remark from BidPriceHistory de,SupplierInfo si where de.supplierId=si.supplierId  and de.rcId="+rcId+" group by de.remark,si.supplierName order by de.remark"; 
		}else{
			hql="select max(de.totalPrice),si.supplierName,de.remark from BidPriceHistory de,SupplierInfo si where de.supplierId=si.supplierId  and de.rcId="+rcId+" group by de.remark,si.supplierName order by de.remark"; 
		}
		return this.getObjects(hql);
	}
	/**
	 * 竞价图标分析查询
	 * @param rcId
	 * @param biddingType
	 * @return
	 * @throws BaseException
	 */
	public List getBidPriceHistoryByBddingChart(Long rcId)
			throws BaseException {
		String hql="select de.totalPrice,si.supplierName,de.supplierId,de.writeDate  from BidPriceHistory de,SupplierInfo si where de.supplierId=si.supplierId  and de.rcId="+rcId+" order by de.supplierId,de.writeDate"; 
		
		return this.getObjects(hql);
	}
}
