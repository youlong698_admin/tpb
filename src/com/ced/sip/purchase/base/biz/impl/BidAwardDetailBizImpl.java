package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidAwardDetailBiz;
import com.ced.sip.purchase.base.entity.BidAwardDetail;
/** 
 * 类名称：BidAwardDetailBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidAwardDetailBizImpl extends BaseBizImpl implements IBidAwardDetailBiz  {
	
	/**
	 * 根据主键获得授标结果明细表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidAwardDetail getBidAwardDetail(Long id) throws BaseException {
		return (BidAwardDetail)this.getObject(BidAwardDetail.class, id);
	}
	
	/**
	 * 获得授标结果明细表实例
	 * @param bidAwardDetail 授标结果明细表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidAwardDetail getBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException {
		return (BidAwardDetail)this.getObject(BidAwardDetail.class, bidAwardDetail.getBadId() );
	}
	
	/**
	 * 添加授标结果明细信息
	 * @param bidAwardDetail 授标结果明细表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void saveBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException{
		this.saveObject( bidAwardDetail ) ;
	}
	
	/**
	 * 更新授标结果明细表实例
	 * @param bidAwardDetail 授标结果明细表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void updateBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException{
		this.updateObject( bidAwardDetail ) ;
	}
	
	/**
	 * 删除授标结果明细表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidAwardDetail(String id) throws BaseException {
		this.removeObject( this.getBidAwardDetail( new Long(id) ) ) ;
	}
	
	/**
	 * 删除授标结果明细表实例
	 * @param bidAwardDetail 授标结果明细表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException {
		this.removeObject( bidAwardDetail ) ;
	}
	
	/**
	 * 删除授标结果明细表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidAwardDetails(String[] id) throws BaseException {
		this.removeBatchObject(BidAwardDetail.class, id) ;
	}
	
	/**
	 * 获得授标结果明细表数据集
	 * bidAwardDetail 授标结果明细表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidAwardDetail getBidAwardDetailByBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAwardDetail de where 1 = 1 " );

		hql.append(" order by de.badId desc ");
		List list = this.getObjects( hql.toString() );
		bidAwardDetail = new BidAwardDetail();
		if(list!=null&&list.size()>0){
			bidAwardDetail = (BidAwardDetail)list.get(0);
		}
		return bidAwardDetail;
	}
	
	/**
	 * 获得所有授标结果明细表数据集
	 * @param bidAwardDetail 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAwardDetailList(BidAwardDetail bidAwardDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAwardDetail de where 1 = 1 " );

		hql.append(" order by de.badId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有授标结果明细表数据集
	 * @param rollPage 分页对象
	 * @param bidAwardDetail 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAwardDetailList(RollPage rollPage, BidAwardDetail bidAwardDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAwardDetail de where 1 = 1 " );

		hql.append(" order by de.badId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有供应商报价明细信息表数据集   关联RequiredCollectDetail表
	 * @param bidPriceDetail 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAwardDetailListRequiredCollectDetail(BidAwardDetail bidAwardDetail) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,rcd from BidAwardDetail de,RequiredCollectDetail rcd where de.rcdId=rcd.rcdId " );
		if(bidAwardDetail != null){
			if(StringUtil.isNotBlank(bidAwardDetail.getBaId())){
				hql.append(" and de.baId =").append(bidAwardDetail.getBaId()).append("");
			}
		}
		hql.append(" order by de.rcdId desc ");
		return this.getObjects( hql.toString() );
	}	
}
