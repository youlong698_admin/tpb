package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidResponseNegotiateBiz;
import com.ced.sip.purchase.base.entity.BidResponseNegotiate;
/** 
 * 类名称：BidResponseNegotiateBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidResponseNegotiateBizImpl extends BaseBizImpl implements IBidResponseNegotiateBiz  {
	
	/**
	 * 根据主键获得商务响应项磋商表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidResponseNegotiate getBidResponseNegotiate(Long id) throws BaseException {
		return (BidResponseNegotiate)this.getObject(BidResponseNegotiate.class, id);
	}
	
	/**
	 * 获得商务响应项磋商表实例
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidResponseNegotiate getBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException {
		return (BidResponseNegotiate)this.getObject(BidResponseNegotiate.class, bidResponseNegotiate.getBrnId() );
	}
	
	/**
	 * 添加商务响应项磋商信息
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException{
		this.saveObject( bidResponseNegotiate ) ;
	}
	
	/**
	 * 更新商务响应项磋商表实例
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException{
		this.updateObject( bidResponseNegotiate ) ;
	}
	
	/**
	 * 删除商务响应项磋商表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidResponseNegotiate(String id) throws BaseException {
		this.removeObject( this.getBidResponseNegotiate( new Long(id) ) ) ;
	}
	
	/**
	 * 删除商务响应项磋商表实例
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException {
		this.removeObject( bidResponseNegotiate ) ;
	}
	
	/**
	 * 删除商务响应项磋商表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidResponseNegotiates(String[] id) throws BaseException {
		this.removeBatchObject(BidResponseNegotiate.class, id) ;
	}
	
	/**
	 * 获得所有商务响应项磋商表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidResponseNegotiateList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidResponseNegotiate de where 1 = 1 " );

		hql.append(" order by de.brnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有商务响应项磋商表数据集
	 * @param bidResponseNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidResponseNegotiateList(BidResponseNegotiate bidResponseNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidResponseNegotiate de where 1 = 1 " );
		if(bidResponseNegotiate != null){
			if(StringUtil.isNotBlank(bidResponseNegotiate.getBnId())){
				hql.append(" and de.bnId =").append(bidResponseNegotiate.getBnId()).append("");
			}
		}
		hql.append(" order by de.brnId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有商务响应项磋商表数据集
	 * @param rollPage 分页对象
	 * @param bidResponseNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidResponseNegotiateList(RollPage rollPage, BidResponseNegotiate bidResponseNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidResponseNegotiate de where 1 = 1 " );
		if(bidResponseNegotiate != null){
			if(StringUtil.isNotBlank(bidResponseNegotiate.getBnId())){
				hql.append(" and de.bnId =").append(bidResponseNegotiate.getBnId()).append("");
			}
		}
		hql.append(" order by de.brnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
