package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
/** 
 * 类名称：BusinessResponseItemBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public class BusinessResponseItemBizImpl extends BaseBizImpl implements IBusinessResponseItemBiz  {
	
	/**
	 * 根据主键获得商务响应项表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public BusinessResponseItem getBusinessResponseItem(Long id) throws BaseException {
		return (BusinessResponseItem)this.getObject(BusinessResponseItem.class, id);
	}
	
	/**
	 * 获得商务响应项表表实例
	 * @param businessResponseItem 商务响应项表表实例
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public BusinessResponseItem getBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException {
		return (BusinessResponseItem)this.getObject(BusinessResponseItem.class, businessResponseItem.getBriId() );
	}
	
	/**
	 * 添加商务响应项表信息
	 * @param businessResponseItem 商务响应项表表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void saveBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException{
		this.saveObject( businessResponseItem ) ;
	}
	
	/**
	 * 更新商务响应项表表实例
	 * @param businessResponseItem 商务响应项表表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void updateBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException{
		this.updateObject( businessResponseItem ) ;
	}
	
	/**
	 * 删除商务响应项表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteBusinessResponseItem(String id) throws BaseException {
		this.removeObject( this.getBusinessResponseItem( new Long(id) ) ) ;
	}
	
	/**
	 * 删除商务响应项表表实例
	 * @param businessResponseItem 商务响应项表表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException {
		this.removeObject( businessResponseItem ) ;
	}
	
	/**
	 * 删除商务响应项表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteBusinessResponseItems(String[] id) throws BaseException {
		this.removeBatchObject(BusinessResponseItem.class, id) ;
	}
	
	/**
	 * 获得所有商务响应项表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getBusinessResponseItemList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BusinessResponseItem de where 1 = 1 " );

		hql.append(" order by de.briId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有商务响应项表表数据集
	 * @param businessResponseItem 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getBusinessResponseItemList(BusinessResponseItem businessResponseItem) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BusinessResponseItem de where 1 = 1 " );
		if(businessResponseItem != null){
			if(StringUtil.isNotBlank(businessResponseItem.getRcId())){
				hql.append(" and de.rcId =").append(businessResponseItem.getRcId()).append("");
			}
		}
		hql.append(" order by de.briId ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有商务响应项表表数据集
	 * @param rollPage 分页对象
	 * @param businessResponseItem 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getBusinessResponseItemList(RollPage rollPage, BusinessResponseItem businessResponseItem) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BusinessResponseItem de where 1 = 1 " );
		if(businessResponseItem != null){
			if(StringUtil.isNotBlank(businessResponseItem.getRcId())){
				hql.append(" and de.rcId =").append(businessResponseItem.getRcId()).append("");
			}
		}
		hql.append(" order by de.briId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 删除邀请供应商表表实例
	 * @param rcId
	 * @throws BaseException 
	 */
	public void deleteBusinessResponseItemByRcId(Long rcId) throws BaseException{
		String sql="delete from business_response_item where rc_id="+rcId;
		this.updateJdbcSql(sql);
	}
}
