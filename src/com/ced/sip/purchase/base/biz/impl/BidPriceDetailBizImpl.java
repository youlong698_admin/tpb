package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
/** 
 * 类名称：BidPriceDetailBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceDetailBizImpl extends BaseBizImpl implements IBidPriceDetailBiz  {
	
	/**
	 * 根据主键获得供应商报价明细信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceDetail getBidPriceDetail(Long id) throws BaseException {
		return (BidPriceDetail)this.getObject(BidPriceDetail.class, id);
	}
	
	/**
	 * 获得供应商报价明细信息表实例
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPriceDetail getBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException {
		return (BidPriceDetail)this.getObject(BidPriceDetail.class, bidPriceDetail.getBpdId() );
	}
	
	/**
	 * 添加供应商报价明细信息信息
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException{
		this.saveObject( bidPriceDetail ) ;
	}
	
	/**
	 * 更新供应商报价明细信息表实例
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException{
		this.updateObject( bidPriceDetail ) ;
	}
	
	/**
	 * 删除供应商报价明细信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceDetail(String id) throws BaseException {
		this.removeObject( this.getBidPriceDetail( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商报价明细信息表实例
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException {
		this.removeObject( bidPriceDetail ) ;
	}
	
	/**
	 * 删除供应商报价明细信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPriceDetails(String[] id) throws BaseException {
		this.removeBatchObject(BidPriceDetail.class, id) ;
	}
	
	/**
	 * 获得所有供应商报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceDetail de where 1 = 1 " );

		hql.append(" order by de.rcdId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商报价明细信息表数据集   关联RequiredCollectDetail表
	 * @param bidPriceDetail 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailListRequiredCollectDetail(BidPriceDetail bidPriceDetail) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,rcd from BidPriceDetail de,RequiredCollectDetail rcd where de.rcdId=rcd.rcdId " );
		if(bidPriceDetail != null){
			if(StringUtil.isNotBlank(bidPriceDetail.getBpId())){
				hql.append(" and de.bpId =").append(bidPriceDetail.getBpId()).append("");
			}
		}
		hql.append(" order by de.rcdId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商报价明细信息表数据集   
	 * @param bidPriceDetail 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailList(BidPriceDetail bidPriceDetail) throws BaseException {
		StringBuffer hql = new StringBuffer("from BidPriceDetail de where 1=1 " );
		if(bidPriceDetail != null){
			if(StringUtil.isNotBlank(bidPriceDetail.getBpId())){
				hql.append(" and de.bpId =").append(bidPriceDetail.getBpId()).append("");
			}
		}
		hql.append(" order by de.rcdId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商报价明细信息表数据集   供应商
	 * @param bidPriceDetail 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailListSupplier(BidPriceDetail bidPriceDetail) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,rcd from BidPriceDetail de,RequiredCollectDetail rcd where de.rcdId=rcd.rcdId " );
		if(bidPriceDetail != null){
			if(StringUtil.isNotBlank(bidPriceDetail.getBpId())){
				hql.append(" and de.bpId =").append(bidPriceDetail.getBpId()).append("");
			}
		}
		hql.append(" order by de.bpdId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceDetail 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceDetailList(RollPage rollPage, BidPriceDetail bidPriceDetail) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPriceDetail de where 1 = 1 " );

		hql.append(" order by de.bpdId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获取供应商比价数据集
	 * @param bpIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	public List getBidPriceDetailListForParityPrice(Long[] bpIdStr,Long rcId) throws BaseException{
		int columnCount=5+bpIdStr.length;
		StringBuffer hql = new StringBuffer(" select rcd.buy_code,rcd.buy_name,rcd.material_type,rcd.unit,rcd.amount" );
		for(Long bpId:bpIdStr){
			hql.append(",bpd"+bpId+".price price"+bpId);
		}
		hql.append("  from required_collect_detail rcd ");
		for(Long bpId:bpIdStr){
			hql.append(" left join (select rcd_Id,price from bid_price_detail where bp_id="+bpId+" ) bpd"+bpId+" on rcd.rcd_id=bpd"+bpId+".rcd_Id");
		}
		hql.append("  where rcd.rc_id="+rcId);
		return this.getObjects(hql.toString(), columnCount);
	}
	/**
	 * 根据采购计划明细ID获取当前报价的最低价
	 * @param rcdId
	 * @return
	 * @throws BaseException
	 */	
	public double getLowestPrice(Long rcdId) throws BaseException{
		StringBuffer hql = new StringBuffer(" select nvl(min(price),0.00) from BidPriceDetail de  where de.rcdId="+rcdId);
		return (Double)this.sumOrMinOrMaxObjects(hql.toString(),Double.class);
	}
	
}
