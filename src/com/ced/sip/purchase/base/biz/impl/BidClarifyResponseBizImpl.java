package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidClarifyResponseBiz;
import com.ced.sip.purchase.base.entity.BidClarifyResponse;
/** 
 * 类名称：BidClarifyResponseBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-08
 */
public class BidClarifyResponseBizImpl extends BaseBizImpl implements IBidClarifyResponseBiz  {
	
	/**
	 * 根据主键获得标前澄清响应表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-08
	 * @return
	 * @throws BaseException 
	 */
	public BidClarifyResponse getBidClarifyResponse(Long id) throws BaseException {
		return (BidClarifyResponse)this.getObject(BidClarifyResponse.class, id);
	}
	
	/**
	 * 获得标前澄清响应表实例
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @author luguanglei 2017-04-08
	 * @return
	 * @throws BaseException 
	 */
	public BidClarifyResponse getBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException {
		return (BidClarifyResponse)this.getObject(BidClarifyResponse.class, bidClarifyResponse.getBcrId() );
	}
	
	/**
	 * 添加标前澄清响应信息
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @author luguanglei 2017-04-08
	 * @throws BaseException 
	 */
	public void saveBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException{
		this.saveObject( bidClarifyResponse ) ;
	}
	
	/**
	 * 更新标前澄清响应表实例
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @author luguanglei 2017-04-08
	 * @throws BaseException 
	 */
	public void updateBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException{
		this.updateObject( bidClarifyResponse ) ;
	}
	
	/**
	 * 删除标前澄清响应表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-08
	 * @throws BaseException 
	 */
	public void deleteBidClarifyResponse(String id) throws BaseException {
		this.removeObject( this.getBidClarifyResponse( new Long(id) ) ) ;
	}
	
	/**
	 * 删除标前澄清响应表实例
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @author luguanglei 2017-04-08
	 * @throws BaseException 
	 */
	public void deleteBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException {
		this.removeObject( bidClarifyResponse ) ;
	}
	
	/**
	 * 删除标前澄清响应表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-08
	 * @throws BaseException 
	 */
	public void deleteBidClarifyResponses(String[] id) throws BaseException {
		this.removeBatchObject(BidClarifyResponse.class, id) ;
	}
	
	/**
	 * 获得所有标前澄清响应表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-08
	 * @return
	 * @throws BaseException 
	 */
	public List getBidClarifyResponseList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidClarifyResponse de where 1 = 1 " );

		hql.append(" order by de.bcrId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有标前澄清响应表数据集
	 * @param bidClarifyResponse 查询参数对象
	 * @author luguanglei 2017-04-08
	 * @return
	 * @throws BaseException 
	 */
	public List getBidClarifyResponseList(BidClarifyResponse bidClarifyResponse) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidClarifyResponse de where 1 = 1 " );
		if(bidClarifyResponse != null){
			if(StringUtil.isNotBlank(bidClarifyResponse.getBcId())){
				hql.append(" and de.bcId =").append(bidClarifyResponse.getBcId()).append("");
			}
			if(StringUtil.isNotBlank(bidClarifyResponse.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidClarifyResponse.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.bcrId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有标前澄清响应表数据集
	 * @param rollPage 分页对象
	 * @param bidClarifyResponse 查询参数对象
	 * @author luguanglei 2017-04-08
	 * @return
	 * @throws BaseException 
	 */
	public List getBidClarifyResponseList(RollPage rollPage, BidClarifyResponse bidClarifyResponse) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidClarifyResponse de where 1 = 1 " );
		if(bidClarifyResponse != null){
			if(StringUtil.isNotBlank(bidClarifyResponse.getBcId())){
				hql.append(" and de.bcId =").append(bidClarifyResponse.getBcId()).append("");
			}
			if(StringUtil.isNotBlank(bidClarifyResponse.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidClarifyResponse.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.bcrId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 删除标前澄清响应表实例
	 * @param bcId
	 * @throws BaseException 
	 */
	public void deleteBidClarifyResponseByBcId(Long bcId) throws BaseException{
		String sql="delete from bid_clarify_response where bc_id="+bcId;
		this.updateJdbcSql(sql);
	}
}
