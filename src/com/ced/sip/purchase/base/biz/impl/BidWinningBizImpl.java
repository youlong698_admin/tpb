package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidWinningBiz;
import com.ced.sip.purchase.base.entity.BidWinning;
/** 
 * 类名称：BidWinningBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidWinningBizImpl extends BaseBizImpl implements IBidWinningBiz  {
	
	/**
	 * 根据主键获得中标公示表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidWinning getBidWinning(Long id) throws BaseException {
		return (BidWinning)this.getObject(BidWinning.class, id);
	}
	
	/**
	 * 获得中标公示表实例
	 * @param bidWinning 中标公示表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidWinning getBidWinning(BidWinning bidWinning) throws BaseException {
		return (BidWinning)this.getObject(BidWinning.class, bidWinning.getBwId() );
	}
	
	/**
	 * 添加中标公示信息
	 * @param bidWinning 中标公示表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void saveBidWinning(BidWinning bidWinning) throws BaseException{
		this.saveObject( bidWinning ) ;
	}
	
	/**
	 * 更新中标公示表实例
	 * @param bidWinning 中标公示表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void updateBidWinning(BidWinning bidWinning) throws BaseException{
		this.updateObject( bidWinning ) ;
	}
	
	/**
	 * 删除中标公示表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidWinning(String id) throws BaseException {
		this.removeObject( this.getBidWinning( new Long(id) ) ) ;
	}
	
	/**
	 * 删除中标公示表实例
	 * @param bidWinning 中标公示表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidWinning(BidWinning bidWinning) throws BaseException {
		this.removeObject( bidWinning ) ;
	}
	
	/**
	 * 删除中标公示表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidWinnings(String[] id) throws BaseException {
		this.removeBatchObject(BidWinning.class, id) ;
	}
	
	/**
	 * 获得中标公示表数据集
	 * bidWinning 中标公示表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidWinning getBidWinningByBidWinning(BidWinning bidWinning) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidWinning de where 1 = 1 " );
		if(bidWinning != null){
			if(StringUtil.isNotBlank(bidWinning.getBwId())){
				hql.append(" and de.bwId =").append(bidWinning.getBwId()).append("");
			}
		}
		List list = this.getObjects( hql.toString() );
		bidWinning = new BidWinning();
		if(list!=null&&list.size()>0){
			bidWinning = (BidWinning)list.get(0);
		}
		return bidWinning;
	}
	
	/**
	 * 获得所有中标公示表数据集
	 * @param bidWinning 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidWinningList(BidWinning bidWinning) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidWinning de where 1 = 1 " );
		if(bidWinning != null){
			if(StringUtil.isNotBlank(bidWinning.getRcId())){
				hql.append(" and de.rcId =").append(bidWinning.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidWinning.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidWinning.getBuyWay()).append("'");
			}
		}
		hql.append(" order by de.bwId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有中标公示表数据集
	 * @param rollPage 分页对象
	 * @param bidWinning 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidWinningList(RollPage rollPage, BidWinning bidWinning) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidWinning de where 1 = 1 and de.status='0'" );
		if(bidWinning != null){
			if(StringUtil.isNotBlank(bidWinning.getRcId())){
				hql.append(" and de.rcId =").append(bidWinning.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidWinning.getBuyWay())){
				hql.append(" and de.buyWay ='").append(bidWinning.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(bidWinning.getStatus())){
				hql.append(" and de.status ='").append(bidWinning.getStatus()).append("'");
			}
			if(StringUtil.isNotBlank(bidWinning.getComId())){
				hql.append(" and de.comId =").append(bidWinning.getComId()).append("");
			}
		}
		hql.append(" order by de.bwId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
}
