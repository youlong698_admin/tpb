package com.ced.sip.purchase.base.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;

public class RequiredCollectBizImpl extends BaseBizImpl implements IRequiredCollectBiz{
	/**
	 * 根据主键获得汇总分包表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public RequiredCollect getRequiredCollect(Long id) throws BaseException {
		return (RequiredCollect)this.getObject(RequiredCollect.class, id);
	}
	
	/**
	 * 获得汇总分包表实例
	 * @param requiredCollect 汇总分包表实例
	 * @return
	 * @throws BaseException 
	 */
	public RequiredCollect getRequiredCollect( RequiredCollect requiredCollect ) throws BaseException {
		return (RequiredCollect)this.getObject(RequiredCollect.class, requiredCollect.getRcId() );
	}
	
	/**
	 * 添加汇总分包信息
	 * @param requiredCollect 汇总分包表实例
	 * @throws BaseException 
	 */
	public void saveRequiredCollect(RequiredCollect requiredCollect) throws BaseException{
		this.saveObject( requiredCollect ) ;
	}
	
	/**
	 * 更新汇总分包表实例
	 * @param requiredCollect 汇总分包表实例
	 * @throws BaseException 
	 */
	public void updateRequiredCollect(RequiredCollect requiredCollect) throws BaseException{
		this.updateObject( requiredCollect ) ;
	}
	
	/**
	 * 删除汇总分包表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteRequiredCollect(String id) throws BaseException {
		this.removeObject( this.getRequiredCollect( new Long(id) ) ) ;
	}
	
	/**
	 * 删除汇总分包表实例
	 * @param requiredCollect 汇总分包表实例
	 * @throws BaseException 
	 */
	public void deleteRequiredCollect(RequiredCollect requiredCollect) throws BaseException {
		this.removeObject( requiredCollect ) ;
	}
	
	/**
	 * 删除汇总分包表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteRequiredCollects(String[] id) throws BaseException {
		this.removeBatchObject(RequiredCollect.class, id) ;
	}
	
	/**
	 * 获得汇总分包表数据（对象）
	 * @param requiredCollect 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public RequiredCollect getRequiredCollectInfo(  RequiredCollect requiredCollect ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where 1 = 1 " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getRcId())){
				hql.append(" and de.rcId =").append(requiredCollect.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode ='").append(requiredCollect.getBidCode()).append("'");
			}
		}
		requiredCollect = new RequiredCollect();
		List<RequiredCollect> list = this.getObjects( hql.toString() );
		if(list!=null&&list.size()>0){
			requiredCollect = list.get(0);
		}
		return requiredCollect;
	}
	
	/**
	 * 获得所有汇总分包表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectList(RollPage rollPage,RequiredCollect requiredCollect) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where 1 = 1 " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有汇总分包表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectList(RollPage rollPage,RequiredCollect requiredCollect,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where 1 = 1 " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getServiceStatusCn())){
				hql.append(" and  de.serviceStatusCn='").append( requiredCollect.getServiceStatusCn()).append("'");
			}
		}
		hql.append(sqlStr);
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有汇总分包表数据集  开标管理员查看招标项目信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListByBidOpenAdmin(RollPage rollPage,RequiredCollect requiredCollect,Long openBidUserId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where 1 = 1 " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getServiceStatusCn())){
				hql.append(" and  de.serviceStatusCn='").append( requiredCollect.getServiceStatusCn()).append("'");
			}
		}
		hql.append(" and exists (select t.rcId from TenderBidList t where t.bidOpenAdmin="+openBidUserId+" and t.rcId=de.rcId)");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有汇总分包表数据集  竞价管理员查看竞价项目信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListByBiddingAdmin(RollPage rollPage,RequiredCollect requiredCollect,Long biddingUserId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where 1 = 1 " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getServiceStatusCn())){
				hql.append(" and  de.serviceStatusCn='").append( requiredCollect.getServiceStatusCn()).append("'");
			}
		}
		hql.append(" and exists (select t.rcId from BiddingBidList t where t.bidAdmin="+biddingUserId+" and t.rcId=de.rcId)");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有汇总分包表数据集
	 * @param requiredCollect 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectList(RequiredCollect requiredCollect ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where 1 = 1 " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode ='").append(requiredCollect.getBidCode()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay = '").append(requiredCollect.getBuyWay()).append("'");
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 根据主键获得汇总分包明细表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public RequiredCollectDetail getRequiredCollectDetail(Long id) throws BaseException {
		return (RequiredCollectDetail)this.getObject(RequiredCollectDetail.class, id);
	}
	
	/**
	 * 获得汇总分包明细表实例
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @return
	 * @throws BaseException 
	 */
	public RequiredCollectDetail getRequiredCollectDetail( RequiredCollectDetail requiredCollectDetail ) throws BaseException {
		return (RequiredCollectDetail)this.getObject(RequiredCollectDetail.class, requiredCollectDetail.getRcdId() );
	}
	
	/**
	 * 添加汇总分包明细信息
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @throws BaseException 
	 */
	public void saveRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) throws BaseException{
		this.saveObject( requiredCollectDetail ) ;
	}
	
	/**
	 * 更新汇总分包明细表实例
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @throws BaseException 
	 */
	public void updateRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) throws BaseException{
		this.updateObject( requiredCollectDetail ) ;
	}
	
	/**
	 * 删除汇总分包明细表实例
	 * @param id 主键数组
	 * @author ted 2012-10-18
	 * @throws BaseException 
	 */
	public void deleteRequiredCollectDetail(String id) throws BaseException {
		this.removeObject( this.getRequiredCollectDetail( new Long(id) ) ) ;
	}
	
	/**
	 * 删除汇总分包明细表实例
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @throws BaseException 
	 */
	public void deleteRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) throws BaseException {
		this.removeObject( requiredCollectDetail ) ;
	}
	
	/**
	 * 删除汇总分包明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deleteRequiredCollectDetails(String[] id) throws BaseException {
		this.removeBatchObject(RequiredCollectDetail.class, id) ;
	}
	
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectDetailList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollectDetail de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param requiredCollectDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectDetailList(  RequiredCollectDetail requiredCollectDetail ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollectDetail de where 1 = 1 " );
		if(requiredCollectDetail!=null){
			if(StringUtil.isNotBlank(requiredCollectDetail.getBidCode())){
				hql.append(" and de.bidCode = '").append(requiredCollectDetail.getBidCode()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollectDetail.getRcId())){
				hql.append(" and de.rcId = ").append(requiredCollectDetail.getRcId()).append(" ");
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param rollPage 分页对象
	 * @param requiredCollectDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectDetailList( RollPage rollPage, RequiredCollectDetail requiredCollectDetail ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollectDetail de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param rcId 查询参数对象 汇总表ID
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectDetailList(Long rcId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollectDetail de where 1 = 1 " );
		hql.append(" and de.rcId =").append(rcId);
		return this.getObjects(hql.toString() );
	}
	/**
	 * 获取当年最大流水号
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public int getMaxFloatCodeByPrefixStr(String prefixStr) throws BaseException {
		int floatCode = 0;
		StringBuffer hql = new StringBuffer("select max(de.floatCode) from RequiredCollect de where 1 = 1 " );
		
		hql.append(" and de.bidCode like '").append(prefixStr).append("%'");
		
		List list = this.getObjects(hql.toString());
		if(list!=null && list.get(0)!= null){
			floatCode = Integer.parseInt(list.get(0).toString());
		}
		return floatCode;
	}
	/**
	 * 删除所有汇总分包明细表数据集
	 * @param rcId 查询参数对象 汇总表ID
	 * @return
	 * @throws BaseException 
	 */
	public void deleteRequiredCollectDetailList(Long rcId) throws BaseException {
		List<RequiredCollectDetail> rcdList = this.getRequiredCollectDetailList(rcId);
		for(RequiredCollectDetail rcd : rcdList){
			this.deleteRequiredCollectDetail(rcd);
		}
	}
	public List getRequiredCollecListByRmId(Long rmId)
			throws BaseException {
		StringBuffer hql = new StringBuffer("select distinct  de from RequiredCollect de,RequiredCollectDetail rcd,RequiredMaterialDetail rmd where rcd.rmdId=rmd.rmdId and rcd.rcId=de.rcId " );
		hql.append(" and rmd.rmId = ").append(rmId).append("");
		return this.getObjects( hql.toString() );
	}
	public List getRequiredCollecListByRmCode(String rmCode)
			throws BaseException {
		StringBuffer hql = new StringBuffer("select distinct  de from RequiredCollect de,RequiredCollectDetail rcd,RequiredMaterialDetail rmd where rcd.rmdId=rmd.rmdId and rcd.rcId=de.rcId " );
		hql.append(" and rmd.rmCode like '%").append(rmCode).append("%'");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 查看供应商报名表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListSupplierSignUp(RollPage rollPage,RequiredCollect requiredCollect) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,sc.compName from RequiredCollect de,SysCompany sc where de.comId=sc.scId " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 查看我报名的项目信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListMyApplication(RollPage rollPage,RequiredCollect requiredCollect,Long supplierId) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,sc.compName  from RequiredCollect de,InviteSupplier isp,SysCompany sc where de.comId=sc.scId and de.rcId=isp.rcId and isp.supplierId= "+supplierId );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 查询采购物资 网站查询所用
	 * @param rollPage
	 * @param mapParams
	 * @return
	 * @throws BaseException
	 */
	public List getRequiredCollectListForWebIndex(RollPage rollPage,
			Map<String, Object> mapParams) throws BaseException {
		StringBuffer hql = new StringBuffer("select bb.bbId,bb.publishDate,bb.returnDate,bb.buyWay,bb.sysCompany,rcd.buyName,rcd.materialType,rcd.unit,rcd.amount,sc.industryOwned,sc.province,sc.city from BidBulletin  bb ");
		hql.append(",RequiredCollectDetail rcd,SysCompany sc where bb.rcId=rcd.rcId  " );
		hql.append("and bb.comId=sc.scId and bb.status='0' and bb.returnDate>sysdate " );		
		if(StringUtil.isNotBlank(mapParams) ){
			if(StringUtil.isNotBlank(mapParams.get("buyName"))){
				hql.append(" and (rcd.buyName like '%"+mapParams.get("buyName")+"%' or rcd.materialType like '%"+mapParams.get("buyName")+"%')");
			}
			if(StringUtil.isNotBlank(mapParams.get("sysCompany"))){
				hql.append(" and sc.sysCompany like '%").append(mapParams.get("sysCompany")).append("%'");
			}
			if(StringUtil.isNotBlank(mapParams.get("province"))){
				hql.append(" and sc.province = '").append(mapParams.get("province")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("city"))){
				hql.append(" and sc.city = '").append(mapParams.get("city")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("industryOwned"))){
				hql.append(" and sc.industryOwned = '").append(mapParams.get("industryOwned")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and bb.publishDate > to_date('").append(mapParams.get("startDate")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and bb.publishDate < to_date('").append(mapParams.get("endDate")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(mapParams.get("returnStartDate"))){
				hql.append(" and bb.returnDate < to_date('").append(mapParams.get("returnStartDate")).append("','yyyy-MM-dd')");
			}
			int bidderType=(Integer)mapParams.get("bidderType");
			if(bidderType==1){
				hql.append(" and bb.buyWay = '00'");
			}else if(bidderType==2){
				hql.append(" and bb.buyWay = '01'");
			}else if(bidderType==3){
				hql.append(" and bb.buyWay = '02'");
			}
		}
		hql.append(" order by rcd.rcdId desc ");
		return this.getObjects(rollPage,hql.toString() );
	}
	/**
	 * 获得所有采购完成的项目信息表数据集   新建合同信息所用
	 * @param rollPage 分页对象
	 * @param map  查询条件map对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListForCollectAndOrder(RollPage rollPage,Map<String,Object> map,String sqlStr,int sign) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,ba.bidPrice,ba.baId,si.supplierId,si.supplierName,ba.writeDate from RequiredCollect de,BidAward ba,SupplierInfo si where de.rcId=ba.rcId and ba.supplierId=si.supplierId " );
		if(StringUtil.isNotBlank(map)){
			if(StringUtil.isNotBlank(map.get("bidCode"))){
				hql.append(" and de.bidCode like '%").append(map.get("bidCode")).append("%'");
			}
			
			if(StringUtil.isNotBlank(map.get("buyRemark"))){
				hql.append(" and de.buyRemark like '%").append(map.get("buyRemark")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("buyWay"))){
				hql.append(" and de.buyWay ='").append(map.get("buyWay")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("supplierName"))){
				hql.append(" and si.supplierName like '%").append(map.get("supplierName")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("comId"))){
				hql.append(" and de.comId =").append(map.get("comId"));
			}
		}
		hql.append(sqlStr);
		if(sign==1) hql.append(" and ba.isContract=1 ");
		else if(sign==2) hql.append(" and ba.isOrder=1 ");
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 获得所有汇总分包表数据集   采购方式变更
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListForPurchaseChange(RollPage rollPage,RequiredCollect requiredCollect,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredCollect de where de.status='0' and de.bidStatus='1' " );
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getBidCode())){
				hql.append(" and de.bidCode like '%").append(requiredCollect.getBidCode()).append("%'");
			}
			
			if(StringUtil.isNotBlank(requiredCollect.getBuyRemark())){
				hql.append(" and de.buyRemark like '%").append(requiredCollect.getBuyRemark()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getBuyWay())){
				hql.append(" and de.buyWay ='").append(requiredCollect.getBuyWay()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getIsSendNotice())){
				hql.append(" and de.isSendNotice = "+requiredCollect.getIsSendNotice());
			}
			if(StringUtil.isNotBlank(requiredCollect.getSupplierType())){
				hql.append(" and  de.supplierType='").append( requiredCollect.getSupplierType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredCollect.getServiceStatusCn())){
				hql.append(" and  de.serviceStatusCn='").append( requiredCollect.getServiceStatusCn()).append("'");
			}
		}
		hql.append(sqlStr);
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有标汇总分包表数据集
	 * @param mapParams 查询参数对象
	 * @param sql 权限sql
	 * @return
	 * @throws BaseException 
	 */
	public int countRequiredCollectListList(Map<String, Object> mapParams,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.rcId) from RequiredCollect de where de.status='0' and de.bidStatus='1' " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("bidStatus"))){
				hql.append(" and de.bidStatus = '").append(mapParams.get("bidStatus")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("buyWay"))){
				hql.append(" and de.buyWay ='").append(mapParams.get("buyWay")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		hql.append(sqlStr);
		return this.countObjects(hql.toString() );
	}
	/**
     * 变更采购方式  原理 插入项目明细
     * @param oldRcId
     * @param newRcId
     * @param newBidCode
     * @throws BaseException
     */
	public void updatePurchaseChangeDetail(Long oldRcId,Long newRcId,String newBidCode) throws BaseException {
		String sql="insert into required_collect_detail(rcd_id,rc_id,bid_code,buy_code,buy_name,material_type,unit,amount,deliver_date,remark,rmd_id,is_collect,com_id,contract_amount,mnemonic_code,material_id,estimate_sum_price,estimate_price,new_price)" +
				"select seq_required_collect_detail_id.nextval,"+newRcId+",'"+newBidCode+"',buy_code,buy_name,material_type,unit,amount,deliver_date,remark,rmd_id,is_collect,com_id,contract_amount,mnemonic_code,material_id,estimate_sum_price,estimate_price,new_price from required_collect_detail where rc_id="+oldRcId;
		this.updateJdbcSql(sql);
		
	}

	 /**
     * 更新开标状态
     * @param rcId
     * @throws BaseException
     */
	public void updateOpenStatus(Long rcId) throws BaseException {
		String sql="update required_collect set open_status='02' where rc_id="+rcId;
        this.updateJdbcSql(sql);
		
	}
	
	/**
	 * 采购概况报表查询
	 * @param rollPage 分页对象
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListForPurchaseReport(RollPage rollPage,Map<String,Object> map,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,ba.bidPrice,ba.baId,si.supplierId,si.supplierName,ba.writeDate from RequiredCollect de,BidAward ba,SupplierInfo si where de.rcId=ba.rcId and ba.supplierId=si.supplierId " );
		if(StringUtil.isNotBlank(map)){
			if(StringUtil.isNotBlank(map.get("bidCode"))){
				hql.append(" and de.bidCode like '%").append(map.get("bidCode")).append("%'");
			}
			
			if(StringUtil.isNotBlank(map.get("buyRemark"))){
				hql.append(" and de.buyRemark like '%").append(map.get("buyRemark")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("buyWay"))){
				hql.append(" and de.buyWay ='").append(map.get("buyWay")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("supplierName"))){
				hql.append(" and si.supplierName like '%").append(map.get("supplierName")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("comId"))){
				hql.append(" and de.comId =").append(map.get("comId"));
			}
			if(StringUtil.isNotBlank(map.get("dateStart"))){
				hql.append(" and  ba.writeDate>=to_date('").append(map.get("dateStart")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(map.get("dateEnd"))){
				hql.append(" and  ba.writeDate<=to_date('").append(map.get("dateEnd")).append("','yyyy-MM-dd')");
			}
		}
		hql.append(sqlStr);
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 采购概况报表查询
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredCollectListForPurchaseReport(Map<String,Object> map,String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,ba.bidPrice,ba.baId,si.supplierId,si.supplierName,ba.writeDate from RequiredCollect de,BidAward ba,SupplierInfo si where de.rcId=ba.rcId and ba.supplierId=si.supplierId " );
		if(StringUtil.isNotBlank(map)){
			if(StringUtil.isNotBlank(map.get("bidCode"))){
				hql.append(" and de.bidCode like '%").append(map.get("bidCode")).append("%'");
			}
			
			if(StringUtil.isNotBlank(map.get("buyRemark"))){
				hql.append(" and de.buyRemark like '%").append(map.get("buyRemark")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("buyWay"))){
				hql.append(" and de.buyWay ='").append(map.get("buyWay")).append("'");
			}
			if(StringUtil.isNotBlank(map.get("supplierName"))){
				hql.append(" and si.supplierName like '%").append(map.get("supplierName")).append("%'");
			}
			if(StringUtil.isNotBlank(map.get("comId"))){
				hql.append(" and de.comId =").append(map.get("comId"));
			}
			if(StringUtil.isNotBlank(map.get("dateStart"))){
				hql.append(" and  ba.writeDate>=to_date('").append(map.get("dateStart")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(map.get("dateEnd"))){
				hql.append(" and  ba.writeDate<=to_date('").append(map.get("dateEnd")).append("','yyyy-MM-dd')");
			}
		}
		hql.append(sqlStr);
		hql.append(" order by de.rcId desc ");
		return this.getObjects(hql.toString() );
	}

	/**
	 * 更新项目节点信息
	 * @param requiredCollect
	 * @throws BaseException
	 */
	public void updateRequiredCollectNodeByRequiredCollect(
			RequiredCollect requiredCollect) throws BaseException {
		String sql="update required_collect set rc_id="+requiredCollect.getRcId();
		if(requiredCollect.getNoticePublishDate()!=null){
			sql+=",notice_publish_date=to_date('"+requiredCollect.getNoticePublishDate()+"','yyyy-mm-dd hh24:mi')";
		}
		if(requiredCollect.getBidReturnDate()!=null){
			sql+=",bid_return_date=to_date('"+requiredCollect.getBidReturnDate()+"','yyyy-mm-dd hh24:mi')";
		}
		if(requiredCollect.getBidOpenDate()!=null){
			sql+=",bid_open_date=to_date('"+requiredCollect.getBidOpenDate()+"','yyyy-mm-dd hh24:mi')";
		}
		if(requiredCollect.getBidAwardDate()!=null){
			sql+=",bid_award_date=to_date('"+requiredCollect.getBidAwardDate()+"','yyyy-mm-dd')";
		}
		if(requiredCollect.getChangeNum()!=null){
			sql+=",change_num=change_num+1";
		}
		sql+=" where rc_id="+requiredCollect.getRcId();
		
	}
}
