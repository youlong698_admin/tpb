package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BiddingBidPrice;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
/** 
 * 类名称：BidPriceBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidPriceBizImpl extends BaseBizImpl implements IBidPriceBiz  {
	
	/**
	 * 根据主键获得供应商报价信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPrice getBidPrice(Long id) throws BaseException {
		return (BidPrice)this.getObject(BidPrice.class, id);
	}
	
	/**
	 * 获得供应商报价信息表实例
	 * @param bidPrice 供应商报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidPrice getBidPrice(BidPrice bidPrice) throws BaseException {
		return (BidPrice)this.getObject(BidPrice.class, bidPrice.getBpId() );
	}
	
	/**
	 * 添加供应商报价信息信息
	 * @param bidPrice 供应商报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidPrice(BidPrice bidPrice) throws BaseException{
		this.saveObject( bidPrice ) ;
	}
	
	/**
	 * 更新供应商报价信息表实例
	 * @param bidPrice 供应商报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidPrice(BidPrice bidPrice) throws BaseException{
		this.updateObject( bidPrice ) ;
	}
	
	/**
	 * 删除供应商报价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPrice(String id) throws BaseException {
		this.removeObject( this.getBidPrice( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商报价信息表实例
	 * @param bidPrice 供应商报价信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPrice(BidPrice bidPrice) throws BaseException {
		this.removeObject( bidPrice ) ;
	}
	
	/**
	 * 删除供应商报价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidPrices(String[] id) throws BaseException {
		this.removeBatchObject(BidPrice.class, id) ;
	}
	
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPrice de where 1 = 1 " );

		hql.append(" order by de.bpId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商报价信息表数据集  关联供应商表查询
	 * @param bidPrice 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceListSupplierName(BidPrice bidPrice) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,si.supplierName from BidPrice de,SupplierInfo si where de.supplierId=si.supplierId " );
		if(bidPrice != null){
			if(StringUtil.isNotBlank(bidPrice.getRcId())){
				hql.append(" and de.rcId =").append(bidPrice.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidPrice.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidPrice.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.totalPrice ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param bidPrice 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceList(BidPrice bidPrice) throws BaseException {
		StringBuffer hql = new StringBuffer("from BidPrice de where 1=1 " );
		if(bidPrice != null){
			if(StringUtil.isNotBlank(bidPrice.getRcId())){
				hql.append(" and de.rcId =").append(bidPrice.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidPrice.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidPrice.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.totalPrice desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param bidPrice 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceListForDecrypt(BidPrice bidPrice) throws BaseException {
		StringBuffer hql = new StringBuffer("from BidPrice de where de.totalPrice is null " );
		if(bidPrice != null){
			if(StringUtil.isNotBlank(bidPrice.getRcId())){
				hql.append(" and de.rcId =").append(bidPrice.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidPrice.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidPrice.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.totalPrice desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPrice 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceList(RollPage rollPage, BidPrice bidPrice) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPrice de where 1 = 1 " );

		hql.append(" order by de.bpId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
     * 根据项目ID和供应商ID删除报价信息
     * @param rcId
     * @param supplierId
     */
	public void deleteBidPriceByRcIdAndSupplierId(Long rcId,Long supplierId)throws BaseException {
		StringBuffer hql = new StringBuffer("begin ");
		hql.append(" delete from bid_business_response where bp_id=(select bp_id from bid_price where rc_id="+rcId+" and supplier_id="+supplierId+");");
		hql.append(" delete from bid_price_detail where bp_id=(select bp_id from bid_price where rc_id="+rcId+" and supplier_id="+supplierId+");");
		hql.append(" delete from bid_price where rc_id="+rcId+" and supplier_id="+supplierId+";");
		hql.append(" end;");
		this.updateJdbcSql(hql.toString());
	}
	/**
	 * 获得所有授标表数据集
	 * @param rcId 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getBidPriceAndAwardListByRcId(Long rcId) throws BaseException {
		StringBuffer sql = new StringBuffer(" select bp.*,ba.bid_price,ba.ba_id,si.supplier_name from bid_price bp left join bid_award ba on bp.rc_id=ba.rc_id and bp.supplier_id=ba.supplier_id ");
		sql.append(" left join supplier_info si on bp.supplier_id=si.supplier_id " );
		sql.append(" where bp.rc_id= "+rcId);
		sql.append(" order by bp.bp_id desc ");
		return this.getObjectsList(BidPrice.class, sql.toString());
	}
	/**
	 * 获取单条物资供应商的报价
	 * @param supplierId
	 * @param rcId
	 * @param rcdId
	 * @return
	 * @throws BaseException
	 */		
	public double getBidPriceDetailBySupplierIdAndRcIdAndRcdId(String supplierId,String rcId,String rcdId) throws BaseException{
		StringBuffer hql = new StringBuffer(" select de.price from BidPriceDetail de,BidPrice bp  where bp.bpId=de.bpId and de.rcdId="+rcdId+" and bp.rcId="+rcId+" and de.supplierId="+supplierId+" and bp.supplierId="+supplierId);
		return (Double)this.sumOrMinOrMaxObjects(hql.toString(),Double.class);
	}

	/**
	 * 竞价查询拟中标供应商，竞买查询最低，竞卖查询最高的
	 * @param rcId
	 * @param biddingType
	 * @return
	 * @throws BaseException
	 */
	public BiddingBidPrice getWinningBidPrice(Long rcId, String biddingType)
			throws BaseException {
		BiddingBidPrice biddingBidPrice=new BiddingBidPrice();
		String sql="";
		if(biddingType.equals(BiddingStatus.BIDDING_TYPE_0)){
			sql="select de.total_price,si.supplier_name,de.supplier_id from (select bp.total_price,bp.supplier_id from bid_price bp where bp.rc_id="+rcId+" order by bp.total_price ) de,supplier_info si where de.supplier_id=si.supplier_id and rownum = 1"; 
		}else{
			sql="select de.total_price,si.supplier_name,de.supplier_id from (select bp.total_price,bp.supplier_id from bid_price bp where bp.rc_id="+rcId+" order by bp.total_price desc) de,supplier_info si where de.supplier_id=si.supplier_id and rownum = 1"; 
			}
		List list = this.getObjectsList(BiddingBidPrice.class, sql);
		if(list!=null&&list.size()>0){
			biddingBidPrice = (BiddingBidPrice)list.get(0);
		}
		return biddingBidPrice;
	}
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param bidPrice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public BidPrice getBidPriceListByBidPrice(BidPrice bidPrice) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidPrice de where 1 = 1 " );
		if(bidPrice != null){
			if(StringUtil.isNotBlank(bidPrice.getRcId())){
				hql.append(" and de.rcId =").append(bidPrice.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidPrice.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidPrice.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.bpId desc ");
		List list = this.getObjects( hql.toString() );
		bidPrice = new BidPrice();
		if(list!=null&&list.size()>0){
			bidPrice = (BidPrice)list.get(0);
		}
		return bidPrice;
	}
}
