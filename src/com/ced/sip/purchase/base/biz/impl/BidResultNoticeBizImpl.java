package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidResultNoticeBiz;
import com.ced.sip.purchase.base.entity.BidResultNotice;
import com.ced.sip.purchase.base.entity.RequiredCollect;
/** 
 * 类名称：BidResultNoticeBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidResultNoticeBizImpl extends BaseBizImpl implements IBidResultNoticeBiz  {
	
	/**
	 * 根据主键获得中标通知书表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidResultNotice getBidResultNotice(Long id) throws BaseException {
		return (BidResultNotice)this.getObject(BidResultNotice.class, id);
	}
	
	/**
	 * 获得中标通知书表实例
	 * @param bidResultNotice 中标通知书表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidResultNotice getBidResultNotice(BidResultNotice bidResultNotice) throws BaseException {
		return (BidResultNotice)this.getObject(BidResultNotice.class, bidResultNotice.getBrnId() );
	}
	
	/**
	 * 添加中标通知书信息
	 * @param bidResultNotice 中标通知书表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void saveBidResultNotice(BidResultNotice bidResultNotice) throws BaseException{
		this.saveObject( bidResultNotice ) ;
	}
	
	/**
	 * 更新中标通知书表实例
	 * @param bidResultNotice 中标通知书表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void updateBidResultNotice(BidResultNotice bidResultNotice) throws BaseException{
		this.updateObject( bidResultNotice ) ;
	}
	
	/**
	 * 删除中标通知书表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidResultNotice(String id) throws BaseException {
		this.removeObject( this.getBidResultNotice( new Long(id) ) ) ;
	}
	
	/**
	 * 删除中标通知书表实例
	 * @param bidResultNotice 中标通知书表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidResultNotice(BidResultNotice bidResultNotice) throws BaseException {
		this.removeObject( bidResultNotice ) ;
	}
	
	/**
	 * 删除中标通知书表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidResultNotices(String[] id) throws BaseException {
		this.removeBatchObject(BidResultNotice.class, id) ;
	}
	
	/**
	 * 获得中标通知书表数据集
	 * bidResultNotice 中标通知书表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidResultNotice getBidResultNoticeByBidResultNotice(BidResultNotice bidResultNotice) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidResultNotice de where 1 = 1 " );
		if(bidResultNotice != null){
			if(StringUtil.isNotBlank(bidResultNotice.getRcId())){
				hql.append(" and de.rcId =").append(bidResultNotice.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidResultNotice.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidResultNotice.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.brnId desc ");
		List list = this.getObjects( hql.toString() );
		bidResultNotice = new BidResultNotice();
		if(list!=null&&list.size()>0){
			bidResultNotice = (BidResultNotice)list.get(0);
		}
		return bidResultNotice;
	}
	
	/**
	 * 获得所有中标通知书表数据集
	 * @param bidResultNotice 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidResultNoticeList(BidResultNotice bidResultNotice) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidResultNotice de where 1 = 1 " );
		if(bidResultNotice != null){
			if(StringUtil.isNotBlank(bidResultNotice.getRcId())){
				hql.append(" and de.rcId =").append(bidResultNotice.getRcId()).append("");
			}
		}
		hql.append(" order by de.brnId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有中标通知书表数据集
	 * @param rollPage 分页对象
	 * @param bidResultNotice 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidResultNoticeList(RollPage rollPage, BidResultNotice bidResultNotice) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidResultNotice de where 1 = 1 " );
		if(bidResultNotice != null){
			if(StringUtil.isNotBlank(bidResultNotice.getRcId())){
				hql.append(" and de.rcId =").append(bidResultNotice.getRcId()).append("");
			}
		}
		hql.append(" order by de.brnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得供应商中标信息列表数据集
	 * @param rollPage 分页对象
	 * @param bidResultNotice 查询参数对象
	 * @param requiredCollect 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getSupplierBehavior(RollPage rollPage, BidResultNotice bidResultNotice,RequiredCollect requiredCollect) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,rc.bidCode,rc.buyRemark,rc.buyWay from BidResultNotice de,RequiredCollect rc where de.rcId=rc.rcId " );
		if(bidResultNotice != null){
			if(StringUtil.isNotBlank(bidResultNotice.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidResultNotice.getSupplierId()).append("");
			}
		}
		if(requiredCollect != null){
			if(StringUtil.isNotBlank(requiredCollect.getComId())){
				hql.append(" and rc.comId =").append(requiredCollect.getComId()).append("");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
}
