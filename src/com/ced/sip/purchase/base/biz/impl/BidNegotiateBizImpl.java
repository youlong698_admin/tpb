package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidNegotiateBiz;
import com.ced.sip.purchase.base.entity.BidNegotiate;
/** 
 * 类名称：BidNegotiateBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidNegotiateBizImpl extends BaseBizImpl implements IBidNegotiateBiz  {
	
	/**
	 * 根据主键获得磋商信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidNegotiate getBidNegotiate(Long id) throws BaseException {
		return (BidNegotiate)this.getObject(BidNegotiate.class, id);
	}
	
	/**
	 * 获得磋商信息表实例
	 * @param bidNegotiate 磋商信息表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidNegotiate getBidNegotiate(BidNegotiate bidNegotiate) throws BaseException {
		return (BidNegotiate)this.getObject(BidNegotiate.class, bidNegotiate.getBnId() );
	}
	
	/**
	 * 添加磋商信息信息
	 * @param bidNegotiate 磋商信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidNegotiate(BidNegotiate bidNegotiate) throws BaseException{
		this.saveObject( bidNegotiate ) ;
	}
	
	/**
	 * 更新磋商信息表实例
	 * @param bidNegotiate 磋商信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidNegotiate(BidNegotiate bidNegotiate) throws BaseException{
		this.updateObject( bidNegotiate ) ;
	}
	
	/**
	 * 删除磋商信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidNegotiate(String id) throws BaseException {
		this.removeObject( this.getBidNegotiate( new Long(id) ) ) ;
	}
	
	/**
	 * 删除磋商信息表实例
	 * @param bidNegotiate 磋商信息表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidNegotiate(BidNegotiate bidNegotiate) throws BaseException {
		this.removeObject( bidNegotiate ) ;
	}
	
	/**
	 * 删除磋商信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidNegotiates(String[] id) throws BaseException {
		this.removeBatchObject(BidNegotiate.class, id) ;
	}
	
	/**
	 * 获得所有磋商信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidNegotiateList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidNegotiate de where 1 = 1 " );

		hql.append(" order by de.bnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有磋商信息表数据集
	 * @param bidNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidNegotiateList(BidNegotiate bidNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,si.supplierName from BidNegotiate de,SupplierInfo si where de.supplierId=si.supplierId " );
		if(bidNegotiate != null){
			if(StringUtil.isNotBlank(bidNegotiate.getRcId())){
				hql.append(" and de.rcId =").append(bidNegotiate.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidNegotiate.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidNegotiate.getSupplierId()).append("");
			}
			if(StringUtil.isNotBlank(bidNegotiate.getBnId())){
				hql.append(" and de.bnId =").append(bidNegotiate.getBnId()).append("");
			}
		}
		hql.append(" order by de.bnId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有磋商信息表数据集
	 * @param bidNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidNegotiateListByBidNegotiate(BidNegotiate bidNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidNegotiate de where 1=1 " );
		if(bidNegotiate != null){
			if(StringUtil.isNotBlank(bidNegotiate.getRcId())){
				hql.append(" and de.rcId =").append(bidNegotiate.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidNegotiate.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidNegotiate.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.bnId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有磋商信息表数据集
	 * @param rollPage 分页对象
	 * @param bidNegotiate 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidNegotiateList(RollPage rollPage, BidNegotiate bidNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidNegotiate de where 1 = 1 " );

		hql.append(" order by de.bnId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得磋商信息表数据集
	 * bidNegotiate 磋商信息表实例
	 * @author luguanglei 2017-05-04
	 * @return
	 * @throws BaseException 
	 */
	public BidNegotiate getBidNegotiateByBidNegotiate(BidNegotiate bidNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidNegotiate de where 1 = 1 " );
		if(bidNegotiate != null){
			if(StringUtil.isNotBlank(bidNegotiate.getRcId())){
				hql.append(" and de.rcId =").append(bidNegotiate.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidNegotiate.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidNegotiate.getSupplierId()).append("");
			}
			if(StringUtil.isNotBlank(bidNegotiate.getBnId())){
				hql.append(" and de.bnId =").append(bidNegotiate.getBnId()).append("");
			}
		}
		hql.append(" order by de.bnId desc ");
		List list = this.getObjects( hql.toString() );
		bidNegotiate = new BidNegotiate();
		if(list!=null&&list.size()>0){
			bidNegotiate = (BidNegotiate)list.get(0);
		}
		return bidNegotiate;
	}
	/**
	 * 获得所有磋商信息表数据集  供应商所用
	 * @param bidNegotiate 查询参数对象
	 * @author luguanglei 2017-05-04
	 * @return
	 * @throws BaseException 
	 */
	public List getBidNegotiateListBySupplier(BidNegotiate bidNegotiate) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidNegotiate de where 1 = 1 " );
		if(bidNegotiate != null){
			if(StringUtil.isNotBlank(bidNegotiate.getRcId())){
				hql.append(" and de.rcId =").append(bidNegotiate.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidNegotiate.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidNegotiate.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.bnId desc ");
		return this.getObjects( hql.toString() );
	}
	
	
}
