package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.entity.InviteSupplier;
/** 
 * 类名称：InviteSupplierBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public class InviteSupplierBizImpl extends BaseBizImpl implements IInviteSupplierBiz  {
	
	/**
	 * 根据主键获得邀请供应商表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public InviteSupplier getInviteSupplier(Long id) throws BaseException {
		return (InviteSupplier)this.getObject(InviteSupplier.class, id);
	}
	
	/**
	 * 获得邀请供应商表表实例
	 * @param inviteSupplier 邀请供应商表表实例
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public InviteSupplier getInviteSupplier(InviteSupplier inviteSupplier) throws BaseException {
		return (InviteSupplier)this.getObject(InviteSupplier.class, inviteSupplier.getIsId() );
	}
	
	/**
	 * 添加邀请供应商表信息
	 * @param inviteSupplier 邀请供应商表表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void saveInviteSupplier(InviteSupplier inviteSupplier) throws BaseException{
		this.saveObject( inviteSupplier ) ;
	}
	
	/**
	 * 更新邀请供应商表表实例
	 * @param inviteSupplier 邀请供应商表表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void updateInviteSupplier(InviteSupplier inviteSupplier) throws BaseException{
		this.updateObject( inviteSupplier ) ;
	}
	
	/**
	 * 删除邀请供应商表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteInviteSupplier(String id) throws BaseException {
		this.removeObject( this.getInviteSupplier( new Long(id) ) ) ;
	}
	
	/**
	 * 删除邀请供应商表表实例
	 * @param inviteSupplier 邀请供应商表表实例
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteInviteSupplier(InviteSupplier inviteSupplier) throws BaseException {
		this.removeObject( inviteSupplier ) ;
	}
	
	/**
	 * 删除邀请供应商表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-02
	 * @throws BaseException 
	 */
	public void deleteInviteSuppliers(String[] id) throws BaseException {
		this.removeBatchObject(InviteSupplier.class, id) ;
	}
	
	/**
	 * 获得所有邀请供应商表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getInviteSupplierList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from InviteSupplier de where 1 = 1 " );

		hql.append(" order by de.isId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得邀请供应商表数据集
	 * @param inviteSupplier 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public InviteSupplier getInviteSupplierByInviteSupplier(InviteSupplier inviteSupplier) throws BaseException{
		StringBuffer hql = new StringBuffer(" from InviteSupplier de where 1 = 1 " );
		if(inviteSupplier != null){
			if(StringUtil.isNotBlank(inviteSupplier.getRcId())){
				hql.append(" and de.rcId =").append(inviteSupplier.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(inviteSupplier.getSupplierId())){
				hql.append(" and de.supplierId =").append(inviteSupplier.getSupplierId()).append("");
			}
		}
		List list = this.getObjects( hql.toString() );
		inviteSupplier = new InviteSupplier();
		if(list!=null&&list.size()>0){
			inviteSupplier = (InviteSupplier)list.get(0);
		}
		return inviteSupplier;
	}
	
	/**
	 * 获得所有邀请供应商表表数据集
	 * @param inviteSupplier 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getInviteSupplierList(InviteSupplier inviteSupplier) throws BaseException {
		StringBuffer hql = new StringBuffer(" from InviteSupplier de where 1 = 1 " );
		if(inviteSupplier != null){
			if(StringUtil.isNotBlank(inviteSupplier.getRcId())){
				hql.append(" and de.rcId =").append(inviteSupplier.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(inviteSupplier.getSupplierId())){
				hql.append(" and de.supplierId =").append(inviteSupplier.getSupplierId()).append("");
			}
			if(StringUtil.isNotBlank(inviteSupplier.getIsPriceAa())){
				hql.append(" and de.isPriceAa ='").append(inviteSupplier.getIsPriceAa()).append("'");
			}
		}
		hql.append(" order by de.isId");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有邀请供应商表表的总数
	 * @param inviteSupplier 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public int countInviteSupplierList(InviteSupplier inviteSupplier) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.isId) from InviteSupplier de where 1 = 1 " );
		if(inviteSupplier != null){
			if(StringUtil.isNotBlank(inviteSupplier.getRcId())){
				hql.append(" and de.rcId =").append(inviteSupplier.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(inviteSupplier.getSupplierId())){
				hql.append(" and de.supplierId =").append(inviteSupplier.getSupplierId()).append("");
			}
			if(StringUtil.isNotBlank(inviteSupplier.getIsPriceAa())){
				hql.append(" and de.isPriceAa ='").append(inviteSupplier.getIsPriceAa()).append("'");
			}
		}
		hql.append("");
		return this.countObjects(hql.toString() );
	}
	/**
	 * 获得所有邀请供应商表表数据集
	 * @param rollPage 分页对象
	 * @param inviteSupplier 查询参数对象
	 * @author luguanglei 2017-04-02
	 * @return
	 * @throws BaseException 
	 */
	public List getInviteSupplierList(RollPage rollPage, InviteSupplier inviteSupplier) throws BaseException {
		StringBuffer hql = new StringBuffer(" from InviteSupplier de where 1 = 1 " );
		if(inviteSupplier != null){
			if(StringUtil.isNotBlank(inviteSupplier.getRcId())){
				hql.append(" and de.rcId =").append(inviteSupplier.getRcId()).append("");
			}
		}
		hql.append(" order by de.isId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 删除邀请供应商表表实例
	 * @param rcId
	 * @throws BaseException 
	 */
	public void deleteInviteSupplierByRcId(Long rcId) throws BaseException{
		String sql="delete from invite_supplier where rc_id="+rcId;
		this.updateJdbcSql(sql);
	}


	/**
	 * 更新邀请供应商表中的是否报价字段
	 * @param rcId
	 * @param supplierId
	 * @throws BaseException 
	 */
	public void updateIsPriceAaByRcIdAndSupplierId(Long rcId, Long supplierId)
			throws BaseException {
		String sql="update invite_supplier set is_price_aa='0',price_num=price_num+1 where rc_id="+rcId+" and supplier_id="+supplierId;
		this.updateJdbcSql(sql);		
	}

	/**
	 * 更新邀请供应商表中的是否短信发送字段
	 * @param rcId
	 * @param supplierId
	 * @throws BaseException 
	 */
	public void updateIsSmsByRcIdAndSupplierId(Long rcId, Long supplierId)
			throws BaseException {
		String sql="update invite_supplier set is_sms='0' where rc_id="+rcId+" and supplier_id="+supplierId;
		this.updateJdbcSql(sql);		
	}
	/**
	 * 更新邀请供应商表中的供应商来源字段
	 * @param isId
	 * @param supplierId
	 * @param rcId
	 * @throws BaseException 
	 */
	public void updateInviteSupplierSourceCategory(Long isId,Long supplierId,Long rcId)
			throws BaseException {
		String sql="update invite_supplier set source_category=1 where is_id="+isId+"";
		this.updateJdbcSql(sql);		
	}
}
