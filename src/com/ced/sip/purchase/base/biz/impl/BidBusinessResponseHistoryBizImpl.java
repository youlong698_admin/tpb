package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseHistoryBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponseHistory;
/** 
 * 类名称：BidBusinessResponseHistoryBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BidBusinessResponseHistoryBizImpl extends BaseBizImpl implements IBidBusinessResponseHistoryBiz  {
	
	/**
	 * 根据主键获得供应商历史商务响应表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidBusinessResponseHistory getBidBusinessResponseHistory(Long id) throws BaseException {
		return (BidBusinessResponseHistory)this.getObject(BidBusinessResponseHistory.class, id);
	}
	
	/**
	 * 获得供应商历史商务响应表实例
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public BidBusinessResponseHistory getBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException {
		return (BidBusinessResponseHistory)this.getObject(BidBusinessResponseHistory.class, bidBusinessResponseHistory.getBbrhId() );
	}
	
	/**
	 * 添加供应商历史商务响应信息
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void saveBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException{
		this.saveObject( bidBusinessResponseHistory ) ;
	}
	
	/**
	 * 更新供应商历史商务响应表实例
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void updateBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException{
		this.updateObject( bidBusinessResponseHistory ) ;
	}
	
	/**
	 * 删除供应商历史商务响应表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidBusinessResponseHistory(String id) throws BaseException {
		this.removeObject( this.getBidBusinessResponseHistory( new Long(id) ) ) ;
	}
	
	/**
	 * 删除供应商历史商务响应表实例
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException {
		this.removeObject( bidBusinessResponseHistory ) ;
	}
	
	/**
	 * 删除供应商历史商务响应表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-23
	 * @throws BaseException 
	 */
	public void deleteBidBusinessResponseHistorys(String[] id) throws BaseException {
		this.removeBatchObject(BidBusinessResponseHistory.class, id) ;
	}
	
	/**
	 * 获得所有供应商历史商务响应表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBusinessResponseHistoryList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBusinessResponseHistory de where 1 = 1 " );

		hql.append(" order by de.bbrhId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有供应商历史商务响应表数据集
	 * @param bidBusinessResponseHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBusinessResponseHistoryList(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBusinessResponseHistory de where 1 = 1 " );
		if(bidBusinessResponseHistory != null){
			if(StringUtil.isNotBlank(bidBusinessResponseHistory.getBphId())){
				hql.append(" and de.bphId =").append(bidBusinessResponseHistory.getBphId()).append("");
			}
		}
		hql.append(" order by de.bbrhId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有供应商历史商务响应表数据集
	 * @param rollPage 分页对象
	 * @param bidBusinessResponseHistory 查询参数对象
	 * @author luguanglei 2017-04-23
	 * @return
	 * @throws BaseException 
	 */
	public List getBidBusinessResponseHistoryList(RollPage rollPage, BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBusinessResponseHistory de where 1 = 1 " );

		hql.append(" order by de.bbrhId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
