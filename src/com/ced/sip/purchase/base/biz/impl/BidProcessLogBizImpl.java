package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.entity.BidProcessLog;
/** 
 * 类名称：BidProcessLogBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-03
 */
public class BidProcessLogBizImpl extends BaseBizImpl implements IBidProcessLogBiz  {
	
	/**
	 * 根据主键获得项目流程记录表表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-03
	 * @return
	 * @throws BaseException 
	 */
	public BidProcessLog getBidProcessLog(Long id) throws BaseException {
		return (BidProcessLog)this.getObject(BidProcessLog.class, id);
	}
	
	/**
	 * 获得项目流程记录表表实例
	 * @param bidProcessLog 项目流程记录表表实例
	 * @author luguanglei 2017-04-03
	 * @return
	 * @throws BaseException 
	 */
	public BidProcessLog getBidProcessLog(BidProcessLog bidProcessLog) throws BaseException {
		return (BidProcessLog)this.getObject(BidProcessLog.class, bidProcessLog.getBplId() );
	}
	
	/**
	 * 添加项目流程记录表信息
	 * @param bidProcessLog 项目流程记录表表实例
	 * @author luguanglei 2017-04-03
	 * @throws BaseException 
	 */
	public void saveBidProcessLog(BidProcessLog bidProcessLog) throws BaseException{
		this.saveObject( bidProcessLog ) ;
	}
	
	/**
	 * 更新项目流程记录表表实例
	 * @param bidProcessLog 项目流程记录表表实例
	 * @author luguanglei 2017-04-03
	 * @throws BaseException 
	 */
	public void updateBidProcessLog(BidProcessLog bidProcessLog) throws BaseException{
		this.updateObject( bidProcessLog ) ;
	}
	
	/**
	 * 删除项目流程记录表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-03
	 * @throws BaseException 
	 */
	public void deleteBidProcessLog(String id) throws BaseException {
		this.removeObject( this.getBidProcessLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除项目流程记录表表实例
	 * @param bidProcessLog 项目流程记录表表实例
	 * @author luguanglei 2017-04-03
	 * @throws BaseException 
	 */
	public void deleteBidProcessLog(BidProcessLog bidProcessLog) throws BaseException {
		this.removeObject( bidProcessLog ) ;
	}
	
	/**
	 * 删除项目流程记录表表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-03
	 * @throws BaseException 
	 */
	public void deleteBidProcessLogs(String[] id) throws BaseException {
		this.removeBatchObject(BidProcessLog.class, id) ;
	}
	
	/**
	 * 获得所有项目流程记录表表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidProcessLogList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidProcessLog de where 1 = 1 " );

		hql.append(" order by de.bplId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有项目流程记录表表数据集
	 * @param bidProcessLog 查询参数对象
	 * @author luguanglei 2017-04-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidProcessLogList(BidProcessLog bidProcessLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidProcessLog de where 1 = 1 " );
		if(bidProcessLog != null){
			if(StringUtil.isNotBlank(bidProcessLog.getRcId())){
				hql.append(" and de.rcId ='").append(bidProcessLog.getRcId()).append("'");
			}
			if(StringUtil.isNotBlank(bidProcessLog.getBidNode())){
				hql.append(" and de.bidNode ="+bidProcessLog.getBidNode());
			}
		}
		hql.append(" order by de.bplId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有项目流程记录表表数据集
	 * @param rollPage 分页对象
	 * @param bidProcessLog 查询参数对象
	 * @author luguanglei 2017-04-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidProcessLogList(RollPage rollPage, BidProcessLog bidProcessLog) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidProcessLog de where 1 = 1 " );
		if(bidProcessLog != null){
			if(StringUtil.isNotBlank(bidProcessLog.getRcId())){
				hql.append(" and de.rcId ='").append(bidProcessLog.getRcId()).append("'");
			}
			if(StringUtil.isNotBlank(bidProcessLog.getBidNode())){
				hql.append(" and de.bidNode ="+bidProcessLog.getBidNode());
			}
		}
		hql.append(" order by de.bplId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有标段流程记录表数据集
	 * @param bidProcessLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public BidProcessLog getBidProcessLogByRcIdAndBidNode(  BidProcessLog bidProcessLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidProcessLog de where 1 = 1 " );
		if(bidProcessLog != null){
			if(StringUtil.isNotBlank(bidProcessLog.getRcId())){
				hql.append(" and de.rcId ='").append(bidProcessLog.getRcId()).append("'");
			}
			if(StringUtil.isNotBlank(bidProcessLog.getBidNode())){
				hql.append(" and de.bidNode ="+bidProcessLog.getBidNode());
			}
		}
		List<BidProcessLog> bgList = this.getObjects( hql.toString() );
		if(bgList.size()>0) bidProcessLog = bgList.get(0);
		
		return bidProcessLog;
	}
}
