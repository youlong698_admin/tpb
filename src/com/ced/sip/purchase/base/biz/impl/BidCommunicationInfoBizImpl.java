package com.ced.sip.purchase.base.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidCommunicationInfoBiz;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
/** 
 * 类名称：BidCommunicationInfoBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidCommunicationInfoBizImpl extends BaseBizImpl implements IBidCommunicationInfoBiz  {
	
	/**
	 * 根据主键获得交流信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public BidCommunicationInfo getBidCommunicationInfo(Long id) throws BaseException {
		return (BidCommunicationInfo)this.getObject(BidCommunicationInfo.class, id);
	}
	
	/**
	 * 获得交流信息表实例
	 * @param bidCommunicationInfo 交流信息表实例
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public BidCommunicationInfo getBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException {
		return (BidCommunicationInfo)this.getObject(BidCommunicationInfo.class, bidCommunicationInfo.getBciId() );
	}
	
	/**
	 * 添加交流信息信息
	 * @param bidCommunicationInfo 交流信息表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void saveBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException{
		this.saveObject( bidCommunicationInfo ) ;
	}
	
	/**
	 * 更新交流信息表实例
	 * @param bidCommunicationInfo 交流信息表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void updateBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException{
		this.updateObject( bidCommunicationInfo ) ;
	}
	
	/**
	 * 删除交流信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteBidCommunicationInfo(String id) throws BaseException {
		this.removeObject( this.getBidCommunicationInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除交流信息表实例
	 * @param bidCommunicationInfo 交流信息表实例
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException {
		this.removeObject( bidCommunicationInfo ) ;
	}
	
	/**
	 * 删除交流信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-04-07
	 * @throws BaseException 
	 */
	public void deleteBidCommunicationInfos(String[] id) throws BaseException {
		this.removeBatchObject(BidCommunicationInfo.class, id) ;
	}
	
	/**
	 * 获得所有交流信息表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getBidCommunicationInfoList(RollPage rollPage) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidCommunicationInfo de where 1 = 1 " );

		hql.append(" order by de.bciId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有交流信息表数据集
	 * @param bidCommunicationInfo 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getBidCommunicationInfoList(BidCommunicationInfo bidCommunicationInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidCommunicationInfo de where 1 = 1 " );
		if(bidCommunicationInfo != null){
			if(StringUtil.isNotBlank(bidCommunicationInfo.getRcId())){
				hql.append(" and de.rcId =").append(bidCommunicationInfo.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidCommunicationInfo.getQuestionerId())){
				hql.append(" and de.questionerId =").append(bidCommunicationInfo.getQuestionerId()).append("");
			}
			if(StringUtil.isNotBlank(bidCommunicationInfo.getAnswerId())){
				hql.append(" and de.answerId =").append(bidCommunicationInfo.getAnswerId()).append("");
			}
		}
		hql.append(" order by de.bciId desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得未读消息交流数
	 * @param bidCommunicationInfo 查询参数对象
	 * @author luguanglei 2017-08-24
	 * @return
	 * @throws BaseException 
	 */
	public int countBidCommunicationInfoList(BidCommunicationInfo bidCommunicationInfo,int sign) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.bciId) from BidCommunicationInfo de where 1=1 " );
		if(bidCommunicationInfo != null){
			if(StringUtil.isNotBlank(bidCommunicationInfo.getRcId())){
				hql.append(" and de.rcId =").append(bidCommunicationInfo.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidCommunicationInfo.getQuestionerId())){
				hql.append(" and de.questionerId =").append(bidCommunicationInfo.getQuestionerId()).append("");
			}
			if(StringUtil.isNotBlank(bidCommunicationInfo.getAnswerId())){
				hql.append(" and de.answerId =").append(bidCommunicationInfo.getAnswerId()).append("");
			}
		}
		if(sign==1){
			hql.append(" and de.answerId is null ");
		}else if(sign==2){
			hql.append(" and de.readStatus='1' and de.answerId is not null ");
		}
		return this.countObjects( hql.toString() );
	}
	/**
	 * 获得所有交流信息表数据集
	 * @param rollPage 分页对象
	 * @param bidCommunicationInfo 查询参数对象
	 * @author luguanglei 2017-04-07
	 * @return
	 * @throws BaseException 
	 */
	public List getBidCommunicationInfoList(RollPage rollPage, BidCommunicationInfo bidCommunicationInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidCommunicationInfo de where 1 = 1 " );
		if(bidCommunicationInfo != null){
			if(StringUtil.isNotBlank(bidCommunicationInfo.getRcId())){
				hql.append(" and de.rcId =").append(bidCommunicationInfo.getRcId()).append("");
			}
		}
		hql.append(" order by de.bciId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
