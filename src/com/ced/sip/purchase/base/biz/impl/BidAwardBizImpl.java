package com.ced.sip.purchase.base.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.analysis.entity.PurchaseAmount;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.entity.BidAward;
/** 
 * 类名称：BidAwardBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidAwardBizImpl extends BaseBizImpl implements IBidAwardBiz  {
	
	/**
	 * 根据主键获得授标表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidAward getBidAward(Long id) throws BaseException {
		return (BidAward)this.getObject(BidAward.class, id);
	}
	
	/**
	 * 获得授标表实例
	 * @param bidAward 授标表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidAward getBidAward(BidAward bidAward) throws BaseException {
		return (BidAward)this.getObject(BidAward.class, bidAward.getBaId() );
	}
	
	/**
	 * 添加授标信息
	 * @param bidAward 授标表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void saveBidAward(BidAward bidAward) throws BaseException{
		this.saveObject( bidAward ) ;
	}
	
	/**
	 * 更新授标表实例
	 * @param bidAward 授标表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void updateBidAward(BidAward bidAward) throws BaseException{
		this.updateObject( bidAward ) ;
	}
	
	/**
	 * 删除授标表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidAward(String id) throws BaseException {
		this.removeObject( this.getBidAward( new Long(id) ) ) ;
	}
	
	/**
	 * 删除授标表实例
	 * @param bidAward 授标表实例
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidAward(BidAward bidAward) throws BaseException {
		this.removeObject( bidAward ) ;
	}
	
	/**
	 * 删除授标表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-03
	 * @throws BaseException 
	 */
	public void deleteBidAwards(String[] id) throws BaseException {
		this.removeBatchObject(BidAward.class, id) ;
	}
	
	/**
	 * 获得授标表数据集
	 * bidAward 授标表实例
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public BidAward getBidAwardByBidAward(BidAward bidAward) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAward de where 1 = 1 " );

		hql.append(" order by de.baId desc ");
		List list = this.getObjects( hql.toString() );
		bidAward = new BidAward();
		if(list!=null&&list.size()>0){
			bidAward = (BidAward)list.get(0);
		}
		return bidAward;
	}
	
	/**
	 * 获得所有授标表数据集
	 * @param bidAward 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAwardList(BidAward bidAward) throws BaseException {
		StringBuffer hql = new StringBuffer("select de,si.supplierName,si.mobilePhone,si.contactEmail from BidAward de,SupplierInfo si where de.supplierId=si.supplierId " );
		if(bidAward != null){
			if(StringUtil.isNotBlank(bidAward.getRcId())){
				hql.append(" and de.rcId =").append(bidAward.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(bidAward.getSupplierId())){
				hql.append(" and de.supplierId =").append(bidAward.getSupplierId()).append("");
			}
		}
		hql.append(" order by de.baId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有授标表数据集
	 * @param rollPage 分页对象
	 * @param bidAward 查询参数对象
	 * @author luguanglei 2017-05-03
	 * @return
	 * @throws BaseException 
	 */
	public List getBidAwardList(RollPage rollPage, BidAward bidAward) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidAward de where 1 = 1 " );

		hql.append(" order by de.baId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 删除授标实例
	 * @param rcId
	 * @throws BaseException 
	 */
	public void deleteBidAwardByRcId(Long rcId) throws BaseException{
		String sql="begin delete from bid_award_detail where ba_id in (select ba_id from bid_award where rc_id="+rcId+");delete from bid_award where rc_id="+rcId+";end;";
		this.updateJdbcSql(sql);
	}
	/**
	 * 更新bidAward表中的bid_price
	 * @param bidAward 清单表实例
	 * @throws BaseException 
	 */
	public void updateBidAwardBidPrice(Long rcId) throws BaseException{
		StringBuffer sql = new StringBuffer("update bid_award  t SET t.bid_price=(select round(sum(bad.award_amount*bad.Price),2) from bid_award_detail bad where bad.ba_id=t.ba_id ) where t.rc_id="+rcId+"" );
		this.updateJdbcSql(sql.toString());
		
		sql = new StringBuffer("update required_collect  t SET t.bid_winning_price=(select sum(bid_price) from bid_award  where rc_id="+rcId+") where t.rc_id="+rcId+"" );
		this.updateJdbcSql(sql.toString());
		/*
		sql = new StringBuffer("update invite_supplier t set  t.is_win_biding='0' where exists (select ba.supplier_id from bid_award ba where ba.supplier_id=t.supplier_id and ba.rc_id="+rcId+") and t.rc_id="+rcId+" ");
		this.updateJdbcSql(sql.toString());
		
		sql = new StringBuffer("update invite_supplier t set  t.is_win_biding='1' where not exists (select ba.supplier_id from bid_award ba where ba.supplier_id=t.supplier_id and ba.rc_id="+rcId+") and t.rc_id="+rcId+"  ");
		this.updateJdbcSql(sql.toString());*/
	}
	/**
	 * 查询采购方式对比分析信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public List getPurchseWayList(String sqlStr) throws BaseException {
		StringBuffer hql = new StringBuffer(" select ba.buy_way,trim(to_char(nvl(SUM(ba.bid_price),0),999999999999.99)) sumPrice from bid_award ba where 1=1 ");
	    hql.append(sqlStr);
		hql.append(" group by ba.buy_way ");

		return this.getObjectsList(BidAward.class, hql.toString()); 
	}

	/**
	 * 授标之后更新物料库中的最新采购价信息
	 * @param rcId
	 * @param comId
	 * @throws BaseException
	 */
	public void updateMaterialListPrice(Long rcId)
			throws BaseException {
		String sql=" update material_list t set t.price=(select price from bid_award_detail bad,required_collect_detail rcd where bad.rcd_id=rcd.rcd_id and rcd.material_id=t.material_id and rcd.rc_id="+rcId+") where exists (select price from bid_award_detail bad,required_collect_detail rcd where bad.rcd_id=rcd.rcd_id and rcd.material_id=t.material_id and rcd.rc_id="+rcId+")";
		this.updateJdbcSql(sql);
	}

	/**
	 * 依据条件和授标ID更新授标明细表
	 * @param param
	 * @param baId
	 * @throws BaseException
	 */
	public void updateBidAwardByOrder(String param, Long baId)
			throws BaseException {
		String sql="begin update bid_award_detail t set t.is_order=0 where "+param+";update bid_award t set t.is_order=0 where not exists(select ba_id from bid_award_detail where is_order=1 and ba_id="+baId+") and ba_id="+baId+";end;";
		this.updateJdbcSql(sql);
		
	}
	/**
	 * 审批通过或者执行下一步时候更新授标状态
	 * @param rcId
	 * @throws BaseException
	 */
	public void updateBidAwardStatus(Long rcId)
			throws BaseException {
		String sql=" update bid_award t set t.status='0' where t.rc_id="+rcId+"";
		this.updateJdbcSql(sql);
	}
	/**
	 * 获得成交供应商基本信息表数据集总数
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  int countBidAwardSupplier(Map<String,Object> mapParams) throws BaseException {		
		StringBuffer hql = new StringBuffer("select count(distinct de.supplierId) from BidAward de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		hql.append(" and de.status = '0'");
		return this.countObjects(hql.toString() );
	}
	/**
	 * 获得采购总金额
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  double sumPurchaseMoney(Map<String,Object> mapParams,String sqlStr) throws BaseException {		
		StringBuffer hql = new StringBuffer("select sum(de.bidPrice) from BidAward de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		hql.append(" and de.status = '0'");
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		return (Double)this.sumOrMinOrMaxObjects(hql.toString(),Double.class);
	}
	/**
	 * 获得采购项目量
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  int countPurchaseNumber(Map<String,Object> mapParams,String sqlStr) throws BaseException {		
		StringBuffer hql = new StringBuffer("select count(distinct de.rcId) from BidAward de where 1 = 1 " );
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				hql.append(" and  to_char(de.writeDate,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		hql.append(" and de.status = '0'");
		if (StringUtil.isNotBlank(sqlStr)) {
			hql.append(" "+sqlStr+" ");
		}
		return this.countObjects(hql.toString());
	}
	/**
	 * 获得节资总金额
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public  double sumEconomyMoney(Map<String,Object> mapParams,String sqlStr) throws BaseException {		
		StringBuffer sql = new StringBuffer("select sum(rcd.estimate_price-bad.price) amountMoney from bid_award_detail bad,required_collect_detail rcd, bid_award ba ");
		sql.append("where bad.rcd_id=rcd.rcd_id and ba.ba_id=bad.ba_id and rcd.estimate_price>0 and ba.status='0' ");
		if(StringUtil.isNotBlank(mapParams)){
			if(StringUtil.isNotBlank(mapParams.get("startDate"))){
				sql.append(" and  to_char(ba.write_date,'yyyy-MM-dd')").append(">='").append(mapParams.get("startDate")).append("'");
			}
			if(StringUtil.isNotBlank(mapParams.get("endDate"))){
				sql.append(" and  to_char(ba.write_date,'yyyy-MM-dd')").append("<='").append(mapParams.get("endDate")).append("'");
			}
		}
		if (StringUtil.isNotBlank(sqlStr)) {
			sql.append(" "+sqlStr+" ");
		}
		return (Double)this.sumOrMinOrMaxJdbcSql(sql.toString(),Double.class);
	}
}
