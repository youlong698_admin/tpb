package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidClarifyResponse;
/** 
 * 类名称：IBidClarifyResponseBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-08
 */
public interface IBidClarifyResponseBiz {

	/**
	 * 根据主键获得标前澄清响应表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidClarifyResponse getBidClarifyResponse(Long id) throws BaseException;

	/**
	 * 添加标前澄清响应信息
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @throws BaseException 
	 */
	abstract void saveBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException;

	/**
	 * 更新标前澄清响应表实例
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @throws BaseException 
	 */
	abstract void updateBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException;

	/**
	 * 删除标前澄清响应表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidClarifyResponse(String id) throws BaseException;

	/**
	 * 删除标前澄清响应表实例
	 * @param bidClarifyResponse 标前澄清响应表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidClarifyResponse(BidClarifyResponse bidClarifyResponse) throws BaseException;

	/**
	 * 删除标前澄清响应表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidClarifyResponses(String[] id) throws BaseException;

	/**
	 * 获得所有标前澄清响应表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidClarifyResponseList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有标前澄清响应表数据集
	 * @param bidClarifyResponse 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidClarifyResponseList(BidClarifyResponse bidClarifyResponse) throws BaseException ;
	
	/**
	 * 获得所有标前澄清响应表数据集
	 * @param rollPage 分页对象
	 * @param bidClarifyResponse 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidClarifyResponseList(RollPage rollPage, BidClarifyResponse bidClarifyResponse)
			throws BaseException;
	/**
	 * 删除标前澄清响应表实例
	 * @param bcId
	 * @throws BaseException 
	 */
	abstract void deleteBidClarifyResponseByBcId(Long bcId) throws BaseException;
}