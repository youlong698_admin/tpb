package com.ced.sip.purchase.base.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidBulletin;
/** 
 * 类名称：IBidBulletinBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-05
 */
public interface IBidBulletinBiz {

	/**
	 * 根据主键获得公告表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidBulletin getBidBulletin(Long id) throws BaseException;

	/**
	 * 添加公告表信息
	 * @param bidBulletin 公告表表实例
	 * @throws BaseException 
	 */
	abstract void saveBidBulletin(BidBulletin bidBulletin) throws BaseException;

	/**
	 * 更新公告表表实例
	 * @param bidBulletin 公告表表实例
	 * @throws BaseException 
	 */
	abstract void updateBidBulletin(BidBulletin bidBulletin) throws BaseException;

	/**
	 * 删除公告表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidBulletin(String id) throws BaseException;

	/**
	 * 删除公告表表实例
	 * @param bidBulletin 公告表表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidBulletin(BidBulletin bidBulletin) throws BaseException;

	/**
	 * 删除公告表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidBulletins(String[] id) throws BaseException;

	/**
	 * 获得所有公告表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBulletinList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有公告表表数据集
	 * @param bidBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBulletinList(BidBulletin bidBulletin) throws BaseException ;
	
	/**
	 * 获得所有公告表表数据集
	 * @param rollPage 分页对象
	 * @param bidBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBulletinList(RollPage rollPage, BidBulletin bidBulletin)
			throws BaseException;

	/**
	 * 获得所有公告表表数据集
	 * @param bidBulletin 查询参数对象  type3触屏版首页
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBulletinList(RollPage rollPage, BidBulletin bidBulletin,int type) throws BaseException ;
	/**
	 * 获得所有公告表表总数数据集
	 * @param rollPage 分页对象
	 * @param bidBulletin 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int countBidBulletinList(BidBulletin bidBulletin,int type)
			throws BaseException;
	/**
	 * 获得所有公告表表数据集   触屏版项目信息搜索用
	 * @param bidBulletin 查询参数对象  type3触屏版首页
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBulletinListByWebApp(RollPage rollPage, BidBulletin bidBulletin,Map<String,Object> map) throws BaseException ;
}