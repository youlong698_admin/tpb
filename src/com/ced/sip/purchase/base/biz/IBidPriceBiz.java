package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BiddingBidPrice;
/** 
 * 类名称：IBidPriceBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidPriceBiz {

	/**
	 * 根据主键获得供应商报价信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidPrice getBidPrice(Long id) throws BaseException;

	/**
	 * 添加供应商报价信息信息
	 * @param bidPrice 供应商报价信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBidPrice(BidPrice bidPrice) throws BaseException;

	/**
	 * 更新供应商报价信息表实例
	 * @param bidPrice 供应商报价信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBidPrice(BidPrice bidPrice) throws BaseException;

	/**
	 * 删除供应商报价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPrice(String id) throws BaseException;

	/**
	 * 删除供应商报价信息表实例
	 * @param bidPrice 供应商报价信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidPrice(BidPrice bidPrice) throws BaseException;

	/**
	 * 删除供应商报价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPrices(String[] id) throws BaseException;

	/**
	 * 获得所有供应商报价信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商报价信息表数据集 关联供应商表查询
	 * @param bidPrice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceListSupplierName(BidPrice bidPrice) throws BaseException ;
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param bidPrice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceList(BidPrice bidPrice) throws BaseException ;
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param bidPrice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceListForDecrypt(BidPrice bidPrice) throws BaseException ;
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPrice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceList(RollPage rollPage, BidPrice bidPrice)
			throws BaseException;
    /**
     * 根据项目ID和供应商ID删除报价信息
     * @param rcId
     * @param supplierId
     */
	abstract void deleteBidPriceByRcIdAndSupplierId(Long rcId,Long supplierId) throws BaseException; 
	/**
	 * 获得所有授标表数据集
	 * @param rcId 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceAndAwardListByRcId(Long rcId) throws BaseException ;
	/**
	 * 获取单条物资供应商的报价
	 * @param supplierId
	 * @param rcId
	 * @param rcdId
	 * @return
	 * @throws BaseException
	 */	
	abstract double getBidPriceDetailBySupplierIdAndRcIdAndRcdId(String supplierId,String rcId,String rcdId) throws BaseException;
	/**
	 * 竞价查询拟中标供应商，竞买查询最低，竞卖查询最高的
	 * @param rcId
	 * @param biddingType
	 * @return
	 * @throws BaseException
	 */
	abstract BiddingBidPrice getWinningBidPrice(Long rcId,String biddingType) throws BaseException;
	/**
	 * 获得所有供应商报价信息表数据集
	 * @param bidPrice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract BidPrice getBidPriceListByBidPrice(BidPrice bidPrice) throws BaseException ;
}