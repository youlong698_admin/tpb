package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidNegotiate;
/** 
 * 类名称：IBidNegotiateBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidNegotiateBiz {

	/**
	 * 根据主键获得磋商信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidNegotiate getBidNegotiate(Long id) throws BaseException;

	/**
	 * 添加磋商信息信息
	 * @param bidNegotiate 磋商信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBidNegotiate(BidNegotiate bidNegotiate) throws BaseException;

	/**
	 * 更新磋商信息表实例
	 * @param bidNegotiate 磋商信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBidNegotiate(BidNegotiate bidNegotiate) throws BaseException;

	/**
	 * 删除磋商信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidNegotiate(String id) throws BaseException;

	/**
	 * 删除磋商信息表实例
	 * @param bidNegotiate 磋商信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidNegotiate(BidNegotiate bidNegotiate) throws BaseException;

	/**
	 * 删除磋商信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidNegotiates(String[] id) throws BaseException;

	/**
	 * 获得所有磋商信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidNegotiateList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有磋商信息表数据集
	 * @param bidNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidNegotiateList(BidNegotiate bidNegotiate) throws BaseException ;

	/**
	 * 获得所有磋商信息表数据集
	 * @param bidNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidNegotiateListByBidNegotiate(BidNegotiate bidNegotiate) throws BaseException ;
	
	/**
	 * 获得所有磋商信息表数据集
	 * @param rollPage 分页对象
	 * @param bidNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidNegotiateList(RollPage rollPage, BidNegotiate bidNegotiate)
			throws BaseException;
	/**
	 * 获得所有磋商信息表数据集  供应商所用
	 * @param bidNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidNegotiateListBySupplier(BidNegotiate bidNegotiate) throws BaseException ;
	
}