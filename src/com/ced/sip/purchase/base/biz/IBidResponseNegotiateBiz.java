package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidResponseNegotiate;
/** 
 * 类名称：IBidResponseNegotiateBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidResponseNegotiateBiz {

	/**
	 * 根据主键获得商务响应项磋商表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidResponseNegotiate getBidResponseNegotiate(Long id) throws BaseException;

	/**
	 * 添加商务响应项磋商信息
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @throws BaseException 
	 */
	abstract void saveBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException;

	/**
	 * 更新商务响应项磋商表实例
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @throws BaseException 
	 */
	abstract void updateBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException;

	/**
	 * 删除商务响应项磋商表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidResponseNegotiate(String id) throws BaseException;

	/**
	 * 删除商务响应项磋商表实例
	 * @param bidResponseNegotiate 商务响应项磋商表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidResponseNegotiate(BidResponseNegotiate bidResponseNegotiate) throws BaseException;

	/**
	 * 删除商务响应项磋商表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidResponseNegotiates(String[] id) throws BaseException;

	/**
	 * 获得所有商务响应项磋商表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidResponseNegotiateList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有商务响应项磋商表数据集
	 * @param bidResponseNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidResponseNegotiateList(BidResponseNegotiate bidResponseNegotiate) throws BaseException ;
	
	/**
	 * 获得所有商务响应项磋商表数据集
	 * @param rollPage 分页对象
	 * @param bidResponseNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidResponseNegotiateList(RollPage rollPage, BidResponseNegotiate bidResponseNegotiate)
			throws BaseException;

}