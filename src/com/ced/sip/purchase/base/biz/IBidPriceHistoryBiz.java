package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceHistory;
/** 
 * 类名称：IBidPriceHistoryBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidPriceHistoryBiz {

	/**
	 * 根据主键获得供应商历史报价信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidPriceHistory getBidPriceHistory(Long id) throws BaseException;

	/**
	 * 添加供应商历史报价信息信息
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException;

	/**
	 * 更新供应商历史报价信息表实例
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException;

	/**
	 * 删除供应商历史报价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceHistory(String id) throws BaseException;

	/**
	 * 删除供应商历史报价信息表实例
	 * @param bidPriceHistory 供应商历史报价信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceHistory(BidPriceHistory bidPriceHistory) throws BaseException;

	/**
	 * 删除供应商历史报价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceHistorys(String[] id) throws BaseException;

	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceHistoryList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param bidPriceHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceHistoryList(BidPriceHistory bidPriceHistory) throws BaseException ; 

	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param bidPriceHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceHistoryListForDecrypt(BidPriceHistory bidPriceHistory) throws BaseException ;
	/**
	 * 获得所有供应商历史报价信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceHistoryList(RollPage rollPage, BidPriceHistory bidPriceHistory)
			throws BaseException;
	/**
	 * 竞价查询每轮次的竞价信息，竞买查询最低，竞卖查询最高的
	 * @param rcId
	 * @param biddingType
	 * @return
	 * @throws BaseException
	 */
	abstract List getBidPriceGroupByBiddingRound(Long rcId,String biddingType) throws BaseException;
	/**
	 * 竞价图标分析查询
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	abstract List getBidPriceHistoryByBddingChart(Long rcId) throws BaseException;

}