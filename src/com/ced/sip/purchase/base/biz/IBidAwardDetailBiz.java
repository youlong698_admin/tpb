package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidAwardDetail;
/** 
 * 类名称：IBidAwardDetailBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public interface IBidAwardDetailBiz {

	/**
	 * 根据主键获得授标结果明细表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidAwardDetail getBidAwardDetail(Long id) throws BaseException;

	/**
	 * 添加授标结果明细信息
	 * @param bidAwardDetail 授标结果明细表实例
	 * @throws BaseException 
	 */
	abstract void saveBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException;

	/**
	 * 更新授标结果明细表实例
	 * @param bidAwardDetail 授标结果明细表实例
	 * @throws BaseException 
	 */
	abstract void updateBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException;

	/**
	 * 删除授标结果明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidAwardDetail(String id) throws BaseException;

	/**
	 * 删除授标结果明细表实例
	 * @param bidAwardDetail 授标结果明细表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException;

	/**
	 * 删除授标结果明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidAwardDetails(String[] id) throws BaseException;

	/**
	 * 获得授标结果明细表数据集
	 * bidAwardDetail 授标结果明细表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BidAwardDetail getBidAwardDetailByBidAwardDetail(BidAwardDetail bidAwardDetail) throws BaseException ;
	
	/**
	 * 获得所有授标结果明细表数据集
	 * @param bidAwardDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAwardDetailList(BidAwardDetail bidAwardDetail) throws BaseException ;
	
	/**
	 * 获得所有授标结果明细表数据集
	 * @param rollPage 分页对象
	 * @param bidAwardDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAwardDetailList(RollPage rollPage, BidAwardDetail bidAwardDetail)
			throws BaseException;
	/**
	 * 获得所有供应商授标明细信息表数据集  关联RequiredCollectDetail表
	 * @param bidAwardDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAwardDetailListRequiredCollectDetail(BidAwardDetail bidAwardDetail) throws BaseException ;
	
}