package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
/** 
 * 类名称：IBusinessResponseItemBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public interface IBusinessResponseItemBiz {

	/**
	 * 根据主键获得商务响应项表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BusinessResponseItem getBusinessResponseItem(Long id) throws BaseException;

	/**
	 * 添加商务响应项表信息
	 * @param businessResponseItem 商务响应项表表实例
	 * @throws BaseException 
	 */
	abstract void saveBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException;

	/**
	 * 更新商务响应项表表实例
	 * @param businessResponseItem 商务响应项表表实例
	 * @throws BaseException 
	 */
	abstract void updateBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException;

	/**
	 * 删除商务响应项表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBusinessResponseItem(String id) throws BaseException;

	/**
	 * 删除商务响应项表表实例
	 * @param businessResponseItem 商务响应项表表实例
	 * @throws BaseException 
	 */
	abstract void deleteBusinessResponseItem(BusinessResponseItem businessResponseItem) throws BaseException;

	/**
	 * 删除商务响应项表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBusinessResponseItems(String[] id) throws BaseException;

	/**
	 * 获得所有商务响应项表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBusinessResponseItemList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有商务响应项表表数据集
	 * @param businessResponseItem 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBusinessResponseItemList(BusinessResponseItem businessResponseItem) throws BaseException ;
	
	/**
	 * 获得所有商务响应项表表数据集
	 * @param rollPage 分页对象
	 * @param businessResponseItem 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBusinessResponseItemList(RollPage rollPage, BusinessResponseItem businessResponseItem)
			throws BaseException;

	/**
	 * 删除商务响应项表表实例
	 * @param rcId
	 * @throws BaseException 
	 */
	abstract void deleteBusinessResponseItemByRcId(Long rcId) throws BaseException;
}