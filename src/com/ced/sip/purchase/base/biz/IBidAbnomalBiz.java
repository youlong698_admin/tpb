package com.ced.sip.purchase.base.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidAbnomal;

public interface IBidAbnomalBiz {

	/**
	 * 根据主键获得采购异常表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidAbnomal getBidAbnomal(Long id) throws BaseException;

	/**
	 * 添加采购异常表信息
	 * @param BidAbnomal 采购异常表实例
	 * @throws BaseException 
	 */
	abstract void saveBidAbnomal(BidAbnomal bidAbnomal) throws BaseException;

	/**
	 * 更新采购异常表实例
	 * @param BidAbnomal 采购异常表实例
	 * @throws BaseException 
	 */
	abstract void updateBidAbnomal(BidAbnomal bidAbnomal) throws BaseException;

	/**
	 * 删除采购异常表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidAbnomal(String id) throws BaseException;

	/**
	 * 删除采购异常表实例
	 * @param BidAbnomal采购异常表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidAbnomal(BidAbnomal bidAbnomal) throws BaseException;

	/**
	 * 删除采购异常表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidAbnomals(String[] id) throws BaseException;

	/**
	 * 获得所有采购异常表数据集
	 * @param BidAbnomal 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAbnomalList(  BidAbnomal bidAbnomal ) throws BaseException ;
	
	/**
	 * 获得所有采购异常表数据集
	 * @param rollPage 分页对象
	 * @param BidAbnomal 查询参数对象
	 * @param sql 权限sql
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAbnomalList(RollPage rollPage, BidAbnomal bidAbnomal)
			throws BaseException;

	/**
	 * 获得所有异常采购采购项目信息
	 * @param rollPage 分页对象
	 * @param map 查询参数对象
	 * @author lgl 2017-10-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getAbnomalBidList(RollPage rollPage,
			Map<String,Object> map,String sqlStr) throws BaseException;
	

	/**
	 * 获得所有异常采购采购项目信息
	 * @param rollPage 分页对象
	 * @param map 查询参数对象
	 * @author lgl 2017-10-20
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getAbnomalBidListAll(Map<String,Object> map,String sqlStr) throws BaseException;
	/**
	 * 获得异常采购项目量
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract int countBidAbnomal(Map<String,Object> mapParams,String sqlStr)
	         throws BaseException;
	
}
