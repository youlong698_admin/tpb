package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
/** 
 * 类名称：IBidBusinessResponseBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidBusinessResponseBiz {

	/**
	 * 根据主键获得供应商商务响应表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidBusinessResponse getBidBusinessResponse(Long id) throws BaseException;

	/**
	 * 添加供应商商务响应信息
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @throws BaseException 
	 */
	abstract void saveBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException;

	/**
	 * 更新供应商商务响应表实例
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @throws BaseException 
	 */
	abstract void updateBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException;

	/**
	 * 删除供应商商务响应表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidBusinessResponse(String id) throws BaseException;

	/**
	 * 删除供应商商务响应表实例
	 * @param bidBusinessResponse 供应商商务响应表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidBusinessResponse(BidBusinessResponse bidBusinessResponse) throws BaseException;

	/**
	 * 删除供应商商务响应表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidBusinessResponses(String[] id) throws BaseException;

	/**
	 * 获得所有供应商商务响应表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBusinessResponseList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商商务响应表数据集
	 * @param bidBusinessResponse 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBusinessResponseList(BidBusinessResponse bidBusinessResponse) throws BaseException ;
	
	/**
	 * 获得所有供应商商务响应表数据集
	 * @param rollPage 分页对象
	 * @param bidBusinessResponse 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBusinessResponseList(RollPage rollPage, BidBusinessResponse bidBusinessResponse)
			throws BaseException;
	/**
	 * 获取供应商商务响应比对数据集
	 * @param bpIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	abstract List getBidBusinessResponseForParityPrice(Long[] bpIdStr,Long rcId) throws BaseException;
}