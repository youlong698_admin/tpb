package com.ced.sip.purchase.base.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;

public interface IRequiredCollectBiz {
	
	/**
	 * 获取当年最大流水号
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract int getMaxFloatCodeByPrefixStr(String prefixStr) throws BaseException;
	
	/**
	 * 根据主键获得汇总分包表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract RequiredCollect getRequiredCollect(Long id) throws BaseException;

	/**
	 * 添加汇总分包信息
	 * @param requiredCollect 汇总分包表实例
	 * @throws BaseException 
	 */
	abstract void saveRequiredCollect(RequiredCollect requiredCollect) throws BaseException;

	/**
	 * 更新汇总分包表实例
	 * @param requiredCollect 汇总分包表实例
	 * @throws BaseException 
	 */
	abstract void updateRequiredCollect(RequiredCollect requiredCollect) throws BaseException;

	/**
	 * 删除汇总分包表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollect(String id) throws BaseException;

	/**
	 * 删除汇总分包表实例
	 * @param requiredCollect 汇总分包表实例
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollect(RequiredCollect requiredCollect) throws BaseException;

	/**
	 * 删除汇总分包表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollects(String[] id) throws BaseException;
	
	/**
	 * 获得汇总分包表数据（对象）
	 * @param requiredCollect 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract RequiredCollect getRequiredCollectInfo(  RequiredCollect requiredCollect ) throws BaseException;
	/**
	 * 获得所有汇总分包表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectList( RollPage rollPage,RequiredCollect requiredCollect) throws BaseException ;

	/**
	 * 获得所有汇总分包表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectList( RollPage rollPage,RequiredCollect requiredCollect,String sqlStr) throws BaseException ;
	/**
	 * 获得所有汇总分包表数据集  开标管理员查看招标项目信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListByBidOpenAdmin( RollPage rollPage,RequiredCollect requiredCollect,Long openBidUserId) throws BaseException ;
	/**
	 * 获得所有汇总分包表数据集  竞价管理员查看竞价项目项目信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListByBiddingAdmin( RollPage rollPage,RequiredCollect requiredCollect,Long biddingUserId) throws BaseException ;
	
	/**
	 * 获得所有汇总分包表数据集
	 * @param requiredCollect 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectList(  RequiredCollect requiredCollect ) throws BaseException ;
	
	/**
	 * 根据主键获得汇总分包明细表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract RequiredCollectDetail getRequiredCollectDetail(Long id) throws BaseException;

	/**
	 * 添加汇总分包明细信息
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @throws BaseException 
	 */
	abstract void saveRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) throws BaseException;

	/**
	 * 更新汇总分包明细表实例
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @throws BaseException 
	 */
	abstract void updateRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) throws BaseException;

	/**
	 * 删除汇总分包明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollectDetail(String id) throws BaseException;

	/**
	 * 删除汇总分包明细表实例
	 * @param requiredCollectDetail 汇总分包明细表实例
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollectDetail(RequiredCollectDetail requiredCollectDetail) throws BaseException;

	/**
	 * 删除汇总分包明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollectDetails(String[] id) throws BaseException;

	/**
	 * 获得所有汇总分包明细表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectDetailList( RollPage rollPage  ) throws BaseException ;
	
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param rcId 查询参数对象 汇总表ID
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectDetailList(  Long rcId ) throws BaseException ;
	
	/**
	 * 删除所有汇总分包明细表数据集
	 * @param rcId 查询参数对象 汇总表ID
	 * @return
	 * @throws BaseException 
	 */
	abstract void deleteRequiredCollectDetailList(  Long rcId ) throws BaseException ;
	
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param requiredCollectDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectDetailList(RequiredCollectDetail requiredCollectDetail ) throws BaseException ;
	
	/**
	 * 获得所有汇总分包明细表数据集
	 * @param rollPage 分页对象
	 * @param requiredCollectDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectDetailList(RollPage rollPage, RequiredCollectDetail requiredCollectDetail)
			throws BaseException;

	/**
	 * 根据计划id查询采购立项信息
	 * @param rmId
	 * @return
	 * @throws BaseException
	 */
	abstract List getRequiredCollecListByRmId(Long rmId) throws BaseException; 
	/**
	 * 根据计划编号查询采购立项信息
	 * @param rmCode
	 * @return
	 * @throws BaseException
	 */
	abstract List getRequiredCollecListByRmCode(String rmCode) throws BaseException; 
	/**
	 * 供应商报名表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListSupplierSignUp( RollPage rollPage,RequiredCollect requiredCollect) throws BaseException ;
	/**
	 * 查看我报名的项目信息
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListMyApplication( RollPage rollPage,RequiredCollect requiredCollect,Long supplierId) throws BaseException ;
	/**
	 * 查询采购物资 网站查询所用
	 * @param rollPage
	 * @param mapParams
	 * @return
	 * @throws BaseException
	 */
	abstract List getRequiredCollectListForWebIndex(RollPage rollPage,Map<String, Object> mapParams) throws BaseException;
	/**
	 * 获得所有采购完成的项目信息表数据集   新建合同信息所用
	 * @param rollPage 分页对象
	 * @param map  查询条件map对象
	 * @param sign  sign 1为合同   2为订单
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListForCollectAndOrder( RollPage rollPage,Map<String,Object> map,String sqlStr,int sign) throws BaseException ;
	/**
	 * 获得所有汇总分包表数据集 采购方式变更
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListForPurchaseChange( RollPage rollPage,RequiredCollect requiredCollect,String sqlStr) throws BaseException ;
	/**
	 * 获得所有标汇总分包表数据集
	 * @param mapParams 查询参数对象
	 * @param sql 权限sql
	 * @return
	 * @throws BaseException 
	 */
	abstract int countRequiredCollectListList(Map<String, Object> mapParams,String sql) throws BaseException ;
	/**
     * 变更采购方式  插入项目明细
     * @param oldRcId
     * @param newRcId
     * @param newBidCode
     * @throws BaseException
     */
	abstract void updatePurchaseChangeDetail(Long oldRcId,Long newRcId,String newBidCode) throws BaseException;
	 /**
     * 更新开标状态
     * @param rcId
     * @throws BaseException
     */
	abstract void updateOpenStatus(Long rcId) throws BaseException;
	/**
	 * 采购概况报表查询
	 * @param rollPage 分页对象
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListForPurchaseReport( RollPage rollPage,Map<String,Object> map,String sqlStr) throws BaseException ;
	/**
	 * 采购概况报表查询  导出Excel
	 * @param map
	 * @param sqlStr
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredCollectListForPurchaseReport(Map<String,Object> map,String sqlStr) throws BaseException ;
	/**
	 * 更新项目节点信息
	 * @param requiredCollect
	 * @throws BaseException
	 */
	abstract void updateRequiredCollectNodeByRequiredCollect(RequiredCollect requiredCollect) throws BaseException;
	
}

	
