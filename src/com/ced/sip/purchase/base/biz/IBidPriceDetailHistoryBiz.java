package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidPriceDetailHistory;
/** 
 * 类名称：IBidPriceDetailHistoryBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidPriceDetailHistoryBiz {

	/**
	 * 根据主键获得供应商历史报价明细信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidPriceDetailHistory getBidPriceDetailHistory(Long id) throws BaseException;

	/**
	 * 添加供应商历史报价明细信息信息
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException;

	/**
	 * 更新供应商历史报价明细信息表实例
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException;

	/**
	 * 删除供应商历史报价明细信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceDetailHistory(String id) throws BaseException;

	/**
	 * 删除供应商历史报价明细信息表实例
	 * @param bidPriceDetailHistory 供应商历史报价明细信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceDetailHistory(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException;

	/**
	 * 删除供应商历史报价明细信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceDetailHistorys(String[] id) throws BaseException;

	/**
	 * 获得所有供应商历史报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailHistoryList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商历史报价明细信息表数据集  关联RequiredCollectDetail表
	 * @param bidPriceDetailHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailHistoryListRequiredCollectDetail(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException ;
	/**
	 * 获得所有供应商历史报价明细信息表数据集 
	 * @param bidPriceDetailHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailHistoryList(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException ;
	
	/**
	 * 获得所有供应商历史报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceDetailHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailHistoryList(RollPage rollPage, BidPriceDetailHistory bidPriceDetailHistory)
			throws BaseException;
	/**
	 * 获得所有供应商历史报价明细信息表数据集  供应商端
	 * @param bidPriceDetailHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailHistoryListSupplier(BidPriceDetailHistory bidPriceDetailHistory) throws BaseException ;
	
	
}