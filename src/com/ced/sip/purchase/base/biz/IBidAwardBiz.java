package com.ced.sip.purchase.base.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidAward;
/** 
 * 类名称：IBidAwardBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public interface IBidAwardBiz {

	/**
	 * 根据主键获得授标表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidAward getBidAward(Long id) throws BaseException;

	/**
	 * 添加授标信息
	 * @param bidAward 授标表实例
	 * @throws BaseException 
	 */
	abstract void saveBidAward(BidAward bidAward) throws BaseException;

	/**
	 * 更新授标表实例
	 * @param bidAward 授标表实例
	 * @throws BaseException 
	 */
	abstract void updateBidAward(BidAward bidAward) throws BaseException;

	/**
	 * 删除授标表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidAward(String id) throws BaseException;

	/**
	 * 删除授标表实例
	 * @param bidAward 授标表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidAward(BidAward bidAward) throws BaseException;

	/**
	 * 删除授标表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidAwards(String[] id) throws BaseException;

	/**
	 * 获得授标表数据集
	 * bidAward 授标表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BidAward getBidAwardByBidAward(BidAward bidAward) throws BaseException ;
	
	/**
	 * 获得所有授标表数据集
	 * @param bidAward 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAwardList(BidAward bidAward) throws BaseException ;
	
	/**
	 * 获得所有授标表数据集
	 * @param rollPage 分页对象
	 * @param bidAward 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidAwardList(RollPage rollPage, BidAward bidAward)
			throws BaseException;

	/**
	 * 删除授标实例
	 * @param rcId
	 * @throws BaseException 
	 */
	abstract void deleteBidAwardByRcId(Long rcId) throws BaseException;
	/**
	 * 更新bidAward表中的bid_price
	 * @param bidAward 清单表实例
	 * @throws BaseException 
	 */
	abstract void updateBidAwardBidPrice(Long rcId) throws BaseException;
	/**
	 * 查询采购方式对比分析信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	abstract List getPurchseWayList(String sqlStr) throws BaseException;
	/**
	 * 授标之后更新物料库中的最新采购价信息
	 * @param rcId
	 * @throws BaseException
	 */
	abstract void updateMaterialListPrice(Long rcId) throws BaseException;
	/**
	 * 依据条件和授标ID更新授标明细表
	 * @param param
	 * @param baId
	 * @throws BaseException
	 */
	abstract void updateBidAwardByOrder(String param,Long baId) throws BaseException;
	/**
	 * 审批通过或者执行下一步时候更新授标状态
	 * @param rcId
	 * @throws BaseException
	 */
	abstract void updateBidAwardStatus(Long rcId) throws BaseException;
	/**
	 * 获得成交供应商基本信息表数据集总数
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	public abstract int countBidAwardSupplier(Map<String,Object> mapParams)
	         throws BaseException;
	/**
	 * 获得采购总金额
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract double sumPurchaseMoney(Map<String,Object> mapParams,String sqlstr)
	         throws BaseException;
	/**
	 * 获得采购项目量
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract int countPurchaseNumber(Map<String,Object> mapParams,String sqlStr)
	         throws BaseException;
	/**
	 * 获得节资总金额
	 * @param map 查询参数对象
	 * @author 
	 * @return
	 * @throws BaseException 
	 */
	abstract double sumEconomyMoney(Map<String,Object> mapParams,String sqlStr)
	         throws BaseException;
}