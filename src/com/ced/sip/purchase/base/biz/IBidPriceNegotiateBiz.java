package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidPriceNegotiate;
/** 
 * 类名称：IBidPriceNegotiateBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidPriceNegotiateBiz {

	/**
	 * 根据主键获得价格磋商表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidPriceNegotiate getBidPriceNegotiate(Long id) throws BaseException;

	/**
	 * 添加价格磋商信息
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @throws BaseException 
	 */
	abstract void saveBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException;

	/**
	 * 更新价格磋商表实例
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @throws BaseException 
	 */
	abstract void updateBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException;

	/**
	 * 删除价格磋商表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceNegotiate(String id) throws BaseException;

	/**
	 * 删除价格磋商表实例
	 * @param bidPriceNegotiate 价格磋商表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceNegotiate(BidPriceNegotiate bidPriceNegotiate) throws BaseException;

	/**
	 * 删除价格磋商表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceNegotiates(String[] id) throws BaseException;

	/**
	 * 获得所有价格磋商表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceNegotiateList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有价格磋商表数据集
	 * @param bidPriceNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceNegotiateList(BidPriceNegotiate bidPriceNegotiate) throws BaseException ;
	
	/**
	 * 获得所有价格磋商表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceNegotiate 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceNegotiateList(RollPage rollPage, BidPriceNegotiate bidPriceNegotiate)
			throws BaseException;

}