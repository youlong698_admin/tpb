package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidProcessLog;
/** 
 * 类名称：IBidProcessLogBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-03
 */
public interface IBidProcessLogBiz {

	/**
	 * 根据主键获得项目流程记录表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidProcessLog getBidProcessLog(Long id) throws BaseException;

	/**
	 * 添加项目流程记录表信息
	 * @param bidProcessLog 项目流程记录表表实例
	 * @throws BaseException 
	 */
	abstract void saveBidProcessLog(BidProcessLog bidProcessLog) throws BaseException;

	/**
	 * 更新项目流程记录表表实例
	 * @param bidProcessLog 项目流程记录表表实例
	 * @throws BaseException 
	 */
	abstract void updateBidProcessLog(BidProcessLog bidProcessLog) throws BaseException;

	/**
	 * 删除项目流程记录表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidProcessLog(String id) throws BaseException;

	/**
	 * 删除项目流程记录表表实例
	 * @param bidProcessLog 项目流程记录表表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidProcessLog(BidProcessLog bidProcessLog) throws BaseException;

	/**
	 * 删除项目流程记录表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidProcessLogs(String[] id) throws BaseException;

	/**
	 * 获得所有项目流程记录表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidProcessLogList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有项目流程记录表表数据集
	 * @param bidProcessLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidProcessLogList(BidProcessLog bidProcessLog) throws BaseException ;
	
	/**
	 * 获得所有项目流程记录表表数据集
	 * @param rollPage 分页对象
	 * @param bidProcessLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidProcessLogList(RollPage rollPage, BidProcessLog bidProcessLog)
			throws BaseException;

	/**
	 * 获得标段流程记录表数据集
	 * @param bidProcessLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public BidProcessLog getBidProcessLogByRcIdAndBidNode(BidProcessLog bidProcessLog ) throws BaseException;

}