package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
/** 
 * 类名称：IBidCommunicationInfoBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public interface IBidCommunicationInfoBiz {

	/**
	 * 根据主键获得交流信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidCommunicationInfo getBidCommunicationInfo(Long id) throws BaseException;

	/**
	 * 添加交流信息信息
	 * @param bidCommunicationInfo 交流信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException;

	/**
	 * 更新交流信息表实例
	 * @param bidCommunicationInfo 交流信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException;

	/**
	 * 删除交流信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidCommunicationInfo(String id) throws BaseException;

	/**
	 * 删除交流信息表实例
	 * @param bidCommunicationInfo 交流信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) throws BaseException;

	/**
	 * 删除交流信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidCommunicationInfos(String[] id) throws BaseException;

	/**
	 * 获得所有交流信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidCommunicationInfoList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有交流信息表数据集
	 * @param bidCommunicationInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidCommunicationInfoList(BidCommunicationInfo bidCommunicationInfo) throws BaseException ;

	/**
	 * 获得未读消息交流数
	 * @param bidCommunicationInfo 查询参数对象
	 * @param sign 查询参数对象
	 * @author luguanglei 2017-08-24
	 * @return
	 * @throws BaseException 
	 */
	abstract int countBidCommunicationInfoList(BidCommunicationInfo bidCommunicationInfo,int sign) throws BaseException ;
	/**
	 * 获得所有交流信息表数据集
	 * @param rollPage 分页对象
	 * @param bidCommunicationInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidCommunicationInfoList(RollPage rollPage, BidCommunicationInfo bidCommunicationInfo)
			throws BaseException;

}