package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.InviteSupplier;
/** 
 * 类名称：IInviteSupplierBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-02
 */
public interface IInviteSupplierBiz {

	/**
	 * 根据主键获得邀请供应商表表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract InviteSupplier getInviteSupplier(Long id) throws BaseException;

	/**
	 * 添加邀请供应商表信息
	 * @param inviteSupplier 邀请供应商表表实例
	 * @throws BaseException 
	 */
	abstract void saveInviteSupplier(InviteSupplier inviteSupplier) throws BaseException;

	/**
	 * 更新邀请供应商表表实例
	 * @param inviteSupplier 邀请供应商表表实例
	 * @throws BaseException 
	 */
	abstract void updateInviteSupplier(InviteSupplier inviteSupplier) throws BaseException;

	/**
	 * 删除邀请供应商表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteInviteSupplier(String id) throws BaseException;

	/**
	 * 删除邀请供应商表表实例
	 * @param inviteSupplier 邀请供应商表表实例
	 * @throws BaseException 
	 */
	abstract void deleteInviteSupplier(InviteSupplier inviteSupplier) throws BaseException;

	/**
	 * 删除邀请供应商表表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteInviteSuppliers(String[] id) throws BaseException;

	/**
	 * 获得所有邀请供应商表表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getInviteSupplierList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有邀请供应商表表数据集
	 * @param inviteSupplier 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getInviteSupplierList(InviteSupplier inviteSupplier) throws BaseException ;	
	/**
	 * 获得所有邀请供应商表的总数
	 * @param inviteSupplier 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract int countInviteSupplierList(InviteSupplier inviteSupplier) throws BaseException ;
	/**
	 * 获得邀请供应商表数据集
	 * @param inviteSupplier 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract InviteSupplier getInviteSupplierByInviteSupplier(InviteSupplier inviteSupplier) throws BaseException ;
	
	/**
	 * 获得所有邀请供应商表表数据集
	 * @param rollPage 分页对象
	 * @param inviteSupplier 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getInviteSupplierList(RollPage rollPage, InviteSupplier inviteSupplier)
			throws BaseException;

	/**
	 * 删除邀请供应商表表实例
	 * @param rcId
	 * @throws BaseException 
	 */
	abstract void deleteInviteSupplierByRcId(Long rcId) throws BaseException;

	/**
	 * 更新邀请供应商表中的是否报价字段
	 * @param rcId
	 * @param supplierId
	 * @throws BaseException 
	 */
	abstract void updateIsPriceAaByRcIdAndSupplierId(Long rcId,Long supplierId) throws BaseException;
	/**
	 * 更新邀请供应商表中的是否短信发送
	 * @param rcId
	 * @param supplierId
	 * @throws BaseException 
	 */
	abstract void updateIsSmsByRcIdAndSupplierId(Long rcId,Long supplierId) throws BaseException;
	/**
	 * 更新邀请供应商表中的供应商来源字段
	 * @param isId
	 * @param supplierId
	 * @param rcId
	 * @throws BaseException 
	 */
	abstract void updateInviteSupplierSourceCategory(Long isId,Long supplierId,Long rcId) throws BaseException;
}