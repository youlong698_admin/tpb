package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidResultNotice;
import com.ced.sip.purchase.base.entity.RequiredCollect;
/** 
 * 类名称：IBidResultNoticeBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public interface IBidResultNoticeBiz {

	/**
	 * 根据主键获得中标通知书表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidResultNotice getBidResultNotice(Long id) throws BaseException;

	/**
	 * 添加中标通知书信息
	 * @param bidResultNotice 中标通知书表实例
	 * @throws BaseException 
	 */
	abstract void saveBidResultNotice(BidResultNotice bidResultNotice) throws BaseException;

	/**
	 * 更新中标通知书表实例
	 * @param bidResultNotice 中标通知书表实例
	 * @throws BaseException 
	 */
	abstract void updateBidResultNotice(BidResultNotice bidResultNotice) throws BaseException;

	/**
	 * 删除中标通知书表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidResultNotice(String id) throws BaseException;

	/**
	 * 删除中标通知书表实例
	 * @param bidResultNotice 中标通知书表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidResultNotice(BidResultNotice bidResultNotice) throws BaseException;

	/**
	 * 删除中标通知书表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidResultNotices(String[] id) throws BaseException;

	/**
	 * 获得中标通知书表数据集
	 * bidResultNotice 中标通知书表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BidResultNotice getBidResultNoticeByBidResultNotice(BidResultNotice bidResultNotice) throws BaseException ;
	
	/**
	 * 获得所有中标通知书表数据集
	 * @param bidResultNotice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidResultNoticeList(BidResultNotice bidResultNotice) throws BaseException ;
	
	/**
	 * 获得所有中标通知书表数据集
	 * @param rollPage 分页对象
	 * @param bidResultNotice 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidResultNoticeList(RollPage rollPage, BidResultNotice bidResultNotice)
			throws BaseException;
	/**
	 * 获得供应商中标信息列表数据集
	 * @param rollPage 分页对象
	 * @param bidResultNotice 查询参数对象
	 * @param requiredCollect 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getSupplierBehavior(RollPage rollPage, BidResultNotice bidResultNotice,RequiredCollect requiredCollect)
			throws BaseException;

}