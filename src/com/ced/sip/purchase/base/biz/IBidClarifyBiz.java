package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidClarify;
import com.ced.sip.purchase.base.entity.BidClarifyResponse;
/** 
 * 类名称：IBidClarifyBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public interface IBidClarifyBiz {

	/**
	 * 根据主键获得标前澄清表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidClarify getBidClarify(Long id) throws BaseException;

	/**
	 * 添加标前澄清信息
	 * @param bidClarify 标前澄清表实例
	 * @throws BaseException 
	 */
	abstract void saveBidClarify(BidClarify bidClarify) throws BaseException;

	/**
	 * 更新标前澄清表实例
	 * @param bidClarify 标前澄清表实例
	 * @throws BaseException 
	 */
	abstract void updateBidClarify(BidClarify bidClarify) throws BaseException;

	/**
	 * 删除标前澄清表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidClarify(String id) throws BaseException;

	/**
	 * 删除标前澄清表实例
	 * @param bidClarify 标前澄清表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidClarify(BidClarify bidClarify) throws BaseException;

	/**
	 * 删除标前澄清表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidClarifys(String[] id) throws BaseException;

	/**
	 * 获得所有标前澄清表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidClarifyList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有标前澄清表数据集
	 * @param bidClarify 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidClarifyList(BidClarify bidClarify) throws BaseException ;
	
	/**
	 * 获得所有标前澄清表数据集
	 * @param rollPage 分页对象
	 * @param bidClarify 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidClarifyList(RollPage rollPage, BidClarify bidClarify)
			throws BaseException;

}