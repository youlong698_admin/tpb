package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidBusinessResponseHistory;
/** 
 * 类名称：IBidBusinessResponseHistoryBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidBusinessResponseHistoryBiz {

	/**
	 * 根据主键获得供应商历史商务响应表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidBusinessResponseHistory getBidBusinessResponseHistory(Long id) throws BaseException;

	/**
	 * 添加供应商历史商务响应信息
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @throws BaseException 
	 */
	abstract void saveBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException;

	/**
	 * 更新供应商历史商务响应表实例
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @throws BaseException 
	 */
	abstract void updateBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException;

	/**
	 * 删除供应商历史商务响应表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidBusinessResponseHistory(String id) throws BaseException;

	/**
	 * 删除供应商历史商务响应表实例
	 * @param bidBusinessResponseHistory 供应商历史商务响应表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidBusinessResponseHistory(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException;

	/**
	 * 删除供应商历史商务响应表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidBusinessResponseHistorys(String[] id) throws BaseException;

	/**
	 * 获得所有供应商历史商务响应表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBusinessResponseHistoryList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商历史商务响应表数据集
	 * @param bidBusinessResponseHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBusinessResponseHistoryList(BidBusinessResponseHistory bidBusinessResponseHistory) throws BaseException ;
	
	/**
	 * 获得所有供应商历史商务响应表数据集
	 * @param rollPage 分页对象
	 * @param bidBusinessResponseHistory 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBusinessResponseHistoryList(RollPage rollPage, BidBusinessResponseHistory bidBusinessResponseHistory)
			throws BaseException;

}