package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidWinning;
/** 
 * 类名称：IBidWinningBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public interface IBidWinningBiz {

	/**
	 * 根据主键获得中标公示表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidWinning getBidWinning(Long id) throws BaseException;

	/**
	 * 添加中标公示信息
	 * @param bidWinning 中标公示表实例
	 * @throws BaseException 
	 */
	abstract void saveBidWinning(BidWinning bidWinning) throws BaseException;

	/**
	 * 更新中标公示表实例
	 * @param bidWinning 中标公示表实例
	 * @throws BaseException 
	 */
	abstract void updateBidWinning(BidWinning bidWinning) throws BaseException;

	/**
	 * 删除中标公示表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidWinning(String id) throws BaseException;

	/**
	 * 删除中标公示表实例
	 * @param bidWinning 中标公示表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidWinning(BidWinning bidWinning) throws BaseException;

	/**
	 * 删除中标公示表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidWinnings(String[] id) throws BaseException;

	/**
	 * 获得中标公示表数据集
	 * bidWinning 中标公示表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BidWinning getBidWinningByBidWinning(BidWinning bidWinning) throws BaseException ;
	
	/**
	 * 获得所有中标公示表数据集
	 * @param bidWinning 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidWinningList(BidWinning bidWinning) throws BaseException ;
	
	/**
	 * 获得所有中标公示表数据集
	 * @param rollPage 分页对象
	 * @param bidWinning 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidWinningList(RollPage rollPage, BidWinning bidWinning)
			throws BaseException;

}