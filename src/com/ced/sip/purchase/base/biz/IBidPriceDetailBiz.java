package com.ced.sip.purchase.base.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
/** 
 * 类名称：IBidPriceDetailBiz
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public interface IBidPriceDetailBiz {

	/**
	 * 根据主键获得供应商报价明细信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BidPriceDetail getBidPriceDetail(Long id) throws BaseException;

	/**
	 * 添加供应商报价明细信息信息
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException;

	/**
	 * 更新供应商报价明细信息表实例
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException;

	/**
	 * 删除供应商报价明细信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceDetail(String id) throws BaseException;

	/**
	 * 删除供应商报价明细信息表实例
	 * @param bidPriceDetail 供应商报价明细信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceDetail(BidPriceDetail bidPriceDetail) throws BaseException;

	/**
	 * 删除供应商报价明细信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBidPriceDetails(String[] id) throws BaseException;

	/**
	 * 获得所有供应商报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailList(RollPage rollPage) throws BaseException ;
	
	/**
	 * 获得所有供应商报价明细信息表数据集  关联RequiredCollectDetail表
	 * @param bidPriceDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailListRequiredCollectDetail(BidPriceDetail bidPriceDetail) throws BaseException ;
	/**
	 * 获得所有供应商报价明细信息表数据集
	 * @param bidPriceDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailList(BidPriceDetail bidPriceDetail) throws BaseException ;
	/**
	 * 获得所有供应商报价明细信息表数据集   供应商
	 * @param bidPriceDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailListSupplier(BidPriceDetail bidPriceDetail) throws BaseException ;
	/**
	 * 获得所有供应商报价明细信息表数据集
	 * @param rollPage 分页对象
	 * @param bidPriceDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidPriceDetailList(RollPage rollPage, BidPriceDetail bidPriceDetail)
			throws BaseException;
	/**
	 * 获取供应商比价数据集
	 * @param bpIdStr
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */	
	abstract List getBidPriceDetailListForParityPrice(Long[] bpIdStr,Long rcId) throws BaseException;
	/**
	 * 根据采购计划明细ID获取当前报价的最低价
	 * @param rcdId
	 * @return
	 * @throws BaseException
	 */	
	abstract double getLowestPrice(Long rcdId) throws BaseException;
	

	
}