package com.ced.sip.purchase.base.action.supplier;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.base.biz.IBidCommunicationInfoBiz;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
/** 
 * 类名称：BidCommunicationInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidCommunicationInfoAction extends BaseAction {

	// 问题解答
	private IBidCommunicationInfoBiz iBidCommunicationInfoBiz;
	
	
	private Long rcId;
	// 交流信息
	private BidCommunicationInfo bidCommunicationInfo;

	/**
	 * 查看交流信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidCommunicationInfoMonitor() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setRcId(rcId);
			bidCommunicationInfo.setQuestionerId(supplierId);
			List<BidCommunicationInfo> bidCommunicationInfoList=this.iBidCommunicationInfoBiz.getBidCommunicationInfoList(bidCommunicationInfo);
			this.getRequest().setAttribute("bidCommunicationInfoList", bidCommunicationInfoList);
		
		} catch (Exception e) {
			log.error("查看交流信息信息列表错误！", e);
			throw new BaseException("查看交流信息信息列表错误！", e);
		}	
			
		return "viewBidCommunicationInfo" ;
				
	}	
	/**
	 * 新增问题解答信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidCommunicationInfoInit() throws BaseException {
		
		try{
			bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setType("1");
			bidCommunicationInfo.setIdentification("1");
			bidCommunicationInfo.setRcId(rcId);
			this.getRequest().setAttribute("now", DateUtil.getCurrentDateTime());
		} catch (Exception e) {
			log("新增问题解答信息初始化错误！", e);
			throw new BaseException("新增问题解答信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 新增问题解答信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidCommunicationInfo() throws BaseException {
		
		try{
			String supplierName=UserRightInfoUtil.getSupplierName(getRequest());
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			bidCommunicationInfo.setQuestionerId(supplierId);
			bidCommunicationInfo.setQuestionerName(supplierName);
			bidCommunicationInfo.setReadStatus(TableStatus.READ_STATUS_1);
			bidCommunicationInfo.setQuestionDate(DateUtil.getCurrentDateTime());
			
			this.iBidCommunicationInfoBiz.saveBidCommunicationInfo(bidCommunicationInfo);			
			this.getRequest().setAttribute("message", "新增成功");
			this.getRequest().setAttribute("operModule", "新增问题解答");
		} catch (Exception e) {
			log("新增问题解答信息错误！", e);
			throw new BaseException("新增问题解答信息错误！", e);
		}
		return ADD_INIT;
		
	}
	/**
	 * 查看交流信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidCommunicationInfoDetail() throws BaseException {
		
		try{
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
		} catch (Exception e) {
			log("查看交流信息明细信息错误！", e);
			throw new BaseException("查看交流信息明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 获取未查看的问题解答数
	 * @return
	 * @throws BaseException
	 */
	public String getBidCommunicationInfoForNode() throws BaseException{
		int count=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
            
            bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setRcId(rcId);
			bidCommunicationInfo.setQuestionerId(supplierId);
			count=this.iBidCommunicationInfoBiz.countBidCommunicationInfoList(bidCommunicationInfo,2);
            
			out.print(count);
		} catch (Exception e) {
			log("获取未查看的问题解答数！", e);
			throw new BaseException("获取未查看的问题解答数", e);
		}
		return null;
	}
	/**
	 * 更新问题解答 未读变已读
	 * @return
	 * @throws BaseException
	 */
	public String updateBidCommunicationInfoReadStatus() throws BaseException{
		int count=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
			bidCommunicationInfo.setReadStatus(TableStatus.READ_STATUS_0);
            this.iBidCommunicationInfoBiz.updateBidCommunicationInfo(bidCommunicationInfo);
            count=1;
			out.print(count);
		} catch (Exception e) {
			log("更新问题解答 未读变已读！", e);
			throw new BaseException("更新问题解答 未读变已读", e);
		}
		return null;
	}
	public BidCommunicationInfo getBidCommunicationInfo() {
		return bidCommunicationInfo;
	}

	public void setBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) {
		this.bidCommunicationInfo = bidCommunicationInfo;
	}
	public IBidCommunicationInfoBiz getiBidCommunicationInfoBiz() {
		return iBidCommunicationInfoBiz;
	}
	public void setiBidCommunicationInfoBiz(
			IBidCommunicationInfoBiz iBidCommunicationInfoBiz) {
		this.iBidCommunicationInfoBiz = iBidCommunicationInfoBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	
}
