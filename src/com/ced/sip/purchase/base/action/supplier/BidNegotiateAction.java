package com.ced.sip.purchase.base.action.supplier;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseBiz;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidNegotiateBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceNegotiateBiz;
import com.ced.sip.purchase.base.biz.IBidResponseNegotiateBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
import com.ced.sip.purchase.base.entity.BidBusinessResponseHistory;
import com.ced.sip.purchase.base.entity.BidNegotiate;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
import com.ced.sip.purchase.base.entity.BidPriceDetailHistory;
import com.ced.sip.purchase.base.entity.BidPriceHistory;
import com.ced.sip.purchase.base.entity.BidPriceNegotiate;
import com.ced.sip.purchase.base.entity.BidResponseNegotiate;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.supplier.entity.SupplierInfo;
/** 
 * 类名称：BidNegotiateAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-04
 */
public class BidNegotiateAction extends BaseAction {

	// 磋商信息 
	private IBidNegotiateBiz iBidNegotiateBiz;
	// 价格磋商信息 
	private IBidPriceNegotiateBiz iBidPriceNegotiateBiz;
	// 商务磋商信息 
	private IBidResponseNegotiateBiz iBidResponseNegotiateBiz;
	// 报价信息 
	private IBidPriceBiz iBidPriceBiz;
	// 报价明细信息 
	private IBidPriceDetailBiz iBidPriceDetailBiz;
	// 商务响应信息
	private IBidBusinessResponseBiz iBidBusinessResponseBiz;
	// 供应商历史报价信息 
	private IBidPriceHistoryBiz iBidPriceHistoryBiz;
	// 供应商历史报价明细信息 
	private IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz;
	// 供应商商务响应 
	private IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz;
	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
	
	private BidNegotiate bidNegotiate;
	// 供应商报价信息
	private BidPrice bidPrice;
	// 供应商报价信息明细
	private BidPriceDetail bidPriceDetail;
	// 供应商报价历史信息
	private BidPriceHistory bidPriceHistory;
	// 供应商报价历史明细信息
	private BidPriceDetailHistory bidPriceDetailHistory;
	// 商务响应项信息
	private BidBusinessResponse bidBusinessResponse;
	// 商务响应项历史信息
	private BidBusinessResponseHistory bidBusinessResponseHistory;
	// 供应商报价信息
	private List<BidPriceDetail> bpdList;
	// 商务响应项信息
	private List<BidBusinessResponse> bbrList;
	
	private RequiredCollectDetail requiredCollectDetail;
	
	/**
	 * 查看磋商信息信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidNegotiateMonitor() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
			bidNegotiate=new BidNegotiate();
			bidNegotiate.setRcId(rcId);
			bidNegotiate.setSupplierId(supplierId);
			List<BidNegotiate> list=this.iBidNegotiateBiz.getBidNegotiateListBySupplier(bidNegotiate);
			this.getRequest().setAttribute("list", list);
		} catch (Exception e) {
			log.error("查看磋商信息信息列表错误！", e);
			throw new BaseException("查看磋商信息信息列表错误！", e);
		}	
			
		return "viewBidNegotiate" ;
				
	}
	
	/**
	 * 保存磋商信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidNegotiateInit() throws BaseException {
		try{
			bidNegotiate=this.iBidNegotiateBiz.getBidNegotiate(bidNegotiate.getBnId());
			
			InviteSupplier inviteSupplier=new InviteSupplier();
			inviteSupplier.setRcId(bidNegotiate.getRcId());
			inviteSupplier.setSupplierId(bidNegotiate.getSupplierId());
			inviteSupplier=this.iInviteSupplierBiz.getInviteSupplierByInviteSupplier(inviteSupplier);
			  
			bidPrice=new BidPrice();
			bidPrice.setRcId(bidNegotiate.getRcId());
			bidPrice.setSupplierId(bidNegotiate.getSupplierId());
			bidPrice=this.iBidPriceBiz.getBidPriceListByBidPrice(bidPrice);
			
			//得到目前的报价信息
			List<BidPriceDetail> bpdList=new ArrayList<BidPriceDetail>();
			bidPriceDetail=new BidPriceDetail();
			bidPriceDetail.setBpId(bidPrice.getBpId());
			List<Object[]> objectList=this.iBidPriceDetailBiz.getBidPriceDetailListSupplier(bidPriceDetail);
			
				
			//得到目前的商务响应项信息	
			bidBusinessResponse=new BidBusinessResponse();
			bidBusinessResponse.setBpId(bidPrice.getBpId());
			List<BidBusinessResponse> bbrList=this.iBidBusinessResponseBiz.getBidBusinessResponseList(bidBusinessResponse);
			
			//查询需要磋商报价信息
			BidPriceNegotiate bidPriceNegotiate=new BidPriceNegotiate();
			bidPriceNegotiate.setBnId(bidNegotiate.getBnId());
			List<BidPriceNegotiate> bpnList=this.iBidPriceNegotiateBiz.getBidPriceNegotiateList(bidPriceNegotiate);
			Map<Long, BidPriceNegotiate> bpnMap = new HashMap<Long, BidPriceNegotiate>(); 
			for(BidPriceNegotiate bidPriceNegotiate2:bpnList){
				bpnMap.put(bidPriceNegotiate2.getRcdId(), bidPriceNegotiate2);
			}
			
		    //查询需要磋商的商务响应项信息
		    BidResponseNegotiate bidResponseNegotiate=new BidResponseNegotiate();
		    bidResponseNegotiate.setBnId(bidNegotiate.getBnId());
		    List<BidResponseNegotiate> brnList=this.iBidResponseNegotiateBiz.getBidResponseNegotiateList(bidResponseNegotiate);
		    Map<Long, BidResponseNegotiate> bbrMap = new HashMap<Long, BidResponseNegotiate>(); 
			for(BidResponseNegotiate bidResponseNegotiate2:brnList){
				bbrMap.put(bidResponseNegotiate2.getBriId(), bidResponseNegotiate2);
			}
			
			//重新组合需要报价的信息
			for(Object[] object:objectList){
				bidPriceDetail=(BidPriceDetail)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				bidPriceDetail.setRequiredCollectDetail(requiredCollectDetail);
				if(bpnMap.get(bidPriceDetail.getRcdId())!=null) bidPriceDetail.setBidPriceNegotiate(bpnMap.get(bidPriceDetail.getRcdId()));
				bpdList.add(bidPriceDetail);
			}
			//重新组合商务响应项信息
			for(BidBusinessResponse bidBusinessResponse:bbrList){
				if(bbrMap.get(bidBusinessResponse.getBriId())!=null) bidBusinessResponse.setBidResponseNegotiate(bbrMap.get(bidBusinessResponse.getBriId()));
				
			}

		    this.getRequest().setAttribute("bidPrice", bidPrice);
		    this.getRequest().setAttribute("bpdList", bpdList);
		    this.getRequest().setAttribute("bbrList", bbrList);
			this.getRequest().setAttribute("publicKey", inviteSupplier.getPublicKey());
		} catch (Exception e) {
			log("保存磋商信息信息初始化错误！", e);
			throw new BaseException("保存磋商信息信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	/**
	 * 保存供应商报价信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidNegotiatePriceRespone() throws BaseException {
		
		try{
			bidNegotiate=this.iBidNegotiateBiz.getBidNegotiate(bidNegotiate.getBnId());
			bidNegotiate.setResponseNegotiate(TableStatus.COMMON_0);
			this.iBidNegotiateBiz.updateBidNegotiate(bidNegotiate);
			
			SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(getRequest()) ;
			Date date=DateUtil.getCurrentDateTime();	
			
			//删除旧的报价信息
			this.iBidPriceBiz.deleteBidPriceByRcIdAndSupplierId(bidPrice.getRcId(),supplierInfo.getSupplierId());
			
			//保存新的报价信息
			bidPrice.setWriter(supplierInfo.getSupplierLoginName());
			bidPrice.setWriteDate(date);
			this.iBidPriceBiz.saveBidPrice(bidPrice);
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetail.setBpId(bidPrice.getBpId());
					this.iBidPriceDetailBiz.saveBidPriceDetail(bidPriceDetail);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponse.setBpId(bidPrice.getBpId());
					this.iBidBusinessResponseBiz.saveBidBusinessResponse(bidBusinessResponse);
				}
			}
			
			//保存新的报价历史信息
			bidPriceHistory=new BidPriceHistory();
			BeanUtils.copyProperties(bidPriceHistory, bidPrice);
			bidPriceHistory.setTotalPrice(null);
			this.iBidPriceHistoryBiz.saveBidPriceHistory(bidPriceHistory);
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetailHistory=new BidPriceDetailHistory();
					BeanUtils.copyProperties(bidPriceDetailHistory, bidPriceDetail);
					bidPriceDetailHistory.setPrice(null);
					bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
					this.iBidPriceDetailHistoryBiz.saveBidPriceDetailHistory(bidPriceDetailHistory);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponseHistory=new BidBusinessResponseHistory();
					BeanUtils.copyProperties(bidBusinessResponseHistory, bidBusinessResponse);
					bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
					this.iBidBusinessResponseHistoryBiz.saveBidBusinessResponseHistory(bidBusinessResponseHistory);
				}
			}
			this.getRequest().setAttribute("message", "报价成功");
			this.getRequest().setAttribute("operModule", "供应商报价信息");
		} catch (Exception e) {
			log("保存供应商报价信息信息错误！", e);
			throw new BaseException("保存供应商报价信息信息错误！", e);
		}
		
		return SUCCESS;
		
	}
	

	public IBidNegotiateBiz getiBidNegotiateBiz() {
		return iBidNegotiateBiz;
	}

	public void setiBidNegotiateBiz(IBidNegotiateBiz iBidNegotiateBiz) {
		this.iBidNegotiateBiz = iBidNegotiateBiz;
	}

	public BidNegotiate getBidNegotiate() {
		return bidNegotiate;
	}

	public void setBidNegotiate(BidNegotiate bidNegotiate) {
		this.bidNegotiate = bidNegotiate;
	}

	public IBidPriceNegotiateBiz getiBidPriceNegotiateBiz() {
		return iBidPriceNegotiateBiz;
	}

	public void setiBidPriceNegotiateBiz(IBidPriceNegotiateBiz iBidPriceNegotiateBiz) {
		this.iBidPriceNegotiateBiz = iBidPriceNegotiateBiz;
	}

	public IBidResponseNegotiateBiz getiBidResponseNegotiateBiz() {
		return iBidResponseNegotiateBiz;
	}

	public void setiBidResponseNegotiateBiz(
			IBidResponseNegotiateBiz iBidResponseNegotiateBiz) {
		this.iBidResponseNegotiateBiz = iBidResponseNegotiateBiz;
	}

	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}

	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}

	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}

	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}

	public IBidBusinessResponseBiz getiBidBusinessResponseBiz() {
		return iBidBusinessResponseBiz;
	}

	public void setiBidBusinessResponseBiz(
			IBidBusinessResponseBiz iBidBusinessResponseBiz) {
		this.iBidBusinessResponseBiz = iBidBusinessResponseBiz;
	}

	public List<BidPriceDetail> getBpdList() {
		return bpdList;
	}

	public void setBpdList(List<BidPriceDetail> bpdList) {
		this.bpdList = bpdList;
	}

	public List<BidBusinessResponse> getBbrList() {
		return bbrList;
	}

	public void setBbrList(List<BidBusinessResponse> bbrList) {
		this.bbrList = bbrList;
	}

	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}

	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}

	public IBidPriceHistoryBiz getiBidPriceHistoryBiz() {
		return iBidPriceHistoryBiz;
	}

	public void setiBidPriceHistoryBiz(IBidPriceHistoryBiz iBidPriceHistoryBiz) {
		this.iBidPriceHistoryBiz = iBidPriceHistoryBiz;
	}

	public IBidPriceDetailHistoryBiz getiBidPriceDetailHistoryBiz() {
		return iBidPriceDetailHistoryBiz;
	}

	public void setiBidPriceDetailHistoryBiz(
			IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz) {
		this.iBidPriceDetailHistoryBiz = iBidPriceDetailHistoryBiz;
	}

	public IBidBusinessResponseHistoryBiz getiBidBusinessResponseHistoryBiz() {
		return iBidBusinessResponseHistoryBiz;
	}

	public void setiBidBusinessResponseHistoryBiz(
			IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz) {
		this.iBidBusinessResponseHistoryBiz = iBidBusinessResponseHistoryBiz;
	}

	public BidPrice getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(BidPrice bidPrice) {
		this.bidPrice = bidPrice;
	}
	
}
