package com.ced.sip.purchase.base.action.supplier;

import java.util.List;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.purchase.base.biz.IBidBulletinBiz;
import com.ced.sip.purchase.base.entity.BidBulletin;
/** 
 * 类名称：BidBulletinAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-05
 */
public class BidBulletinAction extends BaseAction {

	// 公告表 
	private IBidBulletinBiz iBidBulletinBiz;
    //项目信息服务类

	private IAttachmentBiz iAttachmentBiz;

	private Long rcId;
	// 公告表
	private BidBulletin bidBulletin;

	/**
	 * 查看公告表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidBulletinDetialBidMonitor() throws BaseException {
		try{			
			bidBulletin = new BidBulletin();
			bidBulletin.setRcId(rcId);
			List<BidBulletin> list=this.iBidBulletinBiz.getBidBulletinList(bidBulletin);
			if(list.size() >0 ){//公告存在
				bidBulletin = list.get(0);
			}
			//获取附件
			bidBulletin.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201 ) ) , "0", this.getRequest() ) ) ;
	    } catch (Exception e) {
			log.error("查看公告表信息列表错误！", e);
			throw new BaseException("查看公告表信息列表错误！", e);
		}	
			
		return "viewBidBulletinDetial" ;				
	}
	

	public BidBulletin getBidBulletin() {
		return bidBulletin;
	}

	public void setBidBulletin(BidBulletin bidBulletin) {
		this.bidBulletin = bidBulletin;
	}
	public IBidBulletinBiz getiBidBulletinBiz() {
		return iBidBulletinBiz;
	}
	public void setiBidBulletinBiz(IBidBulletinBiz iBidBulletinBiz) {
		this.iBidBulletinBiz = iBidBulletinBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	
	
}
