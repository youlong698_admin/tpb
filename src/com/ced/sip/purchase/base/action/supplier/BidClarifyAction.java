package com.ced.sip.purchase.base.action.supplier;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.base.biz.IBidClarifyBiz;
import com.ced.sip.purchase.base.biz.IBidClarifyResponseBiz;
import com.ced.sip.purchase.base.entity.BidClarify;
import com.ced.sip.purchase.base.entity.BidClarifyResponse;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.tender.entity.TenderFileDownload;
/** 
 * 类名称：BidClarifyAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidClarifyAction extends BaseAction {

	// 标前澄清 
	private IBidClarifyBiz iBidClarifyBiz;
	// 标前澄清响应 
	private IBidClarifyResponseBiz iBidClarifyResponseBiz;
	
    private IAttachmentBiz iAttachmentBiz;
	
    private Long rcId;
	// 标前澄清
	private BidClarify bidClarify;
	private BidClarifyResponse bidClarifyResponse;
	
	/**
	 * 查看标前澄清信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidClarifyMonitor() throws BaseException {
		try{

			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			
		    bidClarify=new BidClarify();
			bidClarify.setRcId(rcId);
			bidClarify.setStatus(TableStatus.STATUS_0);			
			
			List<BidClarify> bidClarifyList=this.iBidClarifyBiz.getBidClarifyList(bidClarify);
			for(BidClarify bidClarify:bidClarifyList){
				bidClarifyResponse=new BidClarifyResponse();
				bidClarifyResponse.setBcId(bidClarify.getBcId());
				bidClarifyResponse.setSupplierId(supplierId);
				List<BidClarifyResponse> bidClarifyResponsesList=this.iBidClarifyResponseBiz.getBidClarifyResponseList(bidClarifyResponse);
				if(bidClarifyResponsesList.size()==1)  bidClarify.setIsView(0); else bidClarify.setIsView(1);
			}
			
			this.getRequest().setAttribute("bidClarifyList", bidClarifyList);
			
		} catch (Exception e) {
			log.error("查看标前澄清信息列表错误！", e);
			throw new BaseException("查看标前澄清信息列表错误！", e);
		}
			
		return "viewBidClarify" ;
				
	}
	/**
	 * 保存标前澄清供应商响应信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidClarifyResponse() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			String supplierName=UserRightInfoUtil.getSupplierName(getRequest());
			String supplierLoginName=UserRightInfoUtil.getSupplierLoginName(getRequest());
			Long bcId=Long.parseLong(getRequest().getParameter("bcId"));
			String remark=this.getRequest().getParameter("remark");
			
			bidClarifyResponse=new BidClarifyResponse();
			bidClarifyResponse.setBcId(bcId);
			bidClarifyResponse.setSupplierId(supplierId);
			bidClarifyResponse.setRemark(remark);
			bidClarifyResponse.setSupplierId(supplierId);
			bidClarifyResponse.setSupplierName(supplierName);
			bidClarifyResponse.setViewDate(DateUtil.getCurrentDateTime());
			bidClarifyResponse.setViewr(supplierLoginName);
			this.iBidClarifyResponseBiz.saveBidClarifyResponse(bidClarifyResponse);
			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "保存标前澄清供应商响应");
		} catch (Exception e) {
			log("保存标前澄清供应商响应信息错误！", e);
			throw new BaseException("保存标前澄清供应商响应信息错误！", e);
		}
		
		return DETAIL;
		
	}
	
	/**
	 * 查看标前澄清明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidClarifyDetail() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			bidClarify=this.iBidClarifyBiz.getBidClarify(bidClarify.getBcId() );
			//获取附件
			bidClarify.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( bidClarify.getBcId(), AttachmentStatus.ATTACHMENT_CODE_202 ) ) , "0", this.getRequest() ) ) ;
			bidClarify.setStatusCn(TableStatusMap.statusMap.get(bidClarify.getStatus()));
			
			bidClarifyResponse=new BidClarifyResponse();
			bidClarifyResponse.setBcId(bidClarify.getBcId());
			bidClarifyResponse.setSupplierId(supplierId);
			List<BidClarifyResponse> bidClarifyResponsesList=this.iBidClarifyResponseBiz.getBidClarifyResponseList(bidClarifyResponse);
			if(bidClarifyResponsesList.size()==1) bidClarifyResponse=bidClarifyResponsesList.get(0);
			this.getRequest().setAttribute("bidClarifyResponse", bidClarifyResponse);
		} catch (Exception e) {
			log("查看标前澄清明细信息错误！", e);
			throw new BaseException("查看标前澄清明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 获取未查看的标前澄清数
	 * @return
	 * @throws BaseException
	 */
	public String getBidClarifyForNode() throws BaseException{
		int count=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
            
            bidClarify=new BidClarify();
			bidClarify.setRcId(rcId);
			bidClarify.setStatus(TableStatus.STATUS_0);			
			
			List<BidClarify> bidClarifyList=this.iBidClarifyBiz.getBidClarifyList(bidClarify);
			for(BidClarify bidClarify:bidClarifyList){
				bidClarifyResponse=new BidClarifyResponse();
				bidClarifyResponse.setBcId(bidClarify.getBcId());
				bidClarifyResponse.setSupplierId(supplierId);
				List<BidClarifyResponse> bidClarifyResponsesList=this.iBidClarifyResponseBiz.getBidClarifyResponseList(bidClarifyResponse);
				if(bidClarifyResponsesList.size()==0) count+=1;
			}  
            
			out.print(count);
		} catch (Exception e) {
			log("获取未查看的标前澄清数！", e);
			throw new BaseException("获取未查看的标前澄清数", e);
		}
		return null;
	}
	public BidClarify getBidClarify() {
		return bidClarify;
	}

	public void setBidClarify(BidClarify bidClarify) {
		this.bidClarify = bidClarify;
	}

	public IBidClarifyBiz getiBidClarifyBiz() {
		return iBidClarifyBiz;
	}

	public void setiBidClarifyBiz(IBidClarifyBiz iBidClarifyBiz) {
		this.iBidClarifyBiz = iBidClarifyBiz;
	}

	public IBidClarifyResponseBiz getiBidClarifyResponseBiz() {
		return iBidClarifyResponseBiz;
	}

	public void setiBidClarifyResponseBiz(
			IBidClarifyResponseBiz iBidClarifyResponseBiz) {
		this.iBidClarifyResponseBiz = iBidClarifyResponseBiz;
	}
	
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	
}
