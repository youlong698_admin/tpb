package com.ced.sip.purchase.base.action.supplier;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidBulletinBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBulletin;
import com.ced.sip.purchase.base.entity.RequiredCollect;

public class RequiredCollectAction extends BaseAction {
	//需求汇总分包
	private IRequiredCollectBiz iRequiredCollectBiz;
	private IBidBulletinBiz iBidBulletinBiz; 
	

	private RequiredCollect requiredCollect;
	private BidBulletin bidBulletin;
	

	
	/*********************************************************** 项目报名开始 **********************************************************/
	/**
	 * 查看项目报名页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewRequiredCollectApplication() throws BaseException {
		try{
		    this.getRequest().setAttribute("buyWayMap", TableStatusMap.purchaseWay);
		} catch (Exception e) {
			log.error("查看项目报名页面错误！", e);
			throw new BaseException("查看项目报名页面错误！", e);
		}
		
		return "viewApplication" ;
		
	}
	
	/**
	 * 查看项目报名信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findRequiredCollectApplication() throws BaseException {
		
		try{
			// 项目报名列表信息
			requiredCollect = new RequiredCollect();
			String bidCode=this.getRequest().getParameter("bidCode");
			requiredCollect.setBidCode(bidCode);
			String buyRemark=this.getRequest().getParameter("buyRemark");
			requiredCollect.setBuyRemark(buyRemark);
			String buyWay=this.getRequest().getParameter("buyWay");
			requiredCollect.setBuyWay(buyWay);
			
			requiredCollect.setSupplierType(TableStatus.SUPPLIER_TYPE_00);
			requiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_0);
			
			List<RequiredCollect> rcList=new ArrayList<RequiredCollect>();
			List<Object[]> list=this.iRequiredCollectBiz.getRequiredCollectListSupplierSignUp(this.getRollPageDataTables(), requiredCollect);
			for(Object[] obj:list){
				requiredCollect=(RequiredCollect)obj[0];
				requiredCollect.setCompName((String)obj[1]);
				rcList.add(requiredCollect);
			}
			this.getPagejsonDataTables(rcList);
		} catch (Exception e) {
			log.error("查看项目报名信息列表错误！", e);
			throw new BaseException("查看项目报名信息列表错误！", e);
		}
		
		return null ;
		
	}
	/*********************************************************** 采购项目结束 **********************************************************/
	
	/*********************************************************** 我的项目开始 **********************************************************/
	/**
	 * 查看我的项目页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewRequiredCollectMy() throws BaseException {
        String initPage="viewMy";
		try{
			String buyWay=this.getRequest().getParameter("buyWay");
		    String from=this.getRequest().getParameter("from");
			String buyRemark=this.getRequest().getParameter("buyRemark");
		    this.getRequest().setAttribute("buyWayMap", TableStatusMap.purchaseWay);
		    if(StringUtil.isNotBlank(from)) initPage+="Mobile";
		    this.getRequest().setAttribute("buyWay", buyWay);
		    this.getRequest().setAttribute("buyRemark", buyRemark);
		} catch (Exception e) {
			log.error("查看我的项目页面错误！", e);
			throw new BaseException("查看我的项目页面错误！", e);
		}
		
		return initPage ;
		
	}
	
	/**
	 * 查看我的项目信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findRequiredCollectMy() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			// 查看我的项目信息
			requiredCollect = new RequiredCollect();
			String bidCode=this.getRequest().getParameter("bidCode");
			requiredCollect.setBidCode(bidCode);
			String buyRemark=this.getRequest().getParameter("buyRemark");
			requiredCollect.setBuyRemark(buyRemark);
			String buyWay=this.getRequest().getParameter("buyWay");
			requiredCollect.setBuyWay(buyWay);
			
			requiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_0);
			
			List<RequiredCollect> rcList=new ArrayList<RequiredCollect>();
			List<Object[]> list=this.iRequiredCollectBiz.getRequiredCollectListMyApplication( this.getRollPageDataTables(), requiredCollect,supplierId);
			for(Object[] obj:list){
				requiredCollect=(RequiredCollect)obj[0];
				requiredCollect.setCompName((String)obj[1]);
				rcList.add(requiredCollect);
			}
			this.getPagejsonDataTables(rcList);
		} catch (Exception e) {
			log.error("查看我的项目信息列表错误！", e);
			throw new BaseException("查看我的项目信息列表错误！", e);
		}
		
		return null ;
		
	}
	/*********************************************************** 采购项目结束 **********************************************************/
	
	/**
	 * 点击项目进入项目详情页面
	 * @return
	 * @throws BaseException
	 */
	public String viewRequiredCollectDetailMobile() throws BaseException{
		try{
			bidBulletin=new BidBulletin();
			bidBulletin.setRcId(requiredCollect.getRcId());
			List list=this.iBidBulletinBiz.getBidBulletinList(bidBulletin);
			if(list.size()>0) bidBulletin=(BidBulletin)list.get(0);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
			boolean isApplication=true;
			Date d=new Date();
			if(d.getTime()>bidBulletin.getReturnDate().getTime()){
				isApplication=false;
			}
			this.getRequest().setAttribute("bidBulletin", bidBulletin);
			this.getRequest().setAttribute("isApplication", isApplication);
		} catch (Exception e) {
			log("点击项目进入项目详情页面错误！", e);
			throw new BaseException("点击项目进入项目详情页面报名页面错误！", e);
		}
		return "requiredCollectDetailMobile";
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public IBidBulletinBiz getiBidBulletinBiz() {
		return iBidBulletinBiz;
	}

	public void setiBidBulletinBiz(IBidBulletinBiz iBidBulletinBiz) {
		this.iBidBulletinBiz = iBidBulletinBiz;
	}
	
}
