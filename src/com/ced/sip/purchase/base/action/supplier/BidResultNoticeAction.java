package com.ced.sip.purchase.base.action.supplier;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.purchase.base.biz.IBidResultNoticeBiz;
import com.ced.sip.purchase.base.entity.BidResultNotice;
/** 
 * 类名称：BidResultNoticeAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-04
 */
public class BidResultNoticeAction extends BaseAction {

	// 中标通知书 
	private IBidResultNoticeBiz iBidResultNoticeBiz;

	private IAttachmentBiz iAttachmentBiz;
	
	private Long rcId;
	// 中标通知书
	private BidResultNotice bidResultNotice;
	
	/**
	 * 查看中标通知书信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidResultNoticeMonitor() throws BaseException {
		
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
			bidResultNotice=new BidResultNotice();
			bidResultNotice.setSupplierId(supplierId);
			bidResultNotice.setRcId(rcId);
			bidResultNotice=this.iBidResultNoticeBiz.getBidResultNoticeByBidResultNotice(bidResultNotice);
			bidResultNotice.setAttachmentUrl(iAttachmentBiz.getAttachmentPageUrl(
					iAttachmentBiz.getAttachmentList(new Attachment(bidResultNotice
							.getBrnId(), AttachmentStatus.ATTACHMENT_CODE_203)),
					"0", this.getRequest()));
		} catch (Exception e) {
			log.error("查看中标通知书信息列表错误！", e);
			throw new BaseException("查看中标通知书信息列表错误！", e);
		}	
			
		return "viewBidResultNotice" ;
				
	}
	

	public IBidResultNoticeBiz getiBidResultNoticeBiz() {
		return iBidResultNoticeBiz;
	}

	public void setiBidResultNoticeBiz(IBidResultNoticeBiz iBidResultNoticeBiz) {
		this.iBidResultNoticeBiz = iBidResultNoticeBiz;
	}

	public BidResultNotice getBidResultNotice() {
		return bidResultNotice;
	}

	public void setBidResultNotice(BidResultNotice bidResultNotice) {
		this.bidResultNotice = bidResultNotice;
	}


	public Long getRcId() {
		return rcId;
	}


	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}


	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}


	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	
}
