package com.ced.sip.purchase.base.action.epp;

import java.util.Date;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidNegotiateBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceNegotiateBiz;
import com.ced.sip.purchase.base.biz.IBidResponseNegotiateBiz;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidNegotiate;
import com.ced.sip.purchase.base.entity.BidPriceNegotiate;
import com.ced.sip.purchase.base.entity.BidResponseNegotiate;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.SystemConfiguration;

public class BidNegotiateAction extends BaseAction {
	//磋商信息服务类
	public IBidNegotiateBiz iBidNegotiateBiz;
	//商务响应磋商服务类
	public IBidResponseNegotiateBiz iBidResponseNegotiateBiz;
	//价格磋商服务类
	public IBidPriceNegotiateBiz iBidPriceNegotiateBiz;
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
    //邀请供应商服务类
    private IInviteSupplierBiz iInviteSupplierBiz;
    //商务响应项服务类
    private IBusinessResponseItemBiz iBusinessResponseItemBiz;
    //报价明细服务类
    private IBidPriceDetailBiz iBidPriceDetailBiz;
    //供应商服务类
    private ISupplierInfoBiz iSupplierInfoBiz;
	
	//磋商信息
	public BidNegotiate bidNegotiate;	
	//价格磋商明细
	public BidPriceNegotiate bidPriceNegotiate;
	//商务磋商明细
	public BidResponseNegotiate bidResponseNegotiate;
	
	public RequiredCollect requiredCollect;
	
    private SupplierInfo supplierInfo;
	
	private Long rcId;
	/**
	 * 新建磋商信息
	 * @return
	 * @throws BaseException
	 */	
	public String saveInitNegotiate() throws BaseException{
		try{
		  double lowestPrice=0.00;
		  List<RequiredCollectDetail> rcdList=this.iRequiredCollectBiz.getRequiredCollectDetailList(rcId);
		  for(RequiredCollectDetail requiredCollectDetail:rcdList){
			  lowestPrice=this.iBidPriceDetailBiz.getLowestPrice(requiredCollectDetail.getRcdId());
			  requiredCollectDetail.setLowestPrice(lowestPrice);
		  }
		  
		  InviteSupplier inviteSupplier=new InviteSupplier();
          inviteSupplier.setRcId(rcId);
          inviteSupplier.setIsPriceAa("0");
		  List<InviteSupplier> isList=this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
		  
		  BusinessResponseItem businessResponseItem=new BusinessResponseItem();
		  businessResponseItem.setRcId(rcId);
		  List<BusinessResponseItem> briList=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
		  
		  this.getRequest().setAttribute("rcdList", rcdList);
		  this.getRequest().setAttribute("isList", isList);
		  this.getRequest().setAttribute("briList", briList);
			
		} catch (Exception e) {
			log.error("新建磋商信息错误！", e);
			throw new BaseException("新建磋商信息错误！", e);
		}		
		return ADD_INIT ;		
	}
	/**
	 * 保存新建磋商信息
	 * @return
	 * @throws BaseException
	 */	
	public String saveNegotiate() throws BaseException{
		try{
			String negotiateReturnDateStr=this.getRequest().getParameter("negotiateReturnDateStr");
			Date negotiateReturnDate=DateUtil.StringToDate(negotiateReturnDateStr,"yyyy-MM-dd HH:mm");
			String[] supplierIds=this.getRequest().getParameterValues("supplierId");
			String[] rcdIds=this.getRequest().getParameterValues("rcdId");
			String[] briIds=this.getRequest().getParameterValues("briId");
			String userName=UserRightInfoUtil.getUserName(getRequest());
			String userNameCn=UserRightInfoUtil.getChineseName(this.getRequest());
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			int i=0;
			for(String str:supplierIds){
				bidNegotiate=new BidNegotiate();
				bidNegotiate.setWriteDate(new Date());
				bidNegotiate.setWriter(userName);
				bidNegotiate.setNegotiateReturnDate(negotiateReturnDate);
				bidNegotiate.setRcId(rcId);
				bidNegotiate.setSupplierId(Long.parseLong(str));
				bidNegotiate.setStatus(TableStatus.COMMON_1);
				this.iBidNegotiateBiz.saveBidNegotiate(bidNegotiate);
				
				//价格磋商
				if(rcdIds!=null){
				for(String rcdId:rcdIds){
					String expectPrice=this.getRequest().getParameter("expectPrice_"+rcdId);
					String lowestPrice=this.getRequest().getParameter("lowestPrice_"+rcdId);
					bidPriceNegotiate=new  BidPriceNegotiate();
					bidPriceNegotiate.setBnId(bidNegotiate.getBnId());
					if(StringUtil.isNotBlank(expectPrice)) bidPriceNegotiate.setExpectPrice(Double.parseDouble(expectPrice));
					if(StringUtil.isNotBlank(lowestPrice)) bidPriceNegotiate.setLowestPrice(Double.parseDouble(lowestPrice));
					bidPriceNegotiate.setRcdId(Long.parseLong(rcdId));
					bidPriceNegotiate.setSupplierId(Long.parseLong(str));
					this.iBidPriceNegotiateBiz.saveBidPriceNegotiate(bidPriceNegotiate);
				}
				}
				//商务磋商
				if(briIds!=null){
				for(String briId:briIds){
					String expectResponse=this.getRequest().getParameter("expectResponse_"+briId);
					bidResponseNegotiate=new  BidResponseNegotiate();
					bidResponseNegotiate.setBnId(bidNegotiate.getBnId());
					bidResponseNegotiate.setExpectResponse(expectResponse);
					bidResponseNegotiate.setBriId(Long.parseLong(briId));
					bidResponseNegotiate.setSupplierId(Long.parseLong(str));
					this.iBidResponseNegotiateBiz.saveBidResponseNegotiate(bidResponseNegotiate);
				}
				}
				SystemConfiguration sysConfiguration=new SystemConfiguration();
				if(i==0) {
					requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId); 
					sysConfiguration=BaseDataInfosUtil.convertSystemConfiguration(requiredCollect.getComId()); 
				}else {
					i=1;
				}
				supplierInfo=this.iSupplierInfoBiz.getSupplierInfo(Long.parseLong(str));
				String subject="邮件提醒："+sysConfiguration.getSystemCurrentDept()+"邀请您参与磋商谈判",content="项目编号:"+requiredCollect.getBidCode()+"，项目名称:"+requiredCollect.getBuyRemark();
				String smsTitle=sysConfiguration.getSystemCurrentDept()+"邀请您参与"+requiredCollect.getBuyRemark()+"磋商谈判，项目编号："+requiredCollect.getBidCode(); 
				this.saveSendMailToUsers(supplierInfo.getContactEmail(), subject, content,userNameCn);
				this.saveSmsMessageToUsers(supplierInfo.getSupplierPhone(), smsTitle, userNameCn);
				this.saveSysMessage(userNameCn, userName, supplierInfo.getSupplierName(), supplierInfo.getSupplierId()+"", 1, smsTitle, comId);
			}
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "新建磋商信息成功");
			
		} catch (Exception e) {
			log.error("保存新建磋商信息错误！", e);
			throw new BaseException("保存新建磋商信息错误！", e);
		}		
		return ADD_INIT ;		
	}
	public IBidNegotiateBiz getiBidNegotiateBiz() {
		return iBidNegotiateBiz;
	}
	public void setiBidNegotiateBiz(IBidNegotiateBiz iBidNegotiateBiz) {
		this.iBidNegotiateBiz = iBidNegotiateBiz;
	}
	public IBidResponseNegotiateBiz getiBidResponseNegotiateBiz() {
		return iBidResponseNegotiateBiz;
	}
	public void setiBidResponseNegotiateBiz(
			IBidResponseNegotiateBiz iBidResponseNegotiateBiz) {
		this.iBidResponseNegotiateBiz = iBidResponseNegotiateBiz;
	}
	public IBidPriceNegotiateBiz getiBidPriceNegotiateBiz() {
		return iBidPriceNegotiateBiz;
	}
	public void setiBidPriceNegotiateBiz(IBidPriceNegotiateBiz iBidPriceNegotiateBiz) {
		this.iBidPriceNegotiateBiz = iBidPriceNegotiateBiz;
	}
	public BidNegotiate getBidNegotiate() {
		return bidNegotiate;
	}
	public void setBidNegotiate(BidNegotiate bidNegotiate) {
		this.bidNegotiate = bidNegotiate;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IBusinessResponseItemBiz getiBusinessResponseItemBiz() {
		return iBusinessResponseItemBiz;
	}
	public void setiBusinessResponseItemBiz(
			IBusinessResponseItemBiz iBusinessResponseItemBiz) {
		this.iBusinessResponseItemBiz = iBusinessResponseItemBiz;
	}
	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}
	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	
}
