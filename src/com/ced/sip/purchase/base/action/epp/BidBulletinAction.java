package com.ced.sip.purchase.base.action.epp;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;



import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.biz.IAskBidListBiz;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
import com.ced.sip.purchase.askPrice.util.AskProgressStatus;
import com.ced.sip.purchase.base.biz.IBidBulletinBiz;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBulletin;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.util.TenderProgressStatus;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
/** 
 * 类名称：BidBulletinAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-05
 */
public class BidBulletinAction extends BaseAction {

	@Autowired
	private SnakerEngineFacets facets;
	// 公告表 
	private IBidBulletinBiz iBidBulletinBiz;
    //邀请供应商服务类
    private IInviteSupplierBiz iInviteSupplierBiz;
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
    //标段流程记录实例表
    private IBidProcessLogBiz iBidProcessLogBiz;
	//项目日志记录服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;

	private IAttachmentBiz iAttachmentBiz;
	
	private IAskBidListBiz iAskBidListBiz;
	
	private ITenderBidListBiz iTenderBidListBiz;
	
	private IBiddingBidListBiz iBiddingBidListBiz;
    //商务响应项服务类
    private IBusinessResponseItemBiz iBusinessResponseItemBiz;
	
    private Long rcId;
    private String isDetail;
     
	// 公告表
	private BidBulletin bidBulletin;
    private RequiredCollect requiredCollect;
  	private BidProcessLog bidProcessLog;
    private PurchaseRecordLog purchaseRecordLog;
    
    private TenderBidList tenderBidList;
    private AskBidList askBidList;
    private BiddingBidList biddingBidList;

 	/**
 	* 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
 	* 公告发布完成之后需要同时更新几个节点，更新的节点并不更新完成时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLogNotComplete(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    //bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 查看公告表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidBulletinBidMonitor() throws BaseException {
		
		String view="bidBulletinMonitorDetail";
		try{

			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			String title=BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay())+"公告";
			boolean flag=isWriter(requiredCollect.getWriter());
			if(!flag) isDetail="detail";
			
			bidBulletin = new BidBulletin();
			bidBulletin.setRcId(rcId);
			List<BidBulletin> list=this.iBidBulletinBiz.getBidBulletinList(bidBulletin);
			if(list.size() >0 ){//公告存在
				bidBulletin = list.get(0);
			}else{
				String contentString=getBidBulletionContent(requiredCollect);
				
				bidBulletin.setBulletinContent(contentString);
				bidBulletin.setBidCode(requiredCollect.getBidCode());
				bidBulletin.setBuyWay(requiredCollect.getBuyWay());
				bidBulletin.setStatus(TableStatus.NOTICE_TYPE_1);
				bidBulletin.setBulletinTitle(requiredCollect.getBuyRemark()+title);
			}
			this.getRequest().setAttribute("title", title);
			 //公告未发布 且项目状态为正常 且非查看页面
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.NOTICE_TYPE_1.equals(requiredCollect.getIsSendNotice())&&StringUtil.isBlank(isDetail))
			{   
            	//获取附件
    			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment(bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201 ) );
    			bidBulletin.setUuIdData((String)map.get("uuIdData"));
    			bidBulletin.setFileNameData((String)map.get("fileNameData"));
    			bidBulletin.setFileTypeData((String)map.get("fileTypeData"));
    			bidBulletin.setAttIdData((String)map.get("attIdData"));
            	view="bidBulletinMonitorUpdate";
            	
            	//以下为获取采购项目审批信息
            	Long comId=UserRightInfoUtil.getComId(getRequest());
    		    SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
    		    this.getRequest().setAttribute("work_flow_type",
    					WorkFlowStatus.RC_WorkFlow_Type);
    		    if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
    			    this.getRequest().setAttribute("requiredcollectWorkflow", systemConfiguration.getRequiredcollect00Workflow());
    			    if(StringUtil.convertNullToBlank(systemConfiguration.getRequiredcollect00Workflow()).equals("0")){
    					setProcessLisRc(requiredCollect, WorkFlowStatus.RequiredCollect_Work_Item,comId);
    				}				
    		    }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
    			    this.getRequest().setAttribute("requiredcollectWorkflow", systemConfiguration.getRequiredcollect01Workflow());
    			    if(StringUtil.convertNullToBlank(systemConfiguration.getRequiredcollect01Workflow()).equals("0")){
    					setProcessLisRc(requiredCollect, WorkFlowStatus.RequiredCollect_Work_Item,comId);
    				}
    		    }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
    			    this.getRequest().setAttribute("requiredcollectWorkflow", systemConfiguration.getRequiredcollect02Workflow());
    			    if(StringUtil.convertNullToBlank(systemConfiguration.getRequiredcollect02Workflow()).equals("0")){
    					setProcessLisRc(requiredCollect, WorkFlowStatus.RequiredCollect_Work_Item,comId);
    				}
    		    }
            	
			}else{
				bidBulletin.setStatusCn(TableStatusMap.noticeMap.get(bidBulletin.getStatus()));
				bidBulletin.setPublisherCn(BaseDataInfosUtil.convertLoginNameToChnName(bidBulletin.getPublisher()));
				//获取附件
				bidBulletin.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201 ) ) , "0", this.getRequest() ) ) ;
		         boolean isChange=false;
		        if(flag){
		        	if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
		        		tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(rcId);
		        		if(tenderBidList.getOpenStatus().equals(TableStatus.OPEN_STATUS_01)) isChange=true;
		        	}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
		        		askBidList=this.iAskBidListBiz.getAskBidListByRcId(rcId);
		        		if(askBidList.getPriceStatus().equals(TableStatus.PRICE_STATUS_01)) isChange=true;
		        	}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
		        		biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
		        		if(StringUtil.isBlank(biddingBidList.getBiddingRealTime())) isChange=true;
		        	}
		        }
		        this.getRequest().setAttribute("isChange", isChange);
			       
			}

	        this.getRequest().setAttribute("requiredCollect", requiredCollect);
			
		} catch (Exception e) {
			log.error("查看公告表信息列表错误！", e);
			throw new BaseException("查看公告表信息列表错误！", e);
		}	
			
		return view ;				
	}
	/**
	 * 公告信息生成
	 * @param requiredCollect
	 * @return
	 * @throws BaseException
	 */
	private String getBidBulletionContent(RequiredCollect requiredCollect)throws BaseException{
		String contentString="";
		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			
			String bidlistdetail="",bussResponsedetail="";
			int i=0;
			//得到物资明细
			List<RequiredCollectDetail> listDetails=this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId());
			for(RequiredCollectDetail reCollectDetail:listDetails){
				i=i+1;
				bidlistdetail+= "<tr>"+
				   "<td>"+i+"</td>"+
				   "<td nowrap>"+reCollectDetail.getBuyName()+"</td>"+
				   "<td nowrap>"+StringUtil.convertNullToBlank(reCollectDetail.getMaterialType())+"</td>"+
				   "<td nowrap>"+reCollectDetail.getUnit()+"</td>"+
				   "<td nowrap>"+reCollectDetail.getAmount()+"</td>"+
				   "</tr>";
			}
			i=0;
			// 得到响应项明细
			BusinessResponseItem businessResponseItem=new BusinessResponseItem();
			businessResponseItem.setRcId(requiredCollect.getRcId());
			List<BusinessResponseItem> businessResponseItems=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
			for(BusinessResponseItem responseItem:businessResponseItems){
				i=i+1;
				bussResponsedetail+= "<tr>"+
				   "<td>"+i+"</td>"+
				   "<td nowrap>"+responseItem.getResponseItemName()+"</td>"+
				   "<td nowrap>"+responseItem.getResponseRequirements()+"</td>"+
				   "</tr>";
			}
			
			
			if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>"+systemConfiguration.getSystemCurrentDept()+"对"+requiredCollect.getBuyRemark()+"项目进行招标，诚邀具有相关资质的单位前来投标，具体情况如下:"+
				    "</p>"+
					"<p>　　一、招标内容</p>"+
					"<p>　　1、项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　2、项目标名："+requiredCollect.getBuyRemark()+"</p>"+
			
					"<p>　　二、招标清单</p>"+
			
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>物资名称</td>"+
					   " <td>规格型号</td>"+
					   " <td>单位</td>"+
					  "  <td>数量</td>"+
					 "</tr>"+bidlistdetail+
					  
					 
					 "</table>"+
					   
					"<p>　　三、报名须知</p>"+
					"<p>　　   1、 招标文件发售时间："+DateUtil.getStringFromDate(tenderBidList.getSalesDate())+" - - "+DateUtil.getStringFromDate(tenderBidList.getSaleeDate())+"</p>"+
			        "<p>　　   2、 报名方式：登录“电子采购平台”（http://www.yunqicai.cn/）进行报名。请未注册的供应商及时办理注册审核。因未及时办理注册审核手续影响报名及报价的，责任自负</p>"+
					  
			
					"<p>　　四、报价须知</p>"+
					"<p>　　    1、投标截止时间："+DateUtil.getStringFromDate(tenderBidList.getReturnDate())+"</p>"+
					"<p>　　    2、报名完成之后须在投标截止之间之前进行投标报价，逾期后果自负</p>"+
					"<p>　　    3、开标时间："+DateUtil.getStringFromDate(tenderBidList.getOpenDate())+"</p>"+
					"<p>　　    4、报价须响应条件：</p>"+
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>响应项名称</td>"+
					   " <td>响应项内容</td>"+
					 "</tr>"+bussResponsedetail+
					  
					 
					 "</table>"+
					 
					 "<p>　　五、注意事项</p>"+
						"<p>　　   1、供应商如有疑问可以在线提问并在线查看答疑澄清；</p>"+
				        "<p>　　   2、供应商应合理安排报名时间，特别是网络速度慢的地区为防止在报名结束前网络拥堵无法操作。如果因计算机及网络故障无法报名，责任自负；</p>"+
				        "<p>　　   3、报名及报价过程中如有任何平台操作问题，请联系平台客服；</p>"+
				    
				     "<p>　　六、联系方式</p>"+
						"<p>　　   1、采购单位："+systemConfiguration.getSystemCurrentDept()+"</p>"+
				        "<p>　　   2、联系人："+tenderBidList.getResponsibleUser()+"</p>"+
				        "<p>　　   3、联系电话:"+tenderBidList.getResponsiblePhone()+"</p>"+
				          
					 "</div>";
			    
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				askBidList=this.iAskBidListBiz.getAskBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>"+systemConfiguration.getSystemCurrentDept()+"对"+requiredCollect.getBuyRemark()+"项目进行询价，诚邀具有相关资质的单位前来报价，具体情况如下:"+
				    "</p>"+
					"<p>　　一、询价内容</p>"+
					"<p>　　1、项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　2、项目标名："+requiredCollect.getBuyRemark()+"</p>"+
			
					"<p>　　二、询价清单</p>"+
			
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>物资名称</td>"+
					   " <td>规格型号</td>"+
					   " <td>单位</td>"+
					  "  <td>数量</td>"+
					 "</tr>"+bidlistdetail+
					  
					 
					 "</table>"+
					   
					"<p>　　三、报名须知</p>"+
					"<p>　　   1、 报名截止时间："+DateUtil.getStringFromDate(askBidList.getReturnDate())+"</p>"+
			        "<p>　　   2、 报名方式：登录“电子采购平台”（http://www.yunqicai.cn）进行报名。请未注册的供应商及时办理注册审核。因未及时办理注册审核手续影响报名及报价的，责任自负</p>"+
					  
			
					"<p>　　四、报价须知</p>"+
					"<p>　　    1、报价截止时间："+DateUtil.getStringFromDate(askBidList.getReturnDate())+"</p>"+
					"<p>　　    2、报名完成之后须在报价截止之间之前进行投标报价，逾期后果自负</p>"+
					"<p>　　    3、报价解密时间："+DateUtil.getStringFromDate(askBidList.getDecryptionDate())+"</p>"+
					"<p>　　    4、报价须响应条件：</p>"+
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>响应项名称</td>"+
					   " <td>响应项内容</td>"+
					 "</tr>"+bussResponsedetail+
					  
					 
					 "</table>"+
					 
					 "<p>　　五、注意事项</p>"+
						"<p>　　   1、供应商如有疑问可以在线提问并在线查看答疑澄清；</p>"+
				        "<p>　　   2、供应商应合理安排报名时间，特别是网络速度慢的地区为防止在报名结束前网络拥堵无法操作。如果因计算机及网络故障无法报名，责任自负；</p>"+
				        "<p>　　   3、报名及报价过程中如有任何平台操作问题，请联系平台客服；</p>"+
				    
				     "<p>　　六、联系方式</p>"+
						"<p>　　   1、采购单位："+systemConfiguration.getSystemCurrentDept()+"</p>"+
				        "<p>　　   2、联系人："+askBidList.getResponsibleUser()+"</p>"+
				        "<p>　　   3、联系电话:"+askBidList.getResponsiblePhone()+"</p>"+
				          
					 "</div>";
			    
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>"+systemConfiguration.getSystemCurrentDept()+"对"+requiredCollect.getBuyRemark()+"项目进行竞价，诚邀具有相关资质的单位前来参与竞价，具体情况如下:"+
				    "</p>"+
					"<p>　　一、竞价内容</p>"+
					"<p>　　1、项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　2、项目标名："+requiredCollect.getBuyRemark()+"</p>"+
			
					"<p>　　二、竞价清单</p>"+
			
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>物资名称</td>"+
					   " <td>规格型号</td>"+
					   " <td>单位</td>"+
					  "  <td>数量</td>"+
					 "</tr>"+bidlistdetail+
					  
					 
					 "</table>"+
					   
					"<p>　　三、报名须知</p>"+
					"<p>　　   1、 报名截止时间："+DateUtil.getStringFromDate(biddingBidList.getBiddingStartTime())+"</p>"+
			        "<p>　　   2、 报名方式：登录“电子采购平台”（http://www.yunqicai.cn）进行报名。请未注册的供应商及时办理注册审核。因未及时办理注册审核手续影响报名及报价的，责任自负</p>"+
					  
			
					"<p>　　四、报价须知</p>"+
					"<p>　　    1、竞价时间："+DateUtil.getStringFromDate(biddingBidList.getBiddingStartTime())+" - - "+DateUtil.getStringFromDate(biddingBidList.getBiddingEndTime())+"</p>"+
					"<p>　　    2、报名完成之后须在竞价时间参与竞价，逾期后果自负</p>"+
					"<p>　　    3、竞价须响应条件：</p>"+
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>响应项名称</td>"+
					   " <td>响应项内容</td>"+
					 "</tr>"+bussResponsedetail+
					  
					 
					 "</table>"+
					 
					 "<p>　　五、注意事项</p>"+
						"<p>　　   1、供应商如有疑问可以在线提问并在线查看答疑澄清；</p>"+
				        "<p>　　   2、供应商应合理安排报名时间，特别是网络速度慢的地区为防止在报名结束前网络拥堵无法操作。如果因计算机及网络故障无法报名，责任自负；</p>"+
				        "<p>　　   3、报名及竞价过程中如有任何平台操作问题，请联系平台客服；</p>"+
				    
				     "<p>　　六、联系方式</p>"+
						"<p>　　   1、采购单位："+systemConfiguration.getSystemCurrentDept()+"</p>"+
				        "<p>　　   2、联系人："+biddingBidList.getResponsibleUser()+"</p>"+
				        "<p>　　   3、联系电话:"+biddingBidList.getResponsiblePhone()+"</p>"+
				          
					 "</div>";
			    
			}
		} catch (Exception e) {
			log.error("获取公告内容错误！", e);
			throw new BaseException("获取公告内容错误！", e);
		}
		return contentString;
	}
	/**
	 * 修改公告表信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidBulletin() throws BaseException {
		String view="updateBidBulletinSuccess";
		String text="";
		String userName=UserRightInfoUtil.getUserName(getRequest());
		try{
			String status=this.getRequest().getParameter("status");
			if(bidBulletin.getBbId()==null){
				bidBulletin.setWriteDate(DateUtil.getCurrentDateTime());
				bidBulletin.setWriter(userName);
				this.iBidBulletinBiz.saveBidBulletin(bidBulletin);
			}else{
				this.iBidBulletinBiz.updateBidBulletin(bidBulletin);
			}
			//保存附件
			bidBulletin.setAttIdData(bidBulletin.getAttIdData());
			bidBulletin.setUuIdData(bidBulletin.getUuIdData());
			bidBulletin.setFileNameData(bidBulletin.getFileNameData());
			bidBulletin.setFileTypeData(bidBulletin.getFileTypeData());			
			iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( bidBulletin, bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201, userName ) ) ;
			
			// 删除附件
			iAttachmentBiz.deleteAttachments( parseAttachIds( bidBulletin.getAttIds() ) ) ;
			
			
			if(status.equals(TableStatus.NOTICE_TYPE_0)){
				view="success";
				Long comId=UserRightInfoUtil.getComId(getRequest());
				SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
				
				
				if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
					tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(bidBulletin.getRcId());					
					
					bidBulletin.setPublishDate(new Date());
					bidBulletin.setPublisher(userName);
					bidBulletin.setStatus(TableStatus.NOTICE_TYPE_0);
					bidBulletin.setSysCompany(systemConfiguration.getSystemCurrentDept());
					bidBulletin.setComId(comId);
					bidBulletin.setReturnDate(tenderBidList.getReturnDate());
					this.iBidBulletinBiz.updateBidBulletin(bidBulletin);
					

					requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(bidBulletin.getRcId());
					requiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_0);
					requiredCollect.setNoticePublishDate(new Date());
					this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
					
				    text=TenderProgressStatus.Progress_Status_22_Text;
				    //公告发布成功，进入开标前准备阶段
				    updateBidMonitorAndBidProcessLog(bidBulletin.getRcId(), TenderProgressStatus.Progress_Status_22, TenderProgressStatus.Progress_Status_23, TenderProgressStatus.Progress_Status_23_Text);
				    updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), TenderProgressStatus.Progress_Status_23, TenderProgressStatus.Progress_Status_24, TenderProgressStatus.Progress_Status_24_Text);
				    updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), TenderProgressStatus.Progress_Status_24, TenderProgressStatus.Progress_Status_25, TenderProgressStatus.Progress_Status_25_Text);
				    updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), TenderProgressStatus.Progress_Status_25, TenderProgressStatus.Progress_Status_26, TenderProgressStatus.Progress_Status_26_Text);
				    
				    
				}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
					askBidList=this.iAskBidListBiz.getAskBidListByRcId(bidBulletin.getRcId());
					
					bidBulletin.setPublishDate(new Date());
					bidBulletin.setPublisher(userName);
					bidBulletin.setStatus(TableStatus.NOTICE_TYPE_0);
					bidBulletin.setSysCompany(systemConfiguration.getSystemCurrentDept());
					bidBulletin.setComId(comId);
					bidBulletin.setReturnDate(askBidList.getReturnDate());
					this.iBidBulletinBiz.updateBidBulletin(bidBulletin);

					requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(bidBulletin.getRcId());
					requiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_0);
					requiredCollect.setNoticePublishDate(new Date());
					this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
					
					text=AskProgressStatus.Progress_Status_21_Text;
					updateBidMonitorAndBidProcessLog(bidBulletin.getRcId(), AskProgressStatus.Progress_Status_21, AskProgressStatus.Progress_Status_22, AskProgressStatus.Progress_Status_22_Text);
					updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), AskProgressStatus.Progress_Status_22, AskProgressStatus.Progress_Status_23, AskProgressStatus.Progress_Status_23_Text);
					updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), AskProgressStatus.Progress_Status_23, AskProgressStatus.Progress_Status_24, AskProgressStatus.Progress_Status_24_Text);
					 
				}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
					biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(bidBulletin.getRcId());
					
					bidBulletin.setPublishDate(new Date());
					bidBulletin.setPublisher(userName);
					bidBulletin.setStatus(TableStatus.NOTICE_TYPE_0);
					bidBulletin.setSysCompany(systemConfiguration.getSystemCurrentDept());
					bidBulletin.setComId(comId);
					bidBulletin.setReturnDate(biddingBidList.getBiddingStartTime());
					this.iBidBulletinBiz.updateBidBulletin(bidBulletin);

					requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(bidBulletin.getRcId());
					requiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_0);
					requiredCollect.setNoticePublishDate(new Date());
					this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
					
					text=BiddingProgressStatus.Progress_Status_21_Text;
					updateBidMonitorAndBidProcessLog(bidBulletin.getRcId(), BiddingProgressStatus.Progress_Status_21, BiddingProgressStatus.Progress_Status_22, BiddingProgressStatus.Progress_Status_22_Text);
					updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), BiddingProgressStatus.Progress_Status_22, BiddingProgressStatus.Progress_Status_23, BiddingProgressStatus.Progress_Status_23_Text);
					updateBidMonitorAndBidProcessLogNotComplete(bidBulletin.getRcId(), BiddingProgressStatus.Progress_Status_23, BiddingProgressStatus.Progress_Status_24, BiddingProgressStatus.Progress_Status_24_Text);
					
				}
				purchaseRecordLog = new PurchaseRecordLog();
				purchaseRecordLog.setRcId(bidBulletin.getRcId());
				purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
				purchaseRecordLog.setOperatorName(userName);
				purchaseRecordLog.setOperateContent(text+"已发布！");
				purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
				purchaseRecordLog.setBidNode(text);
				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
				
				String userNameCn=UserRightInfoUtil.getChineseName(this.getRequest());
				if(TableStatus.SUPPLIER_TYPE_01.equals(requiredCollect.getSupplierType())){
					String subject="邮件提醒："+bidBulletin.getSysCompany()+"发布的"+bidBulletin.getBidCode()+"项目邀请您参与回标，公告标题："+bidBulletin.getBulletinTitle()+"",content=bidBulletin.getBulletinContent();
					String smsTitle=bidBulletin.getSysCompany()+"发布的"+bidBulletin.getBidCode()+"项目邀请您参与回标，公告标题："+bidBulletin.getBulletinTitle()+"";
					//邀请供应商
					InviteSupplier inviteSupplier = new InviteSupplier();
					inviteSupplier.setRcId(requiredCollect.getRcId());
					List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
					for(InviteSupplier inviteSupplier2:sulist){
						this.saveSendMailToUsers(inviteSupplier2.getSupplierEmail(), subject, content,userNameCn);
						this.saveSmsMessageToUsers(inviteSupplier2.getSupplierPhone(), smsTitle, userNameCn);
						this.saveSysMessage(userNameCn, userName, inviteSupplier2.getSupplierName(), inviteSupplier2.getSupplierId()+"", 1, smsTitle, comId);
					}
				}
			}
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改"+text+"公告表");
		} catch (Exception e) {
			log("修改"+text+"公告表信息错误！", e);
			throw new BaseException("修改"+text+"公告表信息错误！", e);
		}
		return view;
		
	}
	/**
	 * 变更公告页面初始化
	 * @return
	 * @throws BaseException
	 */
	public String changeBidBulletinInit() throws BaseException{
		try{
			bidBulletin=this.iBidBulletinBiz.getBidBulletin(bidBulletin.getBbId());//获取附件
			//获取附件
			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment(bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201 ) );
			bidBulletin.setUuIdData((String)map.get("uuIdData"));
			bidBulletin.setFileNameData((String)map.get("fileNameData"));
			bidBulletin.setFileTypeData((String)map.get("fileTypeData"));
			bidBulletin.setAttIdData((String)map.get("attIdData"));
			
			if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(bidBulletin.getRcId());
				this.getRequest().setAttribute("tenderBidList", tenderBidList);
			}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				askBidList=this.iAskBidListBiz.getAskBidListByRcId(bidBulletin.getRcId());
				this.getRequest().setAttribute("askBidList", askBidList);
			}else{
				biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(bidBulletin.getRcId());
				this.getRequest().setAttribute("biddingBidList", biddingBidList);
			}
		} catch (Exception e) {
			log("变更公告页面初始化信息错误！", e);
			throw new BaseException("变更公告页面初始化信息错误！", e);
		}
		return "changeBidBulletinInit";
	}
	/**
	 * 生成变更公告内容
	 * @return
	 * @throws BaseException
	 */
	public String changeBidBulletinContent() throws BaseException{
		String contentString="";
		try {
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);

			String bidlistdetail="",bussResponsedetail="";
			int i=0;
			//得到物资明细
			List<RequiredCollectDetail> listDetails=this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId());
			for(RequiredCollectDetail reCollectDetail:listDetails){
				i=i+1;
				bidlistdetail+= "<tr>"+
				   "<td>"+i+"</td>"+
				   "<td nowrap>"+reCollectDetail.getBuyName()+"</td>"+
				   "<td nowrap>"+reCollectDetail.getMaterialType()+"</td>"+
				   "<td nowrap>"+reCollectDetail.getUnit()+"</td>"+
				   "<td nowrap>"+reCollectDetail.getAmount()+"</td>"+
				   "</tr>";
			}
			i=0;
			// 得到响应项明细
			BusinessResponseItem businessResponseItem=new BusinessResponseItem();
			businessResponseItem.setRcId(requiredCollect.getRcId());
			List<BusinessResponseItem> businessResponseItems=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
			for(BusinessResponseItem responseItem:businessResponseItems){
				i=i+1;
				bussResponsedetail+= "<tr>"+
				   "<td>"+i+"</td>"+
				   "<td nowrap>"+StringUtil.convertNullToBlank(responseItem.getResponseItemName())+"</td>"+
				   "<td nowrap>"+StringUtil.convertNullToBlank(responseItem.getResponseRequirements())+"</td>"+
				   "</tr>";
			}
			
			
			if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				String salesDate=this.getRequest().getParameter("salesDate");
				String saleeDate=this.getRequest().getParameter("saleeDate");
				String returnDate=this.getRequest().getParameter("returnDate");
				String openDate=this.getRequest().getParameter("openDate");
				tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>"+systemConfiguration.getSystemCurrentDept()+"对"+requiredCollect.getBuyRemark()+"项目进行招标，诚邀具有相关资质的单位前来投标，具体情况如下:"+
				    "</p>"+
					"<p>　　一、招标内容</p>"+
					"<p>　　1、项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　2、项目标名："+requiredCollect.getBuyRemark()+"</p>"+
			
					"<p>　　二、招标清单</p>"+
			
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>物资名称</td>"+
					   " <td>规格型号</td>"+
					   " <td>单位</td>"+
					  "  <td>数量</td>"+
					 "</tr>"+bidlistdetail+
					  
					 
					 "</table>"+
					   
					"<p>　　三、报名须知</p>"+
					"<p>　　   1、 招标文件发售时间："+salesDate+" - - "+saleeDate+"</p>"+
			        "<p>　　   2、 报名方式：登录“电子采购平台”（http://www.yunqicai.cn）进行报名。请未注册的供应商及时办理注册审核。因未及时办理注册审核手续影响报名及报价的，责任自负</p>"+
					  
			
					"<p>　　四、报价须知</p>"+
					"<p>　　    1、投标截止时间："+returnDate+"</p>"+
					"<p>　　    2、报名完成之后须在投标截止之间之前进行投标报价，逾期后果自负</p>"+
					"<p>　　    3、开标时间："+openDate+"</p>"+
					"<p>　　    4、报价须响应条件：</p>"+
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>响应项名称</td>"+
					   " <td>响应项内容</td>"+
					 "</tr>"+bussResponsedetail+
					  
					 
					 "</table>"+
					 
					 "<p>　　五、注意事项</p>"+
						"<p>　　   1、供应商如有疑问可以在线提问并在线查看答疑澄清；</p>"+
				        "<p>　　   2、供应商应合理安排报名时间，特别是网络速度慢的地区为防止在报名结束前网络拥堵无法操作。如果因计算机及网络故障无法报名，责任自负；</p>"+
				        "<p>　　   3、报名及报价过程中如有任何平台操作问题，请联系平台客服；</p>"+
				    
				     "<p>　　六、联系方式</p>"+
						"<p>　　   1、采购单位："+systemConfiguration.getSystemCurrentDept()+"</p>"+
				        "<p>　　   2、联系人："+tenderBidList.getResponsibleUser()+"</p>"+
				        "<p>　　   3、联系电话:"+tenderBidList.getResponsiblePhone()+"</p>"+
				          
					 "</div>";
			    
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				//String askDate=this.getRequest().getParameter("askDate");
				String returnDate=this.getRequest().getParameter("returnDate");
				String decryptionDate=this.getRequest().getParameter("decryptionDate");
				
				askBidList=this.iAskBidListBiz.getAskBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>"+systemConfiguration.getSystemCurrentDept()+"对"+requiredCollect.getBuyRemark()+"项目进行询价，诚邀具有相关资质的单位前来报价，具体情况如下:"+
				    "</p>"+
					"<p>　　一、询价内容</p>"+
					"<p>　　1、项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　2、项目标名："+requiredCollect.getBuyRemark()+"</p>"+
			
					"<p>　　二、询价清单</p>"+
			
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>物资名称</td>"+
					   " <td>规格型号</td>"+
					   " <td>单位</td>"+
					  "  <td>数量</td>"+
					 "</tr>"+bidlistdetail+
					  
					 
					 "</table>"+
					   
					"<p>　　三、报名须知</p>"+
					"<p>　　   1、 报名截止时间："+returnDate+"</p>"+
			        "<p>　　   2、 报名方式：登录“电子采购平台”（http://www.yunqicai.cn）进行报名。请未注册的供应商及时办理注册审核。因未及时办理注册审核手续影响报名及报价的，责任自负</p>"+
					  
			
					"<p>　　四、报价须知</p>"+
					"<p>　　    1、报价截止时间："+returnDate+"</p>"+
					"<p>　　    2、报名完成之后须在报价截止之间之前进行投标报价，逾期后果自负</p>"+
					"<p>　　    3、报价解密时间："+decryptionDate+"</p>"+
					"<p>　　    4、报价须响应条件：</p>"+
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>响应项名称</td>"+
					   " <td>响应项内容</td>"+
					 "</tr>"+bussResponsedetail+
					  
					 
					 "</table>"+
					 
					 "<p>　　五、注意事项</p>"+
						"<p>　　   1、供应商如有疑问可以在线提问并在线查看答疑澄清；</p>"+
				        "<p>　　   2、供应商应合理安排报名时间，特别是网络速度慢的地区为防止在报名结束前网络拥堵无法操作。如果因计算机及网络故障无法报名，责任自负；</p>"+
				        "<p>　　   3、报名及报价过程中如有任何平台操作问题，请联系平台客服；</p>"+
				    
				     "<p>　　六、联系方式</p>"+
						"<p>　　   1、采购单位："+systemConfiguration.getSystemCurrentDept()+"</p>"+
				        "<p>　　   2、联系人："+askBidList.getResponsibleUser()+"</p>"+
				        "<p>　　   3、联系电话:"+askBidList.getResponsiblePhone()+"</p>"+
				          
					 "</div>";
			    
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){

				String biddingStartTime=this.getRequest().getParameter("biddingStartTime");
				String biddingEndTime=this.getRequest().getParameter("biddingEndTime");
				
				biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>"+systemConfiguration.getSystemCurrentDept()+"对"+requiredCollect.getBuyRemark()+"项目进行竞价，诚邀具有相关资质的单位前来参与竞价，具体情况如下:"+
				    "</p>"+
					"<p>　　一、竞价内容</p>"+
					"<p>　　1、项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　2、项目标名："+requiredCollect.getBuyRemark()+"</p>"+
			
					"<p>　　二、竞价清单</p>"+
			
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>物资名称</td>"+
					   " <td>规格型号</td>"+
					   " <td>单位</td>"+
					  "  <td>数量</td>"+
					 "</tr>"+bidlistdetail+
					  
					 
					 "</table>"+
					   
					"<p>　　三、报名须知</p>"+
					"<p>　　   1、 报名截止时间："+biddingEndTime+"</p>"+
			        "<p>　　   2、 报名方式：登录“电子采购平台”（http://www.yunqicai.cn）进行报名。请未注册的供应商及时办理注册审核。因未及时办理注册审核手续影响报名及报价的，责任自负</p>"+
					  
			
					"<p>　　四、报价须知</p>"+
					"<p>　　    1、竞价时间："+biddingStartTime+" - - "+biddingEndTime+"</p>"+
					"<p>　　    2、报名完成之后须在竞价时间参与竞价，逾期后果自负</p>"+
					"<p>　　    3、竞价须响应条件：</p>"+
					"<table width=\"60%\" id=\"tbl\" border=\"1\"  cellspacing=\"0\" bordercolor=\"#000000\" cellpadding=\"2\">"+
					"<tr class=\"trhead\">"+
					   " <td>序号</td>"+
					   " <td>响应项名称</td>"+
					   " <td>响应项内容</td>"+
					 "</tr>"+bussResponsedetail+
					  
					 
					 "</table>"+
					 
					 "<p>　　五、注意事项</p>"+
						"<p>　　   1、供应商如有疑问可以在线提问并在线查看答疑澄清；</p>"+
				        "<p>　　   2、供应商应合理安排报名时间，特别是网络速度慢的地区为防止在报名结束前网络拥堵无法操作。如果因计算机及网络故障无法报名，责任自负；</p>"+
				        "<p>　　   3、报名及竞价过程中如有任何平台操作问题，请联系平台客服；</p>"+
				    
				     "<p>　　六、联系方式</p>"+
						"<p>　　   1、采购单位："+systemConfiguration.getSystemCurrentDept()+"</p>"+
				        "<p>　　   2、联系人："+biddingBidList.getResponsibleUser()+"</p>"+
				        "<p>　　   3、联系电话:"+biddingBidList.getResponsiblePhone()+"</p>"+
				          
					 "</div>";
			    
			}
			PrintWriter out = this.getResponse().getWriter();			
			out.println(contentString);
		} catch (Exception e) {
			log("生成变更公告内容错误！", e);
			throw new BaseException("生成变更公告内容错误！", e);
		}
		return null;
	}
	/**
	 * 变更公告表保存信息
	 * @return
	 * @throws BaseException 
	 */
	public String changeBidBulletin() throws BaseException {
		String view="success";
		String text="";
		String userName=UserRightInfoUtil.getUserName(getRequest());
		try{
			if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				//变更开标日期等
				String openDateStr=this.getRequest().getParameter("openDateStr");
				String salesDateStr=this.getRequest().getParameter("salesDateStr");
				String saleeDateStr=this.getRequest().getParameter("saleeDateStr");
				String returnDateStr=this.getRequest().getParameter("returnDateStr");
				
				tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(bidBulletin.getRcId());

				tenderBidList.setOpenDate(DateUtil.StringToDate(openDateStr,"yyyy-MM-dd HH:mm"));
				tenderBidList.setSalesDate(DateUtil.StringToDate(salesDateStr,"yyyy-MM-dd HH:mm"));
				tenderBidList.setSaleeDate(DateUtil.StringToDate(saleeDateStr,"yyyy-MM-dd HH:mm"));
				tenderBidList.setReturnDate(DateUtil.StringToDate(returnDateStr,"yyyy-MM-dd HH:mm"));

				bidBulletin.setWriteDate(DateUtil.getCurrentDateTime());
				bidBulletin.setWriter(userName);
				bidBulletin.setPublishDate(new Date());
				bidBulletin.setPublisher(userName);
				bidBulletin.setStatus(TableStatus.NOTICE_TYPE_0);				
				bidBulletin.setReturnDate(tenderBidList.getReturnDate());				
				
				this.iBidBulletinBiz.saveBidBulletin(bidBulletin);

				this.iTenderBidListBiz.updateTenderBidList(tenderBidList);

				
				text=TenderProgressStatus.Progress_Status_22_Text;
				
			}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				//变更询价日期等
				String askDateStr=this.getRequest().getParameter("askDateStr");
				String returnDateStr=this.getRequest().getParameter("returnDateStr");
				String decryptionDateStr=this.getRequest().getParameter("decryptionDateStr");
				
				askBidList=this.iAskBidListBiz.getAskBidListByRcId(bidBulletin.getRcId());
				
				askBidList.setAskDate(DateUtil.StringToDate(askDateStr,"yyyy-MM-dd HH:mm"));
				askBidList.setDecryptionDate(DateUtil.StringToDate(decryptionDateStr,"yyyy-MM-dd HH:mm"));
				askBidList.setReturnDate(DateUtil.StringToDate(returnDateStr,"yyyy-MM-dd HH:mm"));

				bidBulletin.setWriteDate(DateUtil.getCurrentDateTime());
				bidBulletin.setWriter(userName);
				bidBulletin.setPublishDate(new Date());
				bidBulletin.setPublisher(userName);
				bidBulletin.setStatus(TableStatus.NOTICE_TYPE_0);				
				bidBulletin.setReturnDate(askBidList.getReturnDate());
				
				this.iBidBulletinBiz.saveBidBulletin(bidBulletin);

				this.iAskBidListBiz.updateAskBidList(askBidList);

				text=AskProgressStatus.Progress_Status_21_Text;
				
			}else if(bidBulletin.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				String biddingStartTimeStr=this.getRequest().getParameter("biddingStartTimeStr");
				String biddingEndTimeStr=this.getRequest().getParameter("biddingEndTimeStr");
				
				biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(bidBulletin.getRcId());

				biddingBidList.setBiddingStartTime(DateUtil.StringToDate(biddingStartTimeStr,"yyyy-MM-dd HH:mm"));
				biddingBidList.setBiddingEndTime(DateUtil.StringToDate(biddingEndTimeStr,"yyyy-MM-dd HH:mm"));
				
				bidBulletin.setWriteDate(DateUtil.getCurrentDateTime());
				bidBulletin.setWriter(userName);
				bidBulletin.setPublishDate(new Date());
				bidBulletin.setPublisher(userName);
				bidBulletin.setStatus(TableStatus.NOTICE_TYPE_0);				
				bidBulletin.setReturnDate(biddingBidList.getBiddingStartTime());
				
				this.iBidBulletinBiz.saveBidBulletin(bidBulletin);
				
				this.iBiddingBidListBiz.updateBiddingBidList(biddingBidList);

				text=BiddingProgressStatus.Progress_Status_21_Text;
			}
			requiredCollect=new RequiredCollect();
			requiredCollect.setRcId(bidBulletin.getRcId());
			requiredCollect.setChangeNum(1L);
			requiredCollect.setNoticePublishDate(new Date());
			this.iRequiredCollectBiz.updateRequiredCollectNodeByRequiredCollect(requiredCollect);
			
			//保存附件
			bidBulletin.setAttIdData(bidBulletin.getAttIdData());
			bidBulletin.setUuIdData(bidBulletin.getUuIdData());
			bidBulletin.setFileNameData(bidBulletin.getFileNameData());
			bidBulletin.setFileTypeData(bidBulletin.getFileTypeData());			
			iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( bidBulletin, bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201, userName) ) ;
			
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRcId(bidBulletin.getRcId());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(userName);
			purchaseRecordLog.setOperateContent(text+"已发布！");
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setBidNode(text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);

			String userNameCn=UserRightInfoUtil.getChineseName(this.getRequest());
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			//if(TableStatus.SUPPLIER_TYPE_01.equals(requiredCollect.getSupplierType())){
				String subject="邮件提醒："+bidBulletin.getSysCompany()+"发布的"+bidBulletin.getBidCode()+"项目邀请您参与回标，公告标题“"+bidBulletin.getBulletinTitle()+"”",content=bidBulletin.getBulletinContent();
				String smsTitle=bidBulletin.getSysCompany()+"发布的"+bidBulletin.getBidCode()+"项目邀请您参与回标，公告标题："+bidBulletin.getBulletinTitle()+""; 
				//邀请供应商
				InviteSupplier inviteSupplier = new InviteSupplier();
				inviteSupplier.setRcId(requiredCollect.getRcId());
				List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
				for(InviteSupplier inviteSupplier2:sulist){
					this.saveSendMailToUsers(inviteSupplier2.getSupplierEmail(), subject, content,userNameCn);
					this.saveSmsMessageToUsers(inviteSupplier2.getSupplierPhone(), smsTitle, userNameCn);
					this.saveSysMessage(userNameCn, userName, inviteSupplier2.getSupplierName(), inviteSupplier2.getSupplierId()+"", 1, smsTitle, comId);
				}
			//}
				
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改"+text+"公告表");
		} catch (Exception e) {
			log("变更"+text+"公告表信息错误！", e);
			throw new BaseException("变更"+text+"公告表信息错误！", e);
		}
		return view;
		
	}
	/**
	 * 查看公告表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidBulletinBidDetail() throws BaseException {
		bidBulletin = new BidBulletin();
		bidBulletin.setRcId(rcId);
		List<BidBulletin> list=this.iBidBulletinBiz.getBidBulletinList(bidBulletin);
		if(list.size() >0 ){//公告存在
			bidBulletin = list.get(0);
			bidBulletin.setStatusCn(TableStatusMap.noticeMap.get(bidBulletin.getStatus()));
			bidBulletin.setPublisherCn(BaseDataInfosUtil.convertLoginNameToChnName(bidBulletin.getPublisher()));
			//获取附件
			bidBulletin.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( bidBulletin.getBbId(), AttachmentStatus.ATTACHMENT_CODE_201 ) ) , "0", this.getRequest() ) ) ;
		}
        this.getRequest().setAttribute("isChange", false);
        return "bidBulletinMonitorDetail";
	}
	/**************************************************************招标项目审批*******************************************************************/

	 /**
	 * 设置计划流程相关列表
	 */
	private void setProcessLisRc(RequiredCollect rc, String processName,Long comId){
		//step1：首先获取流程定义实体
		org.snaker.engine.entity.Process process = facets.getEngine().process().getProcessByName(processName,comId);
		//step2：1、判断流程定义是否存在，2、遍历集合添加流程实例
		if(process != null){
				String newOrderNo=WorkFlowStatus.RC_WorkFlow_Type+rc.getRcId().toString();
				//step3:判断流程实例是否运行
				QueryFilter filter = new QueryFilter();
             filter.setOrderNo(newOrderNo);
				filter.setProcessId(process.getId());
				List<HistoryOrder> holist = facets.getEngine().query().getHistoryOrders(filter);
				if(holist.size() > 0){
					//step5:获取正在运行的流程实例
					filter.setOrderNo(newOrderNo);
					filter.setProcessId(process.getId());
					List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
					if(orderlist.size() > 0){
						Order order = orderlist.get((orderlist.size()-1));
						if(order != null){
							List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
							if(tasklist.size() > 0){
								//设置当前流程名称
								rc.setProcessName(tasklist.get(0).getDisplayName());
							}
							rc.setOrderId(order.getId());//设置实例标示
						}
					}
					HistoryOrder ho = holist.get(0);
					rc.setOrderState(ho.getOrderState().toString());
					rc.setOrderStateName(WorkFlowStatusMap.getWorkflowOrderStatus(ho.getOrderState().toString()));
				}else{
					rc.setOrderStateName(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01_TEXT);
				}
				rc.setProcessId(process.getId());//设置流程标示
				rc.setInstanceUrl(process.getInstanceUrl());//设置流程实例URL
			
		}
	}

	public SnakerEngineFacets getFacets() {
		return facets;
	}

	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}
	public BidBulletin getBidBulletin() {
		return bidBulletin;
	}

	public void setBidBulletin(BidBulletin bidBulletin) {
		this.bidBulletin = bidBulletin;
	}
	public IBidBulletinBiz getiBidBulletinBiz() {
		return iBidBulletinBiz;
	}
	public void setiBidBulletinBiz(IBidBulletinBiz iBidBulletinBiz) {
		this.iBidBulletinBiz = iBidBulletinBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public IAskBidListBiz getiAskBidListBiz() {
		return iAskBidListBiz;
	}
	public void setiAskBidListBiz(IAskBidListBiz iAskBidListBiz) {
		this.iAskBidListBiz = iAskBidListBiz;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}
	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IBusinessResponseItemBiz getiBusinessResponseItemBiz() {
		return iBusinessResponseItemBiz;
	}
	public void setiBusinessResponseItemBiz(
			IBusinessResponseItemBiz iBusinessResponseItemBiz) {
		this.iBusinessResponseItemBiz = iBusinessResponseItemBiz;
	}
	
	
}
