package com.ced.sip.purchase.base.action.epp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.util.AskProgressStatus;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectBidAward;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
import com.ced.sip.purchase.tender.util.TenderProgressStatus;
import com.ced.sip.requirement.biz.IRequiredMaterialBiz;
import com.ced.sip.requirement.entity.RequiredMaterial;
import com.ced.sip.requirement.entity.RequiredMaterialDetail;
import com.ced.sip.system.biz.ICategoryBuyerBiz;
import com.ced.sip.system.biz.IPurchaseDepartBuyerBiz;
import com.ced.sip.system.entity.CategoryBuyer;
import com.ced.sip.system.entity.PurchaseDepartBuyer;
import com.ced.sip.system.entity.SystemConfiguration;
import com.sun.org.apache.commons.beanutils.BeanUtils;

public class PurchaseBaseAction extends BaseAction {
	// 计划
	private IRequiredMaterialBiz iRequiredMaterialBiz;
	//需求汇总分包
	private IRequiredCollectBiz iRequiredCollectBiz;
	//采购类别采购员
	private ICategoryBuyerBiz iCategoryBuyerBiz;
	//采购员所管理的采购组织
	private IPurchaseDepartBuyerBiz iPurchaseDepartBuyerBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;
	//日志服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	// BidMonitor 标段流程记录表
	private IBidProcessLogBiz iBidProcessLogBiz;

	private RequiredMaterial requiredMaterial;
	private RequiredCollect requiredCollect;
	private RequiredCollectBidAward requiredCollectBidAward; 
	private RequiredMaterialDetail requiredMaterialDetail;
	
	private CategoryBuyer categoryBuyer;
	private PurchaseDepartBuyer purchaseDepartBuyer;
	private PurchaseRecordLog purchaseRecordLog;
	private BidProcessLog bidProcessLog;

	/**
	 * 选择未汇总的需求信息
	 * @return
	 */
	public String viewNotCollectedRequiredPlan()throws BaseException{
		
		try{
			
		} catch (Exception e) {
			log("查看未汇总需求信息错误！", e);
			throw new BaseException("查看未汇总需求信息错误！", e);
		}
		
		return "select";
	}
	
	/**
	 * 选择未汇总的需求信息
	 * @return
	 */
	public String findNotCollectedRequiredPlan() throws BaseException{
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(requiredMaterialDetail == null){
				requiredMaterialDetail = new RequiredMaterialDetail();
			}
			if(requiredMaterial == null){
				requiredMaterial = new RequiredMaterial();
			}
			String rmName=this.getRequest().getParameter("rmName");
			requiredMaterial.setRmName(rmName);
			String rmType=this.getRequest().getParameter("rmType");
			requiredMaterial.setRmType(rmType);
			String rmCode=this.getRequest().getParameter("rmCode");
			requiredMaterialDetail.setRmCode(rmCode);
			String buyName=this.getRequest().getParameter("buyName");
			requiredMaterialDetail.setBuyName(buyName);
			String buyCode=this.getRequest().getParameter("buyCode");
			requiredMaterialDetail.setBuyCode(buyCode);
			requiredMaterial.setComId(comId);
			requiredMaterial.setPurchaseDeptId(UserRightInfoUtil.getUserDeptId(this.getRequest()));
			//通过采购员关联的类别查询当前人员可以查看未汇总的信息
			categoryBuyer=new CategoryBuyer();
			categoryBuyer.setBuyerId(UserRightInfoUtil.getUserId(this.getRequest()));
			List<CategoryBuyer> listbuyer=this.iCategoryBuyerBiz.getCategoryBuyerList(categoryBuyer);
			//通过采购员查询所管理的采购组织
			purchaseDepartBuyer=new PurchaseDepartBuyer();
			purchaseDepartBuyer.setBuyerId(UserRightInfoUtil.getUserId(this.getRequest()));
			List<PurchaseDepartBuyer> listpurchase=this.iPurchaseDepartBuyerBiz.getPurchaseDepartBuyerList(purchaseDepartBuyer);
			//如果当前登录人员查询出来的采购类别权限为空,则不能汇总信息
			if(listbuyer.size()>0){
				List<RequiredMaterialDetail> rmdList = iRequiredMaterialBiz.getRequiredMaterialDetailListForRequiredCollect(this.getRollPageDataTables(),requiredMaterialDetail,requiredMaterial,listbuyer,listpurchase);
				if(rmdList!=null&&rmdList.size()>0){
					for(RequiredMaterialDetail rmd:rmdList){
						requiredMaterial = this.iRequiredMaterialBiz.getRequiredMaterial(rmd.getRmId());
						rmd.setRmType(requiredMaterial.getRmType());
						rmd.setRmName(requiredMaterial.getRmName());
						rmd.setDeptName(BaseDataInfosUtil
								.convertDeptIdToName(requiredMaterial.getDeptId()));
					}
				}
				this.getPagejsonDataTables(rmdList);
			}else{
				this.getPagejsonDataTables(null);
			}
			/*List<RequiredMaterialDetail> rmdList = iRequiredMaterialBiz.getRequiredMaterialDetailListForRequiredCollect(this.getRollPageDataTables(),requiredMaterialDetail,requiredMaterial,null,null);
			if(rmdList!=null&&rmdList.size()>0){
				for(RequiredMaterialDetail rmd:rmdList){
					requiredMaterial = this.iRequiredMaterialBiz.getRequiredMaterial(rmd.getRmId());
					rmd.setRmType(requiredMaterial.getRmType());
					rmd.setRmName(requiredMaterial.getRmName());
					rmd.setDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredMaterial.getDeptId()));
				}
			}
			this.getPagejsonDataTables(rmdList);*/
		} catch (Exception e) {
			log("查看未汇总需求信息错误！", e);
			throw new BaseException("查看未汇总需求信息错误！", e);
		}
		return null;
	}

	/***********************************************************采购方式变更********************************************************/
	/**
	 * 查看采购方式变更查看页面
	 * @return
	 */
	public String viewRequiredCollectedForPurchaseChange()throws BaseException{		
		try{
		} catch (Exception e) {
			log("查看采购方式变更查看页面错误！", e);
			throw new BaseException("查看采购方式变更查看页面错误！", e);
		}		
		return "viewPurchaseChange";
	}
	
	/**
	 * 采购方式变更查询
	 * @return
	 */
	public String findRequiredCollectedForPurchaseChange() throws BaseException{
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			// 汇总 列表信息
			requiredCollect = new RequiredCollect();
			String bidCode=this.getRequest().getParameter("bidCode");
			requiredCollect.setBidCode(bidCode);
			String buyRemark=this.getRequest().getParameter("buyRemark");
			requiredCollect.setBuyRemark(buyRemark);
			
			
			String rmCode=this.getRequest().getParameter("rmCode");
			if(StringUtil.isNotBlank(rmCode))
			{
				List<RequiredCollect> list=this.iRequiredCollectBiz.getRequiredCollecListByRmCode(rmCode);
				String condtion=" and (de.rc_id='1' ";
				for(RequiredCollect rc:list){
					condtion+="or de.rc_id='"+rc.getRcId()+"'";
				}
				condtion+=")";
				requiredCollect.setCondition(condtion);
			}
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = " and de.writer= '"+UserRightInfoUtil.getUserName(this.getRequest())+"' ";
			}
			
			this.setListValue( this.iRequiredCollectBiz.getRequiredCollectListForPurchaseChange( this.getRollPageDataTables(), requiredCollect,sqlStr) ) ;
			if(this.getListValue()!=null&&this.getListValue().size()>0){
				for(int i=0;i<this.getListValue().size();i++){
					requiredCollect = (RequiredCollect) this.getListValue().get(i);
					requiredCollect.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
					requiredCollect.setPurchaseDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredCollect
									.getPurchaseDeptId()));
				}
			}
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log("采购方式变更查询错误！", e);
			throw new BaseException("采购方式变更查询错误！", e);
		}
		return null;
	}
	/**
	 * 采购方式变更初始化页面
	 * @return
	 */
	public String purchaseChangeInit()throws BaseException{		
		try{
		    Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
		    requiredCollect = this.iRequiredCollectBiz.getRequiredCollect(rcId);
		    
		    // 获取附件
			Map<String, Object> map = iAttachmentBiz
					.getAttachmentMap(new Attachment(
							requiredCollect.getRcId(),
							AttachmentStatus.ATTACHMENT_CODE_103));
			requiredCollect.setUuIdData((String) map.get("uuIdData"));
			requiredCollect.setFileNameData((String) map.get("fileNameData"));
			requiredCollect.setFileTypeData((String) map.get("fileTypeData"));
			requiredCollect.setAttIdData((String) map.get("attIdData"));
			requiredCollect.setAttIds((String) map.get("attIds"));
			
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getPurchaseDeptId()));
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredCollect.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredCollect.getDeptId()));
			this.getRequest().setAttribute("buyWayCn", BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
			this.getRequest().setAttribute("supplierTypeCn", BaseDataInfosUtil.convertSupplierTypeCnTosupplierType(requiredCollect.getSupplierType()));
			
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			Map<String,String> purchaseType=TableStatusMap.purchaseWay;
			//purchaseType.remove(requiredCollect.getBuyWay());
			this.getRequest().setAttribute("purchaseType", purchaseType);
		} catch (Exception e) {
			log("采购方式变更初始化页面信息错误！", e);
			throw new BaseException("采购方式变更初始化页面信息错误！", e);
		}		
		return "purchaseChangeInit";
	}
	/**
	 * 保存采购方式变更页面  新建一个项目，变更原来的项目状态
	 * @return
	 */
	public String savePurchaseChange()throws BaseException{
		try{
		    Long oldRcId=Long.parseLong(this.getRequest().getParameter("oldRcId"));
		    RequiredCollect oldRequiredCollect = this.iRequiredCollectBiz.getRequiredCollect(oldRcId);
		    
		    RequiredCollect newRequiredCollect=new RequiredCollect();
		    
		    BeanUtils.copyProperties(newRequiredCollect,oldRequiredCollect);
		    newRequiredCollect.setRcId(null);
		    newRequiredCollect.setBidCode(oldRequiredCollect.getBidCode()+"(变)");
		    newRequiredCollect.setBuyWay(requiredCollect.getBuyWay());
		    if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
		    	newRequiredCollect.setServiceStatus(TenderProgressStatus.Progress_Status_20);
		    	newRequiredCollect.setServiceStatusCn(TenderProgressStatus.Progress_Status_20_Text);
		    }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
		    	newRequiredCollect.setServiceStatus(AskProgressStatus.Progress_Status_20);
		    	newRequiredCollect.setServiceStatusCn(AskProgressStatus.Progress_Status_20_Text);
		    }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
		    	newRequiredCollect.setServiceStatus(BiddingProgressStatus.Progress_Status_20);
		    	newRequiredCollect.setServiceStatusCn(BiddingProgressStatus.Progress_Status_20_Text);
		    }
		    newRequiredCollect.setIsSendNotice(TableStatus.NOTICE_TYPE_1);
		    this.iRequiredCollectBiz.saveRequiredCollect(newRequiredCollect);
		    
		    bidProcessLog=new BidProcessLog();
		    bidProcessLog.setRcId(newRequiredCollect.getRcId());
			bidProcessLog.setBidNode(newRequiredCollect.getServiceStatus());
			bidProcessLog.setReceiveDate(new Date());
			this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
			
			newRequiredCollect.setAttIdData(requiredCollect.getAttIdData());
			newRequiredCollect.setAttIds(requiredCollect.getAttIds());
			newRequiredCollect.setFileNameData(requiredCollect.getFileNameData());
			newRequiredCollect.setFileTypeData(requiredCollect.getFileTypeData());
			newRequiredCollect.setUuIdData(requiredCollect.getUuIdData());
			
		    // 保存附件
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					newRequiredCollect, newRequiredCollect.getRcId(),
					AttachmentStatus.ATTACHMENT_CODE_103, UserRightInfoUtil
							.getUserName(this.getRequest())));
		    
			
			this.iRequiredCollectBiz.updatePurchaseChangeDetail(oldRcId, newRequiredCollect.getRcId(), newRequiredCollect.getBidCode());
			
		    oldRequiredCollect.setBidCode(oldRequiredCollect.getBidCode()+"(旧)");
		    oldRequiredCollect.setChangeReason(requiredCollect.getChangeReason());
		    oldRequiredCollect.setBidStatus(TableStatus.BID_STATUS_7);
		    oldRequiredCollect.setBidStatusCn(TableStatus.BID_STATUS_7_TEXT);
		    this.iRequiredCollectBiz.updateRequiredCollect(oldRequiredCollect);
		    
		    String operateContent = "采购项目变更成功，项目编号为【"+newRequiredCollect.getBidCode()+"】";
			purchaseRecordLog = new PurchaseRecordLog(null, null,newRequiredCollect.getRcId(), UserRightInfoUtil.getUserId(this.getRequest())+"", UserRightInfoUtil.getUserName(this.getRequest()), DateUtil.getCurrentDateTime(), operateContent,"采购项目变更");
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
		    this.getRequest().setAttribute("message", "采购方式变更成功");
			this.getRequest().setAttribute("operModule", "采购方式变更信息");
		} catch (Exception e) {
			log("保存采购方式变更页面错误！", e);
			throw new BaseException("保存采购方式变更页面错误！", e);
		}		
		return "success";
	}
	/***********************************************************合同选择采购项目********************************************************/
	/**
	 * 选择采购项目信息
	 * @return
	 */
	public String viewRequiredCollectedForContractAndOrder()throws BaseException{		
		try{
			int sign=Integer.parseInt(this.getRequest().getParameter("sign"));
			this.getRequest().setAttribute("sign", sign);
			this.getRequest().setAttribute("type", 1);
		} catch (Exception e) {
			log("查看采购项目信息错误！", e);
			throw new BaseException("查看采购项目信息错误！", e);
		}		
		return "selectRc";
	}
	
	/**
	 * 选择采购项目信息
	 * @return
	 */
	public String findRequiredCollectedForContractAndOrder() throws BaseException{
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			int sign=Integer.parseInt(this.getRequest().getParameter("sign"));
			// 汇总 列表信息
			Map<String,Object> map= new HashMap<String,Object>();
			String bidCode=this.getRequest().getParameter("bidCode");
			map.put("bidCode", bidCode);
			String buyRemark=this.getRequest().getParameter("buyRemark");
			map.put("buyRemark", buyRemark);
			String supplierName=this.getRequest().getParameter("supplierName");
			map.put("supplierName", supplierName);
		
			map.put("comId", comId);
			
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			List<RequiredCollectBidAward> rcList=new ArrayList<RequiredCollectBidAward>();
			List<Object[]> list=this.iRequiredCollectBiz.getRequiredCollectListForCollectAndOrder( this.getRollPageDataTables(), map,sqlStr,sign);
			
			for(Object[] obj:list){
				requiredCollect = (RequiredCollect)obj[0];
				requiredCollectBidAward= new RequiredCollectBidAward();
				BeanUtils.copyProperties(requiredCollectBidAward,requiredCollect);
				requiredCollectBidAward.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(requiredCollectBidAward.getWriter()));
				requiredCollectBidAward.setPurchaseDeptName(BaseDataInfosUtil
						.convertDeptIdToName(requiredCollectBidAward
								.getPurchaseDeptId()));
				requiredCollectBidAward.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollectBidAward.getBuyWay()));
				requiredCollectBidAward.setBidPrice((Double)obj[1]);
				requiredCollectBidAward.setBaId((Long)obj[2]);
				requiredCollectBidAward.setSupplierId((Long)obj[3]);
				requiredCollectBidAward.setSupplierName((String)obj[4]);
				requiredCollectBidAward.setBaDate((Date)obj[5]);
				rcList.add(requiredCollectBidAward);
			}
			this.getPagejsonDataTables(rcList);
		} catch (Exception e) {
			log("查看采购项目信息错误！", e);
			throw new BaseException("查看采购项目信息错误！", e);
		}
		return null;
	}
	public IRequiredMaterialBiz getiRequiredMaterialBiz() {
		return iRequiredMaterialBiz;
	}
	public void setiRequiredMaterialBiz(IRequiredMaterialBiz iRequiredMaterialBiz) {
		this.iRequiredMaterialBiz = iRequiredMaterialBiz;
	}

	public RequiredMaterial getRequiredMaterial() {
		return requiredMaterial;
	}

	public void setRequiredMaterial(RequiredMaterial requiredMaterial) {
		this.requiredMaterial = requiredMaterial;
	}

	public RequiredMaterialDetail getRequiredMaterialDetail() {
		return requiredMaterialDetail;
	}

	public void setRequiredMaterialDetail(
			RequiredMaterialDetail requiredMaterialDetail) {
		this.requiredMaterialDetail = requiredMaterialDetail;
	}
	
	public ICategoryBuyerBiz getiCategoryBuyerBiz() {
		return iCategoryBuyerBiz;
	}

	public void setiCategoryBuyerBiz(ICategoryBuyerBiz iCategoryBuyerBiz) {
		this.iCategoryBuyerBiz = iCategoryBuyerBiz;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}

	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}

	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}

	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}

	public IPurchaseDepartBuyerBiz getiPurchaseDepartBuyerBiz() {
		return iPurchaseDepartBuyerBiz;
	}

	public void setiPurchaseDepartBuyerBiz(
			IPurchaseDepartBuyerBiz iPurchaseDepartBuyerBiz) {
		this.iPurchaseDepartBuyerBiz = iPurchaseDepartBuyerBiz;
	}
	
}
