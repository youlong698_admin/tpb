package com.ced.sip.purchase.base.action.epp;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.util.AskProgressStatus;
import com.ced.sip.purchase.base.biz.IBidCommunicationInfoBiz;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
/** 
 * 类名称：BidCommunicationInfoAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidCommunicationInfoAction extends BaseAction {

	// 问题解答
	private IBidCommunicationInfoBiz iBidCommunicationInfoBiz;
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
    //标段流程记录实例表
    private IBidProcessLogBiz iBidProcessLogBiz;
	//项目日志记录服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	
	
	private Long rcId;
    private String isDetail;
	// 交流信息
	private BidCommunicationInfo bidCommunicationInfo;

    private RequiredCollect requiredCollect;
  	private BidProcessLog bidProcessLog;
    private PurchaseRecordLog purchaseRecordLog;
	/**
 	* 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
	 * 当前节点的完成时间
	 * 
	 * @param rcId
	 *            项目id
	 * @param currProgress_Status
	 *            当前节点
	 * @author luguanglei
	 * @throws BaseException
	 */
	private void updateBidMonitorAndBidProcessLogNoInsert(Long rcId,Long currProgress_Status) throws BaseException {
		// 修改项目日志表的当前节点的完成时间
		bidProcessLog = new BidProcessLog();
		bidProcessLog.setRcId(rcId);
		bidProcessLog.setBidNode(currProgress_Status);
		bidProcessLog = this.iBidProcessLogBiz
				.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
		bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
		this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);

	}
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 查看交流信息列表 标段监控节点
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidCommunicationInfoMonitor() throws BaseException {
		
		String view="bidCommunicationInfoMonitorDetail";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			
			bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setRcId(rcId);
			List<BidCommunicationInfo> bidCommunicationInfoList=this.iBidCommunicationInfoBiz.getBidCommunicationInfoList(bidCommunicationInfo);
			
			
			this.getRequest().setAttribute("bidCommunicationInfoList", bidCommunicationInfoList);
			
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.OPEN_STATUS_01.equals(requiredCollect.getOpenStatus())&&StringUtil.isBlank(isDetail))
			{   
            	if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
            		this.getRequest().setAttribute("isNext", requiredCollect.getServiceStatus()==AskProgressStatus.Progress_Status_24);
            	}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
            		this.getRequest().setAttribute("isNext", requiredCollect.getServiceStatus()==BiddingProgressStatus.Progress_Status_24);
            	}
            	
            	view="bidCommunicationInfoMonitorUpdate";
			}
		
		} catch (Exception e) {
			log.error("查看交流信息信息列表标段监控错误！", e);
			throw new BaseException("查看交流信息信息列表标段监控错误！", e);
		}	
			
		return view ;
				
	}
	/**
	 * 修改问题解答
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidCommunicationInfoMonitor() throws BaseException {
		String view="success";
		String text="",operateContent="";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);

			operateContent="查看问题解答，执行下一步";
			
			if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				text=AskProgressStatus.Progress_Status_24_Text;
				updateBidMonitorAndBidProcessLogNoInsert(rcId, AskProgressStatus.Progress_Status_22);
				updateBidMonitorAndBidProcessLogNoInsert(rcId, AskProgressStatus.Progress_Status_23);
				updateBidMonitorAndBidProcessLog(rcId, AskProgressStatus.Progress_Status_24, AskProgressStatus.Progress_Status_25, AskProgressStatus.Progress_Status_25_Text);
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				text=BiddingProgressStatus.Progress_Status_24_Text;
				updateBidMonitorAndBidProcessLogNoInsert(rcId, BiddingProgressStatus.Progress_Status_22);
				updateBidMonitorAndBidProcessLogNoInsert(rcId, BiddingProgressStatus.Progress_Status_23);
				updateBidMonitorAndBidProcessLog(rcId, BiddingProgressStatus.Progress_Status_24, BiddingProgressStatus.Progress_Status_25, BiddingProgressStatus.Progress_Status_25_Text);
			}
			
				
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setRcId(rcId);
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog.setBidNode(text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", text+"成功");
		} catch (Exception e) {
			log("查看问题解答，执行下一步错误！", e);
			throw new BaseException("查看问题解答，执行下一步错误！", e);
		}
		return view;
		
	}
	
	/**
	 * 修改问题解答信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidCommunicationInfoInit() throws BaseException {
		
		try{
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
		} catch (Exception e) {
			log("修改问题解答信息初始化错误！", e);
			throw new BaseException("修改问题解答信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改问题解答信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidCommunicationInfo() throws BaseException {
		
		try{
			bidCommunicationInfo.setAnswerDate(DateUtil.getCurrentDateTime());
			bidCommunicationInfo.setAnswerId(UserRightInfoUtil.getUserId(this.getRequest()));
			bidCommunicationInfo.setAnswerName(UserRightInfoUtil.getChineseName(this.getRequest()));
			
			this.iBidCommunicationInfoBiz.updateBidCommunicationInfo(bidCommunicationInfo);			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改问题解答");
		} catch (Exception e) {
			log("修改问题解答信息错误！", e);
			throw new BaseException("修改问题解答信息错误！", e);
		}
		return MODIFY_INIT;
		
	}
	/**
	 * 查看交流信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidCommunicationInfoDetail() throws BaseException {
		
		try{
			bidCommunicationInfo=this.iBidCommunicationInfoBiz.getBidCommunicationInfo(bidCommunicationInfo.getBciId() );
		} catch (Exception e) {
			log("查看交流信息明细信息错误！", e);
			throw new BaseException("查看交流信息明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 获取未查看的问题解答数
	 * @return
	 * @throws BaseException
	 */
	public String getBidCommunicationInfoForNode() throws BaseException{
		int count=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            
            bidCommunicationInfo=new BidCommunicationInfo();
			bidCommunicationInfo.setRcId(rcId);
			count=this.iBidCommunicationInfoBiz.countBidCommunicationInfoList(bidCommunicationInfo,1);
            
			out.print(count);
		} catch (Exception e) {
			log("获取未查看的问题解答数！", e);
			throw new BaseException("获取未查看的问题解答数", e);
		}
		return null;
	}
	public BidCommunicationInfo getBidCommunicationInfo() {
		return bidCommunicationInfo;
	}

	public void setBidCommunicationInfo(BidCommunicationInfo bidCommunicationInfo) {
		this.bidCommunicationInfo = bidCommunicationInfo;
	}
	public IBidCommunicationInfoBiz getiBidCommunicationInfoBiz() {
		return iBidCommunicationInfoBiz;
	}
	public void setiBidCommunicationInfoBiz(
			IBidCommunicationInfoBiz iBidCommunicationInfoBiz) {
		this.iBidCommunicationInfoBiz = iBidCommunicationInfoBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	
}
