package com.ced.sip.purchase.base.action.epp;

import java.util.Date;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.askPrice.biz.IAskBidListBiz;
import com.ced.sip.purchase.askPrice.entity.AskBidList;
import com.ced.sip.purchase.askPrice.util.AskProgressStatus;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IBidWinningBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAward;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.BidWinning;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.entity.TenderBidList;
import com.ced.sip.purchase.tender.util.TenderProgressStatus;
import com.ced.sip.system.entity.SystemConfiguration;
/** 
 * 类名称：BidWinningAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidWinningAction extends BaseAction {

	// 中标公示 
	private IBidWinningBiz iBidWinningBiz;
	
	//项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
    //标段流程记录实例表
    private IBidProcessLogBiz iBidProcessLogBiz;
	//项目日志记录服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	//授标信息
	private IBidAwardBiz iBidAwardBiz;
	private IAskBidListBiz iAskBidListBiz;
	
	private ITenderBidListBiz iTenderBidListBiz;
	
	private IBiddingBidListBiz iBiddingBidListBiz;
	
    private Long rcId;
    private String isDetail;

	// 中标公示
	private BidWinning bidWinning;
    private RequiredCollect requiredCollect;
  	private BidProcessLog bidProcessLog;
    private PurchaseRecordLog purchaseRecordLog;

 	/**
 	* 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 查看中标公示表信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidWinningBidMonitor() throws BaseException {
		
		String view="bidWinningMonitorDetail";
		try{

			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			String title=BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay())+"中标公示";
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			bidWinning = new BidWinning();
			bidWinning.setRcId(rcId);
			List<BidWinning> list=this.iBidWinningBiz.getBidWinningList(bidWinning);
			if(list.size() >0 ){//中标公示存在
				bidWinning = list.get(0);
			}else{
				String contentString=getBidWinningContent(requiredCollect);
				bidWinning.setWinningContent(contentString);
				bidWinning.setBuyWay(requiredCollect.getBuyWay());
				bidWinning.setBidCode(requiredCollect.getBidCode());
				bidWinning.setStatus(TableStatus.WINNING_TYPE_1);
				bidWinning.setWinningTitle(requiredCollect.getBuyRemark()+title);
			}
			this.getRequest().setAttribute("title", title);
			
			 //中标公示未发布 且项目状态为正常 且非查看页面
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.NOTICE_TYPE_1.equals(bidWinning.getStatus())&&StringUtil.isBlank(isDetail))
			{   
            	view="bidWinningMonitorUpdate";
			}else{
				bidWinning.setStatusCn(TableStatusMap.noticeMap.get(bidWinning.getStatus()));
				bidWinning.setPublisherCn(BaseDataInfosUtil.convertLoginNameToChnName(bidWinning.getPublisher()));
			}
		
		} catch (Exception e) {
			log.error("查看中标公示表信息列表错误！", e);
			throw new BaseException("查看中标公示表信息列表错误！", e);
		}	
			
		return view ;				
	}
	/**
	 * 公示信息生成
	 * @param requiredCollect
	 * @return
	 * @throws BaseException
	 */
	private String getBidWinningContent(RequiredCollect requiredCollect)throws BaseException{
		String contentString="";
		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);

			String winSupplierName="";
			BidAward bidAward=new BidAward();
			bidAward.setRcId(requiredCollect.getRcId());
			List<Object[]> awards=this.iBidAwardBiz.getBidAwardList(bidAward);
			for(Object[] obj:awards){
				winSupplierName+=obj[1]+"   ";
			}
			if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				TenderBidList tenderBidList=this.iTenderBidListBiz.getTenderBidListByRcId(requiredCollect.getRcId());
			    contentString="<div id=autocontent>"+
					"<p>　　项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　项目标名："+requiredCollect.getBuyRemark()+"</p>"+
					"<p>　　本项目通过网上招标，经采购人确认，现将结果公示如下：</p>"+
					"<p>　　最终供应商："+winSupplierName+"</p>"+
					"<p>　　如对以上公示有疑问，请在三个工作日内以书面形式与"+systemConfiguration.getSystemCurrentDept()+"联系</p>"+
			        "<p>　　 联系人："+tenderBidList.getResponsibleUser()+"</p>"+
			        "<p>　　 联系电话:"+tenderBidList.getResponsiblePhone()+"</p>"+
			        "<p align=\"right\">　  "+systemConfiguration.getSystemCurrentDept()+" "+
					 "<p align=\"right\">　 "+DateUtil.getWateStringFromDateChinese(new Date())+"</p>"+		   
					 "</div>";
			    
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				AskBidList askBidList=this.iAskBidListBiz.getAskBidListByRcId(requiredCollect.getRcId());

			    contentString="<div id=autocontent>"+
					"<p>　　项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　项目标名："+requiredCollect.getBuyRemark()+"</p>"+
					"<p>　　本项目通过网上询价，经采购人确认，现将结果公示如下：</p>"+
					"<p>　　最终供应商："+winSupplierName+"</p>"+
					"<p>　　如对以上公示有疑问，请在三个工作日内以书面形式与"+systemConfiguration.getSystemCurrentDept()+"联系</p>"+
			        "<p>　　 联系人："+askBidList.getResponsibleUser()+"</p>"+
			        "<p>　　 联系电话:"+askBidList.getResponsiblePhone()+"</p>"+
			        "<p align=\"right\">　  "+systemConfiguration.getSystemCurrentDept()+" "+
					 "<p align=\"right\">　 "+DateUtil.getWateStringFromDateChinese(new Date())+"</p>"+		   
					 "</div>";
			    
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				BiddingBidList biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(requiredCollect.getRcId());

			    contentString="<div id=autocontent>"+
					"<p>　　项目标号："+requiredCollect.getBidCode()+"</p>"+
					"<p>　　项目标名："+requiredCollect.getBuyRemark()+"</p>"+
					"<p>　　本项目通过网上竞价，经采购人确认，现将结果公示如下：</p>"+
					"<p>　　最终供应商："+winSupplierName+"</p>"+
					"<p>　　如对以上公示有疑问，请在三个工作日内以书面形式与"+systemConfiguration.getSystemCurrentDept()+"联系</p>"+
			        "<p>　　 联系人："+biddingBidList.getResponsibleUser()+"</p>"+
			        "<p>　　 联系电话:"+biddingBidList.getResponsiblePhone()+"</p>"+
			        "<p align=\"right\">　  "+systemConfiguration.getSystemCurrentDept()+" "+
					 "<p align=\"right\">　 "+DateUtil.getWateStringFromDateChinese(new Date())+"</p>"+		   
					 "</div>";
			    
			}
		} catch (Exception e) {
			log.error("获取公示内容错误！", e);
			throw new BaseException("获取公示内容错误！", e);
		}
		return contentString;
	}
	/**
	 * 修改中标公示表信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidWinning() throws BaseException {
		String view="success";
		String text="";
		String userName=UserRightInfoUtil.getUserName(getRequest());
		try{
			String status=this.getRequest().getParameter("status");
			if(bidWinning.getBwId()==null){
				bidWinning.setWriteDate(new Date());
				bidWinning.setWriter(userName);
				this.iBidWinningBiz.saveBidWinning(bidWinning);
			}else{
				this.iBidWinningBiz.updateBidWinning(bidWinning);
			}
			
			
			if(status.equals(TableStatus.WINNING_TYPE_0)){
				Long comId=UserRightInfoUtil.getComId(getRequest());
				SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
				
				bidWinning.setPublishDate(new Date());
				bidWinning.setPublisher(userName);
				bidWinning.setStatus(TableStatus.WINNING_TYPE_0);
				bidWinning.setSysCompany(systemConfiguration.getSystemCurrentDept());
				bidWinning.setComId(comId);
				this.iBidWinningBiz.updateBidWinning(bidWinning);
				
				if(bidWinning.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				    text=TenderProgressStatus.Progress_Status_29_Text;
				    updateBidMonitorAndBidProcessLog(bidWinning.getRcId(), TenderProgressStatus.Progress_Status_29, TenderProgressStatus.Progress_Status_30, TenderProgressStatus.Progress_Status_30_Text);
				}else if(bidWinning.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
					text=AskProgressStatus.Progress_Status_28_Text;
					updateBidMonitorAndBidProcessLog(bidWinning.getRcId(), AskProgressStatus.Progress_Status_27, AskProgressStatus.Progress_Status_28, AskProgressStatus.Progress_Status_28_Text);
				}else if(bidWinning.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
					text=BiddingProgressStatus.Progress_Status_28_Text;
					updateBidMonitorAndBidProcessLog(bidWinning.getRcId(), BiddingProgressStatus.Progress_Status_27, BiddingProgressStatus.Progress_Status_28, BiddingProgressStatus.Progress_Status_28_Text);
				}
				purchaseRecordLog = new PurchaseRecordLog();
				purchaseRecordLog.setRcId(bidWinning.getRcId());
				purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
				purchaseRecordLog.setOperatorName(userName);
				purchaseRecordLog.setOperateContent(text+"已发布！");
				purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
				purchaseRecordLog.setBidNode(text);
				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);

				String userNameCn=UserRightInfoUtil.getChineseName(this.getRequest());
				BidAward bidAward=new BidAward();
				bidAward.setRcId(requiredCollect.getRcId());
				List<Object[]> awards=this.iBidAwardBiz.getBidAwardList(bidAward);
				String subject="邮件提醒：恭喜您，"+systemConfiguration.getSystemCurrentDept()+"发布的"+bidWinning.getBidCode()+"项目预中标啦！！公示标题"+bidWinning.getWinningTitle()+"",content=bidWinning.getWinningContent();
				String smsTitle=systemConfiguration.getSystemCurrentDept()+"发布的"+bidWinning.getBidCode()+"项目预中标啦！！公示标题："+bidWinning.getWinningTitle()+""; 
				
				for(Object[] obj:awards){
					this.saveSendMailToUsers((String)obj[3], subject, content,userNameCn);
					this.saveSmsMessageToUsers((String)obj[2], smsTitle, userNameCn);
					this.saveSysMessage(userNameCn, userName, (String)obj[1], ((BidAward)obj[0]).getSupplierId()+"", 1, smsTitle, comId);
				}
			}
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改"+text+"中标公示表");
		} catch (Exception e) {
			log("修改"+text+"中标公示表信息错误！", e);
			throw new BaseException("修改"+text+"中标公示表信息错误！", e);
		}
		return view;
		
	}

	public IBidWinningBiz getiBidWinningBiz() {
		return iBidWinningBiz;
	}

	public void setiBidWinningBiz(IBidWinningBiz iBidWinningBiz) {
		this.iBidWinningBiz = iBidWinningBiz;
	}

	public BidWinning getBidWinning() {
		return bidWinning;
	}

	public void setBidWinning(BidWinning bidWinning) {
		this.bidWinning = bidWinning;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}
	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}
	public IAskBidListBiz getiAskBidListBiz() {
		return iAskBidListBiz;
	}
	public void setiAskBidListBiz(IAskBidListBiz iAskBidListBiz) {
		this.iAskBidListBiz = iAskBidListBiz;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}
	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}
	
}
