package com.ced.sip.purchase.base.action.epp;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidClarifyBiz;
import com.ced.sip.purchase.base.biz.IBidClarifyResponseBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidClarify;
import com.ced.sip.purchase.base.entity.BidClarifyResponse;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
/** 
 * 类名称：BidClarifyAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-07
 */
public class BidClarifyAction extends BaseAction {

	// 标前澄清 
	private IBidClarifyBiz iBidClarifyBiz;
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
	// 标前澄清响应 
	private IBidClarifyResponseBiz iBidClarifyResponseBiz;
	
    private IAttachmentBiz iAttachmentBiz;
    //邀请供应商服务类
    private IInviteSupplierBiz iInviteSupplierBiz;
	
    private Long rcId;
    private String isDetail;
	// 标前澄清
	private BidClarify bidClarify;
	private BidClarifyResponse bidClarifyResponse;
    private RequiredCollect requiredCollect;
	
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 查看标前澄清信息列表 标段监控
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidClarifyMonitor() throws BaseException {
		String view="bidClarifyMonitorDetail";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			
			bidClarify=new BidClarify();
			bidClarify.setRcId(rcId);
			List<BidClarify> bidClarifyList=this.iBidClarifyBiz.getBidClarifyList(bidClarify);
			for(BidClarify bidClarify:bidClarifyList){
				 bidClarify.setStatusCn(TableStatusMap.statusMap.get(bidClarify.getStatus()));
			}
			
			this.getRequest().setAttribute("bidClarifyList", bidClarifyList);
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.OPEN_STATUS_01.equals(requiredCollect.getOpenStatus())&&StringUtil.isBlank(isDetail))
			{   
            	view="bidClarifyMonitorUpdate";
			}
			
		
		} catch (Exception e) {
			log.error("查看标前澄清信息列表标段监控错误！", e);
			throw new BaseException("查看标前澄清信息列表标段监控错误！", e);
		}	
			
		return view ;
				
	}
	/**
	 * 保存标前澄清信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidClarifyInit() throws BaseException {
		try{
		  
		} catch (Exception e) {
			log("保存标前澄清信息初始化错误！", e);
			throw new BaseException("保存标前澄清信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存标前澄清信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBidClarify() throws BaseException {
		
		try{
			String userName=UserRightInfoUtil.getUserName(getRequest());
			String status=this.getRequest().getParameter("status");
			bidClarify.setStatus(status);
			bidClarify.setWriteDate(new Date());
			bidClarify.setWriter(userName);			
			this.iBidClarifyBiz.saveBidClarify(bidClarify);		
			//保存附件
			bidClarify.setAttIdData(bidClarify.getAttIdData());
			bidClarify.setUuIdData(bidClarify.getUuIdData());
			bidClarify.setFileNameData(bidClarify.getFileNameData());
			bidClarify.setFileTypeData(bidClarify.getFileTypeData());			
			iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( bidClarify, bidClarify.getBcId(), AttachmentStatus.ATTACHMENT_CODE_202, userName ) ) ;
			
			if(status.equals(TableStatus.CLARIFY_TYPE_0)){
				String userNameCn=UserRightInfoUtil.getChineseName(this.getRequest());
				Long comId=UserRightInfoUtil.getComId(this.getRequest());
				requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(bidClarify.getRcId());
				String subject="邮件提醒："+requiredCollect.getBidCode()+"项目发布澄清，澄清标题："+bidClarify.getClarifyTitle()+"",content=bidClarify.getClarifyContent();
				String smsTitle=requiredCollect.getPurchaseDeptName()+"发布的"+requiredCollect.getBidCode()+"项目发布澄清，澄清标题："+bidClarify.getClarifyTitle();
				//邀请供应商
				InviteSupplier inviteSupplier = new InviteSupplier();
				inviteSupplier.setRcId(requiredCollect.getRcId());
				List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
				for(InviteSupplier inviteSupplier2:sulist){
					this.saveSendMailToUsers(inviteSupplier2.getSupplierEmail(), subject, content,userNameCn);
					this.saveSmsMessageToUsers(inviteSupplier2.getSupplierPhone(), smsTitle, userNameCn);
					this.saveSysMessage(userNameCn, userName, inviteSupplier2.getSupplierName(), inviteSupplier2.getSupplierId()+"", 1, smsTitle, comId);
				}
			}
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增标前澄清");
		} catch (Exception e) {
			log("保存标前澄清信息错误！", e);
			throw new BaseException("保存标前澄清信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改标前澄清信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidClarifyInit() throws BaseException {
		
		try{
			bidClarify=this.iBidClarifyBiz.getBidClarify(bidClarify.getBcId() );
			//获取附件
			Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment(bidClarify.getBcId(), AttachmentStatus.ATTACHMENT_CODE_202 ) );
			bidClarify.setUuIdData((String)map.get("uuIdData"));
			bidClarify.setFileNameData((String)map.get("fileNameData"));
			bidClarify.setFileTypeData((String)map.get("fileTypeData"));
			bidClarify.setAttIdData((String)map.get("attIdData"));
			} catch (Exception e) {
			log("修改标前澄清信息初始化错误！", e);
			throw new BaseException("修改标前澄清信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改标前澄清信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidClarify() throws BaseException {
		
		try{
			String userName=UserRightInfoUtil.getUserName(getRequest());
			String status=this.getRequest().getParameter("status");
			bidClarify.setStatus(status);
			this.iBidClarifyBiz.updateBidClarify(bidClarify);
			//保存附件
			bidClarify.setAttIdData(bidClarify.getAttIdData());
			bidClarify.setUuIdData(bidClarify.getUuIdData());
			bidClarify.setFileNameData(bidClarify.getFileNameData());
			bidClarify.setFileTypeData(bidClarify.getFileTypeData());			
			iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( bidClarify, bidClarify.getBcId(), AttachmentStatus.ATTACHMENT_CODE_202, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
			// 删除附件
			iAttachmentBiz.deleteAttachments( parseAttachIds( bidClarify.getAttIds() ) ) ;
			
			bidClarify.setAttIdData("[]");
			bidClarify.setUuIdData("[]");
			bidClarify.setFileNameData("[]");
			bidClarify.setFileTypeData("[]");	
			
			if(status.equals(TableStatus.CLARIFY_TYPE_0)){
				String userNameCn=UserRightInfoUtil.getChineseName(this.getRequest());
				Long comId=UserRightInfoUtil.getComId(this.getRequest());
				requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(bidClarify.getRcId());
				String subject="邮件提醒："+requiredCollect.getBidCode()+"项目发布澄清，澄清标题"+bidClarify.getClarifyTitle()+"",content=bidClarify.getClarifyContent();
				String smsTitle=requiredCollect.getPurchaseDeptName()+"发布的"+requiredCollect.getBidCode()+"项目发布澄清，澄清标题："+bidClarify.getClarifyTitle(); 
				//邀请供应商
				InviteSupplier inviteSupplier = new InviteSupplier();
				inviteSupplier.setRcId(requiredCollect.getRcId());
				List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
				for(InviteSupplier inviteSupplier2:sulist){
					this.saveSendMailToUsers(inviteSupplier2.getSupplierEmail(), subject, content,userNameCn);
					this.saveSmsMessageToUsers(inviteSupplier2.getSupplierPhone(), smsTitle, userNameCn);
					this.saveSysMessage(userNameCn, userName, inviteSupplier2.getSupplierName(), inviteSupplier2.getSupplierId()+"", 1, smsTitle, comId);
				}
			}
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改标前澄清");
		} catch (Exception e) {
			log("修改标前澄清信息错误！", e);
			throw new BaseException("修改标前澄清信息错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 删除标前澄清信息
	 * @return
	 * @throws BaseException 
	 */
	public String deleteBidClarify() throws BaseException {
		try{
		    BidClarify bidClarify=new BidClarify();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				bidClarify=this.iBidClarifyBiz.getBidClarify(new Long(str[i]));
	 				this.iBidClarifyBiz.deleteBidClarify(bidClarify);
	 				this.iBidClarifyResponseBiz.deleteBidClarifyResponseByBcId(bidClarify.getBcId());
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除标前澄清");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除标前澄清信息错误！", e);
			throw new BaseException("删除标前澄清信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看标前澄清明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidClarifyDetail() throws BaseException {
		
		try{
			bidClarify=this.iBidClarifyBiz.getBidClarify(bidClarify.getBcId() );
			//获取附件
			bidClarify.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( bidClarify.getBcId(), AttachmentStatus.ATTACHMENT_CODE_202 ) ) , "0", this.getRequest() ) ) ;
			bidClarify.setStatusCn(TableStatusMap.statusMap.get(bidClarify.getStatus()));
			
			bidClarifyResponse=new BidClarifyResponse();
			bidClarifyResponse.setBcId(bidClarify.getBcId());
			List<BidClarifyResponse> bidClarifyResponsesList=this.iBidClarifyResponseBiz.getBidClarifyResponseList(bidClarifyResponse);
			this.getRequest().setAttribute("bidClarifyResponsesList", bidClarifyResponsesList);
		} catch (Exception e) {
			log("查看标前澄清明细信息错误！", e);
			throw new BaseException("查看标前澄清明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public BidClarify getBidClarify() {
		return bidClarify;
	}

	public void setBidClarify(BidClarify bidClarify) {
		this.bidClarify = bidClarify;
	}

	public IBidClarifyBiz getiBidClarifyBiz() {
		return iBidClarifyBiz;
	}

	public void setiBidClarifyBiz(IBidClarifyBiz iBidClarifyBiz) {
		this.iBidClarifyBiz = iBidClarifyBiz;
	}

	public IBidClarifyResponseBiz getiBidClarifyResponseBiz() {
		return iBidClarifyResponseBiz;
	}

	public void setiBidClarifyResponseBiz(
			IBidClarifyResponseBiz iBidClarifyResponseBiz) {
		this.iBidClarifyResponseBiz = iBidClarifyResponseBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}
	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	
}
