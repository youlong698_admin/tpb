package com.ced.sip.purchase.base.action.epp;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.priceLibrary.biz.IPriceLibraryBiz;
import com.ced.sip.priceLibrary.entity.PriceLibrary;
import com.ced.sip.purchase.askPrice.util.AskProgressStatus;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IBidAwardDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAward;
import com.ced.sip.purchase.base.entity.BidAwardDetail;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
import com.ced.sip.purchase.tender.util.TenderProgressStatus;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
/** 
 * 类名称：BidAwardAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-03
 */
public class BidAwardAction extends BaseAction {

	@Autowired
	private SnakerEngineFacets facets;
	// 授标 
	private IBidAwardBiz iBidAwardBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	// 供应商报价明细信息 
	private IBidPriceDetailBiz iBidPriceDetailBiz;
	// 授标明细 
	private IBidAwardDetailBiz iBidAwardDetailBiz;
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
    //邀请供应商实例
    private IInviteSupplierBiz iInviteSupplierBiz;
    //标段流程记录实例表
    private IBidProcessLogBiz iBidProcessLogBiz;
	//项目日志记录服务类
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	//价格库数据
	private IPriceLibraryBiz iPriceLibraryBiz;
    
    
	// 授标
	private BidAward bidAward;
	// 授标明细
	private BidAwardDetail bidAwardDetail;
	private BidPrice bidPrice;
	
	private PriceLibrary priceLibrary;
	
	private Long rcId;
    private String isDetail;
    
    private RequiredCollect requiredCollect;
    private RequiredCollectDetail requiredCollectDetail;
  	private BidProcessLog bidProcessLog;
    private PurchaseRecordLog purchaseRecordLog;
	/**
 	* 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 查看授标信息 标段监控
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidAwardMonitor() throws BaseException {
		String view="bidAwardMonitorDetail";
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			List list=this.iBidPriceBiz.getBidPriceAndAwardListByRcId(rcId);
			
			this.setListValue(list);
			
			boolean falg=false;
			int isWorkFlow=0;
			if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
				falg=TenderProgressStatus.Progress_Status_28==requiredCollect.getServiceStatus();
				this.getRequest().setAttribute("bidpurchaseresultWorkflow", systemConfiguration.getBidpurchaseresult00Workflow());
				if(StringUtil.convertNullToBlank(systemConfiguration.getBidpurchaseresult00Workflow()).equals("0")){
				    isWorkFlow=1;
				    setProcessLisPR(requiredCollect, WorkFlowStatus.BidPurchaseResult_Work_Item,comId);
				}
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				falg=AskProgressStatus.Progress_Status_26==requiredCollect.getServiceStatus();
				this.getRequest().setAttribute("bidpurchaseresultWorkflow", systemConfiguration.getBidpurchaseresult01Workflow());
				if(StringUtil.convertNullToBlank(systemConfiguration.getBidpurchaseresult01Workflow()).equals("0")){
				    isWorkFlow=1;
				    setProcessLisPR(requiredCollect, WorkFlowStatus.BidPurchaseResult_Work_Item,comId);
				}
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				falg=BiddingProgressStatus.Progress_Status_26==requiredCollect.getServiceStatus();
				this.getRequest().setAttribute("bidpurchaseresultWorkflow", systemConfiguration.getBidpurchaseresult02Workflow());
				if(StringUtil.convertNullToBlank(systemConfiguration.getBidpurchaseresult02Workflow()).equals("0")){
				    isWorkFlow=1;
				    setProcessLisPR(requiredCollect, WorkFlowStatus.BidPurchaseResult_Work_Item,comId);
				}
			}
		    this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.PR_WorkFlow_Type);
		    this.getRequest().setAttribute("isWorkFlow",isWorkFlow);
		    this.getRequest().setAttribute("requiredCollect",requiredCollect);
			//当前流程进度为授标 且项目状态为正常 且非查看页面
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&falg&&StringUtil.isBlank(isDetail))
			{   
            	view="bidAwardMonitorUpdate";
			}
			
		
		} catch (Exception e) {
			log.error("查看授标信息列表标段监控错误！", e);
			throw new BaseException("查看授标信息列表标段监控错误！", e);
		}	
			
		return view ;
				
	}


	 /**
	 * 设置计划流程相关列表
	 */
	private void setProcessLisPR(RequiredCollect rc, String processName,Long comId){
		//step1：首先获取流程定义实体
		org.snaker.engine.entity.Process process = facets.getEngine().process().getProcessByName(processName,comId);
		//step2：1、判断流程定义是否存在，2、遍历集合添加流程实例
		if(process != null){
				String newOrderNo=WorkFlowStatus.PR_WorkFlow_Type+rc.getRcId().toString();
				//step3:判断流程实例是否运行
				QueryFilter filter = new QueryFilter();
             filter.setOrderNo(newOrderNo);
				filter.setProcessId(process.getId());
				List<HistoryOrder> holist = facets.getEngine().query().getHistoryOrders(filter);
				if(holist.size() > 0){
					//step5:获取正在运行的流程实例
					filter.setOrderNo(newOrderNo);
					filter.setProcessId(process.getId());
					List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
					if(orderlist.size() > 0){
						Order order = orderlist.get((orderlist.size()-1));
						if(order != null){
							List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
							if(tasklist.size() > 0){
								//设置当前流程名称
								rc.setProcessName(tasklist.get(0).getDisplayName());
							}
							rc.setOrderId(order.getId());//设置实例标示
						}
					}
					HistoryOrder ho = holist.get(0);
					rc.setOrderState(ho.getOrderState().toString());
					rc.setOrderStateName(WorkFlowStatusMap.getWorkflowOrderStatus(ho.getOrderState().toString()));
				}else{
					rc.setOrderStateName(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01_TEXT);
				}
				rc.setProcessId(process.getId());//设置流程标示
				rc.setInstanceUrl(process.getInstanceUrl());//设置流程实例URL
			
		}
	}
	/**
	 * 授标信息执行下一步
	 * @return
	 * @throws BaseException 
	 */
	public String updateBidAwardMonitor() throws BaseException {
		String view="success";
		String text="",operateContent="";
		try{			
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);

			this.iBidAwardBiz.updateBidAwardStatus(requiredCollect.getRcId());
			
			operateContent="查看授标信息，执行下一步";
			if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_00)){
			    text=TenderProgressStatus.Progress_Status_28_Text;
			    updateBidMonitorAndBidProcessLog(rcId, TenderProgressStatus.Progress_Status_28, TenderProgressStatus.Progress_Status_29, TenderProgressStatus.Progress_Status_29_Text);
			}else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_01)){
				text=AskProgressStatus.Progress_Status_27_Text;
				updateBidMonitorAndBidProcessLog(rcId, AskProgressStatus.Progress_Status_26, AskProgressStatus.Progress_Status_27, AskProgressStatus.Progress_Status_27_Text);
	        }else if(requiredCollect.getBuyWay().equals(TableStatus.PURCHASE_WAY_02)){
				text=BiddingProgressStatus.Progress_Status_27_Text;
				updateBidMonitorAndBidProcessLog(rcId, BiddingProgressStatus.Progress_Status_26, BiddingProgressStatus.Progress_Status_27, BiddingProgressStatus.Progress_Status_27_Text);
			}	

			//插入授标价 价格库数据
			this.iPriceLibraryBiz.saveBidAwardPriceLibrary("授标价", rcId, comId);
			
			//插入项目价  价格库数据
			this.iPriceLibraryBiz.saveBidPriceLibrary("项目价", rcId, comId);
			
			//更新物料库中的最新采购单价
			this.iBidAwardBiz.updateMaterialListPrice(rcId);
			
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setRcId(rcId);
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog.setBidNode(text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", text+"成功");
		} catch (Exception e) {
			log("授标信息执行下一步错误！", e);
			throw new BaseException("授标信息执行下一步错误！", e);
		}
		return view;
		
	}
	
	/**
	 * 保存授标信息初始化  授标单个供应商
	 * @return
	 * @throws BaseException 
	 */
	public String saveOneInitBidAward() throws BaseException {
		try{
			String supplierName="";
			List<BidPrice> bpList=new ArrayList<BidPrice>();
			bidPrice=new BidPrice();
			bidPrice.setRcId(rcId);
			List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);
			for(Object[] object:objectList){
				bidPrice=(BidPrice)object[0];					
				supplierName=(String)object[1];
				bidPrice.setSupplierName(supplierName);
				bpList.add(bidPrice);
			}
			this.setListValue(bpList);
		} catch (Exception e) {
			log("保存授标信息初始化错误--授标单个供应商！", e);
			throw new BaseException("保存授标信息初始化错误--授标单个供应商！", e);
		}
		return "addOneInit";
		
	}
	
	/**
	 * 保存授标信息  授标单个供应商
	 * @return
	 * @throws BaseException 
	 */
	public String saveOneBidAward() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			Long supplierId=Long.parseLong(this.getRequest().getParameter("supplierId"));
			//删除上次的授标信息
			this.iBidAwardBiz.deleteBidAwardByRcId(rcId);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			//得到报价id
			bidPrice=new BidPrice();
			bidPrice.setSupplierId(supplierId);
			bidPrice.setRcId(rcId);
			List list=this.iBidPriceBiz.getBidPriceList(bidPrice);
			if(list.size()>0) bidPrice=(BidPrice)list.get(0);
			Long bpId=bidPrice.getBpId();
			Double taxRate=bidPrice.getTaxRate();
			
			//保存本次授标信息			
			bidAward=new BidAward();
			bidAward.setTaxRate(taxRate);
			bidAward.setRcId(rcId);
			bidAward.setSupplierId(supplierId);
			bidAward.setWriteDate(new Date());
			bidAward.setWriter(UserRightInfoUtil.getUserName(getRequest()));
			bidAward.setBuyWay(requiredCollect.getBuyWay());
			bidAward.setDeptId(requiredCollect.getDeptId());
			bidAward.setComId(comId);
			bidAward.setIsContract(1);
			bidAward.setIsOrder(1);
			bidAward.setStatus(TableStatus.STATUS_1);
			this.iBidAwardBiz.saveBidAward(bidAward);
			
			requiredCollect.setBidWinningPrice(bidAward.getBidPrice());
			this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
			
			BidPriceDetail bidPriceDetail=new BidPriceDetail();
			bidPriceDetail.setBpId(bpId);
			List<Object[]> objectList=this.iBidPriceDetailBiz.getBidPriceDetailListRequiredCollectDetail(bidPriceDetail);
			for(Object[] object:objectList){
				bidPriceDetail=(BidPriceDetail)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				
				bidAwardDetail=new BidAwardDetail();
				bidAwardDetail.setAwardAmount(requiredCollectDetail.getAmount());
				bidAwardDetail.setRcdId(requiredCollectDetail.getRcdId());
				bidAwardDetail.setBaId(bidAward.getBaId());
				bidAwardDetail.setPrice(bidPriceDetail.getPrice());
				bidAwardDetail.setIsOrder(1);
				this.iBidAwardDetailBiz.saveBidAwardDetail(bidAwardDetail);
			}

			this.iBidAwardBiz.updateBidAwardBidPrice(rcId);
			
			
			this.getRequest().setAttribute("operModule", "授标推荐供应商");
			this.getRequest().setAttribute("message", "推荐成功");
		} catch (Exception e) {
			log("保存授标信息错误--授标单个供应商！", e);
			throw new BaseException("保存授标信息错误--授标单个供应商！", e);
		}
		
		return "addOneInit";
		
	}
	
	/**
	 * 保存授标信息初始化  授标多个供应商
	 * @return
	 * @throws BaseException 
	 */
	public String saveMoreInitBidAward() throws BaseException {
		try{
			InviteSupplier inviteSupplier=new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			inviteSupplier.setIsPriceAa(TableStatus.COMMON_0);
			List invList = iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
			
			List<RequiredCollectDetail> rcdList=this.iRequiredCollectBiz.getRequiredCollectDetailList(rcId);

			this.getRequest().setAttribute("rcdList", rcdList);
			this.getRequest().setAttribute("invList", invList);
		} catch (Exception e) {
			log("保存授标信息初始化错误--授标多个供应商！", e);
			throw new BaseException("保存授标信息初始化错误---授标多个供应商！", e);
		}
		return "addMoreInit";
		
	}
	/**
	 * 获得供应商单个物资报价信息
	 * @return
	 * @throws BaseException
	 * @author luguanglei
	 */
	public String getPriceDetailSupplier() throws BaseException {
		try{
			PrintWriter out = this.getResponse().getWriter();
			String supplierId=this.getRequest().getParameter("supplierId");
			String rcId=this.getRequest().getParameter("rcId");
			String rcdId=this.getRequest().getParameter("rcdId");
			double price=iBidPriceBiz.getBidPriceDetailBySupplierIdAndRcIdAndRcdId(supplierId,rcId,rcdId);
			out.print(price);			
		} catch (Exception e) {
			log.error("获得供应商单个物资报价信息错误！", e);
			throw new BaseException("获得供应商单个物资报价信息错误！", e);
		}
		
		return null;
	}
	
	/**
	 * 保存授标信息 授标多个供应商
	 * @return
	 * @throws BaseException 
	 */
	public String saveMoreBidAward() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String[] supplierIds=this.getRequest().getParameterValues("supplierId"); 
			String[] prices=this.getRequest().getParameterValues("price");  
			String[] rcdIds=this.getRequest().getParameterValues("rcdId");
			String[] amounts=this.getRequest().getParameterValues("amount");
			
			//删除上次的授标信息
			this.iBidAwardBiz.deleteBidAwardByRcId(rcId);

			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			Map map=new HashMap();
            for(String supplierId:supplierIds){
				if(map.get(supplierId)==null){
				//得到报价id
				bidPrice=new BidPrice();
				bidPrice.setSupplierId(Long.parseLong(supplierId));
				bidPrice.setRcId(rcId);
				List list=this.iBidPriceBiz.getBidPriceList(bidPrice);
				if(list.size()>0) bidPrice=(BidPrice)list.get(0);
				Double taxRate=bidPrice.getTaxRate();
				//保存本次授标信息			
				bidAward=new BidAward();
				bidAward.setTaxRate(taxRate);
				bidAward.setRcId(rcId);
				bidAward.setSupplierId(Long.parseLong(supplierId));
				bidAward.setWriteDate(new Date());
				bidAward.setWriter(UserRightInfoUtil.getUserName(getRequest()));	
				bidAward.setBuyWay(requiredCollect.getBuyWay());
				bidAward.setDeptId(requiredCollect.getDeptId());
				bidAward.setComId(comId);
				bidAward.setIsContract(1);
				bidAward.setIsOrder(1);
				bidAward.setStatus(TableStatus.STATUS_1);
				this.iBidAwardBiz.saveBidAward(bidAward);				
				
				map.put(supplierId, bidAward.getBaId());
				}
			}
			for(int i=0;i<supplierIds.length;i++)
			{
				bidAwardDetail=new BidAwardDetail();
				bidAwardDetail.setAwardAmount(Double.parseDouble(amounts[i]));
				bidAwardDetail.setRcdId(Long.parseLong(rcdIds[i]));
				bidAwardDetail.setBaId((Long)map.get(supplierIds[i]));
				bidAwardDetail.setPrice(Double.parseDouble(prices[i]));
				bidAwardDetail.setIsOrder(1);
				this.iBidAwardDetailBiz.saveBidAwardDetail(bidAwardDetail);
			}
				
			this.iBidAwardBiz.updateBidAwardBidPrice(rcId);	
			
			this.getRequest().setAttribute("message", "保存成功");
			this.getRequest().setAttribute("operModule", "保存推荐多个供应商");
		} catch (Exception e) {
			log("保存授标信息错误--授标多个供应商！", e);
			throw new BaseException("保存授标信息错误-授标多个供应商！", e);
		}
		
		return "addMoreInit";
		
	}
	
	/**
	 * 查看授标明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBidAwardDetail() throws BaseException {
		
		try{
			Long baId=Long.parseLong(this.getRequest().getParameter("baId"));
			bidAward=this.iBidAwardBiz.getBidAward(baId);
						
			List<BidAwardDetail> badList=new ArrayList<BidAwardDetail>();
			bidAwardDetail=new BidAwardDetail();
			bidAwardDetail.setBaId(bidAward.getBaId());
			List<Object[]> objectList=this.iBidAwardDetailBiz.getBidAwardDetailListRequiredCollectDetail(bidAwardDetail);
			for(Object[] object:objectList){
				bidAwardDetail=(BidAwardDetail)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				bidAwardDetail.setRequiredCollectDetail(requiredCollectDetail);
				badList.add(bidAwardDetail);
			}
			
			this.getRequest().setAttribute("bidAward", bidAward);			
			this.getRequest().setAttribute("badList", badList);	
		} catch (Exception e) {
			log("查看授标明细信息错误！", e);
			throw new BaseException("查看授标明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}

	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}
	public IBidAwardDetailBiz getiBidAwardDetailBiz() {
		return iBidAwardDetailBiz;
	}

	public void setiBidAwardDetailBiz(IBidAwardDetailBiz iBidAwardDetailBiz) {
		this.iBidAwardDetailBiz = iBidAwardDetailBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}
	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}
	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}
	public SnakerEngineFacets getFacets() {
		return facets;
	}
	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}
	public IPriceLibraryBiz getiPriceLibraryBiz() {
		return iPriceLibraryBiz;
	}
	public void setiPriceLibraryBiz(IPriceLibraryBiz iPriceLibraryBiz) {
		this.iPriceLibraryBiz = iPriceLibraryBiz;
	}
	
}
