package com.ced.sip.purchase.base.action.epp;

import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidAbnomalBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAbnomal;
import com.ced.sip.purchase.base.entity.RequiredCollect;
/** 
 * 类名称：BidAbnomalAction
 * 创建人：luguanglei 
 * 创建时间：2017-07-16
 */
public class BidAbnomalAction extends BaseAction {

	// 采购异常表 
	private IBidAbnomalBiz iBidAbnomalBiz;
	
	private IRequiredCollectBiz iRequiredCollectBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;

	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	
	// 采购异常表
	private BidAbnomal bidAbnomal;
	
	private RequiredCollect requiredCollect;
	
	private PurchaseRecordLog purchaseRecordLog;
	
	
	/**
	 * 采购业务终止初始化页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveBidAbnomalInit() throws BaseException {
		
		try{
			requiredCollect=iRequiredCollectBiz.getRequiredCollect(requiredCollect.getRcId());
			requiredCollect.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(requiredCollect.getBuyWay()));
			
			bidAbnomal=new BidAbnomal();
			bidAbnomal.setBidCode(requiredCollect.getBidCode());
			List<BidAbnomal> babList=iBidAbnomalBiz.getBidAbnomalList(bidAbnomal);
			if(babList.size()>0)
			{
				bidAbnomal=babList.get(0);
				//获取附件
				Map<String,Object> map=iAttachmentBiz.getAttachmentMap( new Attachment( bidAbnomal.getBaId(), AttachmentStatus.ATTACHMENT_CODE_901 ) );
				bidAbnomal.setUuIdData((String)map.get("uuIdData"));
				bidAbnomal.setFileNameData((String)map.get("fileNameData"));
				bidAbnomal.setFileTypeData((String)map.get("fileTypeData"));
				bidAbnomal.setAttIdData((String)map.get("attIdData"));
				bidAbnomal.setAttIds((String)map.get("attIds"));
			}else
			{
				bidAbnomal.setWriterId(UserRightInfoUtil.getUserId(this.getRequest()));
				bidAbnomal.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
				bidAbnomal.setWriterCn(BaseDataInfosUtil.convertLoginNameToChnName(UserRightInfoUtil.getUserName(this.getRequest())));
				bidAbnomal.setDeptId(UserRightInfoUtil.getUserDeptId(this.getRequest()));
				bidAbnomal.setWriteDete(DateUtil.getCurrentDateTime());
			}
			this.getRequest().setAttribute("abnomalList",TableStatusMap.bidAbnomalMap);
			
		} catch (Exception e) {
			log.error("查看采购业务终止初始化页面错误！", e);
			throw new BaseException("查看采购业务终止初始化页面错误！", e);
		}
		
		return "saveAbnomal";
		
	}
	
	/**
	 * 保存采购业务终止信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveBidAbnomal() throws BaseException {
		
		try{
			if(StringUtil.isNotBlank(bidAbnomal.getBaId()))
			{
				// 删除附件
				iAttachmentBiz.deleteAttachments( parseAttachIds( bidAbnomal.getAttIds() ) ) ;
				//保存附件			
				iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( bidAbnomal, bidAbnomal.getBaId(), AttachmentStatus.ATTACHMENT_CODE_901, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
				
				String rcId=this.getRequest().getParameter("rcId");
				requiredCollect=iRequiredCollectBiz.getRequiredCollect(new Long(rcId));
				requiredCollect.setBidStatus(bidAbnomal.getAbnomalType());
				requiredCollect.setBidStatusCn(BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(bidAbnomal.getAbnomalType()));
				iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
				bidAbnomal.setRcId(requiredCollect.getRcId());
				iBidAbnomalBiz.updateBidAbnomal(bidAbnomal);
			}else
			{
				String rcId=this.getRequest().getParameter("rcId");
				requiredCollect=iRequiredCollectBiz.getRequiredCollect(new Long(rcId));
				requiredCollect.setBidStatus(bidAbnomal.getAbnomalType());
				requiredCollect.setBidStatusCn(BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(bidAbnomal.getAbnomalType()));
				iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
				
				bidAbnomal.setSubmitDate(DateUtil.getCurrentDateTime());
				bidAbnomal.setRcId(requiredCollect.getRcId());
				iBidAbnomalBiz.saveBidAbnomal(bidAbnomal);
				
				//保存附件			
				iAttachmentBiz.saveAttachmentAndUpload( this.setUploadFile( bidAbnomal, bidAbnomal.getBaId(), AttachmentStatus.ATTACHMENT_CODE_901, UserRightInfoUtil.getUserName(this.getRequest()) ) ) ;
				
				//保存流程跟踪信息
				purchaseRecordLog = new PurchaseRecordLog();
				purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
				purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
				purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
				purchaseRecordLog.setRcId(requiredCollect.getRcId());
				String bidStatus=BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(bidAbnomal.getAbnomalType());
				purchaseRecordLog.setOperateContent("【"+requiredCollect.getBuyRemark()+"】的采购业务状态变更为【"+bidStatus+"】,原因【"+bidAbnomal.getAbnomalReason()+"】");
				purchaseRecordLog.setBidNode("采购业务终止");
				this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
				
				this.getRequest().setAttribute("message", BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(bidAbnomal.getAbnomalType())+"成功");
				this.getRequest().setAttribute("operModule", "保存采购业务终止信息");
			}
			
		} catch (Exception e) {
			log.error("保存采购业务终止信息错误！", e);
			throw new BaseException("保存采购业务终止信息错误！", e);
		}
		return "success";
		
	}
	
	/**
	 * 查看采购业务终止信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBidAbnomalDetail() throws BaseException {
		
		try{
			String rcId=this.getRequest().getParameter("rcId");
			
			bidAbnomal=new BidAbnomal();
			bidAbnomal.setRcId(Long.parseLong(rcId));			
			List<BidAbnomal> babList=iBidAbnomalBiz.getBidAbnomalList(bidAbnomal);
			
			bidAbnomal=babList.get(0);
			bidAbnomal.setAbnomalTypeCn(BaseDataInfosUtil.convertAbnomalTypeToAbnomalType(bidAbnomal.getAbnomalType()));
			bidAbnomal.setWriterCn(BaseDataInfosUtil.convertUserIdToChnName(bidAbnomal.getWriterId()));
			bidAbnomal.setBuyWayCn(BaseDataInfosUtil.convertBuyWayToBuyType(bidAbnomal.getBuyWay()));
			String attachment = iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment(bidAbnomal.getBaId(), AttachmentStatus.ATTACHMENT_CODE_901 ) ) , "0", this.getRequest() );
		
			this.getRequest().setAttribute("attachment", attachment);
			
		} catch (Exception e) {
			log.error("查看采购业务终止信息错误！", e);
			throw new BaseException("查看采购业务终止信息错误！", e);
		}
		return "abnomalDetail";
		
	}
	

	public IBidAbnomalBiz getiBidAbnomalBiz() {
		return iBidAbnomalBiz;
	}

	public void setiBidAbnomalBiz(IBidAbnomalBiz iBidAbnomalBiz) {
		this.iBidAbnomalBiz = iBidAbnomalBiz;
	}

	public BidAbnomal getBidAbnomal() {
		return bidAbnomal;
	}

	public void setBidAbnomal(BidAbnomal bidAbnomal) {
		this.bidAbnomal = bidAbnomal;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}

	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	
}
