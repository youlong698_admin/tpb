package com.ced.sip.purchase.bidding.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.bidding.entity.BiddingBidRound;
/** 
 * 类名称：IBiddingBidRoundBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public interface IBiddingBidRoundBiz {

	/**
	 * 根据主键获得竞价轮次信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingBidRound getBiddingBidRound(Long id) throws BaseException;

	/**
	 * 添加竞价轮次信息信息
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException;

	/**
	 * 更新竞价轮次信息表实例
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException;

	/**
	 * 删除竞价轮次信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidRound(String id) throws BaseException;

	/**
	 * 删除竞价轮次信息表实例
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException;

	/**
	 * 删除竞价轮次信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidRounds(String[] id) throws BaseException;

	/**
	 * 获得竞价轮次信息表数据集
	 * biddingBidRound 竞价轮次信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingBidRound getBiddingBidRoundByBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException ;
	
	/**
	 * 获得所有竞价轮次信息表数据集
	 * @param biddingBidRound 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBiddingBidRoundList(BiddingBidRound biddingBidRound) throws BaseException ;
	
	/**
	 * 获得所有竞价轮次信息表数据集
	 * @param rollPage 分页对象
	 * @param biddingBidRound 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBiddingBidRoundList(RollPage rollPage, BiddingBidRound biddingBidRound)
			throws BaseException;
	/**
	 * 删除竞价轮次表表实例
	 * @param rcId
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidRoundByRcId(Long rcId) throws BaseException;
	/**
	 * 查询当前所处的竞价轮次信息
	 * @param rcId
	 * @param currentBidRound
	 * @param isTrialBid
	 * @return
	 * @throws BaseException
	 */
	abstract BiddingBidRound getCurrentBiddingBidRound(Long rcId,Long currentBidRound,int isTrialBid) throws BaseException;
	/**
	 * 查询最小的轮次信息
	 * @param rcId
	 * @param sign
	 * @throws BaseException
	 */
	abstract int getMinBiddingRound(Long rcId,int sign) throws BaseException;
	/**
	 * 更新延迟竞价信息
	 * @param bbrId
	 * @param rcId
	 * @param delayTime
	 */
	abstract void updateBiddingRoudDelay(Long bbrId,Long rcId,String delayTime) throws BaseException ;
	/**
	 *开始竞价信息
	 * @param bbrId
	 * @param rcId
	 * @param sign
	 */
	abstract void updateBiddingRoudStart(Long bblId,Long rcId,int sign,int minBiddingRound) throws BaseException ;
	/**
	 * 查询竞价是否全部结束
	 * @param rcId
	 * @param sign
	 * @throws BaseException
	 */
	abstract int getAllStopBid(Long rcId) throws BaseException;

}