package com.ced.sip.purchase.bidding.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
/** 
 * 类名称：IBiddingBidListBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public interface IBiddingBidListBiz {

	/**
	 * 根据主键获得竞价信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingBidList getBiddingBidList(Long id) throws BaseException;

	/**
	 * 添加竞价信息信息
	 * @param biddingBidList 竞价信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBiddingBidList(BiddingBidList biddingBidList) throws BaseException;

	/**
	 * 更新竞价信息表实例
	 * @param biddingBidList 竞价信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBiddingBidList(BiddingBidList biddingBidList) throws BaseException;

	/**
	 * 删除竞价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidList(String id) throws BaseException;

	/**
	 * 删除竞价信息表实例
	 * @param biddingBidList 竞价信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidList(BiddingBidList biddingBidList) throws BaseException;

	/**
	 * 删除竞价信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBiddingBidLists(String[] id) throws BaseException;

	/**
	 * 获得竞价信息表数据集
	 * biddingBidList 竞价信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingBidList getBiddingBidListByBiddingBidList(BiddingBidList biddingBidList) throws BaseException ;
	
	/**
	 * 获得所有竞价信息表数据集
	 * @param biddingBidList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBiddingBidListList(BiddingBidList biddingBidList) throws BaseException ;
	
	/**
	 * 获得所有竞价信息表数据集
	 * @param rollPage 分页对象
	 * @param biddingBidList 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBiddingBidListList(RollPage rollPage, BiddingBidList biddingBidList)
			throws BaseException;
	/**
	 * 根据rcId获得竞价_竞价信息表实例
	 * @param rcId 项目ID
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingBidList getBiddingBidListByRcId(Long rcId) throws BaseException;
}