package com.ced.sip.purchase.bidding.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.bidding.biz.IBiddingBidRoundBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidRound;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
/** 
 * 类名称：BiddingBidRoundBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class BiddingBidRoundBizImpl extends BaseBizImpl implements IBiddingBidRoundBiz  {
	
	/**
	 * 根据主键获得竞价轮次信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidRound getBiddingBidRound(Long id) throws BaseException {
		return (BiddingBidRound)this.getObject(BiddingBidRound.class, id);
	}
	
	/**
	 * 获得竞价轮次信息表实例
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidRound getBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException {
		return (BiddingBidRound)this.getObject(BiddingBidRound.class, biddingBidRound.getBbrId() );
	}
	
	/**
	 * 添加竞价轮次信息信息
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void saveBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException{
		this.saveObject( biddingBidRound ) ;
	}
	
	/**
	 * 更新竞价轮次信息表实例
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void updateBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException{
		this.updateObject( biddingBidRound ) ;
	}
	
	/**
	 * 删除竞价轮次信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteBiddingBidRound(String id) throws BaseException {
		this.removeObject( this.getBiddingBidRound( new Long(id) ) ) ;
	}
	
	/**
	 * 删除竞价轮次信息表实例
	 * @param biddingBidRound 竞价轮次信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException {
		this.removeObject( biddingBidRound ) ;
	}
	
	/**
	 * 删除竞价轮次信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteBiddingBidRounds(String[] id) throws BaseException {
		this.removeBatchObject(BiddingBidRound.class, id) ;
	}
	
	/**
	 * 获得竞价轮次信息表数据集
	 * biddingBidRound 竞价轮次信息表实例
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidRound getBiddingBidRoundByBiddingBidRound(BiddingBidRound biddingBidRound) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidRound de where 1 = 1 " );
		if(biddingBidRound != null){
			if(StringUtil.isNotBlank(biddingBidRound.getRcId())){
				hql.append(" and de.rcId =").append(biddingBidRound.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(biddingBidRound.getBidType())){
				hql.append(" and de.bidType =").append(biddingBidRound.getBidType()).append("");
			}
			if(StringUtil.isNotBlank(biddingBidRound.getBiddingRound())){
				hql.append(" and de.biddingRound =").append(biddingBidRound.getBiddingRound()).append("");
			}
		}
		hql.append(" order by de.bbrId ");
		List list = this.getObjects( hql.toString() );
		biddingBidRound = new BiddingBidRound();
		if(list!=null&&list.size()>0){
			biddingBidRound = (BiddingBidRound)list.get(0);
		}
		return biddingBidRound;
	}
	
	/**
	 * 获得所有竞价轮次信息表数据集
	 * @param biddingBidRound 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getBiddingBidRoundList(BiddingBidRound biddingBidRound) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidRound de where 1 = 1 " );
		if(biddingBidRound != null){
			if(StringUtil.isNotBlank(biddingBidRound.getRcId())){
				hql.append(" and de.rcId =").append(biddingBidRound.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(biddingBidRound.getBidType())){
				hql.append(" and de.bidType =").append(biddingBidRound.getBidType()).append("");
			}
		}
		hql.append(" order by de.bbrId ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有竞价轮次信息表数据集
	 * @param rollPage 分页对象
	 * @param biddingBidRound 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getBiddingBidRoundList(RollPage rollPage, BiddingBidRound biddingBidRound) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidRound de where 1 = 1 " );
		if(biddingBidRound != null){
			if(StringUtil.isNotBlank(biddingBidRound.getRcId())){
				hql.append(" and de.rcId =").append(biddingBidRound.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(biddingBidRound.getBidType())){
				hql.append(" and de.bidType =").append(biddingBidRound.getBidType()).append("");
			}
		}
		hql.append(" order by de.bbrId ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 删除竞价轮次表表实例
	 * @param rcId
	 * @throws BaseException 
	 */
	public void deleteBiddingBidRoundByRcId(Long rcId) throws BaseException {
		String sql="delete from bidding_bid_round where rc_id="+rcId;
		this.updateJdbcSql(sql);		
	}

	/**
	 * 查询当前所处的竞价轮次信息
	 * @param rcId
	 * @param currentBidRound
	 * @param isTrialBid
	 * @return
	 * @throws BaseException
	 */
	public BiddingBidRound getCurrentBiddingBidRound(Long rcId, Long currentBidRound,
			int isTrialBid) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidRound de where 1 = 1 " );
		hql.append(" and de.rcId =").append(rcId).append("");
		hql.append(" and de.biddingRound =").append(currentBidRound).append("");
		//isTrialBid 是否试竞价 0为是 1 为否  
		if(isTrialBid==0){
		    hql.append(" and de.bidType =").append(BiddingStatus.BIDDING_LX_0).append("");
		}else{
			hql.append(" and de.bidType =").append(BiddingStatus.BIDDING_LX_1).append("");
		}
		List list = this.getObjects( hql.toString() );
		BiddingBidRound biddingBidRound = new BiddingBidRound();
		if(list!=null&&list.size()>0){
			biddingBidRound = (BiddingBidRound)list.get(0);
		}
		return biddingBidRound;
	}
	/**
	 * 查询最小的轮次信息
	 * @param rcId
	 * @param sign
	 * @throws BaseException
	 */
	public int getMinBiddingRound(Long rcId,int sign) throws BaseException{
		String sql="select min(de.biddingRound) from BiddingBidRound de where de.status=0 and de.bidType="+sign+" and de.rcId="+rcId;
		return (Integer)this.sumOrMinOrMaxObjects(sql, Integer.class);
	}

	/**
	 * 更新延迟竞价信息
	 * @param bbrId
	 * @param rcId
	 * @param delayTime
	 */
	public void updateBiddingRoudDelay(Long bbrId, Long rcId, String delayTime)  throws BaseException {
		int delayTime_tmp=Integer.parseInt(delayTime);
		String sql="begin update bidding_bid_round set time_long=time_long+"+delayTime_tmp*60+",surplus_time=surplus_time+"+delayTime_tmp*60000+" where bbr_id="+bbrId+";" +
				"update bidding_bid_list set is_delay_bid=0 where rc_id="+rcId+";end;";
		this.updateJdbcSql(sql);
		
	}
	/**
	 * 开始竞价信息
	 * @param bbrId
	 * @param rcId
	 * @param sign
	 */
	public void updateBiddingRoudStart(Long bblId, Long rcId, int sign,int minBiddingRound)  throws BaseException {
		//1 正式竞价
		if(sign==1) {
			String sql="begin ";
			if(minBiddingRound==1){
				//正式竞价前删除试竞价的报价干扰信息
				sql+="delete from bid_price where rc_id="+rcId+";";
				sql+="delete from bid_price_detail where rc_id="+rcId+";";
				sql+="delete from bid_price_detail_history where rc_id="+rcId+";";
				sql+="delete from bid_price_history where rc_id="+rcId+";";
				sql+="delete from bid_business_response where rc_id="+rcId+";";
				sql+="delete from bid_business_response_history where rc_id="+rcId+";";
				sql+="update bidding_bid_list set bidding_real_time=sysdate where bbl_id="+bblId+";";
			}
			sql+="update bidding_bid_round set start_time=sysdate,suspend_time=0,again_start_time=sysdate,status="+BiddingStatus.BIDDING_STATUS_1+"  where bid_type=1 and bidding_round="+minBiddingRound+" and rc_id="+rcId+";" +
					"update bidding_bid_list set current_bid_round="+minBiddingRound+", is_trial_bid=1,is_delay_bid=1 where bbl_id="+bblId+";end;";
			this.updateJdbcSql(sql);
		}else{
			String sql="begin ";
			if(minBiddingRound==1){
				sql+="update bidding_bid_list set bidding_real_time=sysdate where bbl_id="+bblId+";";
			}
			sql+="update bidding_bid_round set start_time=sysdate,suspend_time=0,again_start_time=sysdate,status="+BiddingStatus.BIDDING_STATUS_1+"  where bid_type=0 and bidding_round="+minBiddingRound+" and rc_id="+rcId+";" +
			"update bidding_bid_list set current_bid_round="+minBiddingRound+", is_trial_bid=0 where bbl_id="+bblId+";end;";
	        this.updateJdbcSql(sql);
		}
		
	}

	
	public int getAllStopBid(Long rcId) throws BaseException {
		String sql="select count(bbrId) from BiddingBidRound de where de.status!=3 and de.rcId="+rcId;
		return this.countObjects(sql);
	}
	
}
