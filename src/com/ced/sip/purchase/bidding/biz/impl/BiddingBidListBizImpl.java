package com.ced.sip.purchase.bidding.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
/** 
 * 类名称：BiddingBidListBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class BiddingBidListBizImpl extends BaseBizImpl implements IBiddingBidListBiz  {
	
	/**
	 * 根据主键获得竞价信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidList getBiddingBidList(Long id) throws BaseException {
		return (BiddingBidList)this.getObject(BiddingBidList.class, id);
	}
	
	/**
	 * 获得竞价信息表实例
	 * @param biddingBidList 竞价信息表实例
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidList getBiddingBidList(BiddingBidList biddingBidList) throws BaseException {
		return (BiddingBidList)this.getObject(BiddingBidList.class, biddingBidList.getBblId() );
	}
	
	/**
	 * 添加竞价信息信息
	 * @param biddingBidList 竞价信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void saveBiddingBidList(BiddingBidList biddingBidList) throws BaseException{
		this.saveObject( biddingBidList ) ;
	}
	
	/**
	 * 更新竞价信息表实例
	 * @param biddingBidList 竞价信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void updateBiddingBidList(BiddingBidList biddingBidList) throws BaseException{
		this.updateObject( biddingBidList ) ;
	}
	
	/**
	 * 删除竞价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteBiddingBidList(String id) throws BaseException {
		this.removeObject( this.getBiddingBidList( new Long(id) ) ) ;
	}
	
	/**
	 * 删除竞价信息表实例
	 * @param biddingBidList 竞价信息表实例
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteBiddingBidList(BiddingBidList biddingBidList) throws BaseException {
		this.removeObject( biddingBidList ) ;
	}
	
	/**
	 * 删除竞价信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-17
	 * @throws BaseException 
	 */
	public void deleteBiddingBidLists(String[] id) throws BaseException {
		this.removeBatchObject(BiddingBidList.class, id) ;
	}
	
	/**
	 * 获得竞价信息表数据集
	 * biddingBidList 竞价信息表实例
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidList getBiddingBidListByBiddingBidList(BiddingBidList biddingBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidList de where 1 = 1 " );
		if(biddingBidList != null){
			if(StringUtil.isNotBlank(biddingBidList.getRcId())){
				hql.append(" and de.rcId =").append(biddingBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.bblId desc ");
		List list = this.getObjects( hql.toString() );
		biddingBidList = new BiddingBidList();
		if(list!=null&&list.size()>0){
			biddingBidList = (BiddingBidList)list.get(0);
		}
		return biddingBidList;
	}
	
	/**
	 * 获得所有竞价信息表数据集
	 * @param biddingBidList 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getBiddingBidListList(BiddingBidList biddingBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidList de where 1 = 1 " );
		if(biddingBidList != null){
			if(StringUtil.isNotBlank(biddingBidList.getRcId())){
				hql.append(" and de.rcId =").append(biddingBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.bblId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有竞价信息表数据集
	 * @param rollPage 分页对象
	 * @param biddingBidList 查询参数对象
	 * @author luguanglei 2017-05-17
	 * @return
	 * @throws BaseException 
	 */
	public List getBiddingBidListList(RollPage rollPage, BiddingBidList biddingBidList) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidList de where 1 = 1 " );
		if(biddingBidList != null){
			if(StringUtil.isNotBlank(biddingBidList.getRcId())){
				hql.append(" and de.rcId =").append(biddingBidList.getRcId()).append("");
			}
		}
		hql.append(" order by de.bblId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 根据rcId获得竞价_竞价信息表实例
	 * @param rcId 项目ID
	 * @return
	 * @throws BaseException 
	 */
	public BiddingBidList getBiddingBidListByRcId(Long rcId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingBidList de where de.rcId =").append(rcId);
        List list = this.getObjects( hql.toString() );
        BiddingBidList biddingBidList = new BiddingBidList();
		if(list!=null&&list.size()>0){
			biddingBidList = (BiddingBidList)list.get(0);
		}
		return biddingBidList;
	}
}
