package com.ced.sip.purchase.bidding.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.bidding.biz.IBiddingCommunicationInfoBiz;
import com.ced.sip.purchase.bidding.entity.BiddingCommunicationInfo;
/** 
 * 类名称：BiddingCommunicationInfoBizImpl
 * 创建人：luguanglei 
 * 创建时间：2017-05-19
 */
public class BiddingCommunicationInfoBizImpl extends BaseBizImpl implements IBiddingCommunicationInfoBiz  {
	
	/**
	 * 根据主键获得竞价交流信息表实例
	 * @param id 主键
	 * @author luguanglei 2017-05-19
	 * @return
	 * @throws BaseException 
	 */
	public BiddingCommunicationInfo getBiddingCommunicationInfo(Long id) throws BaseException {
		return (BiddingCommunicationInfo)this.getObject(BiddingCommunicationInfo.class, id);
	}
	
	/**
	 * 获得竞价交流信息表实例
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @author luguanglei 2017-05-19
	 * @return
	 * @throws BaseException 
	 */
	public BiddingCommunicationInfo getBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException {
		return (BiddingCommunicationInfo)this.getObject(BiddingCommunicationInfo.class, biddingCommunicationInfo.getBciId() );
	}
	
	/**
	 * 添加竞价交流信息信息
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @author luguanglei 2017-05-19
	 * @throws BaseException 
	 */
	public void saveBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException{
		this.saveObject( biddingCommunicationInfo ) ;
	}
	
	/**
	 * 更新竞价交流信息表实例
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @author luguanglei 2017-05-19
	 * @throws BaseException 
	 */
	public void updateBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException{
		this.updateObject( biddingCommunicationInfo ) ;
	}
	
	/**
	 * 删除竞价交流信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-19
	 * @throws BaseException 
	 */
	public void deleteBiddingCommunicationInfo(String id) throws BaseException {
		this.removeObject( this.getBiddingCommunicationInfo( new Long(id) ) ) ;
	}
	
	/**
	 * 删除竞价交流信息表实例
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @author luguanglei 2017-05-19
	 * @throws BaseException 
	 */
	public void deleteBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException {
		this.removeObject( biddingCommunicationInfo ) ;
	}
	
	/**
	 * 删除竞价交流信息表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-05-19
	 * @throws BaseException 
	 */
	public void deleteBiddingCommunicationInfos(String[] id) throws BaseException {
		this.removeBatchObject(BiddingCommunicationInfo.class, id) ;
	}
	
	/**
	 * 获得竞价交流信息表数据集
	 * biddingCommunicationInfo 竞价交流信息表实例
	 * @author luguanglei 2017-05-19
	 * @return
	 * @throws BaseException 
	 */
	public BiddingCommunicationInfo getBiddingCommunicationInfoByBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingCommunicationInfo de where 1 = 1 " );

		hql.append(" order by de.bciId desc ");
		List list = this.getObjects( hql.toString() );
		biddingCommunicationInfo = new BiddingCommunicationInfo();
		if(list!=null&&list.size()>0){
			biddingCommunicationInfo = (BiddingCommunicationInfo)list.get(0);
		}
		return biddingCommunicationInfo;
	}
	
	/**
	 * 获得所有竞价交流信息表数据集
	 * @param biddingCommunicationInfo 查询参数对象
	 * @author luguanglei 2017-05-19
	 * @return
	 * @throws BaseException 
	 */
	public List getBiddingCommunicationInfoList(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BiddingCommunicationInfo de where 1 = 1 " );

		hql.append(" order by de.bciId desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有竞价交流信息表数据集
	 * @param rollPage 分页对象
	 * @param biddingCommunicationInfo 查询参数对象
	 * @author luguanglei 2017-05-19
	 * @return
	 * @throws BaseException 
	 */
	public List getBiddingCommunicationInfoList(RollPage rollPage, BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException {
		StringBuffer hql = new StringBuffer(" select de  from BiddingCommunicationInfo de where (de.publish="+biddingCommunicationInfo.getPublish()+"  or de.receive is null ");
		if(biddingCommunicationInfo != null){
			if(StringUtil.isNotBlank(biddingCommunicationInfo.getReceive())){
		    hql.append("or de.receive="+biddingCommunicationInfo.getReceive()+"" );
		    }
		}		
		hql.append(" ) and de.rcId="+biddingCommunicationInfo.getRcId()+" " );
        hql.append(" order by de.bciId desc ");
        //System.out.println(hql.toString());
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
