package com.ced.sip.purchase.bidding.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.purchase.bidding.entity.BiddingCommunicationInfo;
/** 
 * 类名称：IBiddingCommunicationInfoBiz
 * 创建人：luguanglei 
 * 创建时间：2017-05-19
 */
public interface IBiddingCommunicationInfoBiz {

	/**
	 * 根据主键获得竞价交流信息表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingCommunicationInfo getBiddingCommunicationInfo(Long id) throws BaseException;

	/**
	 * 添加竞价交流信息信息
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @throws BaseException 
	 */
	abstract void saveBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException;

	/**
	 * 更新竞价交流信息表实例
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @throws BaseException 
	 */
	abstract void updateBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException;

	/**
	 * 删除竞价交流信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBiddingCommunicationInfo(String id) throws BaseException;

	/**
	 * 删除竞价交流信息表实例
	 * @param biddingCommunicationInfo 竞价交流信息表实例
	 * @throws BaseException 
	 */
	abstract void deleteBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException;

	/**
	 * 删除竞价交流信息表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteBiddingCommunicationInfos(String[] id) throws BaseException;

	/**
	 * 获得竞价交流信息表数据集
	 * biddingCommunicationInfo 竞价交流信息表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BiddingCommunicationInfo getBiddingCommunicationInfoByBiddingCommunicationInfo(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException ;
	
	/**
	 * 获得所有竞价交流信息表数据集
	 * @param biddingCommunicationInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBiddingCommunicationInfoList(BiddingCommunicationInfo biddingCommunicationInfo) throws BaseException ;
	
	/**
	 * 获得所有竞价交流信息表数据集
	 * @param rollPage 分页对象
	 * @param biddingCommunicationInfo 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBiddingCommunicationInfoList(RollPage rollPage, BiddingCommunicationInfo biddingCommunicationInfo)
			throws BaseException;

}