package com.ced.sip.purchase.bidding.util;

public class BiddingProgressStatus {
//	--------------------------- 询价采购流程节点状态  begin---------------------------
//   采购流程节点状态以 1012 显示，前两位为大的流程节点  后两位为小的流程节点
	/** 竞价方案 */
	public static long Progress_Status_20 = 20 ;
	/** 竞价方案 */
	public static String Progress_Status_20_Text = "竞价方案" ;	
	/** 竞价方案 url */
	public static String Progress_Status_20_URL = "viewBiddingBidListMonitor_biddingBidList.action" ;
	/** 竞价方案 IMG */
	public static String Progress_Status_20_IMG = "images/bidImages/swhz.png" ;
	
	
	/** 竞价公告 */
	public static long Progress_Status_21 = 21 ;
	/** 竞价公告 */
	public static String Progress_Status_21_Text = "竞价公告" ;	
	/** 竞价公告 url */
	public static String Progress_Status_21_URL = "viewBidBulletinBidMonitor_bidBulletin.action" ;
	/** 竞价公告 IMG */
	public static String Progress_Status_21_IMG = "images/bidImages/gg.png" ;
	
	
	/** 竞价响应 */
	public static long Progress_Status_22 = 22 ;
	/** 竞价响应 */
	public static String Progress_Status_22_Text = "竞价响应" ;	
	/** 竞价响应 url */
	public static String Progress_Status_22_URL = "viewBiddingReceivedBulletinMonitor_biddingReceivedBulletin.action" ;
	/** 竞价响应 IMG */
	public static String Progress_Status_22_IMG = "images/bidImages/ybgl.png" ;
	
	
	/** 标前澄清 */
	public static long Progress_Status_23 = 23 ;
	/** 标前澄清 */
	public static String Progress_Status_23_Text = "标前澄清" ;	
	/** 标前澄清 url */
	public static String Progress_Status_23_URL = "viewBidClarifyMonitor_bidClarify.action" ;
	/** 标前澄清 IMG */
	public static String Progress_Status_23_IMG = "images/bidImages/wtcq.png" ;
	
	
	/** 问题解答 */
	public static long Progress_Status_24 = 24 ;
	/** 问题解答 */
	public static String Progress_Status_24_Text = "问题解答" ;	
	/** 问题解答 url */
	public static String Progress_Status_24_URL = "viewBidCommunicationInfoMonitor_bidCommunicationInfo.action" ;
	/** 问题解答 IMG */
	public static String Progress_Status_24_IMG = "images/bidImages/sr.png" ;
	
	
	
	/** 竞价现场 */
	public static long Progress_Status_25 = 25 ;
	/** 竞价现场 */
	public static String Progress_Status_25_Text = "竞价现场" ;	
	/** 竞价现场 url */
	public static String Progress_Status_25_URL = "viewBiddingBidSceneMonitor_biddingBidScene.action" ;
	/** 竞价现场 IMG */
	public static String Progress_Status_25_IMG = "images/bidImages/kbxc.png" ;
	
	/** 授标 */
	public static long Progress_Status_26 = 26 ;
	/** 授标 */
	public static String Progress_Status_26_Text = "授标" ;	
	/** 授标 url */
	public static String Progress_Status_26_URL = "viewBidAwardMonitor_bidAward.action" ;
	/** 授标 IMG */
	public static String Progress_Status_26_IMG = "images/bidImages/sr.png" ;
	
	/** 结果公示 */
	public static long Progress_Status_27 = 27 ;
	/** 结果公示 */
	public static String Progress_Status_27_Text = "结果公示" ;	
	/** 结果公示 url */
	public static String Progress_Status_27_URL = "viewBidWinningBidMonitor_bidWinning.action" ;
	/** 结果公示 IMG */
	public static String Progress_Status_27_IMG = "images/bidImages/fqht.png" ;
	
	
	/** 中标通知 */
	public static long Progress_Status_28 = 28 ;
	/** 中标通知 */
	public static String Progress_Status_28_Text = "中标通知" ;	
	/** 中标通知 url */
	public static String Progress_Status_28_URL = "viewBidResultNoticeMonitor_bidResultNotice.action" ;
	/** 中标通知 IMG */
	public static String Progress_Status_28_IMG = "images/bidImages/zbtzs.png" ;
	
	/** 采购完成 */
	public static long Progress_Status_29 = 29 ;
	/** 采购完成 */
	public static String Progress_Status_29_Text = "竞价完成" ;	
	
//  ---------------------------  整个采购流程节点状态  end---------------------------
}
