package com.ced.sip.purchase.bidding.util;

public class BiddingStatus {

	//	--------------------------- 竞价方式 ---------------------------
	/** 竞买 */
	public static final String BIDDING_TYPE_0 = "0" ;
	
	/** 竞买   */
	public static final String BIDDING_TYPE_0_TEXT = "竞买 " ;
	
	/** 竞卖 */
	public static final String BIDDING_TYPE_1 = "1" ;
	
	/** 竞卖 */
	public static final String BIDDING_TYPE_1_TEXT = "竞卖" ;
	
//	--------------------------- 竞价类型 ---------------------------
	/** 试竞价 */
	public static final int BIDDING_LX_0 = 0;
	
	/** 正式竞价 */
	public static final int BIDDING_LX_1 = 1;
	
//	--------------------------- 报价显示方式 ---------------------------
	/** 竞争对手和报价全部显示 */
	public static final String PRICE_MODE_0 = "0" ;
	
	/** 竞争对手和报价全部显示   */
	public static final String PRICE_MODE_0_TEXT = "竞争对手和报价全部显示" ;
	
	/** 竞争对手和报价都隐藏 */
	public static final String PRICE_MODE_1 = "1" ;
	
	/** 竞争对手和报价都隐藏 */
	public static final String PRICE_MODE_1_TEXT = "竞争对手和报价都隐藏" ;
	
	/** 竞争对手显示，报价隐藏 */
	public static final String PRICE_MODE_2 = "2" ;
	
	/** 竞争对手显示，报价隐藏   */
	public static final String PRICE_MODE_2_TEXT = "竞争对手显示，报价隐藏" ;
	
	/** 报价显示，竞争对手隐藏 */
	public static final String PRICE_MODE_3 = "3" ;
	
	/** 报价显示，竞争对手隐藏 */
	public static final String PRICE_MODE_3_TEXT = "报价显示，竞争对手隐藏" ;
	
	
//	--------------------------- 报价原则 ---------------------------
	/** 越来越低 */
	public static final String PRICE_PRINCIPLE_0 = "0" ;
	
	/** 越来越低    */
	public static final String PRICE_PRINCIPLE_0_TEXT = "越来越低" ;
	
	/** 越来越高  */
	public static final String PRICE_PRINCIPLE_1 = "1" ;
	
	/** 越来越高  */
	public static final String PRICE_PRINCIPLE_1_TEXT = "越来越高" ;
	
	
//	--------------------------- 竞价策略 ---------------------------
	/** 单价基准竞价 */
	public static final String BIDDING_PRINCIPLE_0 = "0" ;
	
	/** 单价基准竞价   */
	public static final String BIDDING_PRINCIPLE_0_TEXT = "单价基准竞价" ;
	
	/** 总价基准竞价*/
	public static final String BIDDING_PRINCIPLE_1 = "1" ;
	
	/** 总价基准竞价 */
	public static final String BIDDING_PRINCIPLE_1_TEXT = "总价基准竞价" ;
//	--------------------------- 竞价状态 ---------------------------
	/** 未开始 */
	public static final int BIDDING_STATUS_0 = 0;
	
	/** 开始 */
	public static final int BIDDING_STATUS_1 = 1;
	
	/** 暂停*/
	public static final int BIDDING_STATUS_2 = 2;
	
	/** 结束 */
	public static final int BIDDING_STATUS_3 = 3;
	
	/** 暂停后重新开始*/
	public static final int BIDDING_STATUS_4 = 4;
//	--------------------------- 交流类型 ---------------------------
	/** 管理员对供应商 */
	public static final long COMMUNICATION_INFO_0 = 0;
	
	/** 供应商对管理员 */
	public static final long COMMUNICATION_INFO_1 = 1;
}
