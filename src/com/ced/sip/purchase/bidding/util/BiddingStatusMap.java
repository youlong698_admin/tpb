package com.ced.sip.purchase.bidding.util;

import java.util.LinkedHashMap;
import java.util.Map;


public class BiddingStatusMap {
	/**
	 * 竞价方式Map
	 */
	public static Map<String, String> biddingType = new LinkedHashMap<String, String>();
	static{
		biddingType.put(BiddingStatus.BIDDING_TYPE_0,BiddingStatus.BIDDING_TYPE_0_TEXT);
		biddingType.put(BiddingStatus.BIDDING_TYPE_1,BiddingStatus.BIDDING_TYPE_1_TEXT);
	}
	/**
	 * 报价显示方式Map
	 */
	public static Map<String, String> priceMode = new LinkedHashMap<String, String>();
	static{
		priceMode.put(BiddingStatus.PRICE_MODE_0,BiddingStatus.PRICE_MODE_0_TEXT);
		priceMode.put(BiddingStatus.PRICE_MODE_1,BiddingStatus.PRICE_MODE_1_TEXT);
		priceMode.put(BiddingStatus.PRICE_MODE_2,BiddingStatus.PRICE_MODE_2_TEXT);
		priceMode.put(BiddingStatus.PRICE_MODE_3,BiddingStatus.PRICE_MODE_3_TEXT);
	}
	/**
	 * 报价原则Map
	 */
	public static Map<String, String> pricePrinciple = new LinkedHashMap<String, String>();
	static{
		pricePrinciple.put(BiddingStatus.PRICE_PRINCIPLE_0,BiddingStatus.PRICE_PRINCIPLE_0_TEXT);
		pricePrinciple.put(BiddingStatus.PRICE_PRINCIPLE_1,BiddingStatus.PRICE_PRINCIPLE_1_TEXT);
	}
	/**
	 * 竞价策略Map
	 */
	public static Map<String, String> biddingPrinciple = new LinkedHashMap<String, String>();
	static{
		biddingPrinciple.put(BiddingStatus.BIDDING_PRINCIPLE_0,BiddingStatus.BIDDING_PRINCIPLE_0_TEXT);
		biddingPrinciple.put(BiddingStatus.BIDDING_PRINCIPLE_1,BiddingStatus.BIDDING_PRINCIPLE_1_TEXT);
	}

}
