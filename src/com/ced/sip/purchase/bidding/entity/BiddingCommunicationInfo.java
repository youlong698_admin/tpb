package com.ced.sip.purchase.bidding.entity;

import java.util.Date;

/** 
 * 类名称：BiddingCommunicationInfo
 * 创建人：luguanglei 
 * 创建时间：2017-05-19
 */
public class BiddingCommunicationInfo implements java.io.Serializable {

	// 属性信息
	private Long bciId;     //主键
	private Long rcId;     //项目ID
	private Long publish;     //发布人编号
	private String publisher;	 //发布人
	private Long receive;     //接收人编号
	private String receiver;	 //接收人
	private String info;	 //信息内容
	private Date publishDate;    //发布时间
	private Long type;     //类型
	
	
	public BiddingCommunicationInfo() {
		super();
	}
	
	public Long getBciId(){
	   return  bciId;
	} 
	public void setBciId(Long bciId) {
	   this.bciId = bciId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public Long getPublish(){
	   return  publish;
	} 
	public void setPublish(Long publish) {
	   this.publish = publish;
    }     
	public String getPublisher(){
	   return  publisher;
	} 
	public void setPublisher(String publisher) {
	   this.publisher = publisher;
    }
	public Long getReceive(){
	   return  receive;
	} 
	public void setReceive(Long receive) {
	   this.receive = receive;
    }     
	public String getReceiver(){
	   return  receiver;
	} 
	public void setReceiver(String receiver) {
	   this.receiver = receiver;
    }
	public String getInfo(){
	   return  info;
	} 
	public void setInfo(String info) {
	   this.info = info;
    }
	public Date getPublishDate(){
	   return  publishDate;
	} 
	public void setPublishDate(Date publishDate) {
	   this.publishDate = publishDate;
    }	    
	public Long getType(){
	   return  type;
	} 
	public void setType(Long type) {
	   this.type = type;
    }     
}