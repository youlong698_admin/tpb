package com.ced.sip.purchase.bidding.entity;

import java.util.Date;

/** 
 * 类名称：BiddingBidRound
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class BiddingBidRound implements java.io.Serializable {

	// 属性信息
	private Long bbrId;     //主键
	private Long rcId;     //项目ID
	private int biddingRound;     //竞拍轮次
	private Date startTime;    //本轮起始时间
	private Date endTime;    //本轮结束时间
	private int timeLong;     //时长
	private int suspendTime;     //暂停时间
	private int surplusTime;     //剩余时间
	private Date againStartTime;    //重新开始时间
	private int status;	 //状态 0 未开始 1开始    2暂停   3结束 4暂停重新开始
	private int bidType;	 //竞价类型
	private Long priceCount;//限制供应商报价次数
	
	
	public BiddingBidRound() {
		super();
	}
	
	public Long getBbrId(){
	   return  bbrId;
	} 
	public void setBbrId(Long bbrId) {
	   this.bbrId = bbrId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public int getBiddingRound(){
	   return  biddingRound;
	} 
	public void setBiddingRound(int biddingRound) {
	   this.biddingRound = biddingRound;
    }     
	public Date getStartTime(){
	   return  startTime;
	} 
	public void setStartTime(Date startTime) {
	   this.startTime = startTime;
    }	    
	public Date getEndTime(){
	   return  endTime;
	} 
	public void setEndTime(Date endTime) {
	   this.endTime = endTime;
    }	    
	public int getTimeLong(){
	   return  timeLong;
	} 
	public void setTimeLong(int timeLong) {
	   this.timeLong = timeLong;
    }     
	public int getSuspendTime(){
	   return  suspendTime;
	} 
	public void setSuspendTime(int suspendTime) {
	   this.suspendTime = suspendTime;
    }     
	public int getSurplusTime(){
	   return  surplusTime;
	} 
	public void setSurplusTime(int surplusTime) {
	   this.surplusTime = surplusTime;
    }     
	public Date getAgainStartTime(){
	   return  againStartTime;
	} 
	public void setAgainStartTime(Date againStartTime) {
	   this.againStartTime = againStartTime;
    }	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public int getBidType() {
		return bidType;
	}

	public void setBidType(int bidType) {
		this.bidType = bidType;
	}

	public Long getPriceCount() {
		return priceCount;
	}

	public void setPriceCount(Long priceCount) {
		this.priceCount = priceCount;
	}
}