package com.ced.sip.purchase.bidding.entity;

import java.util.Date;

/** 
 * 类名称：BiddingBidList
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class BiddingBidList implements java.io.Serializable {

	// 属性信息
	private Long bblId;     //主键
	private Long rcId;     //项目ID
	private String biddingType;	 //竞价方式
	private String priceMode;	 //报价方式
	private String pricePrinciple;	 //报价原则
	private String biddingPrinciple;	 //竞价策略
	private int biddingRound;     //竞拍轮次
	private Double hignPrice;	//最高限价
	private Double minPrice;	//最低限价
	private Date biddingStartTime;    //竞价开始时间
	private Date biddingEndTime;    //竞价结束时间
	private String supplierRights;	 //供应商权限
	private String biddingAdminRights;	 //竞价管理员权限
	private Long delayTime;     //延迟时间
	private int trialBiddingRound;     //试竞价轮次
	private int isDelay;	 //是否延迟
	private String biddingMode;	 //专家决标 价格决标
	private String priceType;	 //报价类型
	private String priceColumnType;	 //报价列类型
	private String responsibleUser;	 //项目负责人
	private String responsiblePhone;	 //负责人手机号
	private String writer;	 //编制人
	private Date writeDate;    //编制日期
	private String remark1;	 //备用1
	private String remark2;	 //备用2
	private String remark3;	 //备用3
	private String remark4;	 //备用4
	private String remark;	 //备注
	private Long bidAdmin;//竞价管理员
	private int isBidEnd;//是否竞价结束
	private int isDelayBid;//是否延迟竞价
	private int isTrialBid;//是否试竞价
	private Long currentBidRound;//当前竞价轮次
	private Date biddingRealTime;//竞价实际开始时间
	

	private String priceTypeCn;	 //报价类型
	private String priceColumnTypeCn;	 //报价列类型;
	private String biddingTypeCn;	 //竞价方式
	private String priceModeCn;	 //报价方式
	private String pricePrincipleCn;	 //报价原则
	private String biddingPrincipleCn;	 //竞价策略
	private String biddingStartTimeStr;    //竞价开始时间
	private String biddingEndTimeStr;    //竞价结束时间
	private String bidAdminCn;//竞价管理员
	
	
	public BiddingBidList() {
		super();
	}
	
	public Long getBblId(){
	   return  bblId;
	} 
	public void setBblId(Long bblId) {
	   this.bblId = bblId;
    }     
	public Long getRcId(){
	   return  rcId;
	} 
	public void setRcId(Long rcId) {
	   this.rcId = rcId;
    }     
	public String getBiddingType(){
	   return  biddingType;
	} 
	public void setBiddingType(String biddingType) {
	   this.biddingType = biddingType;
    }
	public String getPriceMode(){
	   return  priceMode;
	} 
	public void setPriceMode(String priceMode) {
	   this.priceMode = priceMode;
    }
	public String getPricePrinciple(){
	   return  pricePrinciple;
	} 
	public void setPricePrinciple(String pricePrinciple) {
	   this.pricePrinciple = pricePrinciple;
    }
	public String getBiddingPrinciple(){
	   return  biddingPrinciple;
	} 
	public void setBiddingPrinciple(String biddingPrinciple) {
	   this.biddingPrinciple = biddingPrinciple;
    }
	public int getBiddingRound(){
	   return  biddingRound;
	} 
	public void setBiddingRound(int biddingRound) {
	   this.biddingRound = biddingRound;
    }     
	public Double getHignPrice(){
	   return  hignPrice;
	} 
	public void setHignPrice(Double hignPrice) {
	   this.hignPrice = hignPrice;
    }	
	public Double getMinPrice(){
	   return  minPrice;
	} 
	public void setMinPrice(Double minPrice) {
	   this.minPrice = minPrice;
    }	
	public Date getBiddingStartTime(){
	   return  biddingStartTime;
	} 
	public void setBiddingStartTime(Date biddingStartTime) {
	   this.biddingStartTime = biddingStartTime;
    }	    
	public Date getBiddingEndTime(){
	   return  biddingEndTime;
	} 
	public void setBiddingEndTime(Date biddingEndTime) {
	   this.biddingEndTime = biddingEndTime;
    }	    
	public String getSupplierRights(){
	   return  supplierRights;
	} 
	public void setSupplierRights(String supplierRights) {
	   this.supplierRights = supplierRights;
    }
	public String getBiddingAdminRights(){
	   return  biddingAdminRights;
	} 
	public void setBiddingAdminRights(String biddingAdminRights) {
	   this.biddingAdminRights = biddingAdminRights;
    }
	public Long getDelayTime(){
	   return  delayTime;
	} 
	public void setDelayTime(Long delayTime) {
	   this.delayTime = delayTime;
    }     
	public int getTrialBiddingRound(){
	   return  trialBiddingRound;
	} 
	public void setTrialBiddingRound(int trialBiddingRound) {
	   this.trialBiddingRound = trialBiddingRound;
    }     
	public int getIsDelay(){
	   return  isDelay;
	} 
	public void setIsDelay(int isDelay) {
	   this.isDelay = isDelay;
    }
	public String getBiddingMode(){
	   return  biddingMode;
	} 
	public void setBiddingMode(String biddingMode) {
	   this.biddingMode = biddingMode;
    }
	public String getPriceType(){
	   return  priceType;
	} 
	public void setPriceType(String priceType) {
	   this.priceType = priceType;
    }
	public String getPriceColumnType(){
	   return  priceColumnType;
	} 
	public void setPriceColumnType(String priceColumnType) {
	   this.priceColumnType = priceColumnType;
    }
	public String getResponsibleUser(){
	   return  responsibleUser;
	} 
	public void setResponsibleUser(String responsibleUser) {
	   this.responsibleUser = responsibleUser;
    }
	public String getResponsiblePhone(){
	   return  responsiblePhone;
	} 
	public void setResponsiblePhone(String responsiblePhone) {
	   this.responsiblePhone = responsiblePhone;
    }
	public String getWriter(){
	   return  writer;
	} 
	public void setWriter(String writer) {
	   this.writer = writer;
    }
	public Date getWriteDate(){
	   return  writeDate;
	} 
	public void setWriteDate(Date writeDate) {
	   this.writeDate = writeDate;
    }	    
	public String getRemark1(){
	   return  remark1;
	} 
	public void setRemark1(String remark1) {
	   this.remark1 = remark1;
    }
	public String getRemark2(){
	   return  remark2;
	} 
	public void setRemark2(String remark2) {
	   this.remark2 = remark2;
    }
	public String getRemark3(){
	   return  remark3;
	} 
	public void setRemark3(String remark3) {
	   this.remark3 = remark3;
    }
	public String getRemark4(){
	   return  remark4;
	} 
	public void setRemark4(String remark4) {
	   this.remark4 = remark4;
    }
	public String getRemark(){
	   return  remark;
	} 
	public void setRemark(String remark) {
	   this.remark = remark;
    }

	public String getPriceTypeCn() {
		return priceTypeCn;
	}

	public void setPriceTypeCn(String priceTypeCn) {
		this.priceTypeCn = priceTypeCn;
	}

	public String getPriceColumnTypeCn() {
		return priceColumnTypeCn;
	}

	public void setPriceColumnTypeCn(String priceColumnTypeCn) {
		this.priceColumnTypeCn = priceColumnTypeCn;
	}

	public String getBiddingTypeCn() {
		return biddingTypeCn;
	}

	public void setBiddingTypeCn(String biddingTypeCn) {
		this.biddingTypeCn = biddingTypeCn;
	}

	public String getPriceModeCn() {
		return priceModeCn;
	}

	public void setPriceModeCn(String priceModeCn) {
		this.priceModeCn = priceModeCn;
	}

	public String getPricePrincipleCn() {
		return pricePrincipleCn;
	}

	public void setPricePrincipleCn(String pricePrincipleCn) {
		this.pricePrincipleCn = pricePrincipleCn;
	}

	public String getBiddingPrincipleCn() {
		return biddingPrincipleCn;
	}

	public void setBiddingPrincipleCn(String biddingPrincipleCn) {
		this.biddingPrincipleCn = biddingPrincipleCn;
	}

	public String getBiddingStartTimeStr() {
		return biddingStartTimeStr;
	}

	public void setBiddingStartTimeStr(String biddingStartTimeStr) {
		this.biddingStartTimeStr = biddingStartTimeStr;
	}

	public String getBiddingEndTimeStr() {
		return biddingEndTimeStr;
	}

	public void setBiddingEndTimeStr(String biddingEndTimeStr) {
		this.biddingEndTimeStr = biddingEndTimeStr;
	}

	public Long getBidAdmin() {
		return bidAdmin;
	}

	public void setBidAdmin(Long bidAdmin) {
		this.bidAdmin = bidAdmin;
	}

	public String getBidAdminCn() {
		return bidAdminCn;
	}

	public void setBidAdminCn(String bidAdminCn) {
		this.bidAdminCn = bidAdminCn;
	}

	public int getIsBidEnd() {
		return isBidEnd;
	}

	public void setIsBidEnd(int isBidEnd) {
		this.isBidEnd = isBidEnd;
	}

	public int getIsDelayBid() {
		return isDelayBid;
	}

	public void setIsDelayBid(int isDelayBid) {
		this.isDelayBid = isDelayBid;
	}

	public int getIsTrialBid() {
		return isTrialBid;
	}

	public void setIsTrialBid(int isTrialBid) {
		this.isTrialBid = isTrialBid;
	}

	public Long getCurrentBidRound() {
		return currentBidRound;
	}

	public void setCurrentBidRound(Long currentBidRound) {
		this.currentBidRound = currentBidRound;
	}

	public Date getBiddingRealTime() {
		return biddingRealTime;
	}

	public void setBiddingRealTime(Date biddingRealTime) {
		this.biddingRealTime = biddingRealTime;
	}
}