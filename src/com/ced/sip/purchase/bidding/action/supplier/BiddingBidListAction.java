package com.ced.sip.purchase.bidding.action.supplier;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.biz.IBiddingBidRoundBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.entity.BiddingBidRound;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
import com.ced.sip.purchase.bidding.util.BiddingStatusMap;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Users;

public class BiddingBidListAction extends BaseAction {
	 //竞价信息服务类
	private IBiddingBidListBiz iBiddingBidListBiz;
    //竞价轮次信息管理
    private IBiddingBidRoundBiz iBiddingBidRoundBiz;
	 //邀请供应商服务类
	private IInviteSupplierBiz iInviteSupplierBiz;
	 //项目信息服务类
	private IRequiredCollectBiz iRequiredCollectBiz;
	private Long rcId;
	
	private RequiredCollect requiredCollect;
	private BiddingBidList biddingBidList;
    private BiddingBidRound biddingBidRound;
	private InviteSupplier inviteSupplier;
    
	/**
	 * 竞价项目报名初始化页面
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitBiddingBidListApplication() throws BaseException {
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			biddingBidList = this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
			biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
			biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
			biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
			biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
			biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
			
			
			inviteSupplier = new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			inviteSupplier.setSupplierId(UserRightInfoUtil.getSupplierId(this.getRequest()));
			List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
            boolean isApplication=false;
			if(sulist.size()>0) isApplication=true;
			
			boolean isApplicationDate=false;
			//判断当时时间是否大于竞价开始时间
			Date d=new Date();
			if(biddingBidList.getBiddingStartTime()!=null){
			if(d.getTime()<=biddingBidList.getBiddingStartTime().getTime()){
				isApplicationDate=true;
			}}	

			this.getRequest().setAttribute("isApplicationDate", isApplicationDate);
			this.getRequest().setAttribute("isApplication", isApplication);
		} catch (Exception e) {
			log("竞价项目报名初始化页面错误！", e);
			throw new BaseException("竞价项目报名初始化页面错误！", e);
		}
		return "saveInitBiddingBidListApplication";
	}
	/**
	 * 竞价项目供应商报名
	 * @return
	 * @throws BaseException
	 */
    public String saveBiddingBidListApplication() throws BaseException{
    	String from="";
    	try {
    		from=this.getRequest().getParameter("from");
    		SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(this.getRequest());
    		InviteSupplier inviteSupplier = new InviteSupplier();
    		inviteSupplier.setRcId(rcId);
			inviteSupplier.setSupplierId(supplierInfo.getSupplierId());
    		List<InviteSupplier> list=this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
    		if(list.size()==0){
			inviteSupplier.setSupplierName(supplierInfo.getSupplierName());
			inviteSupplier.setWriteDate(DateUtil.getCurrentDateTime());
			inviteSupplier.setWriter(supplierInfo.getSupplierLoginName());
			inviteSupplier.setSupplierEmail(supplierInfo.getContactEmail());
			inviteSupplier.setSupplierPhone(supplierInfo.getMobilePhone());
			inviteSupplier.setPriceNum( new Long(0));
			inviteSupplier.setSourceCategory(2);
			this.iInviteSupplierBiz.saveInviteSupplier(inviteSupplier); 
            
			this.iInviteSupplierBiz.updateInviteSupplierSourceCategory(inviteSupplier.getIsId(),inviteSupplier.getSupplierId(),rcId);
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
			Users user=BaseDataInfosUtil.convertLoginNameToUsers(biddingBidList.getWriter());
			String subject="邮件提醒："+supplierInfo.getSupplierName()+"报名"+requiredCollect.getBuyRemark()+"项目。",content=""+requiredCollect.getBuyRemark()+"("+requiredCollect.getBidCode()+")供应商报名，供应商名称："+supplierInfo.getSupplierName()+"";
			this.saveSendMailToUsersSupplier(user.getEmail(), subject, content, supplierInfo.getSupplierName());
			
			String messageContent=""+supplierInfo.getSupplierName()+"报名"+requiredCollect.getBuyRemark()+"项目。";
			this.saveSmsMessageToUsersSupplier(biddingBidList.getResponsiblePhone(), messageContent, supplierInfo.getSupplierName());
    		}
			if(StringUtil.isNotBlank(from)){
				PrintWriter out = this.getResponse().getWriter();
				out.print("success");
			}
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "供应商报名成功");
		} catch (Exception e) {
			log("竞价项目供应商报名错误！", e);
			throw new BaseException("竞价项目供应商报名错误！", e);
		}
		if(StringUtil.isNotBlank(from)) return null;
		else return saveInitBiddingBidListApplication();
    }
    /**
     * 进入我的竞价项目页面
     * @return
     * @throws BaseException
     */
    public String viewBiddingBidListMyMonitor() throws BaseException{
    	try {
    		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
		} catch (Exception e) {
			log("进入我的竞价项目页面错误！", e);
			throw new BaseException("进入我的竞价项目页面错误！", e);
		}
    	return "viewBiddingBidListMyMonitor";
    }    
    /**
	 * 项目基本信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBiddingBidListDetailMonitor() throws BaseException {
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			this.setListValue(this.iRequiredCollectBiz.getRequiredCollectDetailList(requiredCollect.getRcId()));
			
			biddingBidList = this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
			biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
			biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
			biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
			biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
			biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
			List<BiddingBidRound> trialBidRounds=null,bidRounds=null;
			if(biddingBidList.getBblId()!=null){//清单计划存在
				
				biddingBidRound=new BiddingBidRound();
				biddingBidRound.setRcId(rcId);
				biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_0);
				trialBidRounds=this.iBiddingBidRoundBiz.getBiddingBidRoundList(biddingBidRound);
				

				biddingBidRound=new BiddingBidRound();
				biddingBidRound.setRcId(rcId);
				biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_1);
				bidRounds=this.iBiddingBidRoundBiz.getBiddingBidRoundList(biddingBidRound);
				
			}

			this.getRequest().setAttribute("trialBidRounds", trialBidRounds);
			this.getRequest().setAttribute("bidRounds", bidRounds);
			
		} catch (Exception e) {
			log("项目基本信息初始化页面错误！", e);
			throw new BaseException("项目基本信息初始化页面错误！", e);
		}
		return "viewBiddingBidListDetail";
	}
	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}
	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}

	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}

	public BiddingBidList getBiddingBidList() {
		return biddingBidList;
	}

	public void setBiddingBidList(BiddingBidList biddingBidList) {
		this.biddingBidList = biddingBidList;
	}
	public IBiddingBidRoundBiz getiBiddingBidRoundBiz() {
		return iBiddingBidRoundBiz;
	}
	public void setiBiddingBidRoundBiz(IBiddingBidRoundBiz iBiddingBidRoundBiz) {
		this.iBiddingBidRoundBiz = iBiddingBidRoundBiz;
	}

 	
	
}
