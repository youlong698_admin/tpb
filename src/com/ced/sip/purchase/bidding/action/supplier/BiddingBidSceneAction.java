package com.ced.sip.purchase.bidding.action.supplier;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.biz.IBiddingBidRoundBiz;
import com.ced.sip.purchase.bidding.biz.IBiddingCommunicationInfoBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.entity.BiddingBidRound;
import com.ced.sip.purchase.bidding.entity.BiddingCommunicationInfo;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
import com.ced.sip.purchase.bidding.util.BiddingStatusMap;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseBiz;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidPriceHistoryBiz;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
import com.ced.sip.purchase.base.entity.BidBusinessResponseHistory;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
import com.ced.sip.purchase.base.entity.BidPriceDetailHistory;
import com.ced.sip.purchase.base.entity.BidPriceHistory;
import com.ced.sip.purchase.base.entity.BiddingBidPrice;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
import com.ced.sip.supplier.entity.SupplierInfo;
/** 
 * 类名称：BidPriceAction
 * 创建人：luguanglei 
 * 创建时间：2017-04-23
 */
public class BiddingBidSceneAction extends BaseAction {
	//采购计划明细
	private IRequiredCollectBiz iRequiredCollectBiz;
	//商务响应项
	private IBusinessResponseItemBiz iBusinessResponseItemBiz;
	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
    //竞价基本信息
	private IBiddingBidListBiz iBiddingBidListBiz;
	//竞价轮次信息
	private IBiddingBidRoundBiz iBiddingBidRoundBiz;
	//竞价交流信息
	private IBiddingCommunicationInfoBiz iBiddingCommunicationInfoBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	// 供应商报价明细信息 
	private IBidPriceDetailBiz iBidPriceDetailBiz;
	// 供应商历史报价信息 
	private IBidPriceHistoryBiz iBidPriceHistoryBiz;
	// 供应商历史报价明细信息 
	private IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz;
	// 供应商商务响应 
	private IBidBusinessResponseBiz iBidBusinessResponseBiz;
	// 供应商商务响应 
	private IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz;
		
	// 供应商报价信息
	private BidPrice bidPrice;
	// 供应商报价信息
	private BiddingBidPrice biddingBidPrice;
	// 供应商报价信息明细
	private BidPriceDetail bidPriceDetail;
	// 供应商报价历史信息
	private BidPriceHistory bidPriceHistory;
	// 供应商报价历史明细信息
	private BidPriceDetailHistory bidPriceDetailHistory;
	// 供应商报价信息
	private List<BidPriceDetail> bpdList;
	// 商务响应项信息
	private BidBusinessResponse bidBusinessResponse;
	// 商务响应项历史信息
	private BidBusinessResponseHistory bidBusinessResponseHistory;
	// 商务响应项信息
	private List<BidBusinessResponse> bbrList;
	
	private BiddingBidList biddingBidList;

	private BiddingCommunicationInfo biddingCommunicationInfo;

	private BiddingBidRound biddingBidRound;
	
	private List<BidCommunicationInfo> bciList;
	
	private RequiredCollectDetail requiredCollectDetail;
	
	private Long rcId;
	private Long bbrId;
	private Long bblId;
	private List<BidPrice> bpList;
	
	
	/**
	 * 竞价现场信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBiddingBidSceneMonitor() throws BaseException {
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			
			RequiredCollect requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
			biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
			biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
			biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
			biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
			
			String title="当前最低报价";
            if(BiddingStatus.BIDDING_TYPE_1.equals(biddingBidList.getBiddingType())) title="当前最高报价";
            
            biddingCommunicationInfo=new BiddingCommunicationInfo();
			biddingCommunicationInfo.setRcId(rcId);
			biddingCommunicationInfo.setPublish(supplierId);
			biddingCommunicationInfo.setReceive(supplierId);
			bciList=this.iBiddingCommunicationInfoBiz.getBiddingCommunicationInfoList(this.getRollPageDataTables(),biddingCommunicationInfo);
			this.getRequest().setAttribute("bciList", bciList);
			this.getRequest().setAttribute("biddingBidList", biddingBidList);
			this.getRequest().setAttribute("requiredCollect", requiredCollect);
			this.getRequest().setAttribute("supplierId", supplierId);
            this.getRequest().setAttribute("title", title);
            
		} catch (Exception e) {
			log.error("竞价现场信息列表错误！", e);
			throw new BaseException("竞价现场信息列表错误！", e);
		}
		
		return "viewBiddingBidScene" ;
		
	}
	/**
	 * 查询竞价详情信息
	 * @return
	 * @throws BaseException
	 */
	public String getBidMain() throws BaseException{
		try {
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			int num=Integer.parseInt(this.getRequest().getParameter("num"));
			String biddingType=this.getRequest().getParameter("biddingType");
			String priceMode=this.getRequest().getParameter("priceMode");
			if(num==0){
				biddingCommunicationInfo=new BiddingCommunicationInfo();
				biddingCommunicationInfo.setRcId(rcId);
				biddingCommunicationInfo.setPublish(supplierId);
				biddingCommunicationInfo.setReceive(supplierId);
				bciList=this.iBiddingCommunicationInfoBiz.getBiddingCommunicationInfoList(this.getRollPageDataTables(),biddingCommunicationInfo);
				this.getRequest().setAttribute("bciList", bciList);
			}else if(num==1){
				bidPriceHistory=new BidPriceHistory();
				bidPriceHistory.setRcId(rcId);
				bidPriceHistory.setSupplierId(supplierId);
				List list=this.iBidPriceHistoryBiz.getBidPriceHistoryList(bidPriceHistory);
				this.setListValue(list);
			}else if(num==2){
				bidPrice=new BidPrice();
				bidPrice.setRcId(rcId);
				bpList=new ArrayList<BidPrice>();
				List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);
				for(Object[] object:objectList){
					bidPrice=(BidPrice)object[0];
					bidPrice.setSupplierName((String)object[1]);
					bpList.add(bidPrice);
				}
				this.getRequest().setAttribute("bpList", bpList);	
				this.getRequest().setAttribute("priceMode", priceMode);				
			}else if(num==3){
				biddingBidPrice=this.iBidPriceBiz.getWinningBidPrice(rcId,biddingType);
				String title="当前最低报价";
	            if(BiddingStatus.BIDDING_TYPE_1.equals(biddingType)) title="当前最高报价";
	            this.getRequest().setAttribute("title", title);
				this.getRequest().setAttribute("price", biddingBidPrice.getTotalPrice());
			}
			this.getRequest().setAttribute("num", num);
		} catch (Exception e) {
			log.error("查询竞价详情信息错误！", e);
			throw new BaseException("查询竞价详情信息错误！", e);
		}
		return "bidMain";
	}
	/**
	 * 查询竞价项目所处的状态 返回json数据
	 * @return
	 * @throws BaseException
	 */
	public String viewBiddingBidListStrtus() throws BaseException{
		PrintWriter out = null;
		JSONObject jsonObject = new JSONObject();
		try{
			out = this.getResponse().getWriter();
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidList(bblId);
			//   是否延迟竞价  当前竞价是否在延迟竞价时间内  是否试竞价 
			int isDelay=biddingBidList.getIsDelay(),isDelayBid=biddingBidList.getIsDelayBid(),isTrialBid=biddingBidList.getIsTrialBid();
			
			//当前竞价轮次
			Long currBiddingRound=biddingBidList.getCurrentBidRound();
			if(currBiddingRound.equals(0L)) currBiddingRound=1L;
			biddingBidRound=this.iBiddingBidRoundBiz.getCurrentBiddingBidRound(rcId, currBiddingRound, isTrialBid);
			long currentTime=DateUtil.getCurrentDateTime().getTime();
			long endTime=0; //本轮竞价应该结束的时间
			if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_0){
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("isDelayBid", isDelayBid);				//当前竞价是否是延迟竞价
				jsonObject.put("isTrialBid", isTrialBid); //是否试竞价
			}else if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_1||biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_4){
					     Calendar c=Calendar.getInstance();
					     c.setTime(biddingBidRound.getAgainStartTime());
					     c.add(Calendar.MILLISECOND,biddingBidRound.getSurplusTime());
					     endTime=c.getTimeInMillis()-currentTime;
				Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
				biddingBidPrice=this.iBidPriceBiz.getWinningBidPrice(rcId,biddingBidList.getBiddingType());
                if(supplierId.equals(biddingBidPrice.getSupplierId())) jsonObject.put("flag", true);else jsonObject.put("flag", false);
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("isDelayBid", isDelayBid);				//当前竞价是否是延迟竞价
				jsonObject.put("isTrialBid", isTrialBid);//是否试竞价
				jsonObject.put("isDelay", isDelay);
				jsonObject.put("bbrId", biddingBidRound.getBbrId());
			}else if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_3){
				biddingBidPrice=this.iBidPriceBiz.getWinningBidPrice(rcId,biddingBidList.getBiddingType());
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("isDelayBid", isDelayBid);  //当前竞价是否延迟竞价
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("supplierName", biddingBidPrice.getSupplierName());//中标供应商
				jsonObject.put("isTrialBid", isTrialBid);	 //是否试竞价
				jsonObject.put("isBidEnd", biddingBidList.getIsBidEnd());
			}else if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_2){
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("surplusTime", biddingBidRound.getSurplusTime()/1000); //剩余竞价时间
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("isDelayBid", isDelayBid);  //当前竞价是否是延迟竞价
				jsonObject.put("isTrialBid", isTrialBid); //是否试竞价
				jsonObject.put("bbrId", biddingBidRound.getBbrId());				
			}
			jsonObject.put("info", "success");
			//System.out.println(jsonObject.toString());
			out.print(jsonObject.toString());
		} catch (Exception e) {
			log("查询竞价项目所处的状态 错误！", e);
			jsonObject.put("info", "error");
			out.print(jsonObject.toString());
		}
		return null;
	}
	/**
	 * 更新延迟竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListDelay() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			String delayTime=this.getRequest().getParameter("delayTime");
            this.iBiddingBidRoundBiz.updateBiddingRoudDelay(bbrId, rcId, delayTime);
            
            this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "更新延迟竞价信息错误");
            out.print("success");
		} catch (Exception e) {
			log("更新延迟竞价信息错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 结束竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListStop() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			biddingBidRound=this.iBiddingBidRoundBiz.getBiddingBidRound(bbrId);
			biddingBidRound.setEndTime(DateUtil.getCurrentDateTime());
            biddingBidRound.setStatus(BiddingStatus.BIDDING_STATUS_3);
            this.iBiddingBidRoundBiz.updateBiddingBidRound(biddingBidRound);
            
            int count=this.iBiddingBidRoundBiz.getAllStopBid(rcId);
            if(count==0){
            	biddingBidList=this.iBiddingBidListBiz.getBiddingBidList(bblId);
            	biddingBidList.setIsBidEnd(0);
            	this.iBiddingBidListBiz.updateBiddingBidList(biddingBidList);
            	
    			this.getRequest().setAttribute("message","操作成功");
    			this.getRequest().setAttribute("operModule", "竞价结束下一步成功");
            }
            out.print("success");
		} catch (Exception e) {
			log("开始价信息错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 保存竞价交流信息
	 * @return
	 * @throws BaseException
	 */
	public String saveBiddingCommunication() throws BaseException{
		PrintWriter out = null;
		try{
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());
			String supplierName=UserRightInfoUtil.getSupplierName(this.getRequest());
			out = this.getResponse().getWriter();
			biddingCommunicationInfo=new BiddingCommunicationInfo();
			String info=this.getRequest().getParameter("info");
			biddingCommunicationInfo.setPublish(supplierId);
			biddingCommunicationInfo.setPublisher(supplierName);
			biddingCommunicationInfo.setRcId(rcId);
			biddingCommunicationInfo.setReceive(null);
			biddingCommunicationInfo.setReceiver(null);
			biddingCommunicationInfo.setPublishDate(DateUtil.getCurrentDateTime());
			biddingCommunicationInfo.setInfo(info);
			biddingCommunicationInfo.setType(BiddingStatus.COMMUNICATION_INFO_0);
			this.iBiddingCommunicationInfoBiz.saveBiddingCommunicationInfo(biddingCommunicationInfo);
    		this.getRequest().setAttribute("message","操作成功");
    		this.getRequest().setAttribute("operModule", "交流信息保存成功");
            out.print("success");
		} catch (Exception e) {
			log("交流信息保存成功错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 保存供应商报价信息信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String saveInitBiddingBidPriceRespone() throws BaseException {
		  String returnPage=""; //这里区分第一次报价和第二次以后的报价，因为读取的信息不一样
		try{
		  Long supplierId=UserRightInfoUtil.getSupplierId(getRequest());
		  
		  String error="";
		  
		  biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
          biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		  biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
		  
		  Long currentBidRound=biddingBidList.getCurrentBidRound();
		  biddingBidRound=new BiddingBidRound();
		  biddingBidRound.setBiddingRound(currentBidRound.intValue());
		  biddingBidRound.setRcId(rcId);
		  biddingBidRound.setBidType(biddingBidList.getIsTrialBid());
		  biddingBidRound=this.iBiddingBidRoundBiz.getBiddingBidRoundByBiddingBidRound(biddingBidRound);
		  
		  if(biddingBidRound.getPriceCount()!=null){
		  bidPriceHistory=new BidPriceHistory();
		  bidPriceHistory.setRcId(rcId);
		  bidPriceHistory.setSupplierId(supplierId);
		  bidPriceHistory.setRemark(currentBidRound+"");
		  bidPriceHistory.setRemark1(biddingBidList.getIsTrialBid()+"");
		  List<BidPriceHistory> list=this.iBidPriceHistoryBiz.getBidPriceHistoryList(bidPriceHistory);
		  if(list.size()==biddingBidRound.getPriceCount().intValue()){
			  error="很遗憾，您已经达到本轮竞价设定的报价次数，不能再次报价";
		      this.getRequest().setAttribute("error", error);
		  }else{
			   this.getRequest().setAttribute("price", (biddingBidRound.getPriceCount().intValue()-list.size())); //剩余报价次数
		  }
		    returnPage="addInitOne";
		  }
		  if(error.equals("")){
			  //查询上次的报价信息
			  bidPrice=new BidPrice();
			  bidPrice.setRcId(rcId);
			  bidPrice.setSupplierId(supplierId);
			  bidPrice=this.iBidPriceBiz.getBidPriceListByBidPrice(bidPrice);
			  //还没有报价
			  if(bidPrice.getBpId()==null){				  
				  List<RequiredCollectDetail> rcdlist=this.iRequiredCollectBiz.getRequiredCollectDetailList(rcId);
				  BusinessResponseItem businessResponseItem=new BusinessResponseItem();
				  businessResponseItem.setRcId(rcId);
				  List<BusinessResponseItem> briList=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);

				  this.getRequest().setAttribute("rcdlist", rcdlist);
				  this.getRequest().setAttribute("briList", briList);

				  returnPage="addInitOne";
			  }else{
				  List<BidPriceDetail> bpdList=new ArrayList<BidPriceDetail>();
				  bidPriceDetail=new BidPriceDetail();
				  bidPriceDetail.setBpId(bidPrice.getBpId());
				  List<Object[]> objectList=this.iBidPriceDetailBiz.getBidPriceDetailListRequiredCollectDetail(bidPriceDetail);
				  for(Object[] object:objectList){
					bidPriceDetail=(BidPriceDetail)object[0];
					requiredCollectDetail=(RequiredCollectDetail)object[1];
					bidPriceDetail.setRequiredCollectDetail(requiredCollectDetail);
					bpdList.add(bidPriceDetail);
				 }
					
				  bidBusinessResponse=new BidBusinessResponse();
				  bidBusinessResponse.setBpId(bidPrice.getBpId());
				  List<BidBusinessResponse> bbrList=this.iBidBusinessResponseBiz.getBidBusinessResponseList(bidBusinessResponse);

				  this.getRequest().setAttribute("bpdList", bpdList);
				  this.getRequest().setAttribute("bbrList", bbrList);

				  returnPage="addInitTwo";
			   }	
			  this.getRequest().setAttribute("supplierId", supplierId);
			  this.getRequest().setAttribute("biddingBidList", biddingBidList);
			  this.getRequest().setAttribute("bbrId", biddingBidRound.getBbrId());
		  }
		} catch (Exception e) {
			log("保存供应商报价信息信息初始化错误！", e);
			throw new BaseException("保存供应商报价信息信息初始化错误！", e);
		}
		return returnPage;
		
	}
	/**
	 * 保存供应商报价信息信息
	 * @return
	 * @throws BaseException 
	 */
	public String saveBiddingBidPriceRespone() throws BaseException {
		
		try{
			
			Long bbrId=Long.parseLong(this.getRequest().getParameter("bbrId"));
			biddingBidRound=this.iBiddingBidRoundBiz.getBiddingBidRound(bbrId);
			
			if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_1||biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_4){
			SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(getRequest()) ;
			Date date=DateUtil.getCurrentDateTime();	
			//更新邀请供应商表中的是否报价字段
			this.iInviteSupplierBiz.updateIsPriceAaByRcIdAndSupplierId(rcId, supplierInfo.getSupplierId());
			
			
			//删除旧的报价信息
			this.iBidPriceBiz.deleteBidPriceByRcIdAndSupplierId(bidPrice.getRcId(),supplierInfo.getSupplierId());
			
			//保存新的报价信息
			bidPrice.setWriter(supplierInfo.getSupplierLoginName());
			bidPrice.setWriteDate(date);
			this.iBidPriceBiz.saveBidPrice(bidPrice);
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetail.setBpId(bidPrice.getBpId());
					bidPriceDetail.setRcId(bidPrice.getRcId());
					this.iBidPriceDetailBiz.saveBidPriceDetail(bidPriceDetail);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponse.setBpId(bidPrice.getBpId());
					this.iBidBusinessResponseBiz.saveBidBusinessResponse(bidBusinessResponse);
				}
			}
			
			//保存新的报价历史信息
			bidPriceHistory=new BidPriceHistory();
			BeanUtils.copyProperties(bidPriceHistory, bidPrice);
			bidPriceHistory.setTotalPrice(bidPrice.getTotalPrice());
			this.iBidPriceHistoryBiz.saveBidPriceHistory(bidPriceHistory);
			
			// 保存报价明细
			if (bpdList != null) {
				for (int i = 0; i < bpdList.size(); i++) {
					bidPriceDetail = bpdList.get(i);
					bidPriceDetailHistory=new BidPriceDetailHistory();
					BeanUtils.copyProperties(bidPriceDetailHistory, bidPriceDetail);
					bidPriceDetailHistory.setPrice(bidPriceDetail.getPrice());
					bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
					bidPriceDetailHistory.setRcId(bidPriceHistory.getRcId());
					this.iBidPriceDetailHistoryBiz.saveBidPriceDetailHistory(bidPriceDetailHistory);
				}
			}
			// 保存商务响应项明细
			if (bbrList != null) {
				for (int i = 0; i < bbrList.size(); i++) {
					bidBusinessResponse = bbrList.get(i);
					bidBusinessResponseHistory=new BidBusinessResponseHistory();
					BeanUtils.copyProperties(bidBusinessResponseHistory, bidBusinessResponse);
					bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
					this.iBidBusinessResponseHistoryBiz.saveBidBusinessResponseHistory(bidBusinessResponseHistory);
				}
			}
			  this.getRequest().setAttribute("message", "报价成功");
			  this.getRequest().setAttribute("operModule", "供应商报价信息");
			}else{
              this.getRequest().setAttribute("message", "很遗憾，已经超过允许报价的时间！！");
  			  this.getRequest().setAttribute("operModule", "供应商报价信息");
			}
		} catch (Exception e) {
			log("保存供应商报价信息信息错误！", e);
			throw new BaseException("保存供应商报价信息信息错误！", e);
		}
		
		return SUCCESS;
		
	}
	
	/**
	 * 查看供应商报价信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBiddingBidPriceResponeDetail() throws BaseException {
		
		try{

		      
			bidPriceHistory=this.iBidPriceHistoryBiz.getBidPriceHistory(bidPriceHistory.getBphId() );
			
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(bidPriceHistory.getRcId());
            biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		    biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
		    List<BidPriceDetailHistory> bpdhList=new ArrayList<BidPriceDetailHistory>();
			bidPriceDetailHistory=new BidPriceDetailHistory();
			bidPriceDetailHistory.setBphId(bidPriceHistory.getBphId());
			List<Object[]> objectList=this.iBidPriceDetailHistoryBiz.getBidPriceDetailHistoryListSupplier(bidPriceDetailHistory);
			for(Object[] object:objectList){
				bidPriceDetailHistory=(BidPriceDetailHistory)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				bidPriceDetailHistory.setRequiredCollectDetail(requiredCollectDetail);
				bpdhList.add(bidPriceDetailHistory);
			}
				
				
			bidBusinessResponseHistory=new BidBusinessResponseHistory();
			bidBusinessResponseHistory.setBphId(bidPriceHistory.getBphId());
			List<BidBusinessResponseHistory> bbrhList=this.iBidBusinessResponseHistoryBiz.getBidBusinessResponseHistoryList(bidBusinessResponseHistory);
			
			this.getRequest().setAttribute("bidPriceHistory", bidPriceHistory);			
			this.getRequest().setAttribute("bpdhList", bpdhList);			
			this.getRequest().setAttribute("bbrhList", bbrhList);			
			this.getRequest().setAttribute("biddingBidList", biddingBidList);	
		} catch (Exception e) {
			log("查看供应商报价信息明细信息错误！", e);
			throw new BaseException("查看供应商报价信息明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}

	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}

	public BidPrice getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(BidPrice bidPrice) {
		this.bidPrice = bidPrice;
	}

	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}

	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}

	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}

	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}

	public IBidPriceHistoryBiz getiBidPriceHistoryBiz() {
		return iBidPriceHistoryBiz;
	}

	public void setiBidPriceHistoryBiz(IBidPriceHistoryBiz iBidPriceHistoryBiz) {
		this.iBidPriceHistoryBiz = iBidPriceHistoryBiz;
	}

	public IBidPriceDetailHistoryBiz getiBidPriceDetailHistoryBiz() {
		return iBidPriceDetailHistoryBiz;
	}

	public void setiBidPriceDetailHistoryBiz(
			IBidPriceDetailHistoryBiz iBidPriceDetailHistoryBiz) {
		this.iBidPriceDetailHistoryBiz = iBidPriceDetailHistoryBiz;
	}

	public IBidBusinessResponseBiz getiBidBusinessResponseBiz() {
		return iBidBusinessResponseBiz;
	}

	public void setiBidBusinessResponseBiz(
			IBidBusinessResponseBiz iBidBusinessResponseBiz) {
		this.iBidBusinessResponseBiz = iBidBusinessResponseBiz;
	}

	public List<BidPriceDetail> getBpdList() {
		return bpdList;
	}

	public void setBpdList(List<BidPriceDetail> bpdList) {
		this.bpdList = bpdList;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IBusinessResponseItemBiz getiBusinessResponseItemBiz() {
		return iBusinessResponseItemBiz;
	}

	public void setiBusinessResponseItemBiz(
			IBusinessResponseItemBiz iBusinessResponseItemBiz) {
		this.iBusinessResponseItemBiz = iBusinessResponseItemBiz;
	}

	public List<BidBusinessResponse> getBbrList() {
		return bbrList;
	}

	public void setBbrList(List<BidBusinessResponse> bbrList) {
		this.bbrList = bbrList;
	}

	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}

	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IBidBusinessResponseHistoryBiz getiBidBusinessResponseHistoryBiz() {
		return iBidBusinessResponseHistoryBiz;
	}

	public void setiBidBusinessResponseHistoryBiz(
			IBidBusinessResponseHistoryBiz iBidBusinessResponseHistoryBiz) {
		this.iBidBusinessResponseHistoryBiz = iBidBusinessResponseHistoryBiz;
	}

	public IBiddingBidRoundBiz getiBiddingBidRoundBiz() {
		return iBiddingBidRoundBiz;
	}

	public void setiBiddingBidRoundBiz(IBiddingBidRoundBiz iBiddingBidRoundBiz) {
		this.iBiddingBidRoundBiz = iBiddingBidRoundBiz;
	}

	public IBiddingCommunicationInfoBiz getiBiddingCommunicationInfoBiz() {
		return iBiddingCommunicationInfoBiz;
	}

	public void setiBiddingCommunicationInfoBiz(
			IBiddingCommunicationInfoBiz iBiddingCommunicationInfoBiz) {
		this.iBiddingCommunicationInfoBiz = iBiddingCommunicationInfoBiz;
	}
	public Long getBbrId() {
		return bbrId;
	}
	public void setBbrId(Long bbrId) {
		this.bbrId = bbrId;
	}
	public Long getBblId() {
		return bblId;
	}
	public void setBblId(Long bblId) {
		this.bblId = bblId;
	}
	public BidPriceHistory getBidPriceHistory() {
		return bidPriceHistory;
	}
	public void setBidPriceHistory(BidPriceHistory bidPriceHistory) {
		this.bidPriceHistory = bidPriceHistory;
	}
	
}
