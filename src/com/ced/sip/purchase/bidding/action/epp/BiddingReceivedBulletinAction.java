package com.ced.sip.purchase.bidding.action.epp;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
/** 
 * 类名称：BiddingReceivedBulletinAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class BiddingReceivedBulletinAction extends BaseAction {

	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
	
	 
	 private Long rcId;
     private String isDetail;
     
     private RequiredCollect requiredCollect;
     private InviteSupplier inviteSupplier;
	
	
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 竞价响竞价段监控
	 * @return
	 * @throws BaseException 
	 */
	public String viewBiddingReceivedBulletinMonitor() throws BaseException {
		
		String view="biddingReceivedBulletinMonitorDetail";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			//if(requiredCollect.getSupplierType().equals(TableStatus.SUPPLIER_TYPE_00)){
				inviteSupplier=new InviteSupplier();
				inviteSupplier.setRcId(rcId);
				List<InviteSupplier> inviteSupplierList=this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
				this.getRequest().setAttribute("inviteSupplierList", inviteSupplierList);
			//}
			
			this.getRequest().setAttribute("supplierType", requiredCollect.getSupplierType());
			
			if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.OPEN_STATUS_01.equals(requiredCollect.getOpenStatus())&&StringUtil.isBlank(isDetail))
			{   
            	view="biddingReceivedBulletinMonitorUpdate";
			}
            
		} catch (Exception e) {
			log("竞价响竞价段监控初始化错误！", e);
			throw new BaseException("竞价响竞价段监控初始化错误！", e);
		}
		return view;
		
	}
	/**
	 * 获取应标响应数
	 * @return
	 * @throws BaseException
	 */
	public String getBiddingReceiveBulletinForNode() throws BaseException{
		int count=0;
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
            inviteSupplier=new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			count=this.iInviteSupplierBiz.countInviteSupplierList(inviteSupplier);
			
			out.print(count);
		} catch (Exception e) {
			log("获取应标响应厂家数！", e);
			throw new BaseException("获取应标响应厂家数！", e);
		}
		return null;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	
}
