package com.ced.sip.purchase.bidding.action.epp;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;



import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.RandomColor;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.biz.IBiddingBidRoundBiz;
import com.ced.sip.purchase.bidding.biz.IBiddingCommunicationInfoBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.entity.BiddingBidRound;
import com.ced.sip.purchase.bidding.entity.BiddingCommunicationInfo;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
import com.ced.sip.purchase.bidding.util.BiddingStatusMap;
import com.ced.sip.purchase.base.biz.IBidBusinessResponseBiz;
import com.ced.sip.purchase.base.biz.IBidPriceBiz;
import com.ced.sip.purchase.base.biz.IBidPriceDetailBiz;
import com.ced.sip.purchase.base.biz.IBidPriceHistoryBiz;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBusinessResponse;
import com.ced.sip.purchase.base.entity.BidCommunicationInfo;
import com.ced.sip.purchase.base.entity.BidPrice;
import com.ced.sip.purchase.base.entity.BidPriceDetail;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.BiddingBidPrice;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.purchase.base.entity.RequiredCollectDetail;
/** 
 * 类名称：BidPriceAction
 * 创建人：luguanglei 
 * 创建时间：2017-05-17
 */
public class BiddingBidSceneAction extends BaseAction {
    //项目信息服务类
    private IRequiredCollectBiz iRequiredCollectBiz;
	//邀请供应商
	private IInviteSupplierBiz iInviteSupplierBiz;
    //竞价基本信息
	private IBiddingBidListBiz iBiddingBidListBiz;
	//竞价轮次信息
	private IBiddingBidRoundBiz iBiddingBidRoundBiz;
	//竞价交流信息
	private IBiddingCommunicationInfoBiz iBiddingCommunicationInfoBiz;
	// 供应商报价信息 
	private IBidPriceBiz iBidPriceBiz;
	// 供应商报价明细信息 
	private IBidPriceDetailBiz iBidPriceDetailBiz;
	// 供应商历史报价信息 
	private IBidPriceHistoryBiz iBidPriceHistoryBiz;
	// 供应商商务响应 
	private IBidBusinessResponseBiz iBidBusinessResponseBiz;
	 //项目日志记录服务类
	 private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	 //标段流程记录实例表
	 private IBidProcessLogBiz iBidProcessLogBiz;
	 
		
	// 供应商报价信息
	private BidPrice bidPrice;
	
	private BiddingBidPrice biddingBidPrice;
	// 供应商报价信息明细
	private BidPriceDetail bidPriceDetail;
	// 商务响应项信息
	private BidBusinessResponse bidBusinessResponse;
	
	
	private BiddingBidList biddingBidList;
	
	private BiddingBidRound biddingBidRound;
	
	private BiddingCommunicationInfo biddingCommunicationInfo;
	
	private List<BidCommunicationInfo> bciList;
	
	private List bphList;
	
	private List<BidPrice> bpList;
	
	private RequiredCollectDetail requiredCollectDetail;
	
	private Long rcId;
	private Long bbrId;
	private Long bblId;
    private String isDetail;

    private RequiredCollect requiredCollect;
    private PurchaseRecordLog purchaseRecordLog;
 	private BidProcessLog bidProcessLog;
 	
	/**
 	* 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	
	/**
	 *竞价现场信息列表项目监控
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewBiddingBidSceneMonitor() throws BaseException {
		String view="biddingBidSceneMonitorDetail";
		try{
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			//if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
			biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
			biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
			biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
			biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
			

            String title="当前最低报价";
            if(BiddingStatus.BIDDING_TYPE_1.equals(biddingBidList.getBiddingType())) title="当前最高报价";
            
			InviteSupplier inviteSupplier=new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			List<InviteSupplier> inviteSupplierList=this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
			
			this.getRequest().setAttribute("inviteSupplierList", inviteSupplierList);
			
			Map ssxxMap=BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1705);
			this.getRequest().setAttribute("ssxxMap", ssxxMap);
			
			//公告未发布 且项目状态为正常 且非查看页面
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&UserRightInfoUtil.getUserId(this.getRequest()).equals(biddingBidList.getBidAdmin())&&BiddingProgressStatus.Progress_Status_25==requiredCollect.getServiceStatus())
			{  
    	        view="biddingBidSceneMonitorUpdate";
			}else{
				if(biddingBidList.getIsBidEnd()==1){
					this.getRequest().setAttribute("info", "很遗憾，在竞价结束之前，只有竞价管理员有权限操作竞价信息");
				}else{
					biddingBidPrice=this.iBidPriceBiz.getWinningBidPrice(rcId,biddingBidList.getBiddingType());
					this.getRequest().setAttribute("supplierName", biddingBidPrice.getSupplierName());
					this.getRequest().setAttribute("price", biddingBidPrice.getTotalPrice());
				}
			}
            biddingCommunicationInfo=new BiddingCommunicationInfo();
			biddingCommunicationInfo.setRcId(rcId);
			biddingCommunicationInfo.setPublish(UserRightInfoUtil.getUserId(this.getRequest()));
			bciList=this.iBiddingCommunicationInfoBiz.getBiddingCommunicationInfoList(this.getRollPageDataTables(),biddingCommunicationInfo);
			this.getRequest().setAttribute("bciList", bciList);
			
            this.getRequest().setAttribute("title", title);
		   } catch (Exception e) {
			log.error("竞价现场信息列表错误！", e);
			throw new BaseException("竞价现场信息列表错误！", e);
		}		
		return view;		
	}
	/**
	 * 修改竞价信息执行下一步信息
	 * @return
	 * @throws BaseException 
	 */
	public String updateBiddingBidSceneMonitor() throws BaseException {
		String view="success";
		String text="",operateContent="";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);

			operateContent="竞价结束，执行下一步";
			text=BiddingProgressStatus.Progress_Status_26_Text;
			updateBidMonitorAndBidProcessLog(rcId, BiddingProgressStatus.Progress_Status_25, BiddingProgressStatus.Progress_Status_26, BiddingProgressStatus.Progress_Status_26_Text);
				
			//保存流程跟踪信息
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setRcId(rcId);
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog.setBidNode(text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", text+"成功");
		} catch (Exception e) {
			log("竞价结束，执行下一步错误！", e);
			throw new BaseException("竞价结束，执行下一步错误！", e);
		}
		return view;
		
	}
	/**
	 * 查询竞价详情信息
	 * @return
	 * @throws BaseException
	 */
	public String getBidMain() throws BaseException{
		try {
			int num=Integer.parseInt(this.getRequest().getParameter("num"));
			String biddingType=this.getRequest().getParameter("biddingType");
			if(num==0){
				biddingCommunicationInfo=new BiddingCommunicationInfo();
				biddingCommunicationInfo.setRcId(rcId);
				biddingCommunicationInfo.setPublish(UserRightInfoUtil.getUserId(this.getRequest()));
				bciList=this.iBiddingCommunicationInfoBiz.getBiddingCommunicationInfoList(this.getRollPageDataTables(),biddingCommunicationInfo);
				this.getRequest().setAttribute("bciList", bciList);
			}else if(num==1){
				bidPrice=new BidPrice();
				bidPrice.setRcId(rcId);
				bpList=new ArrayList<BidPrice>();
				List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);
				for(Object[] object:objectList){
					bidPrice=(BidPrice)object[0];
					bidPrice.setSupplierName((String)object[1]);
					bpList.add(bidPrice);
				}
				this.getRequest().setAttribute("bpList", bpList);				
			}else if(num==2){
				biddingBidPrice=this.iBidPriceBiz.getWinningBidPrice(rcId,biddingType);
				String title="当前最低报价";
	            if(BiddingStatus.BIDDING_TYPE_1.equals(biddingType)) title="当前最高报价";
	            this.getRequest().setAttribute("title", title);
				this.getRequest().setAttribute("price", biddingBidPrice.getTotalPrice());
			}else if(num==3){
				bphList=this.iBidPriceHistoryBiz.getBidPriceGroupByBiddingRound(rcId,biddingType);
				this.getRequest().setAttribute("bphList", bphList);
			}else if(num==4){
				bphList=this.iBidPriceHistoryBiz.getBidPriceHistoryByBddingChart(rcId);
				biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
				Date biddingRealtiume=biddingBidList.getBiddingRealTime();
				String supplierNames="", data="";
				if(biddingRealtiume!=null){
				Long supplierId;
				Set<Long> set=new HashSet<Long>();
				int minBetween=0;
				Object[] obj=null;
				for(int i=0;i<bphList.size();i++){
					obj=(Object[])bphList.get(i);
					supplierId=(Long)obj[2];
					minBetween=DateUtil.minBetween(biddingRealtiume, (Date)obj[3]);
					if(set.add(supplierId)){
						if(!supplierNames.equals("")) data+="]},";
						supplierNames+="'"+(String)obj[1]+"',";
						data+="{ name:'"+(String)obj[1]+"',type:'line',data:[["+minBetween+","+obj[0]+"]";
					}else{
						data+=",["+minBetween+","+obj[0]+"] ";
					}
				 }
				if(!supplierNames.equals("")){
					data+="]}";
					supplierNames=supplierNames.substring(0, supplierNames.length()-1);
				}
				}
				this.getRequest().setAttribute("supplierNames", supplierNames);
				this.getRequest().setAttribute("data", data);
			}
			this.getRequest().setAttribute("num", num);
		} catch (Exception e) {
			log.error("查询竞价详情信息错误！", e);
			throw new BaseException("查询竞价详情信息错误！", e);
		}
		return "bidMain";
	}
	/**
	 * 查询竞价项目所处的状态 返回json数据
	 * @return
	 * @throws BaseException
	 */
	public String viewBiddingBidListStrtus() throws BaseException{
		PrintWriter out = null;
		JSONObject jsonObject = new JSONObject();
		try{
			out = this.getResponse().getWriter();
			
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidList(bblId);
			//   是否延迟竞价  当前竞价是否在延迟竞价时间内  是否试竞价 
			int isDelay=biddingBidList.getIsDelay(),isDelayBid=biddingBidList.getIsDelayBid(),isTrialBid=biddingBidList.getIsTrialBid();
			
			//当前竞价轮次
			Long currBiddingRound=biddingBidList.getCurrentBidRound();
			if(currBiddingRound.equals(0L)) currBiddingRound=1L;
			biddingBidRound=this.iBiddingBidRoundBiz.getCurrentBiddingBidRound(rcId, currBiddingRound, isTrialBid);
			long currentTime=DateUtil.getCurrentDateTime().getTime();
			long endTime=0; //本轮竞价应该结束的时间
			if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_0){
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("isDelayBid", isDelayBid);				//当前竞价是否是延迟竞价
				jsonObject.put("isTrialBid", isTrialBid); //是否试竞价
			}else if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_1||biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_4){
					     Calendar c=Calendar.getInstance();
					     c.setTime(biddingBidRound.getAgainStartTime());
					     c.add(Calendar.MILLISECOND,biddingBidRound.getSurplusTime());
					     endTime=c.getTimeInMillis()-currentTime;
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("isDelayBid", isDelayBid);				//当前竞价是否是延迟竞价
				jsonObject.put("isTrialBid", isTrialBid);//是否试竞价
				jsonObject.put("isDelay", isDelay);
				jsonObject.put("bbrId", biddingBidRound.getBbrId());
			}else if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_3){
				biddingBidPrice=this.iBidPriceBiz.getWinningBidPrice(rcId,biddingBidList.getBiddingType());
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("isDelayBid", isDelayBid);  //当前竞价是否延迟竞价
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("supplierName", biddingBidPrice.getSupplierName());//中标供应商
				jsonObject.put("price", biddingBidPrice.getTotalPrice()); //中标价
				jsonObject.put("isTrialBid", isTrialBid);	 //是否试竞价
				jsonObject.put("isBidEnd", biddingBidList.getIsBidEnd());
			}else if(biddingBidRound.getStatus()==BiddingStatus.BIDDING_STATUS_2){
				jsonObject.put("status", biddingBidRound.getStatus());
				jsonObject.put("endTime", endTime);
				jsonObject.put("timeLong", biddingBidRound.getTimeLong());
				jsonObject.put("surplusTime", biddingBidRound.getSurplusTime()/1000); //剩余竞价时间
				jsonObject.put("currBiddingRound", currBiddingRound);
				jsonObject.put("isDelayBid", isDelayBid);  //当前竞价是否是延迟竞价
				jsonObject.put("isTrialBid", isTrialBid); //是否试竞价
				jsonObject.put("bbrId", biddingBidRound.getBbrId());				
			}
			jsonObject.put("info", "success");
			//System.out.println(jsonObject.toString());
			out.print(jsonObject.toString());
		} catch (Exception e) {
			log("查询竞价项目所处的状态 错误！", e);
			jsonObject.put("info", "error");
			out.print(jsonObject.toString());
			throw new BaseException("查询竞价详情信息错误！", e);
		}
		return null;
	}
	/**
	 * 更新延迟竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListDelay() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			String delayTime=this.getRequest().getParameter("delayTime");
            this.iBiddingBidRoundBiz.updateBiddingRoudDelay(bbrId, rcId, delayTime);
            
            this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "更新延迟竞价信息错误");
            out.print("success");
		} catch (Exception e) {
			log("更新延迟竞价信息错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 开始竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListStart() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			int sign=Integer.parseInt(this.getRequest().getParameter("sign"));
			int minBiddingRound=this.iBiddingBidRoundBiz.getMinBiddingRound(rcId, sign);
            this.iBiddingBidRoundBiz.updateBiddingRoudStart(bblId, rcId, sign,minBiddingRound);

            this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "开始竞价信息错误");
			
            out.print("success");
		} catch (Exception e) {
			log("开始竞价信息错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 暂停竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListBreak() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
            biddingBidRound=this.iBiddingBidRoundBiz.getBiddingBidRound(bbrId);
            long sysj = 0;

			/** 第一次暂停 */
			if (biddingBidRound.getSurplusTime() == biddingBidRound.getTimeLong()) {
				sysj = biddingBidRound.getTimeLong()* 1000
						- (DateUtil.getCurrentDateTime().getTime() - biddingBidRound.getAgainStartTime().getTime());
			} else {
				sysj = biddingBidRound.getSurplusTime()
						- (DateUtil.getCurrentDateTime().getTime() - biddingBidRound.getAgainStartTime().getTime());
			}
			long ztsj = biddingBidRound.getTimeLong() * 1000 - sysj;

			biddingBidRound.setSuspendTime(Integer.parseInt(ztsj + ""));

			biddingBidRound.setSurplusTime(Integer.parseInt(sysj + ""));
			
			biddingBidRound.setStatus(BiddingStatus.BIDDING_STATUS_2); // 2暂停
			
            this.iBiddingBidRoundBiz.updateBiddingBidRound(biddingBidRound);

            this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "暂停竞价信息错误");
            out.print("success");
		} catch (Exception e) {
			log("暂停竞价信息错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 重新开始竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListReStart() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			biddingBidRound=this.iBiddingBidRoundBiz.getBiddingBidRound(bbrId);
			
			biddingBidRound.setAgainStartTime(DateUtil.getCurrentDateTime());
			biddingBidRound.setStatus(BiddingStatus.BIDDING_STATUS_4); //暂停后重新开始
			//一下+1000毫秒是为了解决暂停之后重新开始，倒计时时间少一秒问题
			biddingBidRound.setSuspendTime(biddingBidRound.getSuspendTime()-1000);
			biddingBidRound.setSurplusTime(biddingBidRound.getSurplusTime()+1000);
			
            this.iBiddingBidRoundBiz.updateBiddingBidRound(biddingBidRound);
            
            this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "重新开始竞价信息");
            out.print("success");
		} catch (Exception e) {
			log("重新开始竞价信息错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 结束竞价信息
	 * @return
	 * @throws BaseException
	 */
	public String updateBiddingBidListStop() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			biddingBidRound=this.iBiddingBidRoundBiz.getBiddingBidRound(bbrId);
			biddingBidRound.setEndTime(DateUtil.getCurrentDateTime());
            biddingBidRound.setStatus(BiddingStatus.BIDDING_STATUS_3);
            this.iBiddingBidRoundBiz.updateBiddingBidRound(biddingBidRound);
            
            int count=this.iBiddingBidRoundBiz.getAllStopBid(rcId);
            if(count==0){
            	biddingBidList=this.iBiddingBidListBiz.getBiddingBidList(bblId);
            	biddingBidList.setIsBidEnd(0);
            	this.iBiddingBidListBiz.updateBiddingBidList(biddingBidList);
            	
            	this.getRequest().setAttribute("message","操作成功");
    			this.getRequest().setAttribute("operModule", "竞价结束");
            }
            out.print("success");
		} catch (Exception e) {
			log("结束竞价错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 保存竞价交流信息
	 * @return
	 * @throws BaseException
	 */
	public String saveBiddingCommunication() throws BaseException{
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			biddingCommunicationInfo=new BiddingCommunicationInfo();
			long receive=Long.parseLong(this.getRequest().getParameter("receive"));
			String receiver=this.getRequest().getParameter("receiver");
			String info=this.getRequest().getParameter("info");
			biddingCommunicationInfo.setPublish(UserRightInfoUtil.getUserId(this.getRequest()));
			biddingCommunicationInfo.setPublisher(UserRightInfoUtil.getChineseName(this.getRequest()));
			biddingCommunicationInfo.setRcId(rcId);
			biddingCommunicationInfo.setReceive(receive);
			biddingCommunicationInfo.setReceiver(receiver);
			biddingCommunicationInfo.setPublishDate(DateUtil.getCurrentDateTime());
			biddingCommunicationInfo.setInfo(info);
			biddingCommunicationInfo.setType(BiddingStatus.COMMUNICATION_INFO_0);
			this.iBiddingCommunicationInfoBiz.saveBiddingCommunicationInfo(biddingCommunicationInfo);
    		this.getRequest().setAttribute("message","操作成功");
    		this.getRequest().setAttribute("operModule", "交流信息保存成功");
            out.print("success");
		} catch (Exception e) {
			log("交流信息保存成功错误！", e);
			out.print("");
		}
		return null;
	}
	/**
	 * 查看供应商报价信息明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String viewBiddingBidPriceResponeDetail() throws BaseException {
		
		try{		      			
            bidPrice=this.iBidPriceBiz.getBidPrice(bidPrice.getBpId() );
			
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(bidPrice.getRcId());
            biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		    biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
		    List<BidPriceDetail> bpdList=new ArrayList<BidPriceDetail>();
			bidPriceDetail=new BidPriceDetail();
			bidPriceDetail.setBpId(bidPrice.getBpId());
			List<Object[]> objectList=this.iBidPriceDetailBiz.getBidPriceDetailListRequiredCollectDetail(bidPriceDetail);
			for(Object[] object:objectList){
				bidPriceDetail=(BidPriceDetail)object[0];
				requiredCollectDetail=(RequiredCollectDetail)object[1];
				bidPriceDetail.setRequiredCollectDetail(requiredCollectDetail);
				bpdList.add(bidPriceDetail);
			}
				
				
			bidBusinessResponse=new BidBusinessResponse();
			bidBusinessResponse.setBpId(bidPrice.getBpId());
			List<BidBusinessResponse> bbrList=this.iBidBusinessResponseBiz.getBidBusinessResponseList(bidBusinessResponse);
			
			this.getRequest().setAttribute("bidPrice", bidPrice);			
			this.getRequest().setAttribute("bpdList", bpdList);			
			this.getRequest().setAttribute("bbrList", bbrList);			
			this.getRequest().setAttribute("biddingBidList", biddingBidList);
			  
		} catch (Exception e) {
			log("查看供应商报价信息明细信息错误！", e);
			throw new BaseException("查看供应商报价信息明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 供应商比价
	 * @return
	 * @throws BaseException 
	 */
	public String viewParityPrice() throws BaseException {
		
		try{	
			biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
            biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		    biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
			
		    
			bidPrice=new BidPrice();
			bidPrice.setRcId(rcId);
			List<Object[]> objectList=this.iBidPriceBiz.getBidPriceListSupplierName(bidPrice);
			String[] supplierStr=new String[objectList.size()];
			Long[] bpIdStr=new Long[objectList.size()];
			Double[] totalPriceStr=new Double[objectList.size()];
			Double[] taxRateStr=new Double[objectList.size()];
			String[] colorStr=new String[objectList.size()];
			int i=0;
			for(Object[] object:objectList){
				bidPrice=(BidPrice)object[0];
				supplierStr[i]=(String)object[1];
				bpIdStr[i]=bidPrice.getBpId();
				totalPriceStr[i]=bidPrice.getTotalPrice();
				taxRateStr[i]=bidPrice.getTaxRate()==null?Double.parseDouble("0"):bidPrice.getTaxRate();
				colorStr[i]=RandomColor.generateColor(i);
				i++;
			}
			
			List bidPriceList=this.iBidPriceDetailBiz.getBidPriceDetailListForParityPrice(bpIdStr, rcId);

			List bbrfList=this.iBidBusinessResponseBiz.getBidBusinessResponseForParityPrice(bpIdStr, rcId);
			
						
			this.getRequest().setAttribute("supplierStr", supplierStr);
			this.getRequest().setAttribute("bpIdStr", bpIdStr);
			this.getRequest().setAttribute("totalPriceStr", totalPriceStr);
			this.getRequest().setAttribute("colorStr", colorStr);
			this.getRequest().setAttribute("taxRateStr", taxRateStr);
			this.getRequest().setAttribute("bidPriceList", bidPriceList);
			this.getRequest().setAttribute("bbrfList", bbrfList);	
			this.getRequest().setAttribute("biddingBidList", biddingBidList);
		} catch (Exception e) {
			log("供应商比价信息错误！", e);
			throw new BaseException("供应商比价信息错误！", e);
		}
		return "parityPrice";
	}
	
	public IBidPriceBiz getiBidPriceBiz() {
		return iBidPriceBiz;
	}

	public void setiBidPriceBiz(IBidPriceBiz iBidPriceBiz) {
		this.iBidPriceBiz = iBidPriceBiz;
	}
	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}

	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}

	public IBidPriceDetailBiz getiBidPriceDetailBiz() {
		return iBidPriceDetailBiz;
	}

	public void setiBidPriceDetailBiz(IBidPriceDetailBiz iBidPriceDetailBiz) {
		this.iBidPriceDetailBiz = iBidPriceDetailBiz;
	}

	public IBidPriceHistoryBiz getiBidPriceHistoryBiz() {
		return iBidPriceHistoryBiz;
	}

	public void setiBidPriceHistoryBiz(IBidPriceHistoryBiz iBidPriceHistoryBiz) {
		this.iBidPriceHistoryBiz = iBidPriceHistoryBiz;
	}

	public IBidBusinessResponseBiz getiBidBusinessResponseBiz() {
		return iBidBusinessResponseBiz;
	}

	public void setiBidBusinessResponseBiz(
			IBidBusinessResponseBiz iBidBusinessResponseBiz) {
		this.iBidBusinessResponseBiz = iBidBusinessResponseBiz;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}

	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public BidPrice getBidPrice() {
		return bidPrice;
	}
	public void setBidPrice(BidPrice bidPrice) {
		this.bidPrice = bidPrice;
	}
	public Long getBbrId() {
		return bbrId;
	}
	public void setBbrId(Long bbrId) {
		this.bbrId = bbrId;
	}
	public IBiddingBidRoundBiz getiBiddingBidRoundBiz() {
		return iBiddingBidRoundBiz;
	}
	public void setiBiddingBidRoundBiz(IBiddingBidRoundBiz iBiddingBidRoundBiz) {
		this.iBiddingBidRoundBiz = iBiddingBidRoundBiz;
	}
	public IBiddingCommunicationInfoBiz getiBiddingCommunicationInfoBiz() {
		return iBiddingCommunicationInfoBiz;
	}
	public void setiBiddingCommunicationInfoBiz(
			IBiddingCommunicationInfoBiz iBiddingCommunicationInfoBiz) {
		this.iBiddingCommunicationInfoBiz = iBiddingCommunicationInfoBiz;
	}
	public Long getBblId() {
		return bblId;
	}
	public void setBblId(Long bblId) {
		this.bblId = bblId;
	}
	public BiddingBidList getBiddingBidList() {
		return biddingBidList;
	}
	public void setBiddingBidList(BiddingBidList biddingBidList) {
		this.biddingBidList = biddingBidList;
	}
	public RequiredCollect getRequiredCollect() {
		return requiredCollect;
	}
	public void setRequiredCollect(RequiredCollect requiredCollect) {
		this.requiredCollect = requiredCollect;
	}
	
}
