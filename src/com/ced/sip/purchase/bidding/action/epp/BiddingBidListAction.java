package com.ced.sip.purchase.bidding.action.epp;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.common.utils.encrypt.RSAEncrypt;
import com.ced.sip.purchase.bidding.biz.IBiddingBidListBiz;
import com.ced.sip.purchase.bidding.biz.IBiddingBidRoundBiz;
import com.ced.sip.purchase.bidding.entity.BiddingBidList;
import com.ced.sip.purchase.bidding.entity.BiddingBidRound;
import com.ced.sip.purchase.bidding.util.BiddingProgressStatus;
import com.ced.sip.purchase.bidding.util.BiddingStatus;
import com.ced.sip.purchase.bidding.util.BiddingStatusMap;
import com.ced.sip.purchase.base.biz.IBidProcessLogBiz;
import com.ced.sip.purchase.base.biz.IBusinessResponseItemBiz;
import com.ced.sip.purchase.base.biz.IInviteSupplierBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidProcessLog;
import com.ced.sip.purchase.base.entity.BusinessResponseItem;
import com.ced.sip.purchase.base.entity.InviteSupplier;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;

public class BiddingBidListAction extends BaseAction {
	 //竞价信息服务类
     private IBiddingBidListBiz iBiddingBidListBiz;
     //竞价轮次信息管理
     private IBiddingBidRoundBiz iBiddingBidRoundBiz;
     //邀请供应商服务类
     private IInviteSupplierBiz iInviteSupplierBiz;
     //商务响应项服务类
     private IBusinessResponseItemBiz iBusinessResponseItemBiz;
     //项目信息服务类
     private IRequiredCollectBiz iRequiredCollectBiz;
 	 //标段流程记录实例表
 	 private IBidProcessLogBiz iBidProcessLogBiz;
 	 //供应商服务类
 	 private ISupplierInfoBiz iSupplierInfoBiz;
 	 //项目日志记录服务类
 	 private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
 	
     
     private Long rcId;
     private String isDetail;
     
     private RequiredCollect requiredCollect;
     private BiddingBidList biddingBidList;
     private BiddingBidRound biddingBidRound;
     private InviteSupplier inviteSupplier;
     private BusinessResponseItem businessResponseItem;
     private PurchaseRecordLog purchaseRecordLog;
  	 private BidProcessLog bidProcessLog;
  	 private List<BusinessResponseItem> briList;

 	/**
 	* 当流程监控执行关键节点时候需要更新标段监控表，同时更新当前节点的完成时间，插入下一个节点的接收时间
 	* @param rcId 项目id
 	* @param currProgress_Status 当前节点
 	* @param nextProgress_Status 下一个节点
 	* @param nextProgress_Status_Text 下一个节点的节点名称
 	* @author luguanglei
 	* @throws BaseException 
 	*/
 	private void updateBidMonitorAndBidProcessLog(Long rcId,Long currProgress_Status, Long nextProgress_Status,String nextProgress_Status_Text)throws BaseException{
 		//修改项目监控至下一个节点
 		requiredCollect=new RequiredCollect();
 		requiredCollect.setRcId(rcId);
 		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
 	    if(requiredCollect.getServiceStatus()<nextProgress_Status){
 	    	requiredCollect.setServiceStatus(nextProgress_Status);
 	    	requiredCollect.setServiceStatusCn(nextProgress_Status_Text);
 	        this.iRequiredCollectBiz.updateRequiredCollect(requiredCollect);
 	    }
 	  //修改项目日志表的当前节点的完成时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(currProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    bidProcessLog.setCompleteDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.updateBidProcessLog(bidProcessLog);
 	  //新增项目日志表的下一个节点的接收时间
 	    bidProcessLog=new BidProcessLog();
 	    bidProcessLog.setRcId(rcId);
 	    bidProcessLog.setBidNode(nextProgress_Status);
 	    bidProcessLog=this.iBidProcessLogBiz.getBidProcessLogByRcIdAndBidNode(bidProcessLog);
 	    if(bidProcessLog.getBplId()==null){
 	    bidProcessLog.setReceiveDate(DateUtil.getCurrentDateTime());
 	    this.iBidProcessLogBiz.saveBidProcessLog(bidProcessLog);
 	    }
 	
 	}
 	/**
	 * 判断是否是编制人员
	 * @param bidMonitor
	 * @return
	 */
 	private  boolean isWriter(String writer){
		boolean isWriter=false;
		String username=UserRightInfoUtil.getUserName(this.getRequest());
		//当前登录人是不是编制人员
		if(username.equals(writer)) isWriter=true;
		return isWriter;
	}
	/**
	 * 竞价项目竞价方案项目监控
	 * @return
	 * @throws BaseException 
	 */
	public String viewBiddingBidListMonitor() throws BaseException {
		String view="biddingBidListMonitorDetail";
		try{
			
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			
			if(!isWriter(requiredCollect.getWriter())) isDetail="detail";
			List<BiddingBidRound> trialBidRounds=null,bidRounds=null;
			biddingBidList =this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
			if(biddingBidList.getBblId()!=null){//清单计划存在
				
				biddingBidRound=new BiddingBidRound();
				biddingBidRound.setRcId(rcId);
				biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_0);
				trialBidRounds=this.iBiddingBidRoundBiz.getBiddingBidRoundList(biddingBidRound);
				

				biddingBidRound=new BiddingBidRound();
				biddingBidRound.setRcId(rcId);
				biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_1);
				bidRounds=this.iBiddingBidRoundBiz.getBiddingBidRoundList(biddingBidRound);
				
				biddingBidList.setBidAdminCn(BaseDataInfosUtil.convertUserIdToChnName(biddingBidList.getBidAdmin()));

			}else{
				biddingBidList.setResponsibleUser(UserRightInfoUtil.getChineseName(this.getRequest()));
                biddingBidList.setResponsiblePhone(UserRightInfoUtil.getUserPhone(this.getRequest()));
				biddingBidList.setRcId(rcId);
				biddingBidList.setBiddingPrinciple(BiddingStatus.BIDDING_PRINCIPLE_0);
			}

			this.getRequest().setAttribute("trialBidRounds", trialBidRounds);
			this.getRequest().setAttribute("bidRounds", bidRounds);
			
			
			if(TableStatus.SUPPLIER_TYPE_01.equals(requiredCollect.getSupplierType())){
            //邀请供应商
			inviteSupplier = new InviteSupplier();
			inviteSupplier.setRcId(rcId);
			List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
			String supplierId=",";
			String supplierType=",";
			String supplierName="";
			String returnVals="";
			for(int i=0;i<sulist.size();i++){
				inviteSupplier =  sulist.get(i);
				 supplierId +=inviteSupplier.getSupplierId()+",";
				 supplierName +=inviteSupplier.getSupplierName()+",";
				 supplierType +=inviteSupplier.getSourceCategory()+",";
				 returnVals+=inviteSupplier.getSupplierId()+":"+inviteSupplier.getSupplierName()+":"+inviteSupplier.getSourceCategory()+",";
				
			}
			returnVals = StringUtil.subStringLastOfSeparator(returnVals,',');
			this.getRequest().setAttribute("returnVals", returnVals);
			this.getRequest().setAttribute("supplierIds", supplierId);
			this.getRequest().setAttribute("supplierTypes", supplierType);
			
			}
			this.getRequest().setAttribute("supplierType", requiredCollect.getSupplierType());
			
		    //商务响应项
			businessResponseItem=new BusinessResponseItem();
			businessResponseItem.setRcId(rcId);
			List<BusinessResponseItem> businessResponseItemsList=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
            this.getRequest().setAttribute("businessResponseItemsList", businessResponseItemsList);
			//公告未发布 且项目状态为正常 且非查看页面
            if(TableStatus.BID_STATUS_1.equals(requiredCollect.getBidStatus())&&TableStatus.NOTICE_TYPE_1.equals(requiredCollect.getIsSendNotice())&&StringUtil.isBlank(isDetail)&&(requiredCollect.getStatus().equals(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01)||requiredCollect.getStatus().equals(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3)))
			{   
            	Map priceTypeMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1702);
        	    Map priceColumnTypeMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1703);
        	    Map biddingTypeMap = BiddingStatusMap.biddingType;
        	    Map priceModeMap = BiddingStatusMap.priceMode;
        	    Map pricePrincipleMap = BiddingStatusMap.pricePrinciple;
        	    Map biddingPrincipleMap = BiddingStatusMap.biddingPrinciple;
        	    this.getRequest().setAttribute("priceTypeMap", priceTypeMap);
        	    this.getRequest().setAttribute("priceColumnTypeMap", priceColumnTypeMap);
        	    this.getRequest().setAttribute("biddingTypeMap", biddingTypeMap);
        	    this.getRequest().setAttribute("priceModeMap", priceModeMap);
        	    this.getRequest().setAttribute("pricePrincipleMap", pricePrincipleMap);
        	    this.getRequest().setAttribute("biddingPrincipleMap", biddingPrincipleMap);
            	view="biddingBidListMonitorUpdate";
			}else{
				biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
				biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
                biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
				biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
				biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
				biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
			}
			
			
		} catch (Exception e) {
			log("通过项目进入竞价方案错误！", e);
			throw new BaseException("通过项目进入竞价方案错误！", e);
		}
		return view;
	}
	/**
	 * 保存竞价方案 商务响应项  邀请供应商
	 * @return
	 * @throws BaseException 
	 */
	public String updateBiddingBidList() throws BaseException {
		String view="success";
		try{
			String userNameCn=UserRightInfoUtil.getChineseName(getRequest());
			String userName=UserRightInfoUtil.getUserName(this.getRequest());
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			
			String[] trialLcs=this.getRequest().getParameterValues("trialLc");
			String[] trialTimeLongs=this.getRequest().getParameterValues("trialTimeLong");
			String[] trialPriceCounts=this.getRequest().getParameterValues("trialPriceCount");
			String[] lcs=this.getRequest().getParameterValues("lc");
			String[] timeLongs=this.getRequest().getParameterValues("timeLong");
			String[] priceCounts=this.getRequest().getParameterValues("priceCount");

			String[] biddingAdminRightss=this.getRequest().getParameterValues("biddingAdminRights");
			String[] supplierRightss=this.getRequest().getParameterValues("supplierRights");
			
			biddingBidList.setBiddingAdminRights(StringUtil.toStringArrayForString(biddingAdminRightss));
			biddingBidList.setSupplierRights(StringUtil.toStringArrayForString(supplierRightss));
			biddingBidList.setBiddingStartTime(DateUtil.StringToDate(biddingBidList.getBiddingStartTimeStr(),"yyyy-MM-dd HH:mm"));
			biddingBidList.setBiddingEndTime(DateUtil.StringToDate(biddingBidList.getBiddingEndTimeStr(),"yyyy-MM-dd HH:mm"));	
			if(StringUtil.isBlank(biddingBidList.getDelayTime())) biddingBidList.setIsDelay(1); else  biddingBidList.setIsDelay(0); 
			biddingBidList.setIsBidEnd(1);
			biddingBidList.setIsDelayBid(1);
			biddingBidList.setCurrentBidRound(new Long(0));
			if(StringUtil.isBlank(trialLcs)){ 
				biddingBidList.setTrialBiddingRound(0);
                biddingBidList.setIsTrialBid(1);
			} else {
				biddingBidList.setTrialBiddingRound(trialLcs.length);
	            biddingBidList.setIsTrialBid(0);
			}
			if(StringUtil.isBlank(lcs)) biddingBidList.setBiddingRound(0); else biddingBidList.setBiddingRound(lcs.length);
			if(biddingBidList.getBblId()==null){
				biddingBidList.setWriteDate(new Date());
				biddingBidList.setWriter(UserRightInfoUtil.getUserName(getRequest()));
				this.iBiddingBidListBiz.saveBiddingBidList(biddingBidList);
			}else{
				this.iBiddingBidListBiz.updateBiddingBidList(biddingBidList);
				//删除轮次信息
				this.iBiddingBidRoundBiz.deleteBiddingBidRoundByRcId(biddingBidList.getRcId());
				//删除
				this.iInviteSupplierBiz.deleteInviteSupplierByRcId(biddingBidList.getRcId());
				this.iBusinessResponseItemBiz.deleteBusinessResponseItemByRcId(biddingBidList.getRcId());
			}
			
			requiredCollect=new RequiredCollect();
			requiredCollect.setRcId(biddingBidList.getRcId());
			requiredCollect.setBidReturnDate(biddingBidList.getBiddingEndTime());
			requiredCollect.setBidOpenDate(biddingBidList.getBiddingStartTime());
			this.iRequiredCollectBiz.updateRequiredCollectNodeByRequiredCollect(requiredCollect);
			
			//试竞价轮次信息
			for(int i=0;i<trialLcs.length;i++){
				biddingBidRound=new BiddingBidRound();
				biddingBidRound.setBiddingRound(Integer.parseInt(trialLcs[i]));
				biddingBidRound.setTimeLong(Integer.parseInt(trialTimeLongs[i])*60);
				biddingBidRound.setSurplusTime(Integer.parseInt(trialTimeLongs[i])*60*1000);
				biddingBidRound.setSuspendTime(0);
				biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_0);
				biddingBidRound.setRcId(biddingBidList.getRcId());
				biddingBidRound.setStatus(BiddingStatus.BIDDING_STATUS_0);
				if(StringUtil.isBlank(trialPriceCounts[i])) biddingBidRound.setPriceCount(null);else biddingBidRound.setPriceCount(Long.parseLong(trialPriceCounts[i]));
				this.iBiddingBidRoundBiz.saveBiddingBidRound(biddingBidRound);
			}
			
			//正式轮次信息
			for(int i=0;i<lcs.length;i++){
				biddingBidRound=new BiddingBidRound();
				biddingBidRound.setBiddingRound(Integer.parseInt(lcs[i]));
				biddingBidRound.setTimeLong(Integer.parseInt(timeLongs[i])*60);
				biddingBidRound.setSurplusTime(Integer.parseInt(timeLongs[i])*60*1000);
				biddingBidRound.setSuspendTime(0);
				biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_1);
				biddingBidRound.setRcId(biddingBidList.getRcId());
				biddingBidRound.setStatus(BiddingStatus.BIDDING_STATUS_0);
				if(StringUtil.isBlank(priceCounts[i])) biddingBidRound.setPriceCount(null);else biddingBidRound.setPriceCount(Long.parseLong(priceCounts[i]));
				this.iBiddingBidRoundBiz.saveBiddingBidRound(biddingBidRound);
			}
			Map<String,String> map=null;
			String publicKey,privateKey;
			//增加
			String supValue = this.getRequest().getParameter("supIds");
			String supTypeValue = this.getRequest().getParameter("supTypes");
			Long id;
			int type;
			SupplierInfo suppInfo;
			InviteSupplier inviteSupplier;
			if(!supValue.equals(",")){
				String[] supArr = supValue.split(",");
				String[] supTypeArr = supTypeValue.split(",");
				//System.out.println(supArr);
				for(int i=1;i<supArr.length;i++){
					if(supArr[i]!=null&&supArr[i]!=""){
						type=Integer.parseInt(supTypeArr[i]);
						id = Long.parseLong(supArr[i]);
						suppInfo = this.iSupplierInfoBiz.getSupplierInfo(id);
						inviteSupplier = new InviteSupplier();
						inviteSupplier.setRcId(biddingBidList.getRcId());
						inviteSupplier.setSupplierId(id);
						inviteSupplier.setSupplierName(suppInfo.getSupplierName());
						inviteSupplier.setWriteDate(DateUtil.getCurrentDateTime());
						inviteSupplier.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
						inviteSupplier.setPriceNum( new Long(0) ) ;
						inviteSupplier.setSupplierEmail(suppInfo.getContactEmail());
						inviteSupplier.setSupplierPhone(suppInfo.getMobilePhone());
						map=RSAEncrypt.genKeyPair();
						publicKey=map.get("publicKey");
						privateKey=map.get("privateKey");
						inviteSupplier.setPrivateKey(privateKey);
						inviteSupplier.setPublicKey(publicKey);
						inviteSupplier.setSourceCategory(type);
						this.iInviteSupplierBiz.saveInviteSupplier(inviteSupplier); 
					}
				}
			}
			//增加商务响应项方案
			// 保存新数据
			if (briList != null) {
				for (int i = 0; i < briList.size(); i++) {
					businessResponseItem = briList.get(i);
					if (businessResponseItem != null) {
						businessResponseItem.setRcId(biddingBidList.getRcId());
						businessResponseItem.setWriteDate(DateUtil.getCurrentDateTime());
						businessResponseItem.setWriter(UserRightInfoUtil.getUserName(this.getRequest()));
						iBusinessResponseItemBiz.saveBusinessResponseItem(businessResponseItem);
					}
				}
			}
			String operateContent="竞价方案已编制，竞价开始时间为【"+DateUtil.getStringFromDate(biddingBidList.getBiddingStartTime())+"】,结束时间【"+DateUtil.getStringFromDate(biddingBidList.getBiddingEndTime())+"】";
			updateBidMonitorAndBidProcessLog(biddingBidList.getRcId(), BiddingProgressStatus.Progress_Status_20, BiddingProgressStatus.Progress_Status_21, BiddingProgressStatus.Progress_Status_21_Text);
				
			//保存流程跟踪计划
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setOperateDate(DateUtil.getCurrentDateTime());
			purchaseRecordLog.setOperatorId(UserRightInfoUtil.getUserId(this.getRequest())+"");
			purchaseRecordLog.setOperatorName(UserRightInfoUtil.getUserName(this.getRequest()));
			purchaseRecordLog.setRcId(biddingBidList.getRcId());
			purchaseRecordLog.setOperateContent(operateContent);
			purchaseRecordLog.setBidNode(BiddingProgressStatus.Progress_Status_20_Text);
			this.iPurchaseRecordLogBiz.savePurchaseRecordLog(purchaseRecordLog);
			
			this.getRequest().setAttribute("message","操作成功");
			this.getRequest().setAttribute("operModule", "编制竞价方案成功");
		} catch (Exception e) {
			log("编制竞价方案错误！", e);
			throw new BaseException("编制竞价方案错误！", e);
		}
		return view;
		
	}
	/**
	 * 通用方法判断消息当前登录人是否显示待办消息提示(采购流程界面)
	 * @return 0 不显示 1 显示
	 * @throws BaseException 
	 */
	public String messageBidMonitor() throws BaseException {
		String msg="0";
		PrintWriter out = null;
		try{
			out = this.getResponse().getWriter();
			requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
			boolean isWriter=isWriter(requiredCollect.getWriter());
			long currNode=requiredCollect.getServiceStatus();
			if(currNode==BiddingProgressStatus.Progress_Status_25){
				biddingBidList=this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
				if(UserRightInfoUtil.getUserId(this.getRequest()).equals(biddingBidList.getBidAdmin())){
					msg="1";
				}
			}else{
				if(isWriter) msg="1";
			}
			out.print(msg);
		} catch (Exception e) {
			log("通用方法判断消息当前登录人是否显示待办消息提示(采购流程界面)！", e);
			throw new BaseException("通用方法判断消息当前登录人是否显示待办消息提示(采购流程界面)！", e);
		}
		return null;
		
	}
	/**
	 * 竞价项目竞价方案
	 * @return
	 * @throws BaseException 
	 */
	public String viewBiddingBidListDetail() throws BaseException {

		requiredCollect=this.iRequiredCollectBiz.getRequiredCollect(rcId);
		List<BiddingBidRound> trialBidRounds=null,bidRounds=null;
		biddingBidList =this.iBiddingBidListBiz.getBiddingBidListByRcId(rcId);
		if(biddingBidList.getBblId()!=null){//清单计划存在
			
			biddingBidRound=new BiddingBidRound();
			biddingBidRound.setRcId(rcId);
			biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_0);
			trialBidRounds=this.iBiddingBidRoundBiz.getBiddingBidRoundList(biddingBidRound);
			

			biddingBidRound=new BiddingBidRound();
			biddingBidRound.setRcId(rcId);
			biddingBidRound.setBidType(BiddingStatus.BIDDING_LX_1);
			bidRounds=this.iBiddingBidRoundBiz.getBiddingBidRoundList(biddingBidRound);

		}

		this.getRequest().setAttribute("trialBidRounds", trialBidRounds);
		this.getRequest().setAttribute("bidRounds", bidRounds);
		
		
		if(TableStatus.SUPPLIER_TYPE_01.equals(requiredCollect.getSupplierType())){
        //邀请供应商
		inviteSupplier = new InviteSupplier();
		inviteSupplier.setRcId(rcId);
		List<InviteSupplier> sulist = this.iInviteSupplierBiz.getInviteSupplierList(inviteSupplier);
		String supplierId=",";
		String supplierType=",";
		String supplierName="";
		String returnVals="";
		for(int i=0;i<sulist.size();i++){
			inviteSupplier =  sulist.get(i);
			 supplierId +=inviteSupplier.getSupplierId()+",";
			 supplierName +=inviteSupplier.getSupplierName()+",";
			 supplierType +=inviteSupplier.getSourceCategory()+",";
			 returnVals+=inviteSupplier.getSupplierId()+":"+inviteSupplier.getSupplierName()+":"+inviteSupplier.getSourceCategory()+",";
			
		}
		returnVals = StringUtil.subStringLastOfSeparator(returnVals,',');
		this.getRequest().setAttribute("returnVals", returnVals);
		this.getRequest().setAttribute("supplierIds", supplierId);
		this.getRequest().setAttribute("supplierTypes", supplierType);
		
		}
		this.getRequest().setAttribute("supplierType", requiredCollect.getSupplierType());
		
	    //商务响应项
		businessResponseItem=new BusinessResponseItem();
		businessResponseItem.setRcId(rcId);
		List<BusinessResponseItem> businessResponseItemsList=this.iBusinessResponseItemBiz.getBusinessResponseItemList(businessResponseItem);
        this.getRequest().setAttribute("businessResponseItemsList", businessResponseItemsList);
		biddingBidList.setBidAdminCn(BaseDataInfosUtil.convertUserIdToChnName(biddingBidList.getBidAdmin()));
		biddingBidList.setPriceTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1702));
		biddingBidList.setPriceColumnTypeCn(BaseDataInfosUtil.convertDictCodeToName(biddingBidList.getPriceType(),DictStatus.COMMON_DICT_TYPE_1703));
        biddingBidList.setBiddingTypeCn(BiddingStatusMap.biddingType.get(biddingBidList.getBiddingType()));
		biddingBidList.setPriceModeCn(BiddingStatusMap.priceMode.get(biddingBidList.getPriceMode()));
		biddingBidList.setPricePrincipleCn(BiddingStatusMap.pricePrinciple.get(biddingBidList.getPricePrinciple()));
		biddingBidList.setBiddingPrincipleCn(BiddingStatusMap.biddingPrinciple.get(biddingBidList.getBiddingPrinciple()));
		
		return "biddingBidListMonitorDetail";
	}	
	public IBiddingBidListBiz getiBiddingBidListBiz() {
		return iBiddingBidListBiz;
	}
	public void setiBiddingBidListBiz(IBiddingBidListBiz iBiddingBidListBiz) {
		this.iBiddingBidListBiz = iBiddingBidListBiz;
	}
	public IInviteSupplierBiz getiInviteSupplierBiz() {
		return iInviteSupplierBiz;
	}
	public void setiInviteSupplierBiz(IInviteSupplierBiz iInviteSupplierBiz) {
		this.iInviteSupplierBiz = iInviteSupplierBiz;
	}
	public IBusinessResponseItemBiz getiBusinessResponseItemBiz() {
		return iBusinessResponseItemBiz;
	}
	public void setiBusinessResponseItemBiz(
			IBusinessResponseItemBiz iBusinessResponseItemBiz) {
		this.iBusinessResponseItemBiz = iBusinessResponseItemBiz;
	}
	public Long getRcId() {
		return rcId;
	}
	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}
	public String getIsDetail() {
		return isDetail;
	}
	public void setIsDetail(String isDetail) {
		this.isDetail = isDetail;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public BiddingBidList getBiddingBidList() {
		return biddingBidList;
	}
	public void setBiddingBidList(BiddingBidList biddingBidList) {
		this.biddingBidList = biddingBidList;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}
	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	public List<BusinessResponseItem> getBriList() {
		return briList;
	}
	public void setBriList(List<BusinessResponseItem> briList) {
		this.briList = briList;
	}
	public IBidProcessLogBiz getiBidProcessLogBiz() {
		return iBidProcessLogBiz;
	}
	public void setiBidProcessLogBiz(IBidProcessLogBiz iBidProcessLogBiz) {
		this.iBidProcessLogBiz = iBidProcessLogBiz;
	}
	public IBiddingBidRoundBiz getiBiddingBidRoundBiz() {
		return iBiddingBidRoundBiz;
	}
	public void setiBiddingBidRoundBiz(IBiddingBidRoundBiz iBiddingBidRoundBiz) {
		this.iBiddingBidRoundBiz = iBiddingBidRoundBiz;
	}	
}
