package com.ced.sip.requirement.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.requirement.biz.IRequiredMaterialBiz;
import com.ced.sip.requirement.entity.RequiredMaterial;
import com.ced.sip.requirement.entity.RequiredMaterialDetail;
import com.ced.sip.system.entity.CategoryBuyer;
import com.ced.sip.system.entity.PurchaseDepartBuyer;

public class RequiredMaterialBizImpl extends BaseBizImpl implements
		IRequiredMaterialBiz {

	/**
	 * 根据主键获得需求计划表实例
	 * @param id 主键
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public RequiredMaterial getRequiredMaterial(Long id) throws BaseException {
		return (RequiredMaterial)this.getObject(RequiredMaterial.class, id);
	}
	
	/**
	 * 获得需求计划表实例
	 * @param requiredMaterial 需求计划表实例
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public RequiredMaterial getRequiredMaterial( RequiredMaterial requiredMaterial ) throws BaseException {
		return (RequiredMaterial)this.getObject(RequiredMaterial.class, requiredMaterial.getRmId() );
	}
	
	/**
	 * 添加需求计划信息
	 * @param requiredMaterial 需求计划表实例
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void saveRequiredMaterial(RequiredMaterial requiredMaterial) throws BaseException{
		this.saveObject( requiredMaterial ) ;
	}
	
	/**
	 * 更新需求计划表实例
	 * @param requiredMaterial 需求计划表实例
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void updateRequiredMaterial(RequiredMaterial requiredMaterial) throws BaseException{
		this.updateObject( requiredMaterial ) ;
	}
	
	/**
	 * 删除需求计划表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void deleteRequiredMaterial(String id) throws BaseException {
		this.removeObject( this.getRequiredMaterial( new Long(id) ) ) ;
	}
	
	/**
	 * 删除需求计划表实例
	 * @param requiredMaterial 需求计划表实例
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void deleteRequiredMaterial(RequiredMaterial requiredMaterial) throws BaseException {
		this.removeObject( requiredMaterial ) ;
	}
	
	/**
	 * 删除需求计划表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void deleteRequiredMaterials(String[] id) throws BaseException {
		this.removeBatchObject(RequiredMaterial.class, id) ;
	}
	
	/**
	 * 获得所有需求计划表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterial de where 1 = 1 " );
		
		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得需求计划表数据集
	 * @param requiredMaterial 查询参数对象
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public RequiredMaterial getRequiredMaterial( String rmCode ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterial de where 1 = 1 " );
		hql.append(" and de.rmCode ='").append(rmCode).append("'");
		hql.append(" order by de.id desc ");
		List<RequiredMaterial>  rmList = this.getObjects( hql.toString() );
		RequiredMaterial requiredMaterial = null;
		if(rmList!=null&&rmList.size()>0){
			requiredMaterial = rmList.get(0);
		}
		return requiredMaterial;
	}
	
	/**
	 * 获得所有需求计划表数据集
	 * @param requiredMaterial 查询参数对象
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialList(  RequiredMaterial requiredMaterial ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterial de where 1 = 1 " );
		if(requiredMaterial!=null){
			if(StringUtil.isNotBlank(requiredMaterial.getRmCode())){
				hql.append(" and de.rmCode like '%").append(requiredMaterial.getRmCode()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getDeptId())){
				hql.append(" and de.deptId =").append(requiredMaterial.getDeptId());
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmName())){
				hql.append(" and de.rmName ='").append(requiredMaterial.getRmName()).append("'");
			}
		}
		
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有需求计划表数据集
	 * @param requiredMaterial 查询参数对象
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialList(  RequiredMaterial requiredMaterial ,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterial de where 1 = 1 " );
		if(requiredMaterial!=null){
			if(StringUtil.isNotBlank(requiredMaterial.getRmCode())){
				hql.append(" and de.rmCode ='").append(requiredMaterial.getRmCode()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getDeptId())){
				hql.append(" and de.deptId =").append(requiredMaterial.getDeptId());
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmName())){
				hql.append(" and de.rmName ='").append(requiredMaterial.getRmName()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getWriter())){
				hql.append(" and de.writer ='").append(requiredMaterial.getWriter()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getWriteDateStart())){
				hql.append(" and  to_char(de.writeDete,'yyyy-MM-dd')").append(">='").append(requiredMaterial.getWriteDateStart()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getWriteDateEnd())){
				hql.append(" and  to_char(de.writeDete,'yyyy-MM-dd')").append("<='").append(requiredMaterial.getWriteDateEnd()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmType())){
				hql.append(" and de.rmType ='").append(requiredMaterial.getRmType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getStatus())){
				hql.append(" and de.status ='").append(requiredMaterial.getStatus()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getPurchaseDeptId())){
				hql.append(" and de.purchaseDeptId =").append(requiredMaterial.getPurchaseDeptId());
			}
			if(StringUtil.isNotBlank(requiredMaterial.getDeptId())){
				hql.append(" and de.deptId =").append(requiredMaterial.getDeptId());
			}
			if (StringUtil.isNotBlank(requiredMaterial.getComId())) {
				hql.append(" and de.comId = ").append(requiredMaterial.getComId()).append("");
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有需求计划表数据集
	 * @param rollPage 分页对象
	 * @param requiredMaterial 查询参数对象
	 * @author Ushine,2014-11-05
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialList( RollPage rollPage, RequiredMaterial requiredMaterial ,String sqlstr) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterial de where 1 = 1 " );
		if(StringUtil.isNotBlank(requiredMaterial)){
			if(StringUtil.isNotBlank(requiredMaterial.getRmCode())){
				hql.append(" and de.rmCode ='").append(requiredMaterial.getRmCode()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getDeptId())){
				hql.append(" and de.deptId =").append(requiredMaterial.getDeptId());
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmName())){
				hql.append(" and de.rmName ='").append(requiredMaterial.getRmName()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getWriter())){
				hql.append(" and de.writer ='").append(requiredMaterial.getWriter()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getWriteDateStart())){
				hql.append(" and  to_char(de.writeDete,'yyyy-MM-dd')").append(">='").append(requiredMaterial.getWriteDateStart()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getWriteDateEnd())){
				hql.append(" and  to_char(de.writeDete,'yyyy-MM-dd')").append("<='").append(requiredMaterial.getWriteDateEnd()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmType())){
				hql.append(" and de.rmType ='").append(requiredMaterial.getRmType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getStatus())){
				hql.append(" and de.status ='").append(requiredMaterial.getStatus()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getPurchaseDeptId())){
				hql.append(" and de.purchaseDeptId =").append(requiredMaterial.getPurchaseDeptId());
			}
			if(StringUtil.isNotBlank(requiredMaterial.getDeptId())){
				hql.append(" and de.deptId =").append(requiredMaterial.getDeptId());
			}
            if (StringUtil.isNotBlank(requiredMaterial.getComId())) {
				hql.append(" and de.comId = ").append(requiredMaterial.getComId()).append("");
			}
		}
		if (StringUtil.isNotBlank(sqlstr)) {
			hql.append(" "+sqlstr+" ");
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 根据当前年月获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	public int getMaxFloatCodeByRmCodePrefix(String rmCodePrefix,Long comId)throws BaseException{
		int floatCode = 0;
		StringBuffer hql = new StringBuffer("select max(de.floatCode) from RequiredMaterial de where 1 = 1 " );
		hql.append(" and de.comId = ").append(comId).append("");
		hql.append(" and de.rmCode like '").append(rmCodePrefix).append("%'");
		
		List list = this.getObjects(hql.toString());
		if(list!=null && list.get(0)!= null){
			floatCode = Integer.parseInt(list.get(0).toString());
		}
		return floatCode;
	}
	
	
	
	
	
	/**
	 * 根据主键获得需求计划明细表实例
	 * @param id 主键
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public RequiredMaterialDetail getRequiredMaterialDetail(Long id) throws BaseException {
		return (RequiredMaterialDetail)this.getObject(RequiredMaterialDetail.class, id);
	}
	
	/**
	 * 获得需求计划明细表实例
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public RequiredMaterialDetail getRequiredMaterialDetail( RequiredMaterialDetail requiredMaterialDetail ) throws BaseException {
		return (RequiredMaterialDetail)this.getObject(RequiredMaterialDetail.class, requiredMaterialDetail.getRmdId() );
	}
	
	/**
	 * 添加需求计划明细信息
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void saveRequiredMaterialDetail(RequiredMaterialDetail requiredMaterialDetail) throws BaseException{
		this.saveObject( requiredMaterialDetail ) ;
	}
	
	/**
	 * 更新需求计划明细表实例
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void updateRequiredMaterialDetail(RequiredMaterialDetail requiredMaterialDetail) throws BaseException{
		this.updateObject( requiredMaterialDetail ) ;
	}
	
	/**
	 * 删除需求计划明细表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void deleteRequiredMaterialDetail(String id) throws BaseException {
		this.removeObject( this.getRequiredMaterialDetail( new Long(id) ) ) ;
	}
	
	/**
	 * 删除需求计划明细表实例
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void deleteRequiredMaterialDetail(RequiredMaterialDetail requiredMaterialDetail) throws BaseException {
		this.removeObject( requiredMaterialDetail ) ;
	}
	
	/**
	 * 删除需求计划明细表实例
	 * @param id 主键数组
	 * @author luguanglei 2017-3-25
	 * @throws BaseException 
	 */
	public void deleteRequiredMaterialDetails(String[] id) throws BaseException {
		this.removeBatchObject(RequiredMaterialDetail.class, id) ;
	}
	
	/**
	 * 获得所有需求计划明细表数据集
	 * @param rollPage 分页对象
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialDetailList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterialDetail de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有需求计划明细表数据集
	 * @param requiredMaterialDetail 查询参数对象
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialDetailList(  RequiredMaterialDetail requiredMaterialDetail ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterialDetail de where 1 = 1 " );
		if(requiredMaterialDetail != null){
			if(StringUtil.isNotBlank(requiredMaterialDetail.getRmId())){
				hql.append(" and de.rmId = ").append(requiredMaterialDetail.getRmId());
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getRmdId())){
				hql.append(" and de.rmdId = ").append(requiredMaterialDetail.getRmdId());
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getBuyName())){
				hql.append(" and de.buyName like '%").append(requiredMaterialDetail.getBuyName()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getBuyCode())){
				hql.append(" and de.buyCode like '%").append(requiredMaterialDetail.getBuyCode()).append("%'");
			}
		}
		hql.append(" order by de.id desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有需求计划明细表数据集
	 * @param rollPage 分页对象
	 * @param requiredMaterialDetail requiredMaterial 计划追踪 
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialDetailListForRequiredMaterialTrack( RollPage rollPage, RequiredMaterialDetail requiredMaterialDetail,RequiredMaterial requiredMaterial) throws BaseException {
		StringBuffer hql = new StringBuffer("select de from RequiredMaterialDetail de, RequiredMaterial rm where 1 = 1  and de.rmId=rm.rmId and rm.status='0' " );
		if(requiredMaterial != null){
			if(StringUtil.isNotBlank(requiredMaterial.getRmName())){
				hql.append(" and rm.rmName like '%").append(requiredMaterial.getRmName()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmCode())){
				hql.append(" and rm.rmCode like '%").append(requiredMaterial.getRmCode()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmType())){
				hql.append(" and rm.rmType = '").append(requiredMaterial.getRmType()).append("'");
			}
			if (StringUtil.isNotBlank(requiredMaterial.getComId())) {
				hql.append(" and de.comId = ").append(requiredMaterial.getComId()).append("");
			}
		}
		if(requiredMaterialDetail != null){
			if(StringUtil.isNotBlank(requiredMaterialDetail.getRmId())){
				hql.append(" and de.rmId = ").append(requiredMaterialDetail.getRmId());
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getBuyName())){
				hql.append(" and de.buyName like '%").append(requiredMaterialDetail.getBuyName()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getBuyCode())){
				hql.append(" and de.buyCode like '%").append(requiredMaterialDetail.getBuyCode()).append("%'");
			}
		}
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有需求计划明细表数据集
	 * @param rollPage 分页对象
	 * @param requiredMaterialDetail requiredMaterial 已采购数量小于计划数量  选择未汇总的采购计划
	 * @author luguanglei 2017-3-25
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialDetailListForRequiredCollect( RollPage rollPage, RequiredMaterialDetail requiredMaterialDetail,RequiredMaterial requiredMaterial,List<CategoryBuyer> listBuyers,List<PurchaseDepartBuyer> listpurchase) throws BaseException {
		StringBuffer hql = new StringBuffer("select de from RequiredMaterialDetail de, RequiredMaterial rm where 1 = 1  and de.rmId=rm.rmId and rm.status='0' " );
		hql.append(" and de.amount>de.yamount ");
		if(requiredMaterial != null){
			if(StringUtil.isNotBlank(requiredMaterial.getRmName())){
				hql.append(" and rm.rmName like '%").append(requiredMaterial.getRmName()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmCode())){
				hql.append(" and rm.rmCode like '%").append(requiredMaterial.getRmCode()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getRmType())){
				hql.append(" and rm.rmType = '").append(requiredMaterial.getRmType()).append("'");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getComId())){
				hql.append(" and rm.comId = ").append(requiredMaterial.getComId()).append("");
			}
			if(StringUtil.isNotBlank(requiredMaterial.getPurchaseDeptId())){
				hql.append(" and rm.purchaseDeptId = ").append(requiredMaterial.getPurchaseDeptId()).append("");
			}
		}
		if(requiredMaterialDetail != null){
			if(StringUtil.isNotBlank(requiredMaterialDetail.getRmId())){
				hql.append(" and de.rmId = ").append(requiredMaterialDetail.getRmId());
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getBuyName())){
				hql.append(" and de.buyName like '%").append(requiredMaterialDetail.getBuyName()).append("%'");
			}
			if(StringUtil.isNotBlank(requiredMaterialDetail.getBuyCode())){
				hql.append(" and de.buyCode like '%").append(requiredMaterialDetail.getBuyCode()).append("%'");
			}
		}
		hql.append(" and ( 1=1 ");
		for(PurchaseDepartBuyer departBuyer:listpurchase){
			hql.append(" or rm.deptId=").append(departBuyer.getDeptId());
		}
		hql.append(" ) ");
		
		hql.append(" and ( 1=1 ");
		for(CategoryBuyer buyer:listBuyers){
			hql.append(" or de.buyCode like '").append(buyer.getMkThreeCode()).append("%'");
		}
		hql.append(" ) ");
		
		hql.append(" order by "+rollPage.getOrderColumn()+" "+rollPage.getOrderDir()+" ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 获得所有需求计划明细表数据集 
	 * @param rmIds 查询参数对象 
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialDetailListByRmIds(String rmIds) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterialDetail de where 1 = 1 " );
		if(StringUtil.isNotBlank(rmIds))
		{
			hql.append(" and de.rmId in (").append(rmIds).append(")");
		}
		
		hql.append(" order by de.id desc ");
		return this.getObjects(hql.toString() );
	}
	/**
	 * 获得所有需求计划明细表数据集   根据计划id
	 * @param requiredMaterialDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getRequiredMaterialDetailListByRmId(Long rmId) throws BaseException {
		StringBuffer hql = new StringBuffer(" from RequiredMaterialDetail de where 1 = 1 " );
		hql.append(" and de.rmId = ").append(rmId);
		return this.getObjects(hql.toString() );
	}

	/**
     * sign 0 ：当删除采购计划明细时候，需要同时更新需求计划明细中的已采购数量yamount,   根据rmdId和amount来操作 
     * sign 1 ：当新增采购计划明细时候，需要同时更新需求计划明细中的已采购数量yamount,   根据rmdId和amount来操作 
     * @param rcdId
     * @param amount
     * @throws BaseException
     */
	public void updateRequiredMaterialYamountByRcdId(Long rmdId, Double amount,int sign)
			throws BaseException {
		String  sql="";
		if(sign==0) sql="update required_material_detail set yamount=yamount-"+amount+" where rmd_id="+rmdId;
		if(sign==1) sql="update required_material_detail set yamount=yamount+"+amount+" where rmd_id="+rmdId;
		this.updateJdbcSql(sql);		
	}

	/**
	 * 根据项目Id查询需求计划数据集
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	public List getDeptListByRcId(Long rcId) throws BaseException {
		StringBuffer hql = new StringBuffer("select distinct dept from RequiredMaterial rm,RequiredMaterialDetail rmd,RequiredCollectDetail rcd,Departments dept " );
		hql.append(" where rm.rmId=rmd.rmId and rmd.rmdId=rcd.rmdId");
		hql.append(" and rm.deptId = dept.depId");
		hql.append(" and rcd.rcId =").append(rcId);
		return this.getObjects(hql.toString() );
	}
	
}
