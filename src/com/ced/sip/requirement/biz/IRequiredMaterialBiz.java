package com.ced.sip.requirement.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.requirement.entity.RequiredMaterial;
import com.ced.sip.requirement.entity.RequiredMaterialDetail;
import com.ced.sip.system.entity.CategoryBuyer;
import com.ced.sip.system.entity.PurchaseDepartBuyer;

public interface IRequiredMaterialBiz {
	/**
	 * 根据主键获得需求计划表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract RequiredMaterial getRequiredMaterial(Long id) throws BaseException;

	/**
	 * 添加需求计划信息
	 * @param requiredMaterial 需求计划表实例
	 * @throws BaseException 
	 */
	abstract void saveRequiredMaterial(RequiredMaterial requiredMaterial) throws BaseException;

	/**
	 * 更新需求计划表实例
	 * @param requiredMaterial 需求计划表实例
	 * @throws BaseException 
	 */
	abstract void updateRequiredMaterial(RequiredMaterial requiredMaterial) throws BaseException;

	/**
	 * 删除需求计划表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredMaterial(String id) throws BaseException;

	/**
	 * 删除需求计划表实例
	 * @param requiredMaterial 需求计划表实例
	 * @throws BaseException 
	 */
	abstract void deleteRequiredMaterial(RequiredMaterial requiredMaterial) throws BaseException;

	/**
	 * 删除需求计划表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredMaterials(String[] id) throws BaseException;

	/**
	 * 获得所有需求计划表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialList( RollPage rollPage  ) throws BaseException ;
	
	/**
	 * 获得需求计划表数据集
	 * @param requiredMaterial 查询参数对象
	 * @author xuliang 2014-09-01
	 * @return
	 * @throws BaseException 
	 */
	abstract RequiredMaterial getRequiredMaterial( String rmCode ) throws BaseException;
	
	/**
	 * 获得所有需求计划表数据集
	 * @param requiredMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialList(  RequiredMaterial requiredMaterial) throws BaseException ;
	
	/**
	 * 获得所有需求计划表数据集
	 * @param requiredMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialList(  RequiredMaterial requiredMaterial ,String sqlstr) throws BaseException ;
	
	/**
	 * 获得所有需求计划表数据集
	 * @param rollPage 分页对象
	 * @param requiredMaterial 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialList(RollPage rollPage, RequiredMaterial requiredMaterial,String sqlstr)
			throws BaseException;
	
	/**
	 * 根据当前年月获取最大流水号
	 * @return
	 * @throws BaseException
	 */
	abstract int getMaxFloatCodeByRmCodePrefix(String rmCodePrefix,Long comId)throws BaseException;
	
	
	
	/**
	 * 根据主键获得需求计划明细表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract RequiredMaterialDetail getRequiredMaterialDetail(Long id) throws BaseException;

	/**
	 * 添加需求计划明细信息
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @throws BaseException 
	 */
	abstract void saveRequiredMaterialDetail(RequiredMaterialDetail requiredMaterialDetail) throws BaseException;

	/**
	 * 更新需求计划明细表实例
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @throws BaseException 
	 */
	abstract void updateRequiredMaterialDetail(RequiredMaterialDetail requiredMaterialDetail) throws BaseException;

	/**
	 * 删除需求计划明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredMaterialDetail(String id) throws BaseException;

	/**
	 * 删除需求计划明细表实例
	 * @param requiredMaterialDetail 需求计划明细表实例
	 * @throws BaseException 
	 */
	abstract void deleteRequiredMaterialDetail(RequiredMaterialDetail requiredMaterialDetail) throws BaseException;

	/**
	 * 删除需求计划明细表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteRequiredMaterialDetails(String[] id) throws BaseException;

	/**
	 * 获得所有需求计划明细表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialDetailList( RollPage rollPage  ) throws BaseException ;
	
	/**
	 * 获得所有需求计划明细表数据集
	 * @param rmIds 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialDetailListByRmIds( String rmIds) throws BaseException ;
	
	
	/**
	 * 获得所有需求计划明细表数据集
	 * @param requiredMaterialDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialDetailListByRmId(  Long rmId ) throws BaseException ;
	/**
	 * 获得所有需求计划明细表数据集
	 * @param requiredMaterialDetail 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialDetailList(  RequiredMaterialDetail requiredMaterialDetail ) throws BaseException ;
	
	/**
	 * 获得所有需求计划明细表数据集
	 * @param rollPage 分页对象
	 * @param requiredMaterialDetail requiredMaterial  计划追踪
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialDetailListForRequiredMaterialTrack(RollPage rollPage, RequiredMaterialDetail requiredMaterialDetail,RequiredMaterial requiredMaterial) throws BaseException;
	/**
	 * 获得所有需求计划明细表数据集
	 * @param rollPage 分页对象
	 * @param requiredMaterialDetail requiredMaterial 已采购数量小于计划数量  选择未汇总的采购计划
	 * @return
	 * @throws BaseException 
	 */
	abstract List getRequiredMaterialDetailListForRequiredCollect(RollPage rollPage, RequiredMaterialDetail requiredMaterialDetail,RequiredMaterial requiredMaterial,List<CategoryBuyer> listBuyers,List<PurchaseDepartBuyer> listpurchase) throws BaseException;
    /**
     * sign 0 ：当删除采购计划明细时候，需要同时更新需求计划明细中的已采购数量yamount,   根据rmdId和amount来操作 
     * sign 1 ：当新增采购计划明细时候，需要同时更新需求计划明细中的已采购数量yamount,   根据rmdId和amount来操作 
     * @param rcdId
     * @param amount
     * @throws BaseException
     */
	abstract void updateRequiredMaterialYamountByRcdId(Long rcdId,Double amount,int sign) throws BaseException;
	/**
	 * 根据项目Id查询需求计划数据集
	 * @param rcId
	 * @return
	 * @throws BaseException
	 */
	abstract List getDeptListByRcId(Long rcId) throws BaseException;
}
