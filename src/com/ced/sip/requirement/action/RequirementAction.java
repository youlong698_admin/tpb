package com.ced.sip.requirement.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryOrder;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;
import org.springframework.beans.factory.annotation.Autowired;
import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ExcelUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.requirement.biz.IRequiredMaterialBiz;
import com.ced.sip.requirement.entity.RequiredMaterial;
import com.ced.sip.requirement.entity.RequiredMaterialDetail;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;

public class RequirementAction extends BaseAction {
	@Autowired
	private SnakerEngineFacets facets;
	// 计划
	private IRequiredMaterialBiz iRequiredMaterialBiz;
	// 采购项目
	private IRequiredCollectBiz iRequiredCollectBiz;
	//跟踪日志
	private IPurchaseRecordLogBiz iPurchaseRecordLogBiz;
	// 附件上传服务类
	private IAttachmentBiz iAttachmentBiz;

	private RequiredMaterial requiredMaterial;
	private RequiredMaterialDetail requiredMaterialDetail;
	private List<RequiredMaterialDetail> rmdList;
	private PurchaseRecordLog purchaseRecordLog;

	
	private String ids;
	private Departments departments;

	/*********************************************************** 计划管理开始 **********************************************************/
	/**
	 * 查看计划信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewRequiredPlan() throws BaseException {

		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			this.getRequest().setAttribute("requiredMaterialWorkflow", systemConfiguration.getRequiredMaterialWorkflow());
			this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.RM_WorkFlow_Type);
		} catch (Exception e) {
			log.error("查看计划信息列表错误！", e);
			throw new BaseException("查看计划信息列表错误！", e);
		}

		return VIEW;
	}

	/**
	 * 查看计划信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String findRequiredPlan() throws BaseException {

		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);
			// 计划 列表信息
			if (requiredMaterial == null) {
				requiredMaterial = new RequiredMaterial();
			}
			String rmCode = this.getRequest().getParameter("rmCode");
			requiredMaterial.setRmCode(rmCode);
			String rmName = this.getRequest().getParameter("rmName");
			requiredMaterial.setRmName(rmName);
			String writeDateStart = this.getRequest().getParameter(
					"writeDateStart");
			String writeDateEnd = this.getRequest()
					.getParameter("writeDateEnd");
			if (StringUtil.isNotBlank(writeDateStart)) {
				requiredMaterial.setWriteDateStart(writeDateStart);
			}
			if (StringUtil.isNotBlank(writeDateEnd)) {
				requiredMaterial.setWriteDateEnd(writeDateEnd);
			}
			String writer = this.getRequest().getParameter("writer");
			requiredMaterial.setWriter(writer);
			String rmType = this.getRequest().getParameter("rmType");
			requiredMaterial.setRmType(rmType);
			String status = this.getRequest().getParameter("status");
			requiredMaterial.setStatus(status);
			requiredMaterial.setComId(comId);
			String sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(),
						"de");
			}
			this.setListValue(this.iRequiredMaterialBiz
					.getRequiredMaterialList(this.getRollPageDataTables(),
							requiredMaterial, sqlStr));
			if (this.getListValue() != null && this.getListValue().size() > 0) {
				for (int i = 0; i < this.getListValue().size(); i++) {
					requiredMaterial = (RequiredMaterial) this.getListValue()
							.get(i);
					requiredMaterial.setWriterCN(BaseDataInfosUtil
							.convertLoginNameToChnName(requiredMaterial
									.getWriter()));
					requiredMaterial.setPurchaseDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredMaterial
									.getPurchaseDeptId()));
					requiredMaterial.setDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredMaterial.getDeptId()));
					if(systemConfiguration.getRequiredMaterialWorkflow().equals("0")){
					  setProcessLisRm(requiredMaterial, WorkFlowStatus.RequiredPlan_Work_Item,comId);
					}else{
						requiredMaterial.setOrderState(requiredMaterial.getStatus());
					}
				}
			}
			this.getPagejsonDataTables(this.getListValue());
		} catch (Exception e) {
			log.error("查看计划信息列表错误！", e);
			throw new BaseException("查看计划信息列表错误！", e);
		}

		return null;

	}

	 /**
	 * 设置计划流程相关列表
	 */
	private void setProcessLisRm(RequiredMaterial rm, String processName,Long comId){
		//step1：首先获取流程定义实体
		org.snaker.engine.entity.Process process = facets.getEngine().process().getProcessByName(processName,comId);
		//step2：1、判断流程定义是否存在，2、遍历集合添加流程实例
		if(process != null){
				String newOrderNo=WorkFlowStatus.RM_WorkFlow_Type+rm.getRmId().toString();
				//step3:判断流程实例是否运行
				QueryFilter filter = new QueryFilter();
               filter.setOrderNo(newOrderNo);
				filter.setProcessId(process.getId());
				List<HistoryOrder> holist = facets.getEngine().query().getHistoryOrders(filter);
				if(holist.size() > 0){
					//step5:获取正在运行的流程实例
					filter.setOrderNo(newOrderNo);
					filter.setProcessId(process.getId());
					List<Order> orderlist = facets.getEngine().query().getActiveOrders(filter);
					if(orderlist.size() > 0){
						Order order = orderlist.get((orderlist.size()-1));
						if(order != null){
							List<Task> tasklist = facets.getEngine().query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
							if(tasklist.size() > 0){
								//设置当前流程名称
								rm.setProcessName(tasklist.get(0).getDisplayName());
							}
							rm.setOrderId(order.getId());//设置实例标示
						}
					}
					HistoryOrder ho = holist.get(0);
					rm.setOrderState(ho.getOrderState().toString());
					rm.setOrderStateName(WorkFlowStatusMap.getWorkflowOrderStatus(ho.getOrderState().toString()));
				}else{
					rm.setOrderStateName(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01_TEXT);
				}
				rm.setProcessId(process.getId());//设置流程标示
				rm.setInstanceUrl(process.getInstanceUrl());//设置流程实例URL
			
		}
	}
	/**
	 * 页面点击物资编码时的弹框
	 */
	public String viewMaterialInfoIndex() throws BaseException{
		try{
			
		} catch (Exception e) {
			log.error("查看计划信息列表错误！", e);
			throw new BaseException("查看计划信息列表错误！", e);
		}
		return INDEX;
	}
	/**
	 * 计划中查看年度采购计划信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewRequiredPlanPro() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("选择历史计划信息列表错误！", e);
			throw new BaseException("选择历史计划信息列表错误！", e);
		}
		
		return "viewRequiredPro" ;
		
	}
	
	/**
	 * 计划中查看历史计划信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findRequiredPlanPro() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看历史计划信息列表错误！", e);
			throw new BaseException("查看历史计划信息列表错误！", e);
		}
		
		return null;
		
	}
	/**
	 * 保存计划信息初始化
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String saveRequiredPlanInit() throws BaseException {
		String result = "";
		try {
			this.getRequest().setAttribute("now",DateUtil.getCurrentDateTime());
			departments=UserRightInfoUtil.getDepartments(this.getRequest());
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(departments.getPurchaseDeptId()));
			this.getRequest().setAttribute("purchaseDeptId",departments.getPurchaseDeptId());
			this.getRequest().setAttribute("ownPurchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(departments.getOwnPurchaseDeptId()));
			this.getRequest().setAttribute("ownPurchaseDeptId",departments.getOwnPurchaseDeptId());
			this.getRequest().setAttribute("writerCn",UserRightInfoUtil.getChineseName(this.getRequest()));
			this.getRequest().setAttribute("writer",UserRightInfoUtil.getUserName(this.getRequest()));
			this.getRequest().setAttribute("deptName",departments.getDeptName());
			this.getRequest().setAttribute("deptId",departments.getDepId());
			this.getRequest().setAttribute("requireType",TableStatusMap.rmType);
			this.getRequest().setAttribute("buyType",TableStatusMap.buyType);
			result = "addInit";
		} catch (Exception e) {
			log("保存计划信息初始化错误！", e);
			throw new BaseException("保存计划信息初始化错误！", e);
		}
		return result;

	}

	/**
	 * 保存计划信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String saveRequiredPlan() throws BaseException {
		try {
			Long comId=UserRightInfoUtil.getComId(getRequest());
			SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(comId);	
			if (requiredMaterial.getRmId() == null) {
				requiredMaterial.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String rmCodePrefix = systemConfiguration.getRequiredMaterialPrefix()+"-"+sdf.format(new Date());
				int floatCode = this.iRequiredMaterialBiz
						.getMaxFloatCodeByRmCodePrefix(rmCodePrefix,comId);
				requiredMaterial.setRmCode(systemConfiguration.getRequiredMaterialPrefix()+"-"+sdf.format(new Date())
						+"-"+ (new DecimalFormat("000").format(floatCode + 1)));
				requiredMaterial.setWriteDate(new Date());
				requiredMaterial.setFloatCode((long) floatCode + 1);
				requiredMaterial.setWriter(UserRightInfoUtil
						.getUserName(getRequest()));
				requiredMaterial.setComId(comId);
				iRequiredMaterialBiz.saveRequiredMaterial(requiredMaterial);
			} else {
				requiredMaterial.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_01);
				iRequiredMaterialBiz.updateRequiredMaterial(requiredMaterial);
				// 删除附件
				iAttachmentBiz
						.deleteAttachments(parseAttachIds(requiredMaterial
								.getAttIds()));
			}
			// 保存附件
			iAttachmentBiz.saveAttachmentAndUpload(this.setUploadFile(
					requiredMaterial, requiredMaterial.getRmId(),
					AttachmentStatus.ATTACHMENT_CODE_102, UserRightInfoUtil
							.getUserName(this.getRequest())));
			//删除老数据
			List<RequiredMaterialDetail> oldRmdList = this.iRequiredMaterialBiz.getRequiredMaterialDetailListByRmId(requiredMaterial.getRmId());
			for(RequiredMaterialDetail rmd:oldRmdList){
                this.iRequiredMaterialBiz.deleteRequiredMaterialDetail(rmd);
	         }
			// 保存新数据
			if (rmdList != null) {
				for (int i = 0; i < rmdList.size(); i++) {
					requiredMaterialDetail = rmdList.get(i);
					if (requiredMaterialDetail != null
							&& StringUtil.isNotBlank(requiredMaterialDetail.getBuyCode().trim())) {
						requiredMaterialDetail.setRmCode(requiredMaterial.getRmCode());
						requiredMaterialDetail.setMaterialType(requiredMaterialDetail.getMaterialType());
						requiredMaterialDetail.setRmId(requiredMaterial.getRmId());
						requiredMaterialDetail.setComId(comId);
						requiredMaterialDetail.setYamount(0.00);
						iRequiredMaterialBiz.saveRequiredMaterialDetail(requiredMaterialDetail);
					}
				}
			}

			this.getRequest().setAttribute("requiredMaterialWorkflow", systemConfiguration.getRequiredMaterialWorkflow());
			this.getRequest().setAttribute("work_flow_type",
					WorkFlowStatus.RM_WorkFlow_Type);
			this.getRequest().setAttribute("message", "保存成功");
			this.getRequest().setAttribute("operModule", "保存计划信息");

		} catch (Exception e) {
			log("保存计划信息错误！", e);
			throw new BaseException("保存计划信息错误！", e);
		}
		return "requiredPlanSuccess";

	}

	/**
	 * 修改计划初始化
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updateRequiredMaterialInit() throws BaseException {
		try {
			requiredMaterial = this.iRequiredMaterialBiz
					.getRequiredMaterial(requiredMaterial.getRmId());
			// 获取附件
			Map<String, Object> map = iAttachmentBiz
					.getAttachmentMap(new Attachment(
							requiredMaterial.getRmId(),
							AttachmentStatus.ATTACHMENT_CODE_102));
			requiredMaterial.setUuIdData((String) map.get("uuIdData"));
			requiredMaterial.setFileNameData((String) map.get("fileNameData"));
			requiredMaterial.setFileTypeData((String) map.get("fileTypeData"));
			requiredMaterial.setAttIdData((String) map.get("attIdData"));
			requiredMaterial.setAttIds((String) map.get("attIds"));

			departments=UserRightInfoUtil.getDepartments(this.getRequest());
			this.getRequest().setAttribute("purchaseDeptId",requiredMaterial.getPurchaseDeptId());
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredMaterial.getPurchaseDeptId()));
			this.getRequest().setAttribute("ownPurchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(departments.getOwnPurchaseDeptId()));
			this.getRequest().setAttribute("ownPurchaseDeptId",departments.getOwnPurchaseDeptId());			
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredMaterial.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredMaterial.getDeptId()));
			this.getRequest().setAttribute("requireType",TableStatusMap.rmType);
			this.getRequest().setAttribute("buyType",TableStatusMap.buyType);
			
			this.setListValue(this.iRequiredMaterialBiz
					.getRequiredMaterialDetailListByRmId(requiredMaterial
							.getRmId()));

		} catch (Exception e) {
			log("修改计划初始化错误！", e);
			throw new BaseException("修改计划初始化错误！", e);
		}
		return MODIFY_INIT;
	}

	/**
	 * 删除计划
	 */
	public String deleteRequiredMaterial() throws BaseException {
		try {
			ids = this.getRequest().getParameter("ids");
			// 页面所选汇总明细信息
			String[] idss = ids.split(",");
			for (int i = 0; i < idss.length; i++) {
				if (idss[i] != null) {
					RequiredMaterial rm = this.iRequiredMaterialBiz
							.getRequiredMaterial(new Long(idss[i]));
					requiredMaterial = rm;
					List rmdList = this.iRequiredMaterialBiz
							.getRequiredMaterialDetailListByRmId(requiredMaterial
									.getRmId());
					for (int j = 0; j < rmdList.size(); j++) {
						RequiredMaterialDetail rmd = (RequiredMaterialDetail) rmdList
								.get(j);

						this.iRequiredMaterialBiz
								.deleteRequiredMaterialDetail(rmd);

					}
					// 删除流程信息
					List<HistoryOrder> hoList = facets
							.getEngine()
							.query()
							.getHistoryOrders(
									new QueryFilter()
											.setOrderNo(WorkFlowStatus.RM_WorkFlow_Type
													+ requiredMaterial
															.getRmId()));
					for (HistoryOrder ho : hoList) {
						facets.getEngine().order().cascadeRemove(ho.getId());
					}
					this.iRequiredMaterialBiz.deleteRequiredMaterial(rm);
				}
			}
			PrintWriter out = this.getResponse().getWriter();
			String message = "删除成功";
			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除计划信息");
			out.print(message);
		} catch (Exception e) {
			log("删除计划错误！", e);
			throw new BaseException("删除计划错误！", e);
		}

		// return viewRequiredPlan();
		return null;
	}

	/**
	 * 查看计划明细信息
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String viewRequiredPlanDetail() throws BaseException {

		try {
			// 取计划信息
			requiredMaterial = iRequiredMaterialBiz
					.getRequiredMaterial(requiredMaterial.getRmId());
			requiredMaterial.setWriterCN(BaseDataInfosUtil
					.convertLoginNameToChnName(requiredMaterial.getWriter()));
			requiredMaterial.setRmTypeCn(BaseDataInfosUtil
					.convertPurchKindType(requiredMaterial.getRmType()));

			requiredMaterialDetail = new RequiredMaterialDetail();
			requiredMaterialDetail.setRmId(requiredMaterial.getRmId());
			this.setListValue(iRequiredMaterialBiz
					.getRequiredMaterialDetailList(requiredMaterialDetail));
			for (int i = 0; i < this.getListValue().size(); i++) {
				requiredMaterialDetail = (RequiredMaterialDetail) this
						.getListValue().get(i);
				requiredMaterialDetail.setEstimatePriceStr(StringUtil
						.formatNumbertoEnglishForm(requiredMaterialDetail
								.getEstimatePrice()));
			}
			// 获取附件
			requiredMaterial.setAttachmentUrl(iAttachmentBiz
					.getAttachmentPageUrl(
							iAttachmentBiz.getAttachmentList(new Attachment(
									requiredMaterial.getRmId(),
									AttachmentStatus.ATTACHMENT_CODE_102)), "0",
							this.getRequest()));
			this.getRequest().setAttribute("purchaseDeptName",BaseDataInfosUtil.convertDeptIdToName(requiredMaterial.getPurchaseDeptId()));
			this.getRequest().setAttribute("writerCn",BaseDataInfosUtil.convertLoginNameToChnName(requiredMaterial.getWriter()));
			this.getRequest().setAttribute("deptName",BaseDataInfosUtil.convertDeptIdToName(requiredMaterial.getDeptId()));
			this.getRequest().setAttribute("requireTypeCn",BaseDataInfosUtil.convertPurchKindType(requiredMaterial.getRmType()));			

		} catch (Exception e) {
			log("查看计划明细信息错误！", e);
			throw new BaseException("查看计划明细信息错误！", e);
		}
		return DETAIL;

	}
	/**
	 * 需求计划不需要审批的时候提交正式计划
	 * @return
	 * @throws BaseException
	 */
    public String updateProcessRequiredMaterial() throws BaseException{
    	
    	try {
			ids = this.getRequest().getParameter("ids");
			// 页面所选汇总明细信息
			String[] idss = ids.split(",");
			for (int i = 0; i < idss.length; i++) {
				if (idss[i] != null) {
					requiredMaterial = this.iRequiredMaterialBiz
							.getRequiredMaterial(new Long(idss[i]));
					requiredMaterial.setStatus(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0);
					requiredMaterial.setAuditDate(DateUtil.getCurrentDateTime());
   					this.iRequiredMaterialBiz.updateRequiredMaterial(requiredMaterial);
				}
			}
			PrintWriter out = this.getResponse().getWriter();
			String message = "提交成功";
			this.getRequest().setAttribute("message", "提交成功");
			this.getRequest().setAttribute("operModule", "提交正式计划信息");
			out.print(message);
		} catch (Exception e) {
			log("提交正式计划信息错误！", e);
			throw new BaseException("提交正式计划信息错误！", e);
		}
		return null;
    }
	/**
	 * 计划信息Excel导出
	 * 
	 * @return
	 * @throws BaseException
	 */
	public void exportRequiredMaterialExcel() throws BaseException {
		try {
			List<String> titleList = new ArrayList<String>();
			titleList.add("采购组织");
			titleList.add("计划编码");
			titleList.add("计划名称");
			titleList.add("填报人");
			titleList.add("填报部门");
			titleList.add("填报日期");
			titleList.add("计划类型");

			String sqlStr = "";
			String rmIds = "";

			sqlStr = UserRightInfoUtil.getUserDepartNameHql(getRequest(), "de");

			if (requiredMaterial == null) {
				requiredMaterial = new RequiredMaterial();
			}

			String rmCode = this.getRequest().getParameter("rmCode");
			requiredMaterial.setRmCode(rmCode);
			String rmName = this.getRequest().getParameter("rmName");
			requiredMaterial.setRmName(rmName);
			String writeDateStart = this.getRequest().getParameter(
			"writeDateStart");
			String writeDateEnd = this.getRequest()
					.getParameter("writeDateEnd");
			if (StringUtil.isNotBlank(writeDateStart)) {
				requiredMaterial.setWriteDateStart(writeDateStart);
			}
			if (StringUtil.isNotBlank(writeDateEnd)) {
				requiredMaterial.setWriteDateEnd(writeDateEnd);
			}
			String writer = this.getRequest().getParameter("writer");
			requiredMaterial.setWriter(writer);
			String rmType = this.getRequest().getParameter("rmType");
			requiredMaterial.setRmType(rmType);
			String status = this.getRequest().getParameter("status");
			requiredMaterial.setStatus(status);

			List<RequiredMaterial> rmList = this.iRequiredMaterialBiz
					.getRequiredMaterialList(requiredMaterial, sqlStr);
			List<Object[]> objList = new ArrayList<Object[]>();
			for (int i = 0; i < rmList.size(); i++) {
				RequiredMaterial requiredMaterial = (RequiredMaterial) rmList
						.get(i);
				if (StringUtil.isNotBlank(requiredMaterial)) {
					rmIds += "'" + requiredMaterial.getRmId() + "',";
					requiredMaterial.setPurchaseDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredMaterial
									.getPurchaseDeptId()));
					requiredMaterial.setWriterCN(BaseDataInfosUtil
							.convertLoginNameToChnName(requiredMaterial
									.getWriter()));
					requiredMaterial.setDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredMaterial.getDeptId()));
					requiredMaterial
							.setRmTypeCn(BaseDataInfosUtil
									.convertPurchKindType(requiredMaterial
											.getRmType()));

					Object[] obj = new Object[] {
							requiredMaterial.getPurchaseDeptName(),
							requiredMaterial.getRmCode(),
							requiredMaterial.getRmName(),
							requiredMaterial.getWriterCN(),
							requiredMaterial.getDeptName(),
							requiredMaterial.getWriteDate(),
							requiredMaterial.getRmTypeCn()};
					objList.add(obj);
				}
			}

			List<String> titleList2 = new ArrayList<String>();
			titleList2.add("计划编码");
			titleList2.add("编码");
			titleList2.add("名称");
			titleList2.add("规格型号");
			titleList2.add("单位");
			titleList2.add("采购数量");
			titleList2.add("估算单价");
			titleList2.add("交货时间");
			titleList2.add("交货地点");
			titleList2.add("备注");

			List<RequiredMaterialDetail> proList2 = this.iRequiredMaterialBiz
					.getRequiredMaterialDetailListByRmIds(rmIds.substring(0,
							rmIds.lastIndexOf(",")));
			List<Object[]> objList2 = new ArrayList<Object[]>();
			for (int i = 0; i < proList2.size(); i++) {
				RequiredMaterialDetail requiredMaterialDetail = (RequiredMaterialDetail) proList2
						.get(i);
				if (StringUtil.isNotBlank(requiredMaterialDetail)) {
					Object[] obj2 = new Object[] {
							requiredMaterialDetail.getRmCode(),
							requiredMaterialDetail.getBuyCode(),
							requiredMaterialDetail.getBuyName(),
							requiredMaterialDetail.getMaterialType(),
							requiredMaterialDetail.getUnit(),
							requiredMaterialDetail.getAmount(),
							requiredMaterialDetail.getEstimatePrice(),
							requiredMaterialDetail.getDeliverDate(),
							requiredMaterialDetail.getDeliverPlace(),
							requiredMaterialDetail.getRemark()

					};
					objList2.add(obj2);
				}
			}

			// 输出的excel文件名
			String file = "计划信息.xls";
			String targetfile = this.getServletContext().getRealPath(file);
			// 输出的excel文件工作表名
			List<Map<String, Object>> excelList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("worksheet", "计划信息");
			map.put("titleList", titleList);
			map.put("valueList", objList);
			excelList.add(map);

			map = new HashMap<String, Object>();
			map.put("worksheet", "计划明细");
			map.put("titleList", titleList2);
			map.put("valueList", objList2);
			excelList.add(map);

			new ExcelUtil().expCommonExcel(targetfile, excelList);
			this.getResponse().setContentType(
					"application/octet-stream; charset=utf-8");
			this.getResponse().setHeader(
					"Content-Disposition",
					"attachment; filename="
							+ new String(file.getBytes("gbk"), "iso-8859-1"));
			// this.getResponse().setHeader( "Set-Cookie",
			// "name=value; HttpOnly");
			File files = new File(targetfile);
			FileInputStream is = new FileInputStream(files);
			OutputStream os = this.getResponse().getOutputStream();
			byte[] buff = new byte[1024];
			int readCount = 0;
			readCount = is.read(buff);
			while (readCount != -1) {
				os.write(buff, 0, readCount);
				readCount = is.read(buff);
			}
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			log.error("导出供应商查询信息列表错误！", e);
			throw new BaseException("导出采购进度查询信息列表错误！", e);
		}
	}

	
	/**
	 * 查看计划追踪页面的信息
	 * @return
	 */
	public String viewRequiredPlanTrack()throws BaseException{
		
		try{
			
		} catch (Exception e) {
			log("查看计划追踪页面信息错误！", e);
			throw new BaseException("查看计划追踪页面信息错误！", e);
		}
		
		return "requiredPlanTrack";
	}
	
	/**
	 * 选择未汇总的信息
	 * @return
	 */
	public String findRequiredPlanTrack() throws BaseException{
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(requiredMaterialDetail == null){
				requiredMaterialDetail = new RequiredMaterialDetail();
			}
			if(requiredMaterial == null){
				requiredMaterial = new RequiredMaterial();
			}
			requiredMaterial.setComId(comId);
			String rmName=this.getRequest().getParameter("rmName");
			requiredMaterial.setRmName(rmName);
			String rmType=this.getRequest().getParameter("rmType");
			requiredMaterial.setRmType(rmType);
			String rmCode=this.getRequest().getParameter("rmCode");
			requiredMaterialDetail.setRmCode(rmCode);
			String buyName=this.getRequest().getParameter("buyName");
			requiredMaterialDetail.setBuyName(buyName);
			String buyCode=this.getRequest().getParameter("buyCode");
			requiredMaterialDetail.setBuyCode(buyCode);
			
			List<RequiredMaterialDetail> rmdList = iRequiredMaterialBiz.getRequiredMaterialDetailListForRequiredMaterialTrack(this.getRollPageDataTables(),requiredMaterialDetail,requiredMaterial);
			if(rmdList!=null&&rmdList.size()>0){
				for(RequiredMaterialDetail rmd:rmdList){
					requiredMaterial = this.iRequiredMaterialBiz.getRequiredMaterial(rmd.getRmId());
					rmd.setRmType(requiredMaterial.getRmType());
					rmd.setRmName(requiredMaterial.getRmName());
					rmd.setDeptName(BaseDataInfosUtil
							.convertDeptIdToName(requiredMaterial.getDeptId()));
				}
			}
			this.getPagejsonDataTables(rmdList);
		} catch (Exception e) {
			log("查看计划追踪信息错误！", e);
			throw new BaseException("查看计划追踪信息错误！", e);
		}
		return null;
	}

    
    /**
	 * 查看流程跟踪信息（需求方）
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String followAndViewProcessPlan() throws BaseException {
		
		try{
			// 流程列表信息
			Long rmId = Long.parseLong(this.getRequest().getParameter("param"));
			String type = this.getRequest().getParameter("type");
			purchaseRecordLog = new PurchaseRecordLog();
			purchaseRecordLog.setRmId(rmId);
			List listPurListAll=new ArrayList();
			List listPurList=this.iPurchaseRecordLogBiz.getPurchaseRecordLogList(purchaseRecordLog);
			if(listPurList!=null&&listPurList.size()>0){
				for(int i=0;i<listPurList.size();i++){
					purchaseRecordLog = (PurchaseRecordLog)listPurList.get(i);
					purchaseRecordLog.setOperatorNameCn(BaseDataInfosUtil.convertLoginNameToChnName(purchaseRecordLog.getOperatorName()));
					listPurListAll.add(purchaseRecordLog);
				}
			}
			List<RequiredCollect> list=this.iRequiredCollectBiz.getRequiredCollecListByRmId(rmId);
			String condtion=" and (de.rcId='1' ";
			for(RequiredCollect rc:list){
				condtion+="or de.rcId='"+rc.getRcId()+"'";
			}
			condtion+=")";
			listPurList=this.iPurchaseRecordLogBiz.getPurchaseRecordLogListByContion(condtion);
			if(listPurList!=null&&listPurList.size()>0){
				for(int i=0;i<listPurList.size();i++){
					purchaseRecordLog = (PurchaseRecordLog) listPurList.get(i);
					purchaseRecordLog.setOperatorNameCn(BaseDataInfosUtil.convertLoginNameToChnName(purchaseRecordLog.getOperatorName()));
					listPurListAll.add(purchaseRecordLog);
				}
			}
			this.setListValue(listPurListAll);
			if(listPurListAll.size()>0){
			   purchaseRecordLog=(PurchaseRecordLog)listPurListAll.get(listPurListAll.size()-1);
			}else{
				purchaseRecordLog=new PurchaseRecordLog();
			}
			this.getRequest().setAttribute("type", type);
		} catch (Exception e) {
			log.error("查看流程跟踪信息错误！", e);
			throw new BaseException("查看流程跟踪信息错误！", e);
		}
		
		return "viewProPlan" ;
		
	}
	/*********************************************************** 计划管理结束 **********************************************************/

	
	
	public SnakerEngineFacets getFacets() {
		return facets;
	}

	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}

	public IRequiredMaterialBiz getiRequiredMaterialBiz() {
		return iRequiredMaterialBiz;
	}

	public void setiRequiredMaterialBiz(IRequiredMaterialBiz iRequiredMaterialBiz) {
		this.iRequiredMaterialBiz = iRequiredMaterialBiz;
	}

	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}

	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}

	public RequiredMaterial getRequiredMaterial() {
		return requiredMaterial;
	}

	public void setRequiredMaterial(RequiredMaterial requiredMaterial) {
		this.requiredMaterial = requiredMaterial;
	}

	public RequiredMaterialDetail getRequiredMaterialDetail() {
		return requiredMaterialDetail;
	}

	public void setRequiredMaterialDetail(
			RequiredMaterialDetail requiredMaterialDetail) {
		this.requiredMaterialDetail = requiredMaterialDetail;
	}

	public List<RequiredMaterialDetail> getRmdList() {
		return rmdList;
	}

	public void setRmdList(List<RequiredMaterialDetail> rmdList) {
		this.rmdList = rmdList;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}

	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}

	public IPurchaseRecordLogBiz getiPurchaseRecordLogBiz() {
		return iPurchaseRecordLogBiz;
	}

	public void setiPurchaseRecordLogBiz(IPurchaseRecordLogBiz iPurchaseRecordLogBiz) {
		this.iPurchaseRecordLogBiz = iPurchaseRecordLogBiz;
	}
	
}
