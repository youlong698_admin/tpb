package com.ced.sip.requirement.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;


public class RequiredMaterial extends BaseObject implements java.io.Serializable {

	// Fields

	private Long rmId;
	private String rmCode;
	private String rmName;
	private String rmType;
	private String buyType;
	private Double totalBudget;
	private String writer;
	private Date writeDate;
	private Date auditDate;
	private String status;
	private Long floatCode;
	private Long deptId;
	private Long purchaseDeptId;//采购组织
	private Long comId;
	
	private String orderId;//流程实例标示
	private String processId;//流程定义标示
	private String processName;//流程定义名称
	private String orderState;//流程状态
	private String orderStateName;//流程状态名称
	private String instanceUrl;//实例启动页面
	private String deptName;
	private String purchaseDeptName;
	private String rmTypeCn;
	private String writerCN;
	private String writeDateStart;//开始时间
	private String writeDateEnd;//结束时间
	
	public Long getRmId() {
		return rmId;
	}
	public void setRmId(Long rmId) {
		this.rmId = rmId;
	}
	public String getRmCode() {
		return rmCode;
	}
	public void setRmCode(String rmCode) {
		this.rmCode = rmCode;
	}
	public String getRmName() {
		return rmName;
	}
	public void setRmName(String rmName) {
		this.rmName = rmName;
	}
	public String getRmType() {
		return rmType;
	}
	public void setRmType(String rmType) {
		this.rmType = rmType;
	}
	public String getRmTypeCn() {
		return rmTypeCn;
	}
	public void setRmTypeCn(String rmTypeCn) {
		this.rmTypeCn = rmTypeCn;
	}
	public Double getTotalBudget() {
		return totalBudget;
	}
	public void setTotalBudget(Double totalBudget) {
		this.totalBudget = totalBudget;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getWriterCN() {
		return writerCN;
	}
	public void setWriterCN(String writerCN) {
		this.writerCN = writerCN;
	}
	public Date getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}
	public String getWriteDateStart() {
		return writeDateStart;
	}
	public void setWriteDateStart(String writeDateStart) {
		this.writeDateStart = writeDateStart;
	}
	public String getWriteDateEnd() {
		return writeDateEnd;
	}
	public void setWriteDateEnd(String writeDateEnd) {
		this.writeDateEnd = writeDateEnd;
	}
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getFloatCode() {
		return floatCode;
	}
	public void setFloatCode(Long floatCode) {
		this.floatCode = floatCode;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Long getPurchaseDeptId() {
		return purchaseDeptId;
	}
	public void setPurchaseDeptId(Long purchaseDeptId) {
		this.purchaseDeptId = purchaseDeptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getPurchaseDeptName() {
		return purchaseDeptName;
	}
	public void setPurchaseDeptName(String purchaseDeptName) {
		this.purchaseDeptName = purchaseDeptName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getOrderState() {
		return orderState;
	}
	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	public String getOrderStateName() {
		return orderStateName;
	}
	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}
	public String getInstanceUrl() {
		return instanceUrl;
	}
	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	public String getBuyType() {
		return buyType;
	}
	public void setBuyType(String buyType) {
		this.buyType = buyType;
	}

}