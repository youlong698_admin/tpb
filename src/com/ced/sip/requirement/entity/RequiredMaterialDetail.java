package com.ced.sip.requirement.entity;

import java.util.Date;

import com.ced.base.entity.BaseObject;

/**
 * RequiredMaterialDetail entity. @author MyEclipse Persistence Tools
 */

public class RequiredMaterialDetail extends BaseObject implements java.io.Serializable {

	// Fields

	private Long rmdId;
	private Long rmId; 
	private String rmCode;
	private String buyCode;
	private String buyName;
	private String materialType;
	private String unit;
	private Double amount;
	private Double estimatePrice;
	private String estimatePriceStr;
	private Double estimateSumPrice;
	private Double newPrice;
	private Date deliverDate;
	private String deliverPlace;
	private String remark;
	private String remark1;
	private String remark2;
	private String remark3;
	private String remark4;
	private Double yamount;
	
	private Long comId;
	private String mnemonicCode;
	private Long materialId;     //采购对象ID
	
	// Constructors
	private String rmName;
	private String rmType;
	private String deptName;

	/** default constructor */
	public RequiredMaterialDetail() {
	}

	public Long getRmdId() {
		return rmdId;
	}

	public void setRmdId(Long rmdId) {
		this.rmdId = rmdId;
	}

	public Long getRmId() {
		return rmId;
	}

	public void setRmId(Long rmId) {
		this.rmId = rmId;
	}

	public String getRmCode() {
		return rmCode;
	}

	public void setRmCode(String rmCode) {
		this.rmCode = rmCode;
	}

	public String getBuyCode() {
		return buyCode;
	}

	public void setBuyCode(String buyCode) {
		this.buyCode = buyCode;
	}

	public String getBuyName() {
		return buyName;
	}

	public void setBuyName(String buyName) {
		this.buyName = buyName;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getEstimatePrice() {
		return estimatePrice;
	}

	public void setEstimatePrice(Double estimatePrice) {
		this.estimatePrice = estimatePrice;
	}

	public String getEstimatePriceStr() {
		return estimatePriceStr;
	}

	public void setEstimatePriceStr(String estimatePriceStr) {
		this.estimatePriceStr = estimatePriceStr;
	}

	public Double getNewPrice() {
		return newPrice;
	}

	public void setNewPrice(Double newPrice) {
		this.newPrice = newPrice;
	}

	public Date getDeliverDate() {
		return deliverDate;
	}

	public void setDeliverDate(Date deliverDate) {
		this.deliverDate = deliverDate;
	}

	public String getDeliverPlace() {
		return deliverPlace;
	}

	public void setDeliverPlace(String deliverPlace) {
		this.deliverPlace = deliverPlace;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public String getRemark4() {
		return remark4;
	}

	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}

	public String getRmName() {
		return rmName;
	}

	public void setRmName(String rmName) {
		this.rmName = rmName;
	}

	public String getRmType() {
		return rmType;
	}

	public void setRmType(String rmType) {
		this.rmType = rmType;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Double getYamount() {
		return yamount;
	}

	public void setYamount(Double yamount) {
		this.yamount = yamount;
	}

	public Long getComId() {
		return comId;
	}

	public void setComId(Long comId) {
		this.comId = comId;
	}

	public String getMnemonicCode() {
		return mnemonicCode;
	}

	public void setMnemonicCode(String mnemonicCode) {
		this.mnemonicCode = mnemonicCode;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public Double getEstimateSumPrice() {
		return estimateSumPrice;
	}

	public void setEstimateSumPrice(Double estimateSumPrice) {
		this.estimateSumPrice = estimateSumPrice;
	}
   	
}