package com.ced.sip.evaluate.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.PasswordUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.expert.biz.IExpertBiz;
import com.ced.sip.expert.entity.Expert;
import com.ced.sip.purchase.tender.biz.ITenderBidItemBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidJudgeTypeBiz;
import com.ced.sip.purchase.tender.biz.ITenderBidListBiz;
import com.ced.sip.purchase.tender.biz.ITenderJudgeBusScoreBiz;
import com.ced.sip.purchase.tender.biz.ITenderJudgeTechScoreBiz;
import com.ced.sip.purchase.tender.biz.ITenderReceivedBulletinBiz;
import com.ced.sip.purchase.tender.entity.TenderBidItem;
import com.ced.sip.purchase.tender.entity.TenderBidJudge;
import com.ced.sip.purchase.tender.entity.TenderBidJudgeType;
import com.ced.sip.purchase.tender.entity.TenderJudgeBusScore;
import com.ced.sip.purchase.tender.entity.TenderJudgeTechScore;
import com.ced.sip.purchase.tender.entity.TenderReceivedBulletin;

public class EvaluateAction extends BaseAction {
	//招标计划
	private ITenderBidListBiz iTenderBidListBiz;
    //专家服务类
	private IExpertBiz iExpertBiz;
	//评标专家服务类
	private ITenderBidJudgeBiz iTenderBidJudgeBiz;
	//评委评标方式
	private ITenderBidJudgeTypeBiz iTenderBidJudgeTypeBiz;
	//评标项服务类
	private ITenderBidItemBiz iTenderBidItemBiz;
	//回标表
	private ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz;
	//技术评分汇总
	private ITenderJudgeTechScoreBiz iTenderJudgeTechScoreBiz;
	//商务评分汇总
	private ITenderJudgeBusScoreBiz iTenderJudgeBusScoreBiz;
	
	private Expert expert;
	
	private TenderBidJudge tenderBidJudge;
	
	private TenderBidJudgeType tenderBidJudgeType;
	
	private TenderBidItem tenderBidItem;
	
	private TenderReceivedBulletin tenderReceivedBulletin;

	//回标供应商 集合
	private List<TenderReceivedBulletin> receivedBulletinList;
	//评标项目集合
	private List<TenderBidItem> tenderBidItemList;
	//商务评分表 集合
	private List<TenderJudgeBusScore> judgeBusScoreList;
	//技术评分表 集合
	private List<TenderJudgeTechScore> judgeTechScoreList;

	
	/**
	 * 查看专家明细信息
	 * Ushine 2014-11-11
	 * @return
	 * @throws BaseException
	 */
	public String viewExpertDetail() throws BaseException {
		
		try{
			expert = this.iExpertBiz.getExpert(UserRightInfoUtil.getExpertId(this.getRequest()));
			expert.setDegreeCn(BaseDataInfosUtil.convertDictCodeToName(expert.getDegree(), DictStatus.COMMON_DICT_TYPE_1706));
			 this.getRequest().setAttribute("expert", expert);
		} catch (Exception e) {
			log("查看专家明细信息错误！", e);
			throw new BaseException("查看专家明细信息错误！", e);
		}
		return DETAIL;
		
	}
	/**
	 * 正在评标的项目
	 * @return
	 * @throws ServletException
	 */
	public String viewIsTenderBidsJudger() throws BaseException {
		try {

			Map judgeType = TableStatusMap.judgeType;
			List<TenderBidJudgeType> bidJudgeTypes;
			
			Long expertId=UserRightInfoUtil.getExpertId(this.getRequest());
			List<TenderBidJudge> bidJudgeList=new ArrayList<TenderBidJudge>();
			List<Object[]> list=this.iTenderBidJudgeBiz.getIsTenderBidJudgeList(expertId);
			for(Object[] obj:list){
				tenderBidJudge = (TenderBidJudge)obj[0];
				tenderBidJudge.setBuyRemark((String)obj[1]);
				tenderBidJudge.setBidCode((String)obj[2]);
				tenderBidJudge.setOpenDate((Date)obj[3]);
				if(StringUtil.isNotBlank(tenderBidJudge.getExpertSign()))
				{
					bidJudgeTypes=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeTypeListByTbjId(tenderBidJudge.getTbjId());
					tenderBidJudge.setBidJudgeTypes(bidJudgeTypes);
				}
				tenderBidJudge.setOpenStatus(this.iTenderBidListBiz.getTenderBidListByRcId(tenderBidJudge.getRcId()).getOpenStatus());
				bidJudgeList.add(tenderBidJudge);
			}

			this.getRequest().setAttribute("judgeType", judgeType);
			this.getRequest().setAttribute("bidJudgeList", bidJudgeList);
			
		} catch (Exception e) {
			log.error("查看评委可以评标的项目错误！", e);
			throw new BaseException("查看评委可以评标的项目错误！", e);
		}
		return "isTenderBidsJudger";
	}
	/**
	 * 历史评标的项目
	 * @return
	 * @throws ServletException
	 */
	public String viewHistoryTenderBidsJudger() throws BaseException {
		try {

			Map judgeType = TableStatusMap.judgeType;
			String bidCode=this.getRequest().getParameter("bidCode");
			String buyRemark=this.getRequest().getParameter("buyRemark");
			List<TenderBidJudgeType> bidJudgeTypes;
			
			Long expertId=UserRightInfoUtil.getExpertId(this.getRequest());
			List<TenderBidJudge> bidJudgeList=new ArrayList<TenderBidJudge>();
			List<Object[]> list=this.iTenderBidJudgeBiz.getHistoryTenderBidJudgeList(expertId,bidCode,buyRemark);
			for(Object[] obj:list){
				tenderBidJudge = (TenderBidJudge)obj[0];
				tenderBidJudge.setBuyRemark((String)obj[1]);
				tenderBidJudge.setBidCode((String)obj[2]);
				tenderBidJudge.setOpenDate((Date)obj[3]);
				if(StringUtil.isNotBlank(tenderBidJudge.getExpertSign()))
				{
					bidJudgeTypes=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeTypeListByTbjId(tenderBidJudge.getTbjId());
					tenderBidJudge.setBidJudgeTypes(bidJudgeTypes);
				}
				bidJudgeList.add(tenderBidJudge);
			}

			this.getRequest().setAttribute("judgeType", judgeType);
			this.getRequest().setAttribute("bidJudgeList", bidJudgeList);
			
		} catch (Exception e) {
			log.error("查看历史评标的项目错误！", e);
			throw new BaseException("查看历史可以评标的项目错误！", e);
		}
		return "historyTenderBidsJudger";
	}
	/**
	 * 专家签到
	 * @return
	 * @throws BaseException
	 */
	public String signTenderBidJudge() throws BaseException{
		String result="签到失败";
		PrintWriter out= null;
		try{ 
			out = getResponse().getWriter(); 
			Long tbjId=Long.parseLong(this.getRequest().getParameter("tbjId"));
			tenderBidJudge=this.iTenderBidJudgeBiz.getTenderBidJudge(tbjId);
			tenderBidJudge.setExpertSign(TableStatus.COMMON_0);
			tenderBidJudge.setExpertSignDate(DateUtil.getCurrentDateTime());
			this.iTenderBidJudgeBiz.updateTenderBidJudge(tenderBidJudge);
			result="签到成功"; 
			this.getRequest().setAttribute("operModule", "评标专家签到");
			this.getRequest().setAttribute("message", result);
			out.print(result);  
		} catch (Exception e) {
			log.error("专家签到错误！", e);
			out.print(result); 
		}
		return null;
	}
	/**
	 * 保存评委评标信息初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveTechOrBusJudgerBidsInit() throws BaseException {
		
		try{
			Long tbjtId=Long.parseLong(this.getRequest().getParameter("tbjtId"));
			tenderBidJudgeType=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeType(tbjtId);
			tenderBidItem = new TenderBidItem();
			tenderBidItem.setRcId(tenderBidJudgeType.getRcId());
			tenderBidItem.setItemType(tenderBidJudgeType.getTableName());
			tenderBidItemList = this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem);
			
			tenderReceivedBulletin = new TenderReceivedBulletin();
			tenderReceivedBulletin.setRcId(tenderBidJudgeType.getRcId());
			receivedBulletinList = this.iTenderReceivedBulletinBiz.getTenderReceivedBulletinList(tenderReceivedBulletin);

            this.getRequest().setAttribute("tenderBidItemList", tenderBidItemList);
            this.getRequest().setAttribute("receivedBulletinList", receivedBulletinList);
            this.getRequest().setAttribute("tenderBidJudgeType", tenderBidJudgeType);
            this.getRequest().setAttribute("expertName", UserRightInfoUtil.getExpertName(this.getRequest()));
            this.getRequest().setAttribute("date", DateUtil.getCurrentDateTime());
			
		} catch (Exception e) {
			log.error("保存评委评标初始化技术评分信息错误！", e);
			throw new BaseException("保存评委评标初始化评标技术评分信息错误！", e);
		}
		
		return "judgeBidsInit" ;
		
	}
	
	/**
	 * 保存评委评标信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String saveTechOrBusJudgerBids() throws BaseException {
		
		try{
			Long tbjtId=Long.parseLong(this.getRequest().getParameter("tbjtId"));
			String param=this.getRequest().getParameter("param");
			tenderBidJudgeType=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeType(tbjtId);
			
			if(receivedBulletinList!=null&&receivedBulletinList.size()>0){
				if(StringUtil.isNotBlank(tenderBidJudgeType.getTableName())){
					if("0".equals(tenderBidJudgeType.getTableName())){
						TenderJudgeTechScore jTechScore=null;
						//保存技术评标信息
						for(TenderReceivedBulletin receivedBulletin:receivedBulletinList){
							if(receivedBulletin!=null){
								for(TenderJudgeTechScore jtScore:judgeTechScoreList){
									if(jtScore!=null){
										jTechScore = new TenderJudgeTechScore();
										jTechScore.setRcId(tenderBidJudgeType.getRcId());
										jTechScore.setExpertId(tenderBidJudgeType.getExpertId());
										jTechScore.setSupplierId(receivedBulletin.getSupplierId());
										jTechScore.setSupplierName(receivedBulletin.getSupplierName());
										if(StringUtil.isNotBlank(receivedBulletin.getJudgePoints())){
											String[] jdPoints = receivedBulletin.getJudgePoints().split(",");
											if(StringUtil.isNotBlank(jdPoints[judgeTechScoreList.indexOf(jtScore)].trim())){
												jTechScore.setJudgePoints(Double.parseDouble(jdPoints[judgeTechScoreList.indexOf(jtScore)].trim()));
											}else{
												jTechScore.setJudgePoints(0.00);
											}
										}
										if(StringUtil.isNotBlank(receivedBulletin.getSupPoints())){
											jTechScore.setSupPoints(Double.parseDouble(receivedBulletin.getSupPoints()));
										}else{
											jTechScore.setSupPoints(0.00);
										}
										jTechScore.setScoreTime(DateUtil.getCurrentDateTime());
										jTechScore.setStatus(param);
										//保存技术评分项
										jTechScore.setItemId(jtScore.getItemId());
										jTechScore.setDispNumber(jtScore.getDispNumber());
										this.iTenderJudgeTechScoreBiz.saveTenderJudgeTechScore(jTechScore);
									}
								}
							}
						}
						//更新评委标段表信息
						tenderBidJudgeType.setTableStatu(param);
						if(param.equals("1")) tenderBidJudgeType.setTableSubmitTime(DateUtil.getCurrentDateTime());
						this.iTenderBidJudgeTypeBiz.updateTenderBidJudgeType(tenderBidJudgeType);
					}else{
						TenderJudgeBusScore jBusScore =null;
						//保存商务评标信息
						for(TenderReceivedBulletin receivedBulletin:receivedBulletinList){
							if(receivedBulletin!=null){
								for(TenderJudgeBusScore jbScore:judgeBusScoreList){
									if(jbScore!=null){
										jBusScore = new TenderJudgeBusScore();
										jBusScore.setRcId(tenderBidJudgeType.getRcId());
										jBusScore.setExpertId(tenderBidJudgeType.getExpertId());
										jBusScore.setSupplierId(receivedBulletin.getSupplierId());
										jBusScore.setSupplierName(receivedBulletin.getSupplierName());
										if(StringUtil.isNotBlank(receivedBulletin.getJudgePoints())){
											String[] jdPoints = receivedBulletin.getJudgePoints().split(",");
											if(StringUtil.isNotBlank(jdPoints[judgeBusScoreList.indexOf(jbScore)].trim())){
												jBusScore.setJudgePoints(Double.parseDouble(jdPoints[judgeBusScoreList.indexOf(jbScore)].trim()));
											}else{
												jBusScore.setJudgePoints(0.00);
											}
										}
										if(StringUtil.isNotBlank(receivedBulletin.getSupPoints().trim())){
											jBusScore.setSupPoints(Double.parseDouble(receivedBulletin.getSupPoints()));
										}else{
											jBusScore.setSupPoints(0.00);
										}
										jBusScore.setScoreTime(DateUtil.getCurrentDateTime());
										jBusScore.setStatus(param);
										//保存商务评分项
										jBusScore.setItemId(jbScore.getItemId());
										jBusScore.setDispNumber(jbScore.getDispNumber());
										this.iTenderJudgeBusScoreBiz.saveTenderJudgeBusScore(jBusScore);
									}
								}
							}
						}
						//更新评委标段表信息
						tenderBidJudgeType.setTableStatu(param);
						if(param.equals("1")) tenderBidJudgeType.setTableSubmitTime(DateUtil.getCurrentDateTime());
						this.iTenderBidJudgeTypeBiz.updateTenderBidJudgeType(tenderBidJudgeType);
					}
				}
			}
			//更新TenderBidList 表中的评标状态
			if(param.equals("1")) this.iTenderBidListBiz.updateCalibtationStatus(tenderBidJudgeType.getRcId());
			this.getRequest().setAttribute("operModule", "保存评委评分");
			this.getRequest().setAttribute("message", "评分成功");
		} catch (Exception e) {
			log.error("保存评委评标技术评分列表错误！", e);
			throw new BaseException("保存评委评标技术评分列表错误！", e);
		}
		
		return "success" ;
		
	}
	
	/**
	 * 修改评委评标信息初始化
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateTechOrBusJudgerBidsInit() throws BaseException {
		
		try{
			List jtcList = new ArrayList();
			List supList = new ArrayList();
			Long tbjtId=Long.parseLong(this.getRequest().getParameter("tbjtId"));
			tenderBidJudgeType=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeType(tbjtId);
			
			tenderBidItem = new TenderBidItem();
			tenderBidItem.setRcId(tenderBidJudgeType.getRcId());
			tenderBidItem.setItemType(tenderBidJudgeType.getTableName());
			tenderBidItemList = this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem);
			
			
			if(StringUtil.isNotBlank(tenderBidJudgeType.getTableName())){
				if("0".equals(tenderBidJudgeType.getTableName())){
					TenderJudgeTechScore judgeTechScore = new TenderJudgeTechScore();
					judgeTechScore.setRcId(tenderBidJudgeType.getRcId());
					judgeTechScore.setExpertId(tenderBidJudgeType.getExpertId());
					jtcList = this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreList(judgeTechScore);
					supList = this.iTenderJudgeTechScoreBiz.getDiffrentTechJudgeList("supplierId,supplierName,supPoints", judgeTechScore, "");
				}else{
					TenderJudgeBusScore judgeBusScore = new TenderJudgeBusScore();
					judgeBusScore.setRcId(tenderBidJudgeType.getRcId());
					judgeBusScore.setExpertId(tenderBidJudgeType.getExpertId());
					jtcList = this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreList(judgeBusScore);
					supList = this.iTenderJudgeBusScoreBiz.getDiffrentBusJudgeList("supplierId,supplierName,supPoints", judgeBusScore, "");
				}
			}
			receivedBulletinList = new ArrayList<TenderReceivedBulletin>();
			TenderReceivedBulletin receivedBulletin=null;
			if(supList!=null&&supList.size()>0){
				for(int i=0;i<supList.size();i++){
					Object[] objArr = (Object[]) supList.get(i);
					receivedBulletin = new TenderReceivedBulletin();
					receivedBulletin.setSupplierId((Long)objArr[0]);
					receivedBulletin.setSupplierName((String)objArr[1]);
					receivedBulletin.setSupPoints(((Double)objArr[2]).toString());
					receivedBulletinList.add(receivedBulletin);
				}
			}
			receivedBulletinList = this.orderInviteSupplierByID(receivedBulletinList);
			
			this.getRequest().setAttribute("jtcList", jtcList);
			this.getRequest().setAttribute("tenderBidItemList", tenderBidItemList);
            this.getRequest().setAttribute("receivedBulletinList", receivedBulletinList);
            this.getRequest().setAttribute("tenderBidJudgeType", tenderBidJudgeType);
            this.getRequest().setAttribute("expertName", UserRightInfoUtil.getExpertName(this.getRequest()));
            this.getRequest().setAttribute("date", DateUtil.getCurrentDateTime());
		} catch (Exception e) {
			log.error("修改评委评标初始化技术评分信息错误！", e);
			throw new BaseException("修改评委评标初始化评标技术评分信息错误！", e);
		}
		return "jdBidsUpdateInit" ;
	}
	
	/**
	 * 修改评委评标信息
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String updateTechOrBusJudgerBids() throws BaseException {
		
		try{
			Long tbjtId=Long.parseLong(this.getRequest().getParameter("tbjtId"));
			String param=this.getRequest().getParameter("param");
			tenderBidJudgeType=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeType(tbjtId);
			if(receivedBulletinList!=null&&receivedBulletinList.size()>0){
				if(StringUtil.isNotBlank(tenderBidJudgeType.getTableName())){
					if("0".equals(tenderBidJudgeType.getTableName())){
						//删除技术评标信息
						this.iTenderJudgeTechScoreBiz.deleteTenderJudgeTechScoreByRcIdAndExpertId(tenderBidJudgeType.getRcId(), tenderBidJudgeType.getExpertId());
						//保存技术评标信息
						TenderJudgeTechScore jTechScore=null;
						for(TenderReceivedBulletin receivedBulletin:receivedBulletinList){
							if(receivedBulletin!=null){
								for(TenderJudgeTechScore jtScore:judgeTechScoreList){
									if(jtScore!=null){
										jTechScore = new TenderJudgeTechScore();
										jTechScore.setRcId(tenderBidJudgeType.getRcId());
										jTechScore.setExpertId(tenderBidJudgeType.getExpertId());
										jTechScore.setSupplierId(receivedBulletin.getSupplierId());
										jTechScore.setSupplierName(receivedBulletin.getSupplierName());
										if(StringUtil.isNotBlank(receivedBulletin.getJudgePoints().trim())){
											String[] jdPoints = receivedBulletin.getJudgePoints().split(",");
											if(StringUtil.isNotBlank(jdPoints[judgeTechScoreList.indexOf(jtScore)].trim())){
												jTechScore.setJudgePoints(Double.parseDouble(jdPoints[judgeTechScoreList.indexOf(jtScore)].trim()));
											}else{
												jTechScore.setJudgePoints(0.00);
											}
										}else{
											jTechScore.setJudgePoints(0.00);
										}
										if(StringUtil.isNotBlank(receivedBulletin.getSupPoints().trim())){
											jTechScore.setSupPoints(Double.parseDouble(receivedBulletin.getSupPoints()));
										}else{
											jTechScore.setSupPoints(0.00);
										}
										jTechScore.setScoreTime(DateUtil.getCurrentDateTime());
										jTechScore.setStatus(param);
										//保存技术评分项
										jTechScore.setItemId(jtScore.getItemId());
										jTechScore.setDispNumber(jtScore.getDispNumber());
										this.iTenderJudgeTechScoreBiz.saveTenderJudgeTechScore(jTechScore);
									}
								}
							}
						}
						//更新评委标段表信息
						tenderBidJudgeType.setTableStatu(param);
						if(param.equals("1")) tenderBidJudgeType.setTableSubmitTime(DateUtil.getCurrentDateTime());
						this.iTenderBidJudgeTypeBiz.updateTenderBidJudgeType(tenderBidJudgeType);
					}else{
						//删除商务评标信息
						this.iTenderJudgeBusScoreBiz.deleteTenderJudgeBusScoreByRcIdAndExpertId(tenderBidJudgeType.getRcId(),tenderBidJudgeType.getExpertId());
						
						TenderJudgeBusScore jBusScore=null;
						//保存商务评标信息
						for(TenderReceivedBulletin receivedBulletin:receivedBulletinList){
							if(receivedBulletin!=null){
								for(TenderJudgeBusScore jbScore:judgeBusScoreList){
									if(jbScore!=null){
										jBusScore = new TenderJudgeBusScore();
										jBusScore.setRcId(tenderBidJudgeType.getRcId());
										jBusScore.setExpertId(tenderBidJudgeType.getExpertId());
										jBusScore.setSupplierId(receivedBulletin.getSupplierId());
										jBusScore.setSupplierName(receivedBulletin.getSupplierName());
										if(StringUtil.isNotBlank(receivedBulletin.getJudgePoints().trim())){
											String[] jdPoints = receivedBulletin.getJudgePoints().split(",");
											if(StringUtil.isNotBlank(jdPoints[judgeBusScoreList.indexOf(jbScore)].trim())){
												jBusScore.setJudgePoints(Double.parseDouble(jdPoints[judgeBusScoreList.indexOf(jbScore)].trim()));
											}else{
												jBusScore.setJudgePoints(0.00);
											}
										}else{
											jBusScore.setJudgePoints(0.00);
										}
										if(StringUtil.isNotBlank(receivedBulletin.getSupPoints().trim())){
											jBusScore.setSupPoints(Double.parseDouble(receivedBulletin.getSupPoints()));
										}else{
											jBusScore.setSupPoints(0.00);
										}
										jBusScore.setSupPoints(Double.parseDouble(receivedBulletin.getSupPoints()));
										jBusScore.setScoreTime(DateUtil.getCurrentDateTime());
										jBusScore.setStatus(param);
										//保存商务评分项
										jBusScore.setItemId(jbScore.getItemId());
										jBusScore.setDispNumber(jbScore.getDispNumber());
										this.iTenderJudgeBusScoreBiz.saveTenderJudgeBusScore(jBusScore);
									}
								}
							}
						}
						//更新评委标段表信息
						tenderBidJudgeType.setTableStatu(param);
						if(param.equals("1")) tenderBidJudgeType.setTableSubmitTime(DateUtil.getCurrentDateTime());
						this.iTenderBidJudgeTypeBiz.updateTenderBidJudgeType(tenderBidJudgeType);
					}
				}
			}

			if(param.equals("1")) this.iTenderBidListBiz.updateCalibtationStatus(tenderBidJudgeType.getRcId());
			this.getRequest().setAttribute("operModule", "修改评委评分");
			this.getRequest().setAttribute("message", "评分成功");
		} catch (Exception e) {
			log.error("修改评委评标技术评分列表错误！", e);
			throw new BaseException("修改评委评标技术评分列表错误！", e);
		}
		
		return "success" ;
		
	}
	
	/**
	 * 查看评委评标信息明细
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTechOrBusJudgerBidsDetail() throws BaseException {
		
		try{
			List jtcList = new ArrayList();
			List supList = new ArrayList();
			Long tbjtId=Long.parseLong(this.getRequest().getParameter("tbjtId"));
			tenderBidJudgeType=this.iTenderBidJudgeTypeBiz.getTenderBidJudgeType(tbjtId);
			tenderBidItem = new TenderBidItem();
			tenderBidItem.setRcId(tenderBidJudgeType.getRcId());
			tenderBidItem.setItemType(tenderBidJudgeType.getTableName());
			tenderBidItemList = this.iTenderBidItemBiz.getTenderBidItemList(tenderBidItem);
			
			if(StringUtil.isNotBlank(tenderBidJudgeType.getTableName())){
				if("0".equals(tenderBidJudgeType.getTableName())){
					TenderJudgeTechScore judgeTechScore = new TenderJudgeTechScore();
					judgeTechScore.setRcId(tenderBidJudgeType.getRcId());
					judgeTechScore.setExpertId(tenderBidJudgeType.getExpertId());
					jtcList = this.iTenderJudgeTechScoreBiz.getTenderJudgeTechScoreList(judgeTechScore);
					supList = this.iTenderJudgeTechScoreBiz.getDiffrentTechJudgeList("supplierId,supplierName,supPoints", judgeTechScore, "");
				}else{
					TenderJudgeBusScore judgeBusScore = new TenderJudgeBusScore();
					judgeBusScore.setRcId(tenderBidJudgeType.getRcId());
					judgeBusScore.setExpertId(tenderBidJudgeType.getExpertId());
					jtcList = this.iTenderJudgeBusScoreBiz.getTenderJudgeBusScoreList(judgeBusScore);
					supList = this.iTenderJudgeBusScoreBiz.getDiffrentBusJudgeList("supplierId,supplierName,supPoints", judgeBusScore, "");
				}
			}
			receivedBulletinList = new ArrayList<TenderReceivedBulletin>();
			TenderReceivedBulletin receivedBulletin=null;
			
			if(supList!=null&&supList.size()>0){
				for(int i=0;i<supList.size();i++){
					Object[] objArr = (Object[]) supList.get(i);
					receivedBulletin = new TenderReceivedBulletin();
					receivedBulletin.setSupplierId((Long)objArr[0]);
					receivedBulletin.setSupplierName((String)objArr[1]);
					receivedBulletin.setSupPoints(((Double)objArr[2]).toString());
					receivedBulletinList.add(receivedBulletin);
				}
			}
			receivedBulletinList = this.orderInviteSupplierByID(receivedBulletinList);
			
			this.getRequest().setAttribute("jtcList", jtcList);
			this.getRequest().setAttribute("tenderBidItemList", tenderBidItemList);
            this.getRequest().setAttribute("receivedBulletinList", receivedBulletinList);
            this.getRequest().setAttribute("tenderBidJudgeType", tenderBidJudgeType);
            this.getRequest().setAttribute("expertName", UserRightInfoUtil.getExpertName(this.getRequest()));
		} catch (Exception e) {
			log.error("查看评委评标信息明细错误！", e);
			throw new BaseException("查看评委评标信息明细错误！", e);
		}
		return "jdBidsDetail" ;
	}

	/**
	 * 邀请供应商信息排序
	 * @return
	 * @throws BaseException 
	 */
	public List orderInviteSupplierByID(List invSupList) {
		
		List orderSupList = new ArrayList();
		Long[] supplierIdArr = new Long[invSupList.size()];
		for(int i=0;i<invSupList.size();i++){
			TenderReceivedBulletin inSup = (TenderReceivedBulletin) invSupList.get(i);
			supplierIdArr[i] = inSup.getSupplierId();
		}
		Long temp = null;
		if(supplierIdArr!=null){
			for(int i=0;i<supplierIdArr.length;i++){
				for(int j=0;j<supplierIdArr.length;j++){
					if(supplierIdArr[i] != null && supplierIdArr[j] != null){
						if(supplierIdArr[i]>supplierIdArr[j]){
							temp = supplierIdArr[i];
							supplierIdArr[i] = supplierIdArr[j];
							supplierIdArr[j] = temp;
						}
					}
				}
			}
			for(int i=0;i<supplierIdArr.length;i++){
				for(int j=0;j<supplierIdArr.length;j++){
					TenderReceivedBulletin invSup = (TenderReceivedBulletin) invSupList.get(j);
					if(supplierIdArr[i].equals(invSup.getSupplierId())){
						orderSupList.add(invSup);
						break;
					}
				}
			}
		}
		return orderSupList;
	}

	/**
	 * 跳转到修改用户密码页面
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws BaseException
	 */
	public String updateExpertPwdInit( ) throws BaseException {	
		try {
		} catch (Exception e) {
			log.error("修改专家密码初始化错误！", e);
			throw new BaseException("修改专家密码初始化错误！", e);
		}
		
		return  "updateExpertPassWordInit"   ;
	}

	
	/**
	 * 修改用户密码信息
	 * @return
	 * @throws BaseException 
	 * @author fyh
	 */
	public String updateExpertPassWord() throws Exception{
		try{
			String message="";
			String oldPassWord=this.getRequest().getParameter("oldPassWord");
			String newPassWord=this.getRequest().getParameter("newPassWord");
			Expert expert=this.iExpertBiz.getExpert(UserRightInfoUtil.getExpertId(this.getRequest()));
			if(expert!=null){
				if(!expert.getExpertPassword().equals(PasswordUtil.encrypt(oldPassWord))){
					message="原始密码错误,请重新输入";
				}else{
					expert.setExpertPassword(PasswordUtil.encrypt(newPassWord));
					this.iExpertBiz.updateExpert(expert);
					message="修改成功";
				}
				this.getRequest().setAttribute("message", message);
			}
			
			
		} catch (Exception e) {
			log.error("修改专家密码信息！", e);
			throw new BaseException("修改专家密码信息！", e);
		}
		return "updateExpertPassWordInit";
	}

	public IExpertBiz getiExpertBiz() {
		return iExpertBiz;
	}

	public void setiExpertBiz(IExpertBiz iExpertBiz) {
		this.iExpertBiz = iExpertBiz;
	}
	public ITenderBidJudgeBiz getiTenderBidJudgeBiz() {
		return iTenderBidJudgeBiz;
	}
	public void setiTenderBidJudgeBiz(ITenderBidJudgeBiz iTenderBidJudgeBiz) {
		this.iTenderBidJudgeBiz = iTenderBidJudgeBiz;
	}
	public ITenderBidJudgeTypeBiz getiTenderBidJudgeTypeBiz() {
		return iTenderBidJudgeTypeBiz;
	}
	public void setiTenderBidJudgeTypeBiz(
			ITenderBidJudgeTypeBiz iTenderBidJudgeTypeBiz) {
		this.iTenderBidJudgeTypeBiz = iTenderBidJudgeTypeBiz;
	}
	public ITenderBidItemBiz getiTenderBidItemBiz() {
		return iTenderBidItemBiz;
	}
	public void setiTenderBidItemBiz(ITenderBidItemBiz iTenderBidItemBiz) {
		this.iTenderBidItemBiz = iTenderBidItemBiz;
	}
	public ITenderReceivedBulletinBiz getiTenderReceivedBulletinBiz() {
		return iTenderReceivedBulletinBiz;
	}
	public void setiTenderReceivedBulletinBiz(
			ITenderReceivedBulletinBiz iTenderReceivedBulletinBiz) {
		this.iTenderReceivedBulletinBiz = iTenderReceivedBulletinBiz;
	}
	public ITenderBidListBiz getiTenderBidListBiz() {
		return iTenderBidListBiz;
	}
	public void setiTenderBidListBiz(ITenderBidListBiz iTenderBidListBiz) {
		this.iTenderBidListBiz = iTenderBidListBiz;
	}
	public ITenderJudgeTechScoreBiz getiTenderJudgeTechScoreBiz() {
		return iTenderJudgeTechScoreBiz;
	}
	public void setiTenderJudgeTechScoreBiz(
			ITenderJudgeTechScoreBiz iTenderJudgeTechScoreBiz) {
		this.iTenderJudgeTechScoreBiz = iTenderJudgeTechScoreBiz;
	}
	public ITenderJudgeBusScoreBiz getiTenderJudgeBusScoreBiz() {
		return iTenderJudgeBusScoreBiz;
	}
	public void setiTenderJudgeBusScoreBiz(
			ITenderJudgeBusScoreBiz iTenderJudgeBusScoreBiz) {
		this.iTenderJudgeBusScoreBiz = iTenderJudgeBusScoreBiz;
	}
	public List<TenderReceivedBulletin> getReceivedBulletinList() {
		return receivedBulletinList;
	}
	public void setReceivedBulletinList(
			List<TenderReceivedBulletin> receivedBulletinList) {
		this.receivedBulletinList = receivedBulletinList;
	}
	public List<TenderJudgeBusScore> getJudgeBusScoreList() {
		return judgeBusScoreList;
	}
	public void setJudgeBusScoreList(List<TenderJudgeBusScore> judgeBusScoreList) {
		this.judgeBusScoreList = judgeBusScoreList;
	}
	public List<TenderJudgeTechScore> getJudgeTechScoreList() {
		return judgeTechScoreList;
	}
	public void setJudgeTechScoreList(List<TenderJudgeTechScore> judgeTechScoreList) {
		this.judgeTechScoreList = judgeTechScoreList;
	}
	
	
}
