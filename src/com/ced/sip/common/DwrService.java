package com.ced.sip.common;

import com.ced.base.exception.BaseException;
import com.ced.sip.common.biz.IDwrServiceBiz;
import com.ced.sip.common.utils.StringUtil;
 
public class DwrService {
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名,多列以逗号分割,如deptCode,isUsable
	 * @param columnValue 列值,多以逗号分割,如05,0
	 * @param comId 公司Id
	 * @return 0不存在 其它值存在 
	 */
	public static int ifExitCodeInTableComId(String tableName , String columnName ,String columnValue,Long comId) 
		throws BaseException{
		int rValue = 0 ;
		if( StringUtil.isNotBlank( columnName ) || StringUtil.isNotBlank( columnValue ) ) {
			String [] columnNames  = columnName.split(",") ;
			String [] columnValues = columnValue.split(",") ;
			rValue = iDwrServiceBiz.ifExitCodeInTableComId(tableName, columnNames, columnValues,comId) ;
		}
		
		return rValue  ;
		
	}	
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名,多列以逗号分割,如deptCode,isUsable
	 * @param columnValue 列值,多以逗号分割,如05,0
	 * @return 0不存在 其它值存在 
	 */
	public static int ifExitCodeInTable(String tableName , String columnName ,String columnValue) 
		throws BaseException{
		int rValue = 0 ;
		if( StringUtil.isNotBlank( columnName ) || StringUtil.isNotBlank( columnValue ) ) {
			String [] columnNames  = columnName.split(",") ;
			String [] columnValues = columnValue.split(",") ;
			rValue = iDwrServiceBiz.ifExitCodeInTable(tableName, columnNames, columnValues) ;
		}
		
		return rValue  ;
		
	}

	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名,多列以逗号分割,如deptCode,isUsable
	 * @param columnValue 列值,多以逗号分割,如05,0
	 * @param queryColumnName  如 userId
	 * @return 0不存在 其它值存在 
	 */
	public static int getIdInTable(String tableName,String columnName ,String columnValue,String queryColumnName) 
		throws BaseException{
		int rValue = 0 ;
		if( StringUtil.isNotBlank( columnName ) || StringUtil.isNotBlank( columnValue ) ) {
			String [] columnNames  = columnName.split(",") ;
			String [] columnValues = columnValue.split(",") ;
			rValue = iDwrServiceBiz.getIdInTable(tableName, columnNames, columnValues,queryColumnName) ;
		}
		
		return rValue  ;
		
	}
	private static IDwrServiceBiz iDwrServiceBiz = null ;
	
	public static IDwrServiceBiz getIDwrServiceBiz() {
		return iDwrServiceBiz;
	}

	public static boolean setIDwrServiceBiz(IDwrServiceBiz dwrServiceBiz) {
		iDwrServiceBiz = dwrServiceBiz;
		return true; 
	}
	
	
}
