package com.ced.sip.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ced.sip.common.entity.SessionInfo;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Users;

public class UserRightInfoUtil {
	
	/**
	 * 取用户信息 -> session 
	 * @param request
	 * @return
	 */
	public static SessionInfo getSessionInfo( HttpServletRequest request ) {
		SessionInfo info = null ;
		
		if( request.getSession().getAttribute(TableStatus.LOGIN_INFO_KEY) != null ) {
			info = (SessionInfo) request.getSession().getAttribute(TableStatus.LOGIN_INFO_KEY) ; 
		}
		
		return info ;
	}

	/**
	 * 保存用户信息->session 
	 * @param sessionInfo
	 * @param request
	 */
	public static void setSessionInfo(SessionInfo sessionInfo, HttpServletRequest request ) {

		HttpSession session = request.getSession();
		session.setAttribute(TableStatus.LOGIN_INFO_KEY, sessionInfo);
	}
	
	/**
	 * 获取用户名称 -- 登录名( 英文名 )
	 * @param request
	 * @return
	 */
	public static String getUserName(HttpServletRequest request) {
		//登录人的姓名
		return  timeoutUser( request ) ? "" : getSessionInfo(request).getUsers().getUserName() ;
	}
	/**
	 * 获取用户名称 -- 登录名( 英文名 ) 从通用的sessionUser中读取
	 * @param request
	 * @return
	 */
	public static String getSessionUserLoginName(HttpServletRequest request) {
		//登录人的姓名
		return  timeoutUser( request ) ? "" : getSessionInfo(request).getSessionUser().getLoginName() ;
	}
	/**
	 * 获取用户 -- 登录密码
	 * @param request
	 * @return
	 * @author fyh
	 */
	public static String getUserPassWord(HttpServletRequest request) {
		//登录人的密码
		return  timeoutUser( request ) ? "" : getSessionInfo(request).getUsers().getUserPassword();
	}
	/**
	 * 获取用户 -- ID
	 * @param request
	 * @return
	 * @author fyh
	 */
	public static Long getUserId(HttpServletRequest request) {
		//登录人的密码
		return  timeoutUser( request ) ? new Long(0) : getSessionInfo(request).getUsers().getUserId();
	}
	/**
	 * 获取用户公司Id -- ID
	 * @param request
	 * @return
	 * @author fyh
	 */
	public static Long getComId(HttpServletRequest request) {
		//登录人的密码
		return  timeoutUser( request ) ? new Long(0) : getSessionInfo(request).getSessionUser().getComId();
	}
	/**
	 * 取得登录用户的中文名称
	 * @param request
	 * @return
	 */
	public static String getChineseName( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getUsers().getUserChinesename() ;
	}
	/**
	 * 取得登录用户的手机号
	 * @param request
	 * @return
	 */
	public static String getUserPhone( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getUsers().getPhoneNum() ;
	}
	/**
	 * 取得登录用户的中文名称  从通用的sessionUser中读取
	 * @param request
	 * @return
	 */
	public static String getSessionUserName( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getSessionUser().getName() ;
	}
	/**
	 * 取得登录用户的信息
	 * @param request
	 * @return
	 */
	public static Users getUsers( HttpServletRequest request ){
		return timeoutUser( request ) ? null : getSessionInfo(request).getUsers();
	}
	/**
	 * 取得登录用户的部门信息
	 * @param request
	 * @return
	 */
	public static Departments getDepartments( HttpServletRequest request ){
		return timeoutUser( request ) ? null : getSessionInfo(request).getDept();
	}
	/**
	 * 获取当前登录用户的用户类别 0普通用户 1中层用户 2高层用户
	 * @param request
	 * @return
	 */
	public static  int getUserType( HttpServletRequest request ){
		return timeoutUser( request ) ? 0 : getSessionInfo(request).getUsers().getUserType() ;
	}
	/**
	 * 获取当前登录用户的用户类别 从通用的sessionUser中读取  0内部用户 1供应商  2专家
	 * @param request
	 * @return
	 */
	public static  int getSessionUserType( HttpServletRequest request ){
		return timeoutUser( request ) ? 99 : getSessionInfo(request).getSessionUser().getType();
	}	
	/**
	 * 获取当前登录用户的部门名称
	 * @param request
	 * @return
	 */
	public static String getUserDeptName( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getDept().getDeptName() ;
	}
	
	/**
	 * 获取当前登录用户的部门ID
	 * @param request
	 * @return
	 */
	public static Long getUserDeptId( HttpServletRequest request ){
		return timeoutUser( request ) ? new Long(0) : getSessionInfo(request).getUsers().getDepId() ;
	}
	
	/**
	 * 判断是否系统管理员
	 * @param request
	 * @return
	 */
	public static boolean ifSystemManagerRole( HttpServletRequest request ) {
		return ifSystemManagerRole( getSessionInfo(request) ) ;
	}
	
	/**
	 * 判断是否系统管理员 
	 * @param info 
	 * @return
	 */
	public static boolean ifSystemManagerRole( SessionInfo info ) {
		boolean rValue = false ;
		if(info.getUsers()!=null){
		String username=info.getUsers().getUserName();
		if(username.equals("qcyddyc")){
			rValue 	= true ;
		}
		}
		return rValue ;
	}
			
	/**
	 * 校验用户SESSION超时
	 * @param request
	 * @return
	 */
	public static boolean timeoutUser( HttpServletRequest request ) {
		boolean rValue = false ;
		
		if( ! ( getSessionInfo( request ) != null ) ) {
			rValue = true ;
		}
		
		return rValue ;
	}
	/**
	 * 权限过滤
	 * @param request
	 * writer  为userName
	 * @param aliasTable
	 * @return
	 */
	public static String getUserDepartNameHql( HttpServletRequest request, String aliasTable ) {
		StringBuffer rValue = new StringBuffer( "" );

		SessionInfo sessionInfo=getSessionInfo(request);
		if( sessionInfo != null ) {
			
			if( UserRightInfoUtil.ifSystemManagerRole(sessionInfo)) {
				// 判断是否管理员的权限
			}else {
				int type=getUserType(request);
				if(type==2){
					
				}else{
				//判断依据： 如果不在群组树里面，则此员工即为普通员工，只能查看自己编制的信息，
				//           如果在群组树里面，则分为中层领导和高层领导，基本的权限为都能看到本部门的信息，除此之外还可以看到 list里面的信息
				List<Departments> list=sessionInfo.getDeptIdList();
				List<Departments> leader=sessionInfo.getLeaderList();
				
				//以下判断是当前登录人员非需求
				rValue.append(" and (").append( aliasTable ).append(".writer= '").append( UserRightInfoUtil.getUserName(request) ).append("' ");
            	
			    Departments departments=null;
			    if(list.size()>0){						    
			    for(int i=0;i<list.size();i++){ 
			    	
			    		departments=(Departments)list.get(i);
			    		rValue.append(" or ").append( aliasTable ).append(".deptId= '").append( departments.getDepId()).append("' ");
			    	    
			      }
			    }
			    
			    if(leader.size()>0){						    
				    for(int i=0;i<leader.size();i++){  
				    	
				    	departments=(Departments)leader.get(i);
				    	rValue.append(" or ").append( aliasTable ).append(".deptId= '").append( departments.getDepId()).append("' ");
				    	     
				      }
				}

			    rValue.append(" )");
				}
			}
		}
		return rValue.toString() ;
	}
	/**
	 * 资金计划 单独权限过滤
	 * @param request
	 * writer  为userName
	 * @param aliasTable
	 * @return
	 */
	public static String getUserDepartNameCapitalPlanHql( HttpServletRequest request, String aliasTable ) {
		StringBuffer rValue = new StringBuffer( "" );

		SessionInfo sessionInfo=getSessionInfo(request);
		if( sessionInfo != null ) {
			
			if( UserRightInfoUtil.ifSystemManagerRole(sessionInfo)) {
				// 判断是否管理员的权限
			}else {
				int type=getUserType(request);
				if(type==2){
					
				}else{
				//判断依据： 如果不在群组树里面，则此员工即为普通员工，只能查看自己编制的信息，
				//           如果在群组树里面，则分为中层领导和高层领导，基本的权限为都能看到本部门的信息，除此之外还可以看到 list里面的信息
				List<Departments> list=sessionInfo.getDeptIdList();
				List<Departments> leader=sessionInfo.getLeaderList();
				
				//以下判断是当前登录人员非需求
				rValue.append(" and (").append( aliasTable ).append(".writer= '").append( UserRightInfoUtil.getUserName(request) ).append("' ");
            	
			    Departments departments=null;
			    if(list.size()>0){						    
			    for(int i=0;i<list.size();i++){ 
			    	
			    		departments=(Departments)list.get(i);
			    		rValue.append(" or ").append( aliasTable ).append(".deptId= '").append( departments.getDepId()).append("' ");
			    		rValue.append(" or ").append( aliasTable ).append(".applicantDeptId= '").append( departments.getDepId()).append("' ");
			    	    
			      }
			    }
			    
			    if(leader.size()>0){						    
				    for(int i=0;i<leader.size();i++){  
				    	
				    	departments=(Departments)leader.get(i);
				    	rValue.append(" or ").append( aliasTable ).append(".deptId= '").append( departments.getDepId()).append("' ");
			    		rValue.append(" or ").append( aliasTable ).append(".applicantDeptId= '").append( departments.getDepId()).append("' ");
				    	     
				      }
				}

			    rValue.append(" )");
				}
			}
		}
		return rValue.toString() ;
	}
	/**
	 *  权限过滤
	 * @param request
	 * writer  为userName
	 * @param aliasTable
	 * @return
	 */
	public static String getUserDepartNameSql( HttpServletRequest request, String aliasTable ) {
		StringBuffer rValue = new StringBuffer( "" );

		SessionInfo sessionInfo=getSessionInfo(request);
		if( sessionInfo != null ) {
			
			if( UserRightInfoUtil.ifSystemManagerRole(sessionInfo)) {
				// 判断是否管理员的权限
			}else {
				int type=getUserType(request);
				if(type==2){
					
				}else{
				//判断依据： 如果不在群组树里面，则此员工即为普通员工，只能查看自己编制的信息，
				//           如果在群组树里面，则分为中层领导和高层领导，基本的权限为都能看到本部门的信息，除此之外还可以看到 list里面的信息
				List<Departments> list=sessionInfo.getDeptIdList();
				List<Departments> leader=sessionInfo.getLeaderList();
				
				//以下判断是当前登录人员非需求
				rValue.append(" and (").append( aliasTable ).append(".writer= '").append( UserRightInfoUtil.getUserName(request) ).append("' ");
            	
			    Departments departments=null;
			    if(list.size()>0){						    
			    for(int i=0;i<list.size();i++){ 
			    	
			    		departments=(Departments)list.get(i);
			    		rValue.append(" or ").append( aliasTable ).append(".dept_id= '").append( departments.getDepId()).append("' ");
			    	    
			      }
			    }
			    
			    if(leader.size()>0){						    
				    for(int i=0;i<leader.size();i++){  
				    	
				    	departments=(Departments)leader.get(i);
				    	rValue.append(" or ").append( aliasTable ).append(".dept_id= '").append( departments.getDepId()).append("' ");
				    	     
				      }
				}

			    rValue.append(" )");
				}
			}
		}
		return rValue.toString() ;
	}
	/**
	 * 采购管理数据权限过滤-根据用户组里面对应的数据权限
	 * 查询
	 * @param sessionInfo 
	 * @param aliasTable
	 * @return
	 */
	public static String getPurchaseRightSql( HttpServletRequest request, String aliasTable ) {
		StringBuffer rValue = new StringBuffer( "" );
		SessionInfo sessionInfo=getSessionInfo(request);
		if( sessionInfo != null ) {
			
			if( UserRightInfoUtil.ifSystemManagerRole(sessionInfo)) {
				// 判断是否管理员的权限
			}else {
				rValue.append(" or ").append( aliasTable ).append(".writer= '").append( sessionInfo.getUsers().getUserName() ).append("') ");
           }
		}
		return rValue.toString() ;
	}
	/**
	 * 校验用户SESSION超时
	 * @param request
	 * @return
	 */
	public static boolean timeoutExpertUser( HttpServletRequest request ) {
		boolean rValue = false ;
		
		if( ! ( getSessionInfo( request ) != null ) ) {
			rValue = true ;
		}
		
		return rValue ;
	}
	/**
	 * 取得登录专家的中文名称
	 * @param request
	 * @return
	 */
	public static String getExpertName( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getSessionUser().getName() ;
	}
	/**
	 * 取得登录专家的中文名称
	 * @param request
	 * @return
	 */
	public static Long getExpertId( HttpServletRequest request ){
		return timeoutUser( request ) ? 0 : getSessionInfo(request).getSessionUser().getId() ;
	}
	/**
	 * 取得供应商中文名称
	 * @param request
	 * @return
	 */
	public static String getSupplierName( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getSupplierName();
	}
	/**
	 * 取得供应商登录名称
	 * @param request
	 * @return
	 */
	public static String getSupplierLoginName( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getSupplierLoginName();
	}
	/**
	 * 取得供应商登录密码
	 * @param request
	 * @return
	 */
	public static String getSupplierLoginPwd( HttpServletRequest request ){
		return timeoutUser( request ) ? "" : getSessionInfo(request).getSupplierInfo().getSupplierLoginPwd();
	}
	/**
	 * 取得供应商Id
	 * @param request
	 * @return
	 */
	public static Long getSupplierId( HttpServletRequest request ){
		return timeoutUser( request ) ? 0 : getSessionInfo(request).getSupplierInfo().getSupplierId();
	}
	/**
	 * 取得供应商信息
	 * @param request
	 * @return
	 */
	public static SupplierInfo getSupplierInfo( HttpServletRequest request ){
		return timeoutUser( request ) ? null : getSessionInfo(request).getSupplierInfo();
	}
}
