/**
 * @author Ted 2010-06-25
 */
package com.ced.sip.common;

public class WorkFlowStatus {
	
	//----------------------------- 审批结果 ---------------------------------------
	/** 审批状态：不同意  "0" */
	public static String Common_Check_NoPass = "-1";
	
	/** 审批状态：不同意的标签 */
	public static String Common_Check_NoPass_Text = "不通过";
	
	/** 审批状态：同意 "1" */
	public static String Common_Check_Pass = "1";
	/** 审批状态：同意的标签 */
	public static String Common_Check_Pass_Text = "通过";
	
	
	/*****************************流程相关常量********************************************/
	public static final String WORK_FLOW_STATUS_0 = "0";//禁用
	public static final String WORK_FLOW_STATUS_1 = "1";//启用
	
	public static final String WORK_FLOW_STATUS_0_TEXT = "禁用";//禁用
	public static final String WORK_FLOW_STATUS_1_TEXT = "启用";//启用
	
	public static final String WORK_FLOW_ORDER_STATUS_01 = "01";//操作状态
	public static final String WORK_FLOW_ORDER_STATUS_0 = "0";//已结束
	public static final String WORK_FLOW_ORDER_STATUS_1 = "1";//运行中
	public static final String WORK_FLOW_ORDER_STATUS_2 = "2";//终止
	public static final String WORK_FLOW_ORDER_STATUS_3 = "3";//已退回

	public static final String WORK_FLOW_ORDER_STATUS_01_TEXT = "等待提交";
	public static final String WORK_FLOW_ORDER_STATUS_0_TEXT = "已完成";//已结束
	public static final String WORK_FLOW_ORDER_STATUS_1_TEXT = "流程审批中";//运行中
	public static final String WORK_FLOW_ORDER_STATUS_2_TEXT = "终止";//已退回
	public static final String WORK_FLOW_ORDER_STATUS_3_TEXT = "已退回";//已退回
	
	
	//计划审批流程定义编码
	public static final String RequiredPlan_Work_Item = "RequiredPlan";
	public static final String RequiredPlan_Work_Item_Text = "计划审批";
	
	//采购立项审批流程定义编码
	public static final String RequiredCollect_Work_Item = "RequiredCollect";
	public static final String RequiredCollect_Work_Item_Text = "采购立项审批";

	//采购结果审批流程定义编码
	public static final String BidPurchaseResult_Work_Item = "BidPurchaseResult";
	public static final String BidPurchaseResult_Work_Item_Text = "采购结果审批";
	
	//供应商考核流程定义编码（发送代办）
	public static final String SupplierEvaluate_Work_Item = "SupplierEvaluate";
	public static final String SupplierEvaluate_Work_Item_Text = "供应商考核";

	//合同审批流程定义编码
	public static final String Contract_Work_Item = "Contract";
	public static final String Contract_Work_Item_Text = "合同审批";

	//订单审批流程定义编码
	public static final String Order_Work_Item = "Order";
	public static final String Order_Work_Item_Text = "订单结果审批";	

	//资金计划审批流程定义编码
	public static final String CapitalPlan_Work_Item = "CapitalPlan";
	public static final String CapitalPlan_Work_Item_Text = "资金计划审批";
	
	/*****************************流程相关常量********************************************/
	/*****************************流程方式********************************************/

	//计划审批
	public static final String RM_WorkFlow_Type = "RM"; //
	//采购立项审批
	public static final String RC_WorkFlow_Type = "RC"; //
	//采购结果审批
	public static final String PR_WorkFlow_Type = "PR"; //
	//供应商考核
	public static final String SE_WorkFlow_Type = "SE"; //
	//合同审批
	public static final String CT_WorkFlow_Type = "CT"; //
	//订单审批
	public static final String OD_WorkFlow_Type = "OD"; //
	//资金计划审批
	public static final String CP_WorkFlow_Type = "CP"; //
}
