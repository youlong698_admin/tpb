/**
 * 
 */
package com.ced.sip.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ted 2010-06-25 
 *
 */
public class WorkFlowStatusMap {
	
	/***************************************常量参数转换文本********************************************/
	
	//流程状态
	private static Map<String, String> WorkFlowStatusMap	=	new HashMap<String, String>();
	static{
		WorkFlowStatusMap.put(WorkFlowStatus.WORK_FLOW_STATUS_0, WorkFlowStatus.WORK_FLOW_STATUS_0_TEXT);
		WorkFlowStatusMap.put(WorkFlowStatus.WORK_FLOW_STATUS_1, WorkFlowStatus.WORK_FLOW_STATUS_1_TEXT);
	};
	
	//流程状态
	private static Map<String, String> WorkFlowOrderStatusMap	=	new HashMap<String, String>();
	static{
		WorkFlowOrderStatusMap.put(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0, WorkFlowStatus.WORK_FLOW_ORDER_STATUS_0_TEXT);
		WorkFlowOrderStatusMap.put(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1, WorkFlowStatus.WORK_FLOW_ORDER_STATUS_1_TEXT);
		WorkFlowOrderStatusMap.put(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_2, WorkFlowStatus.WORK_FLOW_ORDER_STATUS_2_TEXT);
		WorkFlowOrderStatusMap.put(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3, WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3_TEXT);
	};
	//流程状态
	private static Map<String, String> WorkFlowResultMap	=	new HashMap<String, String>();
	static{
		WorkFlowResultMap.put(WorkFlowStatus.Common_Check_NoPass, WorkFlowStatus.Common_Check_NoPass_Text);
		WorkFlowResultMap.put(WorkFlowStatus.Common_Check_Pass, WorkFlowStatus.Common_Check_Pass_Text);
	};
	
	public static String getWorkflowStatus(String status){
		return WorkFlowStatusMap.get(status);
	}
	
	public static String getWorkflowOrderStatus(String status){
		return WorkFlowOrderStatusMap.get(status);
	}
	public static String getWorkflowResult(String status){
		return WorkFlowResultMap.get(status);
	}

}
