package com.ced.sip.common.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

public class AppServiceHelper {
	private static final Log logger = LogFactory.getLog(AppServiceHelper.class);
private static ApplicationContext applicationContext;
	
	public static void setApplicationContext(ApplicationContext appCtxIn){
		applicationContext=appCtxIn;
	}

	public static Object getBean(String beanId){
		Object service = null;
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("get bean="+beanId);
			}
			service = applicationContext.getBean(beanId);
		} catch (NoSuchBeanDefinitionException ex) {
			throw new RuntimeException("no such bean for["+beanId+"]", ex);
		}
		return service;
	}

	public static ApplicationContext findApplicationContext() {
		return applicationContext;
	}
}
