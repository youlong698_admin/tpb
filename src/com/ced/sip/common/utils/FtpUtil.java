package com.ced.sip.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;


import com.ced.base.utils.FtpUtilBase;
import com.ced.sip.common.entity.Attachment;

public class FtpUtil extends FtpUtilBase {

	 
	/**
	 * 
	 * @param attachment
	 * @return
	 */
	public static String getOperatorFileName(Attachment attachment) {
		if (attachment == null) {
			return "file.file";

		}
		String extendName = "";
		int index = attachment.getAttachmenetName().lastIndexOf(".");
		if (index > -1) {
			extendName = attachment.getAttachmenetName().substring(index);

		}
		return attachment.getUuid() + extendName;

	}
	/**
	 * 
	 * @param attachment
	 * @return
	 */
	public static String getOperatorFileNameNoExtendName(Attachment attachment) {
		if (attachment == null) {
			return "file.file";

		}
//		String extendName = "";
//		int index = attachment.getAttachmenetName().lastIndexOf(".");
//		if (index > -1) {
//			extendName = attachment.getAttachmenetName().substring(index);
//
//		}
		return attachment.getAttachmentId().toString() ;

	}
	
}
