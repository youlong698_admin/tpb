package com.ced.sip.common.utils.encrypt;

import java.util.Map;


public class MainTest {

	public static void main(String[] args) throws Exception {

		Map<String,String> map=RSAEncrypt.genKeyPair();
		//String publicKey=map.get("publicKey");
		//String privateKey=map.get("privateKey");
		//System.out.println(publicKey);
		//System.out.println(privateKey);
		
		System.out.println("--------------公钥加密私钥解密过程-------------------");
		String plainText="123.4";
		//公钥加密过程
		byte[] cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPublicKeyByStr("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIz2+csMXUYQJaeN38Il5U2+LkVvfx+x3SEDYTho1S7eXd1VipAGfNe+sVYL5DcmHJ41D+x9pPO0gBvdrSYTgxMCAwEAAQ=="),plainText.getBytes());
		String cipher=Base64.encode(cipherData);
		System.out.println("加密："+cipher);
		cipher="Ubw5pUC7PoOhTcuUINbtaB8/qo4xghEDjYvRU2N6YCZLZbKM5RcNE+Cw8SCamY6jsTyPkT8JEF+mKqOwVXEmmg==";
		//私钥解密过程
		byte[] res=RSAEncrypt.decrypt(RSAEncrypt.loadPrivateKeyByStr("MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAjPb5ywxdRhAlp43fwiXlTb4uRW9/H7HdIQNhOGjVLt5d3VWKkAZ8176xVgvkNyYcnjUP7H2k87SAG92tJhODEwIDAQABAkA1mu3e0zLdI+LN1tvSXsrWR44dBYtYKDEpDrn305XOHllfSjvZ1mF77qCPSdSyxAnSFxEA4kStGFhSbB5MAKDRAiEA7ptP0l7lY63Yuu+m97qZlN0adEi23ncSWDE57keA+28CIQCXPYw7BBggo3Th+uIJl2R726NG0E2R1r/4rYQz+DOwnQIhAKC5+hDpX3e/uUhi+oPRLHc8YP+Ho7dDWhYSQsWhYN79AiANZp+5TviQU8t0hpVmwipeKy0Bhk2JQZ0K/Nz8+d4ipQIhAMCqwJHquCVcMWzY16PA6PnSf/ObEGlRKlpMJROYt/7n"), Base64.decode(cipher));
		String restr=new String(res);

		System.out.println("解密："+restr);
		
		/*System.out.println("原文："+plainText);
		System.out.println("加密："+cipher);
		System.out.println("解密："+restr);
		System.out.println();
		
		
		System.out.println("--------------私钥加密公钥解密过程-------------------");
		plainText="ihep_私钥加密公钥解密";
		//私钥加密过程
		cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPrivateKeyByStr(privateKey),plainText.getBytes());
		cipher=Base64.encode(cipherData);
		//公钥解密过程
		res=RSAEncrypt.decrypt(RSAEncrypt.loadPublicKeyByStr(publicKey), Base64.decode(cipher));
		restr=new String(res);
		System.out.println("原文："+plainText);
		System.out.println("加密："+cipher);
		System.out.println("解密："+restr);
		System.out.println();
		
		System.out.println("---------------私钥签名过程------------------");
		String content="ihep_这是用于签名的原始数据";
		String signstr=RSASignature.sign(content,privateKey);
		System.out.println("签名原串："+content);
		System.out.println("签名串："+signstr);
		System.out.println();
		
		System.out.println("---------------公钥校验签名------------------");
		System.out.println("签名原串："+content);
		System.out.println("签名串："+signstr);
		
		System.out.println("验签结果："+RSASignature.doCheck(content, signstr, publicKey));
		System.out.println();*/
		
	}
}
