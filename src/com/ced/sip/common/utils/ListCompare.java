package com.ced.sip.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListCompare {
	public static <T> List<T> compare(T[] t1, T[] t2) {    
	      List<T> list1 = Arrays.asList(t1);    
	      List<T> list2 = new ArrayList<T>();    
	      for (T t : t2) {    
	          if (!list1.contains(t)) {    
	              list2.add(t);    
	          }    
	      }    
	      return list2;    
	  }   
	  
	  public static void main(String[] arg){  
	        
	        Integer[] array1 = {1, 2, 3,5};  
	        Integer[] array2 = {1, 2, 3, 4,44};  
	          
	        List<Integer> list = compare(array2,array1);  
	        for (Integer integer : list) {  
	            System.out.println(integer);  
	        }  
	  } 
}
