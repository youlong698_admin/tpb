package com.ced.sip.common.utils;

import java.io.File;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {


	private static final SimpleDateFormat mFormatIso8601Day = new SimpleDateFormat(
			"yyyy-MM-dd");

	private static final SimpleDateFormat mFormatZh = new SimpleDateFormat(
			"yyyy年MM月dd日");

	private static final SimpleDateFormat mFormatIso8601 = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ssZ");
	
	private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	public static String getWateStringFromDateChinese(Date date){
		if(date==null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(date);
	}
	public static String format(Date aDate, SimpleDateFormat aFormat) {
		if (aDate == null || aFormat == null) {
			return "";
		}
		return aFormat.format(aDate);
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static Date getCurrentDateTime() {
		Date current = new Date(System.currentTimeMillis());
		return current;
	}

	/**
	 * 转换不同格式的日期为字符串
	 * 
	 * @return
	 */
	public static String formatDateByType(Date aDate, String type) {

		SimpleDateFormat aFormat = new SimpleDateFormat(type);

		if (aDate == null || aFormat == null) {
			return "";
		}
		return aFormat.format(aDate);
	}

	/**
	 * 字符串日期转字符串日期格式格式 lh
	 * 
	 * @return
	 */
	public static String formatDate(String ss, String c) {
		String s1 = null;
		if (ss.indexOf(c) != 0 && ss != null && ss != "") {
			String s[] = ss.split(c);
			s1 = s[0] + "年" + s[1] + "月" + s[2] + "日";
		}
		return s1;
	}

	public static String format(Date aDate, String aFormat) {
		if (aDate == null || aFormat == null) {
			return "";
		}
		return new SimpleDateFormat(aFormat).format(aDate);
	}

	public static String getDefaultDateFormat(Date aDate) {

		return DateUtil.format(aDate, mFormatIso8601Day);
	}

	public static String getTimeFormat(Date aDate) {

		return DateUtil.format(aDate, mFormatIso8601);
	}

	/**
	 * 获取中文格式的日期格式2005年09月03日
	 * 
	 * @param data
	 * @return
	 */
	public static String getZhTimeFormat(Date data) {

		return DateUtil.format(data, mFormatZh);

	}

	public static String getDefaultDateFormat(long aDate) {
		Date date = new Date();
		date.setTime(aDate);

		return DateUtil.format(date, mFormatIso8601Day);
	}

	public static String getYear(Date date) {
		String currentTime = DateUtil.getDefaultDateFormat(date.getTime());
		String year = currentTime.substring(0, 4);
		return year;

	}

	public static String getMonth(Date date) {
		String currentTime = DateUtil.getDefaultDateFormat(date.getTime());
		String month = currentTime.substring(5, 7);
		return month;

	}

	public static String getDay(Date date) {
		String currentTime = DateUtil.getDefaultDateFormat(date.getTime());
		String day = currentTime.substring(8, 10);
		return day;

	}

	/**
	 * 根据日期生成附件上传目录结构 \\2004\\09
	 * 
	 * @param date
	 * @return
	 */
	public static String getDirectoryByDate(Date date) {
		return getYear(date) + File.separator + getMonth(date) + File.separator;

	}

	/**
	 * 根据日期获取访问url（相对于路径）
	 * 
	 * @param date
	 * @return
	 */
	public static String getUrlByDate(Date date) {
		return getYear(date) + "/" + getMonth(date) + "/";
	}

	/**
	 * 获取当前年
	 * 
	 * @return
	 */
	public static int getCurrentYear() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR);

	}

	/**
	 * 获取当前月
	 * 
	 * @return
	 */
	public static int getCurrentMonth() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.MONTH) + 1;

	}

	/**
	 * 获取当前天
	 * 
	 * @return
	 */
	public static int getCurrentDay() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.DAY_OF_MONTH);

	}

	/**
	 * 获取年
	 * 
	 * @return
	 */
	public static String getYears(Date date) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return String.valueOf(calendar.get(Calendar.YEAR));
		} else {
			return "";
		}
	}

	/**
	 * 获取月
	 * 
	 * @return
	 */
	public static String getMonths(Date date) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return String.valueOf(calendar.get(Calendar.MONTH) + 1);
		} else {
			return "";
		}
	}

	/**
	 * 获取天
	 * 
	 * @return
	 */
	public static String getDays(Date date) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		} else {
			return "";
		}
	}

	/**
	 * 获取当前中文格式的日期格式2005年09月03日
	 * 
	 * @param data
	 * @return
	 */
	public static String getCurrentZhTimeFormat() {
		return DateUtil.format(new Date(), mFormatZh);
	}

	/**
	 * //将date型转换成yyyy-MM-dd HH:mm:ss的String
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String getStringFromDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	/**
	 * //将date型转换成yyyy-MM-dd的String
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String getWateStringFromDate(Date date) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 将String日期转换成Timestamp
	 * 
	 * @param str
	 * @return
	 */
	public static Timestamp getTimestampFromString(String str) {
		return Timestamp.valueOf(str);
	}

	/**
	 * 返回不同格式的日期字符串
	 * 
	 * @return
	 * 
	 */
	public static String formatDateToStringByType(Date date, String dateFormat) {
		String dateStr = "";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		dateStr = sdf.format(date);
		return dateStr;
	}

	/**
	 * 将yyyy-MM-dd HH:mm:ss 时间格式字符串转化成时间
	 * 
	 * @return
	 */
	public static Date StringToDate(String time, String dateFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		Date s = null;
		try {
			if (StringUtil.isNotBlank(time)) {
				s = formatter.parse(time);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * 将Date类型转换为Timestamp（DateTime）
	 * 
	 * @return
	 * 
	 */
	public static Timestamp formatDateToTimestamp(Date date) {

		Timestamp timestamp = new Timestamp(date.getTime()); // 2013-01-14
																// 22:45:36.484
		return timestamp;
	}
	 /**  
     * 计算两个日期之间相差的天数  
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException    
    {      
        long time1 = smdate.getTime();        
        long time2 = bdate.getTime();         
        long between_days=(time2-time1)/(1000*3600*24);  
       return Integer.parseInt(String.valueOf(between_days));           
    }
    /**  
     * 计算两个日期之间相差的分钟数
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差分钟数
     * @throws ParseException  
     */    
    public static int minBetween(Date smdate,Date bdate) throws ParseException    
    {      
        long time1 = smdate.getTime();        
        long time2 = bdate.getTime();         
        long between_days=(time2-time1)/(1000*60);  
       return Integer.parseInt(String.valueOf(between_days));           
    }
    /**
     * 计算当前日期n天之前的日期;
     * @param n
     * @return
     * @throws ParseException
     */
    public static String dayAgo(int n) throws ParseException{
    	 Calendar calendar1 = Calendar.getInstance();
    	  SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    	  calendar1.add(Calendar.DATE, -n);
    	  String three_days_ago = sdf1.format(calendar1.getTime());
    	  return three_days_ago;
    }
    /**
	 * 当前时间后n天的日期
	 */

	public static Date getDateTimeAfter(int num) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, num);
		return now.getTime();
	}
	
	/**
	 * 获取当前月的第一天
	 */
	public static String getDateFristDay(){
		 //获取当前月第一天：
		Calendar c = Calendar.getInstance();    
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
		String first = format.format(c.getTime());
		return first;
	}
	
	/**
	 * 获取当前月的最后一天
	 */
	public static String getDateLastDay(){
		//获取当前月最后一天
		Calendar ca = Calendar.getInstance();    
		ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));  
		String last = format.format(ca.getTime());
		return last;
	}

}
