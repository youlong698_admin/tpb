package com.ced.sip.common.utils;

import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;



/**
 * 重写SPRING 监听器
 * @ClassName: ApplicationContextListener
 * @author zhc
 * @date 2013-07-31
 */
public class ApplicationContextListener extends ContextLoaderListener {
	private static final Log logger = LogFactory.getLog(ApplicationContextListener.class);

	
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
        
		AppServiceHelper.setApplicationContext(applicationContext);
    	logger.info(" - Put Spring Context to AppServiceHelper");
	}
	
	
	public void contextDestroyed(ServletContextEvent event) {
		super.contextDestroyed(event);
	}


}
