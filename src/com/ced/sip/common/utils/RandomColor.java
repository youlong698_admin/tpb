package com.ced.sip.common.utils;

import java.util.Random;

/**
 * 随机生成颜色代码
 */
public class RandomColor {

	/**
	 * @Title:main
	 * @Description:生成随机颜色
	 * @param:@param args
	 * @return: void
	 * @throws
	 */
	public static String generateColor(int i) 
	{
		String color = "";
		if(i==0) color="#FFF0F5";
		else if(i==1) color="#FFE4B5";
		else if(i==2) color="#FFD700";
		else if(i==3) color="#FFC0CB";
		else if(i==4) color="#FFA07A";
		else if(i==5) color="#FF82AB";
		else if(i==6) color="#FF3E96";
		else if(i==7) color="#FF00FF";
		else if(i==8) color="#F5F5F5";
		else if(i==9) color="#EECFA1";
		else if(i==10) color="#E066FF";
		else if(i==11) color="#D6D6D6";
		else if(i==12) color="#CD2990";
		else if(i==13) color="#C67171";
		//System.out.println(color);
		return color;  
	}
}
