package com.ced.sip.common.utils;

import java.math.BigDecimal;

public class NumberFormatUtil {

	/**
	 * Double类型数值的比较
	 * @param db1,db2
	 * @return
	 */
	public static int compareDoubleValue(Double db1,Double db2){
		int flag = 0;
		BigDecimal d1 = new BigDecimal(db1); 
		BigDecimal d2 = new BigDecimal(db2); 
		int i = d1.compareTo(d2);
		switch (i) {
			case -1:
				flag = -1;//d1小于d2
				break;
			case 0:
				flag = 0;//d1等于d2
				break;
			case 1:
				flag = 1;//d1大于d2
				break;
		}
		return flag;
	}
}
