/**
 * 字典类型状态定义
 * @author luguanglei
 */
package com.ced.sip.common;

public class DictStatus {
	
	/************************** 所属模块 系统管理1700  **************************/
	/** 字典表类型: - 常用审批意见  ‘1701’ */
	public static final String COMMON_DICT_TYPE_1701 = "1701" ;
	
	/** 字典表类型: - 报价类型  ‘1702’ */
	public static final String COMMON_DICT_TYPE_1702 = "1702" ;
	
	/** 字典表类型: - 报价列类型  ‘1703’ */
	public static final String COMMON_DICT_TYPE_1703 = "1703" ;
	
	/** 字典表类型: - 学历  ‘1706’ */
	public static final String COMMON_DICT_TYPE_1706 = "1706" ;
	
	/** 字典表类型: - 职称  ‘1704’ */
	public static final String COMMON_DICT_TYPE_1704 = "1704" ;

	/** 字典表类型: - 竞价快捷回复  ‘1705’ */
	public static final String COMMON_DICT_TYPE_1705 = "1705" ;
	

	/** 字典表类型: - 所属行业  ‘1707’ */
	public static final String COMMON_DICT_TYPE_1707 = "1707" ;	

	/** 字典表类型: - 经营模式  ‘1708’ */
	public static final String COMMON_DICT_TYPE_1708 = "1708" ;

	/** 字典表类型: - 合同字典  ‘1709’ */
	public static final String COMMON_DICT_TYPE_1709 = "1709" ;	
	

	/** 字典表类型: - 计量单位  ‘1711’ */
	public static final String COMMON_DICT_TYPE_1711 = "1711" ;	
	

	/** 字典表类型: - 岗位  ‘1717’ */
	public static final String COMMON_DICT_TYPE_1717 = "1717" ;	
	
	
	/************************** 所属模块 供应商管理  **************************/
	/** 字典表类型: - 企业性质 ‘1710’ */
	public static final String COMMON_DICT_TYPE_1710 = "1710" ;
	/** 字典表类型: - 供应商级别 ‘1712’ */
	public static final String COMMON_DICT_TYPE_1712 = "1712" ;
	/** 字典表类型: - 供应商状态 ‘1713’ */
	public static final String COMMON_DICT_TYPE_1713 = "1713" ;
	/** 字典表类型: - 注册代码证类型 ‘1714’ */
	public static final String COMMON_DICT_TYPE_1714 = "1714" ;
	
	
	/** 字典表类型: -  采购项目的异常类别 ‘1730’ */
	public static final String COMMON_DICT_TYPE_1740 = "1740" ;

}
