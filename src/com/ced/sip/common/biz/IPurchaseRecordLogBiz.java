package com.ced.sip.common.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.entity.PurchaseRecordLog;

public interface IPurchaseRecordLogBiz {

	/**
	 * 根据主键获得采购记录日志表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public abstract PurchaseRecordLog getPurchaseRecordLog(Long id)
			throws BaseException;

	/**
	 * 获得采购记录日志表实例
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @return
	 * @throws BaseException 
	 */
	public abstract PurchaseRecordLog getPurchaseRecordLog(
			PurchaseRecordLog purchaseRecordLog) throws BaseException;

	/**
	 * 添加采购记录日志信息
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @author zy 2015-01-30
	 * @throws BaseException 
	 */
	public abstract void savePurchaseRecordLog(
			PurchaseRecordLog purchaseRecordLog) throws BaseException;

	/**
	 * 更新采购记录日志表实例
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @throws BaseException 
	 */
	public abstract void updatePurchaseRecordLog(
			PurchaseRecordLog purchaseRecordLog) throws BaseException;

	/**
	 * 删除采购记录日志表实例
	 * @param id 主键数组
	 * @author zy 2015-01-30
	 * @throws BaseException 
	 */
	public abstract void deletePurchaseRecordLog(String id)
			throws BaseException;

	/**
	 * 删除采购记录日志表实例
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @throws BaseException 
	 */
	public abstract void deletePurchaseRecordLog(
			PurchaseRecordLog purchaseRecordLog) throws BaseException;

	/**
	 * 删除采购记录日志表实例
	 * @param id 主键数组
	 * @author zy 2015-01-30
	 * @throws BaseException 
	 */
	public abstract void deletePurchaseRecordLogs(String[] id)
			throws BaseException;

	/**
	 * 获得所有采购记录日志表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getPurchaseRecordLogList(RollPage rollPage)
			throws BaseException;

	/**
	 * 获得所有采购记录日志表数据集
	 * @param purchaseRecordLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getPurchaseRecordLogList(
			PurchaseRecordLog purchaseRecordLog) throws BaseException;
	/**
	 * 获得所有采购记录日志表数据集
	 * @param purchaseRecordLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getPurchaseRecordLogListByContion(
			String conditon) throws BaseException;

	/**
	 * 获得所有采购记录日志表数据集
	 * @param rollPage 分页对象
	 * @param purchaseRecordLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public abstract List getPurchaseRecordLogList(RollPage rollPage,
			PurchaseRecordLog purchaseRecordLog) throws BaseException;
	/**
	 * 删除采购记录日志表实例
	 * @throws BaseException 
	 */
	public void deletePurchaseRecordLogByBidCode(String bidCode) throws BaseException;

}