package com.ced.sip.common.biz;

import java.util.List;

import com.ced.base.exception.BaseException;

public interface ISysSetBiz {
	/**
	 * 查询所有的表名信息
	 * @return
	 * @throws BaseException
	 */
	public List<Object> getAllTableList() throws BaseException;
	/**
	 * 根据表名查询表中的字段信息
	 * @param tableName
	 * @return
	 * @throws BaseException
	 */
	public List<Object> getAllColums(String tableName) throws BaseException;

}
