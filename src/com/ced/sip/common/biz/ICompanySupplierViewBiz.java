package com.ced.sip.common.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.entity.CompanySupplierView;

public interface ICompanySupplierViewBiz {
	/**
	 * 获得所有公司表数据集
	 * @param rollPage 分页对象
	 * @param sysCompany 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getCompanySupplierViewList(RollPage rollPage, CompanySupplierView companySupplierView)
			throws BaseException;
	/**
	 * 获得公司表数据集
	 * sysCompany 公司表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract CompanySupplierView getCompanySupplierViewByCompanySupplierView(CompanySupplierView companySupplierView) throws BaseException ;
	/**
	 * 获得公司数据集总数
	 * @param companySupplierView
	 * @return
	 * @throws BaseException
	 */
	abstract int countCompanySupplierViewList(CompanySupplierView companySupplierView) throws BaseException;
}
