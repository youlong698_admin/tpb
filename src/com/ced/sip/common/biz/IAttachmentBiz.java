package com.ced.sip.common.biz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ced.base.entity.BaseAttachment;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.entity.Attachment;

public interface IAttachmentBiz {

	/**
	 * 根据主键获得附件表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract Attachment getAttachment(Long id) throws BaseException;

	/**
	 * 添加附件信息
	 * @param Attachment 附件表实例
	 * @throws BaseException 
	 */
	abstract void saveAttachment(Attachment Attachment) throws BaseException;

	/**
	 * 更新附件表实例
	 * @param Attachment 附件表实例
	 * @throws BaseException 
	 */
	abstract void updateAttachment(Attachment Attachment) throws BaseException;

	/**
	 * 删除附件表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteAttachment(String id) throws BaseException;

	/**
	 * 删除附件表实例
	 * @param Attachment 附件表实例
	 * @throws BaseException 
	 */
	abstract void deleteAttachment(Attachment Attachment) throws BaseException;

	/**
	 * 删除附件表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void deleteAttachments(String[] id) throws BaseException;

	/**
	 * 获得所有附件表数据集
	 * @param Attachment 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getAttachmentList(Attachment Attachment) throws BaseException;

	/**
	 * 获得所有附件表数据集
	 * @param Attachment 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract Map<String,Object> getAttachmentMap(Attachment Attachment) throws BaseException;

	/**
	 * 
	 * @param formFile
	 * @param parentId
	 * @param clazz
	 * @param filed当附件为多种类型时，使用.对象的属性名称
	 * @throws BaseException
	 */
	//	public void saveAttachment(FormFile formFile, Long parentId, Class clazz, String filed)
	//			throws BaseException {
	abstract void saveAttachment(File formFile, Long parentId, Class clazz,
			String filed) throws BaseException;

	/**
	 * 获取附件的上传路径-相对 
	 * @param attachment
	 * @return
	 */
	abstract String getAttachmentPath(Attachment attachment);
	
	/**
	 * 获取ftp附件下载的url
	 * @param attachmentId
	 * @return
	 */
	abstract String getFtpAttachmentUrl(Attachment attachment,
			HttpServletRequest request);


	/**
	 * 获取附件的真实路径 上传到本机需要知道真实路径
	 * @param attachment 附件对象实例 
	 * @param abspath 相对开始路径, 从配置文件中取
	 * @author ted 2011-05-28 
	 * @return String 完整的真实路径
	 */
	abstract String getAttachmentRealPath(Attachment attachment, String abspath);

	/**
	 * 获取附件的url
	 * 
	 * @param attachmentId
	 * @return
	 */
	abstract String getAttachmentUrl(Attachment attachment, HttpServletRequest request) ;
	
	/**
	 * 获取页面附件的URL地址
	 * @param attachments 附件
	 * @param viewType 显示类型 0 查看 1 修改
	 * @param request 
	 * @author ted Ted 2011-01-05 
	 * @return
	 */
	abstract String getAttachmentPageUrl( List attachments, String viewType, HttpServletRequest request ) ;
	
	/**
	 * 获取页面附件的URL地址（完整显示文件名）
	 * @param attachments 附件
	 * @param viewType 显示类型 0 查看 1 修改
	 * @param request 
	 * @author ted Ted 2011-01-05 
	 * @return
	 */
	public String getAttachmentPageAllUrl( List attachments, String viewType, HttpServletRequest request );
	
	/**
	 * 获取页面附件的URL地址 (多个附件类型增加删除方法类型)
	 * @param attachments 附件
	 * @param viewType 显示类型 0 查看 1 修改
	 * @param methodType 删除方法(js) 
	 * @param request 
	 * @author ted Ted 2011-01-05 
	 * @return
	 */
	abstract String getAttachmentPageUrl( List attachments, String viewType, String methodType, HttpServletRequest request ) ;
	
	/**
	 * 附件上传  多文件上传
	 * @param att 附件对象实例 
	 * @author lgl
	 * @return String 完整的真实路径
	 */
	abstract void saveAttachmentAndUpload( BaseAttachment att ) throws FileNotFoundException, Exception ;
	/**
	 * 附件上传  单文件上传
	 * @param att 附件对象实例 
	 * @author lgl 
	 * @return String 完整的真实路径
	 */
	abstract void saveAttachmentAndUploadOne( BaseAttachment att ) throws FileNotFoundException, Exception ;
	
	
	/**
	 * 获取ftp附件完整显示url
	 * @param attachment 
	 * @author ted 2013-01-16 
	 * @return
	 */
	abstract String getFtpAttachmentViewUrl(Attachment attachment ) ;


	/*** 
	 * 删除附件表实例
	 * @param att 附件对象实例 
	 * @author lgl 
	 * @return String 完整的真实路径
	 * @throws BaseException 
	 */
	abstract void deleteAttachments(String attachmentType,Long attachmentTypeId,String attachmentField) throws BaseException;
}