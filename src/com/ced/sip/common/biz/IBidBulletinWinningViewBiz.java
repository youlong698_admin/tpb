package com.ced.sip.common.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.entity.BidBulletinWinningView;

public interface IBidBulletinWinningViewBiz {
	/**
	 * 获得所有公告或公示表数据集
	 * @param rollPage 分页对象
	 * @param bidBulletinWinningView 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List getBidBulletinWinningViewList(RollPage rollPage, BidBulletinWinningView bidBulletinWinningView,Map<String,Object> map)
			throws BaseException;
	/**
	 * 获得公告或公示表数据集
	 * bidBulletinWinningView 公告或公示表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract BidBulletinWinningView getBidBulletinWinningViewByBidBulletinWinningView(BidBulletinWinningView bidBulletinWinningView) throws BaseException ;
	
}
