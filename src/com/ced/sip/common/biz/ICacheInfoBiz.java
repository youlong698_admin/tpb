package com.ced.sip.common.biz;

import java.util.List;
import java.util.Map;

import com.ced.base.exception.BaseException;

public interface ICacheInfoBiz {

	/**
	 * 取得指定缓存的总数
	 * @param cacheName 缓存名称 
	 * @return
	 */
	abstract int getCacheInfoSize( String cacheName ) ;
	
	/**
	 * 转换缓存信息为Map方式 key: id value: po
	 * @param cacheName 缓存名称 
	 * @return
	 */
	abstract Map convertCacheInfoToMap( Object cacheName ) ;
	/**
	 * 转换缓存信息为list
	 * @param cacheName 缓存名称 
	 * @return
	 */
	public List convertCacheInfoToList( Object cacheName );
	/**
	 * 转换缓存信息为Map方式 key: id value: po
	 * @param cacheName 缓存名称 
	 * @return
	 */
	abstract Map convertCacheInfoToMap( Object keyValue, Object cacheName ) ;
	
	/**
	 * 初始化数据字典数据同步到内存
	 * @param initType 同步类型 
	 * @author Ted 2010-01-05
	 * @return
	 * @throws BaseException 
	 */
	abstract List initCacheInfos(String initType) throws BaseException;
	
	/**
	 * 获得Sequence的下一个值
	 * @return
	 * @throws BaseException 
	 */
	public String getSequenceNextValue(String seqName) throws BaseException;
	

	/**
	 * remove 缓存
	 * @return
	 * @throws BaseException 
	 */
	public void removeCache( Object keyValue, Object cacheName) throws BaseException;
	/**
	 * update 缓存
	 * @return
	 * @throws BaseException 
	 */
	public void updateCache( Object keyValue, Object cacheName,Object object) throws BaseException;
	
	/**
	 * add 缓存
	 * @return
	 * @throws BaseException 
	 */
	public void addCache( Object keyValue, Object cacheName,Object object) throws BaseException;
	
	
}