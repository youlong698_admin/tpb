﻿package com.ced.sip.common.biz.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.biz.ICacheInfoBiz;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Dictionary;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.system.entity.Users;
 
public class CacheInfoBizImpl extends BaseBizImpl implements ICacheInfoBiz   {
	 
	CacheManager manager;
	
	/**
	 * 取得指定缓存的总数
	 * @param cacheName 缓存名称 
	 * @return
	 */
	public int getCacheInfoSize( String cacheName ) {
		return manager.getEhcache( cacheName ).getSize() ;
	}
	
	/**
	 * 转换缓存信息为Map方式 key: id value: po
	 * @param cacheName 缓存名称 
	 * @return
	 */
	public Map convertCacheInfoToMap( Object cacheName ) {
		Map map 		= new TreeMap();
		Ehcache cache 	= manager.getEhcache( cacheName.toString() ) ;
		Element element = null ;
		
		if( cache != null ) {
			List gList = cache.getKeys() ;
			for ( int k=0; k<gList.size(); k++ ) {
				element =  cache.get( gList.get(k) ) ;
				if( element != null ) {
					map.put( element.getObjectKey(), element.getObjectValue() ) ;
				}
		    }
		}
		return map ;
	}
	/**
	 * 转换缓存信息为list
	 * @param cacheName 缓存名称 
	 * @return
	 */
	public List convertCacheInfoToList( Object cacheName ) {
		List list 		= new ArrayList();
		Ehcache cache 	= manager.getEhcache( cacheName.toString() ) ;
		Element element = null ;
		
		if( cache != null ) {
			List gList = cache.getKeys() ;
			for ( int k=0; k<gList.size(); k++ ) {
				element =  cache.get( gList.get(k) ) ;
				if( element != null ) {
					list.add(element.getObjectValue() ) ;
				}
		    }
		}
		return list ;
	}
	
	/**
	 * 转换缓存信息为Map方式 key: id value: po
	 * @param cacheName 缓存名称 
	 * @return
	 */
	public Map convertCacheInfoToMap( Object keyValue, Object cacheName ) {
		Map map 		= new TreeMap();
		Ehcache cache 	= manager.getEhcache( cacheName.toString() ) ;
		Element element = null ;
		
		element =  cache.get( keyValue ) ;
		if( element != null ) {
			map.put( element.getObjectKey(), element.getObjectValue() ) ;
		}
		
		return map ;
	}
	
	/**
	 * 初始化数据字典数据同步到内存
	 * @param cacheName 缓存名称 
	 * @return
	 * @throws BaseException 
	 */
	public List initCacheInfos( String cacheName ) throws BaseException{
		Element element = null ;
		List gList 		= new ArrayList();
		
		Ehcache cache 	= manager.getEhcache( cacheName ) ;
		cache.removeAll() ;
		
		if( cacheName !=null && cacheName.length()>0 ) {
			
			if(TableStatus.BASE_DATA_TYPE_01.equals( cacheName ) ) { 		//用户表信息
				Users user ;
				
				gList =  this.getObjects( " from Users u order by u.userId asc " ) ;
				//部门经理判断
				List<Departments> list_dept = this.getObjects( "select new Departments(t.depLeaderId,t.depLeaderCn)  from Departments t " ) ;
				//分管领导判断
				List<Departments> list_com_dept = this.getObjects( "select new Departments(t.compLeaderId,t.compLeaderCn)  from Departments t" ) ;
				//高层领导判断
				List list=this.getObjects( "select distinct usr from Users usr,OrgUser de  where usr.userName=de.userName and de.orgCode ='"+TableStatus.Leader+"' and usr.isUsable = '0' order by usr.userId desc ") ;
				
				if( gList.size()>0 ) {
					for (Iterator<Users> iter = gList.iterator(); iter.hasNext(); ) {
						user =  iter.next() ;
						int userType=3;
							
							for(Departments departments:list_dept){
								if(StringUtil.convertNullToBlank(departments.getDepLeaderId()).equals(user.getUserId())){
									userType=1;
									break;
								}
							}
							for(Departments departments:list_com_dept){
								if(StringUtil.convertNullToBlank(departments.getDepLeaderId()).equals(user.getUserId())){
									userType=4;
									break;
								}
							}
							for(int i=0;i<list.size();i++){
								Users ur = (Users) list.get(i);
								if(StringUtil.convertNullToBlank(ur.getUserName()).equals(user.getUserName())){
									userType=2;
									break;
								}
				          }

							user.setUserType(userType);
					        cache.put( initElement( user.getUserId() , user ) ) ;
					        cache.put( initElement( user.getUserName() , user ) ) ;
					}
				}
			}else if(TableStatus.BASE_DATA_TYPE_02.equals( cacheName ) ) { //部门表信息
				Departments depart ;
				
				gList = this.getObjects( " from Departments order by length(selflevCode),deptOrder" ) ;
				
				if( gList.size()>0 ) {
					for (Iterator<Departments> iter = gList.iterator(); iter.hasNext(); ) {
						depart 	=  iter.next() ;
						cache.put( initElement( depart.getDepId() , depart ) ) ;
				    }
				}
				
			}else if(TableStatus.BASE_DATA_TYPE_03.equals( cacheName ) ) { 	//字典表信息
				Dictionary dict ;
				gList = this.getObjects( " from Dictionary t where t.isUsable = '0' order by t.dictOrder  " ) ;
				if( gList.size()>0 ) {
					for (Iterator<Dictionary> iter = gList.iterator(); iter.hasNext(); ) {
						dict 	=  iter.next() ;
						if( cache.getKeys().contains( dict.getParentDictCode() ) ) {
							Element selement = cache.getQuiet( dict.getParentDictCode() ) ;
							
							List<Dictionary> dictList = (List<Dictionary>) selement.getValue();
							dictList.add( dict ) ;
							cache.put( initElement( dict.getParentDictCode() , dictList ) ) ;
							
						}else {
							List<Dictionary> dictList = new ArrayList<Dictionary>() ;
							dictList.add( dict ) ;
							cache.put( initElement( dict.getParentDictCode() , dictList ) ) ;
						}
				    }
				}
				
			}else if(TableStatus.BASE_DATA_TYPE_04.equals( cacheName ) ) { 	//系统配置表信息
				SystemConfiguration systemConfiguration ;
				gList = this.getObjects( " from SystemConfiguration t " ) ;
				if( gList.size()>0 ) {
					for (Iterator<SystemConfiguration> iter = gList.iterator(); iter.hasNext(); ) {
						systemConfiguration 	=  iter.next() ;
						cache.put( initElement( systemConfiguration.getComId() , systemConfiguration ) ) ;
				    }
				}
				
			}
		}
		
		return gList ;
	}
	
	/**
	 * 获得Sequence的下一个值
	 * @author luguanglei 2016-08-25
	 * @return
	 * @throws BaseException 
	 */
	public String getSequenceNextValue(String seqName) throws BaseException {  
	    return baseDao.getSequenceNextValue(seqName);  
	} 
	
	/**
	 * remove 缓存
	 * @return
	 * @throws BaseException 
	 */
	public void removeCache( Object keyValue, Object cacheName) throws BaseException{
		 Ehcache cache 	= manager.getEhcache( cacheName.toString() ) ;
		 cache.remove( keyValue ) ;
	}
	/**
	 * update 缓存
	 * @return
	 * @throws BaseException 
	 */
	public void updateCache( Object keyValue, Object cacheName,Object object) throws BaseException{
		 Ehcache cache 	= manager.getEhcache( cacheName.toString() ) ;
		 cache.remove( keyValue ) ;
		 cache.put( initElement( keyValue , object ) ) ;
	}
	/**
	 * add 缓存
	 * @return
	 * @throws BaseException 
	 */
	public void addCache( Object keyValue, Object cacheName,Object object) throws BaseException{
		 Ehcache cache 	= manager.getEhcache( cacheName.toString() ) ;
		 cache.put( initElement( keyValue , object ) ) ;
	}

	private Element initElement( Object obj1, Object obj2 ){
		return new Element( obj1 , obj2 ) ;
	}
	
	public CacheManager getManager() {
		return manager;
	}

	public void setManager(CacheManager manager) {
		this.manager = manager;
	}
}
