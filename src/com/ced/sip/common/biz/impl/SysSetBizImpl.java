package com.ced.sip.common.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.biz.ISysSetBiz;
import com.ced.sip.common.entity.Columns;
import com.ced.sip.common.entity.Tables;

public class SysSetBizImpl extends BaseBizImpl implements ISysSetBiz {

	
	public List<Object> getAllColums(String tableName) throws BaseException {
		String sql="select utc.column_name,utc.data_type,utc.data_length, nvl(utc.data_precision,1) data_precision,nvl(utc.data_scale,1) data_scale,ucc.comments " +
				"from user_tab_columns utc,user_col_comments ucc " +
				"where utc.COLUMN_NAME=ucc.column_name and utc.Table_Name = '"+tableName+"' " +
				"and ucc.Table_Name = '"+tableName+"' order by utc.column_id";
		return this.getObjectsList(Columns.class, sql);
	}

	
	public List<Object> getAllTableList() throws BaseException {
		String sql="select a.TABLE_NAME,b.COMMENTS from user_tables a,user_tab_comments b WHERE a.TABLE_NAME=b.TABLE_NAME order by a.TABLE_NAME";
		return this.getObjectsList(Tables.class, sql);
	}

}
