package com.ced.sip.common.biz.impl;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.biz.IDwrServiceBiz;
import com.ced.sip.common.utils.StringUtil;

public class DwrServiceBizImpl extends BaseBizImpl implements IDwrServiceBiz {
	
	
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名 可为空
	 * @param columnValue 列值  可为空
	 * @param usableColumnName 是否有效列名 可为空
	 * @param usableColumnValue 是否有效列值 可为空
	 * @param comId 公司ID
	 * @author Ted 2010-06-02 
	 * @return 0不存在 其它值存在 
	 */
	public int ifExitCodeInTableComId(  String tableName , String[] columnNames ,String columnValues[],Long comId) throws BaseException{
		int rValue = 0 ;
		
		if( tableName != null && tableName.length()>0 ) {
			StringBuffer sql= new StringBuffer(" select count(*) from " ).append( tableName ).append(" tb where tb.comId= "+comId) ;
			
			if( StringUtil.isNotBlank( columnNames) ){
				for( int i=0; i<columnNames.length; i++ ) {
					sql.append( " and tb." ).append( columnNames[i] ).append( " = '" ).append( columnValues[i] ).append( "' ") ;
				}
			}
			rValue = this.countObjects( sql.toString() ) ;
		}
		
		return rValue ;
	}
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名 可为空
	 * @param columnValue 列值  可为空
	 * @param usableColumnName 是否有效列名 可为空
	 * @param usableColumnValue 是否有效列值 可为空
	 * @author Ted 2010-06-02 
	 * @return 0不存在 其它值存在 
	 */
	public int ifExitCodeInTable(  String tableName , String[] columnNames ,String columnValues[]) throws BaseException{
		int rValue = 0 ;
		
		if( tableName != null && tableName.length()>0 ) {
			StringBuffer sql = new StringBuffer(" select count(*) from " ).append( tableName ).append(" tb where 1=1 ");
			
			if( StringUtil.isNotBlank( columnNames) ){
				for( int i=0; i<columnNames.length; i++ ) {
					sql.append( " and tb." ).append( columnNames[i] ).append( " = '" ).append( columnValues[i] ).append( "' ") ;
				}
			}
			rValue = this.countObjects( sql.toString() ) ;
		}
		
		return rValue ;
	}
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名,多列以逗号分割,如deptCode,isUsable
	 * @param columnValue 列值,多以逗号分割,如05,0
	 * @param queryColumnName  如 userId
	 * @return 0不存在 其它值存在 
	 */
	public int getIdInTable(String tableName , String[] columnNames ,String columnValues[],String queryColumnName) 
			throws BaseException {
		int rValue = 0 ;
		if( tableName != null && tableName.length()>0 ) {
			StringBuffer sql = new StringBuffer(" select "+queryColumnName+" from " ).append( tableName ).append(" tb where 1=1 ");
			
			if( StringUtil.isNotBlank( columnNames) ){
				for( int i=0; i<columnNames.length; i++ ) {
					sql.append( " and tb." ).append( columnNames[i] ).append( " = '" ).append( columnValues[i] ).append( "' ") ;
				}
			}
			rValue = this.countObjects( sql.toString() ) ;
		}
			return rValue ;
	}
	
	 
}
