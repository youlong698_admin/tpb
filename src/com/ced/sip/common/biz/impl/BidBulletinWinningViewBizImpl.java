package com.ced.sip.common.biz.impl;

import java.util.List;
import java.util.Map;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.biz.IBidBulletinWinningViewBiz;
import com.ced.sip.common.entity.BidBulletinWinningView;
import com.ced.sip.common.utils.StringUtil;

public class BidBulletinWinningViewBizImpl extends BaseBizImpl implements IBidBulletinWinningViewBiz {

	
	public BidBulletinWinningView getBidBulletinWinningViewByBidBulletinWinningView(
			BidBulletinWinningView bidBulletinWinningView) throws BaseException {
		StringBuffer hql = new StringBuffer(" from BidBulletinWinningView de where 1 = 1 " );
		if(StringUtil.isNotBlank(bidBulletinWinningView) ){
			if(StringUtil.isNotBlank(bidBulletinWinningView.getBbwId())){
				hql.append(" and de.bbwId =").append(bidBulletinWinningView.getBbwId()).append("");
			}
		}
		List list = this.getObjects( hql.toString() );
		bidBulletinWinningView = new BidBulletinWinningView();
		if(list!=null&&list.size()>0){
			bidBulletinWinningView = (BidBulletinWinningView)list.get(0);
		}
		return bidBulletinWinningView;
	}

	
	public List getBidBulletinWinningViewList(RollPage rollPage,
			BidBulletinWinningView bidBulletinWinningView,Map<String,Object> map) throws BaseException {
		StringBuffer hql = new StringBuffer("select de from BidBulletinWinningView de where 1=1 " );
		if(StringUtil.isNotBlank(bidBulletinWinningView) ){
			if(StringUtil.isNotBlank(bidBulletinWinningView.getTitle())){
				hql.append(" and (de.title like '%"+bidBulletinWinningView.getTitle()+"%' or de.bidCode like '%"+bidBulletinWinningView.getTitle()+"%' or de.content like '%"+bidBulletinWinningView.getTitle()+"%')");
			}
			if(StringUtil.isNotBlank(bidBulletinWinningView.getSysCompany())){
				hql.append(" and de.sysCompany like '%").append(bidBulletinWinningView.getBbwId()).append("%'");
			}			
			if(StringUtil.isNotBlank(map.get("startDate"))){
				hql.append(" and de.publishDate > to_date('").append(map.get("startDate")).append("','yyyy-MM-dd')");
			}
			if(StringUtil.isNotBlank(map.get("endDate"))){
				hql.append(" and de.publishDate < to_date('").append(map.get("endDate")).append("','yyyy-MM-dd')");
			}
		}
		int bidderType=(Integer)map.get("bidderType");
		if(bidderType==1){
			hql.append(" and de.buyWay = '00'");
		}else if(bidderType==2){
			hql.append(" and de.buyWay = '01'");
		}else if(bidderType==3){
			hql.append(" and de.buyWay = '02'");
		}
		int type=(Integer)map.get("type");
		if(type==1){
			hql.append(" and de.type=0 ");
		}else if(type==2){
			hql.append(" and de.type=1 ");
		}
		hql.append(" order by de.bbwId desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

}
