package com.ced.sip.common.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.biz.IPurchaseRecordLogBiz;
import com.ced.sip.common.entity.PurchaseRecordLog;
import com.ced.sip.common.utils.StringUtil;

public class PurchaseRecordLogBizImpl extends BaseBizImpl implements IPurchaseRecordLogBiz  {
	
	/**
	 * 根据主键获得采购记录日志表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	public PurchaseRecordLog getPurchaseRecordLog(Long id) throws BaseException {
		return (PurchaseRecordLog)this.getObject(PurchaseRecordLog.class, id);
	}
	
	/**
	 * 获得采购记录日志表实例
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @return
	 * @throws BaseException 
	 */
	public PurchaseRecordLog getPurchaseRecordLog( PurchaseRecordLog purchaseRecordLog ) throws BaseException {
		return (PurchaseRecordLog)this.getObject(PurchaseRecordLog.class, purchaseRecordLog.getPrlId() );
	}
	
	/**
	 * 添加采购记录日志信息
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @throws BaseException 
	 */
	public void savePurchaseRecordLog(PurchaseRecordLog purchaseRecordLog) throws BaseException{
		this.saveObject( purchaseRecordLog ) ;
	}
	
	/**
	 * 更新采购记录日志表实例
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @throws BaseException 
	 */
	public void updatePurchaseRecordLog(PurchaseRecordLog purchaseRecordLog) throws BaseException{
		this.updateObject( purchaseRecordLog ) ;
	}
	
	/**
	 * 删除采购记录日志表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deletePurchaseRecordLog(String id) throws BaseException {
		this.removeObject( this.getPurchaseRecordLog( new Long(id) ) ) ;
	}
	
	/**
	 * 删除采购记录日志表实例
	 * @param purchaseRecordLog 采购记录日志表实例
	 * @throws BaseException 
	 */
	public void deletePurchaseRecordLog(PurchaseRecordLog purchaseRecordLog) throws BaseException {
		this.removeObject( purchaseRecordLog ) ;
	}
	
	/**
	 * 删除采购记录日志表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	public void deletePurchaseRecordLogs(String[] id) throws BaseException {
		this.removeBatchObject(PurchaseRecordLog.class, id) ;
	}
	
	/**
	 * 获得所有采购记录日志表数据集
	 * @param rollPage 分页对象
	 * @return
	 * @throws BaseException 
	 */
	public List getPurchaseRecordLogList( RollPage rollPage  ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseRecordLog de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
	/**
	 * 获得所有采购记录日志表数据集
	 * @param purchaseRecordLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getPurchaseRecordLogList(  PurchaseRecordLog purchaseRecordLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseRecordLog de where 1 = 1 " );
		if(purchaseRecordLog!=null){
			if(StringUtil.isNotBlank(purchaseRecordLog.getOperatorName())){
				hql.append(" and de.operatorName ='").append(purchaseRecordLog.getOperatorName()).append("'");
			}
			if(StringUtil.isNotBlank(purchaseRecordLog.getRcId())){
				hql.append(" and de.rcId =").append(purchaseRecordLog.getRcId()).append("");
			}
			if(StringUtil.isNotBlank(purchaseRecordLog.getRmId())){
				hql.append(" and de.rmId =").append(purchaseRecordLog.getRmId()).append("");
			}
			if(StringUtil.isNotBlank(purchaseRecordLog.getOperateContent())){
				hql.append(" and de.operateContent like '%").append(purchaseRecordLog.getOperateContent()).append("%'");
			}
		}
		hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}

	/**
	 * 获得所有采购记录日志表数据集
	 * @param purchaseRecordLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getPurchaseRecordLogListByContion(String conditon)  throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseRecordLog de where 1 = 1 " );
		hql.append(conditon);
		hql.append(" order by de.id asc ");
		return this.getObjects( hql.toString() );
	}
	/**
	 * 获得所有采购记录日志表数据集
	 * @param rollPage 分页对象
	 * @param purchaseRecordLog 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getPurchaseRecordLogList( RollPage rollPage, PurchaseRecordLog purchaseRecordLog ) throws BaseException {
		StringBuffer hql = new StringBuffer(" from PurchaseRecordLog de where 1 = 1 " );

		hql.append(" order by de.id desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	/**
	 * 删除采购记录日志表实例
	 * @throws BaseException 
	 */
	public void deletePurchaseRecordLogByBidCode(String bidCode) throws BaseException {
		StringBuffer sql = new StringBuffer("delete from purchase_record_log t where t.bid_Code='"+bidCode+"' " );
		this.updateJdbcSql(sql.toString());
   }
}
