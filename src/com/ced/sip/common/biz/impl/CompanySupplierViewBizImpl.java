package com.ced.sip.common.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.biz.ICompanySupplierViewBiz;
import com.ced.sip.common.entity.CompanySupplierView;
import com.ced.sip.common.utils.StringUtil;

public class CompanySupplierViewBizImpl extends BaseBizImpl implements ICompanySupplierViewBiz {

	/**
	 * 获得公司表数据集
	 * sysCompany 公司表实例
	 * @return
	 * @throws BaseException 
	 */
	public CompanySupplierView getCompanySupplierViewByCompanySupplierView(
			CompanySupplierView companySupplierView) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CompanySupplierView de where 1 = 1 " );
		if(StringUtil.isNotBlank(companySupplierView) ){
			if(StringUtil.isNotBlank(companySupplierView.getCompName())){
				hql.append(" and de.compName like '%").append(companySupplierView.getCompName()).append("%'");
			}
		}
		hql.append(" order by de.scId desc ");
		List list = this.getObjects( hql.toString() );
		companySupplierView = new CompanySupplierView();
		if(list!=null&&list.size()>0){
			companySupplierView = (CompanySupplierView)list.get(0);
		}
		return companySupplierView;
	}

	/**
	 * 获得所有公司表数据集
	 * @param rollPage 分页对象
	 * @param sysCompany 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	public List getCompanySupplierViewList(RollPage rollPage,
			CompanySupplierView companySupplierView) throws BaseException {
		StringBuffer hql = new StringBuffer(" from CompanySupplierView de where 1 = 1 " );
		if(StringUtil.isNotBlank(companySupplierView) ){
			if(StringUtil.isNotBlank(companySupplierView.getCompName())){
				hql.append(" and de.compName like '%").append(companySupplierView.getCompName()).append("%'");
			}
			if(StringUtil.isNotBlank(companySupplierView.getLogo())){
				hql.append(" and de.logo is not null ");
			}
		}
		hql.append(" order by de.createTime desc ");
		return this.getObjects(rollPage, hql.toString() );
	}

	/**
	 * 获得公司数据集总数
	 * @param companySupplierView
	 * @return
	 * @throws BaseException
	 */
	public int countCompanySupplierViewList(
			CompanySupplierView companySupplierView) throws BaseException {
		StringBuffer hql = new StringBuffer("select count(de.csvId) from CompanySupplierView de where 1 = 1 " );
		if(StringUtil.isNotBlank(companySupplierView) ){
			if(StringUtil.isNotBlank(companySupplierView.getCompName())){
				hql.append(" and de.compName like '%").append(companySupplierView.getCompName()).append("%'");
			}
			if(StringUtil.isNotBlank(companySupplierView.getLogo())){
				hql.append(" and de.logo is not null ");
			}
		}
		return this.countObjects(hql.toString());
	}

}
