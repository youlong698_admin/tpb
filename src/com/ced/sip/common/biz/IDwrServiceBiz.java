package com.ced.sip.common.biz;

import com.ced.base.exception.BaseException;

public interface IDwrServiceBiz {

	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnNames 列名 可为空
	 * @param columnValues 列值  可为空
	 * @param comId 公司id
	 * @return 0不存在 其它值存在 
	 */
	public int ifExitCodeInTableComId(  String tableName , String[] columnNames ,String columnValues[],Long comId) 
		throws BaseException ;
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnNames 列名 可为空
	 * @param columnValues 列值  可为空
	 * @return 0不存在 其它值存在 
	 */
	public int ifExitCodeInTable(  String tableName , String[] columnNames ,String columnValues[]) 
		throws BaseException ;
	/**
	 * 公用方法-- 校验代码是否存在 
	 * @param tableName 表名(实体) 可为空, 为空则返回大于零的值
	 * @param columnName 列名,多列以逗号分割,如deptCode,isUsable
	 * @param columnValue 列值,多以逗号分割,如05,0
	 * @param queryColumnName  如 userId
	 * @return 0不存在 其它值存在 
	 */
	public int getIdInTable(String tableName , String[] columnNames ,String columnValues[],String queryColumnName) 
		throws BaseException ;
}