package com.ced.sip.common.entity;

import java.util.Date;

public class PurchaseRecordLog implements java.io.Serializable {

	// Fields

	private Long prlId;
	private Long rmId;
	private Long rcId;
	private String operatorId;
	private String operatorName;
	private String operatorNameCn;
	private Date operateDate;
	private String operateContent;
	private String bidNode;

	// Constructors

	/** default constructor */
	public PurchaseRecordLog() {
	}

	/** full constructor */
	public PurchaseRecordLog(Long prlId, Long rmId, Long rcId,
			String operatorId, String operatorName, Date operateDate,
			String operateContent,String bidNode) {
		this.prlId = prlId;
		this.rmId = rmId;
		this.rcId = rcId;
		this.operatorId = operatorId;
		this.operatorName = operatorName;
		this.operateDate = operateDate;
		this.operateContent = operateContent;
		if(bidNode!=null)bidNode=bidNode.replaceAll("\\d|\\s|\\.","");
		this.bidNode = bidNode;
	}

	// Property accessors

	public Long getPrlId() {
		return this.prlId;
	}

	public void setPrlId(Long prlId) {
		this.prlId = prlId;
	}
    
	public Long getRmId() {
		return rmId;
	}

	public void setRmId(Long rmId) {
		this.rmId = rmId;
	}

	public Long getRcId() {
		return rcId;
	}

	public void setRcId(Long rcId) {
		this.rcId = rcId;
	}

	public String getOperatorId() {
		return this.operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return this.operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public Date getOperateDate() {
		return this.operateDate;
	}

	public void setOperateDate(Date operateDate) {
		this.operateDate = operateDate;
	}

	public String getOperateContent() {
		return this.operateContent;
	}

	public void setOperateContent(String operateContent) {
		this.operateContent = operateContent;
	}

	public String getOperatorNameCn() {
		return operatorNameCn;
	}

	public void setOperatorNameCn(String operatorNameCn) {
		this.operatorNameCn = operatorNameCn;
	}	

	public String getBidNode() {
		return bidNode;
	}

	public void setBidNode(String bidNode) {
		if(bidNode!=null)bidNode=bidNode.replaceAll("\\d|\\s|\\.","");
		this.bidNode = bidNode;
	}

}