package com.ced.sip.common.entity;

import java.util.Date;

/** 
 * 类名称：采购方和供应商综合视图做公司介绍
 * 创建人：luguanglei 
 * 创建时间：2017-07-04
 */
public class CompanySupplierView implements java.io.Serializable {

	// 属性信息
	private Long csvId;	//主键ID
	private String compName;	 //采购单位名称
	private String shortName;	 //采购单位简称
	private String logo;	 //LOGO
	private String contact;	 //联系人
	private String mobilePhone;	 //联系人手机
	private Date createTime;    //创建时间
	private String compPhone;	 //公司电话
	private String compFax;	 //公司传真
	private String industryOwned; //所属行业
	private String managementModel;//经营模式
	private String province;//省
	private String city;//市
	private String companyProfile;//企业简介
	private String companyAddress;//企业地址
	private String companyWebsite;//企业网站
	private String registerFunds;
	private Date createDate;
	private String orgCode;
	private String registrationType;//注册代码证类型
	private String scopeBusiness;//经营范围
	private int type;
	
	private String industryOwnedCn; //所属行业
	private String managementModelCn;//经营模式
	
	
	
	public CompanySupplierView() {
		super();
	}
	public Long getCsvId() {
		return csvId;
	}
	public void setCsvId(Long csvId) {
		this.csvId = csvId;
	}
	public String getCompName(){
	   return  compName;
	} 
	public void setCompName(String compName) {
	   this.compName = compName;
    }
	public String getShortName(){
	   return  shortName;
	} 
	public void setShortName(String shortName) {
	   this.shortName = shortName;
    }
	public String getLogo(){
	   return  logo;
	} 
	public void setLogo(String logo) {
	   this.logo = logo;
    }
	public String getContact(){
	   return  contact;
	} 
	public void setContact(String contact) {
	   this.contact = contact;
    }
	public String getMobilePhone(){
	   return  mobilePhone;
	} 
	public void setMobilePhone(String mobilePhone) {
	   this.mobilePhone = mobilePhone;
    }
	public Date getCreateTime(){
	   return  createTime;
	} 
	public void setCreateTime(Date createTime) {
	   this.createTime = createTime;
    }	
	public String getCompPhone(){
	   return  compPhone;
	} 
	public void setCompPhone(String compPhone) {
	   this.compPhone = compPhone;
    }
	public String getCompFax(){
	   return  compFax;
	} 
	public void setCompFax(String compFax) {
	   this.compFax = compFax;
    }
	public String getIndustryOwned() {
		return industryOwned;
	}

	public void setIndustryOwned(String industryOwned) {
		this.industryOwned = industryOwned;
	}

	public String getManagementModel() {
		return managementModel;
	}

	public void setManagementModel(String managementModel) {
		this.managementModel = managementModel;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getRegisterFunds() {
		return registerFunds;
	}

	public void setRegisterFunds(String registerFunds) {
		this.registerFunds = registerFunds;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getRegistrationType() {
		return registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public String getScopeBusiness() {
		return scopeBusiness;
	}

	public void setScopeBusiness(String scopeBusiness) {
		this.scopeBusiness = scopeBusiness;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	public String getIndustryOwnedCn() {
		return industryOwnedCn;
	}
	public void setIndustryOwnedCn(String industryOwnedCn) {
		this.industryOwnedCn = industryOwnedCn;
	}
	public String getManagementModelCn() {
		return managementModelCn;
	}
	public void setManagementModelCn(String managementModelCn) {
		this.managementModelCn = managementModelCn;
	}
	
}