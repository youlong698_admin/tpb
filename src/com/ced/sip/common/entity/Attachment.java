package com.ced.sip.common.entity;

import java.io.Serializable;

import com.ced.base.entity.BaseAttachment;


public class Attachment extends BaseAttachment implements Serializable {
	private int hashValue = 0;

	private Long attachmentId;

	private String attachmenetName;

	private String attachmentType;

	private Long attachmentTypeId;

	private String attachmentField;

	private String writer;
	
	private java.util.Date writeDate;

	private String remark;
	
	private String remark1;
	
	private String remark2;

	private Long comId;
	
	
	/** 非实例化属性 */
	// 路径
	private String contextPath ;
	

	/** uuid */
	private String uuid ;
	

	/** 文件格式 */
	private String fileType ;
 
	public Attachment() {
	}
	public Attachment(Long attachmentId) {
		this.setAttachmentId(attachmentId);
	}

	public Attachment(Long attachmentTypeId, String attachmentField) {
		super();
		this.attachmentTypeId = attachmentTypeId;
		this.attachmentField = attachmentField;
	}
	
	public Attachment(Long attachmentTypeId, String attachmentField, String writer) {
		super();
		this.attachmentTypeId = attachmentTypeId;
		this.attachmentField = attachmentField;
		this.writer = writer;
	}

	public Long getAttachmentId() {
		return attachmentId;
	}


	public void setAttachmentId(Long attachmentId) {
		this.hashValue = 0;
		this.attachmentId = attachmentId;
	}

	
	public String getAttachmenetName() {
		return this.attachmenetName;
	}

	
	public void setAttachmenetName(String attachmenetName) {
		this.attachmenetName = attachmenetName;
	}


	public String getAttachmentType() {
		return this.attachmentType;
	}


	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}


	public Long getAttachmentTypeId() {
		return this.attachmentTypeId;
	}


	public void setAttachmentTypeId(Long attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}


	public boolean equals(Object rhs) {
		if (rhs == null)
			return false;
		if (!(rhs instanceof Attachment))
			return false;
		Attachment that = (Attachment) rhs;
		if (this.getAttachmentId() != null && that.getAttachmentId() != null) {
			if (!this.getAttachmentId().equals(that.getAttachmentId())) {
				return false;
			}
		}
		return true;
	}


	public int hashCode() {
		if (this.hashValue == 0) {
			int result = 17;
			int attachmentIdValue = this.getAttachmentId() == null ? 0 : this
					.getAttachmentId().hashCode();
			result = result * 37 + attachmentIdValue;
			this.hashValue = result;
		}
		return this.hashValue;
	}

	public java.util.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.util.Date writeDate) {
		this.writeDate = writeDate;
	}

	public String getAttachmentField() {
		return attachmentField;
	}

	public void setAttachmentField(String attachmentField) {
		this.attachmentField = attachmentField;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
	
}
