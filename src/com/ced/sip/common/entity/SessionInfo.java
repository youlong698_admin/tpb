package com.ced.sip.common.entity;

import java.util.List;

import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.Users;

/**
 * @Date 2008-7-3
 * @Author ted
 * @Description：用户登录后的信息
 */
public class SessionInfo {
	
	/** 登录方式*/
	private int  LoginType;
	
	/** 登陆用户信息*/
	private Users users;
	
	private SessionUser sessionUser; 
	
	/** 登陆用户信息*/
	private Departments dept;

	
	/** 当前登录用户可以根据群组可以看到的组织机构信息*/
	private List<Departments> deptIdList;
	
	/** 当前登录用户为领导的部门*/
	private List<Departments> leaderList;
	
	/** 当前登录用户可以根据群组可以看到的组织机构信息的实体*/
	private List<Departments> departList;
	
	
	

	
	/**供应商ID信息**/ 
	private Long supplierId;
	private String supplierLoginName;	
	/**供应商姓名信息**/ 
	private String supplierName;
	
	/**供应商登录信息**/
	private SupplierInfo supplierInfo;
	

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}


	public Departments getDept() {
		return dept;
	}

	public void setDept(Departments dept) {
		this.dept = dept;
	}

	public List<Departments> getDeptIdList() {
		return deptIdList;
	}

	public void setDeptIdList(List<Departments> deptIdList) {
		this.deptIdList = deptIdList;
	}

	public List<Departments> getDepartList() {
		return departList;
	}

	public void setDepartList(List<Departments> departList) {
		this.departList = departList;
	}

	public List<Departments> getLeaderList() {
		return leaderList;
	}

	public void setLeaderList(List<Departments> leaderList) {
		this.leaderList = leaderList;
	}

	public SessionUser getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(SessionUser sessionUser) {
		this.sessionUser = sessionUser;
	}
	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierLoginName() {
		return supplierLoginName;
	}

	public void setSupplierLoginName(String supplierLoginName) {
		this.supplierLoginName = supplierLoginName;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}

	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}

	public int getLoginType() {
		return LoginType;
	}

	public void setLoginType(int loginType) {
		LoginType = loginType;
	}
	

}
