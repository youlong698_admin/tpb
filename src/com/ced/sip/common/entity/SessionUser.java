package com.ced.sip.common.entity;
/**
 * 通用类，如果是内部用户登录，存放的就是内部用户信息，如果是专家登陆，存放的就是专家信息
 * 作用：日志监听、异常监听、登录所用
 * @author luguanglei
 *
 */
public class SessionUser {
   private Long id;
   private String name;
   private String loginName;
   private int type;
   private Long comId;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Long getComId() {
		return comId;
	}
	public void setComId(Long comId) {
		this.comId = comId;
	}
   
}
