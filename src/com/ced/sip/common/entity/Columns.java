package com.ced.sip.common.entity;

import java.math.BigDecimal;

public class Columns {
  private String columnName;
  private String dataType;
  private BigDecimal dataLength;
  private BigDecimal dataPrecision;
  private BigDecimal dataScale;
  private String comments;

public String getColumnName() {
	return columnName;
}
public void setColumnName(String columnName) {
	this.columnName = columnName;
}
public String getDataType() {
	return dataType;
}
public void setDataType(String dataType) {
	this.dataType = dataType;
}
public BigDecimal getDataLength() {
	return dataLength;
}
public void setDataLength(BigDecimal dataLength) {
	this.dataLength = dataLength;
}
public BigDecimal getDataPrecision() {
	return dataPrecision;
}
public void setDataPrecision(BigDecimal dataPrecision) {
	this.dataPrecision = dataPrecision;
}
public BigDecimal getDataScale() {
	return dataScale;
}
public void setDataScale(BigDecimal dataScale) {
	this.dataScale = dataScale;
}
public String getComments() {
	return comments;
}
public void setComments(String comments) {
	this.comments = comments;
}
  
}
