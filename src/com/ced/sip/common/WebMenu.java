package com.ced.sip.common;

public class WebMenu {
	private Long wmId;
	private String menuCode;
	private String menuName;
	public Long getWmId() {
		return wmId;
	}
	public void setWmId(Long wmId) {
		this.wmId = wmId;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	

}
