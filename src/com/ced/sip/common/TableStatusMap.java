/**
 * 公用状态定义
 * @author luguanglei
 */
package com.ced.sip.common;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author luguanglei
 *
 */
public class TableStatusMap {
	/**
	 * 计划类型Map
	 */
	public static Map<String, String> rmType = new LinkedHashMap<String, String>();
	static{
		rmType.put(TableStatus.RM_TYPE_00,TableStatus.RM_TYPE_00_TEXT);
		rmType.put(TableStatus.RM_TYPE_01,TableStatus.RM_TYPE_01_TEXT);
	}
	/**
	 * 采购类型Map
	 */
	public static Map<String, String> buyType = new LinkedHashMap<String, String>();
	static{
		buyType.put(TableStatus.BUY_TYPE_0,TableStatus.BUY_TYPE_0_TEXT);
		buyType.put(TableStatus.BUY_TYPE_1,TableStatus.BUY_TYPE_1_TEXT);
	}
	/**
	 * 状态Map
	 */
	public static Map<String, String> statusMap = new LinkedHashMap<String, String>();
	static{
		statusMap.put(TableStatus.STATUS_0,TableStatus.STATUS_0_TEXT);
		statusMap.put(TableStatus.STATUS_1,TableStatus.STATUS_1_TEXT);
	}
	/**
	 * 采购方式Map
	 */
	public static Map<String,String> purchaseWay = new LinkedHashMap<String,String>();
	static{
		purchaseWay.put(TableStatus.PURCHASE_WAY_00,TableStatus.PURCHASE_WAY_00_TEXT);
		purchaseWay.put(TableStatus.PURCHASE_WAY_01,TableStatus.PURCHASE_WAY_01_TEXT);
		purchaseWay.put(TableStatus.PURCHASE_WAY_02,TableStatus.PURCHASE_WAY_02_TEXT);
		//purchaseWay.put(TableStatus.PURCHASE_WAY_08,TableStatus.PURCHASE_WAY_08_TEXT);
	}
	/**
	 * 供应商选择方式Map
	 */
	public static Map<String,String> supplierType = new LinkedHashMap<String,String>();
	static{
		supplierType.put(TableStatus.SUPPLIER_TYPE_00,TableStatus.SUPPLIER_TYPE_00_TEXT);
		supplierType.put(TableStatus.SUPPLIER_TYPE_01,TableStatus.SUPPLIER_TYPE_01_TEXT);
	}
	/**
	 * 评标专家类型Map
	 */
	public static Map<String,String> judgeType = new HashMap<String,String>();
	static{
		judgeType.put(TableStatus.Expert_Judge_Type_Tech,TableStatus.Expert_Judge_Type_Tech_Text);
		judgeType.put(TableStatus.Expert_Judge_Type_Business,TableStatus.Expert_Judge_Type_Business_Text);
		judgeType.put(TableStatus.Expert_Judge_Type_Colligation,TableStatus.Expert_Judge_Type_Colligation_Text);
	}
	
	/**
	 * 报价汇总表状态Map
	 */
	public static Map<String,String> bidPriceMap = new HashMap<String,String>();
	static{
		bidPriceMap.put(TableStatus.PRICE_STATUS_01, TableStatus.PRICE_STATUS_01_TEXT);
		bidPriceMap.put(TableStatus.PRICE_STATUS_02, TableStatus.PRICE_STATUS_02_TEXT);
		bidPriceMap.put(TableStatus.PRICE_STATUS_03, TableStatus.PRICE_STATUS_03_TEXT);
		bidPriceMap.put(TableStatus.PRICE_STATUS_04, TableStatus.PRICE_STATUS_04_TEXT);
	}
	/**
	 * 公告发布状态
	 */
	public static Map<String,String> noticeMap = new HashMap<String,String>();
	static{
		noticeMap.put(TableStatus.NOTICE_TYPE_0, TableStatus.NOTICE_TYPE_0_TEXT);
		noticeMap.put(TableStatus.NOTICE_TYPE_1, TableStatus.NOTICE_TYPE_1_TEXT);
	}
	/**
	 * 采购异常类别Map
	 */
	public static Map<String,String> bidAbnomalMap = new HashMap<String,String>();
	static{
		bidAbnomalMap.put(TableStatus.BID_STATUS_2, TableStatus.BID_STATUS_2_TEXT);
		bidAbnomalMap.put(TableStatus.BID_STATUS_3, TableStatus.BID_STATUS_3_TEXT);
		bidAbnomalMap.put(TableStatus.BID_STATUS_4, TableStatus.BID_STATUS_4_TEXT);
		bidAbnomalMap.put(TableStatus.BID_STATUS_5, TableStatus.BID_STATUS_5_TEXT);
	}
	/**
	 * 字典信息Map
	 */
	public static Map<String,String> dictMap = new HashMap<String,String>();
	static{
		dictMap.put("计量单位", "计量单位");
		dictMap.put("岗位", "岗位");
	}
}
