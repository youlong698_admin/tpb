package com.ced.sip.common.interceptor;


 
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
 

import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.system.biz.ISysUserOperateLogBiz;
import com.ced.sip.system.entity.SysUserOperateLog;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.PreResultListener;
 
/**
  * 系统日志拦截器
  */
 public class LogInterceptor implements Interceptor {
	 
	 private ISysUserOperateLogBiz iSysUserOperateLogBiz;
	 
	 private SysUserOperateLog sysUserOperateLog; 
 
     private static final long serialVersionUID = 1L;
 
     public String intercept(ActionInvocation ai) throws Exception {
			
         ai.addPreResultListener(new PreResultListener() {
 
             public void beforeResult(ActionInvocation ai, String arg) {
                 try {

         			ActionContext actionContext = ai.getInvocationContext();
         			final HttpServletRequest req = (HttpServletRequest)actionContext.get(ServletActionContext.HTTP_REQUEST);  
                	//请求的IP    
                	 String ip = req.getHeader("X-Real-IP");    
                	 if(req.getSession().getAttribute("uiId")!=null){
                     long uiId=(Long)req.getSession().getAttribute("uiId");
                     //String mehtod=ai.getInvocationContext().getName();
                     //if(!mehtod.contains("view")&&!mehtod.contains("find")&&!mehtod.contains("init")&&!mehtod.contains("Init")&&!mehtod.contains("get")){
                     String operModule="";
                     if(req.getAttribute("operModule")!=null){ operModule=(String)req.getAttribute("operModule");
                     String message="";
                     if(req.getAttribute("message")==null) message=""; else message=(String)req.getAttribute("message");
                     Map<String, Object> map = req.getParameterMap();
                     Set<String> keys = map.keySet();
                     String param="";
                     for (String key : keys) {
                    	 param+=key + "=" + ((Object[]) map.get(key))[0]+ "#";
                     }
                     sysUserOperateLog= new SysUserOperateLog();
                     sysUserOperateLog.setAction(operModule);
                     sysUserOperateLog.setParam(param);
                     sysUserOperateLog.setUolId(uiId);
                     sysUserOperateLog.setResult(message);
                     sysUserOperateLog.setMethod(ai.getInvocationContext().getName());
                     sysUserOperateLog.setIpAddress(ip);
                     sysUserOperateLog.setWriter(UserRightInfoUtil.getSessionUserName(req));
                     sysUserOperateLog.setWriterEn(UserRightInfoUtil.getSessionUserLoginName(req));
                     sysUserOperateLog.setOperDate(new Date());
                     sysUserOperateLog.setType(UserRightInfoUtil.getSessionUserType(req));
                     iSysUserOperateLogBiz.saveSysUserOperateLog(sysUserOperateLog);
                     }
                     }
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
 
             }
         });
 
         return ai.invoke();
     }
 
 
     public void destroy() {
 
     }
 
     public void init() {
 
     }


	public ISysUserOperateLogBiz getiSysUserOperateLogBiz() {
		return iSysUserOperateLogBiz;
	}


	public void setiSysUserOperateLogBiz(
			ISysUserOperateLogBiz iSysUserOperateLogBiz) {
		this.iSysUserOperateLogBiz = iSysUserOperateLogBiz;
	}


     
 }

