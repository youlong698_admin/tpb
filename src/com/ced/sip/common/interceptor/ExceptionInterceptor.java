package com.ced.sip.common.interceptor;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
 

import com.ced.base.exception.BaseException;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.action.supplier.WebIndexAction;
import com.ced.sip.common.utils.ExceptionUtil;
import com.ced.sip.system.biz.ISysErrorLogBiz;
import com.ced.sip.system.entity.SysErrorLog;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
 import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
 
/**
  * 系统日志拦截器
  */
 public class ExceptionInterceptor implements Interceptor {
	 
     private ISysErrorLogBiz iSysErrorLogBiz;
	 
	 private SysErrorLog SysErrorLog; 
 
     private static final long serialVersionUID = 1L;
 
     public String intercept(ActionInvocation invocation) throws Exception {

		ActionContext actionContext = invocation.getInvocationContext();
		final HttpServletRequest req = (HttpServletRequest)actionContext.get(ServletActionContext.HTTP_REQUEST);  
		Object action = (Action) invocation.getAction();
		 
		 String result = null;
         try {
             result = invocation.invoke();
             return result;
             
         } catch (Exception e) {
        	 String errorPage="";
	    	 if(action instanceof WebIndexAction){
	    		 errorPage="errorWeb";
	    	 }else{
	    		 errorPage="exception";
                 if (e instanceof BaseException) {//请求的IP    
                	    String ip = req.getRemoteAddr();    
            	       	SysErrorLog=new SysErrorLog();
            	        SysErrorLog.setErrorUrl(req.getRequestURI());
            	       	SysErrorLog.setHashcode(e.getMessage());
            	       	String error = ExceptionUtil.getExceptionMessage(e);
            	       	SysErrorLog.setErrorInfo(error);
            	       	SysErrorLog.setIpAddress(ip);
            	       	SysErrorLog.setWriter(UserRightInfoUtil.getSessionUserName(req));
            	       	SysErrorLog.setWriterEn(UserRightInfoUtil.getSessionUserLoginName(req));
            	       	SysErrorLog.setErrorDate(new Date());
            	       	SysErrorLog.setType(UserRightInfoUtil.getSessionUserType(req));
                        this.iSysErrorLogBiz.saveSysErrorLog(SysErrorLog);		
                 }
	    	 }
	           return errorPage;
             }
         }
 
 
     public void destroy() {
 
     }
 
     public void init() {
 
     }


	public ISysErrorLogBiz getiTSysErrorLogBiz() {
		return iSysErrorLogBiz;
	}


	public void setiSysErrorLogBiz(ISysErrorLogBiz iSysErrorLogBiz) {
		this.iSysErrorLogBiz = iSysErrorLogBiz;
	}




     
 }

