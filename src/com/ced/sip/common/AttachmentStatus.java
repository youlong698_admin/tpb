package com.ced.sip.common;

public class AttachmentStatus {
	
	
	//	-------------------------------------- 附件类型列表  -----------------------------------------

	//	计划管理
	/** 附件类型 - 计划-相关附件'102' */
	public static final String ATTACHMENT_CODE_102 = "102" ;
	
    //	采购立项管理
	/** 附件类型 - 采购立项-相关附件'103' */
	public static final String ATTACHMENT_CODE_103 = "103" ;
	
	/** 附件类型 - 项目公告发布-公告附件 '201' */
	public static final String ATTACHMENT_CODE_201 = "201" ;
	

	/** 附件类型 - 标前澄清-公告附件 '202' */
	public static final String ATTACHMENT_CODE_202 = "202" ;
	

	/** 附件类型 - 中标通知书-中标通知书附件 '203' */
	public static final String ATTACHMENT_CODE_203 = "203" ;

	/** 附件类型 -  招标文件-招标附件 '204' */
	public static final String ATTACHMENT_CODE_204 = "204" ;

	/** 附件类型 -  投标文件-投标附件 '205' */
	public static final String ATTACHMENT_CODE_205 = "205" ;

	/** 附件类型 - 历史投标文件-历史投标文件 '206' */
	public static final String ATTACHMENT_CODE_206 = "206" ;
	
	
	/** 附件类型 - 供应商管理-供应商资质信息附件 '602' */
	public static final String ATTACHMENT_CODE_602 = "602" ;
	
	/** 附件类型 - 供应商管理-供应商资质信息变更附件 '602' */
	public static final String ATTACHMENT_CODE_6021 = "6021" ;
	
	/** 附件类型 - 供应商管理-供应商产品信息附件 '603' */
	public static final String ATTACHMENT_CODE_603 = "603" ;
	
	/** 附件类型 - 供应商管理-供应商交流信息附件 '603' */
	public static final String ATTACHMENT_CODE_604 = "604" ;
	
	/** 附件类型 - 供应商管理-供应商投标文件附件 '605' */
	public static final String ATTACHMENT_CODE_605 = "605" ;
	

	/** 附件类型 - 供应商管理-供应商状态变更文件附件 '606' */
	public static final String ATTACHMENT_CODE_606 = "606" ;
	//	系统管理附件类型列表
	
	
	/** 附件类型 - 系统公告发布-公告附件 '701' */
	public static final String ATTACHMENT_CODE_701 = "701" ;
	
	/** 附件类型 - 网站信息发布-公告附件 '801' */
	public static final String ATTACHMENT_CODE_801 = "801" ;
	

	/** 附件类型 - 网站信息发布-附件 '901' */
	public static final String ATTACHMENT_CODE_901 = "901" ;
	

	/** 附件类型 - 合同审批原件-附件 '1002' */
	public static final String ATTACHMENT_CODE_1002 = "1002" ;

	/** 附件类型 - 合同扫描件-附件 '1003' */
	public static final String ATTACHMENT_CODE_1003 = "1003" ;	

	/** 附件类型 - 合同业务终止-附件 '1001' */
	public static final String ATTACHMENT_CODE_1001 = "1001" ;
	
	

	/** 附件类型 - 订单业务终止-附件 '1101' */
	public static final String ATTACHMENT_CODE_1101 = "1101" ;

	/** 附件类型 - 发货单-附件 '1201' */
	public static final String ATTACHMENT_CODE_1201 = "1201" ;
	/** 附件类型 - 收货单-附件 '1301' */
	public static final String ATTACHMENT_CODE_1301 = "1301" ;
	

	/** 附件类型 - 资金计划-附件 '1401' */
	public static final String ATTACHMENT_CODE_1401 = "1401" ;
}
