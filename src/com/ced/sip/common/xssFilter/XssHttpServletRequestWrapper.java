package com.ced.sip.common.xssFilter;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {
	HttpServletRequest servletRequest;

	public XssHttpServletRequestWrapper(HttpServletRequest servletRequest) {
		super(servletRequest);
		this.servletRequest = servletRequest;
	}

	public String[] getParameterValues(String parameter) {
		String[] values = super.getParameterValues(parameter);
		
		if (values == null) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++) {
			encodedValues[i] = XssShieldUtil.stripXss(values[i]);
		}
		return encodedValues;
	}

	public String getParameter(String parameter) {
		String value = super.getParameter(parameter);//公钥不过滤 
        if(parameter.equals("publicKey")||parameter.equals("model")){
      	  return value;
         }
		if (value == null) {
			return null;
		}
		return XssShieldUtil.stripXss(value);
	}

	public String getHeader(String name) {
		String value = super.getHeader(name);
		if (value == null)
			return null;
		return XssShieldUtil.stripXss(value);
	}
}
