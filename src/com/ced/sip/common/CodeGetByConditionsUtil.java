package com.ced.sip.common;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ced.base.exception.BaseException;

public class CodeGetByConditionsUtil {

	
	/**
	 * 按要求生成编码
	 * @param gcKindMark 单据编码标识（必须唯一） 
	 * @param dateType 日期格式 
	 * @param serialNumForm 流水号形式（如：00或000或0000等）
	 * @throws BaseException 
	 */
	public static String generateCodeByConditions(String gcKindMark,String dateType,String serialNumForm,String sequence) throws BaseException{
		
		String strCode = "";
		String seqId= BaseDataInfosUtil.getSequenceNextValue(sequence);
		strCode = gcKindMark+CodeGetByConditionsUtil.getStringByDate(dateType)
							+CodeGetByConditionsUtil.getSerialFormatObject(serialNumForm).format(Long.parseLong(seqId));
		return strCode;
	}
	
	/**
	 * 按要求得到日期字符串
	 * @param dateType 日期格式
	 * @throws BaseException 
	 */
	public static String getStringByDate(String dateType){
		
		String strDate = "";
		if(dateType!=null){
			SimpleDateFormat sdf = new SimpleDateFormat(dateType);
			strDate = sdf.format(new Date());
		}
		return strDate;
	}
	
	/**
	 * 按要求得到流水号格式化对象
	 * @param serialNumForm 流水号形式
	 * @throws BaseException 
	 */
	public static DecimalFormat getSerialFormatObject(String serialNumForm){
		DecimalFormat df = new DecimalFormat(serialNumForm);
		return df;
	}
	
}
