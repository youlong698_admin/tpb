/**
 * 公用状态定义
 * @author ted
 */
package com.ced.sip.common;

public class TableStatus {
	
	// 用户session标识
	public static final String LOGIN_INFO_KEY = "LoginInfoKey";
	
	//	--------------------------- 公用状态 ---------------------------
	/** 公用状态 - 是否有效 - 有效 ‘0’ */
	public static final String COMMON_STATUS_VALID = "0" ;
	
	/** 公用状态 - 是否有效 - 有效   */
	public static final String COMMON_STATUS_VALID_TEXT = "有效" ;
	
	/** 公用状态 - 是否有效 - 无效 ‘1’ */
	public static final String COMMON_STATUS_INVALID = "1" ;
	
	/** 公用状态 - 是否有效 - 无效 */
	public static final String COMMON_STATUS_INVALID_TEXT = "无效" ;
	

	//	--------------------------- 公用状态 ---------------------------
	/** 公用状态 - 已读未读 - 已读 ‘0’ */
	public static final String READ_STATUS_0 = "0" ;
	
	/** 公用状态 - 已读未读 - 已读   */
	public static final String READ_STATUS_0_TEXT = "已读" ;
	
	/** 公用状态 - 已读未读 - 未读 ‘1’ */
	public static final String READ_STATUS_1 = "1" ;
	
	/** 公用状态 - 已读未读 - 未读 */
	public static final String READ_STATUS_1_TEXT = "未读" ;
	
//	--------------------------- 公用类型 ---------------------------
	/** 公用类型 - 内部用户 -  ‘0’ */
	public static final int COMMON_TYPE_0 = 0 ;	

	/** 公用类型 - 供应商 -  ‘1’ */
	public static final int COMMON_TYPE_1 = 1 ;
	
	/** 公用类型 - 专家 -  ‘2’ */
	public static final int COMMON_TYPE_2 = 2 ;
	//-------------------------------------- 公告发布状态  ----------------------------------------
	/** 状态 - 正式 ‘0’ */
	public static final String STATUS_0 = "0" ;
	/** 状态 - 正式 ‘0’ */
	public static final String STATUS_0_TEXT = "正式" ;
	
	/** 状态 - 草稿 ‘1’ */
	public static final String STATUS_1 = "1" ;
	/** 状态 - 草稿 ‘1’ */
	public static final String STATUS_1_TEXT = "草稿" ;
	
	
//	----------------------------------------基础数据类型 --------------------------------------------------------------
	/** 基础数据类型 - 用户信息 ‘usersCache’ */
	public static final String BASE_DATA_TYPE_01 = "usersCache" ;
	
	/** 基础数据类型 - 部门信息 ‘departsCache’ */
	public static final String BASE_DATA_TYPE_02 = "departsCache" ;
	
	/** 基础数据类型 - 字典表信息 ‘dictsCache’ */
	public static final String BASE_DATA_TYPE_03 = "dictsCache" ;	
	
	/** 基础数据类型 - 系统设置表信息 ‘systemConfigurationCache’ */
	public static final String BASE_DATA_TYPE_04 = "systemConfigurationCache" ;
	
	
	//-------------------------------------- 汇总分包标识 ----------------------------------------
	/** 汇总分包标识 - 已汇总分包 ‘0’ */
	public static final String COLLECT_TYPE_0 = "0" ;

	/** 汇总分包标识 - 未汇总分包 ‘1’ */
	public static final String COLLECT_TYPE_1 = "1" ;
	
	//-------------------------------------- 评分项目类别  ----------------------------------------
	/** 评分项目类别 - 技术评分项目 ‘0’ */
	public static final String BID_ITEM_TYPE_1 = "0" ;

	/** 评分项目类别 - 商务评分项目 ‘1’ */
	public static final String BID_ITEM_TYPE_2 = "1" ;
	
	//-------------------------------------- 公告发布状态  ----------------------------------------
	/** 公告发布状态 - 已发布 ‘0’ */
	public static final String NOTICE_TYPE_0 = "0" ;
	/** 公告发布状态 - 已发布 ‘0’ */
	public static final String NOTICE_TYPE_0_TEXT = "已发布" ;
	
	/** 公告发布状态 - 未发布 ‘1’ */
	public static final String NOTICE_TYPE_1 = "1" ;
	/** 公告发布状态 - 未发布 ‘1’ */
	public static final String NOTICE_TYPE_1_TEXT = "未发布" ;
	
	//-------------------------------------- 标前澄清发布状态  ----------------------------------------
	/** 标前澄清发布状态 - 已发布 ‘0’ */
	public static final String CLARIFY_TYPE_0 = "0" ;
	/** 标前澄清发布状态 - 已发布 ‘0’ */
	public static final String CLARIFY_TYPE_0_TEXT = "已发布" ;
	
	/** 标前澄清发布状态 - 未发布 ‘1’ */
	public static final String CLARIFY_TYPE_1 = "1" ;
	/** 标前澄清发布状态 - 未发布 ‘1’ */
	public static final String CLARIFY_TYPE_1_TEXT = "未发布" ;
	
	
	//-------------------------------------- 中标公示发布状态  ----------------------------------------
	/** 中标公示发布状态 - 已发布 ‘0’ */
	public static final String WINNING_TYPE_0 = "0" ;
	/** 中标公示发布状态 - 已发布 ‘0’ */
	public static final String WINNING_TYPE_0_TEXT = "已发布" ;
	
	/** 中标公示发布状态 - 未发布 ‘1’ */
	public static final String WINNING_TYPE_1 = "1" ;
	/** 中标公示发布状态 - 未发布 ‘1’ */
	public static final String WINNING_TYPE_1_TEXT = "未发布" ;
	//-------------------------------------- 计划类型  ----------------------------------------
	/** 计划类型 - 00 */
	public static final String RM_TYPE_00 = "00" ;
	/** 计划类型 - 普通计划 */
	public static final String RM_TYPE_00_TEXT = "普通计划" ;
	
	/** 计划类型 - 01 */
	public static final String RM_TYPE_01 = "01" ;
	/** 计划类型 - 加急计划 */
	public static final String RM_TYPE_01_TEXT = "加急计划" ;
	//-------------------------------------- 采购类型  ----------------------------------------
	
	/** 采购类型  - 00 */
	public static final String BUY_TYPE_0 = "0" ;
	/** 采购类型  - 自采 */
	public static final String BUY_TYPE_0_TEXT = "自采" ;
	
	/** 采购类型  - 01 */
	public static final String BUY_TYPE_1 = "1" ;
	/** 采购类型  - 集采*/
	public static final String BUY_TYPE_1_TEXT = "集采" ;
	
	
	//-------------------------------------- 采购方式  ----------------------------------------
	/** 采购方式 - 招标 ‘00’ */
	public static final String PURCHASE_WAY_00 = "00" ;
	/** 采购方式 - 招标 ‘00’ */
	public static final String PURCHASE_WAY_00_TEXT = "招标" ;
	
	/** 采购方式 - 询价 ‘01’ */
	public static final String PURCHASE_WAY_01 = "01" ;
	/** 采购方式 - 询价 ‘01’ */
	public static final String PURCHASE_WAY_01_TEXT = "询价" ;
	
	/** 采购方式 - 竞价 ‘02’ */
	public static final String PURCHASE_WAY_02 = "02" ;
	/** 采购方式 - 竞价 ‘02’ */
	public static final String PURCHASE_WAY_02_TEXT = "竞价" ;
	
	/** 采购方式 - 自主采购 ‘08’ */
	public static final String PURCHASE_WAY_08 = "08" ;
	/** 采购方式 - 自主采购 ‘08’ */
	public static final String PURCHASE_WAY_08_TEXT = "自主采购" ;
	
	//-------------------------------------- 供应商选择方式  ----------------------------------------
	/** 供应商选择方式 - 公开 ‘00’ */
	public static final String SUPPLIER_TYPE_00 = "00" ;
	/** 供应商选择方式 - 公开 ‘00’ */
	public static final String SUPPLIER_TYPE_00_TEXT = "公开" ;
	
	/** 供应商选择方式 - 邀请 ‘01’ */
	public static final String SUPPLIER_TYPE_01 = "01" ;
	/** 供应商选择方式 - 邀请 ‘01’ */
	public static final String SUPPLIER_TYPE_01_TEXT = "邀请" ;
	
	//-----------------------------------------------------专家评委类型----------------------------------------------------
	/*
	 * luguanglei 2016-09-15
	 */
	/** 专家评委类型--技术评委 "0" */
	public static String Expert_Judge_Type_Tech = "0";
	
	/** 专家评委类型--技术评委标签 */
	public static String Expert_Judge_Type_Tech_Text = "技术";
	
	/** 专家评委类型--商务评委 "1" */
	public static String Expert_Judge_Type_Business = "1";
	
	/** 专家评委类型--商务评委标签 */
	public static String Expert_Judge_Type_Business_Text = "商务";
	
	/** 专家评委类型--综合评委 "2" */
	public static String Expert_Judge_Type_Colligation = "2";
	
	/** 专家评委类型--综合评委标签 */
	public static String Expert_Judge_Type_Colligation_Text = "综合";

	/*
	 * luguanglei 2016-09-15
	 */
	/** 评分表类型--技术评分表 "0" */
	public static String Judge_Table_Type_Tech = "0";
	
	/** 评分表类型--技术评分表*/
	public static String Judge_Table_Type_Tech_Text = "技术评分表";
	
	/** 评分表类型--商务评分表 "1" */
	public static String Judge_Table_Type_Business = "1";
	
	/** 评分表类型--商务评分表 */
	public static String Judge_Table_Type_Business_Text = "商务评分表";
	
	/*
	 * 是否最终选择评委
	 */
	/** 是否参加：否 "1" */
	public static String Common_Jion_N = "1";
	/** 是否参加：否标签 */
	public static String Common_Jion_N_Text = "否";
	
	/** 是否参加：是"0" */
	public static String Common_Jion_Y = "0";
	
	/** 是否参加：是的标签 */
	public static String Common_Jion_Y_Text = "是";
	
	//-------------------------------------- 价格评分规则类别  ----------------------------------------
	/** 价格评分规则类别 ： 01 "最低价满分" */
	public static String Bid_Price_Type_01 = "01";
	/** 价格评分规则类别 ： 01 "最低价满分" */
	public static String Bid_Price_Type_01_Text = "最低价满分";
	/** 价格评分规则类别 ： 02 "平均价满分" */
	public static String Bid_Price_Type_02 = "02";
	/** 价格评分规则类别 ： 02 "平均价满分" */
	public static String Bid_Price_Type_02_Text = "平均价满分";
	/** 价格评分规则类别 ： 03 "最高价满分" */
	public static String Bid_Price_Type_03 = "03";
	/** 价格评分规则类别 ： 03 "最高价满分" */
	public static String Bid_Price_Type_03_Text = "最高价满分";
	/** 价格评分规则类别 ： 00 "其他价格规则" */
	public static String Bid_Price_Type_00 = "00";
	/** 价格评分规则类别 ： 00 "其他价格规则" */
	public static String Bid_Price_Type_00_Text = "其他";
	
	//-------------------------------------- 开标状态  ----------------------------------------
	/** 开标状态  ： 01 "未开标" */
	public static String OPEN_STATUS_01 = "01";
	/** 开标状态  ： 01 "未开标" */
	public static String OPEN_STATUS_01_TEXT = "未开标";
	/** 开标状态  ： 02 "已开标" */
	public static String OPEN_STATUS_02 = "02";
	/** 开标状态  ： 02 "已开标" */
	public static String OPEN_STATUS_02_TEXT = "已开标";
//-------------------------------------- 报价汇总表状态  ----------------------------------------
	/** 报价汇总表状态： 01 "等待解密" */
	public static String PRICE_STATUS_01 = "01";
	/** 报价汇总表状态： 01 "等待解密" */
	public static String PRICE_STATUS_01_TEXT = "等待解密";
	/** 报价汇总表状态： 02 "已解密" */
	public static String PRICE_STATUS_02 = "02";
	/** 报价汇总表状态： 02 "已解密" */
	public static String PRICE_STATUS_02_TEXT = "已解密";
	/** 报价汇总表状态： 03 "已提交" */
	public static String PRICE_STATUS_03 = "03";
	/** 报价汇总表状态： 03 "已提交" */
	public static String PRICE_STATUS_03_TEXT = "已提交";
	/** 报价汇总表状态： 04 "已评分" */
	public static String PRICE_STATUS_04 = "04";
	/** 报价汇总表状态： 04 "已评分" */
	public static String PRICE_STATUS_04_TEXT = "已评分";
	
	//-------------------------------------- 评标状态  ----------------------------------------
	/** 评标状态  ： 01 "未评标" */
	public static String CALIBTATION_STATUS_01 = "01";
	/** 评标状态  ： 01 "未评标" */
	public static String CALIBTATION_STATUS_01_TEXT = "未评标";
	/** 评标状态 ： 02 "已评标" */
	public static String CALIBTATION_STATUS_02 = "02";
	/** 评标状态  ： 02 "已评标" */
	public static String CALIBTATION_STATUS_02_TEXT = "已评标";
		

	//	--------------------------- 采购业务状态状态 ---------------------------
	/** 采购业务状态状态 -  正常 ‘1’ */
	public static final String BID_STATUS_1 = "1" ;
	
	/** 采购业务状态状态 -  正常    */
	public static final String BID_STATUS_1_TEXT = "正常" ;	

	/** 采购业务状态状态 -  废标 ‘2’ */
	public static final String BID_STATUS_2 = "2" ;
	
	/** 采购业务状态状态 -  废标    */
	public static final String BID_STATUS_2_TEXT = "废标" ;
	
	/** 采购业务状态状态 -  流标 ‘3’ */
	public static final String BID_STATUS_3 = "3" ;
	
	/** 采购业务状态状态 -  流标    */
	public static final String BID_STATUS_3_TEXT = "流标" ;
	
	/** 采购业务状态状态 -  串标 ‘4’ */
	public static final String BID_STATUS_4 = "4" ;
	
	/** 采购业务状态状态 -  串标    */
	public static final String BID_STATUS_4_TEXT = "串标" ;
	
	/** 采购业务状态状态 -  终止 ‘5’ */
	public static final String BID_STATUS_5 = "5" ;
	
	/** 采购业务状态状态 -  终止    */
	public static final String BID_STATUS_5_TEXT = "终止" ;

	
	/** 采购业务状态状态 -  结束 ‘6’ */
	public static final String BID_STATUS_6 = "6" ;
	
	/** 采购业务状态状态 -  结束    */
	public static final String BID_STATUS_6_TEXT = "结束" ;
	
	/** 采购业务状态状态 -  结束 ‘7’ */
	public static final String BID_STATUS_7 = "7" ;
	
	/** 采购业务状态状态 -  结束    */
	public static final String BID_STATUS_7_TEXT = "变更" ;
	
	
	/** 群组设置的高层领导查询**/
	public static String Leader = "Leader";
	
//	--------------------------- 是否 ---------------------------
	/** 公用状态 - 是否- 是‘0’ */
	public static final String COMMON_IS = "0" ;
	
	/** 公用状态 - 是否 - 否 ‘1’ */
	public static final String COMMON_IS_NOT = "1" ;
	
	/** 公用状态 -  - 0 */
	public static final String COMMON_0 = "0";
	/** 公用状态 -  - 1 */
	public static final String COMMON_1 = "1";
	/** 公用状态 -  - 2 */
	public static final String COMMON_2 = "2";
	
	/** 公用状态 -  - 1 */
	public static final int COMMON_INT_1 =1;
	/** 公用状态 -  - 0 */
	public static final int COMMON_INT_0 =0;
}
