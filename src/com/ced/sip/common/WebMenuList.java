package com.ced.sip.common;

import java.util.ArrayList;
import java.util.List;
/**
 * 获取菜单List type 1 列表   2 单页
 * @author luguanglei
 *
 */
public class WebMenuList {
	  private WebMenu webMenu;
	  private List<WebMenu> list;
	  public List<WebMenu> getWebMenuList(){
		  list=new ArrayList<WebMenu>();		  
		  
		  webMenu=new WebMenu();
		  webMenu.setWmId(new Long(12));
		  webMenu.setMenuName("帮助中心");
		  list.add(webMenu);
		  
		  webMenu=new WebMenu();
		  webMenu.setWmId(new Long(1));
		  webMenu.setMenuName("关于我们");
		  list.add(webMenu);
		  
		  webMenu=new WebMenu();
		  webMenu.setWmId(new Long(32));
		  webMenu.setMenuName("采购政策");
		  list.add(webMenu);
		  
		  webMenu=new WebMenu();
		  webMenu.setWmId(new Long(33));
		  webMenu.setMenuName("重要通知");
		  list.add(webMenu);
		  
		  return list;
	  }
	  public WebMenu getWebMenu(Long id){
		  int idTemp=id.intValue();
		  webMenu=new WebMenu();
		  if(idTemp==12){  
			  webMenu=new WebMenu();
			  webMenu.setWmId(new Long(31));
			  webMenu.setMenuName("帮助中心");
		  }else if(idTemp==1){  
			  webMenu=new WebMenu();
			  webMenu.setWmId(new Long(1));
			  webMenu.setMenuName("关于我们");
		  }else if(idTemp==32){
			  webMenu=new WebMenu();
			  webMenu.setWmId(new Long(32));
			  webMenu.setMenuName("采购政策");
		  }else if(idTemp==33){  
			  webMenu=new WebMenu();
			  webMenu.setWmId(new Long(33));
			  webMenu.setMenuName("重要通知");
		  }
		  return webMenu;
	  }
}
