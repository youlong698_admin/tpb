package com.ced.sip.common.model;

import java.util.List;

import com.ced.sip.system.entity.SysMenu;

public class AuthorityParameter{

	private static final long serialVersionUID = 2903229213249813463L;
	private String $eq_menuCode;
	private String $like_menuName;
	private List<SysMenu> subAuthorityList;

	public String get$eq_menuCode() {
		return $eq_menuCode;
	}

	public void set$eq_menuCode(String $eq_menuCode) {
		this.$eq_menuCode = $eq_menuCode;
	}

	public String get$like_menuName() {
		return $like_menuName;
	}

	public void set$like_menuName(String $like_menuName) {
		this.$like_menuName = $like_menuName;
	}

	public List<SysMenu> getSubAuthorityList() {
		return subAuthorityList;
	}

	public void setSubAuthorityList(List<SysMenu> subAuthorityList) {
		this.subAuthorityList = subAuthorityList;
	}

}
