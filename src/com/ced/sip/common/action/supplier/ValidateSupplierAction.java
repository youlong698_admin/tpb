package com.ced.sip.common.action.supplier;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.biz.ISysUserInoutLogBiz;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.entity.SessionInfo;
import com.ced.sip.common.entity.SessionUser;
import com.ced.sip.common.utils.PasswordUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.system.entity.SysUserInoutLog;

public class ValidateSupplierAction extends BaseAction {
	private ISysUserInoutLogBiz iSysUserInoutLogBiz ;
	private ISupplierInfoBiz iSupplierInfoBiz;
	
	
	Map<String, Object> map = new HashMap<String, Object>();
	private SessionUser sessionUser;	
	private SupplierInfo supplierInfo ;
 	private String errorMsg ;
 	private String validateCode;  //验证码
	
	/**
	 * 供应商登陆校验
	 * @return
	 * @throws ServletException
	 */
	public String verifySupplierLogin() throws BaseException {
		String code="";
		String rUrlPage 		= "invalidlogin" ;
		boolean falg=false;
		List supplierList =null;		
		try {   
				String validateCode =this.getRequest().getParameter("validateCode");
				code=(String) this.getRequest().getSession().getAttribute("randCode");
				if(StringUtil.isBlank(code)) falg=true; else falg=StringUtil.convertNullToBlank(validateCode).equals(code); //验证码错误
				if(!falg){
					this.getRequest().setAttribute("errorMassge", "验证码有误，请重新输入");
					rUrlPage = "exit" ;
				}else{
					supplierInfo.setSupplierLoginPwd(PasswordUtil.encrypt(supplierInfo.getSupplierLoginPwd())) ;
					supplierList= iSupplierInfoBiz.getSupplierInfoListByValidate(supplierInfo);
				}
		        if(falg&&supplierList != null && supplierList.size()>0 ) { // 通过用户校验, 保存供应商信息到session并返回主页面
						supplierInfo= (SupplierInfo) supplierList.get( 0 ) ;
						
						if( TableStatus.COMMON_STATUS_VALID.equals( supplierInfo.getIsUsable() ) ){//判断供应商是否有效	
							
							SessionInfo info = new SessionInfo() ;
							info.setSupplierId(supplierInfo.getSupplierId());
							info.setSupplierInfo(supplierInfo);
							info.setSupplierLoginName(supplierInfo.getSupplierLoginName());
							info.setSupplierName(supplierInfo.getSupplierName());
							
							sessionUser=new SessionUser();
							sessionUser.setType(TableStatus.COMMON_TYPE_1);
							sessionUser.setLoginName(supplierInfo.getSupplierLoginName());
							sessionUser.setId(supplierInfo.getSupplierId());
							sessionUser.setName(supplierInfo.getSupplierName());
							info.setSessionUser(sessionUser);
							
							UserRightInfoUtil.setSessionInfo(info, this.getRequest() ) ;
							
							
							this.getRequest().setAttribute("isRegister", supplierInfo.getIsRegister());
							
							rUrlPage = "main";
							String ip = this.getRequest().getHeader("X-Real-IP");  
							SysUserInoutLog tSysUserInoutLog=new SysUserInoutLog( supplierInfo.getSupplierLoginName(), supplierInfo.getSupplierName(),
									ip, new Date(), this.getSession().getId().toString(), TableStatus.COMMON_STATUS_VALID,TableStatus.COMMON_TYPE_1);
							// 保存登录日志信息
							iSysUserInoutLogBiz.saveSysUserInoutLog(tSysUserInoutLog);
							this.getRequest().getSession().setAttribute("uiId", tSysUserInoutLog.getUilId());
							
						}else {
							this.getRequest().setAttribute("errorMassge", "对不起，账号失效");
							rUrlPage = "invalidlogin" ;
						}	
					}else{ // 校验失败, 返回登陆页面, 提示错误信息
						errorMsg = "incorrectPassword"; // 姓名或密码输入错误！
						this.getRequest().setAttribute("errorMassge", "用户名或密码输入错误，请重新输入");
						rUrlPage = "invalidlogin" ;
					}
		} catch (Exception e) {
			log.error("供应商用户登陆校验错误！", e);
			throw new BaseException("供应商用户登陆校验错误！", e);
		}
		return rUrlPage  ;
	}	
	/**
	 * 用户退出
	 * @return
	 * @throws ServletException
	 */
	public String exit( ) throws BaseException {
		try {
			HttpSession session = this.getSession() ;
			
			iSysUserInoutLogBiz.userLogoutLog(session.getId().toString());
			
			for (Enumeration e = session.getAttributeNames() ; e.hasMoreElements() ;) {
				session.removeAttribute((String)e.nextElement());
		    }
		} catch (Exception e) {
			log.error("用户退出错误！", e);
			throw new BaseException("用户退出错误！", e);

		}
		return "exit"  ;
	}
	/**
	 * 用户登录超时 
	 */
	public String timeout() throws BaseException{
		try{
			
			
		} catch (Exception e) {
			log.error("用户登录超时 ！", e);
			throw new BaseException("用户登录超时 ！", e);
		}
		return "timeout" ;
	}
	
	public ISysUserInoutLogBiz getiSysUserInoutLogBiz() {
		return iSysUserInoutLogBiz;
	}

	public void setiSysUserInoutLogBiz(ISysUserInoutLogBiz iSysUserInoutLogBiz) {
		this.iSysUserInoutLogBiz = iSysUserInoutLogBiz;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}

	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}
	
	
}
