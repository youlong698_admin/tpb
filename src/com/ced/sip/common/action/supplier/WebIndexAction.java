package com.ced.sip.common.action.supplier;

import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.common.AttachmentStatus;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WebMenu;
import com.ced.sip.common.WebMenuList;
import com.ced.sip.common.biz.IAttachmentBiz;
import com.ced.sip.common.biz.IBidBulletinWinningViewBiz;
import com.ced.sip.common.biz.ICompanySupplierViewBiz;
import com.ced.sip.common.entity.Attachment;
import com.ced.sip.common.entity.BidBulletinWinningView;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.HtmlUtil;
import com.ced.sip.common.utils.PasswordUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.purchase.base.biz.IBidBulletinBiz;
import com.ced.sip.purchase.base.biz.IBidResultNoticeBiz;
import com.ced.sip.purchase.base.biz.IBidWinningBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidBulletin;
import com.ced.sip.purchase.base.entity.BidWinning;
import com.ced.sip.purchase.base.entity.RequiredCollect;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.biz.ISupplierProductInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductGroup;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.biz.ISysCompanyBiz;
import com.ced.sip.system.biz.IWebInfoBiz;
import com.ced.sip.system.entity.MaterialKind;
import com.ced.sip.system.entity.SysCompany;
import com.ced.sip.system.entity.SystemConfiguration;
import com.ced.sip.system.entity.WebInfo;

public class WebIndexAction extends BaseAction{ 	
 	// 招标公告表服务类
	private IBidBulletinBiz iBidBulletinBiz  ; 	
 	// 中标公示表服务类
	private IBidWinningBiz iBidWinningBiz ;
	//供应商服务类
	private ISupplierInfoBiz iSupplierInfoBiz;
	//主营产品类别服务类
	private IMaterialBiz iMaterialBiz;
	//网站信息
	private IWebInfoBiz iWebInfoBiz;
	//采购单位服务类
	private ISysCompanyBiz iSysCompanyBiz;
	private IAttachmentBiz iAttachmentBiz;
	private IRequiredCollectBiz iRequiredCollectBiz;
	private ISupplierProductInfoBiz iSupplierProductInfoBiz;
	private ICompanySupplierViewBiz iCompanySupplierViewBiz;
	private IBidBulletinWinningViewBiz iBidBulletinWinningViewBiz;
	private IBidResultNoticeBiz iBidResultNoticeBiz;
	
	// 供应商基本信息 实例
	private SupplierInfo supplierInfo ;
	//主营产品类别
	private MaterialKind materialKind;
	//采购单位信息
	private SysCompany sysCompany;
	//系统配置信息
	private SystemConfiguration systemConfiguration;
 	private Long id;
	private WebMenuList webMenuList=new WebMenuList();
	private WebMenu webMenu;
	
	private WebInfo webInfo;
	private List<WebInfo> webInfos;
	private List<MaterialKind> materialKinds;
	
	
	private RollPage rollPageIndex=this.getRollPageDataTables();
	
	/**
	 * 采购门户网站主页
	 * @return
	 * @throws BaseException
	 */
	public String index() throws BaseException{
		systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
		this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
		
		rollPageIndex.setPagePer(10);
		BidBulletin bidBulletin=new BidBulletin();
		//查询公告信息
		bidBulletin.setBuyWay(TableStatus.PURCHASE_WAY_00);
		List<BidBulletin> bbList00=this.iBidBulletinBiz.getBidBulletinList(rollPageIndex,bidBulletin);
		
		bidBulletin.setBuyWay(TableStatus.PURCHASE_WAY_01);
		List<BidBulletin> bbList01=this.iBidBulletinBiz.getBidBulletinList(rollPageIndex,bidBulletin);
		
		bidBulletin.setBuyWay(TableStatus.PURCHASE_WAY_02);
		List<BidBulletin> bbList02=this.iBidBulletinBiz.getBidBulletinList(rollPageIndex,bidBulletin);
		
		BidWinning bidWinning=new BidWinning();
		//查询公示信息
		bidWinning.setBuyWay(TableStatus.PURCHASE_WAY_00);
		List<BidWinning> bwList00=this.iBidWinningBiz.getBidWinningList(rollPageIndex,bidWinning);		

		bidWinning.setBuyWay(TableStatus.PURCHASE_WAY_01);
		List<BidWinning> bwList01=this.iBidWinningBiz.getBidWinningList(rollPageIndex,bidWinning);		

		bidWinning.setBuyWay(TableStatus.PURCHASE_WAY_02);
		List<BidWinning> bwList02=this.iBidWinningBiz.getBidWinningList(rollPageIndex,bidWinning);
		
		
        //帮助信息
		webInfo=new WebInfo();
		webInfo.setWmId(Long.parseLong("12"));
		rollPageIndex.setPagePer(15);
		webInfos=this.iWebInfoBiz.getWebInfoListWeb(rollPageIndex,webInfo);
		this.getRequest().setAttribute("webInfos", webInfos);
		
		
		//重要通知公告
		webInfo=new WebInfo();
		webInfo.setIsIndex(2);
		rollPageIndex.setPagePer(5);
		List webInfoNoticeIndex=this.iWebInfoBiz.getWebInfoListWebNotice(rollPageIndex,webInfo);
		this.getRequest().setAttribute("webInfoNoticeIndex", webInfoNoticeIndex);		
		
		//按采购类别查询项目
		materialKind=new MaterialKind();
		materialKind.setParentId(0L);
		materialKinds=this.iMaterialBiz.getMaterialKindList(materialKind);
		this.getRequest().setAttribute("materialKinds", materialKinds);
		
		
		this.getRequest().setAttribute("bbList00", bbList00);
		this.getRequest().setAttribute("bbList01", bbList01);
		this.getRequest().setAttribute("bbList02", bbList02);
		this.getRequest().setAttribute("bwList00", bwList00);
		this.getRequest().setAttribute("bwList01", bwList01);
		this.getRequest().setAttribute("bwList02", bwList02);
		
		
		return "index";
	}

	
	/**
	 * 注册供应商页面初始化
	 * @return
	 * @throws BaseException 
	 */
	public String supplierRegInit() throws BaseException {
		try{
			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			Map supplierTypeMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1710);
			Map registrationTypeMap = BaseDataInfosUtil.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1714);
			this.getRequest().setAttribute("supplierTypeMap", supplierTypeMap);
			this.getRequest().setAttribute("registrationTypeMap", registrationTypeMap);
		} catch (Exception e) {
			log("注册供应商页面初始化错误！", e);
			throw new BaseException("注册供应商页面初始化错误！", e);
		}
		return "supplierRegInit";
		
	}
	
	/**
	 * 保存供应商基本信息
	 * @return
	 * @throws BaseException 
	 */
	public String supplierReg() throws BaseException {
		
		try{
			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			if(supplierInfo!=null){
				supplierInfo.setIsUsable("0"); //无效
				supplierInfo.setIsRegister(TableStatus.COMMON_2);
				supplierInfo.setSupplierName(supplierInfo.getSupplierName().trim());
				supplierInfo.setSupplierLoginPwd(PasswordUtil.encrypt(supplierInfo.getSupplierLoginPwd()));
				supplierInfo.setRegistDate(DateUtil.getCurrentDateTime());
				this.iSupplierInfoBiz.saveSupplierInfo( supplierInfo );
				if(StringUtil.isNotBlank(supplierInfo.getProdCodes())){
					SupplierProductGroup supplierProductGroup=null;
					String[] prodCodes = supplierInfo.getProdCodes().split(",");
					//保存供应商对应的产品类别信息
					for(int i=1;i<prodCodes.length-1;i++){
						if(prodCodes[i]!=null){
							supplierProductGroup = new SupplierProductGroup();
							supplierProductGroup.setSupplierId(supplierInfo.getSupplierId());
							materialKind = this.iMaterialBiz.getMaterialKind(Long.parseLong(prodCodes[i]));
							supplierProductGroup.setPdId(materialKind.getMkId());
							supplierProductGroup.setPdCode(materialKind.getMkCode());
							supplierProductGroup.setPdName(materialKind.getMkName());
							this.iSupplierInfoBiz.saveSupplierProductGroup(supplierProductGroup);
						}
					}
				}
			}
			
			this.getRequest().setAttribute("message", "注册成功");
		} catch (Exception e) {
			log("保存供应商基本信息错误！", e);
			throw new BaseException("保存供应商基本信息错误！", e);
		}
		
		return "supplierRegSuccess";
		
	}
	/**
	 * 招标公告信息页面
	 * @return
	 * @throws BaseException
	 */
	public String bidderNotice() throws BaseException{
		
		try{
			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			List<BidBulletin> bbList=this.iBidBulletinBiz.getBidBulletinList(this.getRollPageDataTables(),null,1);
			this.getRequest().setAttribute("bbList", bbList);			
			
		} catch (Exception e) {
			log("查询 招标公告错误！", e);
			throw new BaseException("查询 招标公告错误！", e);
		}
		return "bidderNotice";
	}
	/**
	 * 采购公告信息页面
	 * @return
	 * @throws BaseException
	 */
	public String purchaseNotice() throws BaseException{
		
		try{
			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
            List<BidBulletin> bbList=this.iBidBulletinBiz.getBidBulletinList(this.getRollPageDataTables(),null,2);
			this.getRequest().setAttribute("bbList", bbList);
			
			
		} catch (Exception e) {
			log("查询采购公告错误！", e);
			throw new BaseException("查询采购公告错误！", e);
		}
		return "purchaseNotice";
	}
	/**
	 * 中标公告信息页面
	 * @return
	 * @throws BaseException
	 */
	public String biddingNotice() throws BaseException{
		
		try{
			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			List<BidWinning> bwList=this.iBidWinningBiz.getBidWinningList(this.getRollPageDataTables(),null);
			this.getRequest().setAttribute("bwList", bwList);			
		} catch (Exception e) {
			log("查询中标公告错误！", e);
			throw new BaseException("查询中标公告错误！", e);
		}
		return "biddingNotice";
	}
	
	/**
	 * 公告信息 列表
	 * @return
	 * @throws BaseException
	 */
	public String bidBulletinContent() throws BaseException{
		
		try{	

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			BidBulletin bidBulletin=this.iBidBulletinBiz.getBidBulletin(id);
			this.getRequest().setAttribute("bidBulletin", bidBulletin);
						
		} catch (Exception e) {
			log("查询单个公告信息错误！", e);
			throw new BaseException("查询单个公告信息错误！", e);
		}
		return "bidBulletinContent";
	}
	
	/**
	 * 公示信息 列表
	 * @return
	 * @throws BaseException
	 */
	public String bidWinningContent() throws BaseException{
		
		try{

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			BidWinning bidWinning=this.iBidWinningBiz.getBidWinning(id);
			this.getRequest().setAttribute("bidWinning", bidWinning);
			
		} catch (Exception e) {
			log("查询单个公示信息错误！", e);
			throw new BaseException("查询单个公示信息错误！", e);
		}
		return "bidWinningContent";
	}
	/**
	 * 网站信息 列表  重要公告信息  采购政策
	 * @return
	 * @throws BaseException
	 */
	public String contentList() throws BaseException{
		String typePage="";
		try{

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			String content=this.getRequest().getParameter("content");
			
		    webMenu=webMenuList.getWebMenu(id);
			
		    if(StringUtil.isNotBlank(content)){
			    webInfo=new WebInfo();
				webInfo.setWmId(id);
				webInfos=this.iWebInfoBiz.getWebInfoListWeb(this.getRollPageDataTables(),webInfo);
				this.getRequest().setAttribute("webInfoCjwt", webInfos);
		    }
		    	
		    webInfo=new WebInfo();
			webInfo.setWmId(id);
			webInfo.setContent(content);
			webInfos=this.iWebInfoBiz.getWebInfoListWeb(this.getRollPageDataTables(),webInfo);
			for(WebInfo webInfo:webInfos){
		        	webInfo.setContentShort(HtmlUtil.getTextFromHtml(webInfo.getContent()));
			}
			this.getRequest().setAttribute("webInfos", webInfos);
			if(StringUtil.isBlank(content)){
				this.getRequest().setAttribute("webInfoCjwt", webInfos);
			}
			this.getRequest().setAttribute("webMenu", webMenu);
			
			if(id.equals(new Long(12))) typePage="help";
			else if(id.equals(new Long(32))) typePage="purchaseInfo";
			else if(id.equals(new Long(33))) typePage="purchaseInfo";
			
		} catch (Exception e) {
			log("查询网站信息错误！", e);
			throw new BaseException("查询网站信息错误！", e);
		}
		return typePage;
	}
	/**
	 * 单个网站信息
	 * @return
	 * @throws BaseException
	 */
	public String content() throws BaseException{
		
		try{	

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			webInfo=this.iWebInfoBiz.getWebInfo(id);
			webInfo.setReadCount(webInfo.getReadCount()+1);
			this.iWebInfoBiz.updateWebInfo(webInfo);
			//获取附件
			webInfo.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( webInfo.getWiId(), AttachmentStatus.ATTACHMENT_CODE_801 ) ) , "0", this.getRequest() ) ) ;

						
		    webMenu=webMenuList.getWebMenu(webInfo.getWmId());
		    
			this.getRequest().setAttribute("webInfo", webInfo);
			
			this.getRequest().setAttribute("webMenu", webMenu);
			
		} catch (Exception e) {
			log("查询单个网站信息错误！", e);
			throw new BaseException("查询单个网站信息错误！", e);
		}
		return "content";
	}
	/**
	 * 关于我们
	 * @return
	 * @throws BaseException
	 */
	public String about() throws BaseException{
		
		try{	

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			webInfo=new WebInfo();
			webInfo.setWmId(id);
			webInfo=this.iWebInfoBiz.getWebInfoByWebInfo(webInfo);
			webInfo.setReadCount(webInfo.getReadCount()+1);
			this.iWebInfoBiz.updateWebInfo(webInfo);
			//获取附件
			webInfo.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( webInfo.getWiId(), AttachmentStatus.ATTACHMENT_CODE_801 ) ) , "0", this.getRequest() ) ) ;

						
		    webMenu=webMenuList.getWebMenu(webInfo.getWmId());
		    
			this.getRequest().setAttribute("webInfo", webInfo);
			
			this.getRequest().setAttribute("webMenu", webMenu);
			
		} catch (Exception e) {
			log("查询关于我们错误！", e);
			throw new BaseException("查询关于我们错误！", e);
		}
		return "about";
	}
	/**
	 * 帮助详细信息
	 * @return
	 * @throws BaseException
	 */
	public String helpContent() throws BaseException{
		
		try{

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
			webInfo=this.iWebInfoBiz.getWebInfo(id);

			webInfo.setReadCount(webInfo.getReadCount()+1);
			this.iWebInfoBiz.updateWebInfo(webInfo);
			
			//获取附件
			webInfo.setAttachmentUrl( iAttachmentBiz.getAttachmentPageUrl( iAttachmentBiz.getAttachmentList( new Attachment( webInfo.getWiId(), AttachmentStatus.ATTACHMENT_CODE_801 ) ) , "0", this.getRequest() ) ) ;
	    
			this.getRequest().setAttribute("webInfo", webInfo);
			
			Long wmId=webInfo.getWmId();
			webInfo=new WebInfo();
			webInfo.setWmId(wmId);
			webInfos=this.iWebInfoBiz.getWebInfoListWeb(this.getRollPageDataTables(),webInfo);
			this.getRequest().setAttribute("webInfoCjwt", webInfos);
			
			
		} catch (Exception e) {
			log("查询帮助详细信息信息错误！", e);
			throw new BaseException("查询帮助详细信息信息错误！", e);
		}
		return "helpContent";
	}	
	/**
	 * 搜索 列表
	 * @return
	 * @throws BaseException
	 */
	public String search() throws BaseException{
		String page="";
		try{

			systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
			this.getRequest().setAttribute("systemConfiguration", systemConfiguration);
			
		    //最新中标信息
			rollPageIndex.setPagePer(30);
			rollPageIndex.setOrderColumn("de.brnId");
			rollPageIndex.setOrderDir("desc");
			List allList=this.iBidResultNoticeBiz.getSupplierBehavior(rollPageIndex,null,null);
			this.getRequest().setAttribute("allList", allList);
			
			page="xmSearch";

			BidBulletinWinningView bidBulletinWinningView= new BidBulletinWinningView();
			Map<String,Object> map=new HashMap<String, Object>();
			String bidInfo=this.getRequest().getParameter("bidInfo");
			bidBulletinWinningView.setTitle(bidInfo);
			String startDate=this.getRequest().getParameter("startDate");
			String endDate=this.getRequest().getParameter("endDate");
			
			//招标类型  公告类型 发布时间类型
			int bidderType=0,noticeType=0,publishType=0;
			//定义时间
			String startDateTmp="",endDateTmp="";
			String bidderTypeTmp=this.getRequest().getParameter("bidderType");
			if(StringUtil.isNotBlank(bidderTypeTmp)) bidderType=Integer.parseInt(bidderTypeTmp);
			map.put("bidderType", bidderType);
			
			String noticeTypeTmp=this.getRequest().getParameter("noticeType");
			if(StringUtil.isNotBlank(noticeTypeTmp)) noticeType=Integer.parseInt(noticeTypeTmp);
			map.put("type", noticeType);
			
			String publishTypeTmp=this.getRequest().getParameter("publishType");
			if(StringUtil.isNotBlank(publishTypeTmp)) publishType=Integer.parseInt(publishTypeTmp);
			
			if(publishType==1){
				startDateTmp=DateUtil.dayAgo(4);
				endDateTmp=DateUtil.getDefaultDateFormat(new Date());
			}else if(publishType==2){
				startDateTmp=DateUtil.dayAgo(8);
				endDateTmp=DateUtil.getDefaultDateFormat(new Date());
			}else if(publishType==3){
				startDateTmp=DateUtil.dayAgo(31);
				endDateTmp=DateUtil.getDefaultDateFormat(new Date());
			}else if(publishType==4){
				startDateTmp=startDate;
				endDateTmp= endDate;
			}
			map.put("startDate", startDateTmp);
			map.put("endDate", endDateTmp);
			
			
            List<BidBulletinWinningView> bbwList=this.iBidBulletinWinningViewBiz.getBidBulletinWinningViewList(this.getRollPageDataTables(),bidBulletinWinningView,map);
            
			this.getRequest().setAttribute("bbwList", bbwList);
			this.getRequest().setAttribute("bidInfo", bidInfo);
			this.getRequest().setAttribute("startDate", startDate);
			this.getRequest().setAttribute("endDate", endDate);
			this.getRequest().setAttribute("bidderType", bidderType);
			this.getRequest().setAttribute("noticeType", noticeType);
			this.getRequest().setAttribute("publishType", publishType);

				
			
		} catch (Exception e) {
			log("搜索信息错误！", e);
			throw new BaseException("搜索信息错误！", e);
		}
		return page;
	}
	/**
	 * 注册协议
	 * @return
	 * @throws BaseException
	 */
	public String regAgency() throws BaseException{
		String page="";
		try{
			String sign=this.getRequest().getParameter("sign");
			if(StringUtil.isBlank(sign)) sign="1";//注册协议
			if(sign.equals("1")){
				page="zcxy";
			}
		} catch (Exception e) {
			log("注册协议错误！", e);
			throw new BaseException("注册协议错误！", e);
		}
		return page;
	}
	/**
	 * 跳转至报价页面
	 * @return
	 * @throws BaseException
	 */
	public String bidPrice() throws BaseException{
		String page="";
		try{
			String supplierLoginName=UserRightInfoUtil.getSupplierLoginName(this.getRequest());
			SupplierInfo supplierInfo=UserRightInfoUtil.getSupplierInfo(this.getRequest());
			if(StringUtil.isBlank(supplierLoginName)){
				page="login";
			}else{
				if(supplierInfo.getIsRegister().equals("0")){
				//这里查询一次是防止非法访问
				Long rcId=Long.parseLong(this.getRequest().getParameter("rcId"));
				RequiredCollect requiredCollect=new RequiredCollect();
				requiredCollect.setRcId(rcId);
	            requiredCollect=this.iRequiredCollectBiz.getRequiredCollectInfo(requiredCollect);
	            if(requiredCollect.getRcId()==null){
	            	page="error";
	            }else {
	            	page="bidPriceIndex";
	                if(requiredCollect.getSupplierType().equals(TableStatus.SUPPLIER_TYPE_00)){
	                  this.getRequest().setAttribute("url", "viewRequiredCollectApplication_requiredCollectSupplier.action");
	                }else{
	                	if(requiredCollect.getBuyWay().equals("00"))
	                		this.getRequest().setAttribute("url", "viewTenderBidListMyMonitor_tenderBidListSupplier.action?rcId="+rcId);
	                	else if(requiredCollect.getBuyWay().equals("01"))
	                		this.getRequest().setAttribute("url", "viewAskBidListMyMonitor_askBidListSupplier.action?rcId="+rcId);
	                	else if(requiredCollect.getBuyWay().equals("02"))
	                		this.getRequest().setAttribute("url", "viewBiddingBidListMyMonitor_biddingBidListSupplier.action?rcId="+rcId);
	                }
	            }
				}
				this.getRequest().setAttribute("isRegister", supplierInfo.getIsRegister());
			}
		} catch (Exception e) {
			log("跳转至报价页面错误！", e);
			throw new BaseException("跳转至报价页面错误！", e);
		}
		return page;
	}
	/***************************选择主营产品树************************************/
	/**
	 * 物料树弹出框
	 */
	public String productDictionarySelect() throws BaseException{
		try {
			String proKindIds = this.getRequest().getParameter("proKindIds");
			this.getRequest().setAttribute("proKindIds", proKindIds);
	
		} catch (Exception e) {
			log.error("选择主营产品树错误！", e);
			throw new BaseException("选择主营产品树错误！", e);
		}
		return "productDictionarySelect";
		
	}
	/**
	 * 主营产品树
	 * @return
	 * @throws BaseException
	 */
	public String productDictionaryTree() throws BaseException{
		try {
			String id = this.getRequest().getParameter("id");
			if(StringUtil.isBlank(id)) id="0";
			MaterialKind materialKind=new MaterialKind();
			materialKind.setParentId(Long.parseLong(id));
			List<MaterialKind> mkList = this.iMaterialBiz.getMaterialKindList(materialKind);
			JSONArray jsonArray = new JSONArray();
			for (MaterialKind mk:mkList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", mk.getMkId());
				subJsonObject.element("pid", mk.getParentId());
				subJsonObject.element("name",mk.getMkName());
				
				if("0".equals(mk.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);
					//subJsonObject.element("nocheck", true);
					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log.error("选择品类树错误！", e);
			throw new BaseException("选择品类树错误！", e);
		}
		return null;
	}
	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}
	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}
	public IBidBulletinBiz getiBidBulletinBiz() {
		return iBidBulletinBiz;
	}
	public void setiBidBulletinBiz(IBidBulletinBiz iBidBulletinBiz) {
		this.iBidBulletinBiz = iBidBulletinBiz;
	}
	public IBidWinningBiz getiBidWinningBiz() {
		return iBidWinningBiz;
	}
	public void setiBidWinningBiz(IBidWinningBiz iBidWinningBiz) {
		this.iBidWinningBiz = iBidWinningBiz;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public IWebInfoBiz getiWebInfoBiz() {
		return iWebInfoBiz;
	}


	public void setiWebInfoBiz(IWebInfoBiz iWebInfoBiz) {
		this.iWebInfoBiz = iWebInfoBiz;
	}


	public ISysCompanyBiz getiSysCompanyBiz() {
		return iSysCompanyBiz;
	}


	public void setiSysCompanyBiz(ISysCompanyBiz iSysCompanyBiz) {
		this.iSysCompanyBiz = iSysCompanyBiz;
	}

	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}


	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public SysCompany getSysCompany() {
		return sysCompany;
	}


	public void setSysCompany(SysCompany sysCompany) {
		this.sysCompany = sysCompany;
	}


	public IAttachmentBiz getiAttachmentBiz() {
		return iAttachmentBiz;
	}


	public void setiAttachmentBiz(IAttachmentBiz iAttachmentBiz) {
		this.iAttachmentBiz = iAttachmentBiz;
	}


	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}


	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}


	public ISupplierProductInfoBiz getiSupplierProductInfoBiz() {
		return iSupplierProductInfoBiz;
	}


	public void setiSupplierProductInfoBiz(
			ISupplierProductInfoBiz iSupplierProductInfoBiz) {
		this.iSupplierProductInfoBiz = iSupplierProductInfoBiz;
	}


	public ICompanySupplierViewBiz getiCompanySupplierViewBiz() {
		return iCompanySupplierViewBiz;
	}


	public void setiCompanySupplierViewBiz(
			ICompanySupplierViewBiz iCompanySupplierViewBiz) {
		this.iCompanySupplierViewBiz = iCompanySupplierViewBiz;
	}


	public IBidResultNoticeBiz getiBidResultNoticeBiz() {
		return iBidResultNoticeBiz;
	}


	public void setiBidResultNoticeBiz(IBidResultNoticeBiz iBidResultNoticeBiz) {
		this.iBidResultNoticeBiz = iBidResultNoticeBiz;
	}


	public IBidBulletinWinningViewBiz getiBidBulletinWinningViewBiz() {
		return iBidBulletinWinningViewBiz;
	}


	public void setiBidBulletinWinningViewBiz(
			IBidBulletinWinningViewBiz iBidBulletinWinningViewBiz) {
		this.iBidBulletinWinningViewBiz = iBidBulletinWinningViewBiz;
	}
	
}
