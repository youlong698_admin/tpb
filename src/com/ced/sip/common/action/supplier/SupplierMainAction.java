package com.ced.sip.common.action.supplier;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.entity.SessionInfo;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.PasswordUtil;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.system.biz.ISysMessageLogBiz;
import com.ced.sip.system.entity.SysMessageLog;

public class SupplierMainAction extends BaseAction {
  

	private ISysMessageLogBiz  iSysMessageLogBiz;
	//消息通知实体类
	private SysMessageLog messageLog ;
	
	private ISupplierInfoBiz iSupplierInfoBiz;

	private SupplierInfo supplierInfo ;
	/**
	 * 通过网站首页的我的主页进入页面
	 * @return
	 * @throws BaseException
	 */
	public String viewSupplirtIndex() throws BaseException{
		try {
			
			supplierInfo=UserRightInfoUtil.getSupplierInfo(this.getRequest());			

			this.getRequest().setAttribute("isRegister", supplierInfo.getIsRegister());
			
		}catch (Exception e) {
				log.error("通过网站首页的我的主页进入页面错误！", e);
				throw new BaseException("通过网站首页的我的主页进入页面错误！", e);
		}
		return  "index";
	}
	/**
	 * 供应商右侧我的主页显示
	 * @return
	 * @throws BaseException
	 */
	public String viewIndexInfo() throws BaseException{
		try {
			
			supplierInfo=UserRightInfoUtil.getSupplierInfo(this.getRequest());
			
			if(messageLog==null)
			{
				messageLog=new SysMessageLog();
			}
			String messageContent=this.getRequest().getParameter("messageContent");
			messageLog.setMessageContent(messageContent);
			String sendPersonCn=this.getRequest().getParameter("sendPersonCn");
			messageLog.setSendPersonCn(sendPersonCn);
			String status=this.getRequest().getParameter("status");
			messageLog.setStatus(status);
			messageLog.setType(TableStatus.COMMON_TYPE_1);
			messageLog.setReceivePerson(UserRightInfoUtil.getSupplierId(this.getRequest())+"");
			
			List<SysMessageLog> messageList = this.iSysMessageLogBiz.getSysMessageLogList(this.getRollPageDataTables(),messageLog); 
			
			this.getRequest().setAttribute("messageList", messageList);
		}catch (Exception e) {
				log.error("供应商主页显示错误！", e);
				throw new BaseException("供应商主页显示错误！", e);
		}
		return  "indexMain";
	}
	/**
	 * 查看消息信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewMessages() throws BaseException {

		try {
			
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return "messView";

	}
	/**
	 * 触屏版读取消息总条数
	 * @return
	 * @throws BaseException
	 */
	public String viewCountMessage() throws BaseException {
		
		try{
			if(messageLog==null)
			{
				messageLog=new SysMessageLog();
			}
			messageLog.setType(TableStatus.COMMON_TYPE_1);
			messageLog.setStatus("0");
			messageLog.setReceivePerson(UserRightInfoUtil.getSupplierId(this.getRequest())+"");
			 
			HashMap<String, Object> jsonMap = new HashMap<String, Object>();
			JsonConfig jsonConfig = new JsonConfig(); 
			int count=this.iSysMessageLogBiz.getSysMessageLogCount(messageLog);
			PrintWriter out = this.getResponse().getWriter();
			JSONObject json = new JSONObject();
			jsonMap.put("count", count);
			json.putAll(jsonMap, jsonConfig);
			out.print(json.toString());
			
		} catch (Exception e) {
			log.error("触屏版读取消息总条数错误！", e);
			throw new BaseException("触屏版读取消息总条数错误！", e);
		}
		
		return null;
	}
	/**
	 * 查看消息信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewMessagesMobile() throws BaseException {

		try {
			
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return "messViewMobile";

	}
	/**
	 * 查看消息信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String findMessages() throws BaseException {

		try {
			if(messageLog==null)
			{
				messageLog=new SysMessageLog();
			}
			String messageContent=this.getRequest().getParameter("messageContent");
			messageLog.setMessageContent(messageContent);
			String sendPersonCn=this.getRequest().getParameter("sendPersonCn");
			messageLog.setSendPersonCn(sendPersonCn);
			String status=this.getRequest().getParameter("status");
			messageLog.setStatus(status);

			messageLog.setType(TableStatus.COMMON_TYPE_1);
			messageLog.setReceivePerson(UserRightInfoUtil.getSupplierId(this.getRequest())+"");
			
			List<SysMessageLog> president = this.iSysMessageLogBiz.getSysMessageLogList(this.getRollPageDataTables(),messageLog); 
			
			this.getPagejsonDataTablesByLongDate(president);
			
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return null;

	}
	/**
	 * 跳转到修改供应商密码页面
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updatePassWordInit() throws BaseException {
		try {
			
		} catch (Exception e) {
			log.error("修改供应商密码错误！", e);
			throw new BaseException("修改供应商密码错误！", e);
		}
		
		return  "updatePassWordInit";
	}
	/**
	 * 修改消息已读状态
	 * @return
	 * @throws BaseException
	 */
	public String updateMessage() throws BaseException {
		
		try{
			String messageId=this.getRequest().getParameter("messageId");
			messageLog=iSysMessageLogBiz.getSysMessageLog(new Long(messageId));
			messageLog.setLookDate(DateUtil.getCurrentDateTime());
			messageLog.setStatus("1");  //已读
			this.iSysMessageLogBiz.updateSysMessageLog(messageLog); 
			
			this.getRequest().setAttribute("message", "修改已读状态成功");
			this.getRequest().setAttribute("operModule", "修改消息提醒已读状态信息");
			
		} catch (Exception e) {
			log.error("修改消息已读状态-显示错误！", e);
			throw new BaseException("修改消息已读状态-显示错误！", e);
		}
		
		return null;
	}
	/**
	 * 修改供应商密码
	 * 
	 * @return
	 * @throws BaseException
	 */
	public String updatePassWord( ) throws BaseException {
		try {
			String message="";
			String oldPassWord=this.getRequest().getParameter("oldPassWord");
			String newPassWord=this.getRequest().getParameter("newPassWord");
			supplierInfo = iSupplierInfoBiz.getSupplierInfo(supplierInfo.getSupplierId());
			if(supplierInfo!=null){
				if(!supplierInfo.getSupplierLoginPwd().equals(PasswordUtil.encrypt(oldPassWord))){
					message="原始密码错误,请重新输入";
				}else{
					SessionInfo info = UserRightInfoUtil.getSessionInfo(this.getRequest() ) ;
					supplierInfo.setSupplierLoginPwd(PasswordUtil.encrypt(newPassWord));
					this.iSupplierInfoBiz.updateSupplierInfo(supplierInfo);
					HttpSession session = this.getSession() ;
					for (java.util.Enumeration e = session.getAttributeNames() ; e.hasMoreElements() ;) {
						session.removeAttribute((String)e.nextElement());
				    }
					info.setSupplierInfo(supplierInfo);
					info.setSupplierId(supplierInfo.getSupplierId());
					info.setSupplierName(supplierInfo.getSupplierName());
					info.setSupplierLoginName(supplierInfo.getSupplierLoginName());
					UserRightInfoUtil.setSessionInfo(info, this.getRequest() ) ;
					message="修改成功";
				}
				this.getRequest().setAttribute("message", message);
			}			
		} catch (Exception e) {
			log.error("修改供应商密码错误！", e);
			throw new BaseException("修改供应商密码错误！", e);

		}
		
		return "updatePassWordInit";
	}

	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}

	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}

	public SupplierInfo getSupplierInfo() {
		return supplierInfo;
	}

	public void setSupplierInfo(SupplierInfo supplierInfo) {
		this.supplierInfo = supplierInfo;
	}
	public ISysMessageLogBiz getiSysMessageLogBiz() {
		return iSysMessageLogBiz;
	}
	public void setiSysMessageLogBiz(ISysMessageLogBiz iSysMessageLogBiz) {
		this.iSysMessageLogBiz = iSysMessageLogBiz;
	}
	
}
