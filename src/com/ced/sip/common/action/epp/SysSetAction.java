package com.ced.sip.common.action.epp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.biz.ISysSetBiz;
import com.ced.sip.common.utils.Freemarker;
import com.ced.sip.common.utils.StringUtil;

public class SysSetAction extends BaseAction {
      
	private ISysSetBiz iSysSetBiz;
	/**
	 * 跳转至代码生成页面
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws BaseException
	 */
	public String viewToProductCode( ) throws BaseException {
		try {
			List tableList=iSysSetBiz.getAllTableList();
			//System.out.println(tableList.size());
			this.getRequest().setAttribute("tableList", tableList);
			
			String tableName=this.getRequest().getParameter("tableName");
			if(StringUtil.isNotBlank(tableName)){
				List columnsList=iSysSetBiz.getAllColums(tableName);
				this.getRequest().setAttribute("columnsList", columnsList);
			}
			this.getRequest().setAttribute("tableNameSelected", tableName);
		} catch (Exception e) {
			log.error("跳转至代码生成页面错误！", e);
			throw new BaseException("跳转至代码生成页面错误！", e);
		}
		
		return  "productCode"   ;
	}
	/**
	 * 执行代码生成
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws BaseException
	 */
	public String saveProCode( ) throws BaseException {
		try {

			/* ============================================================================================= */
			String packageName = this.getRequest().getParameter("packageName");  			//包名				========1
			String objectName = this.getRequest().getParameter("objectName");	   			//类名				========2
			String objectComments = this.getRequest().getParameter("objectComments");	   	//类名注释				========2
			String[] fields = this.getRequest().getParameterValues("field");	   	   		//属性总数
			
			List<String[]> fieldList = new ArrayList<String[]>();   	//属性集合  ========4
			String[] strs=new String[10];
			String id="",idUp="";
			for(int i=0; i< fields.length; i++){
				strs=new String[10];
				strs[0]=this.getRequest().getParameter("field0"+i);  //MK_THREE_ID	
				strs[1]=this.getRequest().getParameter("field1"+i);
				strs[2]=this.getRequest().getParameter("field2"+i);
				strs[3]=this.getRequest().getParameter("field3"+i);
				strs[4]=this.getRequest().getParameter("field4"+i);
				strs[5]=this.getRequest().getParameter("field5"+i);

				strs[6]=captureName(strs[0]);   //属性集合  首字母小写  下划线后的字母大写 如  MK_THREE_ID 转换后的为 mkThreeId	
				

				strs[7]=captureNameGet(strs[0]); 	//属性集合  首字母小写  下划线后的字母大写 如  MK_THREE_ID 转换后的为 MkThreeId	
				

				strs[8]=this.getRequest().getParameter("field8"+i);
				

				strs[9]=this.getRequest().getParameter("field9"+i);
				
				fieldList.add(strs);	//属性放到集合里面

				if(i==0) {  id=strs[6]; idUp=strs[7];  }
			}
			String newpackageName=packageName.replace(".", "/");
			Map<String,Object> root = new HashMap<String,Object>();		//创建数据模型
			root.put("fieldList", fieldList);                           //属性集合
			root.put("packageName", packageName);						//包名
			root.put("objectComments", objectComments);					//类名注释
			root.put("objectName", objectName);							//类名(全大写)
			String objectNameUpper=captureNameGet(objectName);
			root.put("objectNameUpper",objectNameUpper);	            //类名(首字母大写)
			String objectNameLower=captureName(objectName);  
			root.put("objectNameLower",objectNameLower);	            //类名  首字母小写
			root.put("nowDate", new Date());							//当前日期
			root.put("id", id);							//id
			root.put("idUp", idUp);							//id大写
			
			/* ============================================================================================= */
			
			String filePath = "E:/workspace-product/epp";						//存放路径
			String ftlPath = "createCode";								//ftl路径
			
			/*生成action*/
			Freemarker.printFile("actionTemplate.ftl", root, "/src/com/ced/sip/"+newpackageName+"/action/"+objectNameUpper+"Action.java", filePath, ftlPath);
			
			/*生成biz*/
			Freemarker.printFile("bizImplTemplate.ftl", root, "/src/com/ced/sip/"+newpackageName+"/biz/impl/"+objectNameUpper+"BizImpl.java", filePath, ftlPath);

			/*生成ibiz*/
			Freemarker.printFile("ibizTemplate.ftl", root, "/src/com/ced/sip/"+newpackageName+"/biz/I"+objectNameUpper+"Biz.java", filePath, ftlPath);

			/*生成hbm.xml*/
			Freemarker.printFile("mapperOracleTemplate.ftl", root, "/src/com/ced/sip/"+newpackageName+"/entity/"+objectNameUpper+".hbm.xml", filePath, ftlPath);
			
			/*生成entity*/
			Freemarker.printFile("entityTemplate.ftl", root, "/src/com/ced/sip/"+newpackageName+"/entity/"+objectNameUpper+".java", filePath, ftlPath);
			
			/*生成editJsp*/
			//Freemarker.printFile("jsp_updateTemplate.ftl", root, "/WebRoot/"+newpackageName+"/"+objectNameLower+"/"+objectNameLower+"_update.jsp", filePath, ftlPath);
			
			/*生成saveJsp*/
			Freemarker.printFile("jsp_saveTemplate.ftl", root, "/WebRoot/"+newpackageName+"/"+objectNameLower+"/"+objectNameLower+"_save.jsp", filePath, ftlPath);
			
			/*生成viewJsp*/
			Freemarker.printFile("jsp_viewTemplate.ftl", root, "/WebRoot/"+newpackageName+"/"+objectNameLower+"/"+objectNameLower+"_view.jsp", filePath, ftlPath);
			
			/*生成detailJsp*/
			Freemarker.printFile("jsp_detailTemplate.ftl", root, "/WebRoot/"+newpackageName+"/"+objectNameLower+"/"+objectNameLower+"_detail.jsp", filePath, ftlPath);
			
			
			List tableList=iSysSetBiz.getAllTableList();
			this.getRequest().setAttribute("tableList", tableList);
			
		} catch (Exception e) {
			log.error("跳转至代码生成页面错误！", e);
			throw new BaseException("跳转至代码生成页面错误！", e);
		}
		
		return  "productCode"   ;
	}
	 public static String captureName(String field) {
		    //System.out.println(field);
		    field=field.toLowerCase();
	        String[] str=field.split("_");
	        String rerurnStr="";
	        for(int i=0;i<str.length;i++){
	        	if(i>0){
			        char[] cs=str[i].toCharArray();
			        cs[0]-=32;
			        rerurnStr+=String.valueOf(cs);
	        	}else{
	        		rerurnStr=str[i];
	        	}
	        }
	        return rerurnStr;
	 }
	 public static String captureNameGet(String field) {
		    field=field.toLowerCase();
	        String[] str=field.split("_");
	        String rerurnStr="";
	        for(int i=0;i<str.length;i++){
        	    char[] cs=str[i].toCharArray();
		        cs[0]-=32;
		        rerurnStr+=String.valueOf(cs);	        	
	        }
	        return rerurnStr;
	 }


	public ISysSetBiz getiSysSetBiz() {
		return iSysSetBiz;
	}
	public void setiSysSetBiz(ISysSetBiz iSysSetBiz) {
		this.iSysSetBiz = iSysSetBiz;
	}
	
	
}
