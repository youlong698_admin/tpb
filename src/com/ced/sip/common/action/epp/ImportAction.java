package com.ced.sip.common.action.epp;


import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;

public class ImportAction extends BaseAction {
	// 类型（上传、导入）
	private String type;
	
	/**
	 * 导入模块
	 * @return
	 * @throws BaseException 
	 * @Action
	 * @author lgl
	 */
	public String Template() throws BaseException {
		
		try{
			 type="template";
			 this.getRequest().setAttribute("type", type);
		} catch (Exception e) {
			log.error("导入文件模板错误！", e);
			throw new BaseException("导入文件模板错误！", e);
		}
		return "index" ;
		
	}
    public String updateTemplate() throws BaseException {
		
		try{
			type="updateTemplate";
			 this.getRequest().setAttribute("type", type);
		} catch (Exception e) {
			log.error("导入文件模块错误！", e);
			throw new BaseException("导入文件模块错误！", e);
		}
		return "index" ;
		
	}
	
}
