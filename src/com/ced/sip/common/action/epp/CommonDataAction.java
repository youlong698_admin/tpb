package com.ced.sip.common.action.epp;

import java.io.PrintWriter;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.DictStatus;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierInfo;
import com.ced.sip.supplier.entity.SupplierProductGroup;
import com.ced.sip.system.biz.IDepartmentsBiz;
import com.ced.sip.system.biz.IMaterialBiz;
import com.ced.sip.system.biz.IQualityCategoryBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Departments;
import com.ced.sip.system.entity.MaterialKind;
import com.ced.sip.system.entity.MaterialList;
import com.ced.sip.system.entity.QualityCategory;
import com.ced.sip.system.entity.Users;

public class CommonDataAction extends BaseAction {

	//部门业务逻辑层
	private IDepartmentsBiz iDepartmentsBiz;
	//用户业务逻辑层
	private IUsersBiz iUsersBiz;
	//资质文件类别业务逻辑层
	private IQualityCategoryBiz iQualityCategoryBiz;
	//物资类别树
	private IMaterialBiz iMaterialBiz;
	// 供应商基本信息 服务类
	private ISupplierInfoBiz iSupplierInfoBiz;

	
	//用户实体类
	private Users users=new Users() ;
	private MaterialKind materialKind;
	private SupplierInfo supplierInfo;
	
	/**
	 * 选择组织机构弹框
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDeptSelect() {
		return "deptSelect" ;
	}
	
	/**
	 * 选择人员弹出窗口 左侧查看部门信息首页列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDeptIndex() {
		String ul=this.getRequest().getParameter("ul");
		this.getRequest().setAttribute("ul", ul);
		return INDEX ;
	}
	/**
	 * 选择人员弹出窗口  查看部门信息表初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDeptInitTree() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}
		
		return "tree" ;
	}
	/**
	 * 查看部门信息表树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewDeptTree() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			//获取部门所以信息
			Departments departments=new Departments();
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) departments.setComId(comId);
			List<Departments> deptList =this.iDepartmentsBiz.getDepartmentsList(departments);			
			
			JSONArray jsonArray = new JSONArray();
			for (Departments dept:deptList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", dept.getDepId());
				subJsonObject.element("pid", dept.getParentDeptId());
				subJsonObject.element("name",dept.getDeptName());
				
				if("1".equals(dept.getDepId()+""))
				{
					subJsonObject.element("open", true);
				}				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log.error("查看部门信息表初始树列表错误！", e);
			throw new BaseException("查看部门信息表初始树列表错误！", e);
		}
		
		return null ;
	}
	/**
	 * 选择人员弹出窗口  人员列表信息页面
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewTopIndex() throws Exception {
		
		String deptId = this.getRequest().getParameter("deptId");
		this.getRequest().setAttribute("deptId", deptId);
		return "top" ;
	}
	/**
	 * 选择人员弹出窗口 查询人员信息列表
	 * @return
	 * @throws BaseException
	 */
	public String findTopIndex() throws BaseException {
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String type = this.getRequest().getParameter("type");
			if(users==null){
				users = new Users();
			}
			String userChinesename = this.getRequest().getParameter("userChinesename");
			users.setUserChinesename(userChinesename);
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) users.setComId(comId);
			String deptId = this.getRequest().getParameter("deptId");
			if(StringUtil.isNotBlank(deptId))
			{
				users.setDepId(new Long(deptId));
			}
			if("1".equals(deptId))
			{
				users.setDepId(null);
			}
			if(StringUtil.isNotBlank(userChinesename))
			{
				users.setDepId(null);
			}
			if(StringUtil.isNotBlank(users)){
				if("addOpUser".equals(type)){
					users.setUserType(UserRightInfoUtil.getUserType(this.getRequest()));
				}	
			}
			List<Users> president = this.iUsersBiz.getUsersListRight( this.getRollPageDataTables(), users ) ;
			for(Users users:president){
				users.setDepName(BaseDataInfosUtil.convertDeptIdToName(users.getDepId()));
			}
			
			this.getPagejsonDataTables(president);
		} catch (Exception e) {
			log.error("选择人员弹框人员信息列表查询错误！", e);
			throw new BaseException("选择人员弹框人员信息列表查询！", e);
		}
		
		return null ;
	}
	/**
	 * 选择人员弹出窗口  底部页面
	 * @return
	 * @throws Exception
	 */
	public String viewButtonIndex() throws BaseException {
		
		String ul=this.getRequest().getParameter("ul");
		if(ul=="-1" || "-1".equals(ul)){ul="";}
		if( StringUtil.isNotBlank(ul)){
			
			String [] mn = ul.split(",");
			String mc ="";
			for(int i=0;i<mn.length;i++){
				String m=BaseDataInfosUtil.convertUserIdToChnName(new Long(mn[i])).toString();
				mc+=mn[i]+":"+m+",";
			}
			//System.out.println(mc);
			this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
		}else{
			this.getRequest().setAttribute("ul", "-1");
		}
		return "button" ;
	}
	/***************************选择资质类别************************************/
	/**
	 * 选择资质文件类别
	 * @return
	 * @throws BaseException
	 */
	public String viewCertIndex() throws BaseException{
		String ul=this.getRequest().getParameter("ul");
		this.getRequest().setAttribute("ul", ul);
		return "certIndex";
	}
	/**
	 * 选择资质文件类别上部
	 * @return
	 * @throws Exception
	 */
	public String viewCertTop() throws BaseException {
		
		return "certTop" ;
	}
	
	public String findCertTop() throws BaseException {
		try{
			QualityCategory qualityCategory=new QualityCategory(); 
			String qualityName=this.getRequest().getParameter("qualityName");
			qualityCategory.setQualityName(qualityName);
			this.setListValue(this.iQualityCategoryBiz.getQualityCategoryList(this.getRollPageDataTables(),qualityCategory ));		
			this.getPagejsonDataTables(this.getListValue());		
		} catch (Exception e) {
			log.error("查看资质文件类别信息表初始树列表错误！", e);
			throw new BaseException("查看资质文件类别信息表初始树列表错误！", e);
		}
		return null;
	}
	/**
	 * 选择资质文件类别下部
	 * @return
	 * @throws Exception
	 */
	public String viewCertButton() throws Exception {
		String ul=this.getRequest().getParameter("ul");
		if(ul=="-1" || "-1".equals(ul)){ul="";}
		if( StringUtil.isNotBlank(ul)){
			String [] mn = ul.split(",");
			String mc ="";
			QualityCategory qualityCategory=new QualityCategory();
			List<QualityCategory> listdict=this.iQualityCategoryBiz.getQualityCategoryList(qualityCategory);			
			for(int i=0;i<mn.length;i++){				
				
				for(QualityCategory di:listdict)
				{
					if(mn[i].equals(di.getQcId().toString()))
					{
						mc+=mn[i]+":"+di.getQualityName()+",";
					}
					
				}
				
			}
			if(mc=="" || "".equals(mc))
			{
				this.getRequest().setAttribute("ul", "-1");
			}else
			{
				this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
			}
		}else{
			this.getRequest().setAttribute("ul", "-1");
		}
		
		return "certbutton" ;
	}
	
	/***************************供应商考核选择供应商************************************/
	/**
	 * 查看供应商考核选择供应商列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewEvaluateSupplierIndex() {
		String ul=this.getRequest().getParameter("ul");
		this.getRequest().setAttribute("ul", ul);
		return INDEX ;
	}
	

	/**
	 * 选择供应商上部
	 * @return
	 * @throws Exception
	 */
	public String viewEvaluateSupplierTopIndex() throws Exception {
	
		return "top" ;
	}
	/**
	 * 选择供应商 查询供应商
	 * @return
	 * @throws Exception
	 */
	public String findEvaluateSupplierTopIndex() throws Exception {
		try {
		String supplierName=this.getRequest().getParameter("supplierName");
		supplierInfo=new SupplierInfo();
		supplierInfo.setSupplierName(supplierName);	
		List<SupplierInfo> list=iSupplierInfoBiz.getSupplierInfoList(this.getRollPageDataTables(),supplierInfo);
		for(SupplierInfo supplierInfo:list){
			supplierInfo.setStatusCn(BaseDataInfosUtil
					.convertDictCodeToName(supplierInfo.getStatus(),
							DictStatus.COMMON_DICT_TYPE_1713));
		}
		this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("供应商考核选择供应商查询错误！", e);
			throw new BaseException("供应商考核选择供应商查询错误！", e);
		}
		return null ;
	}
	/**
	 * 选择供应商下部
	 * @return
	 * @throws Exception
	 */
	
	public String viewEvaluateSupplierButtonIndex() throws Exception {
		
		String ul = this.getRequest().getParameter("ul");

		if (ul == "-1" || "-1".equals(ul)) {
			ul = "";
		}
		if (StringUtil.isNotBlank(ul)) {

			String[] mn = ul.split(",");
			String mc = "";
			for (int i = 0; i < mn.length; i++) {
				String m = iSupplierInfoBiz.getSupplierInfo(new Long(mn[i]))
						.getSupplierName();
				mc += mn[i] + ":" + m + ",";
			}
			//System.out.println(mc.substring(0, mc.lastIndexOf(",")));
			this.getRequest().setAttribute("ul",
					mc.substring(0, mc.lastIndexOf(",")));
		} else {
			this.getRequest().setAttribute("ul", "-1");
		}

		return "button" ;
	}

	/***************************需求计划选择采购物资************************************/	
	/**
	 * 采购品类选择首页
	 * @return
	 * @throws BaseException 
	 */
	public String viewMaterialInfoIndex() throws BaseException{
		try{
		} catch (Exception e) {
			log("品类库管理首页错误！", e);
			throw new BaseException("品类库管理首页错误！", e);
		}
		return INDEX;
	}
	/**
	 * 查看品类类别信息初始树列表 getTree
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMaterialKindInitTree() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log("查看品类类别信息初始树列表错误！", e);
			throw new BaseException("查看品类类别信息初始树列表错误！", e);
		}
		return TREE ;
	}
	
	/**
	 * 查看品类类别信息树列表 
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public void viewMaterialKindTree() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			materialKind=new MaterialKind();
			materialKind.setComId(comId);
			List<MaterialKind> mkList = iMaterialBiz.getMaterialKindList(materialKind);
			JSONObject allJSONObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONObject subJsonObject = new JSONObject();
			for (MaterialKind maKind:mkList) {
				subJsonObject = new JSONObject();
				subJsonObject.element("id", maKind.getMkId());
				subJsonObject.element("name", maKind.getMkName());

				if ( "0".equals( maKind.getIsHaveChild() ) ) {
					subJsonObject.element("type", "folder");
					subJsonObject.element("children", true);
				} else {
					subJsonObject.element("type", "item");
				}
              

				subJsonObject.element("additionalParameters", subJsonObject);
				jsonArray.add(subJsonObject);
			}
			allJSONObject.element("status", "OK");
			allJSONObject.element("data", jsonArray);
			PrintWriter writer = getResponse().getWriter();  
	        writer.print(allJSONObject);  
	        writer.flush();  
	        writer.close(); 
		} catch (Exception e) {
			log("查看品类类别信息初始树列表错误！", e);
			throw new BaseException("查看品类类别信息初始树列表错误！", e);
		}
	}
	
	/**
	 * 查看品类信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewMaterialList() throws BaseException {
		
		try{
			if(materialKind!=null){
			    if(materialKind.getMkId()!=null && materialKind.getMkId()!=0)
			    materialKind = iMaterialBiz.getMaterialKind(materialKind.getMkId());
			}
		} catch (Exception e) {
			log.error("查看品类信息列表错误！", e);
			throw new BaseException("查看品类信息列表错误！", e);
		}
		
		return VIEW ;
		
	}
	
	/**
	 * 查看品类信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String findMaterialList() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			String mkId = this.getRequest().getParameter("mkId");
			MaterialList materialList = new MaterialList();
			materialList.setComId(comId);
			if(StringUtil.isNotBlank(mkId) && !"0".equals(mkId)){
			   materialKind = iMaterialBiz.getMaterialKind(Long.parseLong(mkId));
			   materialList.setMkKindId(materialKind.getMkId());
			}
			String materialCode = this.getRequest().getParameter("materialCode");
			materialList.setMaterialCode(materialCode);
			String materialName = this.getRequest().getParameter("materialName");
			materialList.setMaterialName(materialName);
			this.setListValue( iMaterialBiz.getMaterialListList(getRollPageDataTables(), materialList));
			this.getPagejsonDataTables(this.getListValue());
			
		} catch (Exception e) {
			log.error("查看品类信息列表错误！", e);
			throw new BaseException("查看品类信息列表错误！", e);
		}
		
		return null ;
		
	}
	/***********************************************邀请供应商列表****************************************************************************/
	/**
	 * 邀请供应商选择器--查看邀请供应商信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewInviteSupplier() throws BaseException {
		try{
            String ul=this.getRequest().getParameter("ul");
            String rcId=this.getRequest().getParameter("rcId");
			this.getRequest().setAttribute("ul", ul);
			this.getRequest().setAttribute("rcId", rcId);
			List  supplierList = this.iSupplierInfoBiz.getSuppListByRcId(Long.parseLong(rcId),null);
			
			
			if(supplierList.size()>=0 ){
				this.getRequest().setAttribute("ifTo", "01");
			}else {
				this.getRequest().setAttribute("ifTo", "11");
			}			
		} catch (Exception e) {
			log.error("邀请供应商选择器--查看邀请供应商信息列表错误！", e);
			throw new BaseException("邀请供应商选择器--查看邀请供应商信息列表错误！", e);
		}
		return "suppplierIndex" ;		
	}
	
	/**
	 * 供应商选择 左侧页面
	 * @return
	 * @throws Exception
	 */
	public String viewSupplierLeft() throws BaseException {
		String ul=this.getRequest().getParameter("ul");
        String rcId=this.getRequest().getParameter("rcId");
		String ifTo=this.getRequest().getParameter("ifTo");
		this.getRequest().setAttribute("ul", ul);
		this.getRequest().setAttribute("rcId", rcId);		
		this.getRequest().setAttribute("ifTo", ifTo);		
		return "supplierLeft" ;
	}
    /**
     * 供应商选择 上部页面
     * @return
     * @throws Exception
     */
	public String viewSupplierMaterial() throws BaseException {
	    String rcId=this.getRequest().getParameter("rcId");
		this.getRequest().setAttribute("rcId", rcId);
		return "supplierMaterial" ;
	}
    /**
     * 供应商选择器----查看本类别的供应商
     * @return
     * @throws BaseException
     */
	public String findSupplierMaterial() throws BaseException {
		try{
			String rcId=this.getRequest().getParameter("rcId");
			String supplierName=this.getRequest().getParameter("supplierName");
			if(supplierInfo==null)
			{
				supplierInfo=new SupplierInfo();
			}
			supplierInfo.setSupplierName(supplierName);
		    List<SupplierInfo>  supplierList = this.iSupplierInfoBiz.getSuppListByRcId(Long.parseLong(rcId),supplierInfo);
		    for(SupplierInfo supplierInfo:supplierList){
				supplierInfo.setStatusCn(BaseDataInfosUtil
						.convertDictCodeToName(supplierInfo.getStatus(),
								DictStatus.COMMON_DICT_TYPE_1713));
			}
			this.getPagejsonDataTables(supplierList);
		} catch (Exception e) {
			log.error("邀请供应商选择器--查看邀请供应商信息列表错误！", e);
			throw new BaseException("邀请供应商选择器--查看邀请供应商信息列表错误！", e);
		}
	    return null ;
	}
	/**
	 * 供应商选择器---全部供应商
	 * @return
	 * @throws Exception
	 */
	public String viewSupplierAll() throws BaseException {
		return "supplierAll" ;
	}
	/**
	 * 供应商选择器-----查询全部供应商
	 * @return
	 * @throws Exception
	 */
	public String findSupplierAll() throws BaseException {
		try{
			String supplierName=this.getRequest().getParameter("supplierName");
			if(supplierInfo==null)
			{
				supplierInfo=new SupplierInfo();
			}
			supplierInfo.setSupplierName(supplierName);
			List<SupplierInfo> list=this.iSupplierInfoBiz.getSupplierInfoList(getRollPageDataTables(), supplierInfo);		
			 for(SupplierInfo supplierInfo:list){
					supplierInfo.setStatusCn(BaseDataInfosUtil
							.convertDictCodeToName(supplierInfo.getStatus(),
									DictStatus.COMMON_DICT_TYPE_1713));
				}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("邀请供应商选择器--查看邀请供应商信息列表错误！", e);
			throw new BaseException("邀请供应商选择器--查看邀请供应商信息列表错误！", e);
		}
		return null ;
	}
    /**
     * 供应商选择器-----选择下部页面
     * @return
     * @throws BaseException
     */
	public String viewSupplierBotton() throws BaseException {
		String ul = this.getRequest().getParameter("ul");
		String m="";
		if (ul == "-1" || "-1".equals(ul)) {
			ul = "";
		}
		if (StringUtil.isNotBlank(ul)) {
			
			String [] mn = ul.split(",");
			String mc ="";
			for(int i=0;i<mn.length;i++){
				    m=iSupplierInfoBiz.getSupplierInfo(new Long(mn[i])).getSupplierName();
					mc+=mn[i]+":"+m+":1,";
			}
			this.getRequest().setAttribute("ul", mc.substring(0, mc.lastIndexOf(",")));
		}else{
		    this.getRequest().setAttribute("ul", "-1");
		}
		return "supplierBotton" ;
	}
	/***************************选择主营产品树************************************/
	/**
	 * 主营产品树弹出框
	 */
	public String viewMaterialKindWindow() throws BaseException{
		try {
			String proKindIds = this.getRequest().getParameter("proKindIds");
			this.getRequest().setAttribute("proKindIds", proKindIds);
	
		} catch (Exception e) {
			log.error("选择主营产品树错误！", e);
			throw new BaseException("选择主营产品树错误！", e);
		}
		return "viewMaterialKind_window";
		
	}
	/**
	 * 主营产品树
	 * @return
	 * @throws BaseException
	 */
	public String viewMaterialKindWithTree() throws BaseException{
		try {
			String id = this.getRequest().getParameter("id");
			if(StringUtil.isBlank(id)) id="0";
			MaterialKind materialKind=new MaterialKind();
			materialKind.setParentId(Long.parseLong(id));
			List<MaterialKind> mkList = this.iMaterialBiz.getMaterialKindList(materialKind);
			JSONArray jsonArray = new JSONArray();
			for (MaterialKind mk:mkList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", mk.getMkId());
				subJsonObject.element("pid", mk.getParentId());
				subJsonObject.element("name",mk.getMkName());
				
				if("0".equals(mk.getIsHaveChild()))
				{
					subJsonObject.element("isParent", true);
					//subJsonObject.element("nocheck", true);
					
				}
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log.error("选择品类树错误！", e);
			throw new BaseException("选择品类树错误！", e);
		}
		return null;
	}
	/***************************选择供应商自己的主营产品树************************************/
	/**
	 * 供应商自己的主营产品树弹出框
	 */
	public String viewMyProductDictionaryWindow() throws BaseException{
		try {
			String proKindIds = this.getRequest().getParameter("proKindIds");
			this.getRequest().setAttribute("proKindIds", proKindIds);
	
		} catch (Exception e) {
			log.error("选择供应商自己的主营产品树错误！", e);
			throw new BaseException("选择供应商自己的主营产品树错误！", e);
		}
		return "viewMyProductDictionary_window";
		
	}
	/**
	 * 供应商自己的主营产品树
	 * @return
	 * @throws BaseException
	 */
	public String viewMyProductDictionaryWithTree() throws BaseException{
		try {
			Long supplierId=UserRightInfoUtil.getSupplierId(this.getRequest());	
			SupplierProductGroup supplierProductGroup=new SupplierProductGroup();
			supplierProductGroup.setSupplierId(supplierId);
			List<SupplierProductGroup> sList = this.iSupplierInfoBiz.getSupplierProductGroupList(supplierProductGroup);
			JSONArray jsonArray = new JSONArray();
			for (SupplierProductGroup sGroup:sList) {
				JSONObject subJsonObject = new JSONObject();
				subJsonObject.element("id", sGroup.getPdId());
				subJsonObject.element("pid", 0);
				subJsonObject.element("name",sGroup.getPdName());
				subJsonObject.element("isParent", false);
				subJsonObject.element("nocheck", false);
				
				jsonArray.add(subJsonObject);
			}
			PrintWriter writer = getResponse().getWriter();  
			//System.out.println(jsonArray);
	        writer.print(jsonArray);  
	        writer.flush();  
	        writer.close(); 
	        
		} catch (Exception e) {
			log.error("选择供应商自己的品类树错误！", e);
			throw new BaseException("选择供应商自己的品类树错误！", e);
		}
		return null;
	}
	/**
	 * 选择核心供应商信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewSupplierBaseInfoCompany() throws BaseException {
		this.getRequest().setAttribute(
				"statusMap",
				BaseDataInfosUtil
						.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1713));
		this.getRequest().setAttribute(
				"levelMap",
				BaseDataInfosUtil
						.getDictInfoToMap(DictStatus.COMMON_DICT_TYPE_1712));

		return "supplierCompany";
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public IDepartmentsBiz getiDepartmentsBiz() {
		return iDepartmentsBiz;
	}
	public void setiDepartmentsBiz(IDepartmentsBiz iDepartmentsBiz) {
		this.iDepartmentsBiz = iDepartmentsBiz;
	}
	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}
	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public IQualityCategoryBiz getiQualityCategoryBiz() {
		return iQualityCategoryBiz;
	}
	public void setiQualityCategoryBiz(IQualityCategoryBiz iQualityCategoryBiz) {
		this.iQualityCategoryBiz = iQualityCategoryBiz;
	}
	public IMaterialBiz getiMaterialBiz() {
		return iMaterialBiz;
	}
	public void setiMaterialBiz(IMaterialBiz iMaterialBiz) {
		this.iMaterialBiz = iMaterialBiz;
	}
	public MaterialKind getMaterialKind() {
		return materialKind;
	}
	public void setMaterialKind(MaterialKind materialKind) {
		this.materialKind = materialKind;
	}
	
	
}
