package com.ced.sip.common.action.epp;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;




import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.analysis.biz.IPurchaseAmountBiz;
import com.ced.sip.analysis.entity.PurchaseAmount;
import com.ced.sip.common.BaseDataInfosUtil;
import com.ced.sip.common.TableStatus;
import com.ced.sip.common.TableStatusMap;
import com.ced.sip.common.UserRightInfoUtil;
import com.ced.sip.common.WorkFlowStatus;
import com.ced.sip.common.WorkFlowStatusMap;
import com.ced.sip.common.utils.DateUtil;
import com.ced.sip.common.utils.ListToMap;
import com.ced.sip.common.utils.PasswordUtil;
import com.ced.sip.contract.biz.IContractInfoBiz;
import com.ced.sip.order.biz.IOrderInfoBiz;
import com.ced.sip.purchase.base.biz.IBidAbnomalBiz;
import com.ced.sip.purchase.base.biz.IBidAwardBiz;
import com.ced.sip.purchase.base.biz.IRequiredCollectBiz;
import com.ced.sip.purchase.base.entity.BidAward;
import com.ced.sip.supplier.biz.IEvaluateSupplierBiz;
import com.ced.sip.supplier.biz.IEvaluateSupplierRelationBiz;
import com.ced.sip.supplier.biz.ISupplierInfoBiz;
import com.ced.sip.supplier.entity.SupplierEvaluateUser;
import com.ced.sip.supplier.entity.SupplierEvaluations;
import com.ced.sip.system.biz.INoticesBiz;
import com.ced.sip.system.biz.ISysMessageLogBiz;
import com.ced.sip.system.biz.IUsersBiz;
import com.ced.sip.system.entity.Notices;
import com.ced.sip.system.entity.SysMessageLog;
import com.ced.sip.system.entity.Users;
import com.ced.sip.workflow.base.service.SnakerEngineFacets;
import com.ced.sip.workflow.biz.IWaitForDealBiz;
import com.ced.sip.workflow.entity.WaitWorkItem;

public class IndexInfoAction extends BaseAction {
	@Autowired
	private SnakerEngineFacets facets;
	
	private IUsersBiz iUsersBiz  ;
    
	private IWaitForDealBiz iWaitForDealBiz;	
	//公告通知服务类
	private INoticesBiz iNoticesBiz  ;
	//消息通知服务类
	private ISysMessageLogBiz  iSysMessageLogBiz;

	// 供应商考核 服务类
	private IEvaluateSupplierBiz iEvaluateSupplierBiz;
	// 供应商考核 实例
	private SupplierEvaluations supplierEvaluations ;
	private IEvaluateSupplierRelationBiz iEvaluateSupplierRelationBiz;
	// 供应商考核人信息 实例
	private SupplierEvaluateUser supplierEvaluateUser;
    //汇总分包服务类
	private IRequiredCollectBiz iRequiredCollectBiz;
	
	//供应商服务类
	private ISupplierInfoBiz iSupplierInfoBiz;//新增供应商个数
	
    //授标查询
	private IBidAwardBiz iBidAwardBiz;
	
	//订单服务类
	private IOrderInfoBiz iOrderInfoBiz;
	
	//合同服务类
	private IContractInfoBiz iContractInfoBiz;
	
    //终止服务类
	private IBidAbnomalBiz iBidAbnomalBiz;
	
	// 采购金额统计
	private IPurchaseAmountBiz iPurchaseAmountBiz;
	private PurchaseAmount purchaseAmount;
	
	//公告通知实体类
	private Notices notices ;
	//消息通知实体类
	private SysMessageLog messageLog ;
	
	private Long wfdNewSum;//待办新任务数量
	
	private Long wfdOldSum;//待办总数量
	
	private HashMap<String, Object> jsonMap = new HashMap<String, Object>();
	
	/**
	 * 查看首页信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String viewIndexInfo() throws BaseException {
		String viewUrl="view";
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());
			String user ="";
			if(!UserRightInfoUtil.ifSystemManagerRole(getRequest())){
				user = UserRightInfoUtil.getUserName(getRequest());
			}
			Map<String,Object> WfMap=new HashMap<String,Object>();
			WfMap.put("user", user);
			//待办
			int wfdCount = this.iWaitForDealBiz.getWaitForDealSize(WfMap) ;
			this.getRequest().setAttribute("wfdCount", wfdCount);			

			//已办
			int wadCount = this.iWaitForDealBiz.getWaitAlreadyDoneSize(WfMap) ;
			this.getRequest().setAttribute("wadCount", wadCount);
			
			//消息提醒
			messageLog=new SysMessageLog();			
			messageLog.setStatus("0");
			messageLog.setReceivePerson(UserRightInfoUtil.getUserName(this.getRequest())); 
			int messageCount=this.iSysMessageLogBiz.getSysMessageLogCount(messageLog);
			this.getRequest().setAttribute("messageCount", messageCount);
			
			
			Map<String, Object> mapParams=new HashMap<String, Object>();
			mapParams.put("bidStatus", TableStatus.BID_STATUS_1);
			//在途采购数   招  询  竞
			String sqlStr = UserRightInfoUtil.getUserDepartNameHql(
					getRequest(), "de");
			int reqCount=this.iRequiredCollectBiz.countRequiredCollectListList(mapParams,sqlStr);
			this.getRequest().setAttribute("reqCount", reqCount);
			
			//本年
			String yearStart=DateUtil.getYears(new Date())+"-01-01";
			String yearEnd=DateUtil.getYears(new Date())+"-12-31";
			//本月
			String dateStart=DateUtil.getDateFristDay();
			String dateEnd=DateUtil.getDateLastDay();
			
			//新增供应商个数
			mapParams=new HashMap<String, Object>();
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			int ySuppCount=this.iSupplierInfoBiz.countSupplierInfoListAudit(mapParams, "");
			this.getRequest().setAttribute("ySuppCount", ySuppCount);			

			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			int mSuppCount=this.iSupplierInfoBiz.countSupplierInfoListAudit(mapParams, "");
			this.getRequest().setAttribute("mSuppCount", mSuppCount);
			
			//年度成交供应商数
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			int yBidSuppCount=this.iBidAwardBiz.countBidAwardSupplier(mapParams);
			this.getRequest().setAttribute("yBidSuppCount", yBidSuppCount);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			int mBidSuppCount=this.iBidAwardBiz.countBidAwardSupplier(mapParams);
			this.getRequest().setAttribute("mBidSuppCount", mBidSuppCount);
			
			//新增订单总金额
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			double yOrderSum=this.iOrderInfoBiz.sumOrderInfoMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("yOrderSum", yOrderSum);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			double mOrderSum=this.iOrderInfoBiz.sumOrderInfoMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("mOrderSum", mOrderSum);
			
			//新增合同总金额
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			double yContractSum=this.iContractInfoBiz.sumContractInfoMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("yContractSum", yContractSum);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			double mContractSum=this.iContractInfoBiz.sumContractInfoMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("mContractSum", mContractSum);
			
			//年度终止采购量
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			int yBidAbnomalCount=this.iBidAbnomalBiz.countBidAbnomal(mapParams,sqlStr);
			this.getRequest().setAttribute("yBidAbnomalCount", yBidAbnomalCount);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			int mBidAbnomalCount=this.iBidAbnomalBiz.countBidAbnomal(mapParams,sqlStr);
			this.getRequest().setAttribute("mBidAbnomalCount", mBidAbnomalCount);
			
			//年度采购总额
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			double yPurchaseMoneySum=this.iBidAwardBiz.sumPurchaseMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("yPurchaseMoneySum", yPurchaseMoneySum);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			double mPurchaseMoneySum=this.iBidAwardBiz.sumPurchaseMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("mPurchaseMoneySum", mPurchaseMoneySum);
			
			//年度采购量
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			int yPurchaseCount=this.iBidAwardBiz.countPurchaseNumber(mapParams,sqlStr);
			this.getRequest().setAttribute("yPurchaseCount", yPurchaseCount);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			int mPurchaseCount=this.iBidAwardBiz.countPurchaseNumber(mapParams,sqlStr);
			this.getRequest().setAttribute("mPurchaseCount", mPurchaseCount);
			
			//年度节资总额
			sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
			"ba");
			mapParams.put("startDate", yearStart);
			mapParams.put("endDate", yearEnd);
			double yEconomyMoneySum=this.iBidAwardBiz.sumEconomyMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("yEconomyMoneySum", yEconomyMoneySum);	
			
			mapParams.put("startDate", dateStart);
			mapParams.put("endDate", dateEnd);
			double mEconomyMoneySum=this.iBidAwardBiz.sumEconomyMoney(mapParams,sqlStr);
			this.getRequest().setAttribute("mEconomyMoneySum", mEconomyMoneySum);
			
			//采购金额对比分析
			//横坐标、采购金额
			sqlStr = "";
			if (!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) {
				sqlStr = UserRightInfoUtil.getUserDepartNameSql(getRequest(),
						"t");
			}
			mapParams = new HashMap<String, Object>();
			mapParams.put("year", DateUtil.getCurrentYear());
			String xAxisData="",amountMoney="";
			List<PurchaseAmount> list1 =null;
			Map<String, PurchaseAmount> mapAmount=null;
			list1=this.iPurchaseAmountBiz.getPurchaseAmountBuyMonth(mapParams,sqlStr);
			    mapAmount=ListToMap.list2Map3(list1,"getMonth",PurchaseAmount.class);  
			    xAxisData="'1月',";
			    if(mapAmount.get("01")!=null){
			    	purchaseAmount=mapAmount.get("01");
			    	amountMoney=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'2月',";
			    if(mapAmount.get("02")!=null){
			    	purchaseAmount=mapAmount.get("02");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'3月',";
			    if(mapAmount.get("03")!=null){
			    	purchaseAmount=mapAmount.get("03");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'4月',";
			    if(mapAmount.get("04")!=null){
			    	purchaseAmount=mapAmount.get("04");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'5月',";
			    if(mapAmount.get("05")!=null){
			    	purchaseAmount=mapAmount.get("05");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'6月',";
			    if(mapAmount.get("06")!=null){
			    	purchaseAmount=mapAmount.get("06");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'7月',";
			    if(mapAmount.get("07")!=null){
			    	purchaseAmount=mapAmount.get("07");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'8月',";
			    if(mapAmount.get("08")!=null){
			    	purchaseAmount=mapAmount.get("08");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'9月',";
			    if(mapAmount.get("09")!=null){
			    	purchaseAmount=mapAmount.get("09");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'10月',";
			    if(mapAmount.get("10")!=null){
			    	purchaseAmount=mapAmount.get("10");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'11月',";
			    if(mapAmount.get("11")!=null){
			    	purchaseAmount=mapAmount.get("11");
			    	amountMoney+=purchaseAmount.getAmountMoney()+",";
			    }else{
			    	amountMoney+="0,";
			    }
			    xAxisData+="'12月'";
			    if(mapAmount.get("12")!=null){
			    	purchaseAmount=mapAmount.get("12");
			    	amountMoney+=purchaseAmount.getAmountMoney();
			    }else{
			    	amountMoney+="0";
			    }
	            this.getRequest().setAttribute("xAxisData", xAxisData);
                this.getRequest().setAttribute("amountMoney", amountMoney);
            
            
			//采购方式对比分析
			sqlStr = UserRightInfoUtil.getUserDepartNameSql(
					getRequest(), "ba");
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) sqlStr+=" and ba.com_id="+comId;
			List<BidAward> list=this.iBidAwardBiz.getPurchseWayList(sqlStr);
			StringBuffer sb=new StringBuffer();
			Map<String,String> map = TableStatusMap.purchaseWay;
			int i=0;
			for(BidAward bidAward:list){
				if(i==0){
					sb.append("{value:"+bidAward.getSumPrice()+", name:'"+(String) map.get(bidAward
							.getBuyWay())+"'}");
				}else{
					sb.append(",{value:"+bidAward.getSumPrice()+", name:'"+(String) map.get(bidAward
							.getBuyWay())+"'}");
				}
				i++;
			}

			this.getRequest().setAttribute("data", sb.toString());
		} catch (Exception e) {
			log.error("查看首页信息列表错误！", e);
			throw new BaseException("查看首页信息列表错误！", e);
		}
		return viewUrl;
		
	}
	/* =====================================================Ushine 修改开始 =======================================================*/
	/**
	 * 首页待办查看更多
	 * Ushine 2016-11-19
	 * @return
	 * @throws BaseException
	 */
	public String viewWaitForDeal() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看待办信息列表-分页显示错误！", e);
			throw new BaseException("查看待办信息列表-分页显示错误！", e);
		}
		
		return "view_wfd" ;
	}
	/**
	 * 触屏版   我的待办
	 * Ushine 2017-9-5
	 * @return
	 * @throws BaseException
	 */
	public String viewMobileWaitForDeal() throws BaseException {
		
		try{
			String wfdName=this.getRequest().getParameter("wfdName");
			String wfType=this.getRequest().getParameter("wfType");
			this.getRequest().setAttribute("wfdName", wfdName);
			this.getRequest().setAttribute("wfType", wfType);
		} catch (Exception e) {
			log.error("触屏版   我的待办显示错误！", e);
			throw new BaseException("触屏版   我的待办显示错误！", e);
		}
		
		return "view_mobile_wfd" ;
	}
	/**
	 * 触屏版   我的已办
	 * Ushine 2017-9-5
	 * @return
	 * @throws BaseException
	 */
	public String viewMobileDoneForDeal() throws BaseException {
		
		try{
			String wfdName=this.getRequest().getParameter("wfdName");
			String wfType=this.getRequest().getParameter("wfType");
			this.getRequest().setAttribute("wfdName", wfdName);
			this.getRequest().setAttribute("wfType", wfType);
		} catch (Exception e) {
			log.error("触屏版   我的已办显示错误！", e);
			throw new BaseException("触屏版   我的已办显示错误！", e);
		}
		
		return "view_mobile_done" ;
	}
	/**
	 * 首页待办查看更多
	 * Ushine 2016-11-19
	 * @return
	 * @throws BaseException
	 */
	public String findWaitForDeal() throws BaseException {
		
		try{
			//查询条件的传递
			String wfdName=this.getRequest().getParameter("wfdName");
			String wfType=this.getRequest().getParameter("wfType");
			String createTimeStart=this.getRequest().getParameter("createTimeStart");
			String createTimeEnd=this.getRequest().getParameter("createTimeEnd");
			String user ="";
			if(!UserRightInfoUtil.ifSystemManagerRole(getRequest())){
				user = UserRightInfoUtil.getUserName(getRequest());
			}
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("wfdName", wfdName);
			map.put("wfType", wfType);
			map.put("createTimeStart", createTimeStart);
			map.put("createTimeEnd", createTimeEnd);
			map.put("user", user);
			List<WaitWorkItem> list = this.iWaitForDealBiz.getWaitForDealList(this.getRollPageDataTables(),map) ;
			if(list!=null&&list.size()>0){
				for(WaitWorkItem waitWorkItem:list){
					waitWorkItem.setCreatorCn(BaseDataInfosUtil.convertLoginNameToChnName(waitWorkItem.getCreator()));
					waitWorkItem.setLastUpdatorCn(BaseDataInfosUtil.convertLoginNameToChnName(waitWorkItem.getLastUpdator()));
					//设置供应商考核URL
					if("供应商考核".equals(waitWorkItem.getProcessName())){
						//获取考核信息
						String orderNo=waitWorkItem.getOrderNo().replace(WorkFlowStatus.SE_WorkFlow_Type, "");
						supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(Long.parseLong(orderNo));
						if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date timeEnd = sdf.parse(sdf.format(supplierEvaluations.getTimeEnd()));
						Date nowTime = sdf.parse(sdf.format(new Date()));
						if(nowTime.getTime()>timeEnd.getTime()){
							Map<String, Object> args = new HashMap<String, Object>();
							args.put("result", "-1");
							args.put("signContent", "时间超时未处理");
							facets.executeAndJumpReback(waitWorkItem.getTaskId(),waitWorkItem.getOrderId(), UserRightInfoUtil.getUserName(getRequest()), args, null);
						}
						}
						//获取考核人信息
						supplierEvaluateUser = new SupplierEvaluateUser();
						supplierEvaluateUser.setSeId(Long.parseLong(orderNo));
						supplierEvaluateUser.setUserId(UserRightInfoUtil.getUserId(getRequest()));
						List<SupplierEvaluateUser> seuList = this.iEvaluateSupplierRelationBiz.getSupplierEvaluateUserList(supplierEvaluateUser);
						if(seuList!=null&&seuList.size()>0){
							waitWorkItem.setActionUrl(waitWorkItem.getActionUrl()+"?supplierEvaluations.seId="+orderNo+"&supplierEvaluateUser.seuId="+seuList.get(0).getSeuId()+"&taskId="+waitWorkItem.getTaskId()+"&processId="+waitWorkItem.getProcessId());
							}
					}else{
							if(waitWorkItem.getOrderState().equals(WorkFlowStatus.WORK_FLOW_ORDER_STATUS_3)&&waitWorkItem.getCreator().equals(user)){
								waitWorkItem.setTaskType(Long.parseLong("1"));
								waitWorkItem.setActionUrl(waitWorkItem.getActionUrl()+"?taskId="+waitWorkItem.getTaskId()+"&processId="+waitWorkItem.getProcessId()+"&detail=detail&back=back");
							}else{
								waitWorkItem.setActionUrl(waitWorkItem.getActionUrl()+"?taskId="+waitWorkItem.getTaskId()+"&processId="+waitWorkItem.getProcessId());
							}
					}
					
				}
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看待办信息列表-分页显示错误！", e);
			throw new BaseException("查看待办信息列表-分页显示错误！", e);
		}
		
		return null ;
	}
	/**
	 * 触屏版 待办获取总条数
	 * @return
	 * @throws BaseException
	 */
	public String viewCountWaitForDeal() throws BaseException {
		
		try{
			String user ="";
			
			if(!UserRightInfoUtil.ifSystemManagerRole(getRequest())){
				user = UserRightInfoUtil.getUserName(getRequest());
			}
			Map map=new HashMap();
			map.put("user", user);
			int count = this.iWaitForDealBiz.getWaitForDealSize(map) ;
			
			JsonConfig jsonConfig = new JsonConfig(); 
			PrintWriter out = this.getResponse().getWriter();
			JSONObject json = new JSONObject();
			jsonMap.put("count", count);
			json.putAll(jsonMap, jsonConfig);
			out.print(json.toString());
			
		} catch (Exception e) {
			log.error("触屏版待办获取总条数错误！", e);
			throw new BaseException("触屏版待办获取总条数错误！", e);
		}
		
		return null;
	}
	/**
	 * 首页已办查看更多
	 * Ushine 2016-11-19
	 * @return
	 * @throws BaseException
	 */
	public String viewAlreadyDone() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看标准信息列表-分页显示错误！", e);
			throw new BaseException("查看标准信息列表-分页显示错误！", e);
		}
		
		return "view_ad" ;
	}
	/**
	 * 首页已办查看更多
	 * Ushine 2016-11-19
	 * @return
	 * @throws BaseException
	 */
	public String findAlreadyDone() throws BaseException {
		
		try{
			//查询条件的传递

			String wfdName=this.getRequest().getParameter("wfdName");
			String wfType=this.getRequest().getParameter("wfType");
			String createTimeStart=this.getRequest().getParameter("createTimeStart");
			String createTimeEnd=this.getRequest().getParameter("createTimeEnd");
			String user ="";
			if(!UserRightInfoUtil.ifSystemManagerRole(getRequest())){
				user = UserRightInfoUtil.getUserName(getRequest());
			}
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("wfdName", wfdName);
			map.put("wfType", wfType);
			map.put("createTimeStart", createTimeStart);
			map.put("createTimeEnd", createTimeEnd);
			map.put("user", user);
			List<WaitWorkItem> list = this.iWaitForDealBiz.getWaitAlreadyDoneList(this.getRollPageDataTables(),map) ;
			if(list!=null&&list.size()>0){
				for(WaitWorkItem waitWorkItem:list){
					waitWorkItem.setCreatorCn(BaseDataInfosUtil.convertLoginNameToChnName(waitWorkItem.getCreator()));
					waitWorkItem.setResult(WorkFlowStatusMap.getWorkflowResult((String)waitWorkItem.getTaskVariableMap().get("result")));
					waitWorkItem.setSignContent((String)waitWorkItem.getTaskVariableMap().get("signContent"));
					if("供应商考核".equals(waitWorkItem.getProcessName())){
						//获取考核信息
						String orderNo=waitWorkItem.getOrderNo().replace(WorkFlowStatus.SE_WorkFlow_Type, "");
						//获取考核信息
						supplierEvaluations = this.iEvaluateSupplierBiz.getSupplierEvaluations(Long.parseLong(orderNo));
						waitWorkItem.setTaskName(supplierEvaluations.getSeDescribe());
						waitWorkItem.setActionUrl(waitWorkItem.getActionUrl()+"?supplierEvaluations.seId="+orderNo+"&taskId="+waitWorkItem.getTaskId()+"&detail=detail");
					}else{
						waitWorkItem.setActionUrl(waitWorkItem.getActionUrl()+"?taskId="+waitWorkItem.getTaskId()+"&processId="+waitWorkItem.getProcessId()+"&detail=detail");
					}
				}
			}
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看标准信息列表-分页显示错误！", e);
			throw new BaseException("查看标准信息列表-分页显示错误！", e);
		}
		
		return null ;
	}
	
	
	/**
	 * 获取部门信息统计
	 * 
	 * @return
	 * @throws BaseException
	 * @throws IOException 
	 * @Action
	 */
	public String getDeptIdInfo() throws BaseException, IOException {
		PrintWriter out = this.getResponse().getWriter();
		
		try {
			out.print("");
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return null;

	}
	
	/**
	 * 查看消息信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewMessages() throws BaseException {

		try {
			
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return "messView";

	}
	/**
	 * 触屏版 查看消息信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String viewMobileMessages() throws BaseException {

		try {
			
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return "messMobileView";

	}

	/**
	 * 查看消息信息列表
	 * 
	 * @return
	 * @throws BaseException
	 * @Action
	 */
	public String findMessages() throws BaseException {

		try {
			if(messageLog==null)
			{
				messageLog=new SysMessageLog();
			}
			String messageContent=this.getRequest().getParameter("messageContent");
			messageLog.setMessageContent(messageContent);
			String sendPersonCn=this.getRequest().getParameter("sendPersonCn");
			messageLog.setSendPersonCn(sendPersonCn);
			String status=this.getRequest().getParameter("status");
			messageLog.setStatus(status);
			
			messageLog.setReceivePerson(UserRightInfoUtil.getUserName(this.getRequest()));
			
			List<SysMessageLog> president = this.iSysMessageLogBiz.getSysMessageLogList(this.getRollPageDataTables(),messageLog); 
			
			this.getPagejsonDataTablesByLongDate(president);
			
		} catch (Exception e) {
			log.error("查看消息信息列表错误！", e);
			throw new BaseException("查看消息信息列表错误！", e);
		}

		return null;

	}
	
	/**
	 * 消息显示次数和标题
	 * @return
	 * @throws BaseException
	 */
	public String viewMessageList() throws BaseException {
		
		try{
			if(messageLog==null)
			{
				messageLog=new SysMessageLog();
			}
			messageLog.setStatus("0");
			messageLog.setReceivePerson(UserRightInfoUtil.getUserName(this.getRequest()));
			 
			
			JsonConfig jsonConfig = new JsonConfig(); 
			List<SysMessageLog> messList = this.iSysMessageLogBiz.getSysMessageLogList(this.getRollPageDataTables(),messageLog);
			int count=this.iSysMessageLogBiz.getSysMessageLogCount(messageLog);
			PrintWriter out = this.getResponse().getWriter();
			JSONObject json = new JSONObject();
			jsonMap.put("count", count);
			jsonMap.put("list", messList);
			json.putAll(jsonMap, jsonConfig);
			out.print(json.toString());
			
		} catch (Exception e) {
			log.error("查看消息显示次数和标题-显示错误！", e);
			throw new BaseException("查看消息显示次数和标题-显示错误！", e);
		}
		
		return null;
	}
	/**
	 * 触屏版读取消息总条数
	 * @return
	 * @throws BaseException
	 */
	public String viewCountMessage() throws BaseException {
		
		try{
			if(messageLog==null)
			{
				messageLog=new SysMessageLog();
			}
			messageLog.setStatus("0");
			messageLog.setReceivePerson(UserRightInfoUtil.getUserName(this.getRequest()));
			 
			
			JsonConfig jsonConfig = new JsonConfig(); 
			int count=this.iSysMessageLogBiz.getSysMessageLogCount(messageLog);
			PrintWriter out = this.getResponse().getWriter();
			JSONObject json = new JSONObject();
			jsonMap.put("count", count);
			json.putAll(jsonMap, jsonConfig);
			out.print(json.toString());
			
		} catch (Exception e) {
			log.error("触屏版读取消息总条数错误！", e);
			throw new BaseException("触屏版读取消息总条数错误！", e);
		}
		
		return null;
	}
	/**
	 * 修改消息已读状态
	 * @return
	 * @throws BaseException
	 */
	public String updateMessage() throws BaseException {
		
		try{
			String messageId=this.getRequest().getParameter("messageId");
			messageLog=iSysMessageLogBiz.getSysMessageLog(new Long(messageId));
			messageLog.setLookDate(new Date());
			messageLog.setStatus("1");  //已读
			this.iSysMessageLogBiz.updateSysMessageLog(messageLog); 
			
			this.getRequest().setAttribute("message", "修改已读状态成功");
			this.getRequest().setAttribute("operModule", "修改公告已读状态信息");
			
		} catch (Exception e) {
			log.error("修改消息已读状态-显示错误！", e);
			throw new BaseException("修改消息已读状态-显示错误！", e);
		}
		
		return null;
	}
	
	
	
	/**
	 * 公告显示次数和标题
	 * @return
	 * @throws BaseException
	 */
	public String viewNoticeList() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(this.getRequest());		
			JsonConfig jsonConfig = new JsonConfig();
			notices=new Notices();
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) notices.setComId(comId);
			List<Notices> noticesList=new ArrayList<Notices>();
			List<Object[]> president = this.iNoticesBiz.getNoticesList(this.getRollPageDataTables(), notices); 
			for(Object[] obj:president){
				notices=(Notices)obj[0];
				notices.setCompName((String)obj[1]);
				noticesList.add(notices);
			}
			int count=this.iNoticesBiz.getNoticesCount(notices);
			PrintWriter out = this.getResponse().getWriter();
			JSONObject json = new JSONObject();
			jsonMap.put("count", count);
			jsonMap.put("list", noticesList);
			json.putAll(jsonMap, jsonConfig);
			out.print(json.toString());			
		} catch (Exception e) {
			log.error("查看公告显示次数和标题-显示错误！", e);
			throw new BaseException("查看公告显示次数和标题-显示错误！", e);
		}
		
		return null;
	}

	/**
	 * 首页 公告 查看更多
	 * Ushine 2016-11-19
	 * @return
	 * @throws BaseException
	 */
	public String viewNotice() throws BaseException {
		
		try{
			
		} catch (Exception e) {
			log.error("查看首页 公告 查看更多-分页显示错误！", e);
			throw new BaseException("查看首页 公告 查看更多-分页显示错误！", e);
		}
		
		return "view_notice" ;
	}
	
	/**
	 * 首页 公告 查看更多
	 * Ushine 2016-11-19
	 * @return
	 * @throws BaseException
	 */
	public String findNotice() throws BaseException {
		
		try{
			Long comId=UserRightInfoUtil.getComId(getRequest());
			if(notices==null)
			{
				notices=new Notices();
			}
			String title=this.getRequest().getParameter("title");
			notices.setTitle(title);
			String keyword=this.getRequest().getParameter("keyword");
			notices.setKeyword(keyword);
			if(!UserRightInfoUtil.ifSystemManagerRole(this.getRequest())) notices.setComId(comId);
			List<Notices> noticesList=new ArrayList<Notices>();
			List<Object[]> president = this.iNoticesBiz.getNoticesList(this.getRollPageDataTables(), notices); 
			for(Object[] obj:president){
				notices=(Notices)obj[0];
				notices.setCompName((String)obj[1]);
				noticesList.add(notices);
			}
			this.getPagejsonDataTables(noticesList);
		} catch (Exception e) {
			log.error("查看标准信息列表-分页显示错误！", e);
			throw new BaseException("查看标准信息列表-分页显示错误！", e);
		}
		
		return null;
	}

	/**
	 * 跳转到修改用户密码页面
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws BaseException
	 */
	public String updatePwdInit( ) throws BaseException {	
		try {
			
		} catch (Exception e) {
			log.error("修改用户密码错误！", e);
			throw new BaseException("修改用户密码错误！", e);
		}
		
		return  "updatePassWordInit"   ;
	}
	
	/**
	 * 修改用户密码信息
	 * @return
	 * @throws BaseException 
	 * @author fyh
	 */
	public String updatePassWord() throws Exception{
		try{
			String message="";
			String oldPassWord=this.getRequest().getParameter("oldPassWord");
			String newPassWord=this.getRequest().getParameter("newPassWord");
			Users u=this.iUsersBiz.getUsers(UserRightInfoUtil.getUserId(this.getRequest()));
			if(u!=null){
				if(!u.getUserPassword().equals(PasswordUtil.encrypt(oldPassWord))){
					message="原始密码错误,请重新输入";
				}else{
					u.setUserPassword(PasswordUtil.encrypt(newPassWord));
					this.iUsersBiz.updateUsers(u);
					message="修改成功";
				}
				this.getRequest().setAttribute("message", message);
			}
			
			
		} catch (Exception e) {
			log.error("修改用户密码信息！", e);
			throw new BaseException("修改用户密码信息！", e);
		}
		return "updatePassWordInit";
	}

	/* =====================================================Ushine 修改结束 =======================================================*/

	public INoticesBiz getiNoticesBiz() {
		return iNoticesBiz;
	}
	public void setiNoticesBiz(INoticesBiz iNoticesBiz) {
		this.iNoticesBiz = iNoticesBiz;
	}
	
	public Notices getNotices() {
		return notices;
	}
	
	public void setNotices(Notices notices) {
		this.notices = notices;
	}

	public Long getWfdOldSum() {
		return wfdOldSum;
	}

	public void setWfdOldSum(Long wfdOldSum) {
		this.wfdOldSum = wfdOldSum;
	}
	
	public Long getWfdNewSum() {
		return wfdNewSum;
	}

	public void setWfdNewSum(Long wfdNewSum) {
		this.wfdNewSum = wfdNewSum;
	}

	public ISysMessageLogBiz getiSysMessageLogBiz() {
		return iSysMessageLogBiz;
	}
	public void setiSysMessageLogBiz(ISysMessageLogBiz iSysMessageLogBiz) {
		this.iSysMessageLogBiz = iSysMessageLogBiz;
	}
	public void setiWaitForDealBiz(IWaitForDealBiz iWaitForDealBiz) {
		this.iWaitForDealBiz = iWaitForDealBiz;
	}
	public IWaitForDealBiz getiWaitForDealBiz() {
		return iWaitForDealBiz;
	}
	public IEvaluateSupplierBiz getiEvaluateSupplierBiz() {
		return iEvaluateSupplierBiz;
	}
	public void setiEvaluateSupplierBiz(IEvaluateSupplierBiz iEvaluateSupplierBiz) {
		this.iEvaluateSupplierBiz = iEvaluateSupplierBiz;
	}
	public IUsersBiz getiUsersBiz() {
		return iUsersBiz;
	}
	public void setiUsersBiz(IUsersBiz iUsersBiz) {
		this.iUsersBiz = iUsersBiz;
	}
	public SnakerEngineFacets getFacets() {
		return facets;
	}
	public void setFacets(SnakerEngineFacets facets) {
		this.facets = facets;
	}
	public IEvaluateSupplierRelationBiz getiEvaluateSupplierRelationBiz() {
		return iEvaluateSupplierRelationBiz;
	}
	public void setiEvaluateSupplierRelationBiz(
			IEvaluateSupplierRelationBiz iEvaluateSupplierRelationBiz) {
		this.iEvaluateSupplierRelationBiz = iEvaluateSupplierRelationBiz;
	}
	public IBidAwardBiz getiBidAwardBiz() {
		return iBidAwardBiz;
	}
	public void setiBidAwardBiz(IBidAwardBiz iBidAwardBiz) {
		this.iBidAwardBiz = iBidAwardBiz;
	}
	public IRequiredCollectBiz getiRequiredCollectBiz() {
		return iRequiredCollectBiz;
	}
	public void setiRequiredCollectBiz(IRequiredCollectBiz iRequiredCollectBiz) {
		this.iRequiredCollectBiz = iRequiredCollectBiz;
	}
	public ISupplierInfoBiz getiSupplierInfoBiz() {
		return iSupplierInfoBiz;
	}
	public void setiSupplierInfoBiz(ISupplierInfoBiz iSupplierInfoBiz) {
		this.iSupplierInfoBiz = iSupplierInfoBiz;
	}
	public IOrderInfoBiz getiOrderInfoBiz() {
		return iOrderInfoBiz;
	}
	public void setiOrderInfoBiz(IOrderInfoBiz iOrderInfoBiz) {
		this.iOrderInfoBiz = iOrderInfoBiz;
	}
	public IContractInfoBiz getiContractInfoBiz() {
		return iContractInfoBiz;
	}
	public void setiContractInfoBiz(IContractInfoBiz iContractInfoBiz) {
		this.iContractInfoBiz = iContractInfoBiz;
	}
	public IBidAbnomalBiz getiBidAbnomalBiz() {
		return iBidAbnomalBiz;
	}
	public void setiBidAbnomalBiz(IBidAbnomalBiz iBidAbnomalBiz) {
		this.iBidAbnomalBiz = iBidAbnomalBiz;
	}
	public IPurchaseAmountBiz getiPurchaseAmountBiz() {
		return iPurchaseAmountBiz;
	}
	public void setiPurchaseAmountBiz(IPurchaseAmountBiz iPurchaseAmountBiz) {
		this.iPurchaseAmountBiz = iPurchaseAmountBiz;
	}
	
	
}
