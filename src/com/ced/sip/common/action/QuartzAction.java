package com.ced.sip.common.action;



import org.quartz.Job;
import org.quartz.JobExecutionContext;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;

public class QuartzAction extends BaseAction implements Job{
	
	
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 定时任务执行方法
	 * @return
	 * @throws BaseException 
	 * @throws ClassNotFoundException 
	 * @Action
	 */
	public void execute(JobExecutionContext context) {
		
		try {
			
			System.out.println("定时任务执行开始"); 

			System.out.println("定时任务执行结束");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
}
