package com.ced.sip.${packageName}.action;

import java.io.PrintWriter;
import java.util.List;

import com.ced.base.action.BaseAction;
import com.ced.base.exception.BaseException;
import com.ced.sip.common.utils.StringUtil;
import com.ced.sip.${packageName}.biz.I${objectNameUpper}Biz;
import com.ced.sip.${packageName}.entity.${objectNameUpper};
/** 
 * 类名称：${objectNameUpper}Action
 * 创建人：luguanglei 
 * 创建时间：${nowDate?string("yyyy-MM-dd")}
 */
public class ${objectNameUpper}Action extends BaseAction {

	// ${objectComments} 
	private I${objectNameUpper}Biz i${objectNameUpper}Biz;
	
	// ${objectComments}
	private ${objectNameUpper} ${objectNameLower};
	
	/**
	 * 查看${objectComments}信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String view${objectNameUpper}() throws BaseException {
		
		try{
		
		} catch (Exception e) {
			log.error("查看${objectComments}信息列表错误！", e);
			throw new BaseException("查看${objectComments}信息列表错误！", e);
		}	
			
		return VIEW ;
				
	}
	
	/**
	 * 查看${objectComments}信息列表
	 * @return
	 * @throws BaseException 
	 * @Action
	 */
	public String find${objectNameUpper}() throws BaseException {
		
		try{
			List list=this.i${objectNameUpper}Biz.get${objectNameUpper}List(this.getRollPageDataTables(), ${objectNameLower});
			this.getPagejsonDataTables(list);
		} catch (Exception e) {
			log.error("查看${objectComments}信息列表错误！", e);
			throw new BaseException("查看${objectComments}信息列表错误！", e);
		}
		
		return null ;
		
	}
	
	/**
	 * 保存${objectComments}信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String save${objectNameUpper}Init() throws BaseException {
		try{
		
		} catch (Exception e) {
			log("保存${objectComments}信息初始化错误！", e);
			throw new BaseException("保存${objectComments}信息初始化错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 保存${objectComments}信息
	 * @return
	 * @throws BaseException 
	 */
	public String save${objectNameUpper}() throws BaseException {
		
		try{
			this.i${objectNameUpper}Biz.save${objectNameUpper}(${objectNameLower});			
			this.getRequest().setAttribute("message", "添加成功");
			this.getRequest().setAttribute("operModule", "新增${objectComments}");
		} catch (Exception e) {
			log("保存${objectComments}信息错误！", e);
			throw new BaseException("保存${objectComments}信息错误！", e);
		}
		
		return ADD_INIT;
		
	}
	
	/**
	 * 修改${objectComments}信息初始化
	 * @return
	 * @throws BaseException 
	 */
	public String update${objectNameUpper}Init() throws BaseException {
		
		try{
			${objectNameLower}=this.i${objectNameUpper}Biz.get${objectNameUpper}(${objectNameLower}.get${idUp}() );
		} catch (Exception e) {
			log("修改${objectComments}信息初始化错误！", e);
			throw new BaseException("修改${objectComments}信息初始化错误！", e);
		}
		return MODIFY_INIT;
		
	}
	
	/**
	 * 修改${objectComments}信息
	 * @return
	 * @throws BaseException 
	 */
	public String update${objectNameUpper}() throws BaseException {
		
		try{
			this.i${objectNameUpper}Biz.update${objectNameUpper}(${objectNameLower});			
			this.getRequest().setAttribute("message", "修改成功");
			this.getRequest().setAttribute("operModule", "修改${objectComments}");
		} catch (Exception e) {
			log("修改${objectComments}信息错误！", e);
			throw new BaseException("修改${objectComments}信息错误！", e);
		}
		return ADD_INIT;
		
	}
	
	/**
	 * 删除${objectComments}信息
	 * @return
	 * @throws BaseException 
	 */
	public String delete${objectNameUpper}() throws BaseException {
		try{
		    ${objectNameUpper} ${objectNameLower}=new ${objectNameUpper}();
			String ids=this.getRequest().getParameter("ids");
 			String[] str=ids.split(",");
 			for(int i=0;i<str.length;i++){
 				if(StringUtil.isNotBlank(str[i]))
 				{
	 				${objectNameLower}=this.i${objectNameUpper}Biz.get${objectNameUpper}(new Long(str[i]));
	 				this.i${objectNameUpper}Biz.delete${objectNameUpper}(${objectNameLower});
 				}
 			}
 			this.getRequest().setAttribute("message", "删除成功");
			this.getRequest().setAttribute("operModule", "删除${objectComments}");
			PrintWriter out = this.getResponse().getWriter();			
			out.println("删除成功");
		} catch (Exception e) {
			log("删除${objectComments}信息错误！", e);
			throw new BaseException("删除${objectComments}信息错误！", e);
		}
		return null;
		
	}
	
	/**
	 * 查看${objectComments}明细信息
	 * @return
	 * @throws BaseException 
	 */
	public String view${objectNameUpper}Detail() throws BaseException {
		
		try{
			${objectNameLower}=this.i${objectNameUpper}Biz.get${objectNameUpper}(${objectNameLower}.get${idUp}() );
		} catch (Exception e) {
			log("查看${objectComments}明细信息错误！", e);
			throw new BaseException("查看${objectComments}明细信息错误！", e);
		}
		return DETAIL;
		
	}

	public I${objectNameUpper}Biz geti${objectNameUpper}Biz() {
		return i${objectNameUpper}Biz;
	}

	public void seti${objectNameUpper}Biz(I${objectNameUpper}Biz i${objectNameUpper}Biz) {
		this.i${objectNameUpper}Biz = i${objectNameUpper}Biz;
	}

	public ${objectNameUpper} get${objectNameUpper}() {
		return ${objectNameLower};
	}

	public void set${objectNameUpper}(${objectNameUpper} ${objectNameLower}) {
		this.${objectNameLower} = ${objectNameLower};
	}
	
}
