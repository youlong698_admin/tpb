<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD 3.0//EN"
"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">
<!-- 
       类名称：${objectNameUpper}.hbm.xml
       创建人：luguanglei 
       创建时间：${nowDate?string("yyyy-MM-dd")}
-->
<hibernate-mapping>
    <class name="com.ced.sip.${packageName}.entity.${objectNameUpper}" table="${objectName}">
        <comment>${objectName}(${objectComments})</comment>
        <#list fieldList as var>
           <#if var_index==0>
	    <id name="${var[6]}" type="java.lang.Long">
	        <column name="${var[0]}" precision="${var[3]}"  scale="${var[4]}" />
	        <generator class="sequence" >
	        <param name="sequence">SEQ_${objectName}_ID</param>
	        </generator>
	    </id>
		    </#if>
		    <#if var_index!=0>
			    <#if var[1] == 'VARCHAR2'>
		<property name="${var[6]}" type="java.lang.String">
          <column name="${var[0]}" length="${var[2]}">
          </column>
        </property>	 
				 </#if>
				 <#if var[1] == 'NUMBER'&& var[4] =='0'>
		<property name="${var[6]}" type="java.lang.Long">
          <column name="${var[0]}" length="${var[2]}" precision="${var[3]}">
          </column>
         </property>	 	    
				 </#if>
				 <#if var[1] == 'NUMBER'&& var[4] !='0'>
		<property name="${var[6]}" type="java.lang.Double">
          <column name="${var[0]}" length="${var[2]}" precision="${var[3]}"  scale="${var[4]}">
        </column>	
        </property>
				 </#if>	   
				 <#if var[1] == 'DATE'>
		<property name="${var[6]}" type="java.util.Date">
          <column name="${var[0]}" length="${var[2]}">
        </column>
        </property>
				 </#if>
		    </#if>
        </#list>

   </class>
</hibernate-mapping>