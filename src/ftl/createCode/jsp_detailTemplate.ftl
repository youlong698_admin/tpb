<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看${objectComments}</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>

<script type="text/javascript">
   var api = frameElement.api, W = api.opener;	
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="" >

<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">${objectComments}明细</td>
		    </tr>
	   <#list fieldList as var>
          <#if var[8]=='是'>
            <#if var[1] == 'VARCHAR2'>
             <#if var[9]=='1'>
            <tr>
			    <td  width="30%"  class="Content_tab_style1">${var[5]}：</td>
			    <td width="70%" class="Content_tab_style2">{${objectNameLower}.${var[6]}}</td>
			</tr>
			 <#elseif var[9]=='2'>
			 <tr>
			    <td width="30%"  class="Content_tab_style1">${var[5]}：</td>
			    <td width="70%" class="Content_tab_style2">{${objectNameLower}.${var[6]}}</td>
			</tr>
			<#else>
			 <tr>
			    <td width="30%"  class="Content_tab_style1">${var[5]}：</td>
			    <td width="70%" class="Content_tab_style2">
			     <c:if test="{${objectNameLower}.${var[6]}=='0' }">有效</c:if>
			     <c:if test="{${objectNameLower}.${var[6]}=='1' }">无效</c:if>
			    </td>
			</tr>
			</#if>
           </#if>
           <#if var[1] == 'NUMBER'>
            <tr>
			    <td  width="30%"  class="Content_tab_style1">${var[5]}：</td>
			    <td width="70%" class="Content_tab_style2">{${objectNameLower}.${var[6]}}</td>
			</tr>
           </#if>
           <#if var[1] == 'DATE'>
            <tr>
			    <td  width="30%"  class="Content_tab_style1">${var[5]}：</td>
			    <td width="70%" class="Content_tab_style2"><fmt:formatDate value="{${objectNameLower}.${var[6]}}" pattern="yyyy-MM-dd" /></td>
			</tr>
           </#if>
          </#if>
         </#list>
		</table>
</div>
</div>
</form>
</body>
</html> 	