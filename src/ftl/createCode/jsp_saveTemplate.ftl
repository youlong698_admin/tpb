<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>新增${objectComments}</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="{message!=null}">
		  window.onload=function(){ 
		    showMsg('success','{message}');
		    W.doQuery();
		    api.close();
		  	}
		</c:if>
	    
	});
	
	</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="save${objectNameUpper}_${objectNameLower}.action" >
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">新增${objectComments}</td>
		    </tr>
	   <#list fieldList as var>
          <#if var[8]=='是'>
            <#if var[1] == 'VARCHAR2'>
            <#if var[9]=='1'>
            <tr>
			    <td class="Content_tab_style1">${var[5]}：</td>
			    <td class="Content_tab_style2"><input  datatype="*" nullmsg="${var[5]}不能为空！" name="${objectNameLower}.${var[6]}" type="text" id="${var[6]}" value=""><font color="#ff0000">*</font></td>
			</tr>
			 <#elseif var[9]=='2'>
			 <tr>
			    <td class="Content_tab_style1">${var[5]}：</td>
			    <td class="Content_tab_style2"><textarea   name="${objectNameLower}.${var[6]}" id="${var[6]}" cols="28" class="Content_input_style2"></textarea></td>
			</tr>
			<#else>
			 <tr>
			    <td class="Content_tab_style1">${var[5]}：</td>
			    <td class="Content_tab_style2">
			     <input type="radio" name="${objectNameLower}.${var[6]}" id="${var[6]}" value="0" checked="checked"/>有效&nbsp;&nbsp;
				 <input type="radio" name="${objectNameLower}.${var[6]}" id="${var[6]}" value="1" />无效<br />
			    </td>
			</tr>
			</#if>
           </#if>
           <#if var[1] == 'NUMBER'>
            <tr>
			    <td class="Content_tab_style1">${var[5]}：</td>
			    <td class="Content_tab_style2"><input  datatype="*" nullmsg="${var[5]}不能为空！" name="${objectNameLower}.${var[6]}" type="text" id="${var[6]}" value=""><font color="#ff0000">*</font></td>
			</tr>
           </#if>
           <#if var[1] == 'DATE'>
            <tr>
			    <td class="Content_tab_style1">${var[5]}：</td>
			    <td class="Content_tab_style2"><input class="Wdate" datatype="*" nullmsg="${var[5]}不能为空！" name="${objectNameLower}.${var[6]}" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});" type="text" id="${var[6]}" value=""><font color="#ff0000">*</font></td>
			</tr>
           </#if>
          </#if>          
         </#list>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	