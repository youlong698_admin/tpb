package com.ced.sip.${packageName}.biz;

import java.util.List;

import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.${packageName}.entity.${objectNameUpper};
/** 
 * 类名称：I${objectNameUpper}Biz
 * 创建人：luguanglei 
 * 创建时间：${nowDate?string("yyyy-MM-dd")}
 */
public interface I${objectNameUpper}Biz {

	/**
	 * 根据主键获得${objectComments}表实例
	 * @param id 主键
	 * @return
	 * @throws BaseException 
	 */
	abstract ${objectNameUpper} get${objectNameUpper}(Long id) throws BaseException;

	/**
	 * 添加${objectComments}信息
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @throws BaseException 
	 */
	abstract void save${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException;

	/**
	 * 更新${objectComments}表实例
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @throws BaseException 
	 */
	abstract void update${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException;

	/**
	 * 删除${objectComments}表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void delete${objectNameUpper}(String id) throws BaseException;

	/**
	 * 删除${objectComments}表实例
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @throws BaseException 
	 */
	abstract void delete${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException;

	/**
	 * 删除${objectComments}表实例
	 * @param id 主键数组
	 * @throws BaseException 
	 */
	abstract void delete${objectNameUpper}s(String[] id) throws BaseException;

	/**
	 * 获得${objectComments}表数据集
	 * ${objectNameLower} ${objectComments}表实例
	 * @return
	 * @throws BaseException 
	 */
	abstract ${objectNameUpper} get${objectNameUpper}By${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException ;
	
	/**
	 * 获得所有${objectComments}表数据集
	 * @param ${objectNameLower} 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List get${objectNameUpper}List(${objectNameUpper} ${objectNameLower}) throws BaseException ;
	
	/**
	 * 获得所有${objectComments}表数据集
	 * @param rollPage 分页对象
	 * @param ${objectNameLower} 查询参数对象
	 * @return
	 * @throws BaseException 
	 */
	abstract List get${objectNameUpper}List(RollPage rollPage, ${objectNameUpper} ${objectNameLower})
			throws BaseException;

}