package com.ced.sip.${packageName}.entity;

/** 
 * 类名称：${objectNameUpper}
 * 创建人：luguanglei 
 * 创建时间：${nowDate?string("yyyy-MM-dd")}
 */
public class ${objectNameUpper} implements java.io.Serializable {

	// 属性信息
<#list fieldList as var>
   <#if var[1] == 'VARCHAR2'>
	private String ${var[6]};	 //${var[5]}
   </#if>
   <#if var[1] == 'NUMBER'&& var[4] =='0'>
	private Long ${var[6]};     //${var[5]}
   </#if>
   <#if var[1] == 'NUMBER'&& var[4] !='0'>
	private Double ${var[6]};	//${var[5]}
   </#if>	   
   <#if var[1] == 'DATE'>
	private Date ${var[6]};    //${var[5]}
   </#if>
</#list>
	
	
	public ${objectNameUpper}() {
		super();
	}
	
<#list fieldList as var>
   <#if var[1] == 'VARCHAR2'>
	public String get${var[7]}(){
	   return  ${var[6]};
	} 
	public void set${var[7]}(String ${var[6]}) {
	   this.${var[6]} = ${var[6]};
    }
   </#if>
   <#if var[1] == 'NUMBER'&& var[4] =='0'>
	public Long get${var[7]}(){
	   return  ${var[6]};
	} 
	public void set${var[7]}(Long ${var[6]}) {
	   this.${var[6]} = ${var[6]};
    }     
   </#if>
   <#if var[1] == 'NUMBER'&& var[4] !='0'>
	public Double get${var[7]}(){
	   return  ${var[6]};
	} 
	public void set${var[7]}(Double ${var[6]}) {
	   this.${var[6]} = ${var[6]};
    }	
   </#if>	   
   <#if var[1] == 'DATE'>
	public Date get${var[7]}(){
	   return  ${var[6]};
	} 
	public void set${var[7]}(Date ${var[6]}) {
	   this.${var[6]} = ${var[6]};
    }	    
   </#if>
</#list> 
}