package com.ced.sip.${packageName}.biz.impl;

import java.util.List;

import com.ced.base.biz.impl.BaseBizImpl;
import com.ced.base.exception.BaseException;
import com.ced.base.utils.RollPage;
import com.ced.sip.${packageName}.biz.I${objectNameUpper}Biz;
import com.ced.sip.${packageName}.entity.${objectNameUpper};
/** 
 * 类名称：${objectNameUpper}BizImpl
 * 创建人：luguanglei 
 * 创建时间：${nowDate?string("yyyy-MM-dd")}
 */
public class ${objectNameUpper}BizImpl extends BaseBizImpl implements I${objectNameUpper}Biz  {
	
	/**
	 * 根据主键获得${objectComments}表实例
	 * @param id 主键
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @return
	 * @throws BaseException 
	 */
	public ${objectNameUpper} get${objectNameUpper}(Long id) throws BaseException {
		return (${objectNameUpper})this.getObject(${objectNameUpper}.class, id);
	}
	
	/**
	 * 获得${objectComments}表实例
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @return
	 * @throws BaseException 
	 */
	public ${objectNameUpper} get${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException {
		return (${objectNameUpper})this.getObject(${objectNameUpper}.class, ${objectNameLower}.get${idUp}() );
	}
	
	/**
	 * 添加${objectComments}信息
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @throws BaseException 
	 */
	public void save${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException{
		this.saveObject( ${objectNameLower} ) ;
	}
	
	/**
	 * 更新${objectComments}表实例
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @throws BaseException 
	 */
	public void update${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException{
		this.updateObject( ${objectNameLower} ) ;
	}
	
	/**
	 * 删除${objectComments}表实例
	 * @param id 主键数组
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @throws BaseException 
	 */
	public void delete${objectNameUpper}(String id) throws BaseException {
		this.removeObject( this.get${objectNameUpper}( new Long(id) ) ) ;
	}
	
	/**
	 * 删除${objectComments}表实例
	 * @param ${objectNameLower} ${objectComments}表实例
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @throws BaseException 
	 */
	public void delete${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException {
		this.removeObject( ${objectNameLower} ) ;
	}
	
	/**
	 * 删除${objectComments}表实例
	 * @param id 主键数组
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @throws BaseException 
	 */
	public void delete${objectNameUpper}s(String[] id) throws BaseException {
		this.removeBatchObject(${objectNameUpper}.class, id) ;
	}
	
	/**
	 * 获得${objectComments}表数据集
	 * ${objectNameLower} ${objectComments}表实例
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @return
	 * @throws BaseException 
	 */
	public ${objectNameUpper} get${objectNameUpper}By${objectNameUpper}(${objectNameUpper} ${objectNameLower}) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ${objectNameUpper} de where 1 = 1 " );

		hql.append(" order by de.${id} desc ");
		List list = this.getObjects( hql.toString() );
		${objectNameLower} = new ${objectNameUpper}();
		if(list!=null&&list.size()>0){
			${objectNameLower} = (${objectNameUpper})list.get(0);
		}
		return ${objectNameLower};
	}
	
	/**
	 * 获得所有${objectComments}表数据集
	 * @param ${objectNameLower} 查询参数对象
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @return
	 * @throws BaseException 
	 */
	public List get${objectNameUpper}List(${objectNameUpper} ${objectNameLower}) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ${objectNameUpper} de where 1 = 1 " );

		hql.append(" order by de.${id} desc ");
		return this.getObjects( hql.toString() );
	}
	
	/**
	 * 获得所有${objectComments}表数据集
	 * @param rollPage 分页对象
	 * @param ${objectNameLower} 查询参数对象
	 * @author luguanglei ${nowDate?string("yyyy-MM-dd")}
	 * @return
	 * @throws BaseException 
	 */
	public List get${objectNameUpper}List(RollPage rollPage, ${objectNameUpper} ${objectNameLower}) throws BaseException {
		StringBuffer hql = new StringBuffer(" from ${objectNameUpper} de where 1 = 1 " );

		hql.append(" order by de.${id} desc ");
		return this.getObjects(rollPage, hql.toString() );
	}
	
}
