<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>合同发货管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findContractInfoApplication_contractInfoSupplier.action?sgin=3",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			showMsg("error","查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [            
            {	
            	className : "ellipsis",
				orderable : false,
            	render: function(data,type, row, meta) {					
					 return "<button class=\"btn btn-mini btn-danger\" type=\"button\"  onclick=\"doSend("+row.ciId+")\"><i class=\"icon-white icon-tags\"></i></button>";
                },         	
            	width : "30px"
            }
            ,{
            	className : "ellipsis",
            	data: "projectName",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "20%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractCode",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractName",
            	render: viewContractInfoDetail,
            	width : "15%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractPersonNameA",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%"
             }
            ,{
            	className : "numPrice",
            	data: "contractMoney",
            	render: CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE,
            	width : "8%"
             }
            ,{
            	className : "numPrice",
            	data: "contractAmount",
            	render: CONSTANT.DATA_TABLES.NUMBER,
            	width : "10%",
				orderable : false
             }
            ,{
            	className : "numPrice",
            	render: viewSendScale,
            	width : "8%",
				orderable : false
             }
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
            
        	 //给当前行某列加样式
        	$('td', row).eq(7).addClass("light-blue");
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.ciId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
            case 1:
				param.orderColumn = "de.projectName";
				break;
            case 2:
				param.orderColumn = "de.contractCode";
				break;
            case 3:
				param.orderColumn = "de.contractName";
				break;
            case 4:
				param.orderColumn = "de.PersonNameA";
				break;
            case 5:
				param.orderColumn = "de.contractMoney";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		
		//组装查询参数  ***按需修改****
		param.contractCode = $("#contractCode").val();
		param.contractName = $("#contractName").val();
		param.contractPersonNameA = $("#contractPersonNameA").val();
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	}    				 
		
   };
   
    //查看
    function viewContractInfoDetail(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看合同信息明细\",\"viewContractInfoDetail_contractInfoSupplier.action?contractInfo.ciId=" + row.ciId + "\",1)' title='"+data+"'>"+data+"</a>"; 
    }
    //发货比例
    function viewSendScale(data, type, row,   meta){
        return (row.realSendAmount/row.contractAmount*100).toFixed(2)  + '%' ; 
    }
    function doQuery(){
     _table.draw();
    }
    //订单发货
    function doSend(ciId){
       createdetailwindow("合同发货信息","saveSendContractInfoInit_contractInfoSupplier.action?contractInfo.ciId=" +ciId ,1);
    }
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
									    <button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button> 
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
					<input type="hidden" id="returnValues" name="returnValues"/>
					  <!--***按需修改****-->
					          合同编号：
						<input class="input-medium" placeholder="合同编号" type="text" id="contractCode" name="contractCode" value="" />
						合同名称：
						<input class="input-medium" placeholder="合同名称" type="text" id="contractName" name="contractName" value="" />
						采购单位名称：
						<input class="input-medium" placeholder="采购单位名称" type="text" id="contractPersonNameA" name="contractPersonNameA" value="" />
						<button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
						<button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										操作
									</th>
						            <th>项目名称</th>
						            <th>合同编号</th>
						            <th>合同名称</th>
						            <th>采购单位名称</th>
						            <th>订单金额</th>
						            <th>物资总数量</th>
						            <th>发货比例</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>