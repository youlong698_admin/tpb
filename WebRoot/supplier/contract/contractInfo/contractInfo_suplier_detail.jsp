<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看合同信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
</head>
<body >
<div class="Conter_Container" >
	
    <div class="Conter_main_conter">
   <div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
         
				<li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');"><b>合同基本信息</b></a></li>
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b>合同物资信息</b></a></li>
		        <li id="Tab3" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab3');"><b>供应商确认信息</b></a></li>
         </ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">      
    	<table class="table_ys1"  width="100%">		        	
            <tr>
			    <td width="15%" class="Content_tab_style1">项目名称：</td>
			    <td width="35%" class="Content_tab_style2">
			       ${contractInfo.projectName }</td>
			    <td width="15%" class="Content_tab_style1">项目编号：</td>
			    <td width="35%" class="Content_tab_style2">
			       ${contractInfo.bidCode }</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同编号：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractCode }
			    </td>
			    <td class="Content_tab_style1">合同名称：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractName }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同分类：</td>
			    <td class="Content_tab_style2">
				    ${contractInfo.contractType }
			    </td>
			    <td class="Content_tab_style1">项目分类：</td>
			    <td class="Content_tab_style2">
			      ${contractInfo.projectType }
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">甲方名称：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractPersonNameA }
			    </td>
			    <td class="Content_tab_style1">乙方名称：</td>
			    <td class="Content_tab_style2">
				   ${contractInfo.contractNameB }
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方地址：</td>
			    <td class="Content_tab_style2">
			        ${contractInfo.contractAddressA }
			    </td>
			    <td class="Content_tab_style1">乙方地址：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractAddressB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方联系人：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractUndertaker }
			    </td>
			    <td class="Content_tab_style1">乙方联系人：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractPersonB }
			    </td>
			</tr>
			 <tr>
			    <td class="Content_tab_style1">甲方联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractMobileA }
			    </td>
			    <td class="Content_tab_style1">乙方联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractMobileB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方电话：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractTelA }
			    </td>
			    <td class="Content_tab_style1">乙方电话：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractTelB }
			     </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方传真：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractFaxA }
			    </td>
			    <td class="Content_tab_style1">乙方传真：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractFaxB }
			    </td>
			    </tr>
            <tr>
			    <td class="Content_tab_style1">合同金额：</td>
			    <td class="Content_tab_style2">
			       <fmt:formatNumber value="${contractInfo.contractMoney }" pattern="#,##0.00"/>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        ${contractInfo.conMoneyType}
				</td>
			</tr>
             <tr>
                <td class="Content_tab_style1">框架协议：</td>
			    <td class="Content_tab_style2">
			      <c:choose>
			         <c:when test="${contractInfo.framework==0}">是</c:when>
			         <c:otherwise>否</c:otherwise>
			      </c:choose>
			    </td>
			    <td class="Content_tab_style1">合同审批原件：</td>
			    <td class="Content_tab_style2">
			      <c:if test="${not empty appFile}">
                          <c:out value="${appFile}" escapeXml="false"/>
			      </c:if>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3">${contractInfo.conRemark }</td>
			</tr>
		</table>
		 </div>
             	
      		<!--------------------合同物资明细--------------------------->
      		<div id="dTab2" class="HackBox">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" >
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>单价</th>
				<th width="100px" nowrap>小计</th>
				<th width="100px" nowrap>备注</th>
			</tr>
				<c:forEach items="${cmList}" var="contractMaterial" varStatus="status">
					<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>
						   ${status.index+1}
						</td>
						<td>
						    ${contractMaterial.materialCode}
						</td>
						<td>
						   ${contractMaterial.materialName} 
						</td>
						<td>
						   ${contractMaterial.materialType} 
						</td>
						<td>
						   ${contractMaterial.unit}
					    </td>
						<td align="right">
					     ${contractMaterial.amount} 
						 </td>
					    <td align="right">
					       <fmt:formatNumber value="${contractMaterial.price}" pattern="#,##0.00"/>
					    </td>
					    <td align="right">
					       <fmt:formatNumber value="${contractMaterial.price*contractMaterial.amount}" pattern="#,##0.00"/>
					    </td>
						<td>
						${contractMaterial.remark}
						</td>
					</tr>
				</c:forEach>
       	</table>
	</div>
	
  		<!--------------------供应商确认信息--------------------------->
  		<div id="dTab3" class="HackBox">
  		  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" >
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>确认时间</th>
				<th width="15%" nowrap>确认说明 </th>
				<th width="25%" nowrap>状态 </th>
			</tr>
				<c:forEach items="${confirmList}" var="contractConfirm" varStatus="status">
					<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>
						   ${status.index+1}
						</td>
						<td>
						    <fmt:formatDate value="${contractConfirm.confirmDate}" pattern="yyyy-MM-dd HH:mm"/>
						</td>
						<td>
						   ${contractConfirm.confirmRemark} 
						</td>
						<td>
						   ${contractConfirm.status} 
						</td>
					</tr>
				</c:forEach>
       	</table>
  		</div>
</div>
</div>
</div>
</div>
</body>
</html> 	