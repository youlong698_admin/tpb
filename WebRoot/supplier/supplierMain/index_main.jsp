<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>${sysName }</title>
		<%-- 页面清缓存BGN --%>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<link href="<%= path %>/style/main.css" rel="stylesheet" type="text/css" >
		<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
		<script type="text/javascript">
		   function goAllMessage(){
		    	var addurl = "viewMessages_supplierMain.action";
		  		createdetailwindow("更多消息", addurl, 1, 2);
		    }
		</script>	    
	</head>

	<body>
     <div class="main_content">
		<div class="mainindex">
        
    <div class="xline"></div>
        
    <div class="welinfo">
    <img src="<%=basePath %>/images/i06.png"/>
    <b>电子采购平台管理系统使用指南</b>
    </div>
    <c:if test="${supplierInfo.isRegister=='0'}">
    <ul class="infolist">
    <li><span>您可以快速进行项目报名操作操作</span><a href="javascript:window.location.href='viewRequiredCollectApplication_requiredCollectSupplier.action';" class="ibtn">项目报名</a></li>
    </ul>
    </c:if>
    <div class="xline"></div>
    
    <div class="welinfo">    
    <img src="<%=basePath %>/images/i09.png"/>
    <b>消息提醒：（<a href="javascript:goAllMessage();">更多请点这里</a>）</b></div>
    
     
    <ul class="infolist">
    <c:forEach items="${messageList}" var="message">
     <li><span>${message.messageContent }</span></li>
    </c:forEach>
    
    </ul>
    
    
    </div>
     
     </div>
</body>

</html>