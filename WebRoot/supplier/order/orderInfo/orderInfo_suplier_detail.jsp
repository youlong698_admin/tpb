<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看订单信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
</head>
<body >
<div class="Conter_Container" >
	
    <div class="Conter_main_conter">
   <div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
         
				<li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');"><b>订单基本信息</b></a></li>
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b>订单物资信息</b></a></li>
                <li id="Tab4" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab4');"><b>供应商确认信息</b></a></li>
         </ul>
            
            <div id="cnt">   
    		<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">      
    	<table class="table_ys1"  width="100%">	
    	    <tr>
			    <td width="15%" class="Content_tab_style1" id="td1">${td1 }：</td>
			    <td width="35%" class="Content_tab_style2">
			      ${orderInfo.projectContractName }
			    </td>
			    <td width="15%" class="Content_tab_style1" id="td3">${td2 }：</td>
			    <td width="35%" class="Content_tab_style2">
			       ${orderInfo.bidCode }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">订单编号：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderCode }
			    </td>
			    <td class="Content_tab_style1">订单名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderName }
			    </td>
			</tr>
          <tr>
			    <td class="Content_tab_style1">采购单位名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderPersonNameA }
			    </td>
			    <td class="Content_tab_style1">供应商名称：</td>
			    <td class="Content_tab_style2">
				    ${orderInfo.orderNameB }
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">收货地址：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.orderAddressA }
			    </td>
			    <td class="Content_tab_style1">供应商地址：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderAddressB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">收货联系人：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderUndertaker }
			    </td>
			    <td class="Content_tab_style1">供应商联系人：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderPersonB }
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">收货联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderMobileA }
			    </td>
			    <td class="Content_tab_style1">供应商联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderMobileB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位电话：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderTelA }
			    </td>
			    <td class="Content_tab_style1">供应商电话：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderTelB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位传真：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderFaxA }
			    </td>
			    <td class="Content_tab_style1">供应商传真：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderFaxB }
			    </td>
			    </tr>
            <tr>
			    <td class="Content_tab_style1">订单金额：</td>
			    <td class="Content_tab_style2">
			       <fmt:formatNumber value="${orderInfo.orderMoney }" pattern="#,##0.00"/>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.conMoneyType}
				</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3">${orderInfo.conRemark }</td>
			</tr>
		</table>
		 </div>
             	
      		<!--------------------订单物资明细--------------------------->
      		<div id="dTab2" class="HackBox">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" id="listtable">
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>单价</th>
				<th width="100px" nowrap>小计</th>
				<th width="100px" nowrap>备注</th>
			</tr>
			    <c:set var="totalPrice" value="0"/>
					  <c:forEach items="${omList}" var="orderMaterial" varStatus="status">
						    <c:set var="totalPrice" value="${totalPrice+orderMaterial.amount*orderMaterial.price }"/> 
							<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
								<td>
								  <c:if test="${empty orderInfo.baCiId}"><button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this,${status.index+1})'><i class="icon-white icon-trash"></i></button></c:if> ${status.index+1}
								</td>
								<td>
								    ${orderMaterial.materialCode}								    
								</td>
								<td>
								   ${orderMaterial.materialName}
								</td>
								<td>
								   ${orderMaterial.materialType}
								</td>
								<td>
								   ${orderMaterial.unit}
							    </td>
								<td align="right">
							     ${orderMaterial.amount}
								 </td>
							    <td align="right">
							       <fmt:formatNumber value="${orderMaterial.price}" pattern="#,##0.00"/>
							    </td>
							    <td align="right">
							       <fmt:formatNumber value="${orderMaterial.price*orderMaterial.amount}" pattern="#,##0.00"/>
							    </td>
								<td>
								    ${orderMaterial.remark}
								</td>
							</tr>
						</c:forEach>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总价：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalPriceStr"><fmt:formatNumber value="${totalPrice}" pattern="#,##0.00"/><!-- 小计 --></span>
					</td>
					<td>&nbsp;</td>
				</tr>	
       	</table>
	</div>
	<!--------------------供应商确认信息--------------------------->
  		<div id="dTab4" class="HackBox">
  		  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" >
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>确认时间</th>
				<th width="15%" nowrap>确认说明 </th>
				<th width="25%" nowrap>状态 </th>
			</tr>
				<c:forEach items="${confirmList}" var="orderConfirm" varStatus="status">
					<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>
						   ${status.index+1}
						</td>
						<td>
						    <fmt:formatDate value="${orderConfirm.confirmDate}" pattern="yyyy-MM-dd HH:mm"/>
						</td>
						<td>
						   ${orderConfirm.confirmRemark} 
						</td>
						<td>
						   ${orderConfirm.status} 
						</td>
					</tr>
				</c:forEach>
       	</table>
  		</div>
</div>
</div>
</div>
</div>
</body>
</html> 	