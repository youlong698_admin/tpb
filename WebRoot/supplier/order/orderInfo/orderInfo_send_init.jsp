<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>发货信息初始化页面</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%= path %>/common/script/context_form.js" type="text/javascript" ></script>
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
var api = frameElement.api, W = api.opener;
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="SendBill"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
var fileNameData=[];//已上传的文件名
var fileTypeData=[];//已上传的文件的格式
var attIdData=[];//已存入附件表的附件信息
   function submitConfirm(sign){
   $.dialog.confirm("温馨提示：您确定要提交吗？",function(){
      $("#status").val(sign);
      document.form.submit();
      },function(){},api)
   }
   function account(obj,waitAmount){
      var sendAmount=obj.value;
      var cz=FloatSub(sendAmount,waitAmount);
      if(cz>0){
          showMsgWeb("alert","温馨提示：本次发货的数量不能大于"+waitAmount+"！");
   	      document.getElementById(obj.id).value=waitAmount;
	      return false;
      }
      var oldValue=$("#oldValue").val();
      /*计算总物资数量*/
       var totalAmountStr=$("#totalAmountStr").text();
       var totalAmount=FloatSub(totalAmountStr,oldValue);
	   totalAmount=FloatAdd(totalAmount,sendAmount).toFixed(2);
	   document.getElementById("totalAmountStr").innerText =totalAmount;
   }
   function getOldValue(obj){
     document.getElementById("oldValue").value=obj.value;
   }
   function save(){
        var indexArr=document.getElementsByName("index");
	     var index,flag=true;
		 for(var i=0;i<indexArr.length;i++){
		  index=indexArr[i].value;
		   if($("#sendAmount_"+index).val()==""){
	           showMsgWeb("alert","温馨提示：第"+(i+1)+"行数量不能为空");
	           flag=false;
	           return false;
			}
	     }
	     if(flag){
	        //提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			document.form.submit();
		}
   }
   function doView(oiId){
      window.location.href="viewSendBill_sendBillSupplier.action?oicId="+oiId+"&type=1";
   }
</script>
</head>
<body >
<form name="form" id="form" method="post" action="saveSendOrderInfo_orderInfoSupplier.action">
<input type="hidden" name="sendBill.attIds" id="attIds" />
<input type="hidden" name="sendBill.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="sendBill.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="sendBill.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="sendBill.attIdData" id="attIdData" value=""/>
<div class="Conter_Container" >
	
  <div class="Conter_main_conter">     
    	<table class="table_ys1"  width="100%">	
            <tr>
			    <td class="Content_tab_style1">订单编号：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderCode }
			    </td>
			    <td class="Content_tab_style1">订单名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderName }
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">采购单位名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderPersonNameA }
			    </td>			   
			    <td class="Content_tab_style1">收货联系人：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderUndertaker }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位电话：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderTelA }
			    </td>
			    
			    <td class="Content_tab_style1">采购单位传真：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderFaxA }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">订单金额：</td>
			    <td class="Content_tab_style2">
			       <fmt:formatNumber value="${orderInfo.orderMoney }" pattern="#,##0.00"/>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.conMoneyType}
				</td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">
			    	发货日期：
			    </td>
			    <td class="Content_tab_style2">
			        <input type="text" id="sendDate" class="Wdate" name="sendBill.sendDate"  size="15" value="${today }" onClick="WdatePicker({ dateFmt:'yyyy-MM-dd', minDate:'%y-%M-%d %H:%m' })" />
			    </td>
			    <td class="Content_tab_style1">
			    	摘要：
			    </td>
			    <td class="Content_tab_style2">
			       <INPUT type="text" id="sendRemark" name="sendBill.sendRemark" maxlength="100" size="22" />
			       <INPUT type="hidden" id="supplierId" name="sendBill.supplierId" value="${orderInfo.supplierId }" />
			       <INPUT type="hidden" id="supplierName" name="sendBill.supplierName" value="${orderInfo.orderNameB }" />
			       <INPUT type="hidden" id="oiId" name="sendBill.oiId" value="${orderInfo.oiId }" />
			       <INPUT type="hidden" id="type" name="sendBill.type" value="1" />
			    </td>
	    		</tr>
	    		<tr>
					<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span>
					<br/><font color="#FF0000"> * 发货单复印件或发货凭证</font></td>
					<td class="Content_tab_style2" colspan="3">
						<!-- 附件存放 -->
						<div  id="fileDiv" class="panel"> 
						</div>
						<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
						<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
						
					</td>
				</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" id="listtable">
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>订单数量</th>
				<th width="55px" nowrap>已发货数量</th>
				<th width="100px" nowrap>本次发货数量</th>
				<th width="160px" nowrap>备注</th>
			</tr>
			    <c:set var="totalPrice" value="0"/>
					  <c:forEach items="${omList}" var="orderMaterial" varStatus="status">
						    <c:set var="totalAmount" value="${totalAmount+orderMaterial.waitSendAmount }"/> 
							<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
								<td>
								  ${status.index+1}
								  <input type="hidden" name="index" value="${status.index}"/>
								</td>
								<td>
								    ${orderMaterial.materialCode}				
						           <input type='hidden' value='${orderMaterial.materialCode}' name='sbdList[${status.index}].materialCode' /> 				    
								</td>
								<td>
								   ${orderMaterial.materialName}
						           <input type='hidden' value='${orderMaterial.materialName}' name='sbdList[${status.index}].materialName' /> 
								</td>
								<td>
								   ${orderMaterial.materialType}
						           <input type='hidden' value='${orderMaterial.materialType}' name='sbdList[${status.index}].materialType' /> 
								</td>
								<td>
								   ${orderMaterial.unit}
						           <input type='hidden' value='${orderMaterial.unit}' name='sbdList[${status.index}].unit' />
						           <input type='hidden' value='${orderMaterial.price}' name='sbdList[${status.index}].price' />  
							    </td>
								<td align="right">
							     ${orderMaterial.amount}
								 </td>
							    <td align="right">
							     ${orderMaterial.realSendAmount}
							    </td>
							    <td>
							       <input type="text" class="numPric" id="sendAmount${status.index}" name="sbdList[${status.index}].sendAmount" onfocus="getOldValue(this);" onblur="validateNum(this);account(this,${orderMaterial.waitSendAmount});" value="${orderMaterial.waitSendAmount}" style="width: 80px"/>
							    </td>
								<td>
								    <input type="hidden" name="sbdList[${status.index}].omId" value="${orderMaterial.omId }"/>
								    <input type="text" name="sbdList[${status.index}].remark" value="" style="width: 150px"/>
								</td>
							</tr>
						</c:forEach>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总物资数量：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalAmountStr">${totalAmount}<!-- 小计 --></span>
					</td>
					<td>&nbsp;<input type="hidden" id="oldValue" value="0"/></td>
				</tr>	
       	</table>
		<div class="buttonDiv">
				<button class="btn btn-success" onclick="save();" type="button"><i class="icon-white icon-ok-sign"></i>提交发货信息</button>
				<button class="btn btn-info" onclick="doView(${orderInfo.oiId });" type="button"><i class="icon-white icon-th-list"></i>查看历史发货信息</button>
				<button class="btn btn-danger" onclick="javascript:api.close();" type="button"><i class="icon-white icon-remove-sign"></i>关闭</button> 
		</div>
</div>
</div>
</form>
</body>
</html> 	