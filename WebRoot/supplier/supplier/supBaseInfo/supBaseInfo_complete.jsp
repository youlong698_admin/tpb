<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商完善信息第四步</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script type="text/javascript">
	    function doAudit(supplierId){
	      window.location.href="updateSupplierBaseInfoAudit_supplierBaseInfoSupplier.action?supplierInfo.supplierId="+supplierId;
	    }
	</script>
</head>
 
<body>
<form id="" name="" method="post" action="saveSupplierBaseInfo_supplierBaseInfoSupplier.action">

<div class="Conter_Container">
    <div class="Conter_main_conter">
    <c:choose>
       <c:when test="${falg}">
        <c:choose>
          <c:when test="${(isRegister=='1'||isRegister=='2')&&empty isAudit}">
	         <div class="alert alert-block alert-success">
	             <p>
	                <strong>
				<i class="icon icon-exclamation-sign"></i>
				        恭喜您，信息完善成功，请提交审核!
				</strong>
	             </p>
	         </div>	
	        
	    	<div class="buttonDiv">
					<button type="button" class="btn btn-info" id="btn-edit" onclick="doAudit(${supplierId})"><i class="icon-white icon-hdd"></i> 提交审核</button>
			</div>
		</c:when>
		<c:when test="${isAudit=='2'}">
	         <div class="alert alert-block alert-danger">
	             <p>
	                <strong>
				<i class="icon icon-exclamation-sign"></i>
				      对不起,  您的资料审核不通过，请修改后继续提交审核!
				</strong>
	             </p>
	         </div>	
	        
	    	<div class="buttonDiv">
					<button type="button" class="btn btn-info" id="btn-edit" onclick="doAudit(${supplierId})"><i class="icon-white icon-hdd"></i> 提交审核</button>
			</div>
		</c:when>
		<c:otherwise>
		     <div class="alert alert-block alert-success">
	             <p>
	                <strong>
				<i class="icon icon-exclamation-sign"></i>
				     等待审核中...............
				</strong>
	             </p>
	         </div>	
		</c:otherwise>
		</c:choose>
       </c:when>
       <c:otherwise>
          <div class="alert alert-block alert-danger">
             <p>
                <strong>
			<i class="icon icon-exclamation-sign"></i>
			        系统检测到你缺少以下资料，请务必上传如下资质：
			   <c:forEach items="${list_noting}" var="qualityCategory" varStatus="status">
			     &nbsp;&nbsp;&nbsp;&nbsp;${status.index+1}、${qualityCategory.qualityName }
			   </c:forEach>
			</strong>
             </p>
         </div>	
        
        
       </c:otherwise>
    </c:choose>
	   
	</div>
</div>
</form>
</body></html>