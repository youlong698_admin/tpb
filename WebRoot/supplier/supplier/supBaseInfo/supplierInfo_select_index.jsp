<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<title>供应商查询</title>
	    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
        <link type="text/css" href="<%= path %>/style/step/step.css" rel="stylesheet">
	    <script type="text/javascript">
	        
	      function switchTab(id,url){
	         for(var i=1;i<=3;i++){
	           if(i==id){
	             $("#li"+id).attr("class","step-active");	
	            }else if(i<id){
	             $("#li"+i).attr("class","step-done");	
	            }else{
	             $("#li"+i).attr("class","");
	            }
	         }
	         $('#iframe').attr('src',url);
	      }
	      function openWindowFrame(title,url,type){
		      createdetailwindow(title,url,type);	
		  }
	    </script>
	</head>
	<body>
	 <div class="page-content">

         <!-- /.page-header -->

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">
							<ol class="ui-step ui-step-yellow ui-step-4">
										<li class="step-start step-active" id="li1">
											<div class="ui-step-line"></div>
											<div class="ui-step-cont" onClick="switchTab(1,'viewSupplierBaseInfoDetail_supplierBaseInfoSupplier.action');">
												<span class="ui-step-cont-number">1</span>
												<span class="ui-step-cont-text">基本信息</span>
											</div>
										</li>
										<li  id="li2">
											<div class="ui-step-line"></div>
											<div class="ui-step-cont" onClick="switchTab(2,'viewSupplierProductInfo_supplierProductInfoSupplier.action');">
												<span class="ui-step-cont-number">2</span>
												<span class="ui-step-cont-text">产品信息</span>
											</div>
										</li>										
										<li  id="li3">
											<div class="ui-step-line"></div>
											<div class="ui-step-cont" onClick="switchTab(3,'viewSupplierCertificates_supCertificatesSupplier.action');">
												<span class="ui-step-cont-number">3</span>
												<span class="ui-step-cont-text">资质信息</span>
											</div>
										</li>
										<li class="step-end" id="li4">
											<div class="ui-step-line"></div>
											<div class="ui-step-cont" onClick="switchTab(4,'viewSupplierBaseInfoComplete_supplierBaseInfoSupplier.action');">
												<span class="ui-step-cont-number">4</span>
												<span class="ui-step-cont-text">完成</span>
											</div>
										</li>
							</ol>
								<div class="tab-content">

                                 <div id="smsj">
                                     <iframe src="viewSupplierBaseInfoDetail_supplierBaseInfoSupplier.action?supplierInfo.supplierId=${supplierInfo.supplierId}" id="iframe" frameborder=0 scrolling="yes" width="100%" height="900px"></iframe>
                                 </div> 
                             </div>
                         </div>

                     </div>
                 </div>
                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>
     <!-- /.page-content -->
     
     </body>
</html>