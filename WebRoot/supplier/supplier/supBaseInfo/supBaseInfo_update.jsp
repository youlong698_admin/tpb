<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>修改供应商信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
	<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
	<script src="<%=path%>/common/script/supplier/supBaseInfo.js" type="text/javascript" ></script>
    <script src="<%=path%>/common/jQuery/uploadPreview.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=path%>/common/jqueryCity/jquery.cityselect.js"></script> 
  	<script type="text/javascript">
		$(function (){
		
			//返回信息
		   <c:if test="${message!=null}">
		   window.onload=function(){ 
			  	  showMsg('success','${message}',function(){
			  	  window.location.href="viewSupplierBaseInfoDetail_supplierBaseInfoSupplier.action";
			  	  });
		  	      }
		    </c:if>
		     $("#logo").uploadPreview({ width: 220, height: 80, imgDiv: "#imgDiv"});
			 <c:if test="${empty supplierInfo.province}">
			  $("#city").citySelect({prov:"北京",city:"东城区"}); 
			 </c:if>
			 <c:if test="${not empty supplierInfo.province}">		 
			  $("#city").citySelect({prov:"${supplierInfo.province}",city:"${supplierInfo.city}"});  
			 </c:if> 
		    
		});
	
	</script> 
</head>
 
<body>
<form class="defaultForm" id="supInfo" name="" method="post" action="updateSupplierBaseInfo_supplierBaseInfoSupplier.action" ENCTYPE="multipart/form-data">
<input type="hidden" id="" name="supplierInfo.supplierId" value="${supplierInfo.supplierId}" />
<input type="hidden" id="" name="supplierProductGroup.supplierId" value="${supplierInfo.supplierId}" />
<input type="hidden" id="" name="supplierInfo.supplierLoginPwd" value="${supplierInfo.supplierLoginPwd}" />
<input type="hidden" id="" name="supplierInfo.isUsable" value="${supplierInfo.isUsable}" />
<input type="hidden" id="" name="supplierInfo.registDate" value="${supplierInfo.registDate}" />
<input type="hidden" id="" name="supplierInfo.isRegister" value="${supplierInfo.isRegister}" />
<input type="hidden" id="" name="supplierInfo.isAudit" value="${supplierInfo.isAudit}" />
<input type="hidden" id="" name="supplierInfo.isChange" value="${supplierInfo.isChange}" />
<input type="hidden" id="" name="supplierInfo.logo" value="${supplierInfo.logo}" />
<input type="hidden" id="proKindIds" name="supplierInfo.prodCodes" value="${supplierInfo.prodCodes}" />
<input type="hidden" id="returnVals" name="returnVals" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
        	<tr id="registrationTypeTr">
				<td width="20%" class="Content_tab_style1">公司名称：</td>
				<td width="30%" class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="公司名称不能为空！"  id="supplierName" name="supplierInfo.supplierName" value="${supplierInfo.supplierName}" onblur="checkOutPageValue(this);"/>&nbsp;<font color="#ff0000">*<div id="infoDsp"></div></font>
					<div class="info"><span class="Validform_checktip">公司名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td rowspan="4" colspan="2">
			       <div style="width:220px; height:80px; overflow:hidden;">  
					<div id="imgDiv">
					   <c:choose>
					      <c:when test="${empty supplierInfo.logo}">
                            <img src="<%=path %>/images/no.png" width="220" height="80">
					      </c:when>
					      <c:otherwise>					      
                            <img src="/fileWeb/${supplierInfo.logo}" width="220" height="80">
					      </c:otherwise>
					   </c:choose> 
					</div>  
					</div>
					<br>  
					<input id="logo" name="file" type="file" value="上传公司logo"> <span style="color: red">图片宽度：220  高度 ：80</span>
			    </td>
			</tr>
			 <tr>
				<td  class="Content_tab_style1" width="20%">
				 <c:if test="${not empty supplierInfo.registrationType}">				 
                    <input type="hidden" id="" name="supplierInfo.registrationType" value="${supplierInfo.registrationType}" />
					 <c:choose>
						<c:when test="${supplierInfo.registrationType=='01'}">
						    统一社会信用代码证号：  
						</c:when>
						<c:otherwise>
						     组织机构代码证编号：
						</c:otherwise>
					</c:choose>
				  </c:if>
				  <c:if test="${empty supplierInfo.registrationType}">
					 <select
						   id			 =	"registrationType"
	                       name          =  "supplierInfo.registrationType" style="width: 150px">
	                     <option value="01">统一社会信用代码证号</option>
	                     <option value="02">组织机构代码证编号</option>	                 
	                 </select>
				  </c:if>
				</td>
				<td  class="Content_tab_style2" width="30%" >
				    <c:if test="${not empty supplierInfo.registrationType}">
					   <input type="text"  maxlength="20" id="orgCode" name="supplierInfo.orgCode"  value="${supplierInfo.orgCode }" readonly style="background-color: #ccc"/>&nbsp;<font color="#ff0000">*</font>
					</c:if>
					<c:if test="${empty supplierInfo.registrationType}">
					   <input type="text"  maxlength="20" id="orgCode" name="supplierInfo.orgCode"  value="${supplierInfo.orgCode }" />&nbsp;<font color="#ff0000">*</font>
					</c:if>
				</td>
			</tr>
			 <tr>
			    <td class="Content_tab_style1">用户名：</td>
				<td class="Content_tab_style2">
					<input type="text"  id="supplierLoginName" name="supplierInfo.supplierLoginName" value="${supplierInfo.supplierLoginName}" readonly style="background-color: #ccc"/>&nbsp;<font color="#ff0000">*</font>					
				</td>
			</tr>

			<tr>
				<td class="Content_tab_style1">公司简称：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" id="" name="supplierInfo.supplierNameSimple" value="${supplierInfo.supplierNameSimple}" />&nbsp;
				</td>
    		 </tr>
			 <tr>
			 	<td  class="Content_tab_style1">成立日期：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.createDate" value="<fmt:formatDate value="${supplierInfo.createDate}" pattern="yyyy-MM-dd"/>" class="Wdate" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
				</td>
			    <td class="Content_tab_style1">企业性质：</td>
				<td class="Content_tab_style2">
					 
	                   <select name="supplierInfo.supplierType" datatype="*" nullmsg="企业性质不能为空！" >
	                  	<c:forEach items='${supplierTypeMap}' var='sMpa' >
						<c:if test='${sMpa.key ==supplierInfo.supplierType}'>
						<option value='${sMpa.key }' selected='selected'>${sMpa.value }</option>
						</c:if>
						<c:if test='${sMpa.key !=supplierInfo.supplierType}'>
						<option value='${sMpa.key }'>${sMpa.value }</option>
						</c:if>
						</c:forEach>
	                  </select>&nbsp;<font color="#ff0000">*</font>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">企业性质不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				
			</tr>
			<tr>
			<td class="Content_tab_style1">所在地区：</td>
			    <td class="Content_tab_style2">
			    <div id="city"> 
				      <select class="prov" name="supplierInfo.province" style="width: 105px;"></select>  
				      <select class="city" name="supplierInfo.city" style="width: 105px;" disabled="disabled"></select> 
				</div>          
                </td>
			    <td class="Content_tab_style1">所属行业：</td>
			    <td class="Content_tab_style2">
			    <select datatype="*" nullmsg="所属行业不能为空！" 
						   id			 =	"industryOwned"
	                       name          =  "supplierInfo.industryOwned" >
	                   <c:forEach var="map" items="${industryOwnedMap}">
	                   <option value="${map.key }" <c:if test="${supplierInfo.industryOwned==map.key }">selected</c:if>>${map.value }</option>
	                 </c:forEach>
	                 </select>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">所属行业不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
			<tr>  
			    <td class="Content_tab_style1">经营模式：</td>
			    <td class="Content_tab_style2">
			    <select datatype="*" nullmsg="经营模式不能为空！" 
						   id			 =	"managementModel"
	                       name          =  "supplierInfo.managementModel" >
	                   <c:forEach var="map" items="${managementModelMap}">
	                   <option value="${map.key }" <c:if test="${supplierInfo.managementModel==map.key }">selected</c:if>>${map.value }</option>
	                 </c:forEach>
	                 </select>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">经营模式不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">公司网站：</td>
			    <td class="Content_tab_style2"><input  name="supplierInfo.companyWebsite" type="text" id="companyWebsite" value="${supplierInfo.companyWebsite}">
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">企业法人：</td>
				<td  class="Content_tab_style2">
					<input type="text"  maxlength="15" class="Content_input_style1" id="" name="supplierInfo.legalPerson" value="${supplierInfo.legalPerson}" />
				</td>
				<td  class="Content_tab_style1">注册资金：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="注册资金不能为空！"  id="registerFunds" name="supplierInfo.registerFunds" value="${supplierInfo.registerFunds}"/>&nbsp;
					<font color="#ff0000">*</font>万元
					<div class="info"><span class="Validform_checktip">注册资金不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
    		<tr>
				<td class="Content_tab_style1">地址：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" id="" name="supplierInfo.supplierAddress" value="${supplierInfo.supplierAddress}" />
					&nbsp;
				</td>
				<td class="Content_tab_style1">公司电话：</td>
				<td class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="公司电话不能为空！" id="supplierPhone" name="supplierInfo.supplierPhone" maxlength="30" value="${supplierInfo.supplierPhone}" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">公司电话不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
    		<tr>
			    <td class="Content_tab_style1">公司传真：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" id="" name="supplierInfo.supplierFax" value="${supplierInfo.supplierFax}" maxlength="12"/>
				</td>
				<td class="Content_tab_style1">联系人：</td>
				<td class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="联系人不能为空！" id="contactPerson" name="supplierInfo.contactPerson" value="${supplierInfo.contactPerson}" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				
			</tr>
			
    		<tr>
    		   <td  class="Content_tab_style1">联系人手机：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="*|m" nullmsg="联系手机不能为空或联系人手机号码格式不正确！" errormsg="" id="mobilePhone" name="supplierInfo.mobilePhone" maxlength="11" value="${supplierInfo.mobilePhone}" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">联系手机不能为空或联系人手机号码格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td class="Content_tab_style1">联系人E-mail：</td>
				<td class="Content_tab_style2">
					<input type="text" datatype="/^\s*$/ |e" errormsg="E-mail格式不正确！" id=""  name="supplierInfo.contactEmail" value="${supplierInfo.contactEmail}" maxlength="100"/>
					<div class="info"><span class="Validform_checktip">E-mail格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
    		<tr>
				<td  class="Content_tab_style1">所属部门：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.deptName" value="${supplierInfo.deptName }" maxlength="100"/>
				</td>
				<td  class="Content_tab_style1">现任职务：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.duty" value="${supplierInfo.duty }" maxlength="100"/>
					</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">户名：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.bankAccountName" value="${supplierInfo.bankAccountName }" maxlength="100"/>
				</td>
				<td  class="Content_tab_style1">银行账号：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.bankAccount" value="${supplierInfo.bankAccount }" maxlength="50"/>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">开户银行：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.bank" value="${supplierInfo.bank }" maxlength="200"/>
				</td>
				<td  class="Content_tab_style1">税号：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.dutyParagraph" value="${supplierInfo.dutyParagraph }" maxlength="50"/>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">主营产品：</td>
				<td  class="Content_tab_style2" colspan="3">
					<textarea id="proKindNames" name="proKindNames" style="width: 90%"  rows="3" class="Content_input_style2" readonly datatype="*" nullmsg="主营产品不能为空！">${supplierInfo.prodKindNames}</textarea>
						&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择主营产品" onclick='selectProKind()'/>
					<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">主营产品不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">经营范围：</td>
				<td  class="Content_tab_style2" colspan="3">
				   <textarea id="introduce" name="supplierInfo.scopeBusiness" datatype="s0-1000|/^\s*$/" errormsg="经营范围限500个字！" style="width: 90%" rows="5" class="Content_input_style2" >${supplierInfo.scopeBusiness}</textarea>
					<div class="info"><span class="Validform_checktip">经营范围限500个字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">企业简介：</td>
				<td class="Content_tab_style2" colspan="3">
				   <textarea id="scopeBusiness" name="supplierInfo.introduce" style="width: 90%"  rows="5"  class="Content_input_style2" datatype="/^\s*$/|s0-1000" errormsg="企业简介限500个字！">${supplierInfo.introduce}</textarea><br>
				    <div class="info"><span class="Validform_checktip">企业简介限500个字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
        </table>
    	<div class="buttonDiv">
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body></html>