<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>新增供应商产品信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/supplier/supBaseInfo.js" type="text/javascript" ></script>
<link rel="stylesheet" href="<%=path %>/common/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="<%=path %>/common/kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="<%=path %>/common/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%=path %>/common/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="<%=path %>/common/kindeditor/plugins/code/prettify.js"></script>

<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileImg.js" type="text/javascript"></script>

<script language="javaScript">
var api = frameElement.api, W = api.opener;
$(function (){
	//返回信息
   <c:if test="${message!=null}">
  window.onload=function(){ 
	    showMsg('success','${message}');
	       W.doQuery();
	       api.close();
	  	}
    </c:if>
});	
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="SupplierProductInfo"; //当前是哪个类别功能上传的附件
var path="<%= path %>";
var imgData=new Array();;//已上传的图片列表

var editor1;
			KindEditor.ready(function(K) {
			    editor1 = K.create('textarea[name="supplierProductInfo.description"]', {
			    cssPath : '<%=path %>/common/kindeditor/plugins/code/prettify.css',
			    uploadJson : '<%=path %>/common/kindeditor/jsp/upload_json.jsp',
			    fileManagerJson : '<%=path %>/common/kindeditor/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
				}
			});
			//prettyPrint();
		});
	 	
		function doSave(){
		   document.getElementById("description").value=editor1.html(); 
		   document.form1.action="saveSupplierProductInfo_supplierProductInfoSupplier.action";
		   document.form1.submit();
		}
</script>
</head>
 
<body>
<form class="defaultForm" id="supProd" name="form1" method="post" action="">
<input type="hidden" id="supId" name="supplierProductInfo.supplierId" value=""/>
<input type="hidden" id="supId" name="supplierProductInfo.supplierName" value=""/>
<input type="hidden" id="" name="supplierProductInfo.isUsable" value="0" />
<!-- 修改页面删除附件的Id -->
<input type="hidden" id="proKindIds" name="supplierProductInfo.productId" value="" />
<input type="hidden" id="returnVals" name="returnVals" value=""/>
<input type="hidden" name="imgData"  id="imgData" value=""/>

<s:token/>
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
        	<tr>
          		<td  colspan="4" class="Content_tab_style_05">供应商产品信息</td>
        	</tr>
        	
        	<tr>
				<td width="20%" class="Content_tab_style1">产品名称：</td>
				<td width="30%" class="Content_tab_style2">
					<input type="text" id="productName" name="supplierProductInfo.productName" datatype="*" nullmsg="产品名称不能为空！" value="" class="Content_input_style1" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">产品名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>				
				<td width="20%" class="Content_tab_style1">产品类别：</td>
				<td width="30%" class="Content_tab_style2">
					<input type="text" id="proKindNames" name="supplierProductInfo.productDictionaryName" value="" readonly datatype="*" nullmsg="产品类别不能为空！"class="Content_input_style1" />&nbsp;<font color="#ff0000">*</font>
					<img src="<%=basePath %>/images/select.gif" title="选择产品类别" onclick='selectMyProKind()'/>
					<div class="info"><span class="Validform_checktip">产品类别不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
        	
    		<tr>
    			<td  class="Content_tab_style1">产品技术参数：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierProductInfo.productTechParameter" value="" class="Content_input_style1" />
				</td>
				<td  class="Content_tab_style1">规格型号：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierProductInfo.productKind" value="" class="Content_input_style1" />
				</td>
			</tr>

			<tr>
				<td  class="Content_tab_style1">计量单位：</td>
				<td  class="Content_tab_style2">
					<select name="supplierProductInfo.unit">
				        <c:forEach var="dictionary" items="${dictionaryList}">
				           <option value="${dictionary.dictName }">${dictionary.dictName }</option>
				        </c:forEach>
				   </select>
				</td>
				<td  class="Content_tab_style1">价格：</td>
				<td  class="Content_tab_style2">
					<input type="text" maxlength="10" datatype="n |/^(0|[1-9][0-9]*)+(.[0-9]{1,2})?$/" ignore="ignore" errormsg="价格必须为数字！" name="supplierProductInfo.price" value="" style="width: 100px" class="Content_input_style1" /><font color="red">“0”为面议</font>
					<div class="info"><span class="Validform_checktip">价格必须为数字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">产品销售占公司收入比例（%）：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" maxlength="10"  name="supplierProductInfo.salesPercent" value="" class="Content_input_style1" />
				</td>
				<td  class="Content_tab_style1">产品销售情况：</td>
				<td  class="Content_tab_style2" colspan="3">
					<input type="text" name="supplierProductInfo.productSales"  class="Content_input_style1" value="">
				</td>
			</tr>			
			<tr>
				<td  class="Content_tab_style1">产品详情描述：</td>
				<td  class="Content_tab_style2" colspan="3">
				  <textarea name="supplierProductInfo.description" id="description" style="width:100%;height:250px;visibility:hidden;"></textarea>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 图片存放 -->
					<div  id="imgDiv" class="panel" style="min-height: 100px;padding: 10px;">
						<table border="0" cellspacing="0" cellpadding="1" id="imgTable">
						<!-- 图片存放区域 -->
							<tr class="imgTr">
							</tr>
						<!-- 删除图片按钮存放区域 -->
							<tr class="buttonTr">
							</tr>	
						</table>
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
			
        </table>
        <div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-danger" id="btn-cacel" onclick="api.close();"><i class="icon-white icon-remove-sign"></i>关闭</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			
			//提交之前把选择的附件信息填充值
			$("#imgData").val(imgData);
			doSave();
			return false;	
		}
	});
})
</script>
</body></html>