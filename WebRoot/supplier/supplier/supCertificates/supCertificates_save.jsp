<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
<title>新建供应商资质信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/supplier/supBaseInfo.js" type="text/javascript" ></script> 
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
var api = frameElement.api, W = api.opener;
$(function (){
	//返回信息
   <c:if test="${message!=null}">
  window.onload=function(){ 
	    showMsg('success','${message}');
	       W.doQuery();
	       api.close();
	  	}
    </c:if>
});		
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="SupplierCertificateInfo"; //当前是哪个类别功能上传的附件
var path="<%= path %>" ;
var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
var fileNameData=[];//已上传的文件名
var fileTypeData=[];//已上传的文件的格式
var attIdData=[];//已存入附件表的附件信息
   function doChange(){
     var certificateStr=$("#certificateStr").val();
     var str=certificateStr.split(":");
     var qcId=str[0];
     if(qcId==0){
        $("#certificateName").removeAttr("readonly");
        $("#certificateName").val("");
        $("#qcId").val("0");
     }else{
        $("#certificateName").attr("readonly","readonly");
        $("#certificateName").val(str[1]);
        $("#qcId").val(str[0]);
     }
   }
</script>
</head>
 
<body onload="doChange()">
<form class="defaultForm" id="supCert" name="" method="post" action="saveSupplierCertificates_supCertificatesSupplier.action">
<input type="hidden" id="" name="supplierCertificateInfo.isUsable" value="0" />
<input type="hidden" name="supplierCertificateInfo.qcId" id="qcId" value=""/>

<input type="hidden" name="supplierCertificateInfo.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="supplierCertificateInfo.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="supplierCertificateInfo.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="supplierCertificateInfo.attIdData" id="attIdData" value=""/>

<input type="hidden" name="returnVals" id="returnVals" value=""/>

<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1" style="margin-top:10px;">
        	<tr>
				<td width="15%" class="Content_tab_style1">资质类别：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				    <select id="certificateStr" name="certificateStr" onchange="doChange()">
				         <c:forEach items="${list}" var="qualityCategory">
							    <option value="${qualityCategory.qcId }:${qualityCategory.qualityName }">${qualityCategory.qualityName }**</option>
						 </c:forEach>
						 <option value="0:其他资质">其他资质</option>
				    </select>
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">资质名称：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" id="certificateName" datatype="*" nullmsg="资质名称不能为空！" name="supplierCertificateInfo.certificateName" value="" class="Content_input_style1"/>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">资质名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">资质证书编号：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" maxlength="20" id="certificateCode" datatype="*" nullmsg="资质证书编号不能为空！" name="supplierCertificateInfo.certificateCode" value="" class="Content_input_style1"/>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">资质证书编号不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">有效期起：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" id="openDate" name="supplierCertificateInfo.timeBegain" value="<s:date name="" format="yyyy-MM-dd" />" 
							class="Wdate" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
				</td>
				<td width="15%" class="Content_tab_style1">有效期止：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" id="endDate" name="supplierCertificateInfo.timeEnd" value="<s:date name="" format="yyyy-MM-dd" />" 
					onchange="validateBidsDate();" class="Wdate" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">资质专业：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					<input type="text" id="" name="supplierCertificateInfo.certificateProfession" value="" class="Content_input_style1"/>&nbsp;
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">备注：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   <textarea id="remark" name="supplierCertificateInfo.remark" onkeyup="checkLen(100)" cols="90%" rows="5"  datatype="*0-200 |/^\s*$/ " errormsg="备注内容限100个汉字！" class="Content_input_style2" ></textarea><br>
				   <font color="#FF0000">[限100个汉字!包括标点和空格]</font>&nbsp;可输入字数：<span>100</span>&nbsp;已输入字数：<input name="" id="YesCou03" size=3 readonly value="0" class="YesCou03"/>	
					<div class="info"><span class="Validform_checktip">备注内容限100个汉字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
        </table>
        <div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-danger" id="btn-cacel" onclick="api.close();"><i class="icon-white icon-remove-sign"></i>关闭</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			
			//提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			return true;	
		}
	});
})
</script>
</body></html>