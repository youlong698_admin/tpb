<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商资质明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	
</head>
 
<body>
	<input type="hidden" name="supplierCertificateInfo.sciId" value="<s:property value="supplierCertificateInfo.sciId"/>"/>
<div class="Conter_Container">
	 <div class="Conter_main_conter">
    	<table class="table_ys1">        	
        	<tr>
				<td width="15%" class="Content_tab_style1">供应商名称：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					${supplierCertificateInfo.supplierName}
				</td>
			</tr>
        	
    		<tr>
				<td width="15%" class="Content_tab_style1">资质名称：</td>
				<td width="35%" class="Content_tab_style2">
					${supplierCertificateInfo.certificateName}
				</td>
				<td width="15%" class="Content_tab_style1">资质证书编号：</td>
				<td width="35%" class="Content_tab_style2">
					${supplierCertificateInfo.certificateCode}
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">有效期起：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${supplierCertificateInfo.timeBegain}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
				</td>
				<td width="15%" class="Content_tab_style1">有效期止：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${supplierCertificateInfo.timeEnd}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">资质专业：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					${supplierCertificateInfo.certificateProfession}
				</td>
			</tr>
			
			<tr>
				<td width="15%" class="Content_tab_style1">附件：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
						<c:out value="${supplierCertificateInfo.attachmentUrl}" escapeXml="false"/> 

				</td>
			</tr>
			
			<tr>
				<td width="15%" class="Content_tab_style1">备注：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   ${supplierCertificateInfo.remark}
				</td>
			</tr>
			
        </table>
	</div>
</div>
</body></html>