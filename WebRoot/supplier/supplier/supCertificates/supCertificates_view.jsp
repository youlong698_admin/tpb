<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>供应商资质管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script src="<%=path%>/common/script/supplier/supCertificates.js" type="text/javascript" ></script> 	
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
var _table;
var type="${type}";
var isAudit="${isAudit}";
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "viewToSupplierCertificates_supCertificatesSupplier.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX,
            {
				className : "ellipsis",	
				data : "certificateName",
				width : "35%",
				render: viewSupplierBase,
				orderable : false
			},
			{
				className : "ellipsis",	
				data : "certificateCode",
				width : "15%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{	
				className : "ellipsis",
				data : "timeBegain",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{	
				className : "ellipsis",
				data : "timeEnd",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				className : "ellipsis",	
				data : "certificateProfession",
				width : "15%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				className : "ellipsis",
				data : "remark",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.editItemInit(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	})
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();
	$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.sciId";
		
		//组装查询参数
		
		param.certificateName = $("#certificateName").val();
		param.timeBegain = $("#timeBegain").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	// 新增
	addItemInit: function()  {
	   createdetailwindow("新增供应商资质信息","saveSupplierCertificatesInit_supCertificatesSupplier.action",1);
	},
	//修改
	editItemInit: function(selectedItems)  {
	   var arrObj = selectedItems;
	    var sciId = "";
		if(arrObj.length!=0){
			if(arrObj.length==1){
				var sciId = arrObj[0].sciId;
				//alert(supId);
				if(sciId!='' && sciId!=null){
				   if(arrObj.qcId!=0&&type!=""){
				     createdetailwindow("修改供应商资质信息","updateSupplierCertificatesInitChange_supCertificatesSupplier.action?supplierCertificateInfo.sciId="+sciId,1);
				   }else{
					 createdetailwindow("修改供应商资质信息","updateSupplierCertificatesInit_supCertificatesSupplier.action?supplierCertificateInfo.sciId="+sciId,1);
				   }
				}
			}else{
				showMsgWeb("alert","温馨提示：只能选择一条信息！");
			}
		}else{
			showMsgWeb("alert","温馨提示：请选择要修改的信息");
		}
	},
	//删除
	deleteItem : function(selectedItems) {
		var arrObj = selectedItems;
		 var sciId = "";
		if(arrObj!=null&&arrObj.length!=0){
			for(var i=0;i<arrObj.length;i++){
				if(arrObj[i].qcId!=0&&isAudit=="0"){
				 showMsgWeb("alert","温馨提示：["+arrObj[i].certificateName+"]为必须上传的资质，不能删除");
				 return;
				}
				if(arrObj.length==1){
					sciId = arrObj[0].sciId;
					break;
				}
				sciId += arrObj[i].sciId +",";
			}
			//alert(sciId);
			if(sciId!=''&&sciId!=null){				
				$.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
				  var results = ajaxGeneral("deleteSupplierCertificates_supCertificatesSupplier.action","tempSupCertInfo="+sciId,"text");
				  
					   showMsg('success',''+results+'',function(){
					   		doQuery();							
		   					});
				   	 	});
			}
		}else{
			showMsgWeb("alert","温馨提示：请选择要删除的信息");
		}
	}
	
};
    //查看
    function viewSupplierBase(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看供应商资质信息\",\"viewSupplierCertificatesDetail_supCertificatesSupplier.action?supplierCertificateInfo.sciId=" + row.sciId + "\",2);'>"+data+"</a>"; 
              
    }
    
    function doQuery(){
     _table.draw();
    }

	//重置
	function doReset(){        
		document.forms[0].elements["certificateName"].value	=	"";
		document.forms[0].elements["timeBegain"].value		=	"";
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
			<div class="alert alert-block alert-danger">
			        <p>
                                       <strong>
							<i class="icon icon-exclamation-sign"></i>
							根据您选择的采购类别,请务必上传如下资质：
							   <c:forEach items="${list}" var="qualityCategory" varStatus="status">
							     &nbsp;&nbsp;&nbsp;&nbsp;${status.index+1}、${qualityCategory.qualityName }
							   </c:forEach>
							</strong>
                     </p>
                 </div>	
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<c:if test="${falg}">
							<button type="button" class="btn btn-info" id="btn-add" ><i class="icon-white icon-plus-sign"></i> 增加</button>
							<button type="button" class="btn btn-info" id="btn-edit" ><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del" ><i class="icon-white icon-trash"></i> 删除</button>
							</c:if>
						 </div>
					</div>
				</div>
				
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
						资质名称：
		            	<input type="text" placeholder="资质名称" class="input-medium" id="certificateName" name="supplierCertificateInfo.certificateName" size="14" value="<s:property value="supplierCertificateInfo.certificateName"/>" />
		         		   有效期起：
		            	<input type="text" placeholder="有效期起" name="supplierCertificateInfo.timeBegain" id="timeBegain" value="<s:date name="supplierCertificateInfo.timeBegain" format="yyyy-MM-dd" />" class="input-medium Wdate"
                               onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
                               
							  <button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
						      <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
						
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>资质名称</th>
									<th>资质证书编号</th>
									<th>有效期起</th>
									<th>有效期止</th>
									<th>资质专业</th>
									<th>备注</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>