<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<title>供应商变更审核</title>
	    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	    <script type="text/javascript">
		    
	      function switchTab(url){
	         $('#iframe').attr('src',url);
	      }
	      function openWindowFrame(title,url,type){
		      createdetailwindow(title,url,type);	
		  }
	    </script>
	</head>
	<body>
	 <input type="hidden" id="supplierId" value="${supplierInfo.supplierId }"/>
	 <div class="page-content">
         
         <!-- /.page-header -->

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">

                             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="switchTab('viewSupplierBaseInfoDetailChange_supplierBaseInfoSupplier.action?supplierInfo.supplierId=${supplierInfo.supplierId }');">基本信息</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewSupplierCertificatesChange_base_supCertificatesSupplier.action?supplierId=${supplierInfo.supplierId }');">资质信息</a>
                                 </li>
                                 
                               </ul>
								<div class="tab-content">

                                 <div id="smsj">
                                     <iframe src="viewSupplierBaseInfoDetailChange_supplierBaseInfoSupplier.action?supplierInfo.supplierId=${supplierInfo.supplierId}" id="iframe" frameborder=0 scrolling="yes" width="100%" height="600px"></iframe>
                                 </div> 
                             </div>
                         </div>
                     </div>
                 </div>
                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>
     <!-- /.page-content -->
     
     </body>
</html>