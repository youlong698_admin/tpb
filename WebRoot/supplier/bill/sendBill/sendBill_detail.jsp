<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看发货信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
</head>
<body >
<div class="Conter_Container" >
	
    <div class="Conter_main_conter">  
    	<table class="table_ys1"  width="100%">	
            <tr>
			    <td class="Content_tab_style1" width="15%">发货时间：</td>
			    <td class="Content_tab_style2" width="35%">
			       <fmt:formatDate value="${sendBill.sendDate}" pattern="yyyy-MM-dd"/>
			    </td>
			    <td class="Content_tab_style1" width="15%">发货说明：</td>
			    <td class="Content_tab_style2" width="35%">
			       ${sendBill.sendRemark }
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">发货凭证：</td>
			    <td class="Content_tab_style2" colspan="3">
			       <c:out value="${sendBill.attachmentUrl}" escapeXml="false"/>
			    </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" id="listtable">
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>发货数量</th>
				<th width="100px" nowrap>备注</th>
			</tr>
			    <c:set var="totalAmount" value="0"/>
					  <c:forEach items="${sbdList}" var="sendBillDetail" varStatus="status">
						    <c:set var="totalAmount" value="${totalAmount+sendBillDetail.sendAmount}"/> 
							<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
								<td>
								  ${status.index+1}
								</td>
								<td>
								    ${sendBillDetail.materialCode}								    
								</td>
								<td>
								   ${sendBillDetail.materialName}
								</td>
								<td>
								   ${sendBillDetail.materialType}
								</td>
								<td>
								   ${sendBillDetail.unit}
							    </td>
								<td align="right">
							     ${sendBillDetail.sendAmount}
								 </td>
								<td>
								    ${sendBillDetail.remark}
								</td>
							</tr>
						</c:forEach>
				<tr>
					<td colspan="5" align="right" style="font-weight: bold;color: red">总发货量：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalPriceStr">${totalAmount}<!-- 小计 --></span>
					</td>
					<td>&nbsp;</td>
				</tr>	
       	</table>
</div>
</div>
</body>
</html> 	