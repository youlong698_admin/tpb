<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>竞价信息</title>
	<link href="<%=path%>/style/default.css" rel="stylesheet" type="text/css" />
</head>
  
  <body>
    <c:choose>
       <c:when test="${num==0}">
         <table border="0" width="95%" align="center" cellpadding="0"
			cellspacing="0" class="table_ys1" style="vertical-align: top;">
			<thead>
				<tr class="Content_tab_style_04">
					<td nowrap  width="20%">
						消息发布时间
					</td>
					<td nowrap width="20%">
						发布人
					</td>
					<td nowrap width="60%">
						消息内容
					</td>
				</tr>
			</thead>
			<c:forEach items="${bciList}" var="bci">
			   <tr>
				<td nowrap><fmt:formatDate value="${bci.publishDate}" pattern="HH:mm" /></td>
				<td nowrap>${bci.publisher }</td>
				<td>${bci.info }</td>
			</tr>
			</c:forEach>
           </table>
       </c:when>
       <c:when test="${num==1}">
           <table width="100%" class="table_ys1">
			    <tr>
    	       <td colspan="5" class="Content_tab_style_td_head">历史报价信息</td>
    	    </tr>
				<tr class="Content_tab_style_04">
					<th width="5%" nowrap>
						序号
					</th>
					<th width="55%" nowrap>
						报价时间
					</th>
					<th width="30%" nowrap>
						总价
					</th>
					<th width="10%" nowrap>
						操作
					</th>
				</tr>
				<c:forEach items="${listValue}" var="bidPriceHistory" varStatus="status">
					<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>${status.index+1}</td>
						<td><fmt:formatDate value="${bidPriceHistory.writeDate}" type="both" pattern="yyyy-MM-dd HH:mm" /></td>
						<td><span class="text-muted">${bidPriceHistory.totalPrice}</span></td> 
						<td><button class='myViewButton' type="button"  onclick='doView(${bidPriceHistory.bphId})'><i class="icon-white icon-bullhorn"></i>查看</button></td>		
					</tr>
				</c:forEach>

			</table>
       </c:when>
       <c:when test="${num==2}">
           <table border="0" width="95%" align="center" cellpadding="0"
			cellspacing="0" class="table_ys1" style="vertical-align: top;">
			<thead>
				<tr class="Content_tab_style_04">
					<td nowrap  width="40%">
						供应商名称
					</td>
					<td nowrap width="30%">
						当前最低价格
					</td>
					<td nowrap width="30%">
						报价时间
					</td>
				</tr>
			</thead>
			<c:forEach items="${bpList}" var="bp">
			   <tr>
			   <c:choose>
			        <c:when test="${priceMode=='0'}">
			          	<td nowrap>${bp.supplierName }</td>
						<td align="right"><fmt:formatNumber value="${bp.totalPrice }" pattern="#,##0.00"/></td>
			        </c:when>
			        <c:when test="${priceMode=='1'}">
			          	<td nowrap>**</td>
						<td align="right">**</td>
			        </c:when>
			        <c:when test="${priceMode=='2'}">
			          	<td nowrap>${bp.supplierName }</td>
						<td align="right">**</td>
			        </c:when>
			        <c:otherwise>			           
			          	<td nowrap>**</td>
						<td align="right"><fmt:formatNumber value="${bp.totalPrice }" pattern="#,##0.00"/></td>
			        </c:otherwise>
			  </c:choose>
				<td nowrap><fmt:formatDate value="${bp.writeDate}" pattern="HH:mm" /></td>				
			   </tr>
			</c:forEach>
           </table>
       </c:when>
       <c:otherwise>
	       <div align="center" style="padding-top: 5px;">${title }：
						<font size="6" color="red"><fmt:formatNumber value="${price }" pattern="#,##0.00"/></font>
		   </div>
       </c:otherwise>
    </c:choose>
  </body>
</html>
