<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>网上报价</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
        //计算总价
		function accountPrice() 
		{
		    var indexArr=document.getElementsByName("indexArr");
			var amount,price,index,totalPrice=0;
				 for(var i=1;i<indexArr.length;i++){
				    index=indexArr[i].value;
			        price=$("#price_"+index).val();
			        if(price==""){
			           showMsg("alert","报价列有空值");
			           return false;
			        }
			        amount=$("#amount_"+index).val();
			        //showMsg("alert","amount="+amount);
			        //计算小计
			        var  prices=eval(price*amount).toFixed(2)+"";
			        document.getElementById("priceSumStr_"+index).innerHTML=prices;
		             
			       /*计算总合计*/
					totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
				 }
				 document.getElementById("totalPriceStr").innerHTML =totalPrice;
		}
		//执行保存
		function save(){
		debugger;
		  var indexArr=document.getElementsByName("indexArr");
			var amount,price,index,jsonData,totalPrice=0;
				 for(var i=1;i<indexArr.length;i++){
				    index=indexArr[i].value;
			        price=$("#price_"+index).val();
			        amount=$("#amount_"+index).val();
			        //showMsg("alert","amount="+amount);
			        //计算小计
			        var  prices=eval(price*amount).toFixed(2)+"";
			        document.getElementById("priceSumStr_"+index).innerHTML=prices;
		            
			       /*计算总合计*/
					totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
					
				 }
				 document.getElementById("totalPriceStr").innerHTML =totalPrice;
				 $("#totalPrice").val(totalPrice);
				 
				 var oldTotalPrice=$("#oldTotalPrice").val();
				 var hignPrice=$("#hignPrice").val();
				 var minPrice=$("#minPrice").val();
				 var biddingType=$("#biddingType").val();
				 var cj=FloatSub(totalPrice,hignPrice);
			     if(hignPrice!=""&&hignPrice!=0 && cj>0){
					showMsgWeb("alert","所填报的价格不得高于最高限价:"+hignPrice);
					return false;
				 }
			     var cj1=FloatSub(totalPrice,minPrice);
			     if(minPrice!=""&&minPrice!=0 && cj1<0){
					showMsgWeb("alert","所填报的价格不得低于最低限价:"+minPrice);
					return false;
				 }
				 if(biddingType=="0"){
				     if(FloatSub(totalPrice,oldTotalPrice)>=0){
				        showMsgWeb("alert","此次竞价的报价原则是越来越低，你当前报价大于你已报的最低价格："+oldTotalPrice+"，请重新填报！");
						return false;
				     }				   
				 }else{
				    if(FloatSub(totalPrice,oldTotalPrice)<=0){
				        showMsgWeb("alert","此次竞价的报价原则是越来越高，你当前报价小于你已报的最高价格："+oldTotalPrice+"，请重新填报！");
						return false;
				     }
				 }
				//$.dialog.confirm(" 温馨提示：确定提交报价信息吗? ",function (){
				   document.forms[0].action="saveBiddingBidPriceRespone_biddingBidSceneSupplier.action";
				   document.forms[0].submit();
			    //});
		
		}
    </script>
</head>
<body>
<c:if test="${not empty error}">
   <div class="alert alert-block alert-danger">
       <p>
            <strong>
			<i class="icon-white icon-remove-sign"></i>
			${error }
			</strong>
       </p>
  </div>
</c:if>
<c:if test="${empty error}">
<form class="defaultForm" id="supInfo" name="" method="post" action="">
<input type="hidden" id="biddingType" value="${biddingBidList.biddingType }"/>
<input type="hidden" id="hignPrice" value="${biddingBidList.hignPrice }"/>
<input type="hidden" id="minPrice" value="${biddingBidList.minPrice }"/>
<input type="hidden" id="oldTotalPrice" value="${bidPrice.totalPrice }"/>
<input type="hidden" id="bidPrice.remark" name="bidPrice.remark" value="${biddingBidList.currentBidRound }"/>
<input type="hidden" id="bidPrice.remark1" name="bidPrice.remark1" value="${biddingBidList.isTrialBid }"/>
<input type="hidden" id="bbrId" name="bbrId" value="${bbrId }"/>
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable">			
    	    <tr>
    	       <td colspan="8" class="Content_tab_style_td_head" align="center">报价信息</td>
    	    </tr>
    	     <tr>
		    <td colspan="8" style="font-size:14px;"><font color="red"><b>*.报价列不能有空值，如果此物料不供应，请填写0<br>
																	*.本次报价为：“${biddingBidList.priceTypeCn }”<br>
																	<c:if test="${not empty price}">
																	*.本轮次竞价您还能报价的次数：<strong>${price }</strong>。<br>
																	</c:if>
																	<c:if test="${not empty biddingBidList.hignPrice}">
																	*.最高限价：<strong>${biddingBidList.hignPrice }</strong>。<br>
																	</c:if>
																	<c:if test="${not empty biddingBidList.minPrice}">
																	*.最低限价：<strong>${biddingBidList.minPrice }</strong>。<br>
																	</c:if>
																	</b></font></td>
		    </tr>
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号
					   <input type="hidden" name="indexArr"  id="indexArr" value="-1"/></th>
				<th width="95px" nowrap>编码</th>
				<th width="10%" nowrap>名称 </th>
				<th width="10%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>${biddingBidList.priceColumnTypeCn }</th>
				<th width="100px" nowrap>小计</th>
			</tr>
			<c:forEach items="${bpdList}" var="bidPriceDetail" varStatus="status">
				<tr  class="input_ys1">
					<td>
					   ${status.index+1}
					   <input type="hidden" name="indexArr"  id="indexArr" value="${status.index}"/>
					</td>
					<td>
					    ${bidPriceDetail.requiredCollectDetail.buyCode}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.buyName}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.materialType}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.unit}
				    </td>
				    <td align="right">
				       ${bidPriceDetail.requiredCollectDetail.amount}				       
					   <input type="hidden" name="bpdList[${status.index}].amount"  id="amount_${status.index}" value="${bidPriceDetail.requiredCollectDetail.amount}"/>
				    </td>
					<td>
					 <input type="text" datatype="*" nullmsg="${biddingBidList.priceColumnTypeCn }不能为空！" style="width:150px;background-color: yellow" name="bpdList[${status.index}].price" id="price_${status.index}" value="${bidPriceDetail.price}"
								onblur="validateNum(this);"/>
					 <div class="info"><span class="Validform_checktip">${biddingBidList.priceColumnTypeCn }不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				     <input type="hidden" name="bpdList[${status.index}].supplierId"  id="supplierId_${status.index}" value="${supplierId }"/>
					 <input type="hidden" name="bpdList[${status.index}].rcdId"  id='rcdId_${status.index}' value='${bidPriceDetail.requiredCollectDetail.rcdId}'/>
					</td>
					<td>
					   <span id="priceSumStr_${status.index}"><!-- 小计 -->${bidPriceDetail.price*bidPriceDetail.requiredCollectDetail.amount}</span>
					</td>
				</tr>
			</c:forEach>
				<tr>
					<td colspan="2" align="center">总价：</td>
					<td colspan="6">
					    <input type="hidden" name="bidPrice.totalPrice"  id="totalPrice" value="${bidPrice.totalPrice }"/>
					    <input type="hidden" name="bidPrice.rcId"  id="rcId" value="${rcId }"/>
					    <input type="hidden" name="bidPrice.supplierId"  id="supplierId" value="${supplierId }"/>
						<span id="totalPriceStr"><!-- 小计 -->${bidPrice.totalPrice }</span>
					</td>
				</tr>		
				 <tr>
					<td colspan="2" align="center">税率（%）：</td>
					<td colspan="6">
						<input type='text' name='bidPrice.taxRate' style='width:150px;background-color: yellow'  id='taxRate' value='${bidPrice.taxRate }'  onblur="validateNum(this);" />
					</td>
				</tr>				  		
        </table>
        <c:if test="${fn:length(bbrList)>0}">
        <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
    	    </tr>
			 <tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="10%" nowrap>序号</th>
				<th width="20%" nowrap>响应项名称</th>
				<th width="30%" nowrap>响应项要求 </th>
				<th width="40%" nowrap>我的响应 </th>
			</tr>
			<c:forEach items="${bbrList}" var="bidBusinessResponse" varStatus="status">
			 <tr>
				<td align="center">
				   ${status.index+1}
				</td>
				<td>
				    ${bidBusinessResponse.responseItemName}
				</td>
				<td>
				   ${bidBusinessResponse.responseRequirements}
				</td>
				<td>
				<input type="text" datatype="*" nullmsg="我的响应不能为空！" style="width:150px;background-color: yellow" id="myResponse_${status.index}" name="bbrList[${status.index}].myResponse" value="${bidBusinessResponse.myResponse}"/>
				<div class="info"><span class="Validform_checktip">我的响应不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				<input type="hidden" id="responseRequirements_${status.index}" name="bbrList[${status.index}].responseRequirements" value="${bidBusinessResponse.responseRequirements }"/>
				<input type="hidden" id="responseItemName_${status.index}" name="bbrList[${status.index}].responseItemName" value="${bidBusinessResponse.responseItemName }"/>
				<input type="hidden" id="briId_${status.index}" name="bbrList[${status.index}].briId" value="${bidBusinessResponse.briId }"/>
				<input type="hidden" id="supplierId_${status.index}" name="bbrList[${status.index}].supplierId" value="${supplierId }"/>
				<input type="hidden" id="rcId_${status.index}" name="bbrList[${status.index}].rcId" value="${rcId }"/>
				</td>
				</tr>
		   </c:forEach>	  		
        </table>
        </c:if>        
         <div class="buttonDiv">
				<button class="btn btn-warning" id="btn-sum" type="button" onclick="accountPrice();"><i class="icon-white icon-hdd"></i>计算总价</button>
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>提交报价信息</button>				
		</div>
     </div>  
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			save();
			return false;	
		}
	});
})
</script>
</c:if>
	</body>
</html>