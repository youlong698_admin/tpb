<%@page contentType="text/html; charset=GBK"%>
<%String path = request.getContextPath();%>
<html>
	<head>
		<title>成功页面</title>	
		<script src="<%=path %>/common/script/context.js" type="text/javascript" ></script>
		<script type="text/javascript">
		
        var api = frameElement.api, W = api.opener;
		 //关闭 
		function closeWindow(){		    
			    api.close();
		}
		//指定时间自动关闭
		//window.setTimeout("closeWindow()", 3); 
		</script>
	</head>
	
	<body class="top_02">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="alert alert-block alert-danger">
								${message }
							</div>
							<div class="alert alert-block alert-info" id="div1">
								 <button type="button" class="btn btn-danger" id=btn-danger onclick="closeWindow()"><i class="icon-white icon-remove-sign"></i> 关闭</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>