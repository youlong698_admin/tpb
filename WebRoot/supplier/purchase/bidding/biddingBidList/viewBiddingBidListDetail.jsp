<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>竞价方案查看</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
	<script type="text/javascript">
    var api = frameElement.api, W = api.opener, cDG;
    </script>
</head>
<body>
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
    	<table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">竞价项目信息</td>
    	    </tr>
    	    <tr>
				<td width="15%" class="Content_tab_style1">项目名称：</td>
				<td width="35%" class="Content_tab_style2">
					${requiredCollect.buyRemark }
				</td>
				<td width="15%" class="Content_tab_style1">项目编号：</td>
				<td width="35%" class="Content_tab_style2">
					${requiredCollect.bidCode }
				</td>
			</tr>
			 <tr>
				<td width="15%" class="Content_tab_style1">项目负责人：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.responsibleUser }
				</td>
				<td width="15%" class="Content_tab_style1">负责人手机号：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.responsiblePhone }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">竞价方式：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.biddingTypeCn }
				</td>
				<td width="15%" class="Content_tab_style1">报价显示方式：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.priceModeCn }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价原则：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.pricePrincipleCn }
				</td>
				<td width="15%" class="Content_tab_style1">竞价原则：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.biddingPrincipleCn }
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">竞价开始时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${biddingBidList.biddingStartTime}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${biddingBidList.biddingEndTime}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>
			 <tr>
				<td width="15%" class="Content_tab_style1">最高限价<font color="red">(空则不限价)</font>：</td>
				<td width="35%" class="Content_tab_style2">
				    ${biddingBidList.hignPrice}
				</td>
				<td width="15%" class="Content_tab_style1">最低限价<font color="red">(空则不限价)</font>：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.minPrice}
				</td>
			</tr>	
			<tr>
    	       <td colspan="4" class="Content_tab_style_td_head">试竞价轮次设置</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="trialBidRoundTable" class="table_ys1" width="100%">
    	              <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>序号</th>
						<th width="20%" nowrap>轮次</th>
						<th width="20%" nowrap>时长（分钟） </th>
						<th width="50%" nowrap>限制供应商报价次数(<font color="red">如不限制，则不填写</font>) </th>
					  </tr>
				      <c:forEach var="biddingBidRound" items="${trialBidRounds}" varStatus="status">
				         <tr>
				            <td align="center">
				             ${status.index+1}
				             </td>
				            <td>第${biddingBidRound.biddingRound }轮</td>
				            <td>${biddingBidRound.timeLong/60 }</td>
				            <td>${biddingBidRound.priceCount }</td>
				         </tr>
				     </c:forEach>
				    </table>
				  </td>
		      </tr>		
		      <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">正式竞价轮次设置</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="bidRoundTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>序号</th>
						<th width="20%" nowrap>轮次</th>
						<th width="20%" nowrap>时长（分钟） </th>
						<th width="50%" nowrap>限制供应商报价次数(<font color="red">如不限制，则不填写</font>) </th>
					  </tr>
				      <c:forEach var="biddingBidRound" items="${bidRounds}" varStatus="status">
				         <tr>
				            <td align="center">
						        ${status.index+1}
						    </td>
				            <td>第${biddingBidRound.biddingRound }轮</td>
				            <td>${biddingBidRound.timeLong/60 }</td>
				            <td>${biddingBidRound.priceCount }</td>
				        </tr>
				     </c:forEach>
				    </table>
				  </td>
		      </tr>		  		
        </table>
        <table align="center" class="table_ys1" id="listtable">	
		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap>序号</th>
			<th width="95px" nowrap>编码</th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredCollectDetail" varStatus="status">
			<tr  class="input_ys1">
				<td>${status.index+1}
				</td>
				<td>
				    ${requiredCollectDetail.buyCode}
				</td>
				<td>
				   ${requiredCollectDetail.buyName}
				</td>
				<td>
				   ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.unit}
			    </td>
			    <td align="right">
			       ${requiredCollectDetail.amount}
			    </td>
				<td>
					<fmt:formatDate value="${requiredCollectDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
				</td>
				<td>${requiredCollectDetail.remark }</td>
			</tr>
		</c:forEach>		
	</table>
     </div>  
	</div>
	</body>
</html>