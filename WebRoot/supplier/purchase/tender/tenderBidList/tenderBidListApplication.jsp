<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>招标信息查看</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
	<script type="text/javascript">
    var api = frameElement.api, W = api.opener, cDG;
    function saveApplication(rcId){
      window.location.href="saveTenderBidListApplication_tenderBidListSupplier.action?rcId="+rcId;
    }
    </script>
</head>
<body>
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
    	<table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">招标项目信息</td>
    	    </tr>
    	    <tr>
				<td width="15%" class="Content_tab_style1">项目名称：</td>
				<td width="35%" class="Content_tab_style2">
					${requiredCollect.buyRemark }
				</td>
				<td width="15%" class="Content_tab_style1">项目编号：</td>
				<td width="35%" class="Content_tab_style2">
					${requiredCollect.bidCode }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">项目负责人：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.responsibleUser }
				</td>
				<td width="15%" class="Content_tab_style1">负责人手机号：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.responsiblePhone }
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">开标日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">招标文件领购开始日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.salesDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">招标文件领购截止日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.saleeDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">回标截止日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.returnDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">标书费（元）：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.tenderMoney }
				</td>
				<td width="15%" class="Content_tab_style1">保证金（元）：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.bondMoney }
				</td>
			</tr>					  		
        </table>
        <table align="center" class="table_ys1" id="listtable">	
		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap>序号</th>
			<th width="95px" nowrap>编码</th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredCollectDetail" varStatus="status">
			<tr  class="input_ys1">
				<td>${status.index+1}
				</td>
				<td>
				    ${requiredCollectDetail.buyCode}
				</td>
				<td>
				   ${requiredCollectDetail.buyName}
				</td>
				<td>
				   ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.unit}
			    </td>
			    <td align="right">
			       ${requiredCollectDetail.amount}
			    </td>
				<td>
					<fmt:formatDate value="${requiredCollectDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
				</td>
				<td>${requiredCollectDetail.remark }</td>
			</tr>
		</c:forEach>		
	</table>
         <div class="buttonDiv">
            <c:choose>
               <c:when test="${isApplication}">
                  <div class="alert alert-block alert-success">
	             <p>
	                <strong>
				<i class="icon icon-exclamation-sign"></i>
				           本项目您已报名成功!
				</strong>
	             </p>
	         </div>
               </c:when>
               <c:otherwise>
                  <c:if test="${isApplicationDate}">
		   	    	 <button class="btn btn-success" onclick="saveApplication(${rcId});" type="button"><i class="icon-white icon-bookmark"></i>报名</button>
				  </c:if>
				  <c:if test="${!isApplicationDate}">
		   	    	 <div class="alert alert-block alert-warning">
			             <p>
			                <strong>
						<i class="icon icon-exclamation-sign"></i>
						           对不起,本项目已过报名时间!
						</strong>
			             </p>
			         </div>
				  </c:if>
				</c:otherwise>
            </c:choose>
            </div>	
     </div>  
	</div>
	</body>
</html>