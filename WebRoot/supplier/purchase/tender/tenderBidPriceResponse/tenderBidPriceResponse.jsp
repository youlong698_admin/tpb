<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>网上投标</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 
 function doView(bphId){
	   createdetailwindow("查看供应商投标","viewTenderBidPriceResponeDetail_tenderBidPriceResponseSupplier.action?bidPriceHistory.bphId="+bphId,1);
	}
 function doPrice(rcId){
       createdetailwindow("供应商投标","saveInitTenderBidPriceRespone_tenderBidPriceResponseSupplier.action?rcId="+rcId,1);
 }
 function doSms(rcId){
     //$.dialog.confirm(" 温馨提示：确定要短信邮件通知项目负责人吗? ",function (){
        var result=ajaxGeneral("saveSmsTenderBidPriceRespone_tenderBidPriceResponseSupplier.action","rcId="+rcId,"text");
	    if(result=="1"){
	      $('#btn-sms').attr("disabled","disabled");
	      showMsgWeb("alert","短信邮件已告知！");	      
	    }else{
	      showMsgWeb("alert","短信邮件告知失败，请联系客服人员！");
	    }
	 //});		
 }
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row-fluid">
								<div class="span12">
							     <div class="btn-toolbar">
							        <c:if test="${isReturnTime}">
				                    <button type="button" class="btn btn-info" id="btn-add" onclick="doPrice(${rcId})"><i class="icon-white icon-plus-sign"></i>网上报价</button>
				                    <button type="button" class="btn btn-primary" id="btn-sms" onclick="doSms(${rcId})" <c:if test="${isSms=='0' }">disabled="disabled" title="短信已告知" </c:if> ><i class="icon-white icon-envelope"></i>短信邮件告知</button>
				                    </c:if>
							      </div>
                             </div>
                           </div>
							
							<div class="row">
							  <div class="span12" id="content">
								<div class="row">
									<div class="col-xs-12">
										<div class="alert alert-block alert-danger">
											注意：<br/>
											   1、招标方没有解密报价之前，价格采取保密机制。<br/>
											   2、开标之前可以多次报价，系统以最后一次的投标报价为准。<br/>
											   3、为防止短信骚扰，每个项目报价之后只有一次通知项目负责人的机会，请确认当次报价为最终报价的时候，才进行短信邮件告知项目负责人。
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="5" class="Content_tab_style_td_head">历史投标信息</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="55%" nowrap>
												投标时间
											</th>
											<th width="30%" nowrap>
												总价
											</th>
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${listValue}" var="bidPriceHistory" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td><fmt:formatDate value="${bidPriceHistory.writeDate}" type="both" pattern="yyyy-MM-dd HH:mm" /></td>
												   <c:choose>
												       <c:when test="${empty bidPriceHistory.totalPrice}">
									                       <td>   *****</td><td>
									                       <span class="text-muted">价格保密</span></td> 
												       </c:when>
													   <c:otherwise>  
													    <td><span class="text-muted"><fmt:formatNumber value="${bidPriceHistory.totalPrice}" pattern="#00.00#"/></span></td> 
													    <td><button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidPriceHistory.bphId})'><i class="icon-white icon-bullhorn"></i></button></td>		
											         </c:otherwise>
												   </c:choose>
												</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>