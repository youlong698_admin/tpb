<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>网上投标</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
    </script>
</head>
<body>
<form class="defaultForm" id="supInfo" name="" method="post" action="">
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable">			
    	    <tr>
    	       <td colspan="8" class="Content_tab_style_td_head" align="center">报价信息</td>
    	    </tr>
    	     <tr>
		    <td colspan="8" style="font-size:14px;"><font color="red"><b>1.本次报价为：“${tenderBidList.priceTypeCn }”<br></b></font></td>
		    </tr>
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号
					   <input type="hidden" name="indexArr"  id="indexArr" value="-1"/></th>
				<th width="95px" nowrap>编码</th>
				<th width="10%" nowrap>名称 </th>
				<th width="10%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>${tenderBidList.priceColumnTypeCn }</th>
				<th width="100px" nowrap>小计</th>
			</tr>
			<c:forEach items="${bpdhList}" var="bidPriceDetailHistory" varStatus="status">
				<tr  class="input_ys1">
					<td>
					   ${status.index+1}
					</td>
					<td>
					    ${bidPriceDetailHistory.requiredCollectDetail.buyCode}
					</td>
					<td>
					   ${bidPriceDetailHistory.requiredCollectDetail.buyName}
					</td>
					<td>
					   ${bidPriceDetailHistory.requiredCollectDetail.materialType}
					</td>
					<td>
					   ${bidPriceDetailHistory.requiredCollectDetail.unit}
				    </td>
				    <td align="right">
				     ${bidPriceDetailHistory.requiredCollectDetail.amount}
					 </td>
				    <td align="right">
				       <fmt:formatNumber value="${bidPriceDetailHistory.price}" pattern="#,##0.00"/>
				    </td>
					<td>
					   <fmt:formatNumber value="${bidPriceDetailHistory.price*bidPriceDetailHistory.requiredCollectDetail.amount}" pattern="#,##0.00"/>
					</td>
				</tr>
			</c:forEach>
				<tr>
					<td colspan="2" align="center">总价：</td>
					<td colspan="6">
					    <fmt:formatNumber value="${bidPriceHistory.totalPrice }" pattern="#,##0.00"/>
					</td>
				</tr>		
				 <tr>
					<td colspan="2" align="center">税率（%）：</td>
					<td colspan="6">
						${bidPriceHistory.taxRate }
					</td>
				</tr>				  		
        </table>
        <c:if test="${fn:length(bbrhList)>0}">
        <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
    	    </tr>
			 <tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="10%" nowrap>序号</th>
				<th width="20%" nowrap>响应项名称</th>
				<th width="30%" nowrap>响应项要求 </th>
				<th width="40%" nowrap>我的响应 </th>
			</tr>
			<c:forEach items="${bbrhList}" var="bidBusinessResponseHistory" varStatus="status">
			 <tr>
				<td align="center">
				   ${status.index+1}
				</td>
				<td>
				    ${bidBusinessResponseHistory.responseItemName}
				</td>
				<td>
				   ${bidBusinessResponseHistory.responseRequirements}
				</td>
				<td>
				   ${bidBusinessResponseHistory.myResponse}
				 </td>
				</tr>
		   </c:forEach>	  		
        </table>
        </c:if> 
        <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="2" class="Content_tab_style_td_head" align="center">投标文件</td>
    	    </tr>  
    	    <tr>
				<td  class="Content_tab_style1" width="30%">投标文件</td>
				<td class="Content_tab_style2">
					<c:out value="${bidPriceHistory.attachmentUrl}" escapeXml="false"/>
				</td>
			</tr>	
		 </table>     
     </div>  
	</div>
</form>
	</body>
</html>