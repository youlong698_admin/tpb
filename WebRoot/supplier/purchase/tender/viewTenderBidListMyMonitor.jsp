<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>招标项目信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<link href="<%=path%>/style/bid.css" rel="stylesheet" type="text/css" />
<script language="javaScript">
  var rcId="${requiredCollect.rcId}";
  $(function (){
	 	//获取没有查看的标前澄清个数
	 	var count=ajaxGeneral("getBidClarifyForNode_bidClarifySupplier.action","rcId="+rcId,"text");
	    $("#bqcq").append('<span class="XxtsNum">'+count+'条</span>');
   });
   function doClick(title,url){
      if(url.indexOf("?")!=-1) 
      url=url+"&rcId="+rcId;
      else
      url=url+"?rcId="+rcId;
      createdetailwindow(title,url,0);      
   }
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12" id="content">
				<div class="row-fluid" style="padding-top: 20px">
					<div class="span12" id="div-table-container">
				
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								
								<!--内容-->
								<div class="row">
								<div class="row header orange">
									<div class="col-md-8"><i class="ace-icon fa fa-file-o"></i>招标项目相关信息</div>
									<div class="col-md-2" align="right"></div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<dl>
											<dt>
												项目编号：
											</dt>
											<dd>
												${requiredCollect.bidCode }
											</dd>
										</dl>
									</div>
									<div class="col-md-6">
										<dl>
											<dt>
												项目名称：
											</dt>
											<dd>
												${requiredCollect.buyRemark }
											</dd>
										</dl>
									</div>
								</div>
									</div>
		                              <div class="row">
										<div class="title">
											采购过程
										</div>
										<div class="cardlist">
											<div class="card "  onclick="doClick('基本信息','viewTenderBidListDetailMonitor_tenderBidListSupplier.action')" onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i01.png" />
													<div>
														<span class="bidNode">基本信息</span>
															
													</div>												
											</div>
											<div class="card "  onclick="doClick('招标公告','viewBidBulletinDetialBidMonitor_bidBulletinSupplier.action')" onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i09.png" />
													<div>
														<span class="bidNode">招标公告</span>
															
													</div>												
											</div>
											<div class="card "  onclick="doClick('文件下载','viewTenderFileDownloadDetialBidMonitor_tenderFileDownloadSupplier.action')" onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i02.png" />
													<div>
														<span class="bidNode">文件下载</span>
															
													</div>												
											</div>
											<div class="card " id="bqcq" onclick="doClick('标前澄清','viewBidClarifyMonitor_bidClarifySupplier.action')" onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i03.png" />
													<div>
														<span class="bidNode">标前澄清</span>
															
													</div>												
											</div>
											<div class="card "  onclick="doClick('网上投标','viewTenderBidPriceResponseMonitor_tenderBidPriceResponseSupplier.action')" onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i05.png" />
													<div>
														<span class="bidNode">网上投标</span>
															
													</div>												
											</div>											
											<div class="card "  onclick="doClick('标中质询','viewTenderBidCommunicationInfoMonitor_tenderBidCommunicationInfoSupplier.action')" onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i04.png" />
													<div>
														<span class="bidNode">标中质询</span>
															
													</div>												
											</div>
											<div class="card " onclick="doClick('二次报价','viewBidNegotiateMonitor_bidNegotiateSupplier.action')"   onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i06.png" />
													<div>
														<span class="bidNode">二次报价</span>
															
													</div>												
											</div>
											<div class="card " onclick="doClick('结果通知','viewBidResultNoticeMonitor_bidResultNoticeSupplier.action')"  onmouseover="javascript:this.style.background='#eef4f7'" onmouseout="javascript:this.style.background='#fafbfb'">
													<img src="<%=path %>/images/bidImg/i07.png" />
													<div>
														<span class="bidNode">结果通知</span>
															
													</div>												
											</div>
								       </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
</body>
</html>