<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>招标文件下载</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
function doTenderBidFileDown(rcId){
    ajaxGeneral("saveTenderFileDown_tenderFileDownloadSupplier.action","rcId="+rcId,"text");
 }
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							  <div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
									    <tr>
						    	         <td colspan="2" class="Content_tab_style_td_head">招标文件下载</td>
						    	        </tr>
										<tr>
											<td width="30%" class="Content_tab_style1">开标日期：</td>
											<td width="70%" class="Content_tab_style2">
												<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />
											</td>
				                        </tr>
									    <tr>
											<td class="Content_tab_style1">项目负责人：</td>
											<td class="Content_tab_style2">
												${tenderBidList.responsibleUser }
											</td>
										 </tr>
										 <tr>
										 	<td class="Content_tab_style1">负责人手机号：</td>
											<td class="Content_tab_style2">
												${tenderBidList.responsiblePhone }
											</td>
										</tr>
										<tr>
										    <td class="Content_tab_style1">招标文件领购开始日期：</td>
											<td class="Content_tab_style2">
												<fmt:formatDate value="${tenderBidList.salesDate}" pattern="yyyy-MM-dd HH:mm" />
											</td>
										</tr>
							
										<tr>
											<td class="Content_tab_style1">招标文件领购截止日期：</td>
											<td class="Content_tab_style2">
												<fmt:formatDate value="${tenderBidList.saleeDate }" pattern="yyyy-MM-dd HH:mm" />
											</td>
										</tr>
										
				                        <tr>
											<td class="Content_tab_style1">招标文件：</td>
											<td class="Content_tab_style2" style="color: red">
											  <c:choose>
											   <c:when test="${isApplicationDate}">
											       <c:if test="${empty isTenderBidFile}">
													  对不起,您没有下载权限,请联系项目负责人!!!
													</c:if>
												   <c:if test="${isTenderBidFile=='0'}">
													  <div onclick="doTenderBidFileDown(${tenderBidList.rcId})"><c:out value="${tenderBidFile.attachmentUrl}" escapeXml="false"/></div>
													</c:if>
												</c:when>
												<c:otherwise>
												      对不起,已过下载时间!!!
												</c:otherwise>
												</c:choose>
											</td>
										</tr>

									</table>
								</div>
							</div>
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
										<tr>
							    	       <td colspan="4" class="Content_tab_style_td_head">历史下载查看</td>
							    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="5%" nowrap>
												下载人
											</th>
											<th width="10%" nowrap>
												联系电话
											</th>
											<th width="5%" nowrap>
												下载日期
											</th>
										</tr>
										<c:forEach items="${tenderFileDownloadList}" var="tenderFileDownload" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>												
												<td>${tenderFileDownload.fileDownName}</td>
												<td>${tenderFileDownload.fileDownTel}</td>
												<td><fmt:formatDate value="${tenderFileDownload.fileDownDate}" type="both" pattern="yyyy-MM-dd" /></td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>