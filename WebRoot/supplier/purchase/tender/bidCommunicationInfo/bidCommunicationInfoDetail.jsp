<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>标中质询</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 function doView(bciId){
	   createdetailwindow("查看标中质询","viewBidCommunicationInfoDetail_tenderBidCommunicationInfoSupplier.action?bidCommunicationInfo.bciId="+bciId,1);
	}
	function doUpdate(bciId){
	   createdetailwindow("回复问题解答","updateBidCommunicationInfoInit_tenderBidCommunicationInfoSupplier.action?bidCommunicationInfo.bciId="+bciId,1);
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="7" class="Content_tab_style_td_head">标中质询</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="10%" nowrap>
												质询人
											</th>
											<th width="30%" nowrap>
												质询内容
											</th>
											<th width="10%" nowrap>
												质询日期
											</th>
											<th width="10%" nowrap>
												质询供应商
											</th>
											<th width="30%" nowrap>
												回复内容
											</th>
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${bidCommunicationInfoList}" var="bidCommunicationInfo" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${bidCommunicationInfo.questionerName}</td>	
												<td>${bidCommunicationInfo.questionContent}</td>	
												<td><fmt:formatDate value="${bidCommunicationInfo.questionDate}" type="both" pattern="yyyy-MM-dd" /></td>
												<td>${bidCommunicationInfo.answerName}</td>	
												<td>${bidCommunicationInfo.answerContent}</td>	
												<td>
												   <c:if test="${empty bidCommunicationInfo.answerDate}">
												     <button class='btn btn-mini btn-primary' type="button"  onClick="doUpdate(${bidCommunicationInfo.bciId})"><i class="icon-white icon-hand-right"></i></button>
												   </c:if>
												   <c:if test="${not empty bidCommunicationInfo.answerDate}">
												     <button class='btn btn-mini btn-primary' type="button"  onClick="doView(${bidCommunicationInfo.bciId})"><i class="icon-white icon-bullhorn"></i></button>
												   </c:if>
												</td>	
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>