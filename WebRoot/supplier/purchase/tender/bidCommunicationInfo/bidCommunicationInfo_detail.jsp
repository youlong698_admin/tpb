<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>标中质询</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
			
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">
					 
								<div class="col-xs-12">
									<table width="98%" border="0" align="center"  class="table_ys2">
						        	<tr>
						          		<td  colspan="2" class="Content_tab_style_td_head">问题</td>
						        	</tr>
									<tr>
										<td class="Content_tab_style1"  width="35%">供应商名称：</td>
										<td class="Content_tab_style2">
									      ${bidCommunicationInfo.questionerName}
									    </td>
									</tr>
									<tr>
										<td class="Content_tab_style1">质询内容：</td>
										<td class="Content_tab_style2"> 
											${bidCommunicationInfo.questionContent}
										</td>
									</tr>
									<tr>
										<td class="Content_tab_style1">质询日期：</td>
										<td class="Content_tab_style2"> 
											<fmt:formatDate value="${bidCommunicationInfo.questionDate}" pattern="yyyy-MM-dd" />
										</td>
									</tr>
									</table>
									<table width="98%" border="0" align="center"  class="table_ys2">
						        	<tr>
						          		<td  colspan="2" class="Content_tab_style_td_head">解答</td>
						        	</tr>
									<tr>
										<td class="Content_tab_style1" width="35%">回复内容：</td>
										<td class="Content_tab_style2"> 
											${bidCommunicationInfo.answerContent }
										</td>
									</tr>
									<tr>
										<td class="Content_tab_style1">回复人：</td>
										<td class="Content_tab_style2"> 
											${bidCommunicationInfo.answerName }
										</td>
									</tr>
									<tr>
										<td class="Content_tab_style1">回复日期：</td>
										<td class="Content_tab_style2"> 
											<fmt:formatDate value="${bidCommunicationInfo.answerDate}" pattern="yyyy-MM-dd" />
										</td>
									</tr>
						        </table>
								</div>
								
								
							
							</div>
							
						</div>
					</div>
                    
				</div>
			</div>
		</div>
	</body>
</html>