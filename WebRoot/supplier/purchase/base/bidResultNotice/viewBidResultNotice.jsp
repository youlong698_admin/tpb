<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>结果通知</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
			
					<div class="row">
						<div class="col-xs-12">
						<c:if test="${not empty bidResultNotice.brnId}">
							<div class="row">
							  <div class="span12" id="content">
								<div class="row">
									<div class="col-xs-12">
										<div class="alert alert-block alert-danger">
											<strong>恭喜您，中标了！！</strong>
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="row">						 
								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
										<tr>
											<td colspan="4" class="Content_tab_style_td_head">
												结果通知
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												中标金额：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												 <fmt:formatNumber value="${bidResultNotice.bidPrice}" pattern="#,##0.00"/>
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												附件：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												<c:out value="${bidResultNotice.attachmentUrl}" escapeXml="false"/>
											</td>
										</tr>

									</table>
								</div>
							
							</div>
							
						</c:if>
						<c:if test="${empty bidResultNotice.brnId}">
						<div class="row">
							  <div class="span12" id="content">
								<div class="row">
									<div class="col-xs-12">
										<div class="alert alert-block alert-warning">
											<strong>很遗憾，您没有中标！！</strong>
										</div>
									</div>
								</div>
							</div>
							</div>
						</c:if>
						</div>
					</div>
                    
				</div>
			</div>
		</div>
	</body>
</html>