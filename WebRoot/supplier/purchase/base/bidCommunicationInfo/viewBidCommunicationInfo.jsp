<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>问题解答</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 var rcId="${rcId}";
 $(function(){
   $("#btn-add").click(function(){
     createdetailwindow("我要提问","saveBidCommunicationInfoInit_bidCommunicationInfoSupplier.action?rcId="+rcId,1);
     });
 })
 function doView(bciId){
	createdetailwindow("查看问题解答","viewBidCommunicationInfoDetail_bidCommunicationInfoSupplier.action?bidCommunicationInfo.bciId="+bciId,1);
  }
 function statusMess(bciId,answerId){
   if(answerId==""){
     showMsgWeb("alert","您的 问题还没有得到回复 ,请耐心等待");
   }else{
     var result=ajaxGeneral("updateBidCommunicationInfoReadStatus_bidCommunicationInfoSupplier.action","bidCommunicationInfo.bciId="+bciId,"text");
     if(result==1){
       $("#td"+bciId).html("<span class=\"label label-success arrowed-in arrowed-in-right\"> 已 读 </span>");
     }
   }
 }
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row-fluid">
								<div class="span12">
							     <div class="btn-toolbar">
							         <button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-hand-up"></i>我要提问</button>
								 </div>
			                  </div>
                           </div>
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="8" class="Content_tab_style_td_head">问题解答</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="25%" nowrap>
												问题内容
											</th>
											<th width="10%" nowrap>
												提问日期
											</th>
											<th width="10%" nowrap>
												回复人
											</th>
											<th width="25%" nowrap>
												回复内容
											</th>
											<th width="10%" nowrap>
												回复日期
											</th>
											<th width="10%" nowrap>
												状态
											</th>
											<th width="5%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${bidCommunicationInfoList}" var="bidCommunicationInfo" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${bidCommunicationInfo.questionContent}</td>	
												<td><fmt:formatDate value="${bidCommunicationInfo.questionDate}" type="both" pattern="yyyy-MM-dd" /></td>
												<td>${bidCommunicationInfo.answerName}</td>	
											    <td>${bidCommunicationInfo.answerContent}</td>	
											    <td><fmt:formatDate value="${bidCommunicationInfo.answerDate}" type="both" pattern="yyyy-MM-dd" /></td>	
											    <td id="td${bidCommunicationInfo.bciId}">
											       <c:choose>
											          <c:when test="${bidCommunicationInfo.readStatus=='0'}">
											            <span class="label label-success arrowed-in arrowed-in-right"> 已 读 </span>
											          </c:when>
											          <c:otherwise>
											            <a href="##" onclick="statusMess(${bidCommunicationInfo.bciId},'${bidCommunicationInfo.answerId}')" title="点击已读"><span class="label label-important arrowed"> 未 读 </span></a>
											          </c:otherwise>
											       </c:choose>
											    </td>
											    <td><button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidCommunicationInfo.bciId})'><i class="icon-white icon-bullhorn"></i></button></td>	
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>