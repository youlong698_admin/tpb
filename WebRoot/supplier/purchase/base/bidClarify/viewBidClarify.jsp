<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>标前澄清</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
function doView(bcId){
	   createdetailwindow("查看标前澄清","viewBidClarifyDetail_bidClarifySupplier.action?bidClarify.bcId="+bcId,1);
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="5" class="Content_tab_style_td_head">标前澄清</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="55%" nowrap>
												澄清标题
											</th>
											<th width="20%" nowrap>
												澄清日期
											</th>
											<th width="10%" nowrap>
												是否已阅
											</th>
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${bidClarifyList}" var="bidClarify" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${bidClarify.clarifyTitle}</td>	
												<td><fmt:formatDate value="${bidClarify.writeDate}" type="both" pattern="yyyy-MM-dd" /></td>
												<td>
												   <c:choose>
												       <c:when test="${bidClarify.isView==0}">
												                           已查看
												       </c:when>
													   <c:otherwise>  
													     <span class="text-muted">未查看</span>
													   </c:otherwise>
												   </c:choose>
												</td>
												<td><button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidClarify.bcId})'><i class="icon-white icon-bullhorn"></i></button></td>		
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>