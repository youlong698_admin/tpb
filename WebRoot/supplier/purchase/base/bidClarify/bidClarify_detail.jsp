<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>标前澄清</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
		<script type="text/javascript">
		
        var api = frameElement.api, W = api.opener, cDG;
		var bcId="${bidClarify.bcId}";
		<c:if test="${message!=null}">
		   window.onload=function(){ 
		   api.get("dialog").window.location.reload();
		   api.close();
		  }
        </c:if>
		$(function(){
		  //更新标签澄清供应商查看表
		   $("#btn-save").click(function(){
		        var action = "saveBidClarifyResponse_bidClarifySupplier.action?bcId="+bcId;
		        document.forms[0].action = action;
		        document.forms[0].submit();
			});
		});
		</script>
	</head>
	<body>
     <form class="defaultForm" id="save_bidClarifyResponse" name="save_bidClarifyResponse" method="post" action="">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
			
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">					 
								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
										<tr>
											<td colspan="4" class="Content_tab_style_td_head">
												标前澄清
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												澄清标题：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidClarify.clarifyTitle}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												澄清内容：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidClarify.clarifyContent}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												附件：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												<c:out value="${bidClarify.attachmentUrl}" escapeXml="false"/>
											</td>
										</tr>

									</table>
									<c:choose>
									<c:when test="${empty bidClarifyResponse.bcrId}">
										<table width="100%" class="table_ys2">
											<tr>
												<td colspan="4" class="Content_tab_style_td_head">
													澄清响应
												</td>
											</tr>
											<tr>
												<td width="15%" class="Content_tab_style1">
													响应备注：
												</td>
												<td width="35%" class="Content_tab_style2" colspan="3">
													 <textarea name="remark" id="remark"  rows="2" class="Content_input_style2" ></textarea>
												</td>
											</tr>
	
										</table>
										 <div class="buttonDiv">
											<button class="btn btn-success"  id="btn-save" type="button"><i class="icon-white icon-ok-sign"></i>回复</button>
											<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
										 </div>	
									 </c:when>
									 <c:otherwise>
									    <table width="100%" class="table_ys2">
											<tr>
												<td colspan="4" class="Content_tab_style_td_head">
													澄清响应
												</td>
											</tr>
											<tr>
												<td width="15%" class="Content_tab_style1">
													响应备注：
												</td>
												<td width="35%" class="Content_tab_style2" colspan="3">
													${bidClarifyResponse.remark}
												</td>
											</tr>
											<tr>
												<td width="15%" class="Content_tab_style1">
													响应时间：
												</td>
												<td width="35%" class="Content_tab_style2" colspan="3">
													<fmt:formatDate value="${bidClarifyResponse.viewDate}" type="date" pattern='yyyy-MM-dd HH:mm:ss'/>
												</td>
											</tr>
	
										</table>
									 </c:otherwise>
									 </c:choose>
								</div>
								  
							
							</div>
							
						</div>
					</div>
                    
				</div>
			</div>
		</div>
		</form>
	</body>
</html>