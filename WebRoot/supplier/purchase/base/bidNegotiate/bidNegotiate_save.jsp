<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>磋商报价</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
        //计算总价
		function accountPrice() 
		{
		    var indexArr=document.getElementsByName("indexArr");
			var amount,price,index,totalPrice=0;
				 for(var i=1;i<indexArr.length;i++){
				    index=indexArr[i].value;
			        price=$("#price_"+index).val();
			        if(price==""){
			           showMsgWeb("alert","报价列有空值");
			           return false;
			        }
			        amount=$("#amount_"+index).val();
			        //showMsg("alert","amount="+amount);
			        //计算小计
			        var  prices=eval(price*amount).toFixed(2)+"";
			        document.getElementById("priceSumStr_"+index).innerHTML=prices;
		             
			       /*计算总合计*/
					totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
				 }
				 document.getElementById("totalPriceStr").innerHTML =totalPrice;
		}
		//执行保存
		function save(){
		  //首先判断截标日期
		  var date = new Date(Date.parse(document.getElementById("negotiateReturnDate").value.replace(/\-/g, "\/")));
			if(date != "" && date != undefined && date != null){
			var sDate = date;
		   	var eDate = new Date();
			if(sDate < eDate){
	     		showMsgWeb("alert","温馨提示：很遗憾，已经超过磋商回复时间，不能磋商！！");
	     		return false;
	  			}
			}
		  //报价加密
		  var indexArr=document.getElementsByName("indexArr");
		  var publicKey=$("#publicKey").val();
		  var encryPrice="";
			var amount,price,index,jsonData,totalPrice=0;
				 for(var i=1;i<indexArr.length;i++){
				    index=indexArr[i].value;
			        price=$("#price_"+index).val();
			        amount=$("#amount_"+index).val();
			        //showMsg("alert","amount="+amount);
			        //计算小计
			        var  prices=eval(price*amount).toFixed(2)+"";
			        document.getElementById("priceSumStr_"+index).innerHTML=prices;
		             
			       /*计算总合计*/
					totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
					
					jsonData={"publicKey":publicKey,"prices":price};
					encryPrice=ajaxGeneral("encryPrice_askBidPriceResponseSupplier.action",jsonData,"json");
					//加密每行报价数据
					$("#encryPrice_"+index).val(encryPrice.data);
				 }
				 document.getElementById("totalPriceStr").innerHTML =totalPrice;
				 
				 jsonData={"publicKey":publicKey,"prices":totalPrice};
				 encryPrice=ajaxGeneral("encryPrice_askBidPriceResponseSupplier.action",jsonData,"json");
				  //加密总价数据
				 $("#encryTotalPrice").val(encryPrice.data);
				
				if(encryPrice.data!=""){
				$.dialog.confirm(" 温馨提示：确定提交报价信息吗? ",function (){
				   document.forms[0].action="saveBidNegotiatePriceRespone_bidNegotiateSupplier.action";
				   document.forms[0].submit();
			    });
			    }else{			    
	     		   showMsgWeb("alert","温馨提示：报价加密失败，请联系客服人员！！");
			    }
		
		}
    </script>
</head>
<body>
<form class="defaultForm" id="supInfo" name="" method="post" action="">
<input type="hidden" id="publicKey" value="${publicKey }"/>
<input type="hidden" id="negotiateReturnDate" value="<fmt:formatDate value="${bidNegotiate.negotiateReturnDate}" pattern="yyyy-MM-dd HH:mm" />"/>
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable">			
    	    <tr>
    	       <td colspan="9" class="Content_tab_style_td_head" align="center">磋商信息</td>
    	    </tr>
    	     <tr>
		    <td colspan="9" style="font-size:14px;"><font color="red"><b>1.报价列不能有空值，如果此物料不供应，请填写0<br>
																	2.磋商报价只能报价一次,请仔细核对后提交。</b></font></td>
		    </tr>
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号
					   <input type="hidden" name="indexArr"  id="indexArr" value="-1"/></th>
				<th width="95px" nowrap>编码</th>
				<th width="10%" nowrap>名称 </th>
				<th width="10%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>期望价格</th>
				<th width="55px" nowrap>磋商报价</th>
				<th width="100px" nowrap>小计</th>
			</tr>
			<c:forEach items="${bpdList}" var="bidPriceDetail" varStatus="status">
				<tr  class="input_ys1">
					<td>
					   ${status.index+1}
					   <input type="hidden" name="indexArr"  id="indexArr" value="${status.index}"/>
					</td>
					<td>
					    ${bidPriceDetail.requiredCollectDetail.buyCode}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.buyName}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.materialType}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.unit}
				    </td>
				    <td align="right">
				       ${bidPriceDetail.requiredCollectDetail.amount}				       
					   <input type="hidden" name="bpdList[${status.index}].amount"  id="amount_${status.index}" value="${bidPriceDetail.requiredCollectDetail.amount}"/>
				    </td>
				    <c:choose>
				      <c:when test="${empty bidPriceDetail.bidPriceNegotiate}">
				        <td></td>
				        <td>
				         ${bidPriceDetail.price }
						 <input type="hidden" name=""  id="price_${status.index}" value="${bidPriceDetail.price }"/>
						 <input type="hidden" name="bpdList[${status.index}].encryPrice"  id="encryPrice_${status.index}" value=""/>
					     <input type="hidden" name="bpdList[${status.index}].bpnId"  id="bpnId_${status.index}" value=""/>
					     <input type="hidden" name="bpdList[${status.index}].supplierId"  id="supplierId_${status.index}" value="${bidPrice.supplierId }"/>
						 <input type="hidden" name="bpdList[${status.index}].rcdId"  id='rcdId_${status.index}' value='${bidPriceDetail.rcdId}'/>
						</td>
						<td>
						   <span id="priceSumStr_${status.index}"><fmt:formatNumber value="${bidPriceDetail.price*bidPriceDetail.requiredCollectDetail.amount }" pattern="0.00"/></span>
						</td>
				      </c:when>
				      <c:otherwise>
				         <td style="color: red"><strong><fmt:formatNumber value="${bidPriceDetail.bidPriceNegotiate.expectPrice}" pattern="0.00"/></strong></td>
				         <td>
						 <input type="text" datatype="*" nullmsg="磋商报价不能为空！" style="width:150px;background-color: yellow" id="price_${status.index}" value="${bidPriceDetail.price }"
									onblur="validateNum(this);"/>
						 <div class="info"><span class="Validform_checktip">磋商报价不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					     <input type="hidden" name="bpdList[${status.index}].encryPrice"  id="encryPrice_${status.index}" value=""/>
					     <input type="hidden" name="bpdList[${status.index}].bpnId"  id="bpnId_${status.index}" value="${bidPriceDetail.bidPriceNegotiate.bpnId }"/>
					     <input type="hidden" name="bpdList[${status.index}].supplierId"  id="supplierId_${status.index}" value="${bidPrice.supplierId }"/>
						 <input type="hidden" name="bpdList[${status.index}].rcdId"  id='rcdId_${status.index}' value='${bidPriceDetail.rcdId}'/>
						</td>
						<td>
						   <span id="priceSumStr_${status.index}"><fmt:formatNumber value="${bidPriceDetail.price*bidPriceDetail.requiredCollectDetail.amount }" pattern="0.00"/></span>
						</td>
				      </c:otherwise>
				    </c:choose>
				</tr>
			</c:forEach>
				<tr>
					<td colspan="2" align="center">总价：</td>
					<td colspan="7">
					    <input type="hidden" name="bidPrice.encryTotalPrice"  id="encryTotalPrice" value=""/>
					    <input type="hidden" name="bidPrice.rcId"  id="rcId" value="${bidPrice.rcId }"/>
					    <input type="hidden" name="bidNegotiate.bnId"  id="bnId" value="${bidNegotiate.bnId }"/>
					    <input type="hidden" name="bidPrice.supplierId"  id="supplierId" value="${bidPrice.supplierId }"/>
						<span id="totalPriceStr"><fmt:formatNumber value="${bidPrice.totalPrice }" pattern="0.00"/>&nbsp;</span>
					</td>
				</tr>		
				 <tr>
					<td colspan="2" align="center">税率（%）：</td>
					<td colspan="7">
						<input type='text' name='bidPrice.taxRate' style='width:150px;background-color: yellow'  id='taxRate' value='${bidPrice.taxRate }'  onblur="validateNum(this);" />
					</td>
				</tr>				  		
        </table>
        <c:if test="${fn:length(bbrList)>0}">
        <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="5" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
    	    </tr>
			 <tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="10%" nowrap>序号</th>
				<th width="20%" nowrap>响应项名称</th>
				<th width="20%" nowrap>响应项要求 </th>
				<th width="20%" nowrap>期望响应 </th>
				<th width="30%" nowrap>我的响应 </th>
			</tr>
			<c:forEach items="${bbrList}" var="bidBusinessResponse" varStatus="status">
			 <tr>
				<td align="center">
				   ${status.index+1}
				</td>
				<td>
				    ${bidBusinessResponse.responseItemName}
				</td>
				<td>
				   ${bidBusinessResponse.responseRequirements}
				</td>
				 <c:choose>
				      <c:when test="${empty bidBusinessResponse.bidResponseNegotiate}">
				        <td></td>
						<td>
						${bidBusinessResponse.myResponse }
						<input type="hidden"  id="myResponse_${status.index}" name="bbrList[${status.index}].myResponse" value="${bidBusinessResponse.myResponse }"/>
						<input type="hidden" name="bbrList[${status.index}].brnId"  id="brnId_${status.index}" value=""/>
					     <input type="hidden" id="responseRequirements_${status.index}" name="bbrList[${status.index}].responseRequirements" value="${bidBusinessResponse.responseRequirements }"/>
						<input type="hidden" id="responseItemName_${status.index}" name="bbrList[${status.index}].responseItemName" value="${bidBusinessResponse.responseItemName }"/>
						<input type="hidden" id="briId_${status.index}" name="bbrList[${status.index}].briId" value="${bidBusinessResponse.briId }"/>
						<input type="hidden" id="supplierId_${status.index}" name="bbrList[${status.index}].supplierId" value="${bidPrice.supplierId }"/>
						<input type="hidden" id="rcId_${status.index}" name="bbrList[${status.index}].rcId" value="${bidPrice.supplierId }"/>
						</td>
					  </c:when>
					  <c:otherwise>
					    <td style="color: red"><strong>${bidBusinessResponse.bidResponseNegotiate.expectResponse}</strong></td>
				        <td>
						<input type="text" datatype="*" nullmsg="我的响应不能为空！" style="width:150px;background-color: yellow" id="myResponse_${status.index}" name="bbrList[${status.index}].myResponse" value="${bidBusinessResponse.myResponse }"/>
						<div class="info"><span class="Validform_checktip">我的响应不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
						<input type="hidden" name="bbrList[${status.index}].brnId"  id="brnId_${status.index}" value="${bidBusinessResponse.bidResponseNegotiate.brnId}"/>
					     <input type="hidden" id="responseRequirements_${status.index}" name="bbrList[${status.index}].responseRequirements" value="${bidBusinessResponse.responseRequirements }"/>
						<input type="hidden" id="responseItemName_${status.index}" name="bbrList[${status.index}].responseItemName" value="${bidBusinessResponse.responseItemName }"/>
						<input type="hidden" id="briId_${status.index}" name="bbrList[${status.index}].briId" value="${bidBusinessResponse.briId }"/>
						<input type="hidden" id="supplierId_${status.index}" name="bbrList[${status.index}].supplierId" value="${bidPrice.supplierId }"/>
						<input type="hidden" id="rcId_${status.index}" name="bbrList[${status.index}].rcId" value="${bidPrice.supplierId }"/>
						</td>
					  </c:otherwise>
				</c:choose>
				</tr>
		   </c:forEach>	  		
        </table>
        </c:if>        
         <div class="buttonDiv">
				<button class="btn btn-warning" id="btn-sum" type="button" onclick="accountPrice();"><i class="icon-white icon-hdd"></i>计算总价</button>
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>提交磋商信息</button>				
		</div>
     </div>  
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			save();
			return false;	
		}
	});
})
</script>
	</body>
</html>