<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>磋商报价</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript"><!--
 var api = frameElement.api, W = api.opener, cDG;
 var rcId="${rcId}";
 function doNegotiate(bnId,date){
     //首先判断磋商回复时间
	  var date = new Date(Date.parse(date.replace(/\-/g, "\/")));
		if(date != "" && date != undefined && date != null){
		var sDate = date;
	   	var eDate = new Date();
		if(sDate < eDate){
     		showMsgWeb("alert","温馨提示：很遗憾，已经超过磋商回复时间，不能进行磋商！！");
     		return false;
  			}
		}
      createdetailwindow("磋商报价","saveBidNegotiateInit_bidNegotiateSupplier.action?bidNegotiate.bnId="+bnId,1);
  }
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="7" class="Content_tab_style_td_head">磋商报价</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="45%" nowrap>
												磋商回复时间
											</th>
											<th width="10%" nowrap>
												是否响应
											</th>
											<th width="20%" nowrap>
												状态
											</th>
											<th width="20%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${list}" var="bidNegotiate" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>	
												<td><fmt:formatDate value="${bidNegotiate.negotiateReturnDate}" type="both" pattern="yyyy-MM-dd HH:mm" /></td>
												<td>
													<c:choose>
													  <c:when test="${bidNegotiate.responseNegotiate=='0'}">已响应</c:when>
													  <c:otherwise>未响应</c:otherwise>
													</c:choose>
												</td>	
											    <td>
                                                  <c:choose>
													  <c:when test="${bidNegotiate.status=='0'}">磋商结束</c:when>
													  <c:otherwise>正在磋商</c:otherwise>
													</c:choose>
                                                </td>	
											    <td><button class='btn btn-mini btn-primary' type="button"  onclick="doNegotiate(${bidNegotiate.bnId},'<fmt:formatDate value="${bidNegotiate.negotiateReturnDate}" type="both" pattern="yyyy-MM-dd HH:mm" />')"><i class="icon-white icon-bullhorn"></i></button></td>	
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>