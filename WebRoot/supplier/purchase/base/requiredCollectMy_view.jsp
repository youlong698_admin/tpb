<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>我的项目</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
<![endif]-->
<script type="text/javascript">
	var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findRequiredCollectMy_requiredCollectSupplier.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            {	
            	className : "ellipsis",
				orderable : false,
            	render: function(data,type, row, meta) {					
					 return "<button class=\"btn btn-mini btn-danger\" type=\"button\"  onclick=\"doApplication('"+row.buyWay+"',"+row.rcId+")\"><i class=\"icon-white icon-list\"></i></button>";
                },         	
            	width : "30px"
            },
            {
            	data: "bidCode",            	
            	width : "25%",
            	orderable : false		
            },
            {	
            	className : "ellipsis",
            	data: "buyRemark",
            	render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	orderable : false			
            },
            {	
            	className : "ellipsis",
            	data: "compName",           	
            	width : "25%",
            	render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	orderable : false			
            },
			{
				data : "buyWay",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.BUYWAY,
            	orderable : false		
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
 	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//组装排序参数
		//默认进入的排序
		 param.orderColumn="de.rcId";
		//组装查询参数
		
		param.bidCode = $("#bidCode").val();
		param.buyRemark = $("#buyRemark").val();
		param.buyWay = $("#buyWay").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	}
 	
};
    function doQuery(){
     _table.draw();
    }
    //项目信息报名
    function doApplication(buyWay,rcId){
       if(buyWay=="00"){
          window.location.href="viewTenderBidListMyMonitor_tenderBidListSupplier.action?rcId=" +rcId;
       }else if(buyWay=="01"){
          window.location.href="viewAskBidListMyMonitor_askBidListSupplier.action?rcId=" +rcId;
       }else if(buyWay=="02"){
          window.location.href="viewBiddingBidListMyMonitor_biddingBidListSupplier.action?rcId=" +rcId;
       }else if(buyWay=="08"){
          window.location.href="saveInitAskBidListApplication_requiredCollectSupplier.action?rcId=" +rcId;
       }
    }
	//重置
	function doReset(){        
		document.forms[0].elements["bidCode"].value	=	"";	
		document.forms[0].elements["buyRemark"].value	=	"";
	}
	
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
										    <i class="icon-white icon-refresh"></i>
										</button>
									
									</div>
								</div>
							</div>				
						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
						
						项目编号：
						<input class="input-medium" type="text" name="bidCode" placeholder="项目编号" id="bidCode" value="" size="15" />
						项目名称：
						<input class="input-medium" type="text" name="buyRemark" placeholder="项目名称" id="buyRemark" size="15" value=""  />
						采购方式：
							<select id="buyWay" class="input" style="width: 100px">
			             		<option value = "">--请选择--</option>
			             		<c:forEach items="${buyWayMap}" var="map">
			             		    <option value="${map.key }">${map.value }</option>
			             		</c:forEach>
			             	</select>
					 	<button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					 	
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>操作</th>
									<th>项目编号</th>
									<th>项目名称</th>
									<th>采购单位</th>
									<th>采购方式</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>