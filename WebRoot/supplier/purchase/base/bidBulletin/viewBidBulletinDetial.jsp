<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>${title }</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
			
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">
						 
								<div class="col-xs-12">
								<table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">
		                            <tr>
		                              <td height="40" align="center" style="border-bottom:1px dotted #CCCCCC"><span style="font-size:14px; color:red"><b>${bidBulletin.bulletinTitle}</b></span></td>
		                            </tr>
		                            <tr>
		                              <td valign="top">　<br />
		                                <table >
		                                  <tr>
		                                    <td ><span style="font-size:14px; line-height:25px">${bidBulletin.bulletinContent}</span></td>
		                                  </tr>
		                                </table></td>
		                            </tr>
		                        <tr>
									<td>
										附件：
										<c:out value="${bidBulletin.attachmentUrl}" escapeXml="false"/>
									</td>
								</tr>
                               </table>
                          
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</body>
</html>