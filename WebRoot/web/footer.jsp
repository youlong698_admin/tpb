<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@ include file="/common/context.jsp"%>
<% SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L); %>
<!------友情链接------>
<div class="link">
  <div class="link_con">
    <p><strong>友情链接 : </strong><a href="http://www.yunqicai.cn/" target="_blank">河南云企采信息科技有限公司 </a><a href="http://www.cebpubservice.com/" target="_blank">中国招投标公共服务平台</a><a href="http://www.ctba.org.cn/" target="_blank">中国招投标协会</a><a href="http://www.chinabidding.com.cn/" target="_blank">中国采购与招标网</a></p>
  </div>
</div>
<!------底部------>
<div class="footer">
     <div class="container clearfloat">
        <div class="footer-main">
            <div class="footer-statistic clearfloat">
		              <span>   <%=systemConfiguration.getSystemCurrentDept() %> 版权所有  </span> 
		               <span> 电话：15515640895</span>
		     </div>
            <div class="footer-statistkefu clearfloat">
		               <span> Copyright© 2018 www.nfc.com.cn All Right Reserved </span> 
		               <span> 技术支持：河南云企采信息科技有限公司  </span>
                
            </div>
            <div class="copyright">
                <span>本网站所有资讯与说明文字仅供参考，如有与本公司相关公告及产品法律文件不符，以相关公告及产品法律文件为准</span>
            </div>
        </div>
    
</div>
</div>
