<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@ include file="/common/context.jsp"%>
<%
  SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L); 
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=systemConfiguration.getSystemCurrentDept() %>电子采购信息管理平台</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/style/supplierLeft.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript">
    function changeFrameHeight(){
        var ifm= document.getElementById("iframeIndex"); 
        var iframeHeight=document.documentElement.clientHeight;
        if(iframeHeight<700) iframeHeight=1000;
        ifm.height=iframeHeight;
    }

    window.onresize=function(){  
         changeFrameHeight();  

    } 
    $(function () {
		$(".sidebar_list li ul li").click(function () {
		$("li[class='active']").removeAttr("class");
		$(this).addClass("active");
		});
	});
</script>
</head>
<body>
<jsp:include page="topOther.jsp"></jsp:include>
<!------主体------>
<div class="main mainSuppIndex">
			<div class="suppIndexLeft">
				<div class="sidebar">
					<ul id="sidebarList" class="sidebar_list">
						<li class="active zhuye">
							<h3>
								<em></em>主页
							</h3>
							<ul>
								<li  class="active">
									<a
										href="javascript:toUrl('viewIndexInfo_supplierMain.action');">主页信息<em></em>
									</a>
								</li>
							</ul>
						</li>
						<c:choose>
							<c:when test="${isRegister=='0'}">
								<li class="active qyxx">
									<h3>
										<em></em>企业信息
									</h3>
									<ul>
										<li  class="">
											<a
												href="javascript:toUrl('viewSupplierBaseInfoDetail_supplierBaseInfoSupplier.action?type=up');">基本信息变更</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewSupplierProductInfo_supplierProductInfoSupplier.action?type=up');">产品信息变更</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewSupplierCertificates_supCertificatesSupplier.action?type=up');">资质信息变更</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewSupplierSelectInfoChange_supplierBaseInfoSupplier.action');">待审核的变更信息</a>
										</li>
									</ul>
								</li>
								<li class="active xmxx">
									<h3>
										<em></em>项目信息
									</h3>
									<ul>
										<li  class="">
											<a
												href="javascript:toUrl('viewRequiredCollectApplication_requiredCollectSupplier.action');">项目报名</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewRequiredCollectMy_requiredCollectSupplier.action');">我的项目</a>
										</li>
									</ul>
								</li>
								<li class="active htxx">
									<h3>
										<em></em>合同信息
									</h3>
									<ul>
										<li class="">
											<a
												href="javascript:toUrl('viewContractInfoApplication_contractInfoSupplier.action');">合同确认</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewContractInfoMy_contractInfoSupplier.action');">我的合同</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewSendContractInfo_contractInfoSupplier.action');">合同发货</a>
										</li>
									</ul>
								</li>
								<li class="active ddxx">
									<h3>
										<em></em>订单信息
									</h3>
									<ul>
										<li class="">
											<a
												href="javascript:toUrl('viewOrderInfoApplication_orderInfoSupplier.action');">订单确认</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewOrderInfoMy_orderInfoSupplier.action');">我的订单</a>
										</li>
										<li  class="">
											<a
												href="javascript:toUrl('viewSendOrderInfo_orderInfoSupplier.action');">订单发货</a>
										</li>
									</ul>
								</li>
							</c:when>
							<c:otherwise>
								<li class="active qyxx">
									<h3>
										<em></em>信息完善
									</h3>
									<ul>
										<li class="">
											<a
												href="javascript:toUrl('viewSupplierSelectInfo_supplierBaseInfoSupplier.action');">供应商信息完善</a>
										</li>
									</ul>
								</li>
							</c:otherwise>
						</c:choose>
						<li class="active zhaq">
							<h3>
								<em></em>账号安全
							</h3>
							<ul>
								<li class="">
									<a
										href="javascript:toUrl('updatePassWordInit_supplierMain.action');">密码修改</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="suppIndexRight">
				<c:choose>
					<c:when test="${not empty url}">
						<iframe name="iframeIndex" id="iframeIndex" height="100%"
							width="913" frameborder="0" src="${url }"
							onLoad="changeFrameHeight()">
					</c:when>
					<c:otherwise>
						<iframe name="iframeIndex" id="iframeIndex" height="100%"
							width="913" frameborder="0"
							src="viewIndexInfo_supplierMain.action"
							onLoad="changeFrameHeight()">
					</c:otherwise>
				</c:choose>
				</iframe>
			</div>
		</div>
<!------主体完------> 

<jsp:include page="footer.jsp"></jsp:include>
<script type="text/javascript">

$(function(){	
	//导航切换
	$(".menuson li").click(function(){
		$(".menuson li.active").removeClass("active")
		$(this).addClass("active");
	});
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('ul').slideUp();
		if($ul.is(':visible')){
			$(this).next('ul').slideUp();
		}else{
			$(this).next('ul').slideDown();
		}
	});
});
    function toUrl(url){
       $('#iframeIndex').attr('src',url);
    }

</script>
</body>
</html>
