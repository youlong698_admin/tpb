<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${webInfo.title}- ${systemConfiguration.systemCurrentDept }-招标采购网</title>
<meta name="keywords" content="${webInfo.keyword}" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
</head>

<body>
<jsp:include page="topOther.jsp"></jsp:include>
<!------主体------><c:choose>
 <c:when test="${webMenu.wmId==1}">
    <c:set var="webUrl" value="about.html"/>
 </c:when>
 <c:when test="${webMenu.wmId==32}">
    <c:set var="webUrl" value="purchase.html"/>
 </c:when>
 <c:otherwise>
    <c:set var="webUrl" value="notice.html"/>
 </c:otherwise>
</c:choose>
<div class="main"> 
  <div class="crumbs">
   您所在的位置：<a href="/">首页</a> &gt; <b><span class="orange">${webMenu.menuName }</span></b>
  </div>
  <!-- three -->
  <div class="divFloor">
  <div class="main_info">
    <div class="content_htm">
            <div class="titlex"><b class="titlex-ico">
                    <a href="${webUrl }">${webMenu.menuName }</a>
            </b>
            </div>
            <h1>${webInfo.title}</h1>
            <div class="YZCstrMainLeftHeader YZCstrMainLeftHeaderBor">
	             <div class="text">
			      ${webInfo.content}
			      </div>
	        </div> 
       </div>
  </div>
  </div>
</div>
<!------主体完------> 

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
