<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ced.sip.common.UserRightInfoUtil"%>
<%@ include file="/common/context.jsp"%>
<%
 int userType=UserRightInfoUtil.getSessionUserType(request);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${systemConfiguration.systemCurrentDept }-招标采购网</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/web/css/responsiveslides.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/index.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/mobile.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/responsiveslides.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
</head>

<body>
<jsp:include page="top.jsp"></jsp:include>
<jsp:useBean id="nowDate" class="java.util.Date"/> 
<c:set var="nd" value="${1000*24*60*60}"/>
<c:set var="nh" value="${1000*60*60}"/>
<c:set var="nm" value="${1000*60}"/>
<!------主体------>
<div class="main"> 
  <!-- one -->
  <div class="divFloor">
  <div class="main_left">
    <div class="main_hot main_2">
      <div class="main_l_text">
       <ul class="panelsTabGroup" >
        <li class="panelsTab panelsTabSelected" id="ggxx_li1" onmouseover="tabClick('ggxx',1)">询价公告</li>
        <li class="panelsTab" id="ggxx_li2" onmouseover="tabClick('ggxx',2)">招标公告</li>
        <li class="panelsTab" id="ggxx_li3" onmouseover="tabClick('ggxx',3)">竞价公告</li>
        <li class="panelsTabMore"><span onclick="more(1)" style="color: #ff5400;font-size: 14px;cursor:pointer;margin-left: 10px;">更多>></span></li>
      </ul>
       <font style="color: red"> 最新</font>采购项目公告信息</div>
      <div class="ggxx" id="ggxx_tab1">
          <ul>
            <c:forEach items="${bbList01}" var="bidBulletin">     
                <c:set var="interval" value="${bidBulletin.returnDate.time-nowDate.time}"/>          
                <li>
                <a href="<%=path %>/bidBulletin/content/${bidBulletin.bbId }.html" title="${bidBulletin.bulletinTitle}">
                 ${bidBulletin.bulletinTitle}
			    </a>
			    <c:choose>
					<c:when test="${interval>0}">
				          <span class="date">还剩
				               <c:choose>
				                    <c:when test="${(interval/nd)>=1}">
					                    <span style="color: red"><fmt:formatNumber value="${interval/nd}" pattern="#0"/></span>天
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
				                    </c:when>
					                <c:otherwise>
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
					                    <span style="color: red"><fmt:formatNumber value="${interval%nd%nh/nm}" pattern="#0"/></span>分
					                </c:otherwise>
				               </c:choose>
				          </span>
				    </c:when>
				    <c:otherwise>
				          <span class="date">已截止</span>
				    </c:otherwise>
			    </c:choose>
			    </li>
            </c:forEach>
          </ul>
        </div>
        <div class="ggxx" id="ggxx_tab2" style="display: none">
          <ul>
            <c:forEach items="${bbList00}" var="bidBulletin">              
                <c:set var="interval" value="${bidBulletin.returnDate.time-nowDate.time}"/>          
                <li>
                <a href="<%=path %>/bidBulletin/content/${bidBulletin.bbId }.html" title="${bidBulletin.bulletinTitle}">
                 ${bidBulletin.bulletinTitle}
			    </a>
			    <c:choose>
					<c:when test="${interval>0}">
				          <span class="date">还剩
				               <c:choose>
				                    <c:when test="${(interval/nd)>=1}">
					                    <span style="color: red"><fmt:formatNumber value="${interval/nd}" pattern="#0"/></span>天
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
				                    </c:when>
					                <c:otherwise>
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
					                    <span style="color: red"><fmt:formatNumber value="${interval%nd%nh/nm}" pattern="#0"/></span>分
					                </c:otherwise>
				               </c:choose>
				          </span>
				    </c:when>
				    <c:otherwise>
				          <span class="date">已截止</span>
				    </c:otherwise>
			    </c:choose>
			   </li>            
            </c:forEach>
          </ul>
        </div>
        <div class="ggxx" id="ggxx_tab3" style="display: none">
          <ul>
            <c:forEach items="${bbList02}" var="bidBulletin">              
                <c:set var="interval" value="${bidBulletin.returnDate.time-nowDate.time}"/>          
                <li>
                <a href="<%=path %>/bidBulletin/content/${bidBulletin.bbId }.html" title="${bidBulletin.bulletinTitle}">
                 ${bidBulletin.bulletinTitle}
			    </a>
			    <c:choose>
					<c:when test="${interval>0}">
				          <span class="date">还剩
				               <c:choose>
				                    <c:when test="${(interval/nd)>=1}">
					                    <span style="color: red"><fmt:formatNumber value="${interval/nd}" pattern="#0"/></span>天
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
				                    </c:when>
					                <c:otherwise>
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
					                    <span style="color: red"><fmt:formatNumber value="${interval%nd%nh/nm}" pattern="#0"/></span>分
					                </c:otherwise>
				               </c:choose>
				          </span>
				    </c:when>
				    <c:otherwise>
				          <span class="date">已截止</span>
				    </c:otherwise>
			    </c:choose>
			    </li>
             </c:forEach>
          </ul>
        </div>
    </div>
  </div>
  <div class="main_right">
  
   <div class="main_r_t_login">
      <h2 class="right_text">我要登录</h2>
        <%if(userType==1){ %>
      <div class="LoginBg Bg2">
          <a href="<%=path %>/viewSupplirtIndex_supplierMain.action" class="LoginBtn02">进入我的主页</a>
      </div>
        <%}else{ %>        
      <div class="LoginBg Bg1">
        <a href="<%=path %>/epp.html" class="LoginBtn02 BtnStyle01">采购单位登录</a>
        <a href="<%=path %>/supplier.html" class="LoginBtn02 BtnStyle02">供应商登录</a>
        <a href="<%=path %>/supplier/reg.html" class="LoginBtn02 BtnStyle03">供应商注册</a>
      </div>
        <%} %>
    </div>
    
    <div class="main_r_t">
      <div id="helpDiv">
      <h2 class="right_text"><span><a href="<%=path %>/help.html" ><img src="<%=path %>/web/images/bg_1.jpg" /></a></span>帮助中心</h2>
      <div class="bd">
      <ul>
          <c:forEach items="${webInfos}" var="wi">
           <li>
                <a href="<%=path %>/help/${wi.wiId }.html" >
                <c:choose>  
			        <c:when test="${fn:length(wi.title) > 16}">  
			            <c:out value="${fn:substring(wi.title, 0, 16)}..." />  
			        </c:when>  
			      <c:otherwise>  
			         <c:out value="${wi.title}" />  
			       </c:otherwise>  
			      </c:choose> 
			     </a></li>
           </c:forEach>
      </ul>
      </div>     
       <script type="text/javascript">
                    $(function () {
                        /*帮助中心*/
                        jQuery("#helpDiv").slide({ mainCell: ".bd ul", autoPlay: true, effect: "topMarquee", vis: 8, interTime: 50, trigger: "click" });
                    });
       </script>
    </div>
    </div>
  </div>
  </div>
  <!-- two -->  
    <div class="divFloor">
       <div class="main_cen">
         <img src="<%=path %>/web/images/cer.jpg"/>
       </div>
       <div class="main_cen_r">
         <img src="<%=path %>/web/images/rg.jpg"/>
       </div>
    </div>
  <!-- three -->
  <div class="divFloor">
  <div class="main_left">
    <div class="main_hot main_2">
      <div class="main_l_text">
       <ul class="panelsTabGroup" >
        <li class="panelsTab panelsTabSelected" id="gsxx_li1" onmouseover="tabClick('gsxx',1)">询价结果公示</li>
        <li class="panelsTab" id="gsxx_li2" onmouseover="tabClick('gsxx',2)">招标结果公示</li>
        <li class="panelsTab" id="gsxx_li3" onmouseover="tabClick('gsxx',3)">竞价结果公示</li>
        <li class="panelsTabMore"><span onclick="more(2)" style="color: #ff5400;font-size: 14px;cursor:pointer;margin-left: 10px;">更多>></span></li>
      </ul>公示信息
      </div>
      <div class="ggxx" id="gsxx_tab1">
          <ul>
            <c:forEach items="${bwList01}" var="bidWinning">              
                <li>
                 <a href="<%=path %>/bidWinning/content/${bidWinning.bwId }.html" title="${bidWinning.winningTitle}">
                 ${bidWinning.winningTitle}
			     </a>
			     <span class="date"><fmt:formatDate value="${bidWinning.publishDate }" pattern="yyyy-MM-dd" /></span>
			     </li>
            </c:forEach>
          </ul>
        </div>
        <div class="ggxx" id="gsxx_tab2" style="display: none">
          <ul>
            <c:forEach items="${bwList00}" var="bidWinning">              
                <li>
                <a href="<%=path %>/bidWinning/content/${bidWinning.bwId }.html" title="${bidWinning.winningTitle}">
                 ${bidWinning.winningTitle}
			     </a>
			     <span class="date"><fmt:formatDate value="${bidWinning.publishDate }" pattern="yyyy-MM-dd" /></span>
			     </li>
            </c:forEach>
          </ul>
        </div>
        <div class="ggxx" id="gsxx_tab3" style="display: none">
          <ul>
            <c:forEach items="${bwList02}" var="bidWinning">              
                <li>
                <a href="<%=path %>/bidWinning/content/${bidWinning.bwId }.html" title="${bidWinning.winningTitle}">
                 ${bidWinning.winningTitle}
			     </a>
			     <span class="date"><fmt:formatDate value="${bidWinning.publishDate }" pattern="yyyy-MM-dd" /></span>
                </li>
            </c:forEach>
          </ul>
        </div>
    </div>
  </div>
  <div class="main_right">
    <div class="main_r_t_notice">
    <div id="notice">
     <h2 class="right_text"><span><a href="<%=path %>/notice.html" ><img src="<%=path %>/web/images/bg_1.jpg" /></a></span>公告信息</h2>
      <div class="bd">
       <ul>
        <c:forEach items="${webInfoNoticeIndex}" var="webInfo">      
         <li>
            <a href="<%=path %>/content/${webInfo.wiId }.html" title="${webInfo.title}">${webInfo.title}</a>
         </li>
        </c:forEach>
      </ul>
      </div>
    </div>
    </div> 
    
    <div class="main_r_t_class">
    <div id="class">
      <h2 class="right_text">按类别查看项目</h2>
      <div class="bd">
       <ul>
        <c:forEach items="${materialKinds}" var="materialKind">      
         <li>
             <a href="javascript:;"   title="${materialKind.mkName}">${materialKind.mkName}</a>
         </li>
        </c:forEach>
      </ul>
      </div>
    </div>
    </div> 
    </div>
  </div>
</div>
<!------主体完------> 

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
