<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.ced.sip.common.UserRightInfoUtil"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@ include file="/common/context.jsp"%>
<%
  String name=UserRightInfoUtil.getSessionUserName(request);
  SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L);
 %>
<!------头部------>
<form action="<%=path %>/search.html" method="post" name="form1">
<div class="header"> 
  <!----顶部---->
  <div class="top">
    <%if(name.equals("")){ %>
    <div class="top_con">
      <p><span><a href="<%=path %>/supplier/reg.html">免费注册</a><a href="<%=path %>/help.html" target="_blank" style="border:none">帮助中心</a></span>欢迎光临<a href="/index.html"><%=systemConfiguration.getSystemCurrentDept() %>采购平台网站</a>！</p>
    </div>
    <%}else{ %>
    <div class="top_con">
      <p><span><a href="tencent://message/?uin=3376235370&Site=www.qicaiyida.com&Menu=yes" target="_blank">联系客服</a><a href="<%=path %>/help.html" target="_blank">帮助中心</a><a href="javascript:exitCyd()" style="border:none;color: red">注销</a></span><font color="red" style="font-weight: bold"><%=name %></font>,您好!欢迎光临<a href="/index.html">平台官网</a>,您的企业采购小帮手！</p>
    </div>
    <%} %>
  </div>
  <!----logo---->
  <div class="header_con">
    <div class="div_logo"><span><a href="#" ><img src="<%=basePath %>/web/images/logo.jpg" /></a></span></div>
    <div class="div_search"><div class="searchbox">
	<div class="bodys">
		<p><input type="text" value="" name="bidInfo" id="bidInfo" class="one" placeholder="请输入项目编号或名称"/><button class="one1" type="button"  onclick="doQuery(1)">搜索</button></p>
	</div>
</div>	
 </div>
  </div>
</div>
</form>
<!------导航------>
<div class="navWeb">
  <div class="nav_con">
    <ul>
      <li><a href="index.html">网站首页</a></li>
      <li><a href="<%=path %>/about.html">关于我们</a></li>
      <li><a href="<%=path %>/bidderNotice.html">招标公告</a></li>
      <li><a href="<%=path %>/purchaseNotice.html">采购公告</a></li>
      <li><a href="<%=path %>/biddingNotice.html">中标公示</a></li>
      <li><a href="<%=path %>/purchaseInfo.html">采购政策</a></li>
      <li><a href="<%=path %>/help.html">帮助中心</a></li>
    </ul>
  </div>
</div>