<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>错误页面-招标采购网</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/web/css/responsiveslides.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/index.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/responsiveslides.min.js"></script>
</head>
  
<body>
   
<jsp:include page="top.jsp"></jsp:include>
<div class="main"> 
<div class="error_body">
  <div class="error">
    
    <h2>非常遗憾，您访问的页面不存在！</h2>

    <div class="reindex"><a href="index.html" target="_parent">返回首页</a></div>
    
    </div>
</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
  </body>
</html>
