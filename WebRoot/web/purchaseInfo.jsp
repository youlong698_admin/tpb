<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${webMenu.menuName } - ${systemConfiguration.systemCurrentDept }-招标采购网</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
</head>

<body>
<jsp:include page="topOther.jsp"></jsp:include>
<c:choose>
 <c:when test="${webMenu.wmId==32}">
    <c:set var="webUrl" value="purchaseInfo.html"/>
 </c:when>
 <c:otherwise>
    <c:set var="webUrl" value="notice.html"/>
 </c:otherwise>
</c:choose>
<!------主体------>
<form id="ProductForm" name="form" method="post" target="_self" action="${webUrl}">
<div class="main"> 
  <div class="crumbs">
   您所在的位置：<a href="/">首页</a> &gt; <b><span class="orange"><a href="${webUrl}">${webMenu.menuName }</a></span></b>
  </div>
  <!-- three -->
  <div class="divFloor">
  <div class="main_info">
    <div class="info_htm_left">
            <div class="titlex"><b class="titlex-ico"><a href="regulations.html">${webMenu.menuName }</a></b></div>
            <ul>
             <c:forEach var="webInfo" items="${webInfos}">             
	             <li>
	               <div class="left">
	                  <h1><a href="<%=path %>/content/${webInfo.wiId}.html" title="${webInfo.title }">${webInfo.title }</a></h1>
	                </div>
	                <div class="time"><fmt:formatDate value="${webInfo.publishDate}" pattern="yyyy-MM-dd HH:mm"/></div>
	                <div class="clear"></div>
	             </li>
             
             </c:forEach>
             
     </ul>
     <jsp:include page="/common/rollpage.jsp"/>   
       </div>
  </div>
  </div>
</div>
</form>
<!------主体完------> 

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
