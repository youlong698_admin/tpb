<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${bidBulletin.bulletinTitle}</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript">
   function doPrice(rcId){
         window.location.href="<%=path%>/bidPrice/"+rcId+".html";
   }
</script>
</head>

<body>
<jsp:include page="topOther.jsp"></jsp:include>
<jsp:useBean id="nowDate" class="java.util.Date"/> 
<c:set var="nd" value="${1000*24*60*60}"/>
<c:set var="nh" value="${1000*60*60}"/>
<c:set var="nm" value="${1000*60}"/>
<!------公告信息------>
<div class="main">
  <div class="crumbs">
       您所在的位置：<a href="/">首页</a> &gt; <b><span class="orange">公告正文</span></b>
  </div>
   <!-- three -->
  <div class="divFloor">
  <div class="bid_main"> 
  <div class="SideRight">
  <div class="titlex"><b class="titlex-ico">
                    <a>公告正文</a>
            </b>
            </div>
       <h1>${bidBulletin.bulletinTitle}</h1>
        <c:set var="interval" value="${bidBulletin.returnDate.time-nowDate.time}"/>
        <div class="detail">
           <div class="detail_left">
             <p>采购项目编号：${bidBulletin.bidCode }</p>
             <c:if test="${bidBulletin.returnDate.time>nowDate.time}">
                <p>距离报价截止还剩：<span style="font-size: 22px;color: #ff6600;font-weight: :bold;">
				               <c:choose>
				                    <c:when test="${(interval/nd)>=1}">
					                    <span style="color: red"><fmt:formatNumber value="${interval/nd}" pattern="#0"/></span>天
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
				                    </c:when>
					                <c:otherwise>
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
					                    <span style="color: red"><fmt:formatNumber value="${interval%nd%nh/nm}" pattern="#0"/></span>分
					                </c:otherwise>
				               </c:choose>
				          </span>
				 </p>
             </c:if>
           </div>
           <div class="detail_right">
           <c:choose>
             <c:when test="${bidBulletin.returnDate.time>nowDate.time}">
                 <a href="javascript:doPrice(${bidBulletin.rcId })" class="priceBtn price01">立 即 报 价</a>
             </c:when>
             <c:otherwise>
                 <a href="javascript:;" class="priceBtn price02">已截止</a>
             </c:otherwise>
           </c:choose>
           </div>         
        </div> 
         <div class="YZCstrMainLeftHeader YZCstrMainLeftHeaderBor">
             <div class="text">
		      ${bidBulletin.bulletinContent}
		      </div>
        </div>
        <div style="padding: 20px;">
          <strong>附件下载：</strong>
          <c:choose>
            <c:when test="${empty bidBulletin.attachmentUrl}">无附件下载</c:when>
            <c:otherwise><c:out value="${bidBulletin.attachmentUrl}" escapeXml="false"/></c:otherwise>
          </c:choose>           
        </div>
    </div>
    
     <div style="margin: 10px 0 10px 0;padding:20px 20px; border: 1px #ccc solid;background: #fff;width: 1057px;">
       <div><strong>如何操作：</strong></div>
		<c:choose>
			 <c:when test="${bidBulletin.buyWay=='00'}">
			      <img src="<%=path %>/web/images/tb.jpg"/>
			 </c:when>
			 <c:when test="${bidBulletin.buyWay=='01'}">
			      <img src="<%=path %>/web/images/xj.jpg"/>
			 </c:when>
			 <c:otherwise>
			     <img src="<%=path %>/web/images/jj.jpg"/>
			</c:otherwise>
		</c:choose>
    </div>
  </div>
</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
