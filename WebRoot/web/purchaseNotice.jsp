<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>采购公告 - ${systemConfiguration.systemCurrentDept }-招标采购网</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
</head>

<body>
<jsp:include page="topOther.jsp"></jsp:include>
<jsp:useBean id="nowDate" class="java.util.Date"/> 
<c:set var="nd" value="${1000*24*60*60}"/>
<c:set var="nh" value="${1000*60*60}"/>
<c:set var="nm" value="${1000*60}"/>
<!------主体------>
<div class="main"> 
 <div class="crumbs">
   您所在的位置：<a href="/">首页</a> &gt; <b><span class="orange"><a href="purchaseNotice.html">采购公告</a></span></b>
  </div>
  <!--one -->
  <div class="divFloor">
  <div class="main_bidder">
    <div class="main_ny main_2">
    
       <form id="purchaseNoticeForm" name="form" method="post" target="_self" action="purchaseNotice.html">
      <div class="titlex"><b class="titlex-ico">采购公告</b>
      </div>
      <div class="ggxx_bidder" id="gsxx_tab1">
          <ul>
            <c:forEach items="${bbList}" var="bidBulletin">     
                <c:set var="interval" value="${bidBulletin.returnDate.time-nowDate.time}"/>          
                <li>
                <a href="<%=path %>/bidBulletin/content/${bidBulletin.bbId }.html" >
                 ${bidBulletin.bulletinTitle}
			    </a>
			    <c:choose>
					<c:when test="${interval>0}">
				          <span class="date">还剩
				               <c:choose>
				                    <c:when test="${(interval/nd)>=1}">
					                    <span style="color: red"><fmt:formatNumber value="${interval/nd}" pattern="#0"/></span>天
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
				                    </c:when>
					                <c:otherwise>
									    <span style="color: red"><fmt:formatNumber value="${interval%nd/nh}" pattern="#0"/></span>小时
					                    <span style="color: red"><fmt:formatNumber value="${interval%nd%nh/nm}" pattern="#0"/></span>分
					                </c:otherwise>
				               </c:choose>
				          </span>
				    </c:when>
				    <c:otherwise>
				          <span class="date">已截止</span>
				    </c:otherwise>
			    </c:choose>
			    </li>
            </c:forEach>
          </ul>
        </div>
        <jsp:include page="/common/rollpage.jsp"/>
       </form>
    </div>
  </div>
    </div>
  
</div>
<!------主体完------> 

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
