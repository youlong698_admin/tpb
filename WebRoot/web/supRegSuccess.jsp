<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>注册成功页面</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%= path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<link href="<%= path %>/web/css/reg.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/web/css/responsiveslides.css" rel="stylesheet" type="text/css" />
<script src="<%= path %>/common/jQuery/jquery-1.8.3.min.js" type="text/javascript" ></script>
<script type="text/javascript" src="<%=path %>/web/js/index.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/responsiveslides.min.js"></script>
</head>
  
  <body>
   
<jsp:include page="topOther.jsp"></jsp:include>
<div class="main"> 
<div class="registerSuccss_body">
  <div class="regSuccess">
    
    <h2>恭喜您，注册成功！</h2>
    <div class="reindex"><a href="<%= path %>/web/supplier_login.jsp" target="_parent">立即登录</a></div>
    
    </div>
</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
  </body>
</html>
