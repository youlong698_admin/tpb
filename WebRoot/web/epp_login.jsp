<%@ page language="java"  pageEncoding="UTF-8"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@ include file="/common/context.jsp"%>
<%
  SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L); 
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=systemConfiguration.getSystemCurrentDept() %>电子采购信息管理平台</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/style/login.css" rel="stylesheet" rev="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<%=basePath %>/common/jQuery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=basePath %>/common/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>
<script type="text/javascript" src="<%=basePath%>/common/script/cookie.js"></script>
<script language="javaScript">   

$(function(){
	 //分析cookie值，显示上次的登陆信息
	var userNameValue = getCookieValue("userName");
	if(userNameValue!=""){
	    document.getElementById("username").value = userNameValue;
	    $("#ifRem").attr("checked","checked");	    
	 }
	var passwordValue = getCookieValue("password");
	if(passwordValue!=""){
	    document.getElementById("password").value = passwordValue;
	    $("#ifPwd").attr("checked","checked");
	  }
			     	
	$(".i-text").focus(function(){
		$(this).addClass('h-light');
	});

	$(".i-text").focusout(function(){
		$(this).removeClass('h-light');
	});

	$("#username").focus(function(){
		 var username = $(this).val();
		 if(username=='请输入用户名'){
		 	$(this).val('');
		 }
		});

		$("#username").focusout(function(){
		 var username = $(this).val();
		 if(username==''){
		 	$(this).val('请输入用户名');
		 }
	});


	$("#validateCode").focus(function(){
	 var username = $(this).val();
	 if(username=='请输入验证码'){
	 	$(this).val('');
	 }
	});

	$("#validateCode").focusout(function(){
	 var username = $(this).val();
	 if(username==''){
		 $(this).val('请输入验证码');
	 }
	});

	$("#ifExpert").click(function(){
		var ack=$(this).attr("checked");
		/* 评委登录时  Ushine,2014-11-22*/
		if(ack=="checked"){
			$("#frm").attr("action","verifyBidsJudgerLogin_verify.action");
			
		}else{
		     $("#frm").attr("action","verifyLoginWithAD_verify.action");//域用户登录   记得改动form里面的action
			//$("#frm").attr("action","verifyLogin_verify.action");//本地用户登录
					
		}
	});

	$(".registerform").Validform({
		tiptype:function(msg,o,cssctl){
			var objtip=$(".error-box");
			cssctl(objtip,o.type);
			objtip.text(msg);
		}
	});
	
});

	function checkedIfRem(){
		 if(!document.getElementById("ifRem").checked){
		    setCookie("userName",document.getElementById("username").value,24,"/");
		    }else{		    
		    deleteCookie("userName","/");
		    }
	}
	function checkedIfPwd(){
	     if(document.getElementById("ifPwd").checked){
		    setCookie("password",document.getElementById("password").value,24,"/");
		    }else{
		    deleteCookie("password","/");
		    }
	}
	
</script>
</head>

<body >
<div class="header">
  <h1 class="headerLogo"><img alt="logo" src="<%=basePath %>/images/logo.jpg"/></h1>
  <span class="headerNav">
		<a class="b-no-ln" href="<%=basePath %>/index.html">门户网站</a> | <a class="b-no-ln" href="#">帮助</a>
  </span>
</div>

<div class="banner">
  <div class="login-aside">
  <div id="o-box-up"></div>
  <div id="o-box-down"  style="table-layout:fixed;">
   <div class="error-box"><font color="red">${errorMassge }</font></div>
   
   <form id="frm" method="post" class="registerform" action="verifyLogin_verify.action">
   <div class="fm-item">
	   <label for="logonId" class="form-label">用户名：</label>
	   <input type="text" value="请输入用户名" maxlength="100" id="username" tabindex="1"  name="ur.j_username" class="i-text"   datatype="s4-18" nullmsg="请输入用户名！" errormsg="用户名至少5个字符,最多18个字符！" autocomplete="off"/>    
       <input type="checkbox" name="ifRem" id="ifRem"  onclick="checkedIfRem()"  value="0"/><label for="checkbox"></label>记住账号
  </div>
  
  <div class="fm-item">
	   <label for="logonId" class="form-label">密码：</label>
	   <input type="password" value="" maxlength="100" id="password" name="ur.j_password" tabindex="2"  class="i-text" datatype="*6-16" nullmsg="请输入密码！" errormsg="密码范围在6~16位之间！" />    
       <input type="checkbox" name="ifPwd" id="ifPwd" onclick="checkedIfPwd()" value="0"/><label for="checkbox2"></label>记住密码
  </div>
  
  <div class="fm-item pos-r">
	   <label for="logonId" class="form-label">验证码：</label>
	   <input id="validateCode" value="请输入验证码" name="validateCode" type="text" tabindex="3" class="i-text yzm" maxlength="4" nullmsg="请输入验证码！" onkeydown="if(event.keyCode == 13){this.focus();}" autocomplete="off"/>
	   	<img id="verifyCodeImg" name="verifyCodeImg" onclick="this.src=this.src+'?'+Math.random();" src="<%=basePath %>/servlet/imageRandServlet" style="vertical-align:middle;width:110px;height:34px;" title="看不清?点击我试试"></img>
	
  </div>
  <div class="fm-item2" >
	    <img src="<%=path %>/images/expert.jpg" width="25" height="25" />&nbsp;&nbsp;
      	<input type="checkbox" name="ifExpert" id="ifExpert"/><label for="checkbox2"></label>我是评委
      	<div class="ui-form-explain"></div>
  </div>
  
  <div class="fm-item">
	   <label for="logonId" class="form-label"></label>
	   <input type="submit" value="" tabindex="4" id="send-btn" class="btn-login" /> 
       <div class="ui-form-explain"></div>
  </div>
  
  </form>
  
  </div>

</div>

  
   </div>

<div class="banner-shadow"></div>

<div class="footer">
   <p>Copyright©2016 河南云企采信息科技有限公司</p>
</div>
</body>
</html>
