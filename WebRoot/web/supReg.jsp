<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>供应商注册</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/web/css/responsiveslides.css" rel="stylesheet" type="text/css" />
<script src="<%= path %>/common/jQuery/jquery-1.8.3.min.js" type="text/javascript" ></script>
<script type="text/javascript" src="<%=path %>/web/js/index.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/responsiveslides.min.js"></script>
<link href="<%= path %>/web/css/reg.css" rel="stylesheet" type="text/css" />
<link href="<%= path %>/common/Validform_v5.3.2/css/Validform.css" rel="stylesheet" type="text/css" />
<script src="<%=path%>/common/lhgDialog/lhgdialog.js?skin=bootstrap2" type="text/javascript"></script>
<script src="<%= path %>/common/script/common.js" type="text/javascript" ></script>
<script src="<%=path%>/common/Validform_v5.3.2/js/Validform_v5.3.2.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/date/WdatePicker.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
<script src="<%=path%>/common/script/supplier/supBaseInfo.js" type="text/javascript" ></script> 
<style type="text/css">
	        .leftDiv{
	           float: left;
	           width: 320px;
	        }
	       .leftMDiv{
	           float: left;
	           width: 180px;
	        } 
	        .rightDsp{
	          float:left;
	          color:red;
	        }
	         #infoDspLoginName{
	          float:left;
	          color:red;
	        }
			#tips {
				float: left;
				margin-top: 5px;
			}
			
			#tips span {
				float: left;
				width: 60px;
				height: 28px;
				color: #fff;
				overflow: hidden;
				background: #ccc;
				margin-right: 2px;
				line-height: 20px;
				text-align: center;
			}
			
			#tips.s1 .active {
				background: #f30;
			}
			
			#tips.s2 .active {
				background: #fc0;
			}
			
			#tips.s3 .active {
				background: #090;
			}
		</style> 
	<script type="text/javascript">
		$(function (){		 
	   var oTips = document.getElementById("tips");
	   var oInput = document.getElementById("supplierLoginPwd");
	   var aSpan = oTips.getElementsByTagName("span");
	   var aStr = ["弱", "中", "强"];
	   var i = 0;
	
	   oInput.onkeyup = oInput.onfocus =  function() {
	       var index = checkStrong(this.value);
	       //this.className = index ? "correct" : "error";
	       oTips.className = "s" + index;
	       for ( i = 0; i < aSpan.length; i++)
	           aSpan[i].className = aSpan[i].innerHTML = "";
	           if(index==3){
	               index && (aSpan[index - 1].className = "active", aSpan[index - 1].innerHTML = aStr[index - 1]);
	               index && (aSpan[index - 2].className = "active", aSpan[index - 2].innerHTML = aStr[index - 2]);
	               index && (aSpan[index - 3].className = "active", aSpan[index - 3].innerHTML = aStr[index - 3]);
	           }else if(index==2){
	               index && (aSpan[index - 1].className = "active", aSpan[index - 1].innerHTML = aStr[index - 1]);
	               index && (aSpan[index - 2].className = "active", aSpan[index - 2].innerHTML = aStr[index - 2]);
	           }else if(index==1){
	               index && (aSpan[index - 1].className = "active", aSpan[index - 1].innerHTML = aStr[index - 1]);
	           }
	   };
	});
	
	//检测密码强度
	function checkStrong(sValue) {
	    var modes = 0;
	    if (sValue.length < 6)
	        return modes;
	    if (/\d/.test(sValue))
	        modes++;
	    //数字
	    if (/[a-z]/.test(sValue))
	        modes++;
	    //小写
	    if (/[A-Z]/.test(sValue))
	        modes++;
	    //大写
	    if (/\W/.test(sValue))
	        modes++;
	    //特殊字符
	    switch (modes) {
	        case 1:
	            return 1;
	            break;
	        case 2:
	            return 2;
	        case 3:
	        	return 3;
	        case 4:
	            return 3;
	            break;
	    }
	}
	
	</script>
</head>
 
<body>
<jsp:include page="topOther.jsp"></jsp:include>
<div class="main"> 
<div class="register_body">
<form class="defaultForm" name="supInfo" id="supInfo" method="post" action="">
<input type="hidden" id="proKindIds" name="supplierInfo.prodCodes" value="" />
<input type="hidden" id="returnVals" name="returnVals" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>
    	<div class="reContain">
    	<div class="comWap">
    	<div class="fl reSide">
    	<table class="table_ys1">
        	<tr>
          		<td  colspan="2" class="supReg_td_head">注册供应商信息</td>
        	</tr>
        	<tr>
				<td  class="Content_tab_style1">注册代码证类型：</td>
				<td  class="Content_tab_style2">
					<select datatype="*" nullmsg="注册代码证类型不能为空！" 
						   id			 =	"registrationType"
	                       name          =  "supplierInfo.registrationType" onchange="doSelect(this)">
	                 <c:forEach var="map" items="${registrationTypeMap}">
	                   <option value="${map.key }">${map.value }</option>
	                 </c:forEach>
	                 </select>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">注册代码证类型不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
			<tr id="registrationTypeTr">
				<td  class="Content_tab_style1" id="registrationTypeTd">统一社会信用代码证：</td>
				<td  class="Content_tab_style2">
					<div id="passDiv" class="leftDiv"><input type="text" datatype="*" nullmsg="统一社会信用代码证不能为空"  maxlength="20" id="orgCode" name="supplierInfo.orgCode"  value="" onblur="checkOutPageValueOrg(this);"/>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip" id="registrationTypeDiv">统一社会信用代码证不能为空</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				    </div>
					<div id="infoDspOrg" class="rightDsp"></div>
				</td>
			</tr>
    		<tr>
				<td  class="Content_tab_style1">公司名称：</td>
				<td  class="Content_tab_style2">
					<div id="passDiv" class="leftDiv"><input type="text" datatype="*" nullmsg="公司名称不能为空！" placeholder="营业执照公司全称"  id="supplierName" name="supplierInfo.supplierName" value="" onblur="checkOutPageValueSupp(this);"/>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">公司名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</div>
					<div id="infoDsp" class="rightDsp"></div>
				</td>
			</tr>
            <tr>
				<td  class="Content_tab_style1">用户名：</td>
				<td  class="Content_tab_style2">
					<div id="passDiv" class="leftDiv">
					<input type="text" datatype="/^[a-zA-Z0-9]{4,18}$/" nullmsg="用户名不能为空！" errormsg="4~18位字母或数字组成" placeholder="4~18位字母或数字组成"  id="supplierLoginName" name="supplierInfo.supplierLoginName" value="" onblur="checkOutPageValue(this);"/>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">用户名不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</div>
					<div id="infoDspLoginName" class="rightDsp"></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">密码：</td>
				<td  class="Content_tab_style2">
				     <div id="passDiv">
						<input type="password" datatype="*6-18" nullmsg="密码不能为空！" errormsg="6-18位字母、数字或者特殊字符！" placeholder="6-18位字母、数字或者特殊字符！"  id="supplierLoginPwd" name="supplierInfo.supplierLoginPwd" value=""/>&nbsp;<font color="#ff0000">*</font><div id="infoDsp"></div>
						<div class="info"><span class="Validform_checktip">6-18位字母、数字或者特殊字符！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				    </div>
				    <div id="tips">
						<span></span><span></span><span></span>
					</div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">确认密码：</td>
				<td width="70%" class="Content_tab_style2">
					<input type="password" datatype="*6-18" nullmsg="确认密码不能为空！"  errormsg="两次输入的密码不一致！"  id="supplierLoginConfirmPwd" name="supplierLoginConfirmPwd" recheck="supplierInfo.supplierLoginPwd" value=""/>&nbsp;<font color="#ff0000">*</font><div id="infoDsp"></div>
					<div class="info"><span class="Validform_checktip">两次输入的密码不一致！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">公司简称：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.supplierNameSimple" value="" />
				</td>
			</tr>
			<tr>
			    <td  class="Content_tab_style1">成立日期：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.createDate" value="<s:date name="supplierInfo.createDate" format="yyyy-MM-dd" />" class="Wdate" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
				</td>
			</tr>

			<tr>
				<td  class="Content_tab_style1">企业性质：</td>
				<td  class="Content_tab_style2">
					<select datatype="*" nullmsg="企业性质不能为空！" 
						   id			 =	"supplierType"
	                       name          =  "supplierInfo.supplierType" >
	                   <c:forEach var="map" items="${supplierTypeMap}">
	                   <option value="${map.key }">${map.value }</option>
	                 </c:forEach></select>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">企业性质不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">企业法人：</td>
				<td  class="Content_tab_style2">
					<input type="text"  maxlength="15" id="" name="supplierInfo.legalPerson" value="" />
				</td>
			</tr>

			<tr>	
				<td  class="Content_tab_style1">注册资金：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="注册资金不能为空！"  id="registerFunds" name="supplierInfo.registerFunds" value="" />&nbsp;<font color="#ff0000">*</font>万元
					<div class="info"><span class="Validform_checktip">注册资金不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">地址：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.supplierAddress" value="" />
					&nbsp;
				</td>
			</tr>

			<tr>
				<td  class="Content_tab_style1">公司电话：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="/^((0\d{2,3})-)(\d{7,8})?$/" errormsg="电话格式不正确！格式为：0311-68030978" placeholder="带区号加“-”如0311-68030978" id="supplierPhone" name="supplierInfo.supplierPhone" maxlength="30" value="" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">公司电话不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">公司传真：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.supplierFax" value=""  maxlength="12"/>
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">联系人：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="联系人不能为空！" maxlength="10" id="contactPerson" name="supplierInfo.contactPerson" value="" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>

			
			<tr>
				<td  class="Content_tab_style1">联系人手机：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="m" nullmsg="联系手机不能为空" errormsg="联系人手机号码格式不正确！" placeholder="手机号用于接收平台短信"  id="mobilePhone" name="supplierInfo.mobilePhone" maxlength="11" value="" />&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">联系手机不能为空或联系人手机号码格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">联系人E-mail：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="/^\s*$/ |e" nullmsg="联系人E-mail不能为空" errormsg="E-mail格式不正确！"id="" placeholder="E-mail用于接收平台邮件" name="supplierInfo.contactEmail" value="" maxlength="100"/>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">联系人E-mail不能为空或E-mail格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">所属部门：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.deptName" value="" maxlength="100"/>
					</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">现任职务：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="supplierInfo.duty" value="" maxlength="100"/>
					</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">主营产品：</td>
				<td  class="Content_tab_style2">
					<textarea id="proKindNames" name="proKindNames" style="width: 470px" rows="3" class="Content_input_style2" readonly datatype="*" nullmsg="主营产品不能为空！"></textarea>
						&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择主营产品" onclick='selectProKindWeb()'/>
					<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">主营产品不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">经营范围：</td>
				<td  class="Content_tab_style2">
				   <textarea id="introduce" name="supplierInfo.scopeBusiness" datatype="s0-1000|/^\s*$/ " errormsg="经营范围限500个字！" style="width: 470px" rows="5" class="Content_input_style2" ></textarea>
					<div class="info"><span class="Validform_checktip">经营范围限500个字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">企业简介：</td>
				<td  class="Content_tab_style2">
				    <textarea id="introduce" name="supplierInfo.introduce" datatype="s0-1000 |/^\s*$/ " errormsg="企业简介限500个字！" style="width: 470px" rows="5" class="Content_input_style2" ></textarea>
					<div class="info"><span class="Validform_checktip">企业简介限500个字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
        </table>
	        <div class="buttonDiv readBook">
	                    <input id="isAgreed" type="checkbox">我已阅读并同意<a style="cursor: pointer;" onclick="openWindow(1)">《用户注册协议》</a> 
	        </div>
	        <div class="buttonDiv reBtn">
					<a class="resiteBtn" style="cursor: pointer;" id="btn-save">立即注册</a>
					
			</div>
		</div>
		<div class="fl reMain">
                <h2>用户注册帮助</h2>
                <ul>
                    <li><i>1</i><a href="<%=path %>/help/21.html" target="_blank">用户注册帮助</a></li>
                    <li><i>2</i><a href="<%=path %>/help/24.html" target="_blank">会员注册是否收费？</a></li>
                    <li><i>3</i><a href="<%=path %>/help/25.html" target="_blank">注册完成后，大约多长时间可以通过审核？</a></li>
                    <li class="more"><a href="<%=path %>/help.html" target="_blank">更多帮助信息 &gt;&gt;</a></li>
                </ul>
            </div>
		</div>
		</div>
</form>
</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象
			saveReg();
			return false;	
		}
	});
})
</script>
</body></html>