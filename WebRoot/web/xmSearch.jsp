<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>项目搜索 - ${systemConfiguration.systemCurrentDept }-招标采购网</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
<script src="<%=path%>/common/script/date/WdatePicker.js" type="text/javascript"></script>
<script type="text/javascript">
var path="<%=path%>/";
</script>
</head>

<body>
<jsp:include page="topOther.jsp"></jsp:include>
<jsp:useBean id="nowDate" class="java.util.Date"/> 
<!------主体------>
<div class="main"> 
  <!-- three -->
  <div class="divFloor">
  <div class="main_search">    
    <form id="xmForm" name="form" method="post" target="_self" action="search.html">
    <div class="xmSearchDiv">
         <input type="hidden" id="bidderType" name="bidderType" value="${bidderType }" />
         <input type="hidden" id="noticeType" name="noticeType" value="${noticeType }" />
         <input type="hidden" id="publishType" name="publishType" value="${publishType }" />
        <div class="options" id="city_1">
                    <ul class="option-list biddercate">
                        <li class="option-cate">招标类型：</li>
                        <li class="option-li"><a target="_self" href="javascript:;" <c:choose><c:when test="${bidderType==0 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="0">全部</a></li>
                        <li class="option-li"><a target="_self" href="javascript:;"  <c:choose><c:when test="${bidderType==1 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="1">招标</a></li>
                        <li class="option-li"><a target="_self" href="javascript:;"  <c:choose><c:when test="${bidderType==2 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="2">询价</a></li>
                        <li class="option-li"><a target="_self" href="javascript:;"  <c:choose><c:when test="${bidderType==3 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="3">竞价</a></li>
                    </ul>
                    <ul class="option-list noticecate">
                        <li class="option-cate">公告类型：</li>
                        <li class="option-li"><a href="javascript:;" target="_self" <c:choose><c:when test="${noticeType==0 }"> class="option-btn nbtn option-btn-cur"</c:when><c:otherwise> class="option-btn nbtn"</c:otherwise></c:choose> val="0">全部</a></li>
                        <li class="option-li"><a href="javascript:;" target="_self" <c:choose><c:when test="${noticeType==1 }"> class="option-btn nbtn option-btn-cur"</c:when><c:otherwise> class="option-btn nbtn"</c:otherwise></c:choose> val="1">招标公告</a></li>
                        <li class="option-li"><a href="javascript:;" target="_self" <c:choose><c:when test="${noticeType==2 }"> class="option-btn nbtn option-btn-cur"</c:when><c:otherwise> class="option-btn nbtn"</c:otherwise></c:choose> val="2">结果公示</a></li>                        
                    </ul>
                    <ul class="option-list rangedata">
                        <li class="option-cate">发布时间：</li>
                        <li class="option-li"><a href="javascript:;" target="_self"  <c:choose><c:when test="${publishType==0 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="0">全部</a></li>
                        <li class="option-li"><a href="javascript:;" target="_self"  <c:choose><c:when test="${publishType==1 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="1">近三天</a></li>
                        <li class="option-li"><a href="javascript:;" target="_self"  <c:choose><c:when test="${publishType==2 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="2">近一周</a></li>
                        <li class="option-li"><a href="javascript:;" target="_self"  <c:choose><c:when test="${publishType==3 }"> class="option-btn option-btn-cur"</c:when><c:otherwise> class="option-btn"</c:otherwise></c:choose> val="3">近一个月</a></li>
                        <li class="option-li"><a  <c:choose><c:when test="${publishType==4 }"> class="option-range-btn option-btn-cur"</c:when><c:otherwise> class="option-range-btn"</c:otherwise></c:choose> val="4">自定义</a></li>
                        <li class="option-li customdate" style="display:none;">
                            <input id="startDate" name="startDate" class="Wdate" style="height: 22px; width:100px; border: 1px #ddd solid;padding-left: 12px;" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')||\'2030-10-01\'}'})" value="${startDate }" type="text"/>
                                                                            至
                            <input id="endDate" name="endDate" class="Wdate" style="height: 22px; width:100px; border: 1px #ddd solid; padding-left: 12px;" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'2030-10-01'})" value="${endDate }" type="text"/>
                         </li>
                     </ul>
                    <ul class="option-list">
                        <li class="option-li">项目名称：<input placeholder="项目名称或项目编号" class="input-info" name="bidInfo" value="${bidInfo }" type="text"/></li>
                        <li class="option-li"><a onclick="Query()" class="enterprise-search-btn">搜索</a></li>                      
                    </ul>
                    
                </div>       
    </div>
    <div class="xmSearchContent">
          <ul>
            <c:forEach items="${bbwList}" var="bidBulletinWinning">
                <c:choose>
                   <c:when test="${bidBulletinWinning.type==0}">
                     <c:set var="interval" value="${bidBulletinWinning.returnDate.time-nowDate.time}"/> 
                     <li>
		                <div class="xmSearchLeft">
			                <a href="<%=path %>/bidBulletin/content/${bidBulletinWinning.bbwId }.html" class="xmSearch_title" title="${bidBulletinWinning.title}">
			                 <c:choose>
			                    <c:when test="${bidBulletinWinning.buyWay=='00'}"><span class="bb00">【招标】</span></c:when>
			                    <c:when test="${bidBulletinWinning.buyWay=='01'}"><span class="bb01">【询价】</span></c:when>
			                    <c:otherwise><span class="bb02">【竞价】</span></c:otherwise>
			                 </c:choose>
			                  ${bidBulletinWinning.title}
						    </a>
						    <br/>
						    <span class="xmPro"><fmt:formatDate value="${bidBulletinWinning.publishDate }" pattern="yyyy-MM-dd" /></span>
					    </div>
					    <div class="xmSearchRight">
					    <c:if test="${bidBulletinWinning.returnDate.time>nowDate.time}">
					    <span class="xmDate_bidder">还剩[<span style="color: red"><fmt:formatNumber value="${interval/1000/60/60/24}" pattern="#0"/></span>]天</span>
					    </c:if>
					    <c:if test="${bidBulletinWinning.returnDate.time<=nowDate.time}">
					    <span class="xmDate_bidder">已截止</span>
					    </c:if>
					    </div>
			         </li>
                   </c:when>
                   <c:otherwise>
                     <li>
		                <div class="xmSearchLeft">
			                <a href="<%=path %>/bidWinning/content/${bidBulletinWinning.bbwId }.html" class="xmSearch_title" title="${bidBulletinWinning.title}">
			                 <c:choose>
			                    <c:when test="${bidBulletinWinning.buyWay=='00'}"><span class="bb00">【招标】</span></c:when>
			                    <c:when test="${bidBulletinWinning.buyWay=='01'}"><span class="bb01">【询价】</span></c:when>
			                    <c:otherwise><span class="bb02">【竞价】</span></c:otherwise>
			                 </c:choose>
			                  ${bidBulletinWinning.title}
						    </a>
						    <br/>
						    <span class="xmPro"><fmt:formatDate value="${bidBulletinWinning.publishDate }" pattern="yyyy-MM-dd" /></span>					    </div>
					    <div class="xmSearchRight">
					     <span class="xmDate_bidder"></span>
					    </div>
			         </li>
                   </c:otherwise>
                </c:choose>              
                
            </c:forEach>
          </ul>
          <jsp:include page="/common/rollpage.jsp"/>
     </div>
     </form>
  </div>
  </div>
  <div class="main_right" style="margin-top: 10px;">
    <div>
         <img src="<%=path %>/web/images/rg.jpg"/>
    </div>
    <div id="supplier" class="win-bid-con">
      <h2 class="right_text">最新中标信息</h2>
      <div class="marquee-win">
        <div class="hd"></div>
	    <div class="bd">
            <div class="tempWrap" style="overflow:hidden;height:466px">
                <ul class="win-bid-list">
                 <c:forEach items="${allList}" var="object">      
		          <li class="win-bid-li" style="height: 35px;">
		                 <a href="javascript:;"   title="恭贺${object[0].supplierName }中标${object[2] }">
                         <span class="win-bid-level win-level-Standard"></span>
                            <span class="win-bid-company">恭贺${object[0].supplierName }中标${object[2] }</span>
                          </a>
		         </li>
		        </c:forEach>
             </ul>
           </div>
	    </div>
      </div>

                <script type="text/javascript">
                    $(function () {
                        /*中标公示*/
                        jQuery(".marquee-win").slide({ mainCell: ".bd ul", autoPlay: true, effect: "topMarquee", vis: 8, interTime: 50, trigger: "click" });
                    });
                </script>
    </div>
    </div> 
  
   
  
</div>
<!------主体完------> 

<jsp:include page="footer.jsp"></jsp:include>
<script type="text/javascript">
   $(function(){
        $("#button").click(
		     function(){
		       $("#ProductForm").submit();
		     }
		);
       //招标类型
        $(".biddercate .option-btn").click(function () {
            var val = $(this).attr("val");
            $("#bidderType").val(val);
            Query();
        });
       //公告类型
        $(".noticecate .nbtn").click(function () {
            var val = $(this).attr("val");
            $("#noticeType").val(val);
            Query();
        });
        //发布时间筛选
        $(".rangedata .option-btn").click(function () {
            $("#startDate").val("");  //清空自定义的值
            $("#endDate").val(""); //清空自定义的值
            var val = $(this).attr("val");
            $("#publishType").val(val);
            Query();
        });
        <c:if test="${publishType==4}">
          $(".customdate").css("display", "inline");
        </c:if>
       //自定义时间
        $(".option-range-btn").click(function () {
            if ($(".customdate").css("display") == "none") {
                $(".customdate").css("display", "inline");
                $("#publishType").val("4");
                $(".rangedata .option-btn").removeClass("option-btn-cur");
                $(this).addClass("option-btn-cur");
            }
            else {
                $(".customdate").css("display", "none");
            }
        });
   })
   
   function Query() {
        $("#startIndex").val(0);
        $("#xmForm").submit();
    }
</script>
</body>
</html>
