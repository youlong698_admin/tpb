<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${bidWinning.winningTitle}</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<%=path%>/images/favicon.ico" />
<link href="<%=path %>/web/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=path %>/web/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/indexOther.js"></script>
<script type="text/javascript" src="<%=path %>/web/js/jquery.SuperSlide.2.1.1.js"></script>
</head>

<body>
<jsp:include page="topOther.jsp"></jsp:include>
<jsp:useBean id="nowDate" class="java.util.Date"/> 
<!------公告信息------>
<div class="main">
  <div class="crumbs">
       您所在的位置：<a href="/">首页</a> &gt; <b><span class="orange">公示正文</span></b>
  </div>
   <!-- three -->
  <div class="divFloor">
  <div class="bid_main"> 
    <div class="SideRight">
    <div class="titlex"><b class="titlex-ico">
                    <a>公示正文</a>
            </b>
            </div>
       <h1>${bidWinning.winningTitle}</h1>
         <div class="YZCstrMainLeftHeader YZCstrMainLeftHeaderBor">
             <div class="text">
		      ${bidWinning.winningContent}
		      </div>
        </div>
        <div style="padding: 20px;">
          <strong>附件下载：</strong>
          <c:choose>
            <c:when test="${empty bidWinning.attachmentUrl}">无附件下载</c:when>
            <c:otherwise><c:out value="${bidWinning.attachmentUrl}" escapeXml="false"/></c:otherwise>
          </c:choose>           
        </div>
    </div>
  </div>
</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
