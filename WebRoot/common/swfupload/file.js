$(function(){
var imgUrl="";
initFile();
var uploadUrl = path+"/uploadify_down.action;jsessionid="+sessionId;
   swfu = new SWFUpload({
   //在firefox、chrome下，上传不能保留登录信息，所以必须加上jsessionid。
		
		upload_url : uploadUrl,
		flash_url : path+"/common/swfupload/swfupload.swf",
		file_size_limit : "20 MB",
		file_post_name : "uploadify", 
		post_params: {"attachmentType" : attachmentType},
		file_types : "*.doc;*.docx;*.xls;*.xlsx;*.pdf;*.zip;*.rar;*.jpeg;*.png;*.jpg;*.gif;*.tif;*.txt",
		file_types_description : "所有类型",
		custom_settings : {
			progressTarget : "fsUploadProgress",
			cancelButtonId : "btnCancel"
		},
		debug: false,
		button_image_url : path+"/common/swfupload/uploadfile.png",
		button_placeholder_id : "spanButtonPlaceHolder",
		button_width: 81,
		button_height: 29,
		button_text_top_padding: 2,
		button_text_left_padding: 20,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_start_handler : uploadStart,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : function(file, serverData){
			try {
			    serverData = JSON.parse(serverData);
				fileNameData.push(serverData.fileName);
				uuIdData.push(serverData.uuId);
				fileTypeData.push(serverData.fileType);
				attIdData.push("0");//新上传的附件 附件id为0
				var progress = new FileProgress(file, this.customSettings.progressTarget);
				progress.setComplete();
				progress.setStatus("上传成功.");
				progress.toggleCancel(true);
			} catch (ex) {
				this.debug(ex);
			}
		},
		upload_complete_handler : uploadComplete,
		queue_complete_handler : initFile,
		swfupload_loaded_handler:function(){
			//初始化上传成功数量
		  	var stats = this.getStats();
		  	stats.successful_uploads = uuIdData.length;
		  	this.setStats(stats);
		}
	});

});
   function initFile(){
		$("#fileDiv").html("");
		var filetrStr="";
		for(var i=0;i<uuIdData.length;i++){
		    imgUrl=getimgUrl(fileTypeData[i]);
			filetrStr +='<div><img src="'+path+'/'+imgUrl+'">' + fileNameData[i]  +  '</span><img id="img'+ i+ '"  src="'+path+'/images/delete.gif"  onclick="delFile(this,'+attIdData[i]+')"></div>';
			}
		$("#fileDiv").append(filetrStr);	
	}
 	//删除附件方法
	  function delFile(obj,attId){
	  	var rowIndex = $(obj).parent().index();
	  	$("#fileDiv").children("div").eq(rowIndex).remove();
	  	//删除附件存放位置的同时，删除附件数组中的元素;
	  	fileNameData.splice(rowIndex,1);
	  	uuIdData.splice(rowIndex,1);
	  	fileTypeData.splice(rowIndex,1);
	  	attIdData.splice(rowIndex,1);
	  	var stats = swfu.getStats();  
	  	stats.successful_uploads = uuIdData.length;
	  	swfu.setStats(stats);
	  	//alert(attId);
	  	if(attId!="0"){
	  	var attIds = document.getElementById("attIds") ;
 		if( attIds != undefined  ) {
 			if( attIds.value.length > 0 ){
 				attIds.value = attIds.value + "," + attId  ;
 			}else {
 				attIds.value =  attId  ;
 			}
 		}
	  	}
	  }