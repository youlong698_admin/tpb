$(function(){
var imgUrl="";
initFile();
var uploadUrl = path+"/uploadifyImg_down.action;jsessionid="+sessionId;
   swfu = new SWFUpload({
   //在firefox、chrome下，上传不能保留登录信息，所以必须加上jsessionid。
		
		upload_url : uploadUrl,
		flash_url : path+"/common/swfupload/swfupload.swf",
		file_size_limit : "5 MB",
		file_post_name : "uploadify", 
		post_params: {"attachmentType" : attachmentType},
		file_types_description : "图片类型",
		file_queue_limit : 5,
		file_upload_limit : 5, 
		custom_settings : {
			progressTarget : "fsUploadProgress",
			cancelButtonId : "btnCancel"
		},
		debug: false,
		button_image_url : path+"/common/swfupload/uploadimg.png",
		button_placeholder_id : "spanButtonPlaceHolder",
		button_width: 81,
		button_height: 29,
		button_text_top_padding: 2,
		button_text_left_padding: 20,
		file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_start_handler : uploadStart,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : function(file, serverData){
			try {
				debugger;
				imgData.push(serverData);
				var progress = new FileProgress(file, this.customSettings.progressTarget);
				progress.setComplete();
				progress.setStatus("上传成功.");
				progress.toggleCancel(true);
			} catch (ex) {
				this.debug(ex);
			}
		},
		upload_complete_handler : uploadComplete,
		queue_complete_handler : initFile,
		swfupload_loaded_handler:function(){
			//初始化上传成功数量
		  	var stats = this.getStats();
		  	stats.successful_uploads = imgData.length;
		  	this.setStats(stats);
		}
	});

});
   function initFile(){
	   $(".imgTr").html("");
		$(".buttonTr").html("");
		var imgtrStr="";
		var buttontrStr="";
		debugger;
		for(var i=0;i<imgData.length;i++){
			imgtrStr +='<td><img id="img' + i + '" style="width:150px !important;height:100px !important" src="/fileWeb/'+imgData[i]  +  '"></td>';
			buttontrStr +='<td><a id="button'+ i+ '" href="javascript:void(0)"  onclick="delImg(this)">删除</a></td>';
		}			
		$(".imgTr").append(imgtrStr);
		$(".buttonTr").append(buttontrStr);
	}
 	//删除附件方法
	  function delImg(obj){
		  var rowIndex = $(obj).parent().index();
		  	$("#imgTable tr.imgTr").children("td").eq(rowIndex).remove();
		  	$("#imgTable tr.buttonTr").children("td").eq(rowIndex).remove();
		  	//删除图片存放位置的同时，删除图片数组中的元素;
		  	imgData.splice(rowIndex,1);
		  	var stats = swfu.getStats();  
		  	stats.successful_uploads = imgData.length;
		  	swfu.setStats(stats);
	  }