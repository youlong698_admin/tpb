<%@page contentType="text/html; charset=GBK"%>
<%@ include file="/common/context.jsp"%>
<html>
	<head>
		<title>成功页面</title>	
		<script src="<%=path %>/common/script/context.js" type="text/javascript" ></script>
		<script type="text/javascript">
		
        var api = frameElement.api, W = api.opener;
        var obj;
		 //关闭 
		function closeWindow(){	
		//debugger;
		   if(api.get("Wdialog")){
				W.window.location.href=W.window.location.href;	
				api.get('dialog',1).close();
			    api.close();		
			}else{
				W.window.location.href=W.window.location.href;	
			    api.close();
			}
		}
		//指定时间自动关闭
		window.setTimeout("closeWindow()", 2000); 
		
		</script>
	</head>
	
	<body class="top_02">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="alert alert-block alert-danger">
							<c:choose>
								<c:when test="${not empty message }">
								   ${message }
								</c:when>
								<c:otherwise>
								   操作成功
								</c:otherwise>
							</c:choose>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>