<%@ page contentType="text/html;charset=gb2312" language="java"%>
<%@page import="com.ced.base.utils.RollPage"%>
<%@ include file="/common/context.jsp"%>
<%
  RollPage rollPage=(RollPage)request.getAttribute("rollPage");
  int start=rollPage.getPageFirst();
  int range=rollPage.getPagePer();
  int totalCount=rollPage.getRowCount();
  int pageCur=rollPage.getPageCur()+1;
  int totalPages = (int)Math.ceil((double)totalCount / range);
  StringBuffer sb=new StringBuffer();
        sb.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td height='10'>");
        sb.append("<div class=pg-3>");
        if(pageCur==1){
        	sb.append("<span class=disabled><font size=2>首页</font></span>\r");
            sb.append("<span class=disabled><font size=2>上一页</font></span>\r");
        }else{
        	sb.append("<a href='javascript:formSubmit(0)'><font size=2>首页</font></a>\r");
            sb.append("<a href='javascript:formSubmit("+(pageCur-2)*range+")'><font size=2>上一页</font></a>\r");
        }
        
////////////////////////////////////////////////////////////////        
        if(totalPages <= 10){
            for (int i = 0; i < totalPages; i++) {
                if((i+1)==pageCur){
                    sb.append("<strong class=current>"+pageCur+"</strong>\r");
                    i = i+1;
                    if(pageCur==totalPages)break;
                }
                sb.append("<a href='javascript:formSubmit("+(i*range)+")'>"+(i+1)+"</a>\r");
            }
        }else if(totalPages <= 20){
            //没有把加上
            int l = 0;
            int r = 0;
            if(pageCur<5){
                l=pageCur-1;
                r=10-l-1;
            }else if(totalPages-pageCur<5){
                r=totalPages-pageCur;
                l=10-1-r;
            }else{
                l=4;
                r=5;
            }
            int tmp =  pageCur-l;
            for (int i = tmp; i < tmp+10; i++) {
                if(i==pageCur){
                    sb.append("<strong class=current>"+pageCur+"</strong>\r");
                    i = i+1;
                    if(pageCur==totalPages) break;
                }
                sb.append("<a href='javascript:formSubmit("+((i-1)*range)+")'>"+(i)+"</a>\r");
            }
                
        }else if(pageCur<7){
            for (int i = 0; i < 8; i++) {
                if(i+1==pageCur){
                    sb.append("<strong class=current>"+pageCur+"</strong>\r");
                    i = i+1;
                }
                sb.append("<a href='javascript:formSubmit("+(i*range)+")'>"+(i+1)+"</a>\r");
            }
            sb.append("........");
            sb.append("<a href='javascript:formSubmit("+(totalPages-2)*range+")'>"+(totalPages-1)+"</a>\r");
            sb.append("<a href='javascript:formSubmit("+(totalPages-1)*range+")'>"+(totalPages)+"</a>\r");
        }else if(pageCur>totalPages-6){
            sb.append("<a href='javascript:formSubmit("+(0)+")''>"+(1)+"</a>\r");
            sb.append("<a href='javascript:formSubmit("+(range)+")'>"+(2)+"</a>\r");
            sb.append("........");
            for (int i = totalPages-8; i <totalPages ; i++) {
                if(i+1==pageCur){
                    sb.append("<strong class=current>"+pageCur+"</strong>");
                    i = i+1;
                    if(pageCur==totalPages) break;
                }
                sb.append("<a href='javascript:formSubmit("+(i*range)+")'>"+(i+1)+"</a>\r");
            }
        }else{
            sb.append("<a href='javascript:formSubmit("+0+")'>"+(1)+"</a>\r");
            sb.append("<a href='javascript:formSubmit("+range+")'>"+(2)+"</a>\r");
            sb.append("........");
            
            sb.append("<a href='javascript:formSubmit("+(pageCur-3)*range+")'>"+(pageCur-2)+"</a>\r");
            sb.append("<a href='javascript:formSubmit("+(pageCur-2)*range+")'>"+(pageCur-1)+"</a>\r");
            sb.append("<strong class=current>"+pageCur+"</strong>");
            sb.append("<a href='javascript:formSubmit("+(pageCur*range)+")'>"+(pageCur+1)+"</a>\r");
            sb.append("<a href='javascript:formSubmit("+(pageCur+1)*range+")'>"+(pageCur+2)+"</a>\r");
            
            sb.append("........");
            sb.append("<a href='javascript:formSubmit("+((totalPages-2)*range)+")'>"+(totalPages-1)+"</a>\r");
            sb.append("<a href='javascript:formSubmit("+(totalPages-1)*range+")'>"+(totalPages)+"</a>\r");
        }    
        if(pageCur == totalPages||totalPages==0){
            sb.append("<span class=disabled><font size=2>下一页</font></span>\r");
            sb.append("<span class=disabled><font size=2>尾页</font></span>\r");
        }else{
           sb.append("<a href='javascript:formSubmit("+(pageCur*range)+")'><font size=2>下一页</font></a>\r");
           sb.append("<a href='javascript:formSubmit("+(totalPages-1)*range+")'><font size=2>尾页</font></a>\r");
        }
        //sb.append("<br /><br />");
        sb.append("<span class=total>共"+totalCount+"条记录&nbsp;&nbsp;&nbsp;&nbsp;\r ");
        sb.append("共<span class=totalNum>"+totalPages+"</span>页</span>\r");
        sb.append("<input type='hidden' name='startIndex' value='"+start+"'/></div>");
        sb.append("</td></tr></table>\n"); %>
        
<script language="javascript">
		function formSubmit(num){
			document.form.startIndex.value = num ;
			document.form.submit();
		}
</script>
<%=sb.toString() %>
