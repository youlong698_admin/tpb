<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
     <meta http-equiv="X-UA-Compatible" content="IE=edge"/> 
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="cache-control" content="no-cache"></meta>
	<meta http-equiv="expires" content="0"></meta>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"></meta>
	<meta http-equiv="description" content="This is my page"></meta> 
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>   
	<link rel="stylesheet" type="text/css" href="<%= path %>/common/webuploader/webuploader.css"/>
    <script src="<%= path %>/common/webuploader/webuploader.js" type="text/javascript" ></script>
	<script type="text/javascript" language="javascript"> 
		var api = frameElement.api, W = api.opener,Wdialog;
		var fileType="",url="<%= path %>/",str="",msg="",fileName="";
	
	    $(function (){
	    <c:if test="${type=='template'}">
	 fileType="xls,xlsx";
	 str="导入";
	 fileName="file";
    if(api.get("Wdialog")){
		document.getElementById("title").innerHTML=api.get("dialog").document.getElementById("title").value;
		url=url+api.get("dialog").document.getElementById("submitUrl").value;
		document.getElementById("a_url_name").href="<%= path %>"+api.get("dialog").document.getElementById("url").value;
		document.getElementById("a_url_name").innerHTML=api.get("dialog").document.getElementById("name").value;
	  }else{
	    document.getElementById("title").innerHTML=W.document.getElementById("title").value;
		url=url+W.document.getElementById("submitUrl").value;
		document.getElementById("a_url_name").href="<%= path %>"+W.document.getElementById("url").value;
		document.getElementById("a_url_name").innerHTML=W.document.getElementById("name").value;	  
	  }
	</c:if>
	<c:if test="${type=='updateTemplate'}">
	    fileType="xls,xlsx,doc,docx,rar,zip";
	    str="上传";
	    if(api.get("Wdialog")){
		document.getElementById("title").innerHTML=api.get("dialog").document.getElementById("title").value;
		document.forms[0].action=api.get("dialog").document.getElementById("submitUrl").value;
		url=url+api.get("dialog").document.getElementById("submitUrl").value;
	    fileName=api.get("dialog").document.getElementById("fileName").value;
		 }else{
		 document.getElementById("title").innerHTML=W.document.getElementById("title").value;
		 document.forms[0].action=W.document.getElementById("submitUrl").value;
		 url=url+W.document.getElementById("submitUrl").value;
	     fileName=W.document.getElementById("fileName").value;
		 }
	</c:if>
		     var uploader = WebUploader.create({
					    // swf文件路径
					    swf:'<%= path %>/common/webuploader/Uploader.swf',
					    // 文件接收服务端。
					    server: url,
					    // 选择文件的按钮。可选。
					    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
					    pick: '#addFile',
					    fileVal:fileName,
					    auto: false,
					    accept:{
					    	title: str,
				    	    extensions: fileType,
					    },
					    fileSizeLimit:10*1024*1024
					});
					uploader.on( 'fileQueued', function( file ) {
					    $('#fileS').html(file.name+'等待上传...');
					});
					uploader.on('error', function(type){
						if(type=='Q_TYPE_DENIED'){
							showMsg("alert","请上传文件类型为"+fileType+"的文件");	
						}else if(type=='Q_EXCEED_SIZE_LIMIT'){
							showMsg("alert","单个文件最大支持10M！");	
						}
					})
					uploader.on( 'uploadError', function( file ) {
						showMsg("alert","上传出错");
						$('#uploadPro').fadeOut(function(){//隐藏进度条
							$('#uploadPro .progress-bar').css( 'width', '0%' );
							uploader.reset();
						});
					});
					uploader.on('uploadSuccess', function(file,response) {
					debugger;
					     msg=response.contentImport;
						 $('#fileS').html("");
						 $('#uploadPro').fadeOut(function(){//隐藏进度条
							$('#uploadPro .progress-bar').css( 'width', '0%' );
							uploader.reset();
						 });
						 if(msg!="成功"){
							 showMsg("alert",str+"出错！,错误原因："+msg);
						 }else{
						 	 showMsg('success',str+"成功！",function(){					   		
								 window.location.href="common/success.jsp";
							 });
						 }
					});
					uploader.on('uploadStart',function(file){
						
					});
					uploader.on( 'uploadProgress', function( file, percentage ) {
						$('#uploadPro').show().find('.progress-bar').css( 'width', percentage * 100 + '%' );
						if(percentage==1){
							 $('#fileS').html("已上传完毕。");
						}
					});
					$('#uploadFile').click(function(){
						uploader.upload();
					});
				    $("#btn-danger").click(function(){
				    	api.close();
				    });
		    
		});
	</script>
	
  <style type="text/css">
  	.toolbar{
		height: 50px;
		/* border: 1px solid #d2d2d2; */
		background: #F7F7F7;
		padding-left: 20px;
		position: relative;
		margin-bottom: 20px;
	}
	.toolbar .tool{
		margin-right: 10px;
		margin-top: 12px;
	}
	#addFile{
		display: inline-block;
		margin-right: 10px;
		margin-top: 12px;
		float: left;
	}
	#addFile .webuploader-pick {
		padding: 3px 8px;
		font-size: 12px;
		line-height: 1.5;
		border-radius: 4px;
		color: #fff;
		background-color: #0a67fb;
		border-color: #0354d4;
		-webkit-box-shadow: 0 2px 1px rgba(0,0,0,.1);
		-moz-box-shadow: 0 2px 1px rgba(0,0,0,.1);
		box-shadow: 0 2px 1px rgba(0,0,0,.1);
	}
  </style>
</head>  
  <body>
    <form method="post" action="" enctype="multipart/form-data" >
    	<div class="Conter_Container" >
    		<div class="Conter_main_conter">
				<table  class="table_ys1">	
					<tr>
		          		<td  colspan="2" class="Content_tab_style_05" id="title"></td>
		        	</tr>
		        	<tr>
		        		<td colspan="2"  class="Content_tab_style1" style="text-align: left;" ><div id="fileS"></div>
			<div style="float: left;">
				<div id="addFile">选择<c:if test="${type=='template'}">导入</c:if><c:if test="${type=='updateTemplate'}">上传</c:if>文件</div>
			</div>
			<div id="uploadPro" class="progress progress-striped active" style="display:none;float:left; width:300px;margin-top: 13px;margin-bottom: 0px;margin-left: 20px;background-color: #DDD;">
          		<div class="progress-bar progress-bar-success" role="progressbar" style="width: 80%">
          		</div>
        	</div>
        	</td>
		        		
		        		
		        	</tr>
		        	<tr>
					    <td height="24" class="Content_tab_style1" colspan="2" style="text-align: left;">
					    <br/>
					    &nbsp;&nbsp;使用说明：<br/>
					    <c:if test="${type=='template'}">
						&nbsp;&nbsp;1.  请首先下载文件模板EXCEL；<br/>
						&nbsp;&nbsp;2.	根据文件模板EXCEL填入相应内容，模板样式请不要改动；<br/>
						&nbsp;&nbsp;3.	数据确认无误后，点击"导入"按钮,如果导入失败，请根据系统提示信息修改模板相关数据；<br/>
						&nbsp;&nbsp;4.	文件模板下载：
			
						<a href="" target="_blank" id="a_url_name"></a>
						</c:if>
						<c:if test="${type=='updateTemplate'}">
						&nbsp;&nbsp;1.  文件类型：xls、xlsx、doc、docx、rar、zip；<br>
						&nbsp;&nbsp;2.	数据确认无误后，点击"上传"按钮；<br/>
						</c:if>
						</td>
				   	</tr>
		        </table>
		        <div class="buttonDiv">
					<button class="btn btn-success" id="uploadFile" type="button" ><i class="icon-white icon-ok-sign"></i><c:if test="${type=='template'}">导  入</c:if><c:if test="${type=='updateTemplate'}">上  传</c:if></button>
					<button class="btn btn-danger" type="button" id="btn-danger"><i class="icon-white icon-remove-sign"></i>关闭</button>
	
				</div>
		    </div>
		</div>
    	<div></div>
    </form>
   <input type="hidden" id="contentImport" value="${contentImport}"/>
  </body>
</html>
