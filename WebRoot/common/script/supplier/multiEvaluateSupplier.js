/***************************供应商考核信息管理*****************************/
/***供应商考核信息***/
function multiSupEvaHandle(type){
	if(type=='updateSup'){
		var trObj = document.getElementById("tbSup").rows;
		var blankNum = 0;
		for(var i=1;i<trObj.length;i++){
			var tdObj = trObj[i].cells[1];
			var spanText = tdObj.innerText;
			if(spanText!=""){
				blankNum++;
			}else{
				showMsg("alert","温馨提示：第"+i+"行没有考核人，请先分配考核人后再保存！");
			}
		}
		if(blankNum==(trObj.length-1)){
			document.forms[0].action="updateEvaluateSupplierSup_multiEvaluataion.action";
			document.forms[0].submit();
		}else{
			return;
		}
	}else if(type=='updateUser'){
		var trObj = document.getElementById("tbUser").rows;
		var blankNum = 0;
		for(var i=1;i<trObj.length;i++){
			var tdObj = trObj[i].cells[1];
			var spanText = tdObj.innerText;
			if(spanText!=""){
				blankNum++;
			}else{
				showMsg("alert","温馨提示：第"+i+"行没有被评供应商，请先分配供应商后再保存！");
			}
		}
		if(blankNum==(trObj.length-1)){
			document.forms[0].action="updateEvaluateSupplierUsers_multiEvaluataion.action";
			document.forms[0].submit();
		}else{
			return;
		}
	}else if(type=='delete'){
		var arrObj = supEva.getSelecteds();
		var seId = "";
		if(arrObj!=null&&arrObj.length!=0){
			for(var i=0;i<arrObj.length;i++){
				if(arrObj.length==1){
					seId = arrObj[0].seId;
					break;
				}
				seId += arrObj[i].seId +",";
			}
			var status = arrObj[0].status;
			if(status=='1'){
				showMsg("alert","温馨提示：供应商考核正在进行中，不能删除！");
			}
			else if(status=='2'){
				showMsg("alert","温馨提示：供应商考核已完成，不能删除！");
			}else if(status=='3'){
				showMsg("alert","温馨提示：供应商考核已过期，不能删除！");
			}else{
				if(seId!=''&&seId!=null){
					//alert(seId);
					if(confirm("温馨提示：你确定要删除吗？")){
						document.forms[0].action="deleteEvaluateSupplier_multiEvaluataion.action?tempSupEvaInfo="+seId;
						document.forms[0].submit();
					}else{
						return false;
					}
				}
			}
		}else{
			showMsg("alert","温馨提示：请选择要删除的信息");
		}
	}
}
/***供应商考核提交***/
function submitEvaluation(){
	var arrObj = supEva.getSelecteds();
	if(arrObj.length!=0){
		if(arrObj.length==1){
			var seId = arrObj[0].seId;
			var status = arrObj[0].status;
			if(status=='1'){
				showMsg("alert","温馨提示：供应商考核正在进行中，不能再次提交！");
			}else if(status=='2'){
				showMsg("alert","温馨提示：供应商考核已完成，不能再次提交！");
			}else if(status=='3'){
				showMsg("alert","温馨提示：供应商考核已过期，不能提交！");
			}else{
				if(seId!=''&&seId!=null){
					if(confirm("温馨提示：你确定要提交吗？")){
						var action = "caculateEvaIndexPoint_multiEvaluataion.action";
						var param = "supplierEvaluations.seId="+seId;
						var result = ajaxGeneral(action,param,"text");
						 //alert(result);
						 if(result==100){
							 //alert(validateEvaUserRefSup(seId));
							 if(validateEvaSupRefUser(seId)!=''){
								 showMsg("alert","温馨提示：此次考核中有供应商未设置考核人，请在设置后提交！");
								 //alert("温馨提示：此次考核中供应商【"+validateEvaSupRefUser(seId)+"】没有设置考核人，请在设置后提交！");
							 }else if(validateEvaUserRefSup(seId)!=0){
								 showMsg("alert","温馨提示：此次考核中有考核人未分配供应商，请在设置后提交！");
							 }else{
								 var seId = arrObj[0].seId;
						    	 var processId = arrObj[0].processId;
						    	 var orderId = arrObj[0].orderId;
						    	 var orderState = arrObj[0].orderState;
						    	 var instanceUrl = arrObj[0].instanceUrl;
						    	 window.location.href="saveSubmitSupplierEvaluation_multiEvaluataion.action?processId="+processId+"&orderId="+orderId+"&orderNo="+seId+"&supplierEvaluations.seId="+seId;
							 }
						 }else{
							 if(result==0){
								 alert("温馨提示：请先设置供应商考核指标，保存后再提交！");
							 }else{
								 alert("温馨提示：考核指标分值之和必须等于100，请先调整考核指标分值，保存后再提交！"); 
							 }
						 }
					}else{
						return false;
					}
				}
			}
		}else{
			showMsg("alert","温馨提示：只能选择一条信息！");
		}
	}else{
		showMsg("alert","温馨提示：请选择要提交考核的信息");
	}
}
/*****校验供应商和考核人的关联情况****/
function validateEvaSupRefUser(seId){
	var action = "validateEvaIndexSupRefUser_multiEvaluataion.action";
	var param = "supplierEvaluations.seId="+seId;
	var result = ajaxGeneral(action,param,"text");
	return result;
}
function validateEvaUserRefSup(seId){
	var action = "validateEvaIndexUserRefSup_multiEvaluataion.action";
	var param = "supplierEvaluations.seId="+seId;
	var result = ajaxGeneral(action,param,"text");
	return result;
}
/***复制考核记录***/
function copySupAssessRow(){
	var arrObj = supEva.getSelecteds();
	var seId = "";
	if(arrObj!=null){
		if(arrObj.length!=0){
			for(var i=0;i<arrObj.length;i++){
				if(arrObj.length==1){
					seId = arrObj[0].seId;
					break;
				}
				seId += arrObj[i].seId +",";
			}
			if(seId!=''&&seId!=null){
				window.location.href="saveCopySupEvalationInfo_multiEvaluataion.action?tempSupEvaInfo="+seId;
			}
		}else{
			alert("温馨提示：请选择要复制的信息");
		}
	}
}
/***供应商考核返回父页面***/
function goBackMultiEvaluateParentView(action){
	window.parent.location.href=action+"_multiEvaluataion.action";
}
/***查看供应商考核详细信息弹出窗口***/
function openNewWindow(action){
	createdetailwindow("供应商考核打分",action,1);
}
/******动态添加考核指标信息******/
var tbNum1 = 0;
function addRowsColumn1(){
	var trNum = $("#tbIndex tr").length
	tbNum1 = trNum-1;
	//alert(tbNum1);
	var rowIndex=document.getElementById("tbIndex").rows.length-1;
	tr=document.getElementById("tbIndex").insertRow(rowIndex);
	tr.className ='biaoge_01_a';
	var cell1 = tr.insertCell();
	var cell2 = tr.insertCell();
	var cell3 = tr.insertCell();
	var cell4 = tr.insertCell();
	var cell5 = tr.insertCell();
	cell1.innerHTML = "<input type='button' id='' name='' value='删除' onclick='delteRowsColumns(this)'/>";
	cell2.innerHTML = "<input type='text' onclick='selectEvaIndex(this)' id='eiName"+tbNum1+"' name='evaIndexList["+tbNum1+"].eiName' value='' size='40'/>"+
	  				  "<input type='hidden' id='eiId"+tbNum1+"' name='evaIndexList["+tbNum1+"].eiId' value=''/>";
	cell3.innerHTML = "<input type='text' id='eiDescribe"+tbNum1+"' name='evaIndexList["+tbNum1+"].eiDescribe' value='' size='20'/>";
	cell4.innerHTML = "<input type='text' id='points"+tbNum1+"' name='evaIndexList["+tbNum1+"].points' value='' />";
	cell5.innerHTML = "<input type='text' id='adjustPoints"+tbNum1+"' name='evaIndexList["+tbNum1+"].adjustPoints' value='' />";
	tbNum1++;
}
/******动态删除行******/
function delteRowsColumns(obj){
	
	var supplierId=$(obj).attr("id");
	var supplierIds=$("#supId").val();
	//alert(supplierId+"-----"+supplierIds);
	$("#supId").val(supplierIds.replace(","+supplierId+",",","));
	$(obj).parent().parent().remove(); 
}
/*****校验考核起止日期****/
function checkStartEndDate(){
	var timeStart = getNowStringDate(new Date(document.getElementById("timeStart").value.replace(/\-/g, "\/")));
	var timeEnd = getNowStringDate(new Date(document.getElementById("timeEnd").value.replace(/\-/g, "\/")));
	var timeNow = getNowStringDate(new Date());
	//alert(timeStart+"--"+timeEnd+"--"+timeNow);
	if(timeStart>timeEnd){
		showMsg("alert","温馨提示：考核开始日期不能在结束日期之前！");
		return false;
	}else if(timeNow>timeEnd){
		showMsg("alert","温馨提示：考核结束日期不能在当前日期之前！");
		return false;
	}else{
		return true;
	} 
}
/**********选择供应商考核指标************/
function selectEvaIndex(){
	var ul =document.getElementById("idNames").value;
	 if(ul.length==0){
		 ul=-1;
	 }else{
		 ul=ul.substring(1,ul.lastIndexOf(","));
	 }
	createdetailwindow_choose("选择供应商考核指标","viewDeptIndex_eindexTree.action?evaluationIndex.eiName=0&evaluationIndex.eiIdString="+ul,1);
	
}
function valueEvaIndex(){
	
	var strIndex=$("#returnValues").val();
	
	var seId = document.getElementById("seId").value;
	var tempsef = document.getElementById("tempsef").value;
	var trNum = document.getElementById("tbIndex").getElementsByTagName("tr").length;
	//alert(seId+"---"+strIndex);
	if(strIndex!="" && typeof(strIndex)!='undefined'){
		if((trNum-2)==0){
			//列表无数据
			window.parent.botFrame.location.href="saveEvaluateSupplierIndexInit_multiEvaluataion.action?strIndex="+strIndex+"&supplierEvaluations.seId="+seId+"&tempSupEvaInfo="+tempsef;
		}else if((trNum-2)>0){
			//列表已有数据
			window.parent.botFrame.location.href="updateEvaluateSupplierIndex_multiEvaluataion.action?strIndex="+strIndex+"&supplierEvaluations.seId="+seId+"&tempSupEvaInfo="+tempsef;
		}
		document.getElementById("idNames").value = strIndex; 
	}else{
		return false;
	}
}
/***删除所选考核指标***/
function deleteEvaIndexPoints(){
	var ckNum = 0;
	var evaIds = "";
	var ckEvas = document.getElementsByName("ckEva");
	var tbObj = document.getElementById("tbIndex");
	var seId = document.getElementById("seId").value;
	var tempsef = document.getElementById("tempsef").value;
	var status = document.getElementById("status").value;
	for(var i=0;i<ckEvas.length;i++){
		if(ckEvas[i].checked==true){
			evaIds += ckEvas[i].value+",";
			ckNum++;
		}
	}
	if(ckNum==0){
		showMsg("alert","温馨提示：请选择要删除的指标");
	}else{
		if(confirm("温馨提示：你确定要删除考核指标吗？")){
			window.parent.botFrame.location.href="deleteEvaluateSupplierIndex_multiEvaluataion.action?supplierEvaluations.seId="+seId+"&strIndex="+evaIds+"&tempSupEvaInfo="+tempsef;
		}
	}
}
/***考核指标基准分值调整***/
function adjustEvaIndexPoints(){
	var indexNum = document.getElementById("list").value;
	var sumPoint = caculateIndexValue(indexNum)
	var tempsef = document.getElementById("tempsef").value;
	//alert(sumPoint);
	if(sumPoint!=100){
		if(sumPoint>100){
			showMsg("alert","温馨提示：考核指标分值之和大于100，请调整指标分值或重新选取指标！");
		}
		if(sumPoint<100){
			showMsg("alert","温馨提示：考核指标分值之和小于100，请调整指标分值或重新选取指标！");
		}
	}else{
		
			document.forms[0].action="saveEvaluateSupplierIndex_multiEvaluataion.action?tempSupEvaInfo="+tempsef;
			document.forms[0].submit();
		
	}
}
/***计算考核指标调整分值之和***/
function caculateIndexValue(indexNum){
	var sumPoint = 0.0;
	for(var i=0;i<indexNum;i++){
		var point = document.getElementById("point"+i).value;
		sumPoint += parseFloat(point);
	}
	sumPoint = sumPoint.toFixed(2);
	return sumPoint;
}
/*****计算考核指标调整分值列的和******/
function caculateTdSumValue(obj){
  //alert(obj.id);
  var table =document.getElementById("tbIndex");
  var rows = table.rows;
  var colums = table.rows[0].cells;
  var rowIndex = obj.parentNode.parentNode.rowIndex;//行索引
  var cellIndex = obj.parentNode.cellIndex;//列索引
  var tbValue = rows[rowIndex].cells[6].innerHTML;
  var tdSumVale = 0;
  for(var i=1;i<rows.length-1;i++){
	  var tdValue = rows[i].cells[cellIndex].getElementsByTagName('input');
	  if(tdValue[0]!=null){
		  jdPoint = tdValue[0].value;
		  tdSumVale += Number(jdPoint);
		  //alert(tdValue[0].value);
	  } 
  }
  document.getElementById("sumPoint").innerHTML=tdSumVale.toFixed(2); 
}
/***选择考核人***/
function selectEvaluateUser(){
    var ul =document.getElementById("userIds").value;
	if(ul.length==0 || ul==","){
		 ul=-1;
	 }else{
	 	ul=ul.substring(1,ul.lastIndexOf(","));
	 }
	createdetailwindow_choose("选择考核人","viewDeptIndex_userTree.action?ul="+ul,1);
}
function valueEvaluateUser(){
     var users=$("#returnValues").val();
	 $("#user1Chosen").html("");
	 document.getElementById("userIds").value="";
	 
	if(users!=""){
		var str=",";
		var std=",";
		var ur = users.split(",");
		for(var i=0;i<ur.length;i++){
			var u = ur[i].split(":");
			str += u[0]+",";     
			std += u[1]+",";
			$('<li class="search-choice" user-id="'+u[0]+'"><span>'+u[1]+'</span><a class="search-choice-close" ></a></li>')
			.appendTo('#user1Chosen');
		}		
		document.getElementById("userIds").value=str;
	}
}

/***选择考核供应商***/
function selectSupplierInfo(){
	 var ul =document.getElementById("supplierIds").value;
	 if(ul.length==0 || ul==","){
		 ul=-1;
	 }else{
	 	ul=ul.substring(1,ul.lastIndexOf(","));
	 }
	createdetailwindow_choose("选择考核供应商","viewEvaluateSupplierIndex_evaluateSupplierSelect.action?ul="+ul,1);		
}
function valueSupplierInfo(){
	var suppliers=$("#returnValuesSupplier").val();
	$("#suppliersChosen").html("");
	document.getElementById("supplierIds").value="";	
	 
	if(suppliers!=null){
		var str=",";
		var std=",";
		var idNamesArr = suppliers.split(",");
		for(var i=0;i<idNamesArr.length;i++){
			var u = idNamesArr[i].split(":");
			str += u[0]+",";     
			std += u[1]+",";
			$('<li class="search-choice" supplier-id="'+u[0]+'"><span>'+u[1]+'</span><a class="search-choice-close" ></a></li>')
			.appendTo('#suppliersChosen');
		}
		document.getElementById("supplierIds").value=str;
	}
}
/***分配供应商***/
var indexS;
function selectEvaSup(index){
	indexS=index;	
	var ul =document.getElementById("supplierIds"+indexS).value;
	var seId =document.getElementById("seId").value;
	 if(ul.length==0){
		 ul=-1;
	 }else{
	 	ul.substring(0,ul.lastIndexOf(","));
	 }
	createdetailwindow_choose("分配供应商","viewSupplierIndex_evaSupplier.action?ul="+ul+"&supplierEvaluateSup.seId="+seId,1);
	
}
function valueEvaSup(){
	
	var suppliers=$("#returnValues").val();
	if(suppliers!=null){
		var str="";
		var std="";
		var idNamesArr = suppliers.split(",");
		for(var i=0;i<idNamesArr.length;i++){
			var u = idNamesArr[i].split(":");
			str += u[0]+",";     
			std += u[1]+",";
		}
		//页面动态显示
		document.getElementById("evaSup"+indexS).value = suppliers;
		var supNames = std.substring(0, std.lastIndexOf(","));
		document.getElementById("es"+indexS).innerHTML=supNames+"...";
		$("#es"+indexS).attr("title", std.substring(0, std.lastIndexOf(",")));
		//赋值到隐藏域
		document.getElementById("supplierIds"+indexS).value=str;
	}
}
/***分配考核人***/
var indexT;
function selectEvaUser(index){
	indexT=index;
	var ul =document.getElementById("userIds"+indexT).value;
	var seId =document.getElementById("seId").value;
	 if(ul.length==0){
		 ul=-1;
	 }else{
	 	ul.substring(0,ul.lastIndexOf(","));
	 }
	 createdetailwindow_choose("分配考核人","viewUserIndex_evaSupplier.action?ul="+ul+"&supplierEvaluateUser.seId="+seId,1);
		
}
function valueEvaUser(){	
	var users=$("#returnValues").val();
	if(users!=null){
		var str="";
		var std="";
		var ur = users.split(",");
		for(var i=0;i<ur.length;i++){
			var u = ur[i].split(":");
			str += u[0]+",";     
			std += u[1]+",";
		}
		document.getElementById("evaUsers"+indexT).value = users;
		var userNames = std.substring(0, std.lastIndexOf(","));
		document.getElementById("eu"+indexT).innerHTML=userNames+"...";
		$("#eu"+indexT).attr("title", std.substring(0, std.lastIndexOf(",")));
		document.getElementById("userIds"+indexT).value=str;
	}
}
/***供应商考核打分***/
function supEvaluationPoints(sesId,index,type){
	//判断考核人考核时间是否在指定时间范围，replace(/\-/g, "\/") 转换日期为长日期格式
	 var timeStart = getNowStringDate(new Date(document.getElementById("timeStart").value.replace(/\-/g, "\/")));
	 var timeEnd = getNowStringDate(new Date(document.getElementById("timeEnd").value.replace(/\-/g, "\/")));
	 var timeNow = getNowStringDate(new Date());
	 if((timeNow>=timeStart)&&(timeNow<=timeEnd)){
		 var seId = document.getElementById("seId").value;
		 var seuId = document.getElementById("seuId").value;
		 createdetailwindow("供应商考核","saveViewEvaPointsSuppliers_multiEvaluataion.action?supplierEvaluateResult.seId="+seId+"&supplierEvaluateResult.sesId="+sesId+"&supplierEvaluateResult.seuId="+seuId+"&index="+index,1);
		 	
	 }else{
		 var tStart = document.getElementById("timeStart").value;
		 var tEnd = document.getElementById("timeEnd").value;
		 showMsg("alert","温馨提示：现在不是考核时间，请在"+tStart+"到"+tEnd+"之间进行考核！");
	 }
}
/***提交供应商考核分值***/
function submitJdPoint(index,type){
	var seId = document.getElementById("seId").value;
	var seuId = document.getElementById("seuId").value;
	var sesId = document.getElementById("sesId"+index).value;
	var taskId = document.getElementById("taskId").value;
	var jdPoint = document.getElementById("jdPoint"+index).innerHTML;
	if(jdPoint==""||typeof(jdPoint)=="undefined"){
		showMsg("alert","温馨提示：还未进行考核，不能提交！");
	}else{
		$.dialog.confirm("温馨提示：你确定要提交考核吗？",function(){
			reValue = seId+":"+seuId+":"+sesId;
			//alert(reValue);
			var action = "saveEvaPointsSuppliers_multiEvaluataion.action";
			var param = "tempSupEvaInfo="+reValue+"&type="+type+"&result=-1&taskId="+taskId;
			var result =ajaxGeneral(action,param,"text");
			if(result){
				document.getElementById("subPoint"+index).disabled = true;
			}
		},function(){},api);
	}
}
/***确认供应商考核指标分值***/
function confirmSupEvaPoints(){
	var seId = document.getElementById("seId").value;
	var seuId = document.getElementById("seuId").value;
	var sesId = document.getElementById("sesId").value;
	var index = document.getElementById("index").value;
	var evaIndexList = document.getElementById("evaIndexList").value;
	var evaIndex = "";
	var jdSumPoint = 0;//被评供应商指标值之和
	var jdNum = 0;
	if(evaIndexList!=0){
		for(var i=0;i<evaIndexList;i++){
			var serId = document.getElementById("serId"+i).value;
			var jdPoint = document.getElementById("jdPoint"+i).value;
			if(jdPoint!=''){
				evaIndex += serId+":"+jdPoint+",";
				jdNum++; 
				jdSumPoint += parseFloat(jdPoint);
			}
		}
	}
	 jdSumPoint = jdSumPoint.toFixed(2);
	if(jdNum!=0){
		if(jdNum==evaIndexList){
			var type="save";
			evaIndex = evaIndex.substring(0, evaIndex.length-1);
			returnValue = evaIndex;
			 var action = "saveEvaPointsSuppliers_multiEvaluataion.action";
			 var param = "tempSupEvaInfo="+returnValue+"&type="+type+"&supplierEvaluateResult.seId="+seId+"&supplierEvaluateResult.sesId="+sesId+"&supplierEvaluateResult.seuId="+seuId;
			 var result = ajaxGeneral(action,param,"text");
			 if(result){
				 /*showMsg("success","考核成功",function(){
 		               window.location.href = "success_requiredCollect.action";					
			      });*/
				 api.get("dialog").document.getElementById("jdPoint"+index).innerHTML = jdSumPoint;
				 api.get("dialog").document.getElementById("jdDate"+index).innerHTML = result;
				 api.close();
			 }
		}else{
			showMsg("alert","温馨提示：您还有未评分指标");
		}
	}else{
		showMsg("alert","温馨提示：您还没有给考核指标评分，请评分后再点击[确定]或直接点击[关闭]退出窗口！");
	}
}
/***校验输入的值是否大于指定值***/
function isOverBaseValue(obj){
  var table =document.getElementById("evaIndex");
  var rows = table.rows;
  var colums = table.rows[0].cells;
  var rowIndex = obj.parentNode.parentNode.rowIndex;//行索引
  var cellIndex = obj.parentNode.cellIndex;//列索引
  var tbValue = rows[rowIndex].cells[4].innerHTML;
  if(Number(obj.value)>tbValue){
	  showMsg("alert","温馨提示：输入值不能超过标准分值！"); 
	 document.getElementById(obj.id).value="";
  }
}