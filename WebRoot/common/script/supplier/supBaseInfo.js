/***************************供应商基本信息管理*****************************/
/***选择采购类别***/
function selectProKindWeb(){
	createdetailwindow("选择主营产品","productDictionarySelectIndex.action",4);
}
function selectProKind(){
	createdetailwindow("选择主营产品","viewProductDictionaryWindow_productDictionarySelect.action",4);
}
//此方法是选择供应商自己的产品
function selectMyProKind(){
	createdetailwindow("选择主营产品","viewMyProductDictionaryWindow_productDictionarySelect.action",4);
}
function valueProKind(){
	
	var winObj = $('#returnVals').val();
	var proKindIds = ",";
	var proKindNames = "";
	if(winObj!=null&&winObj!=''){
		var proKindArr = winObj.split(",");
		for(var i=0;i<proKindArr.length;i++){
			if(proKindArr[i]!=""){
				var proKinds = proKindArr[i].split(":");
				proKindIds += proKinds[0]+",";
				proKindNames += proKinds[1]+",";
			}
		}
		proKindNames = proKindNames.substring(0, proKindNames.length-1);
		$("#proKindIds").val(proKindIds);
		$("#proKindNames").val(proKindNames);
	}
}
/***校验供应商名称是否重复***/
function checkOutPageValueSupp(obj){
	DwrService.ifExitCodeInTable("SupplierInfo", "supplierName", obj.value, set_viewSupp );	
}
function set_viewSupp(count){
	if( count >0){
		document.getElementById("infoDsp").innerHTML="该名称已存在,请重新输入!";
		document.getElementById("supplierName").focus();
		$('#submitButton').hide();
	}else{
		document.getElementById("infoDsp").innerHTML="";
		$('#submitButton').show();
	}
}
/***校验组织机构代码证是否重复***/
function checkOutPageValueOrg(obj){
	DwrService.ifExitCodeInTable("SupplierInfo", "orgCode", obj.value, set_viewOrg );	
}
function set_viewOrg(count){
	if( count >0){
		document.getElementById("infoDspOrg").innerHTML="该代码已存在,请重新输入!";
		document.getElementById("orgCode").focus();
		$('#submitButton').hide();
	}else{
		document.getElementById("infoDspOrg").innerHTML="";
		$('#submitButton').show();
	}
}
/***校验用户名是否重复***/
function checkOutPageValue(obj){
	DwrService.ifExitCodeInTable("SupplierInfo", "supplierLoginName", obj.value , set_view );	
}
function set_view(count){
	if( count >0){
		document.getElementById("infoDspLoginName").innerHTML="该名称已存在,请重新输入!";
		document.getElementById("supplierLoginName").focus();
		$('#submitButton').hide();
	}else{
		document.getElementById("infoDspLoginName").innerHTML="";
		$('#submitButton').show();
	}
}
/***校推荐码是否有效***/
function checkOutPageValueM(){
	var m=$("#m").val();
	DwrService.getIdInTable("Users", "referralCode", m ,"userId", set_viewM );	
}
function set_viewM(userId){
	if( userId >0){
		document.getElementById("mDsp").innerHTML="该推荐码有效";
		document.getElementById("userId").value=userId;
	}else{
		document.getElementById("mDsp").innerHTML="该推荐码无效,请确认输入正确";
		document.getElementById("userId").value=0;
	}
}
function checkSupLen(length){ 
	var value = $("#introduce").val();
    
    document.getElementById("supIntroduce").value = value.length; 
} 
/*******选择注册代码证类型*********/
function doSelect(obj){
	var value=$("#registrationType").val();
	if(value=="01"){
		$('#registrationTypeTd').html("统一社会信用代码证号：");
		$('#registrationTypeDiv').html("统一社会信用代码证号不能为空！");
		$('#orgCode').attr("nullmsg","统一社会信用代码证号不能为空！");
		$('#registrationTypeTr').show();
	}else{
		$('#registrationTypeTd').html("组织机构代码证号：");
		$('#registrationTypeDiv').html("统一社会信用代码证号不能为空！");
		$('#orgCode').attr("nullmsg","组织机构代码证号不能为空！");
		$('#registrationTypeTr').show();
	}
}
function checkProLen(length){ 
	var value = $("#remark").val();
//    var len = getStringLen(value);
 //   alert("字符:"+len);
    
    document.getElementById("proRemark").value = value.length; 
}
//日期判断
function validateBidsDate(){
	var openDate = document.getElementById("openDate").value.replace(/-/g, "/");//起始日期
	var endDate = document.getElementById("endDate").value.replace(/-/g, "/");//结束日期
		if(endDate<openDate){
			showMsg("alert","温馨提示：有效时间止不能小于有效时间起!");
			document.getElementById("endDate").value="";
		}
}



function  getStringLen(Str){     
    var i,len,code;     
    if(Str==null || Str == "")   
    	return   0;     
    len = Str.length;     
    for(i=0; i<Str.length; i++){       
	    code = Str.charCodeAt(i);     
	    if(code > 255) {
	    	len ++;
	    }     
    }     
    return   len;     
}     
	
function checkLen(length){ 
	var value = $("#remark").val();
//    var len = getStringLen(value);
 //   alert("字符:"+len);
   
    document.getElementById("YesCou03").value = value.length; 
}
function openWindow(sign){
	createdetailwindow("注册协议须知","regAgencyIndex.action?sign="+sign,7);
}
function saveReg(){
		if ($('#isAgreed').attr('checked')) {
			document.supInfo.action="supplierRegIndex.action";
			document.supInfo.submit();
		}else{
			showMsg("alert","温馨提示：请仔细阅读注册协议！");
		}
}