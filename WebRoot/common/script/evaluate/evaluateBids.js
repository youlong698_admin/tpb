/***************************评委评标管理*****************************/

//保存评委评分
function bidJudgeHandle(type,param){
	if(type=='add'){
		if(param==0){
			$.dialog.confirm("你确定要保存评分？",function(){
				document.forms[0].action="saveTechOrBusJudgerBids_evaluate.action?param=0";
			  	document.forms[0].submit();	
			},function(){},api);
		}else{
			$.dialog.confirm("你确定要提交评分？提交之后将无法修改",function(){
				document.forms[0].action="saveTechOrBusJudgerBids_evaluate.action?param=1";
			  	document.forms[0].submit();	
			},function(){},api);
		}
	}else if(type=='update'){
		if(param==0){
			$.dialog.confirm("你确定要修改评分？",function(){
				document.forms[0].action="updateTechOrBusJudgerBids_evaluate.action?param=0";
			  	document.forms[0].submit();	
			},function(){},api);
		}else{
			$.dialog.confirm("你确定要提交评分？提交之后将无法修改",function(){
				document.forms[0].action="updateTechOrBusJudgerBids_evaluate.action?param=1";
			  	document.forms[0].submit();	
			},function(){},api);	
		}
	}
}
// 评标
function doEvaluate(url){	
	createdetailwindow("专家评标",url,1);	    
}
//校验输入的值是否大于指定值
function isOverBaseValue(obj){
  //alert(obj.id);
  var table =document.getElementById("itemSup");
  var rows = document.getElementById("evatable").rows;
  var colums = document.getElementById("evatable").rows[0].cells;
  var rowIndex = obj.parentNode.parentNode.rowIndex;//行索引
  var cellIndex = obj.parentNode.cellIndex;//列索引
  var tbValue = rows[rowIndex].cells[2].innerHTML;
  if(!isNaN(obj.value)){
	  //alert(obj.value+"--"+tbValue);
	  if(Number(obj.value)>tbValue){
		  showMsg("alert","输入值不能超过标准分值！"); 
		 document.getElementById(obj.id).value="";
	  }else{
		  caculateTdSumValue(obj); 
	  }
  }else{
	  showMsg("alert","请输入数字！");
	  document.getElementById(obj.id).value="";
  }
}
//计算table列的和
function caculateTdSumValue(obj){
  //alert(obj.id);
  var table =document.getElementById("itemSup");
  var rows = document.getElementById("evatable").rows;
  var colums = document.getElementById("evatable").rows[0].cells;
  var rowIndex = obj.parentNode.parentNode.rowIndex;//行索引
  var cellIndex = obj.parentNode.cellIndex;//列索引
  var tbValue = rows[rowIndex].cells[2].innerHTML;
  var tdSumVale = 0;
  for(var i=1;i<rows.length-2;i++){
	  if(i==(rows.length-3)){
		  var tdObj = rows[i].cells[cellIndex];
		  //alert(i+"--"+(cellIndex));
		  var inputObj = tdObj.getElementsByTagName('input');
		  document.getElementById(inputObj[0].id).value=tdSumVale.toFixed(2); 
		  document.getElementById(inputObj[0].id).readOnly = true;
		  //tdObj.innerText=tdSumVale.toFixed(2); 
	  }else{
		  var tdValue = rows[i].cells[cellIndex].getElementsByTagName('input');
		  if(tdValue[0]!=null){
			  jdPoint = tdValue[0].value;
			  tdSumVale += Number(jdPoint);
		  } 
	  }
  }
}
//复制指定列的值到其他列
function copyValueToOther(obj){
  var table =document.getElementById("itemSup");
  var rows = document.getElementById("evatable").rows;
  var colums = document.getElementById("evatable").rows[0].cells;
  var rowIndex = obj.parentNode.parentNode.rowIndex;//行索引
  var cellIndex = obj.parentNode.cellIndex;//列索引
  //alert(rowIndex+"--"+cellIndex);
  for(var i=(rowIndex+1);i<rows.length-2;i++){
	  var tdValue = rows[i].cells[cellIndex-1].getElementsByTagName('input');
	  var tdNextValue = rows[i].cells[cellIndex].getElementsByTagName('input');
	  document.getElementById(tdNextValue[0].id).value = tdValue[0].value; 
	  //alert(i+"--"+(cellIndex-1));
	  //alert(tdNextValue[0].id+"--"+tdValue[0].value);
  }
}
//获取table的行数和列数
function rowsOrCells(obj){
  var table =document.getElementById("itemSup");
  var rows = document.getElementById("evatable").rows;
  var colums = document.getElementById("evatable").rows[0].cells.length;
  if(rows.length!=0&&colums!=0){
	  //alert('行数'+rows.length);
	 // alert('列数'+colums);
	  for(var i=2;i<colums;i++){
		  for(var j=1;j<rows.length;j++){
			  var tbValue = rows[j].cells[i].innerHTML;
			  if(tbValue!=''){
				  if(rows[j].cells[i].innerHTML>rows[j].cells[2].innerHTML){
					  showMsg("alert","评分不能大于标准分值！");
				  }
			  }
		  }
	  }
	  for(var i=1;i<rows.length-3;i++){
	       for(var j=2;j<rows[i].cells.length;j++){
	    	   showMsg("alert","第"+(i+1)+"行，第"+(j+1)+"列的值是:"+rows[i].cells[j].innerHTML);
	       }
	  }
  }
}
//查看评分项明细信息
function find(type){
	createdetailwindow("专家评标","findBidItemDetail_bidItems.action?itemId="+type,4);
	}

