//流程审批提交校验
function submitWorkFlow(param){
	var signContent = document.getElementById("signContent").value;
	//if(signContent==''){
	//	showMsg("alert","温馨提示：审批意见不能为空！");
	//	return false;
	//}else{
		$.dialog.confirm("温馨提示：您确定要提交审批吗？",function(){
			
			$(".btn-success").css("display","none");
			document.getElementById("result").value=param;
			//alert(document.getElementById("result").value);
			document.forms[0].action = "saveInitiatorToApprove_bidworkflow.action";
			document.forms[0].submit();
		},function(){},api)
//}
}
//保存委托代理初始化
function addSurrogate(){
	window.location.href="saveSurrogateInfoInit_workflow.action";
}
//保存委托代理
function submitSurrogate(){
 	if(confirm("温馨提示：确定要提交信息吗？")){
 		document.forms[0].submit();
 	}else{
 		return false;
 	}
}
//日期判断
function validateBidsDate(){
	var openDate = document.getElementById("startDate").value.replace(/-/g, "/");//起始日期
	var endDate = document.getElementById("endDate").value.replace(/-/g, "/");//结束日期
	if(openDate!=""){
		if(endDate<openDate){
			showMsg("alert","结束时间不能小于开始时间!");
			document.getElementById("endDate").value="";
		}
	}
}
//全部或部分委托
function surrogateChange(value){
	if(value=='0'){
		document.getElementById("processName").disabled=true;
	}
	if(value=='1'){
		if(document.getElementById("processName").disabled==true){
			document.getElementById("processName").disabled=false;
		}
	}
}
//校验是否选择委托类型
function surrogateType(){
	var isCheck = document.getElementById("part").checked;
	if(isCheck!=true){
		showMsg("alert","温馨提示：请选择委托类型");
		return ;
	}
}


//发起人提交
function initApplySubmit(){
	//var deptId = $("#deptId").val();
	var processId = $("#processId").val();
	var orderNo = $("#orderNo").val();
	var taskId = $("#taskId").val();
	var orderId = $("#orderId").val();
	var signContent = $("#signContent").val();
		//if(signContent!=""){
			lhgdialog.confirm("温馨提示：您确定要提交信息吗？",function(){
				$(".btn-success").css("display","none");
				document.forms[0].action = "saveInitiatorToApply_bidworkflow.action";
				document.forms[0].submit();
				return true;
			},function(){	
				return false;
			},api);
		//}else{
		//	showMsg("alert","温馨提示：请输入申请人意见！");
		//	return false;
		//}
}
function checkReqLen(length){ 
	var value = $("#description").val();
  if(value.length>500){
  	$("#description").val(value.substr(0,500));
      document.getElementById("reqIntroduce").value =500; 
   }else{
      document.getElementById("reqIntroduce").value = value.length; 
   }
  
} 