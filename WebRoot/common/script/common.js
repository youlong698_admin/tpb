//ajax通用js方法，带返回值
function ajaxGeneral(action,param){
		var result = "";
		$.ajax({
			type:"POST",
			async:false,
			url:action,
			data:param,
			dataType:"text",
			success:function(msg){
				result = msg;
			},
			error:function(error){
				result = error;
			}
		});
		return result;
}

function ajaxGeneral(action,param,type){
	var result = "";
	$.ajax({
		type:"POST",
		async:false,
		url:action,
		data:param,
		dataType:type,
		success:function(msg){
			result = msg;
		},
		error:function(error){
			result = error;
		}
	});
	return result;
}
/**********限制审批意见输入字符长度Ushine,2014-11-07**********/
function  getStringLen(Str){     
	var i,len,code;     
	if(Str==null || Str == "")   
	return   0;     
	len = Str.length;     
		for(i=0; i<Str.length; i++){       
    	code = Str.charCodeAt(i);     
    		if(code > 255) {
    			len ++;
   		 	}     
	}     
	return   len;     
} 
//检查字符串长度
function checkLen(length){ 
	var value = $("#signContent").val();
    var len = getStringLen(value);
    if(len>=length){
    	alert("温馨提示：已经超过规定字数，请重新输入！");
    }
    document.getElementById("YesCou03").value = len; 
}
//竞谈 综合评分汇总 列表页--摘要字段显示颜色
function color(supName){
	var color = "<span style='color:red'>"+supName+"</span>";
	return color;	
}
//校验清单页面所有日期
   function validateDate(id){
	   	var date = new Date(document.getElementById(id).value.replace(/\-/g, "\/"));
	   	if(date!=""&&date!=undefined&&date!=null){
	   		var inputDate = getNowStringDate(date);
	   	   	var nowDate = getNowStringDate(new Date());
	   		// 判断输入日期不能小于当前日期(Ushine,2014-10-25)
	   		/*if(inputDate < nowDate){
	    		alert("温馨提示：不能小于当前日期！");
	    		document.getElementById(id).value = "";
	     	}*/
	   	   	if(date==""&&date==undefined&&date==null){
	   	   		alert("温馨提示：日期不能为空！");
	   	   	}
	   	}
   }
   /**
    * 判断输入内容是否为英文，数字，-，_
    * @param id
    * @return
    */
   	///^[0-9A-Za-z-_]{0,5}$/
   	///^[[A-Za-z0-9]{5}]+$/
      	function checkCode(value){
      		if(value.match(/^[0-9A-Za-z-_]{0,20}$/)){
      			
      			}else{
      				alert("温馨提示：请输入正确的编码！");
      			}
      		}

     //数字校验
   	function validateNum(obj){
   	   if(isNaN(obj.value)){
   	      document.getElementById(obj.id).value="";
   	      return ;
   	   }
   	}

  var dialogG,Wdialog,WWdialog;
  /**
   * 查看时的弹出窗口
   * 
   * @param title
   * @param addurl
   * @param saveurl
   */
  function createdetailwindow(title, addurl,type) {
	 var width = window.top.document.documentElement.clientWidth-100;
	 var height = window.top.document.documentElement.clientHeight-100;
	  if(type==1)
	  {
		  width=1000;
		  height=550;
	  }else if(type==2)
	  {
		  width=800;
		  height=450;
	  }else if(type==3)
	  {
		  width=1100;
		  height=700;
	  }else if(type==4)
	  {
		  width=400;
		  height=400;
	  }else if(type==5)
	  {
		  width=400;
		  height=550;
	  }else if(type==6)
	  {
		  width=700;
		  height=400;
	  }
	 
	  
	  if(window.frameElement && window.frameElement.api){
		  if( !Wdialog || Wdialog.closed ){
		  Wdialog=W.$.dialog({
			id:'Wdialog',
  			content: 'url:'+addurl,
  			lock : true,
  			min:true,
		  	max:true,
  			width:width,
  			height:height,
  			title:title,
  			background: '#000', /* 背景色 */
  		    opacity: 0.5,       /* 透明度 */
  		    parent:api,
  		    resize:false,
  			cache:true
  		});
		  }else{
			  WWdialog=W.$.dialog({
					id:'WWdialog',
		  			content: 'url:'+addurl,
		  			lock : true,
		  			min:true,
				  	max:true,
		  			width:width,
		  			height:height,
		  			title:title,
		  			background: '#000', /* 背景色 */
		  		    opacity: 0.5,       /* 透明度 */
		  		    parent:Wdialog,
		  		    resize:false,
		  			cache:true
			  });
		  }
	}else{
		if(!dialogG||!dialogG.get('dialog'))
	    { 
	    }else{
	    	dialogG.close();
	    }
		dialogG=$.dialog({
  			id:'dialog',
  			content: 'url:'+addurl,
  			lock : true,
  			min:true,
		  	max:true,
  			width:width,
  			height:height,
  			title:title,
  			background: '#000', /* 背景色 */
  		    opacity: 0.5,       /* 透明度 */
  		    resize:false,
  			cache:true
  		});
	}
}
  /**
   * 查看时的弹出窗口
   * 
   * @param title
   * @param addurl
   * @param saveurl
   */
  function createTwindow(id,title, addurl,type) {
	 var width = window.top.document.body.offsetWidth-100;
	 var height = window.top.document.body.offsetHeight;
	  if(type==1)
	  {
		  width=1000;
		  height=500;
	  }else if(type==2)
	  {
		  width=800;
		  height=450;
	  }else if(type==3)
	  {
		  width=1100;
		  height=700;
	  }else if(type==4)
	  {
		  width=400;
		  height=400;
	  }else if(type==5)
	  {
		  width=400;
		  height=550;
	  }
	  
	  W.$.dialog({
			id:id,
  			content: 'url:'+addurl,
  			lock : true,
  			min:true,
		  	max:true,
  			width:width,
  			height:height,
  			title:title,
  			background: '#000', /* 背景色 */
  		    opacity: 0.5,       /* 透明度 */
  		    parent:api,
  		    resize:false,
  			cache:false
  		});
		  
}
  /**
   * 固定弹框大小
   * @param title
   * @param addurl
   * @param type
   * @return
   */
  function createdetailwindow_choose(title, addurl,type) {
		 var width = window.top.document.body.offsetWidth-100;
		 var height = window.top.document.body.offsetHeight;
		 if(type==0)
		  {
			  width=1100;
			  height=500;
		  } else  if(type==1)
		  {
			  width=1000;
			  height=560;
		  }else if(type==2)
		  {
			  width=450;
			  height=560;
		  }else if(type==3)
		  {
			  width=750;
			  height=350;
		  }else if(type==4)
		  {
			  width=500;
			  height=300;
		  }else if(type==5)
		  {
			  width=400;
			  height=400;
		  }
		  
		  if(window.frameElement && window.frameElement.api){
			  W.$.dialog({
				id:'WdialogT',
	  			content: 'url:'+addurl,
	  			lock : true,
	  			min:false,
			  	max:false,
	  			width:width,
	  			height:height,
	  			title:title,
	  			background: '#000', /* 背景色 */
	  		    opacity: 0.5,       /* 透明度 */
	  		    parent:api,
	  		    resize:false,
	  			cache:false
	  		});
		}else{
	  		$.dialog({
	  			id:'dialog',
	  			content: 'url:'+addurl,
	  			lock : true,
	  			min:false,
			  	max:false,
	  			width:width,
	  			height:height,
	  			title:title,
	  			background: '#000', /* 背景色 */
	  		    opacity: 0.5,       /* 透明度 */
	  		    resize:false,
	  			cache:false
	  		});
		}
	}
//js 数字计算防止误差  两数相加
    function FloatAdd(arg1,arg2){   
    	   var r1,r2,m;   
    	   try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}   
    	   try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}   
    	   m=Math.pow(20,Math.max(r1,r2))   
    	   return (arg1*m+arg2*m)/m   
    	 } 
  //js 数字计算防止误差  两数相减
    function FloatSub(arg1,arg2){   
    	var r1,r2,m,n;   
    	try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}   
    	try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}   
    	m=Math.pow(10,Math.max(r1,r2));     
    	n=(r1>=r2)?r1:r2;   
    	return ((arg1*m-arg2*m)/m).toFixed(n);   
    	} 

    function showMsg(type,msg,onclose){//type:'alert','success','error'
   		if(arguments.length==1){
   			msg = arguments[0];
   			type='alert';
   		}
   		if(msg.indexOf('<')==-1){
   			var style="font-size:13px;";
   			if(msg.length<20)
   				style+="width:250px;text-align: left;";
   			msg='<div style="'+style+'">'+msg+'</div>';
   		}
   		//依赖window.$对象，如果在打开的窗口中弹出，调用opener.$；否则异步按需加载lhgdialog
   		//window.$是在jquery模块中导出到window里的
   		if(window.frameElement && window.frameElement.api){
   			window.frameElement.api.opener.$.dialog.alertMore(type,msg,onclose,window.frameElement.api);
   		}else{
   			$.dialog.alertMore(type,msg,onclose);
   		}
   	}
    /**
     * 门户网站 提示信息
     * @param type
     * @param msg
     * @param onclose
     * @return
     */
    function showMsgWeb(type,msg,onclose){//type:'alert','success','error'
   		 if(type=="alert"){
   			$.dialog.tips(msg,3,"hits.png",onclose);
   		 }else if(type=="success"){
   			$.dialog.tips(msg,3,"succ.png",onclose);
   		 }else if(type=="error"){
   			$.dialog.tips(msg,3,"fail.png",onclose);
   		 }
   	}
    function switchTab(tabpage,tabid){
   	    var dvs=document.getElementById("cnt").getElementsByTagName("div");
   		for (var i=0;i<dvs.length;i++){
   		  if (dvs[i].id==('d'+tabid)){
   		    dvs[i].style.display='block';
   		  }else{
   	  	  	if(dvs[i].className!='operaItem'&&dvs[i].id.indexOf('dTab')>=0){
   	  	  		dvs[i].style.display='none';
   	  	  	}
   	  	  }	
   		}
   	} 
    function switchTabUrl(tabid,url){
   		if(tabid=="Tab3"){
	         $('#iframe').attr('src',url);
  		}
   	    var dvs=document.getElementById("cnt").getElementsByTagName("div");
   		for (var i=0;i<dvs.length;i++){
   		  if (dvs[i].id==('d'+tabid)){
   		    dvs[i].style.display='block';
   		  }else{
   	  	  	if(dvs[i].className!='operaItem'&&dvs[i].id.indexOf('dTab')>=0){
   	  	  		dvs[i].style.display='none';
   	  	  	}
   	  	  }	
   		}
   	}
    function openWindow(title,url,type){
    	 window.parent.openWindowFrame(title,url,type);	    	
	 }
    //供应商考核所用  日期转换
    function getNowStringDate(now){
	   var nowYear = now.getFullYear();
	   var nowMonth = now.getMonth();
	   var nowDate = now.getDate();
	   newdate = new Date(nowYear,nowMonth,nowDate);
	   nowMonth = doHandleMonth(nowMonth + 1);
	   nowDate = doHandleMonth(nowDate);
	   return nowYear+"-"+nowMonth+"-"+nowDate;
   }
    function doHandleMonth(month){
	   if(month.toString().length == 1){
	    month = "0"+month;
	   }
	   return month;
    }
    function nextMonthDate(){
        var Nowdate = new Date();
        
        var vYear = Nowdate.getFullYear();
        var vMon = Nowdate.getMonth() + 1;
        var vDay = Nowdate.getDate();
    　　//每个月的最后一天日期（为了使用月份便于查找，数组第一位设为0）
        var daysInMonth = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31);
        if(vMon==12){
            vYear = Nowdate.getFullYear()+1;
            vMon = 1;
        }else{
            vMon = vMon +1;
        }
    　　//若是闰年，二月最后一天是29号
        if(vYear%4 == 0 && vYear%100 != 0){
            daysInMonth[2]= 29;
        }
        if(daysInMonth[vMon] < vDay){
            vDay = daysInMonth[vMon];
        }
        if(vDay<10){
            vDay="0"+vDay;
        }
        if(vMon<10){
            vMon="0"+vMon;
        }
        var date =vYear+"-"+ vMon +"-"+vDay;
        return date;
    }
    //查看历史价格曲线分析
    function viewHistoryPrice(materialId){
    	createdetailwindow("历史价格分析","viewHistoryPrice_priceLibrary.action?materialId="+materialId,1)
    }