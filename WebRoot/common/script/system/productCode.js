	//生成
	function save(){		
		if($("#packageName").val()==""){
			$("#packageName").focus();
			showMsg("alert","错误提示：输入包名！");
			return false;
		}else{
			var pat = new RegExp("^[A-Za-z\.]+$");
			if(!pat.test($("#packageName").val())){
				$("#packageName").focus();
				showMsg("alert","错误提示：只能输入字母！");
				return false;
			}
		}
		$("#Form").submit();	
	}
	
	function doChange(value){
		window.location.href="viewToProductCode_sysSet.action?tableName="+value;		
	}