//保存需求计划
function save(){
	//debugger;
	/*新增需求计划时，判断不能为空*/
	var rmType = document.getElementById('rmType').value;
	var blankNum = 0;
	var temp = /^([1-9][\d]{0,9}|0)(\.[\d]{1,6})?$/;  //估算单价是否是数字格式
	var temp2 = /^[0-9]*[1-9][0-9]*$/;  //采购数量正整数
	//获得当前列表最大行数
	var trObj = document.getElementsByName("rowIndex");
	if(trObj.length==0){
        showMsg("alert","温馨提示：请添加计划物资明细！");
        return false;
    }
	var trNum;
	//校验物资类别
		var budget = 0;
		for (var i = 0; i < trObj.length; i++) {
			trNum=trObj[i].value;
			if(typeof($('#deliverDate_'+trNum).val())!='undefined'){
				if ($('#buyCode_'+trNum).val()=="") {
					showMsg("alert","温馨提示：第"+(i+1)+"行的物资编码不能为空！");
					blankNum++;
					return false;
				}
				if (temp.test($('#amount_'+trNum).val())==false||$('#amount_'+trNum).val()=="0"||$('#amount_'+trNum).val()=="0.00") {
					showMsg("alert","温馨提示：第"+(i+1)+"行的采购数量为空或无效！");
					document.getElementById('amount_'+trNum).value="";
					blankNum++;
					return false;
				}else{
					amount = $('#amount_'+trNum).val();
				}
			}
		}
	
	//保存需求计划
	if(blankNum==0){
		//提交之前把选择的附件信息填充值
		$("#fileNameData").val(fileNameData);
		$("#uuIdData").val(uuIdData);
		$("#fileTypeData").val(fileTypeData);
		$("#attIdData").val(attIdData);
		
        document.forms[0].submit();
	}
}

//校验交货时间
function deliverDateFor(x){
	var deliverDate = new Date(document.getElementById("deliverDate_"+x).value.replace(/\-/g, "\/"));
	
	if(deliverDate != "" && deliverDate != undefined && deliverDate != null){
		var sDate = getNowStringDate(deliverDate);
	   	var eDate = getNowStringDate(new Date());
		// 判断预计采购时间不能小于当前日期(Ushine,2014-10-25)
		if(sDate < eDate){
     		showMsg("alert","温馨提示：交货时间不能小于当前日期！");
     		$('#deliverDate_'+x).val("");
  		}
	}
}

//流程跟踪（需求）
function followProcessPlan(rmCode){
	//showMsg("alert",rmCode);
	var url = "followAndViewProcessPlan_requiredPlan.action?param="+rmCode;
	url=encodeURI(url);
	createdetailwindow('流程追踪',url,1,4);
}

//自动计算总价和合计（先输入数量）
function caculatePriceByAmt(obj,rowIndex){
	var temp = /^([0-9.]+)$/;
	if(temp.test(obj.value)==false){
		obj.value="";
	}else{
		var amount = obj.value;
		var ePrice = $("#estimatePrice_"+(rowIndex)).val();
		//showMsg("alert",amount+":"+ePrice+":rowIndex:"+rowIndex);
		if(ePrice=="" ) ePrice=0;
		if(amount=="" ) amount=0;
		var price = parseFloat(amount)*parseFloat(ePrice);
		document.getElementById("estimateSumPrice_"+(rowIndex)).innerHTML=price.toFixed(2);
		document.getElementById("estimateSumPrice_s_"+(rowIndex)).value=price.toFixed(2);
	}
	
	//获得当前列表最大行数
	var trObj = document.getElementsByName("rowIndex");
	var sumPric=0;
	var trNum;
	for (var i = 0; i <  trObj.length; i++) {
		trNum=trObj[i].value;
		sumPric+= parseFloat(document.getElementById("estimateSumPrice_s_"+trNum).value);
	}
	document.getElementById('totalBudget').value=sumPric;
	document.getElementById('totalBudget_s').innerHTML=sumPric;
}
//自动计算总价和合计（先输入单价）
function caculatePrice(obj,rowIndex){
	var temp = /^([0-9.]+)$/;
	
	//showMsg("alert",rowIndex+"=="+obj.id);
	if(temp.test($("#"+obj.id).val())==false){
		$("#"+obj.id).val("");
	}else{
		var amount = $("#amount_"+(rowIndex)).val();
		var ePrice = $("#"+obj.id).val();
		if(ePrice=="" ) ePrice=0;
		if(amount=="" ) amount=0;
		var price = parseFloat(amount)*parseFloat(ePrice);
		//alert(amount+"---"+price+"---"+ePrice+"----"+rowIndex);
		//alert(amount+":"+ePrice+":"+price);
		
		document.getElementById("estimateSumPrice_"+(rowIndex)).innerHTML=price.toFixed(2);
		document.getElementById("estimateSumPrice_s_"+(rowIndex)).value=price.toFixed(2);
	}
	
	//获得当前列表最大行数
	var trObj = document.getElementsByName("rowIndex");
	var trNum;
	var sumPric=0;
	for (var i = 0; i < trObj.length; i++) {
		trNum=trObj[i].value;
		sumPric+= parseFloat(document.getElementById("estimateSumPrice_s_"+trNum).value);
	}
	document.getElementById('totalBudget').value=sumPric;
	document.getElementById('totalBudget_s').innerHTML=sumPric;
}
