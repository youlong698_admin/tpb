/*常量*/
var CONSTANT = {
		DATA_TABLES : {
			DEFAULT_OPTION : { //DataTables初始化选项
	            language: {
					"sProcessing":   "处理中...",
					"sLengthMenu":   "每页 _MENU_ 项",
					"sZeroRecords":  "没有匹配结果",
					"sInfo":         "当前显示第_START_至_END_项，共 _TOTAL_项。",
					"sInfoEmpty":    "当前显示第 0 至 0 项，共 0 项",
					"sInfoFiltered": "(由 _MAX_ 项结果过滤)",
					"sInfoPostFix":  "",
					"sSearch":       "搜索:",
					"sUrl":          "",
					"sEmptyTable":     "表中数据为空",
					"sLoadingRecords": "载入中...",
					"sInfoThousands":  ",",
					"oPaginate": {
						"sFirst":    "首页",
						"sPrevious": "上页",
						"sNext":     "下页",
						"sLast":     "末页",
						"sJump":     "跳转"
					},
					"oAria": {
						"sSortAscending":  ": 以升序排列此列",
						"sSortDescending": ": 以降序排列此列"
					}
				},
                autoWidth: false,	//禁用自动调整列宽
                fixedHeader: true,
                stripeClasses: ["odd", "even"],//为奇偶行加上样式，兼容不支持CSS伪类的场合
                order: [],			//取消默认排序查询,否则复选框一列会出现小箭头
                processing: false,	//隐藏加载提示,自行处理
                serverSide: true,	//启用服务器端分页
                searching: false	//禁用原生搜索
			},
			SELECT_DEFAULT_OPTION : { //DataTables初始化选项
				dom:"<'row-fluid'r>" +
				"<'row-fluid'<'span12't>>" +
				"ip",
	            language: {
					"sProcessing":   "处理中...",
					"sLengthMenu":   "每页 _MENU_ 项",
					"sZeroRecords":  "没有匹配结果",
					"sInfo":         "当前显示第_START_至_END_项，共 _TOTAL_项。",
					"sInfoEmpty":    "当前显示第 0 至 0 项，共 0 项",
					"sInfoFiltered": "(由 _MAX_ 项结果过滤)",
					"sInfoPostFix":  "",
					"sSearch":       "搜索:",
					"sUrl":          "",
					"sEmptyTable":     "表中数据为空",
					"sLoadingRecords": "载入中...",
					"sInfoThousands":  ",",
					"oPaginate": {
						"sFirst":    "首页",
						"sPrevious": "上页",
						"sNext":     "下页",
						"sLast":     "末页",
						"sJump":     "跳转"
					},
					"oAria": {
						"sSortAscending":  ": 以升序排列此列",
						"sSortDescending": ": 以降序排列此列"
					}
				},
                autoWidth: false,	//禁用自动调整列宽
                fixedHeader: true,
                stripeClasses: ["odd", "even"],//为奇偶行加上样式，兼容不支持CSS伪类的场合
                order: [],			//取消默认排序查询,否则复选框一列会出现小箭头
                processing: false,	//隐藏加载提示,自行处理
                serverSide: true,	//启用服务器端分页
                searching: false	//禁用原生搜索                
			},
			COLUMN: {
                CHECKBOX: {	//复选框单元格
                    className: "td-checkbox",
                    orderable: false,
                    width: "30px",
                    data: null,
                    render: function (data, type, row, meta) {
                        return '<input type="checkbox">';
                    }
                },
                SELECTPAGECHECKBOX: {	//复选框单元格
                    className: "td-checkbox",
                    orderable: false,
                    width: "2%",
                    data: null,
                    render: function (data, type, row, meta) {
                        return '<input type="checkbox">';
                    }
                }
            },
            RENDER: {	//常用render可以抽取出来，如日期时间、头像等
                ELLIPSIS:function (data, type, row, meta) {
                	data = data||"";
                	return '<span title="' + data + '">' + data + '</span>';
                },
                RMTYPE:function(data,type, row, meta) {
                    var rmtype = "";
					if(data == "00"){rmtype = "普通计划";}
					if(data == "01"){rmtype = "加急计划";}
					return rmtype; 
                },
                BUYTYPE:function(data,type, row, meta) {
                    var buytype = "";
					if(data == "0"){buytype = "自采";}
					if(data == "1"){buytype = "集采";}
					return buytype; 
                },
                //价格
                NUMBER_PRICE:function( data,type, row, meta ) {
                	var f_x=Math.round(data*100)/100;
                    var s_x = f_x.toString();
                    var pos_decimal = s_x.indexOf('.');
                    if (pos_decimal < 0) {
                        pos_decimal = s_x.length;
                        s_x += '.';
                    }
                    while (s_x.length <= pos_decimal + 2) {
                        s_x += '0';
                    }
                    return s_x;
                 },
              //普通数字  利于以后扩展
                NUMBER:function( data,type, row, meta ) {
                    return Math.round(data*100)/100;
                 },
                 //采购方式
                 BUYWAY:function( data,type, row, meta ) {
                	 var buytype = "";
	             		if(data == "00"){buytype = "招标";}
	             		if(data == "01"){buytype = "询价";}
	             		if(data == "02"){buytype = "竞价";}
	             		if(data == "08"){buytype = "自主采购";}
	             		return buytype;
                 },
               //供应商选择方式
                 SUPPLIERTYPE:function( data,type, row, meta ) {
                	 var supplierType = "";
	             		if(data == "00"){supplierType = "公开";}
	             		if(data == "01"){supplierType = "邀请";}
	             		return supplierType;
                 },
               //性别
                 SEX:function( data,type, row, meta ) {
                	 var expertSex = "";
         			if(row.expertSex == "0"){expertSex = "男";}
         			if(row.expertSex == "1"){expertSex = "女";}
         			return expertSex;
                 },
                 //系统内外
                 INOUT:function( data,type, row, meta ) {
                	 var inOut = "";
         			if(row.inOut == "0"){inOut = "系统内";}
         			if(row.inOut == "1"){inOut = "系统外";}
         			return inOut;
                 },
                 //专家类型
                 EXPERTTYPE:function( data,type, row, meta ) {
                	 var expertType = "";
         			if(row.expertType == "0"){expertType = "正式";}
         			if(row.expertType == "1"){expertType = "临时";}
         			return expertType;
                 }
                 	
            }
		}
};