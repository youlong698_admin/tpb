/***************************采购单位基本信息管理*****************************/

/***校验采购单位名称是否重复***/
function checkOutPageValueCom(obj){
	DwrService.ifExitCodeInTable("SysCompany", "compName", obj.value, set_viewCom );	
}
function set_viewCom(count){
	if( count >0){
		document.getElementById("infoDsp").innerHTML="该名称已存在,请重新输入!";
		document.getElementById("compName").focus();
		$('#submitButton').hide();
	}else{
		document.getElementById("infoDsp").innerHTML="";
		$('#submitButton').show();
	}
}
function checkOutPageValue(obj){
	DwrService.ifExitCodeInTable("SysCompany", "loginName", obj.value , set_view );
	DwrService.ifExitCodeInTable("Users", "userName", obj.value , set_view );	
}
/***校验用户名是否重复***/
function set_view(count){
	if( count >0){
		document.getElementById("infoDspLoginName").innerHTML="该名称已存在,请重新输入!";
		document.getElementById("loginName").focus();
		$('#submitButton').hide();
	}else{
		document.getElementById("infoDspLoginName").innerHTML="";
		$('#submitButton').show();
	}
}
function checkOutPageValueOrg(obj){
	DwrService.ifExitCodeInTable("SysCompany", "orgCode", obj.value , set_viewOrg );
}
/***校验组织机构代码证是否重复***/
function set_viewOrg(count){
	if( count >0){
		document.getElementById("infoDspOrg").innerHTML="该代码已存在,请重新输入!";
		document.getElementById("orgCode").focus();
		$('#submitButton').hide();
	}else{
		document.getElementById("infoDspOrg").innerHTML="";
		$('#submitButton').show();
	}
}
function openWindow(sign){
	createdetailwindow("注册协议须知","regAgencyIndex.action?sign="+sign,7);
}
function saveReg(){
		if ($('#isAgreed').attr('checked')) {
			document.sysCompany.action="sysCompanyRegIndex.action";
			document.sysCompany.submit();
		}else{
			showMsg("alert","温馨提示：请仔细阅读注册协议！");
		}
}

/*******选择注册代码证类型*********/
function doSelect(obj){
	var value=$("#registrationType").val();
	if(value=="01"){
		$('#registrationTypeTd').html("统一社会信用代码证号：");
		$('#registrationTypeDiv').html("统一社会信用代码证号不能为空！");
		$('#orgCode').attr("nullmsg","统一社会信用代码证号不能为空！");
		$('#registrationTypeTr').show();
	}else{
		$('#registrationTypeTd').html("组织机构代码证号：");
		$('#registrationTypeDiv').html("统一社会信用代码证号不能为空！");
		$('#orgCode').attr("nullmsg","组织机构代码证号不能为空！");
		$('#registrationTypeTr').show();
	}
}