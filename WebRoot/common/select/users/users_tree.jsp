<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>组织机构部门树</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		
		<link rel="stylesheet" href="<%=path %>/common/ace/assets/css/bootstrap.min.css" />
		
		<script src="<%=path %>/common/jQuery/jquery-1.8.3.min.js"></script>
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		<!--  <script src="<%=path %>/common/zTree/jquery.ztree.excheck.js"></script>-->
		<script type="text/javascript">
		var parent_id = null
		 var setting = {
			view: {
				selectedMulti: false,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			data: {
				simpleData: {
                          enable: true,
                          idKey: "id",
                          pIdKey: "pid",
                          rootPId: 0
                  }
			},
			check: {
				 enable: true,
               	 chkStyle: "checkbox",
                 chkboxType: { "Y": "p", "N": "s" }
			},
			async: {
				enable: true,
				dataType:"json",
				url: "viewDeptTree_userTree.action"
			},
			callback: {
				//onCheck: onCheck  //选中事件
				onClick: zTreeOnClick  //点击事件
			}
		};

		
		function zTreeOnClick(e, treeId, treeNode) {
			
			parent.MainTop.location.href = "viewTopIndex_userTree.action?deptId="+treeNode.id;
			
		}
		
		function loudShow() {
			parent.MainTop.location.href = "viewTopIndex_userTree.action?deptId=1";
		}
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		</script>
	</head>

	<body onload="loudShow()">
	
		<ul id="treeDemo" class="ztree"></ul>
		

	</body>
</html>