<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=gbk">
 <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<title>选择用户</title>

<script language="javaScript">
function ul(){
	var ul="<s:property value = '#request.ul'/>";
	if(ul!=-1){
		setDeptAndEmployee(ul);
	}
}
//增加用户
function setDeptAndEmployee(userId)
		
{		//showMsg("alert",url);
	var s=userId.split(",");
		
	for(var t=0;t<s.length;t++){
		var st = s[t].split(":");
		var sr =  divAll().split(",");
			var b=true;
			for(var j=0;j<sr.length;j++){
				var sd = sr[j].split(":");
				if(st[0] == sd[0]){
					b=false;
					break;
			}
		}
		if(b){
			addDiv( st[0],st[1],st[2] );
			}
	 }

}
//动态添加div 图形显示
function addDiv( url ,userName,point){
	//var s=document.getElementById("myDiv");
	//showMsg("alert",url);
	var str ="<tr onclick='del("+url+")' class='search-choice'><td><div class='div_buttom' id='"+url+"' name='"+url+":"+userName+":"+point+"'><font style='color:#438eb9;margin-right:10px;'><i class='icon icon-user' ></i></font><span id="+url+":"+userName+">"+userName+"</span><a class='search-choice-close'></a></div></tr></td>";
	var s = $("#mytable");
	s.append(str);

}
//删除选择的用户
function del(id){
	//showMsg("alert",id);
	folderview(id);
   $('#'+id).remove();
}

//top框架的方法
function folderview(url){
		window.parent.MainTop.setDeptAndEmployee(url);
	}
//获取所有的用户上传到上面框架
function divAll(){
var num = $('#mytable').find('div');
	//var d = document.getElementById("myDiv");
	//var divs = d.childNodes;
	var s ="";
	for(var i=0;i<num.length;i++)
{
		s += num.eq(i).attr('name')+",";
}
	return s;
	
}

function add(){
var num = $('#mytable').find('div');
	var s ="";
	for(var i=0;i<num.length;i++)
{
		s += num.eq(i).attr('name')+",";
}
if(s!=null&&s!=""){
	if(window.parent.api.get("Wdialog")){
	window.parent.api.get("Wdialog").document.getElementById('returnValues').value=s.substring(0,s.lastIndexOf(","));
	window.parent.api.get("Wdialog").valueEvaIndex();
	}else{
	window.parent.W.document.getElementById('returnValues').value=s.substring(0,s.lastIndexOf(","));
	window.parent.W.valueEvaIndex();	
	}
	window.parent.api.close();
	
}else{
//showMsg("alert",s);
	showMsg("alert","请选择考核指标！");
}
}
function windowClose() {
		parent.api.close();
	}
		
</script>
<style type="text/css">
	.div_buttom{padding-left:5px;padding-right:5px;margin-top:5px;border:1px solid #ccc;background:#f9f9f9;font-size: 12px;}
	.div_buttom:hover{background:#fff;}
</style>
</head>
<body  onload="ul();">
<div style="width:100%;">
<div style="height:33px;background:#eff3f8;padding-top:10px;">
	<font color="red">已选用户（单击即可删除）：</font>
</div>


<form id="myform"  method="POST" action="">
<div id="myDiv" style="height:455px;overflow:auto;">
<table id ="mytable" width="96%"  border="0" align="center" cellpadding="0" cellspacing="10">

</table>
</div>
</form>
<div align="right" style="line-height:40px;margin-bottom:0px;margin-right: 3px">
	<button class="btn btn-success" id="btn-save" onClick="add();"><i class="icon-white icon-ok-sign"></i>确定</button>
	<button class="btn btn-danger" type="button" onClick="windowClose();" id="btn-danger" ><i class="icon-white icon-remove-sign"></i>关闭</button>
</div>
</div>
</body>
</html>