<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>组织机构树</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		
        <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
		<script src="<%=path %>/common/jQuery/jquery-1.8.3.min.js"></script>
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		 <script src="<%=path %>/common/zTree/jquery.ztree.excheck.js"></script>
		<script type="text/javascript"><!--
		var api = frameElement.api, W = api.opener;
		var mar;
		 var setting = {
			view: {
				selectedMulti: true,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			check: {
				enable: true,
				chkboxType:  { "Y": "", "N": "" }
			},			
			data: {
				simpleData: {
                          enable: true,
                          idKey: "id",
                          pIdKey: "pid",
                          rootPId: 0
                  }
			},
			async: {
				enable: true,
				dataType:"json",
				url: "viewDeptTree_userTree.action"
			},
			callback: {
				onCheck: zTreeOnClick  //选中事件
				//onClick: zTreeOnClick  //点击事件
			}
		};

		
		function zTreeOnClick(e, treeId, treeNode) {
			
			//alert(mar);
			//parent.MainFrame.location.href = "updateDepartmentsInit_dept.action?departments.depId="+treeNode.id;
			
		}
		
		

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		
		$(function(){
			 //确定事件
	         $('#btn-save').on('click', function() {
	        	
	        	var checkCount;
				//获取所选择的数据
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				checkCount = zTree.getCheckedNodes(true),
				mar="";
				
				for(var i=0;i<checkCount.length;i++)
				{
					mar+=checkCount[i].id+":"+checkCount[i].name+",";
					
				}
				//alert(checkCount.length);
				if(checkCount.length==0){
	      			
	      			showMsg("alert","温馨提示：请选择组织机构"); 
	       			return false;
				}
				
	  			//父页面一定要有id 为 returnVals 的 input 和 valueEvaluateDept()函数
				if(api.get("Wdialog")){
				    api.get("Wdialog").document.getElementById('returnValues').value=mar;
					api.get("Wdialog").valueEvaluateDept();
				}else if(api.get("dialog")){
				    api.get("dialog").document.getElementById('returnValues').value=mar;
					api.get("dialog").valueEvaluateDept();
				}else{			  
					W.document.getElementById('returnValues').value=mar;
					W.valueEvaluateDept();
				}
				api.close();
				
			});
			
			//关闭事件
			$("#btn-danger").click(function(){
				api.close();
			})
		})
		--></script>
	</head>

	<body>
	
		<input type="hidden" id="proKindIds" name="" value="<s:property value="#request.proKindIds"/>"/>
	
		<div  style="height: 350px;overflow: auto">
			<ul id="treeDemo" class="ztree"></ul>
		</div>
		 <div class="buttonDiv" align="right">	
				<button type="button" class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i> 确定</button>
				<button type="button" class="btn btn-danger" id=btn-danger><i class="icon-white icon-remove-sign"></i> 关闭</button>
				
			</div>
		
		
	</body>
</html>