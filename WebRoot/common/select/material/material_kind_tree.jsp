<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>采购类别管理</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		
		<script src="<%=path %>/common/jQuery/jquery-1.8.3.min.js"></script>
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		<script type="text/javascript">
		var parent_id = null
		 var setting = {
			view: {
				selectedMulti: false,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			async: {
				enable: true,
				dataType:"json",
				url: "viewMaterialKindTree_material.action?mkId=${id}",
				autoParam: ["id"]
			},
			callback: {
				//onCheck: onCheck  //选中事件
				onClick: zTreeOnClick  //点击事件
			}
		};

		
		function zTreeOnClick(e, treeId, treeNode) {
		    if(treeNode.isParent){
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.expandNode(treeNode);
			}else{
			parent.MainFrame.location.href = "viewMaterialList_materialTree.action?materialKind.mkId="+treeNode.id;
			}
			
		}
		
		function loudShow() {
			parent.MainFrame.location.href = "viewMaterialList_materialTree.action?materialKind.mkId=${id}";
		
		}
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		</script>
	</head>

	<body onload="loudShow()">
	
		<ul id="treeDemo" class="ztree"></ul>
		

	</body>
</html>