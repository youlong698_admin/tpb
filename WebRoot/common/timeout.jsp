<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
	<head>
		<title>登录超时</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
		<script language="javascript">
			showMsg('alert',"由于您长时间未操作，请重新登录！",function(){
			parent.parent.parent.parent.window.location.href = "<%=basePath%>/index.html"
			});
		</script>
</head>
<body>
</body>
</html>