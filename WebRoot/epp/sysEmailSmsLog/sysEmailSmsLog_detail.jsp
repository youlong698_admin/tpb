<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看用户邮件短信日志</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
   var api = frameElement.api, W = api.opener;	
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="" >

<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">用户邮件短信日志明细</td>
		    </tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">主键：</td>
			    <td width="70%" class="Content_tab_style2">{sysEmailSmsLog.seslId}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">类型 0 短信 1邮件：</td>
			    <td width="70%" class="Content_tab_style2">{sysEmailSmsLog.type}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">地址：</td>
			    <td width="70%" class="Content_tab_style2">{sysEmailSmsLog.address}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">标题：</td>
			    <td width="70%" class="Content_tab_style2">{sysEmailSmsLog.title}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">发送时间：</td>
			    <td width="70%" class="Content_tab_style2"><fmt:formatDate value="{sysEmailSmsLog.sendDate}" pattern="yyyy-MM-dd" /></td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">发送人：</td>
			    <td width="70%" class="Content_tab_style2">{sysEmailSmsLog.sendName}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">COM_ID：</td>
			    <td width="70%" class="Content_tab_style2">{sysEmailSmsLog.comId}</td>
			</tr>
		</table>
</div>
</div>
</form>
</body>
</html> 	