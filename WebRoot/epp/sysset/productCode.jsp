<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>修改密码</title>
    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
    <script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/system/productCode.js"></script>
	<script type="text/javascript">
	     var api = frameElement.api, W = api.opener;
	</script>
	</head>
<body>
	<form action="saveProCode_sysSet.action" name="Form" id="Form" method="post">
		<input type="hidden" name="zindex" id="zindex" value="0">
		<div id="zhongxin">		
		<table border="0">
			<tr>
				<td style="width:76px;text-align: right;">表名：</td>
				<td>
				<select name="objectName" id="objectName" onchange="doChange(this.value)" style="width: 300px">
				  <option value="">--请选择--</option>
				     <c:forEach items="${tableList}" var="table">
				       <c:if test='${table.tableName==tableNameSelected}'>
				          <option value="${table.tableName }" selected="selected">${table.comments }</option>
				       </c:if>
				       <c:if test='${table.tableName!=tableNameSelected}'>
				          <option value="${table.tableName }">${table.comments }</option>
				       </c:if>
				     </c:forEach>
				</select>
				</td>
				<td style="width:76px;text-align: right;">表注释：</td>
				<td>
				<input name="objectComments" id="objectComments" style="width: 300px" value=""/>
				</td>
			</tr>
		</table>
		
		<table style="margin-top: 10px;" border="0">
			<tr>
				<td style="width:76px;text-align: right;">上级包名：</td>
				<td colspan="1"><input type="text" name="packageName" id="packageName" value="" placeholder="这里输入包名  (请不要输入特殊字符,请用纯字母)" style="width:370px" title="包名称"/></td>
				<td>&nbsp;&nbsp;例如:com.ced.sip.<font color="red" style="font-weight: bold;">system</font>&nbsp;&nbsp;红色部分</td>
			</tr>
		</table>
		
		
		<table id="table_report" class="table table-striped table-bordered table-hover">
				
				<thead>
					<tr>
						<th class="center">属性名</th>
						<th class="center">类型</th>
						<th class="center">长度</th>
						<th class="center">精度</th>
						<th class="center">位数</th>
						<th class="center">备注</th>
						<th class="center" style="width:59px;">前台录入</th>
						<th class="center" style="width:100px;">文本类型</th>
					</tr>
				</thead>
										
				<tbody id="fields">
				     <c:forEach items="${columnsList}" var="column" varStatus="status">
				        <tr>
				           <td class="center">${column.columnName }<input type="hidden" name="field0${status.index }" value="${column.columnName }"></td>
				           <td class="center">${column.dataType }<input type="hidden" name="field1${status.index }" value="${column.dataType }"></td>
				           <td class="center">${column.dataLength }<input type="hidden" name="field2${status.index }" value="${column.dataLength }"></td>
				           <td class="center">${column.dataPrecision }<input type="hidden" name="field3${status.index }" value="${column.dataPrecision }"></td>
				           <td class="center">${column.dataScale }<input type="hidden" name="field4${status.index }" value="${column.dataScale }"></td>
				           <td class="center">${column.comments }<input type="hidden" name="field5${status.index }" value="${column.comments }">
				           <input type="hidden" name="field" value="${status.index }"></td>
				           <td class="center">
				           <select name="field8${status.index }" style="width: 50px">
				              <option value="是">是</option>
				              <option value="否">否</option>
				           </select></td>
				           <td class="center">
				           <select name="field9${status.index }" style="width: 100px">
				              <option value="1">短文本</option>
				              <option value="2">长文本</option>
				              <option value="3">单选</option>
				           </select></td>
				        </tr>
				     </c:forEach>
				</tbody>
				
		</table>
		
		
		<table id="table_report" class="table table-striped table-bordered table-hover">
			<tr>
				<td style="text-align: center;" colspan="100">
					<button class="btn  btn-success"  type="button"  onclick="save();" id="productc"><i class="icon-white icon-print"></i>生成</button>
					<button class="btn  btn-danger"  type="button" onclick="api.close();"><i class="icon-white icon-share-alt"></i>取消</button>
				</td>
			</tr>
		</table>
		</div>
	</form>
	
	
</body>
</html>