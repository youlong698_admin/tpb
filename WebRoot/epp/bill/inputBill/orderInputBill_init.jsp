<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>收货信息初始化页面</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%= path %>/common/script/context_form.js" type="text/javascript" ></script>
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
var api = frameElement.api, W = api.opener;
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="InputBill"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
var fileNameData=[];//已上传的文件名
var fileTypeData=[];//已上传的文件的格式
var attIdData=[];//已存入附件表的附件信息
   function submitConfirm(sign){
   $.dialog.confirm("温馨提示：您确定要提交吗？",function(){
      $("#status").val(sign);
      document.form.submit();
      },function(){},api)
   }
   function account(obj,sendAmount){
      var inputAmount=obj.value;
      var cz=FloatSub(inputAmount,sendAmount);
      if(cz>0){
          showMsg("alert","温馨提示：实收数量不能大于"+sendAmount+"！");
   	      document.getElementById(obj.id).value=sendAmount;
	      return false;
      }
      var oldValue=$("#oldValue").val();
      /*计算总物资数量*/
       var totalAmountStr=$("#totalAmountStr").text();
       var totalAmount=FloatSub(totalAmountStr,oldValue);
	   totalAmount=FloatAdd(totalAmount,inputAmount).toFixed(2);
	   document.getElementById("totalAmountStr").innerText =totalAmount;
   }
   function getOldValue(obj){
     document.getElementById("oldValue").value=obj.value;
   }
   function save(){
        var indexArr=document.getElementsByName("index");
	     var index,flag=true;
		 for(var i=0;i<indexArr.length;i++){
		  index=indexArr[i].value;
		   if($("#inputAmount_"+index).val()==""){
	           showMsg("alert","温馨提示：第"+(i+1)+"行数量不能为空");
	           flag=false;
	           return false;
			}
	     }
	     if(flag){
	        //提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			document.form.submit();
		}
   }
   function doView(oiId){
      window.location.href="viewInputBill_inputBill.action?ociId="+oiId+"&type=1";
   }
</script>
</head>
<body >
<form name="form" id="form" method="post" action="saveOrderInputSendBill_sendBill.action">
<input type="hidden" name="inputBill.attIds" id="attIds" />
<input type="hidden" name="inputBill.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="inputBill.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="inputBill.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="inputBill.attIdData" id="attIdData" value=""/>
<div class="Conter_Container" >
	
  <div class="Conter_main_conter">     
    	<table class="table_ys1"  width="100%">	
            <tr>
			    <td class="Content_tab_style1">订单编号：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderCode }
			    </td>
			    <td class="Content_tab_style1">订单名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderName }
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">供应商名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderNameB }
			    </td>			   
			    <td class="Content_tab_style1">供应商联系人：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderPersonB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">供应商电话：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.orderTelB }
			    </td>
			    <td class="Content_tab_style1">供应商传真：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderFaxB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">订单金额：</td>
			    <td class="Content_tab_style2">
			       <fmt:formatNumber value="${orderInfo.orderMoney }" pattern="#,##0.00"/>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.conMoneyType}
				</td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">
			    	收货日期：
			    </td>
			    <td class="Content_tab_style2">
			        <input type="text" id="inputDate" class="Wdate" name="inputBill.inputDate"  size="15" value="${today }" onClick="WdatePicker({ dateFmt:'yyyy-MM-dd', minDate:'%y-%M-%d %H:%m' })" />
			    </td>
			    <td class="Content_tab_style1">
			    	摘要：
			    </td>
			    <td class="Content_tab_style2">
			       <INPUT type="text" id="sendRemark" name="inputBill.inputRemark" maxlength="100" size="22" />
			       <INPUT type="hidden" id="supplierId" name="inputBill.supplierId" value="${orderInfo.supplierId }" />
			       <INPUT type="hidden" id="supplierName" name="inputBill.supplierName" value="${orderInfo.orderNameB }" />
			       <INPUT type="hidden" id="oiId" name="inputBill.oiId" value="${orderInfo.oiId }" />
			       <INPUT type="hidden" id="ids" name="ids" value="${ids }" />
			       <INPUT type="hidden" id="type" name="inputBill.type" value="1" />
			    </td>
	    		</tr>
	    		<tr>
					<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span>
					<br/><font color="#FF0000"> * 收货单复印件或收货凭证</font></td>
					<td class="Content_tab_style2" colspan="3">
						<!-- 附件存放 -->
						<div  id="fileDiv" class="panel"> 
						</div>
						<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
						<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
						
					</td>
				</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" id="listtable">
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="55px" nowrap>发货时间</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="8%" nowrap>计量单位</th>
				<th width="55px" nowrap>发货数量</th>
				<th width="100px" nowrap>实收数量</th>
				<th width="160px" nowrap>备注</th>
			</tr>
			    <c:set var="totalPrice" value="0"/>
					  <c:forEach items="${sbdList}" var="sendBillDetail" varStatus="status">
						    <c:set var="totalAmount" value="${totalAmount+sendBillDetail.sendAmount }"/> 
							<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
								<td>
								  ${status.index+1}
								  <input type="hidden" name="index" value="${status.index}"/>
								</td>
								<td>
								   <fmt:formatDate value="${sendBillDetail.sendDate}" pattern="yyyy-MM-dd"/>					    
								</td>
								<td>
								    ${sendBillDetail.materialCode}							    
								</td>
								<td>
								   ${sendBillDetail.materialName}
								</td>
								<td>
								   ${sendBillDetail.materialType}
								</td>
								<td>
								   ${sendBillDetail.unit}
							    </td>
								<td align="right">
							       ${sendBillDetail.sendAmount}
								 </td>
							    <td>
							       <input type="text" class="numPric" id="inputAmount${status.index}" name="sbdList[${status.index}].inputAmount" onfocus="getOldValue(this);" onblur="validateNum(this);account(this,${sendBillDetail.sendAmount});" value="${sendBillDetail.sendAmount}" style="width: 80px"/>
							    </td>
								<td>
								    <input type="hidden" name="sbdList[${status.index}].sbdId" value="${sendBillDetail.sbdId }"/>
								    <input type="hidden" name="sbdList[${status.index}].omId" value="${sendBillDetail.omId }"/>
								    <input type="text" name="sbdList[${status.index}].inputRemark" value="" style="width: 150px"/>
								</td>
							</tr>
						</c:forEach>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总物资数量：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalAmountStr">${totalAmount}<!-- 小计 --></span>
					</td>
					<td>&nbsp;<input type="hidden" id="oldValue" value="0"/></td>
				</tr>	
       	</table>
		<div class="buttonDiv">
				<button class="btn btn-success" onclick="save();" type="button"><i class="icon-white icon-ok-sign"></i>确认收货</button>
				<button class="btn btn-info" onclick="doView(${orderInfo.oiId });" type="button"><i class="icon-white icon-th-list"></i>查看历史收货信息</button>
				<button class="btn btn-danger" onclick="javascript:api.close();" type="button"><i class="icon-white icon-remove-sign"></i>关闭</button> 
		</div>
</div>
</div>
</form>
</body>
</html> 	