<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>选择合同信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var api = frameElement.api, W = api.opener;
var _table;
var sign=${sign};
var type=${type};
$(function (){
    
     $("#btn-danger").click(function(){
	    	api.close();
	 });
	
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findContractInfoForOrder_contractInfo.action?sign="+sign,
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			showMsg("error","查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.SELECTPAGECHECKBOX
            ,{
            	className : "ellipsis",
            	data: "projectName",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractCode",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractName",
            	render: viewContractInfoDetail,
            	width : "15%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractNameB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%"
             }
            ,{
            	className : "ellipsis",
            	data: "contractPersonB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "contractTelB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "numPrice",
            	data: "contractMoney",
            	render: CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE,
            	width : "8%"
             }
            ,{
            	className : "ellipsis",
            	data: "writeDate",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%"
             }
            ,{
            	className : "ellipsis",
            	data: "signDate",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%"
             }
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	 //给当前行某列加样式
        	$('td', row).eq(11).addClass("light-red");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	
	$("#btn-save").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });	
		GeneralManage.saveItemInit(arrItemId);
	});
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.ciId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
            case 2:
				param.orderColumn = "de.projectName";
				break;
            case 3:
				param.orderColumn = "de.contractCode";
				break;
            case 4:
				param.orderColumn = "de.contractName";
				break;
            case 5:
				param.orderColumn = "de.contractNameB";
				break;
            case 8:
				param.orderColumn = "de.contractMoney";
				break;
            case 9:
				param.orderColumn = "de.writeDate";
				break;
            case 10:
				param.orderColumn = "de.signDate";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		
		//组装查询参数  ***按需修改****
		param.contractCode = $("#contractCode").val();
		param.contractName = $("#contractName").val();
		param.contractNameB = $("#contractNameB").val();
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	saveItemInit:function(selectedItems){
		var v = selectedItems;
		  if(v.length==1){
	    	var ciId =v[0].ciId;
			//父页面一定要有id 为 returnVals 的 input 和 doReturn()函数
			if(sign==2){
			   W.window.location.href="saveOrderInfoInit_orderInfo.action?ciId="+ciId+"&type="+type;
			}else{
			   W.window.location.href="saveCapitalPlanInit_capitalPlan.action?ciId="+ciId+"&type="+type;
			}
			api.close();
		}else{
	    	showMsg("alert",'温馨提示：请选择合同信息！');
	    }
	}  				 
		
   };
   
    //查看
    function viewContractInfoDetail(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看合同信息明细\",\"viewContractInfoDetail_contractInfo.action?contractInfo.ciId=" + row.ciId + "\",1)' title='"+data+"'>"+data+"</a>"; 
              
    }
    function doQuery(){
     _table.draw();
    }

</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
									    <button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button> 
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
						    <button type="button" class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i> 确定</button>
							<button type="button" class="btn btn-danger" id=btn-danger><i class="icon-white icon-remove-sign"></i> 关闭</button>					
						
						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
					<input type="hidden" id="returnValues" name="returnValues"/>
					  <!--***按需修改****-->
					          合同编号：
						<input class="input-medium" placeholder="合同编号" type="text" id="contractCode" name="contractCode" value="" />
						合同名称：
						<input class="input-medium" placeholder="合同名称" type="text" id="contractName" name="contractName" value="" />
						乙方名称：
						<input class="input-medium" placeholder="乙方名称" type="text" id="contractNameB" name="contractNameB" value="" />
						<button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
						<button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
						            <th>项目名称</th>
						            <th>合同编号</th>
						            <th>合同名称</th>
						            <th>乙方名称</th>
						            <th>乙方联系人</th>
						            <th>乙方电话</th>
						            <th>合同金额</th>
						            <th>合同起草日期</th>
						            <th>合同签订日期</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>