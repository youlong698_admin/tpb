<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>订单业务终止页面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="OrderTermination"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
//返回信息
<c:if test="${orderTermination.otId==null}">
	var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
	var fileNameData=[];//已上传的文件名
	var fileTypeData=[];//已上传的文件的格式
	var attIdData=[];//已存入附件表的附件信息
</c:if>
<c:if test="${orderTermination.otId!=null}">
	var uuIdData=${orderTermination.uuIdData};//已上传的文件的文件uuid，上传后的文件以uuId命名
	var fileNameData=${orderTermination.fileNameData};//已上传的文件名
	var fileTypeData=${orderTermination.fileTypeData};//已上传的文件的格式
	var attIdData=${orderTermination.attIdData};//已存入附件表的附件信息
</c:if>
var api = frameElement.api, W = api.opener;
	function subButton()
	{
		//提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			
		var bmId=$("#bmId").val();
		
		$.dialog.confirm("温馨提示：你确定要订单终止么！",function(){
		     document.formAbnomal.action="saveOrderTermination_orderTermination.action";
		     document.formAbnomal.submit();
	   	},function(){},api);			   	 	
	}
</script>
</head>
<body >
<form class="defaultForm" id="formAbnomal" name="formAbnomal"  method="post" action="">
<input type="hidden" name="orderTermination.attIds" id="attIds" />
<input type="hidden" name="orderTermination.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="orderTermination.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="orderTermination.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="orderTermination.attIdData" id="attIdData" value=""/>

<input type="hidden" name="oiId" id="oiId" value="${orderInfo.oiId }"/>
<input type="hidden" name="orderTermination.otId" id="otId" value="${orderTermination.otId }"/>
<input type="hidden" name="orderTermination.status" id="status" value="${orderTermination.status }"/>
<input type="hidden" name="orderTermination.statusCn" id="statusCn" value="${orderTermination.statusCn }"/>

<s:token/>			

<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1" align="center">
			<tr>
				<td width="15%" class="Content_tab_style1">订单编号：</td>
				<td width="35%" class="Content_tab_style2">
					${orderInfo.orderCode }
					<input name="orderTermination.orderCode" type="hidden" id="orderCode" value="${orderInfo.orderCode }" />
				</td>
				<td width="15%" class="Content_tab_style1">订单名称：</td>
				<td width="35%" class="Content_tab_style2">
					${orderInfo.orderName }
					<input name="orderTermination.orderName" type="hidden" id="orderName" value="${orderInfo.orderName}" />
				</td>
			</tr>	
    		<tr>
				<td class="Content_tab_style1">终止原因：</td>
				<td class="Content_tab_style2" colspan="3">
					<textarea name="orderTermination.terminationReason" id="terminationReason" datatype="*" nullmsg="终止原因不能为空！"cols="85%" rows="4" style="resize:none;width:85%"
				   		>${orderTermination.terminationReason }</textarea>
				   		<div class="info"><span class="Validform_checktip">终止原因不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>

			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
			 
			<tr>
				<td class="Content_tab_style1">操作人：</td>
				<td class="Content_tab_style2">
					<input name="orderTermination.writerCn" type="text" id="writerCn" value="${orderTermination.writerCn }" readonly />
					<input name="orderTermination.writer" type="hidden" id="writer" value="${orderTermination.writer }" readonly />
					<input name="orderTermination.writerId" type="hidden" id="writerId" value="${orderTermination.writerId }" readonly />
				</td>
				<td class="Content_tab_style1">终止日期：</td>
				<td class="Content_tab_style2">
					<input name="orderTermination.writeDete" type="text" class="Wdate"  
                        id="writeDete" value="<fmt:formatDate  value="${orderTermination.writeDete}" type="both"/>" readonly/>
				</td>
			</tr>
			
        </table>
        <div class="buttonDiv">
				<button class="btn btn-success" id="btn-save" type="button"><i class="icon-white icon-ok-sign"></i>提交</button>
			    <button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
			</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			subButton();
			return false;	
		}
	});
})
</script>
</body>
</html>