<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>订单信息管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
var orderWorkflow="${orderWorkflow}";
var className;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findOrderInfo_orderInfo.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			showMsg("error","查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX, 
            {	
            	className : "ellipsis",
				orderable : false,
            	render: function(data,type, row, meta) {
					
					if(row.orderStateName=="流程审批中")
					{
					    var action="display_workflow.action?param=view&processId="+row.processId+"&orderId=";
						return "<img src=\"<%=path%>/images/msg_1.jpg\" title=\"流程催办\" onclick=\"openWorkFlowWindow('"+action+"',"+row.oiId+",'"+row.processId+"')\" style=\"cursor:pointer\">";
					}else
					{
						return "";
					}
                },         	
            	width : "30px"
            }
            ,{
            	className : "ellipsis",
            	data: "orderCode",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "orderName",
            	render: viewOrderInfoDetail,
            	width : "20%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "orderNameB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "20%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "orderPersonB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "orderUndertaker",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "numPrice",
            	data: "orderMoney",
            	render: CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "writeDate",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "statusCn",
            	width : "8%",
				orderable : false
             }
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	 //给当前行某列加样式
        	$('td', row).eq(9).addClass("light-red");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.editItemInit(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	});
	
	//撤销确认流程
	$("#btn-processCancel").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.pCancelItem(arrItemId);
	});
	//提交确认流程
	$("#btn-processSubmit").click(function(){
  		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.pSubmitItem(arrItemId);
	});
	$("#btn-send").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.sendItemInit(arrItemId);
	});
	
	//提交订单
	$("#btn-Submit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.btnSubmitItem(arrItemId);
	});
	//订单终止
	$("#btn-term").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.termItem(arrItemId);
	});
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		param.orderColumn="de.oiId";
		
		//组装查询参数  ***按需修改****
		param.orderCode = $("#orderCode").val();
		param.orderName = $("#orderName").val();
		param.orderNameB = $("#orderNameB").val();
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	// 新增
	addItemInit: function()  {
		
	   window.location.href="saveOrderInfoInit_orderInfo.action?type=0";	    
	},
	//修改
	editItemInit: function(selectedItems)  {
	   var v = selectedItems;
	   if(v.length==0){
           showMsg("alert","请选择信息");
       }else if(v.length > 1){
   	      showMsg("alert","温馨提示：只能选择一条信息修改！");
       }else{
	  		var ids = v[0].oiId;
	  		var runStatus=v[0].runStatus;
	  		if(runStatus=="01"){
		      window.location.href="updateOrderInfoInit_orderInfo.action?orderInfo.oiId="+ids;	    
		    }else{
		     showMsg("alert","温馨提示：该订单不是起草状态，不能修改！");
		    }
		    
      }
	 },
	//订单发送至供应商
	sendItemInit: function(selectedItems)  {
	   var v = selectedItems;
		if(v!=0){
	          	var ids = "";
	          	var falg=true;
		        for(var i=0;i<v.length;i++){
		     		if(i == (v.length-1)){
						ids += v[i].oiId;
				    }else{
	 			     	ids += v[i].oiId + ",";
			        }
			        if(v[i].runStatus!="0"){
			          falg=false;
			        }
				}
				if(falg){		        
					$.dialog.confirm("温馨提示：你确定要发送选中的订单信息至供应商么！",function(){
						   result = ajaxGeneral("saveSendOrderInfo_orderInfo.action","ids="+ids);
							   showMsg('success',''+result+'',function(){
							   		doQuery();							
				   					});
						   	 	}); 
				   }else{
				     showMsg("alert","温馨提示：只有提交的订单才能发送至供应商进行确认操作");
				   }
	    }else{
        	showMsg("alert","请选择信息！");
        }
	 }, 
	//删除
	deleteItem : function(selectedItems) {
		var v = selectedItems;
		if(v!=0){
	          	var ids = "";
	          	var falg=true;
		        for(var i=0;i<v.length;i++){
		     		if(i == (v.length-1)){
						ids += v[i].oiId;
				    }else{
	 			     	ids += v[i].oiId + ",";
			        }
			        if(v[i].runStatus!="01"&&v[i].runStatus!="3"){
			          falg=false;
			        }
				}
				if(falg){		        
					$.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
						   result = ajaxGeneral("deleteOrderInfo_orderInfo.action","ids="+ids);
							   showMsg('success',''+result+'',function(){
							   		doQuery();							
				   					});
						   	 	}); 
				}else{
				     showMsg("alert","温馨提示：非起草状态的订单不能删除！");
				}
	    }else{
        	showMsg("alert","请选择信息！");
        } 
    },
    //撤销确认流程
	pCancelItem:function(selectedItems){
		var v = selectedItems;
		if(v.length < 1){
      		showMsg("alert","温馨提示：请选择将要撤销的信息！");
      	}else if(v.length > 1){
	    	showMsg("alert","温馨提示：只能选择一条信息撤销！");
	    }else{
	    	var orderState = v[0].orderState;
	    	if(orderState == "0"){
    	    		showMsg("alert","温馨提示:此项目流程已结束,不能撤销!");
    	    		return false;
   	    	}else{
   	    		var action = "deleteCancelOrderInfoProcess_bidworkflow.action";
				var param = "orderInfo.oiId="+v[0].oiId;
				var result = ajaxGeneral(action,param,"text");
				if(parseInt(result)==0){
					showMsg("alert","温馨提示：流程撤销成功！",function(){
					 window.location.href= window.location.href;
					});
				}else if(parseInt(result)==1){
					showMsg("alert","温馨提示：流程编号不存在！");
					return ;
				}else if(parseInt(result)==2){
					showMsg("alert","温馨提示：没有提交流程，不能撤销！");
					return ;
				}else{
				    showMsg("alert","温馨提示：流程已经处理，不能撤回");
					return ;				
				}
    		}
	    }
	},
	//提交确认流程
	pSubmitItem:function(selectedItems){
  		var v = selectedItems;
  		if(v.length < 1){
	  		showMsg("alert","温馨提示：请选择将要提交的信息！");
	  	}else if(v.length > 1){
	  		showMsg("alert","温馨提示：请选择一条将要提交的信息的信息！");
	    }else{
	    	var oiId = v[0].oiId;
	    	var processId = v[0].processId;
	    	var orderId = v[0].orderId;
	    	var orderState = v[0].orderState;
	    	var instanceUrl = v[0].instanceUrl;
	    	var orderAppFile = v[0].orderAppFile;
	    	if(orderAppFile==""){
	    	  showMsg("alert","温馨提示:请上传订单审批附件信息再提交审批!");
	    	}else{
	    	if(orderState == "0"){
	    		showMsg("alert","温馨提示:此项目流程已结束,请选择将要提交的信息!");
	    	}else if(orderState == "1"){
	    		showMsg("alert","温馨提示:此项目流程正在运行,请选择将要提交的信息!");
	    	}else{
	    		 var orderNo="${work_flow_type}"+oiId;
					createdetailwindow("提交订单审批","<%=path%>"+instanceUrl+"?processId="+processId+"&orderId="+orderId+"&orderNo="+orderNo,1);
			}
			}
	    }
	},
	//提交为订单
	btnSubmitItem:function(selectedItems){
	  var v = selectedItems;
		if(v.length < 1){
      		showMsg("alert","温馨提示：请选择将要提交的订单信息！");
      	}else{
      	        var f=0;
    	    	var ids = "";
      	   		var orderIds = "";
      	   		var noFile=0;
    			for(var i=0;i<v.length;i++){
    			var orderState = v[i].orderState;
				    if(orderState != "01") f=1;	 	   
    				if(i == (v.length-1)){
    					ids += v[i].oiId;
    					orderIds = v[i].orderId;
    				}else{
    					ids += v[i].oiId + ",";
    					orderIds += v[i].orderId + ",";
    				}
    			}
    			if(f==1){
	    	    	showMsg("alert","温馨提示:您选择的订单中存在已提交的订单,不能提交!");
	    	    	return false;
		    	 }
    			$.dialog.confirm("温馨提示：你确定要提交选中的信息！",function(){
 				  var result = ajaxGeneral("updateProcessOrderInfo_orderInfo.action","ids="+ids);
 					   showMsg('success',''+result+'',function(){
 					   		doQuery();							
 		   					});
 				   	 	});
	    }
	},
	//订单终止
	termItem : function(selectedItems) {
		var v = selectedItems;
		 if(v.length>0){
			  if(v.length>1)
			  {
			  	showMsg("alert",'温馨提示：只能选择一条信息！');
			  }else
			  {
				  if(v[0].runStatus=="8")
				  {
				  	showMsg("alert",'温馨提示：该订单已经终止！');
				  }else
				  {
				  	createdetailwindow('订单终止','saveOrderTerminationInit_orderTermination.action?orderInfo.oiId='+v[0].oiId,2);
				  }
		    	 
	
			  }
	    }else{
	    	showMsg("alert",'温馨提示：请选择要终止的信息！');
	    }
	}   				 
		
   };
   
    //查看
    function viewOrderInfoDetail(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看订单信息明细\",\"viewOrderInfoDetail_orderInfo.action?orderInfo.oiId=" + row.oiId + "\",1)' title='"+data+"'>"+data+"</a>"; 
              
    }
    //查看流程审批信息
	
      function viewWorkflowProcess(data, type, row,   meta ){
        if(orderWorkflow=="0"){
    	  if(row.orderState != ""){
	    		  var action="display_workflow.action?param=view&processId="+row.processId+"&orderId=";
	    		  return "<a href='#' onclick='openWorkFlowWindow(\""+action+"\","+row.oiId+",\""+row.processId+"\")'>"+data+"</a>";
    	  }else{
    		  return "起草状态";
    	  }
    	  }else{
    	    if(row.orderState=="01")  return "起草状态";
    	    else  if(row.orderState=="0")  return "已完成";
    	  }
    	  
      }
      
	//弹出一个新窗口
	function openWorkFlowWindow(action, oiId, processId){
	    oiId="${work_flow_type}"+oiId;
		$.ajax({
			url:"<%=path%>/findWfOrder_workflow.action",
			type:"POST",
			dataType: 'json',
			async:false, 
			data:{orderNo:oiId, processId:processId},
			success:function(msg){
				var rsb = msg;
				var rs = rsb.order;
				if(rs.length > 0){
					createdetailwindow("查看流程",action+rs[0].id,0);
				}else{
					return false;
				}
			},
			error:function(){
			}
		});
		
	}
    function doQuery(){
     _table.draw();
    }

</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
									    <button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button> 
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i> 新建订单</button>
							<button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
							<c:if test="${orderWorkflow=='0'}">
							<button type="button" class="btn btn-info" id="btn-processSubmit"><i class="icon-white icon-resize-small"></i> 提交流程</button>  <!--fa fa-chain-broken icon-resize-full -->
							<button type="button" class="btn btn-warning" id="btn-processCancel"><i class="icon-white icon-resize-full"></i> 撤销流程</button>
							</c:if>
							<c:if test="${orderWorkflow=='1'}">
							<button type="button" class="btn btn-info" id="btn-Submit"><i class="icon-white icon-resize-small"></i> 提交订单信息</button>  <!--fa fa-chain-broken icon-resize-full -->
							</c:if>
							<button type="button" class="btn  btn-warning" id="btn-send"><i class="icon-white icon-file"></i> 发送至供应商</button>
							<button type="button" class="btn  btn-warning" id="btn-term"><i class="icon-white icon-remove-sign"></i> 订单终止</button>
						   </div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
					<input type="hidden" id="returnValues" name="returnValues"/>
					  <!--***按需修改****-->
					          订单编号：
						<input class="input-medium" placeholder="订单编号" type="text" id="orderCode" name="orderCode" value="" />
						订单名称：
						<input class="input-medium" placeholder="订单名称" type="text" id="orderName" name="orderName" value="" />
						供应商名称：
						<input class="input-medium" placeholder="供应商名称" type="text" id="orderNameB" name="orderNameB" value="" />
						<button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
						<button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>操作</th>
						            <th>订单编号</th>
						            <th>订单名称</th>
						            <th>供应商名称</th>
						            <th>供应商联系人</th>
						            <th>收货联系人</th>
						            <th>订单金额</th>
						            <th>起草日期</th>
						            <th>订单状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>