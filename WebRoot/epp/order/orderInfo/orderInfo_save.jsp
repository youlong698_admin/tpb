<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>新增订单信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script type="text/javascript">
    var unitStr="";
    <c:forEach var="dictionary" items="${dictionaryList}">
        unitStr+="<option value=\"${dictionary.dictName }\">${dictionary.dictName }</option>";
    </c:forEach>
    
   function doView(){
      window.location.href="viewOrderInfo_orderInfo.action";
   }
   var num = 1,j=1;
   function doAdd(){	
	var tr=document.getElementById("listtable").insertRow(j);
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
	var cell3 = tr.insertCell();
	var cell4 = tr.insertCell();
	var cell5 = tr.insertCell();
	var cell6 = tr.insertCell();
	var cell7 = tr.insertCell();
	var cell8 = tr.insertCell();
	var cell9 = tr.insertCell();
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/><input type='hidden' name='omList["+num+"].tableKey' value='0'/>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this,"+num+")'><i class=\"icon-white icon-trash\"></i></button>";
    cell1.style.textAlign="center";
	cell2.innerHTML="<input type='text' name='omList["+num+"].materialCode' id='materialCode_"+num+"' value=''  style='width: 80%'>" ;
	cell2.style.textAlign="center";
	cell3.innerHTML="<input type='text' id='materialName_"+num+"' value='' name='omList["+num+"].materialName' style='width: 80%'/>" ;
	cell3.style.textAlign="center";
	cell4.innerHTML="<input type='text' id='materialType_"+num+"' value='' name='omList["+num+"].materialType' style='width: 80%'/>";
	cell4.style.textAlign="center";
    cell5.innerHTML="<select id='unit_"+num+"' name='omList["+num+"].unit' style='width: 80%'>"+unitStr+"</select>";
    cell5.style.textAlign="center";
    cell6.innerHTML="<input type='text' id='amount_"+num+"' name='omList["+num+"].amount' value='' class='numPrice' onblur='validateNum(this);accountPrice("+num+");' style='width: 80%'>";
    cell6.style.textAlign="center";
	cell7.innerHTML="<input type='text' id='price_"+num+"' value='' name='omList["+num+"].price' class='numPrice' onblur='validateNum(this);accountPrice("+num+");' style='width: 80%'/>" ;
	cell7.style.textAlign="center";
	cell8.innerHTML="<span id='priceSumStr_"+num+"'>0</span>" ;
	cell8.style.textAlign="right";
	cell9.innerHTML="<input name='omList["+num+"].remark' type='text' style='width: 80%'/>" ;
	cell9.style.textAlign="center";
	num++;
	j++;
}
	//删除一行
	function deleteRow(obj,num){
	    j--;
	    var priceSumStr=$("#priceSumStr_"+num).text();
	    if(priceSumStr!=""){
	        var totalPriceStr=$("#totalPriceStr").text();
	        totalPrice=FloatSub(totalPriceStr,priceSumStr);
		    document.getElementById("totalPriceStr").innerText =parseFloat(totalPrice).toFixed(2);
	    }
		$(obj).parent().parent().remove(); 
	    
	}
  //计算总价
	function accountPrice(num) 
	{
	    var priceSumStr=$("#priceSumStr_"+num).text();
		var amount,price,totalPrice=0;
        price=$("#price_"+num).val();
        if(price==""){
           return false;
        }
        amount=$("#amount_"+num).val();
        if(amount==""){
           return false;
        }
        //showMsg("alert","amount="+amount);
        //计算小计
        var  prices=eval(price*amount).toFixed(2)+"";
        document.getElementById("priceSumStr_"+num).innerText=prices;
            
       /*计算总合计*/
        var totalPriceStr=$("#totalPriceStr").text();
        totalPrice=FloatSub(totalPriceStr,priceSumStr);
		totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
	    document.getElementById("totalPriceStr").innerText =totalPrice;
	}
	//保存
	 function save(){
	     var indexArr=document.getElementsByName("rowIndex");
	     var index,flag=true;
	     if(indexArr.length==0){
	         showMsg("alert","温馨提示：请添加订单物资明细！");
	         return false;
	     }
		 for(var i=0;i<indexArr.length;i++){
		  index=indexArr[i].value;
		   if($("#materialCode_"+index).val()==""){
	           showMsg("alert","温馨提示：第"+(i+1)+"行编码不能为空");
	           flag=false;
	           return false;
			}
		   if($("#materialName_"+index).val()==""){
	           showMsg("alert","温馨提示：第"+(i+1)+"行名称不能为空");
	           flag=false;
	           return false;
			}
			if($("#amount_"+index).val()==""){
	           showMsg("alert","温馨提示：第"+(i+1)+"行数量不能为空");
	           flag=false;
	           return false;
			}
			if($("#price_"+index).val()==""){
	           showMsg("alert","温馨提示：第"+(i+1)+"行单价不能为空");
	           flag=false;
	           return false;
			}
		 }
		 var orderMoney=$("#orderMoney").val();
		 var totalPriceStr=parseFloat($("#totalPriceStr").text());
		 if(FloatSub(totalPriceStr,orderMoney)!=0){
		   showMsg("alert","温馨提示：订单金额和订单物资的总金额不相等");
		   return;
		 } 
		 if(flag){
		    document.defaultForm.submit();
		 }
	 }	 
	 function doClick(type){
	    if(type==1){
	      $("#selectProjectImg").show();
	      $("#selectSupplierImg").hide();
	      $("#projectContractName").attr("readonly","readonly");
	      $("#bidCode").attr("readonly","readonly");
	      $("#td1").text("项目名称：");
	      $("#td2").text("项目名称");
	      $("#td3").text("项目编号：");
	    }else if(type==2){	  
	      $("#selectProjectImg").show();
	      $("#selectSupplierImg").hide();
	      $("#projectContractName").attr("readonly","readonly");
	      $("#bidCode").attr("readonly","readonly");
	      $("#td1").text("合同名称：");
	      $("#td2").text("合同名称");
	      $("#td3").text("合同编号：");
	    }else if(type==3){    
	      $("#selectProjectImg").hide();
	      $("#selectSupplierImg").show();
	      $("#projectContractName").removeAttr("readonly");
	      $("#bidCode").removeAttr("readonly");
	      $("#td1").text("项目名称：");
	      $("#td2").text("项目名称");
	      $("#td3").text("项目编号：");
	    }
	 }
	 function selectProject(){
	   var type=$("input[name='orderInfo.type']:checked").val() 
	   if(type==1){
	    createdetailwindow('选择项目','viewRequiredCollectedForContractAndOrder_purchaseBase.action?sign=2',3);
	   }else{
	    createdetailwindow('选择合同','viewContractForOrder_contractInfo.action?sign=2',3);
	   }
	 }
	 
	function valueEvaluateUser(){
		var users=$("#returnValues").val();
		if(users!=""){
			var ur = users.split(",");
			for(var i=0;i<ur.length;i++){
				var u = ur[i].split(":");				
				document.getElementById("orderUndertakerId").value=u[0];
				document.getElementById("orderUndertaker").value=u[1];	
				document.getElementById("orderMobileA").value=u[3];				
			}			
		}
	}
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="saveOrderInfo_orderInfo.action">
<input  name="orderInfo.comId" type="hidden" id="comId" value="${comId }">
<input  name="orderInfo.isUsable" type="hidden" id="isUsable" value="0">
<input  name="orderInfo.deptId" type="hidden" id="deptId" value="${deptId }">
<input type="hidden" id="returnValues" name="returnValues" value='' />
<c:set var="td1" value=""/>
<c:set var="td2" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter">
   <div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
                <li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');"><b>订单基本信息</b></a></li>
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b>订单物资信息</b></a></li>
            </ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">      
    	<table class="table_ys1"  width="100%">	
    	    <tr>
    	        <td colspan="4" style="color: red" align="left">*订单除来自项目或者合同(为框架协议)生成外，还可新建新的订单</td>
    	    </tr> 
    	    <tr>
    	         <td width="15%" class="Content_tab_style1">订单来源：</td>
    	          <td width="35%" class="Content_tab_style2" colspan="3">
    	             <label class="radio_label">
				        <input class="radio_radio" type="radio" name="orderInfo.type" id="type" <c:if test="${type==1||type==0 }">checked <c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:if> value="1" onclick="doClick(1)">
				        <span class="radio_radioInput"></span>从采购项目获取
				    </label>
				     <label class="radio_label">
				        <input class="radio_radio" type="radio" name="orderInfo.type" id="type" <c:if test="${type==2 }">checked <c:set var="td1" value="合同名称"/> <c:set var="td2" value="合同编号"/></c:if> value="2" onclick="doClick(2)">
				        <span class="radio_radioInput"></span>从合同(框架协议)获取
				    </label>
				     <label class="radio_label">
				        <input class="radio_radio" type="radio" name="orderInfo.type" id="type" <c:if test="${type==3 }">checked <c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:if> value="3" onclick="doClick(3)">
				        <span class="radio_radioInput"></span>新建订单
				    </label>
    	          </td>
    	    </tr> 	        	
            <tr>
			    <td width="15%" class="Content_tab_style1" id="td1">${td1 }：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input  datatype="*" nullmsg="${td1 }不能为空！" name="orderInfo.projectContractName" readonly type="text" id="projectContractName" value="${orderInfo.projectContractName }">
			       <img src="images/select.gif" id="selectProjectImg" onclick="selectProject()"/><font color="#ff0000">*</font>			      
			       <div class="info"><span class="Validform_checktip" id="td2">${td1 }不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			       <input  name="orderInfo.baCiId" type="hidden" id="baCiId" value="${orderInfo.baCiId }"></td>
			    <td width="15%" class="Content_tab_style1" id="td3">${td2 }：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input   type="text" name="orderInfo.bidCode" id="bidCode" value="${orderInfo.bidCode }" readonly><font color="#ff0000">*</font>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">订单编号：</td>
			    <td class="Content_tab_style2">
			       <input name="orderInfo.orderCode" type="text" id="orderCode" value="" readonly placeholder="系统自动生成" style="background-color: #ccc">
			    </td>
			    <td class="Content_tab_style1">订单名称：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="订单名称不能为空！" name="orderInfo.orderName" type="text" id="orderSignAddress" value=""><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">订单名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">采购单位：</td>
			    <td class="Content_tab_style2">
			    <input  datatype="*" nullmsg="采购单位不能为空！" name="orderInfo.orderPersonNameA" type="text" id="orderPersonNameA" value="${orderInfo.orderPersonNameA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">采购单位不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">供应商名称：</td>
			    <td class="Content_tab_style2">
				    <input  datatype="*" nullmsg="供应商名称不能为空！" name="orderInfo.orderNameB" readonly type="text" id="supplierName" value="${orderInfo.orderNameB }">
				    <input  name="orderInfo.supplierId" type="hidden" id="supplierId" value="${orderInfo.supplierId }">
				    <c:if test="${empty orderInfo.baCiId}"><img id="selectSupplierImg" style="display:none" src="images/select.gif" onclick="createdetailwindow('选择供应商','viewSupplierBaseInfoCompany_oneSupplierSelect.action',3);" title="选择供应商"/></c:if><font color="#ff0000">*</font>			      
			        <div class="info"><span class="Validform_checktip">供应商名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">收货地址：</td>
			    <td class="Content_tab_style2">
			        <input  datatype="*" nullmsg="收货地址不能为空！" name="orderInfo.orderAddressA" type="text" id="orderAddressA" value="${orderInfo.orderAddressA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">收货地址不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">供应商地址：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="供应商地址不能为空！" name="orderInfo.orderAddressB" type="text" id="supplierAddress" value="${orderInfo.orderAddressB }"><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">供应商地址不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">收货联系人：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="收货联系人不能为空！" name="orderInfo.orderUndertaker" type="text" id="orderUndertaker" value="">
			       <input  name="orderInfo.orderUndertakerId" type="hidden" id="orderUndertakerId" value="">
			       <img id="selectSupplierImg"  src="images/select.gif" onclick="createdetailwindow('选择收货联系人','viewDeptIndex_userTree.action',1);" title="选择收货联系人"/><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">收货联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">供应商联系人：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="供应商联系人不能为空！" name="orderInfo.orderPersonB" type="text" id="contactPerson" value="${orderInfo.orderPersonB }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">供应商联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">收货联系人手机：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="m" nullmsg="收货联系人手机不能为空！" name="orderInfo.orderMobileA" type="text" id="orderMobileA" value=""><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">收货联系人手机不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">供应商联系人手机：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="m" nullmsg="供应商联系人手机不能为空！" name="orderInfo.orderMobileB" type="text" id="orderMobileB" value="${orderInfo.orderMobileB }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">供应商联系人手机不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位电话：</td>
			    <td class="Content_tab_style2">
			    <input  name="orderInfo.orderTelA" type="text" id="orderTelA" value="${orderInfo.orderTelA }">
			    </td>
			    <td class="Content_tab_style1">供应商电话：</td>
			    <td class="Content_tab_style2">
			    <input   name="orderInfo.orderTelB" type="text" id="supplierPhone" value="${orderInfo.orderTelB }">
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位传真：</td>
			    <td class="Content_tab_style2">
			    <input   name="orderInfo.orderFaxA" type="text" id="orderFaxA" value="${orderInfo.orderFaxA }">
			    </td>
			    <td class="Content_tab_style1">供应商传真：</td>
			    <td class="Content_tab_style2">
			    <input name="orderInfo.orderFaxB" type="text" id="supplierFax" value="${orderInfo.orderFaxB }">
			    </td>
			    </tr>
            <tr>
			    <td class="Content_tab_style1">订单金额：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="n |/^(0|[1-9][0-9]*)+(.[0-9]{1,2})?$/" nullmsg="订单金额不能为空！" errormsg="订单金额必须是数字" name="orderInfo.orderMoney" type="text" id="orderMoney" value="<fmt:formatNumber value="${orderInfo.orderMoney }" pattern="#00.00#"/>"><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">订单金额不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        <select name="orderInfo.conMoneyType" id="conMoneyType">
				      <c:forEach items="${moneyTypeList}" var="orderClass">
				         <option value="${orderClass.content }">${orderClass.content }</option>
				      </c:forEach>
				    </select>
				</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3"><textarea name="orderInfo.conRemark" style="width: 80%"></textarea></td>
			</tr>
		</table>
		 </div>
             	
      		<!--------------------订单物资明细--------------------------->
      		<div id="dTab2" class="HackBox">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" id="listtable">
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap><c:if test="${empty orderInfo.baCiId}"><img src="images/add.gif" onclick="doAdd();" title="添加订单物资"/></c:if><c:if test="${not empty orderInfo.baCiId}">序号</c:if></th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>单价</th>
				<th width="100px" nowrap>小计</th>
				<th width="100px" nowrap>备注</th>
			</tr>
			    <c:set var="totalPrice" value="0"/>
			    <c:choose>
		           <c:when test="${type==1}">
					<c:forEach items="${badList}" var="bidAwardDetail" varStatus="status">
					    <c:set var="totalPrice" value="${totalPrice+bidAwardDetail.price*bidAwardDetail.awardAmount }"/> 
						<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
							<td>
							   <button class='btn btn-mini btn-danger' type="button"  onclick="deleteRow(this,${status.index})"><i class="icon-white icon-trash"></i></button>
							   <input type='hidden' name='rowIndex' value='${status.index}'/>
							   <input type='hidden' value='${bidAwardDetail.badId}' name='omList[${status.index}].tablekey' /> 
							</td>
							<td>
							    ${bidAwardDetail.requiredCollectDetail.buyCode}
							    <input type='hidden' value='${bidAwardDetail.requiredCollectDetail.buyCode}' name='omList[${status.index}].materialCode' /> 
							</td>
							<td>
							   ${bidAwardDetail.requiredCollectDetail.buyName}
							    <input type='hidden' value='${bidAwardDetail.requiredCollectDetail.buyName}' name='omList[${status.index}].materialName' /> 
							</td>
							<td>
							   ${bidAwardDetail.requiredCollectDetail.materialType}
							    <input type='hidden' value='${bidAwardDetail.requiredCollectDetail.materialType}' name='omList[${status.index}].materialType' /> 
							</td>
							<td>
							   ${bidAwardDetail.requiredCollectDetail.unit}
							    <input type='hidden' value='${bidAwardDetail.requiredCollectDetail.unit}' name='omList[${status.index}].unit' /> 
						    </td>
							<td align="right">
						     ${bidAwardDetail.awardAmount}
							    <input type='hidden' value='${bidAwardDetail.awardAmount}' name='omList[${status.index}].amount' /> 
							 </td>
						    <td align="right">
						       <fmt:formatNumber value="${bidAwardDetail.price}" pattern="#,##0.00"/>
							    <input type='hidden' value='${bidAwardDetail.price}' name='omList[${status.index}].price' /> 
						    </td>
						    <td align="right">
						       <span id='priceSumStr_${status.index}'><fmt:formatNumber value="${bidAwardDetail.price*bidAwardDetail.awardAmount}" pattern="#00.00#"/></span>
						    </td>
							<td>
							    <input type='text' value='' name='omList[${status.index}].remark' />
							</td>
						</tr>
					</c:forEach>
					</c:when>
					<c:when test="${type==2}">
					  <c:forEach items="${cmList}" var="contractMaterial" varStatus="status">
						    <c:set var="totalPrice" value="${totalPrice+contractMaterial.amount*contractMaterial.price }"/> 
							<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
								<td>
							       <button class='btn btn-mini btn-danger' type="button" onclick="deleteRow(this,${status.index})"><i class="icon-white icon-trash"></i></button>
							       <input type='hidden' name='rowIndex' value='${status.index}'/>
								   <input type='hidden' value='${contractMaterial.cmId}' name='omList[${status.index}].tablekey' /> 
								</td>
								<td>
								    ${contractMaterial.materialCode}
								    <input type='hidden' value='${contractMaterial.materialCode}' name='omList[${status.index}].materialCode' /> 
								</td>
								<td>
								   ${contractMaterial.materialName}
								    <input type='hidden' value='${contractMaterial.materialName}' name='omList[${status.index}].materialName' /> 
								</td>
								<td>
								   ${contractMaterial.materialType}
								    <input type='hidden' value='${contractMaterial.materialType}' name='omList[${status.index}].materialType' /> 
								</td>
								<td>
								   ${contractMaterial.unit}
								    <input type='hidden' value='${contractMaterial.unit}' name='omList[${status.index}].unit' /> 
							    </td>
								<td align="right">
							     ${contractMaterial.amount}
								    <input type='hidden' value='${contractMaterial.amount}' name='omList[${status.index}].amount' /> 
								 </td>
							    <td align="right">
							       <fmt:formatNumber value="${contractMaterial.price}" pattern="#,##0.00"/>
								    <input type='hidden' value='${contractMaterial.price}' name='omList[${status.index}].price' /> 
							    </td>
							    <td align="right">
							       <span id='priceSumStr_${status.index}'><fmt:formatNumber value="${contractMaterial.price*contractMaterial.amount}" pattern="#00.00#"/></span>
							    </td>
								<td>
								    <input type='text' value='${contractMaterial.remark}' name='omList[${status.index}].remark' />
								</td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总价：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalPriceStr"><fmt:formatNumber value="${totalPrice}" pattern="#00.00#"/><!-- 小计 --></span>
					</td>
					<td>&nbsp;</td>
				</tr>	
       	</table>
	</div>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-info" id="btn-view" type="button" onclick="doView()"><i class="icon-white icon-th-list"></i>查看订单列表</button>		
		</div>
</div>
</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			save();
			return false;	
		}
	});
})
</script>
</body>
</html> 	