<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	
</head>
 
<body>
<form id="" name="" method="post" action="saveSupplierBaseInfo_supplierBaseInfo.action">

<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
        	<tr>
				<td width="20%" class="Content_tab_style1">公司名称：</td>
				<td width="30%" >
					${supplierInfo.supplierName }
				</td>
				<td rowspan="4" colspan="2">
			       <div style="width:220px; height:80px; overflow:hidden;">  
					<div id="imgDiv">
					   <c:choose>
					      <c:when test="${empty supplierInfo.logo}">
                            <img src="<%=path %>/images/no.png" width="220" height="80">
					      </c:when>
					      <c:otherwise>					      
                            <img src="/fileWeb/${supplierInfo.logo}" width="220" height="80">
					      </c:otherwise>
					   </c:choose> 
					   </div>
					</div>
			    </td>
			 </tr>
			 <tr>	
				<td  class="Content_tab_style1">
				<c:choose>
					<c:when test="${supplierInfo.registrationType=='01'}">
					    统一社会信用代码证号：  
					</c:when>
					<c:otherwise>
					     组织机构代码证编号：
					</c:otherwise>
				</c:choose>
				</td>
				<td>
					${supplierInfo.orgCode}
				</td>
			  </tr>
			 <tr>					
				<td  class="Content_tab_style1">用户名：</td>
				<td>
					${supplierInfo.supplierLoginName}
					
				</td>
			  </tr>
			 <tr>
			    <td  class="Content_tab_style1">公司简称：</td>
				<td  >
					${supplierInfo.supplierNameSimple}
				</td>
			 </tr>
			 <tr>
				<td class="Content_tab_style1">成立日期：</td>
				<td>
					<fmt:formatDate value="${supplierInfo.createDate}" pattern="yyyy-MM-dd"/>
				</td>
				<td  class="Content_tab_style1">企业性质：</td>
				<td  >
					${supplierInfo.supplierTypeCn}
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">地区：</td>
				<td  >
					${supplierInfo.province} ${supplierInfo.city }
				</td>	
				<td  class="Content_tab_style1">所属行业：</td>
				<td  >
					${supplierInfo.industryOwnedCn}
					
				</td>
            </tr><tr>
				<td  class="Content_tab_style1">经营模式：</td>
				<td  >
					${supplierInfo.managementModelCn}
				</td>	
				<td  class="Content_tab_style1">公司网站：</td>
				<td  >
					${supplierInfo.companyWebsite}
					
				</td>
            </tr>
			<tr>
				<td  class="Content_tab_style1">企业法人：</td>
				<td  >
					${supplierInfo.legalPerson}
				</td>	
				<td  class="Content_tab_style1">注册资金：</td>
				<td  >
					${supplierInfo.registerFunds}
					
				</td>
            </tr>
			<tr>
				<td  class="Content_tab_style1">地址：</td>
				<td  >
					${supplierInfo.supplierAddress}
					&nbsp;
				</td>
				<td  class="Content_tab_style1">公司电话：</td>
				<td  >
					${supplierInfo.supplierPhone}
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">公司传真：</td>
				<td  >
					${supplierInfo.supplierFax}
				</td>
				<td  class="Content_tab_style1">联系人：</td>
				<td  >
					${supplierInfo.contactPerson}
				</td>
			</tr>
			
			<tr>
			    <td  class="Content_tab_style1">联系人手机号：</td>
				<td  >
					${supplierInfo.mobilePhone}
				</td>
				<td  class="Content_tab_style1">联系人E-mail：</td>
				<td  >
					${supplierInfo.contactEmail}
				</td>
				
			</tr>
			<tr>
				<td  class="Content_tab_style1">户名：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bankAccountName }
				</td>
				<td  class="Content_tab_style1">银行账号：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bankAccount }
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">开户银行：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bank }
				</td>
				<td  class="Content_tab_style1">税号：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bankAccount }
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">主营产品：</td>
				<td width="85%" colspan="3">
					${supplierInfo.prodKindNames}
				</td>
				
			</tr>
			<tr>
				<td  class="Content_tab_style1">经营范围：</td>
				<td colspan="3">
					${supplierInfo.scopeBusiness}
				</td>
				
			</tr>
			<tr>
				<td  class="Content_tab_style1">企业简介：</td>
				<td  colspan="3">
				   ${supplierInfo.introduce}
				</td>
			</tr>
        </table>
        
	</div>
</div>
</form>
</body></html>