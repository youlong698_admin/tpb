<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script> 
    
    <title>查看考核人考核的供应商个数</title>
    <script>
	var api = frameElement.api, W = api.opener;
	
	</script>
	</head>
	<body>
	<form method="post" id="" action="">
		<table class="table_ys1">
			<tr align="center" class="Content_tab_style_05">
		  		<td width="100%" colspan="4"><b>考核编号：</b><s:property value="supplierEvaluations.seCode"/>
				  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>考核名称：</b><s:property value="supplierEvaluations.seDescribe"/>
				</td>
			</tr>
			<tr align="center" class="Content_tab_style_04">
		  		<th width="15%">序号</th>
		  		<th width="55%">考核人</th>
		  		<th width="15%">已评供应商个数</th>
		  		<th width="15%">供应商个数</th>
			</tr>
			<s:iterator value="#request.seuList" status="supNum">
				<tr <s:if test="#supNum.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
					<td>
					<s:property value="#supNum.count" />
					<input type="hidden" id="" value="<s:property value=""/>"/>
					</td>
					<td><s:property value="userNameCn"/></td>
					<td>
						<!-- <s:if test="sedCount!=0">
						<s:property value="sedCount"/>
						</s:if> -->
						<s:property value="sedCount"/>
					</td>
					<td>
						<s:if test="sumSupplier!=0">
						<a href="#" onclick="createdetailwindow('查看供应商信息','viewAndEvaluateSupplierDetail_multiEvaluataion.action?tempSupEvaInfo=supNum&supplierEvaluations.seId=<s:property value="supplierEvaluations.seId"/>&supplierEvaluateUser.seuId=<s:property value="seuId"/>',1);">
						<s:property value="sumSupplier"/>
						</a>
						</s:if>
					</td>
				</tr>
			</s:iterator>
			</table>
			
</form>
</body>
</html>
