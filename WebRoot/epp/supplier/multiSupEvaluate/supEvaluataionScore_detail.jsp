<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>考核人打分</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script> 
	
</head>
 
<body>
<form id="" name="evaScore" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="seuId" name="supplierEvaluateUser.seuId" value="<s:property value="supplierEvaluateUser.seuId"/>" />
<input type="hidden" id="sesId" name="supplierEvaluateUser.sesId" value="<s:property value="supplierEvaluateUser.sesId"/>" />
<input type="hidden" id="seDate" name="supplierEvaluateResult.seDate" value="<s:property value="supplierEvaluateResult.seDate"/>" />
<input type="hidden" id="evaIndexList" name="" value="<s:property value="listValue.size()"/>" />
    <div class="Conter_main_conter">
	        <table class="table_ys1">
	        	<tr>
	          		<td  colspan="6" class="Content_tab_style_05">
	          		<b>供应商名称：</b><s:property value="supplierEvaluateSup.supplierName"/>&nbsp;&nbsp;
	          		<b>考核人：</b><s:property value="supplierEvaluateUser.userNameCn"/>&nbsp;&nbsp;
	          		<b>考核日期：</b><s:date name="supplierEvaluateResult.seDate" format="yyyy-MM-dd"/>
	          		</td>
	        	</tr>
	        	<tr align="center" class="Content_tab_style_04">
					<th width="5%" nowrap>序号</th>
					<th width="10%" nowrap>指标类别</th>
					<th width="25%" nowrap>指标名称</th>
					<th width="20%" nowrap>指标说明</th>
					<th width="10%" nowrap>指标分值</th>
					<th width="10%" nowrap>考核分</th>
				</tr>
				<s:iterator value="listValue" status="supScore">
					<tr id="<s:property value="eiId"/>" <s:if test="#supScore.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td>
						<s:property value="#supScore.count" />
						<input type="hidden" id="serId<s:property value="#supScore.index" />" name="supEvaResultList[<s:property value="#supScore.index" />].serId" value="<s:property value="serId"/>"/>
						</td>
						<td><s:property value="eikName"/></td>
						<td><s:property value="eiName"/></td>
						<td><s:property value="eiDescribe"/></td>
						<td><s:property value="adjustPoints"/></td>
						<td><s:property value="judgePoint" /></td>
					</tr>
				</s:iterator>
	        </table>
		</div>
		
			
</form>
</body></html>