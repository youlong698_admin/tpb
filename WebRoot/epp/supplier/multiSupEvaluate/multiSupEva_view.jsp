<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>供应商考核管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/supplier/supCommuInfo.js" type="text/javascript" ></script> 
<script type="text/javascript">
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "viewToSupplierEvaluataion_multiEvaluataion.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX,
            {
            	className : "ellipsis",
            	data: "seCode",   
            	render: viewGeneralManage,
            	orderable : false	
            },
            {	
            	className : "ellipsis",
            	data: "seDescribe",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%",
            	orderable : false		
            },
			{
				data : "supNum",
				orderable : false,
				render: viewDetailBySupNum,
				width : "7%"
			},
			{
				data : "userNum",
				render: viewDetailByJudgerNum,
				width : "10%",
				orderable : false	
			},
			{
				className : "ellipsis",
				data : "timeStart",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "10%"
			},
			{
				data : "timeEnd",
				orderable : false,
				width : "7%"
			},
			{
				className : "ellipsis",
				data : "writer",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "10%",
				orderable : false	
			},
			{
				className : "ellipsis",
				data : "writeDate",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "10%"
			},
			{
				data : "evastatus",
				orderable : false,	
				width : "10%"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	//给当前行某列加样式
        	$('td', row).eq(9).addClass(data.evastatus=="已完成" || data.evastatus=="等待提交"?"text-success":"text-error");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.updateItem(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	})
	//复制
	$("#btn-processCancel").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.pCancelItem(arrItemId);
	})
	//提交确认流程
	$("#btn-processSubmit").click(function(){
  		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.pSubmitItem(arrItemId);
	});
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//默认进入的排序
		 param.orderColumn="de.seId";
		//组装排序参数
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 5:
				param.orderColumn = "de.timeStart";
				break;
			case 8:
				param.orderColumn = "de.writeDate";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		
		param.seDescribe = $("#seDescribe").val();
		param.timeStart = $("#timeStart").val();;
		param.timeEnd = $("#timeEnd").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	addItemInit:function(){
		window.location.href="saveEvaluateSupplierInit_multiEvaluataion.action?tempSupEvaInfo=addInit";
	},
	updateItem:function(selectedItems){
		var arrObj = selectedItems;
		if(arrObj.length!=0){
			if(arrObj.length==1){
				var seId = arrObj[0].seId;
				var status = arrObj[0].status;
				//alert(status);
				if(status=='1'  ){
					showMsg("alert","温馨提示：供应商考核正在进行中，不能修改！");
				}else if(status=='2'){
					showMsg("alert","温馨提示：供应商考核已完成，不能修改！");
				}else if(status=='3'){
					showMsg("alert","温馨提示：供应商考核已过期，不能修改！");
				}else{
					if(seId!=''&&seId!=null){
						window.location.href="viewEvaluateSupplierIndex_multiEvaluataion.action?tempSupEvaInfo=updateInit&supplierEvaluations.seId="+seId;
					}
				}
			}else{
				showMsg("alert","温馨提示：只能选择一条信息！");
			}
		}else{
			showMsg("alert","温馨提示：请选择要修改的信息");
		}

	},
	deleteItem : function(selectedItems) {
		var arrObj = selectedItems;
		var seId = "";
		if(arrObj!=null&&arrObj.length!=0){
			for(var i=0;i<arrObj.length;i++){
				if(arrObj.length==1){
					seId = arrObj[0].seId;
					break;
				}
				seId += arrObj[i].seId +",";
			}
			var status = arrObj[0].status;
			
			if(status=='1'){
				showMsg("alert","温馨提示：供应商考核正在进行中，不能删除！");
			}
			else if(status=='2'){
				showMsg("alert","温馨提示：供应商考核已完成，不能删除！");
			}else if(status=='3'){
				showMsg("alert","温馨提示：供应商考核已过期，不能删除！");
			}else{
				if(seId!=''&&seId!=null){
					//alert(seId);
					if(confirm("温馨提示：你确定要删除吗？")){
						document.forms[0].action="deleteEvaluateSupplier_multiEvaluataion.action?tempSupEvaInfo="+seId;
						document.forms[0].submit();
					}else{
						return false;
					}
				}
			}
		}else{
			showMsg("alert","温馨提示：请选择要删除的信息");
		}
	},
 	//复制
	pCancelItem:function(selectedItems){
		var arrObj = selectedItems;
		var seId = "";
		if(arrObj!=null){
			if(arrObj.length!=0){
				for(var i=0;i<arrObj.length;i++){
					if(arrObj.length==1){
						seId = arrObj[0].seId;
						break;
					}
					seId += arrObj[i].seId +",";
				}
				if(seId!=''&&seId!=null){
					window.location.href="saveCopySupEvalationInfo_multiEvaluataion.action?tempSupEvaInfo="+seId;
				}
			}else{
				showMsg("alert","温馨提示：请选择要复制的信息");
			}
	}
	},
	//提交确认流程
	pSubmitItem:function(selectedItems){
  		var arrObj = selectedItems;
  		if(arrObj.length!=0){
			if(arrObj.length==1){
				var seId = arrObj[0].seId;
				var status = arrObj[0].status;
				if(status=='1'){
					showMsg("alert","温馨提示：供应商考核正在进行中，不能再次提交！");
				}else if(status=='2'){
					showMsg("alert","温馨提示：供应商考核已完成，不能再次提交！");
				}else if(status=='3'){
					showMsg("alert","温馨提示：供应商考核已过期，不能提交！");
				}else{
					if(seId!=''&&seId!=null){
						//if(confirm("温馨提示：你确定要提交吗？")){
							var action = "caculateEvaIndexPoint_multiEvaluataion.action";
							var param = "supplierEvaluations.seId="+seId;
							var result = ajaxGeneral(action,param,"text");
							// alert(result);
							 if(result==100 || result==100.00){
								 if(validateEvaSupRefUser(seId)!=''){
									 showMsg("alert","温馨提示：此次考核中有供应商未设置考核人，请在设置后提交！");
									 //alert("温馨提示：此次考核中供应商【"+validateEvaSupRefUser(seId)+"】没有设置考核人，请在设置后提交！");
								 }else if(validateEvaUserRefSup(seId)!=0){
									 showMsg("alert","温馨提示：此次考核中有考核人未分配供应商，请在设置后提交！");
								 }else{
								  
									 var seId = arrObj[0].seId;
							    	 var processId = arrObj[0].processId;
							    	 var orderId = arrObj[0].orderId;
							    	 var orderState = arrObj[0].orderState;
							    	 var instanceUrl = arrObj[0].instanceUrl;
							    	 window.location.href="saveSubmitSupplierEvaluation_multiEvaluataion.action?processId="+processId+"&orderId="+orderId+"&orderNo="+seId+"&supplierEvaluations.seId="+seId;
								 }
							 }else{
							 
								 if(result==0){
									showMsg("alert","温馨提示：请先设置供应商考核指标，保存后再提交！");
								 }else{
									 showMsg("alert","温馨提示：考核指标分值之和必须等于100，请先调整考核指标分值，保存后再提交！"); 
								 }
							 }
						//}else{
						//	return false;
						//}
					}
				}
			}else{
				showMsg("alert","温馨提示：只能选择一条信息！");
			}
		}else{
			showMsg("alert","温馨提示：请选择要提交考核的信息");
		}
	}
};

    
    //查看
    function viewGeneralManage(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看考核详情\",\"viewEvaluateSupplierIndex_multiEvaluataion.action?tempSupEvaInfo=detail&supplierEvaluations.seId=" + row.seId + "\",1)'>"+data+"</a>"; 
              
    }
     /***查看被评供应商信息***/
	function viewDetailBySupNum(data, type, row,   meta){
		var action="viewSupplierNumberDetail_multiEvaluataion.action?supplierEvaluations.seId="+row.seId;
		var windowURL;
		if(row.supNum!=0){
			windowURL = "<a href='#' onclick=\"createdetailwindow('查看被评供应商信息','"+action+"',1)\">"+data+"</a>";
		}else{
			windowURL = '0';
		}
		return windowURL;
	}
	 /***查看考核人信息***/
	function viewDetailByJudgerNum(data, type, row,   meta){
		var action="viewSupplierNumberToJudger_multiEvaluataion.action?supplierEvaluations.seId="+row.seId;
		var windowURL;
		if(row.userNum!=0){
			windowURL = "<a href='#' onclick=\"createdetailwindow('查看考核人信息','"+action+"',1)\">"+data+"</a>";
		}else{
			windowURL = '0';
		}
		return windowURL;
	}
	
	/*****校验供应商和考核人的关联情况****/
	function validateEvaSupRefUser(seId){
		var action = "validateEvaIndexSupRefUser_multiEvaluataion.action";
		var param = "supplierEvaluations.seId="+seId;
		var result = ajaxGeneral(action,param,"text");
		return result;
	}
	function validateEvaUserRefSup(seId){
		var action = "validateEvaIndexUserRefSup_multiEvaluataion.action";
		var param = "supplierEvaluations.seId="+seId;
		var result = ajaxGeneral(action,param,"text");
		return result;
	}
	
	/***查看供应商考核详细信息弹出窗口***/
	//function openNewWindow(action){
	//	createdetailwindow('查看详情',action,1);
	//}

	
	function doQuery(){
	     _table.draw();
	   }

	//重置
	function doReset(){        
		document.forms[0].elements["seDescribe"].value	=	"";
		document.forms[0].elements["timeStart"].value		=	"";
		document.forms[0].elements["timeEnd"].value	=	"";
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
										     <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i> 新增</button>
							<button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
							<button type="button" class="btn btn-info" id="btn-processSubmit"><i class="icon-white icon-resize-small"></i> 提交</button>
							<button type="button" class="btn btn-warning" id="btn-processCancel"><i class="icon-white icon-ok-sign"></i> 复制</button>

						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
					考核名称：
					<input type="text" placeholder="考核名称" class="input-medium" id="seDescribe" name="" size="14" value="<s:property value="supplierEvaluations.seDescribe"/>"/>
					考核时间起：
	            	<input type="text" placeholder="考核时间起" name="" id="timeStart" value="<s:date name="supplierEvaluations.timeStart" format="yyyy-MM-dd" />" class="input-medium Wdate"
                               onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
	          		  考核时间止：
	            	<input type="text" placeholder="考核时间止" name="" id="timeEnd" value="<s:date name="supplierEvaluations.timeEnd" format="yyyy-MM-dd" />" class="input-medium Wdate"
                               onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
	           
					    <button type="button" class="btn btn-info" id="btn-advanced-search"><i class="icon-white icon-search" ></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon icon-search" ></i> 重置</button>
			
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>考核编号</th>
									<th>考核名称</th>
									<th>被评供应商</th>
									<th>考核人</th>
									<th>考核时间起</th>
									<th>考核时间止</th>
									<th>编制人</th>
									<th>编制日期</th>
									<th>状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>