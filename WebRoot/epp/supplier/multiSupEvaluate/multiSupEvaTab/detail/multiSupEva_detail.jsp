<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>修改供应商考核信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>
</head>
 
<body>
<form id="supEva" name="" method="post" action="">
<input type="hidden" id="" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="" name="supplierEvaluations.seCode" value="<s:property value="supplierEvaluations.seCode"/>" />
<input type="hidden" id="" name="supplierEvaluations.writer" value="<s:property value="supplierEvaluations.writer"/>" />
<input type="hidden" id="" name="supplierEvaluations.writeDate" value="<s:property value="supplierEvaluations.writeDate"/>" />
<input type="hidden" id="" name="supplierEvaluations.status" value="<s:property value="supplierEvaluations.status"/>" />
<input type="hidden" id="" name="supplierEvaluations.isUsable" value="<s:property value="supplierEvaluations.isUsable"/>" />
<div class="Conter_Container">
    <div class="Conter_main_conter">
    
	    	<table class="table_ys1">
	        	<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核编号：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<s:property value="supplierEvaluations.seCode"/>
					</td>
				</tr>
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核名称：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<s:property value="supplierEvaluations.seDescribe"/>
					</td>
				</tr>
				
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核开始日期：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<s:date name="supplierEvaluations.timeStart" format="yyyy-MM-dd" />
					</td>
				</tr>
				
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核结束日期：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<s:date name="supplierEvaluations.timeEnd" format="yyyy-MM-dd" />
					</td>
				</tr>
				
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">备注：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
					   <s:property value="supplierEvaluations.remark"/>
					</td>
				</tr>
	        </table>
        
	</div>
</div>
</form>
</body></html>