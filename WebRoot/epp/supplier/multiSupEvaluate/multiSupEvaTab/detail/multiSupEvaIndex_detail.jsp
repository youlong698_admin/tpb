<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商考核指标明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>
	<script>
	
	</script>
</head>
 
<body>
<form id="evaDetail" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
    <div class="Conter_main_conter">
	        <table class="table_ys1">
	        	<tr align="center" class="Content_tab_style_04">
	        		<th width="3%" nowrap>序号</th>
					<th width="10%" nowrap>指标类别</th>
					<th width="15%" nowrap>指标名称</th>
					<th width="20%" nowrap>指标说明</th>
					<th width="10%" nowrap>基准分</th>
					<th width="10%" nowrap>调整基准分</th>
				</tr>
				<s:iterator value="#request.evaIndexList" status="supIndex">
					<tr id="<s:property value="eiId"/>" <s:if test="#supIndex.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td width="3%" nowrap><s:property value="#supIndex.count"/></td>
						<td width="10%" nowrap align="left"><s:property value="eikName"/></td>
						<td width="15%" nowrap align="left"><s:property value="eiName"/></td>
						<td width="20%" nowrap align="left"><s:property value="eiDescribe"/></td>
						<td width="10%" nowrap><s:property value="points"/></td>
						<td width="10%" nowrap><s:property value="adjustPoints"/></td>
					</tr>
				</s:iterator>
				<tr class="biaoge_01_b">
	          		<td colspan="3"></td>
	          		<td><b>（考核指标分值）合计：</b></td>
	          		<td><b><s:property value="refPointsSum"/></b></td>
	          		<td>
	          			<b id="sumPoint">
		          		<s:if test="adjPointsSum!=0"><s:property value="adjPointsSumStr"/></s:if>
		          		<s:else><s:property value="refPointsSum"/></s:else>
		          		</b>
	          		</td>
	        	</tr>
	        </table>
	</div>
</form>
</body></html>