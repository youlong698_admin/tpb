<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商考核用户明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>  
	<script>
	
	</script>
</head>
 
<body>
<form id="evaDetail" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
   <div class="Conter_main_conter">
	        <table class="table_ys1" >
	        	<tr align="center" class="Content_tab_style_04">
	        		<th width="5%" nowrap>序号</th>
					<th width="10%" nowrap>考核人</th>
					<th width="75%" nowrap>被考核供应商</th>
				</tr>
				<s:iterator value="listValue" status="users">
					<tr <s:if test="#users.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td nowrap><s:property value="#users.count"/></td>
						<td nowrap><s:property value="userNameCn"/></td>
						<td align="left" title="<s:property value="evaSupNames"/>"><s:property value="evaSupNames"/></td>
					</tr>
				</s:iterator>
	        </table>
	</div>
</form>
</body></html>