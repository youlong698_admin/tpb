<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>被考核供应商信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>   
	<script>
	
		
	</script>
</head>
 
<body>
<form id="" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
    <div class="Conter_main_conter">
	        <table class="table_ys1">
	        	<tr align="center" class="Content_tab_style_04">
	        		<th width="5%" nowrap>序号</th>
	        		<th width="20%" nowrap>供应商名称</th>
					<th width="50%" nowrap>考核人</th>
				</tr>
				<s:iterator value="listValue" status="sup">
					<tr <s:if test="#sup.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td nowrap><s:property value="#sup.count"/></td>
						<td align="left"><s:property value="supplierName"/></td>
						<td align="left" title="<s:property value="evaUserNames"/>"><s:property value="evaUserNames"/></td>
					</tr>
				</s:iterator>
	        </table>
	</div>
</form>
</body></html>