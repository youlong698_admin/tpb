<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商考核指标明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>  
	<script type="text/javascript">
	
	</script>
</head>
 
<body>
<form id="evaDetail" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="seCode" name="supplierEvaluations.seCode" value="<s:property value="supplierEvaluations.seCode"/>" />
<input type="hidden" id="status" name="supplierEvaluations.status" value="<s:property value="supplierEvaluations.status"/>" />
<input type="hidden" id="list" value="<s:property value="evaIndexList.size()"/>"/>
<input type="hidden" id="tempsef" name="tempSupEvaInfo" value="<s:property value='tempSupEvaInfo'/>"/>
<input type="hidden" id="idNames" value=""/>
<input type="hidden" id="returnValues" name="returnValues"/>
<!-- 防止表单重复提交 -->
<s:token/>
    <div class="Conter_main_conter">
	        <table class="table_ys1" id="tbIndex">
				<tr align="center" class="Content_tab_style_04">
	        		<th width="3%" nowrap>序号</th>
	        		<th width="3%" nowrap>选择</th>
					<th width="10%" nowrap>指标类别</th>
					<th width="15%" nowrap>指标名称</th>
					<th width="20%" nowrap>指标说明</th>
					<th width="10%" nowrap>基准分</th>
					<th width="10%" nowrap>调整基准分</th>
				</tr>
				<s:iterator value="#request.evaIndexList" status="supIndex">
					<tr id="<s:property value="eiId"/>" <s:if test="#supIndex.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td width="3%" nowrap>
							<s:property value="#supIndex.count"/>
							<input type="hidden" name="evaIndexList[<s:property value="#supIndex.index" />].eikId" value="<s:property value="eikId"/>" />
							<input type="hidden" name="evaIndexList[<s:property value="#supIndex.index" />].eikName" value="<s:property value="eikName"/>" />
							<input type="hidden" name="evaIndexList[<s:property value="#supIndex.index" />].eiId" value="<s:property value="eiId"/>" />
							<input type="hidden" name="evaIndexList[<s:property value="#supIndex.index" />].eiName" value="<s:property value="eiName"/>" />
							<input type="hidden" name="evaIndexList[<s:property value="#supIndex.index" />].eiDescribe" value="<s:property value="eiDescribe"/>" />
							<input type="hidden" name="evaIndexList[<s:property value="#supIndex.index" />].points" value="<s:property value="points"/>" />
						</td>
						<td width="3%" nowrap>
							<s:if test='supplierEvaluations.status=="0"'>
								<input type="checkbox" id="" name="ckEva" value="<s:property value="seiId"/>"/>
							</s:if>
							<s:else>
								<input type="checkbox" disabled id="" name="ckEva" value="<s:property value="seiId"/>"/>
							</s:else>
						</td>
						<td width="10%" nowrap align="left"><s:property value="eikName"/></td>
						<td width="15%" nowrap align="left"><s:property value="eiName"/></td>
						<td width="20%" nowrap align="left"><s:property value="eiDescribe"/></td>
						<td width="10%" nowrap><s:property value="points"/></td>
						<td width="10%" nowrap>
							<s:if test='supplierEvaluations.status!="0"'>
								<s:property value="adjustPoints"/>
							</s:if>
							<s:else>
							<input type="text" onkeyup="value=value.replace(/[^\d\.]/g,'');caculateTdSumValue(this)" id="point<s:property value="#supIndex.index" />" name="evaIndexList[<s:property value="#supIndex.index" />].adjustPoints" value="<s:property value="adjustPoints"/>" />
							</s:else>
		          		</td>
					</tr>
				</s:iterator>
				<tr>
	          		<td colspan="4">&nbsp;</td>
	          		<td><b>（考核指标分值）合计：</b></td>
	          		<td><b><s:property value="refPointsSum"/></b></td>
	          		<td>
	          			<b id="sumPoint">
		          		<s:if test="adjPointsSum!=0"><s:property value="adjPointsSumStr"/></s:if>
		          		<s:else><s:property value="refPointsSum"/></s:else>
		          		</b>
	          		</td>
	        	</tr>
	        </table>
	        <div class="buttonDiv">
		        <s:if test='supplierEvaluations.status=="0"'>
				<button type="button"  onclick="selectEvaIndex();" class="btn btn-info"><i class="icon-white icon-plus-sign"></i>新增</button>
				<button type="button"  onclick="deleteEvaIndexPoints();" class="btn btn-danger"><i class="icon-white icon-remove-sign"></i> 删除</button>
				<button class="btn btn-success" type="button" onclick="adjustEvaIndexPoints();" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				</s:if>
				<button class="btn btn-info" type="button" onclick="goBackMultiEvaluateParentView('viewSupplierEvaluataion');" id="btn-danger"><i class="icon-white icon-repeat"></i>返回</button>
			</div>
	</div>
</form>
</body></html>