<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商考核用户明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>  
</head>
 
<body>
<form id="evaDetail" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="returnValues" name="returnValues"/>
<!-- 防止表单重复提交 -->
<s:token/>
   <div class="Conter_main_conter">
	        <table  class="table_ys1" id="tbUser">
	        	<tr align="center" class="Content_tab_style_04">
					<th width="10%" nowrap>考核人</th>
					<th width="80%" nowrap>供应商</th>
					<th width="10%" nowrap>操作</th>
				</tr>
				<s:iterator value="listValue" status="users">
					<tr <s:if test="#users.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td nowrap><s:property value="userNameCn"/></td>
						<td align="left">
						<span id='es<s:property value="#users.index"/>' title='<s:property value="evaSupNames"/>'><s:property value="evaSupNames"/></span>
						<input type='hidden' id='evaSup<s:property value="#users.index"/>' name='mesUserList[<s:property value="#users.index"/>].evaSuppliers' value='<s:property value="evaSuppliers"/>' />
						 <input type="hidden" id="supplierIds<s:property value="#users.index"/>" name="supplierIds<s:property value="#users.index"/>" value="<s:property value="evaSupplierIds"/>"/>
						</td>
						<td nowrap>
						<button type="button"  onclick="selectEvaSup(<s:property value="#users.index"/>)" class="btn btn-info">分配供应商</button>
						<input type="hidden" id="" name="mesUserList[<s:property value="#users.index"/>].seuId" value="<s:property value="seuId"/>" />
						<input type="hidden" id="" name="mesUserList[<s:property value="#users.index"/>].userNameCn" value="<s:property value="userNameCn"/>" />
						</td>
					</tr>
				</s:iterator>
	        </table>
	        <div class="buttonDiv">
				<button class="btn btn-success" type="button" onclick="multiSupEvaHandle('updateUser');" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				<button class="btn btn-info" type="button"  onclick="goBackMultiEvaluateParentView('viewSupplierEvaluataion');" id="btn-danger"><i class="icon-white icon-repeat"></i>返回</button>
			</div>
	</div>
</form>
</body></html>