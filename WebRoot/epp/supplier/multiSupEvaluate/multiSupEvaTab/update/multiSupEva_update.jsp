<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>修改供应商考核信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>

	<script type="text/javascript">
	$(function(){
	    valueEvaluateUser();
	    valueSupplierInfo();
		//移除供应商
		$('#suppliersChosen .search-choice').live('click',function(){
			var that=$(this);
			var supplierId=that.attr('supplier-id');
			var supplierIds=$("#supplierIds").val();
			//alert(supplierId+"-----"+supplierIds);
			$("#supplierIds").val(supplierIds.replace(","+supplierId+",",","));
			that.remove();
		});
		
		//移除考核人
		$('#user1Chosen .search-choice').live('click',function(){
			var that=$(this);
			var userId=that.attr('user-id');
			var userIds=$("#userIds").val();
			//alert(userId+"--"+userIds)
			$("#userIds").val(userIds.replace(","+userId+",",","));
			that.remove();
		});
		
	}) 
	</script>
</head>
 
<body>
<form class="defaultForm" id="supEva" name="" method="post" action="updateEvaluateSupplier_multiEvaluataion.action">
<input type="hidden" id="" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="" name="supplierEvaluations.seCode" value="<s:property value="supplierEvaluations.seCode"/>" />
<input type="hidden" id="" name="supplierEvaluations.writer" value="<s:property value="supplierEvaluations.writer"/>" />
<input type="hidden" id="" name="supplierEvaluations.writeDate" value="<s:property value="supplierEvaluations.writeDate"/>" />
<input type="hidden" id="" name="supplierEvaluations.status" value="<s:property value="supplierEvaluations.status"/>" />
<input type="hidden" id="" name="supplierEvaluations.comId" value="<s:property value="supplierEvaluations.comId"/>" />
<input type="hidden" id="" name="supplierEvaluations.isUsable" value="<s:property value="supplierEvaluations.isUsable"/>" />
<input type="hidden" id="returnValues" name="returnValues" value="${returnValues }"/>
<input type="hidden" id="returnValuesSupplier" name="returnValuesSupplier" value="${returnValuesSupplier }"/>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container">
    <div class="Conter_main_conter">
	    	<table class="table_ys1">
	        	<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核编号：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<s:property value="supplierEvaluations.seCode"/>
					</td>
				</tr>
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核名称：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<input type="text" datatype="*" nullmsg="考核名称不能为空！" id="" name="supplierEvaluations.seDescribe" value="<s:property value="supplierEvaluations.seDescribe"/>" />
						<div class="info"><span class="Validform_checktip">考核名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</td>
				</tr>
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核人：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<input id="userIds" type="hidden" name="userIds"/>
						<div class="chosen-container-multi">
							<ul class="chosen-choices" id="user1Chosen">
							</ul>
						</div>
						<div id="chosen-img">
						&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择考核人"  onclick="selectEvaluateUser();"/>
						</div>
					</td>
				</tr>
				
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">被评供应商：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<input id="supplierIds" type="hidden" name="supplierIds"/>
						<div class="chosen-container-multi">
							<ul class="chosen-choices" id="suppliersChosen">
							</ul>
						</div>
						<div id="chosen-img">
						&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择被评供应商"  onclick="selectSupplierInfo();"/>
						</div>
					</td>
				</tr>
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核开始日期：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<input type="text" id="timeStart" name="supplierEvaluations.timeStart" value="<s:date name="supplierEvaluations.timeStart" format="yyyy-MM-dd" />" class="Wdate"
	                               onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />&nbsp;<font color="#ff0000">*</font>
					</td>
				</tr>
				
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">考核结束日期：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
						<input type="text" id="timeEnd" name="supplierEvaluations.timeEnd" value="<s:date name="supplierEvaluations.timeEnd" format="yyyy-MM-dd" />" class="Wdate"
	                               onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />&nbsp;<font color="#ff0000">*</font>
					</td>
				</tr>
				
				<tr>
					<td width="15%" class="Content_tab_style1" colspan="3">备注：</td>
					<td width="35%" class="Content_tab_style2" colspan="3">
					   <textarea id="" name="supplierEvaluations.remark" cols="40%" rows="5" class="Content_input_style2" ><s:property value="supplierEvaluations.remark"/></textarea>
					</td>
				</tr>
	        </table>
         <div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-cacel" type="button" id="btn-cacel"  onclick="goBackMultiEvaluateParentView('viewSupplierEvaluataion');"><i class="icon icon-repeat"></i>返 回</button>
	
      	</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			
			var userIds=$("#userIds").val();
			$("#userIds").val(userIds.substring(1, userIds.lastIndexOf(",")));
			
			var supplierIds=$("#supplierIds").val();
			$("#supplierIds").val(supplierIds.substring(1, supplierIds.lastIndexOf(",")));
			
			return true;
		}
	});
})
</script>
</body></html>