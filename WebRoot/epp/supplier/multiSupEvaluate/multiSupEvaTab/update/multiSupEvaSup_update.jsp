<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>被考核供应商信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script>   
</head>
 
<body>
<form id="" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="returnValues" name="returnValues"/>
<!-- 防止表单重复提交 -->
<s:token/>
    <div class="Conter_main_conter">
	        <table class="table_ys1" id="tbSup">
	        	<tr align="center" class="Content_tab_style_04">
	        		<th width="30%" nowrap>供应商名称</th>
					<th width="60%" nowrap>考核人</th>
					<th width="10%" nowrap>操作</th>
				</tr>
				<s:iterator value="listValue" status="sup">
					<tr <s:if test="#sup.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td align="left">&nbsp;<s:property value="supplierName"/></td>
						<td align="left">
						<span id='eu<s:property value="#sup.index"/>' title='<s:property value="evaUserNames"/>'><s:property value="evaUserNames"/></span>
						<input type='hidden' id='evaUsers<s:property value="#sup.index"/>' name='mesSupList[<s:property value="#sup.index"/>].evaUsers' value='<s:property value="evaUsers"/>' />
						 <input type="hidden" id="userIds<s:property value="#sup.index"/>" name="userIds<s:property value="#sup.index"/>" value="<s:property value="evaUserIds"/>"/>
						</td>
						<td>
						<button type="button"  onclick="selectEvaUser(<s:property value="#sup.index"/>)" class="btn btn-info">分配考核人</button>
						<input type="hidden" id="" name="mesSupList[<s:property value="#sup.index"/>].sesId" value="<s:property value="sesId"/>" />
						<input type="hidden" id="" name="mesSupList[<s:property value="#sup.index"/>].supplierName" value="<s:property value="supplierName"/>" />
						</td>
					</tr>
				</s:iterator>
	        </table>
	        <div class="buttonDiv">
				<button class="btn btn-success" type="button" onclick="multiSupEvaHandle('updateSup');" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				<button class="btn btn-info" type="button" onclick="goBackMultiEvaluateParentView('viewSupplierEvaluataion');" id="btn-danger"><i class="icon-white icon-repeat"></i>返回</button>
			</div>
	</div>
</form>
</body></html>