<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	
    <title>查看每个考核人对供应商评分明细</title>
     <script>
	var api = frameElement.api, W = api.opener;
	
	</script>
	</head>
	<body>
	<form method="post" id="arrange" action="">
		<table class="table_ys1">
			<tr align="center" class="Content_tab_style_05">
		  		<td width="92%" colspan="4"><b>供应商名称：</b><s:property value="#request.seSup.supplierName"/>  
		  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>总得分：</b><s:property value="supplierEvaluateSup.supSumPoints"/></td>
			</tr>
			<tr align="center" class="Content_tab_style_04">
		  		<th width="23%">序号</th>
		  		<th width="23%">考核人</th>
		  		<th width="23%">分数</th>
		  		<th width="23%">考核日期</th>
			</tr>
			<s:iterator value="#request.seuPointList" status="judNum">
				<tr <s:if test="#judNum.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
					<td><s:property value="#judNum.count" />
					<input type="hidden" id="" value="<s:property value=""/>"/>
					</td>
					<td><s:property value="userName"/></td>
					<td>
					<a href="#" onclick="openNewWindow('viewEvaPointsSuppliers_evaSupplier.action?tempSupEvaInfo=supIndex&supplierEvaluateResult.seId=<s:property value="supplierEvaluations.seId"/>&supplierEvaluateResult.sesId=<s:property value='supplierEvaluateSup.sesId'/>&supplierEvaluateResult.seuId=<s:property value="seuId"/>&supplierEvaluateUser.seDate=<s:property value="seDate"/>')">
					<s:if test="evaPoint!=0"><s:property value="evaPoint"/></s:if>
					</a>
					</td>
					<td><s:property value="seDate"/></td>
				</tr>
			</s:iterator>
			</table>
	</form>
	</body>
</html>
