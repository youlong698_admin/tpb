<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	 
    <title>查看考核人考核的供应商个数</title>
    <script>
	var api = frameElement.api, W = api.opener;
	
	</script>
	</head>
	<body>
	<form method="post" id="" action="">
		<table class="table_ys1">
			<tr align="center" class="Content_tab_style_05">
		  		<td width="100%" colspan="4"><b>考核编号：</b><s:property value="supplierEvaluations.seCode"/>
				  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>考核名称：</b><s:property value="supplierEvaluations.seDescribe"/>
				</td>
			</tr>
			<tr align="center" class="Content_tab_style_04">
		  		<th width="30%">序号</th>
		  		<th width="35%">考核人</th>
		  		<th width="15%">已评供应商数</th>
		  		<th width="15%">应评供应商数</th>
			</tr>
			<s:iterator value="#request.seuList" status="supNum">
				<tr <s:if test="#supNum.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
					<td>
					<s:property value="#supNum.count" />
					<input type="hidden" id="" value="<s:property value=""/>"/>
					</td>
					<td><s:property value="userName"/></td>
					<td>
						<s:if test="sedCount!=0">
						<a href="#" onclick="openNewWindow('viewAndEvaluateSupplier_evaSupplier.action?tempSupEvaInfo=supNum&supplierEvaluations.seId=<s:property value="supplierEvaluations.seId"/>&supplierEvaluateUser.seuId=<s:property value="seuId"/>')">
						<s:property value="sedCount"/>
						</a>
						</s:if>
						<s:else><s:property value="sedCount"/></s:else>
					</td>
					<td><s:property value="sumSupplier"/></td>
				</tr>
			</s:iterator>
			</table>
			
</form>
</body>
</html>
