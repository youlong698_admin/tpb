<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<title>供应商考核明细信息</title>
		<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	    <script type="text/javascript">
		<c:if test="${tempSupEvaInfo!='updateInit'}">
            var api = frameElement.api, W = api.opener;
	    </c:if>
	      function switchTab(url){
	         $('#iframe').attr('src',url);
	      }
	    </script>
	
	</head>
	<body>
	 <div class="page-content">

         <!-- /.page-header --> 

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">
                         <c:choose>
							<c:when test="${tempSupEvaInfo=='updateInit'}">
	
                             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="switchTab('updateEvaluateSupplierInit_multiEvaluataion.action?supplierEvaluations.seId=<s:property value='supplierEvaluations.seId'/>&tempSupEvaInfo=<s:property value='tempSupEvaInfo'/>');">供应商考核基本信息</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewEvaluateSupplierEvaIndex_multiEvaluataion.action?supplierEvaluations.seId=<s:property value='supplierEvaluations.seId'/>&tempSupEvaInfo=<s:property value='tempSupEvaInfo'/>');">供应商考核指标信息</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('updateEvaluateSupplierSupInit_multiEvaluataion.action?supplierEvaluations.seId=<s:property value='supplierEvaluations.seId'/>&tempSupEvaInfo=<s:property value='tempSupEvaInfo'/>');">被考核供应商信息</a>
                                 </li>
                                 
                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('updateEvaluateSupplierUsersInit_multiEvaluataion.action?supplierEvaluations.seId=<s:property value='supplierEvaluations.seId'/>&tempSupEvaInfo=<s:property value='tempSupEvaInfo'/>');">供应商考核人信息</a>
                                 </li>
                                 

                             </ul>
							</c:when>
							<c:otherwise>
							<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="switchTab('viewSupplierEvaluataionDetail_multiEvaluataion.action?supplierEvaluations.seId=${supplierEvaluations.seId}&tempSupEvaInfo=${tempSupEvaInfo }');">供应商考核基本信息</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewEvaluateSupplierEvaIndexDetail_multiEvaluataion.action?supplierEvaluations.seId=${supplierEvaluations.seId}&tempSupEvaInfo=${tempSupEvaInfo }');">供应商考核指标信息</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewEvaluateSupplierSupDetail_multiEvaluataion.action?supplierEvaluations.seId=${supplierEvaluations.seId}&tempSupEvaInfo=${tempSupEvaInfo }');">被考核供应商信息</a>
                                 </li>
                                 
                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewEvaluateSupplierUsersDetail_multiEvaluataion.action?supplierEvaluations.seId=${supplierEvaluations.seId}&tempSupEvaInfo=${tempSupEvaInfo }');">供应商考核人信息</a>
                                 </li>
                                 

                             </ul>
							</c:otherwise>
							</c:choose>
                             <div class="tab-content">

                                 <div id="smsj">
                                	 <s:if test='tempSupEvaInfo=="updateInit"'>
	
                                    	 <iframe src="updateEvaluateSupplierInit_multiEvaluataion.action?supplierEvaluations.seId=${supplierEvaluations.seId}&tempSupEvaInfo=${tempSupEvaInfo}" id="iframe" name="botFrame" frameborder=0 scrolling="yes" width="100%" height="500px"></iframe>
                                     </s:if>
                                     <s:else>
                                     	<iframe src="viewSupplierEvaluataionDetail_multiEvaluataion.action?supplierEvaluations.seId=${supplierEvaluations.seId }&tempSupEvaInfo=${tempSupEvaInfo}" id="iframe"  name="botFrame" frameborder=0 scrolling="yes" width="100%" height="500px"></iframe>
                                   	</s:else>
                                 </div> 
                             </div>
                         </div>

                     </div>
                 </div>

                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>		
</body>
</html>