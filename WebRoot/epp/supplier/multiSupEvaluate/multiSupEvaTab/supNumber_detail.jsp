<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	
    <title>查看被评供应商信息</title>
    <script>
	var api = frameElement.api, W = api.opener;
	
	</script>
	</head>
	<body>
	<form method="post" id="arrange" action="">
	<table  class="table_ys1">
		<tr align="center" class="Content_tab_style_05">
		  		<td width="100%" colspan="6"><b>考核编号：</b><s:property value="supplierEvaluations.seCode"/>
				  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>考核名称：</b><s:property value="supplierEvaluations.seDescribe"/>
				</td>
			</tr>
		<tr align="center" class="Content_tab_style_04">
	  		<th width="5%">序号</th>
			<th width="20%">供应商名称</th>
			<th width="10%">供应商简称</th>
			<th width="15%">平均得分</th>
			<th width="15%">已提交考核人数量</th>
			<th width="15%">应提交考核人数量</th>
		</tr>
		<s:iterator value="#request.supAvgPointList" status="supNum">
			<tr <s:if test="#supNum.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
				<td>
				<s:property value="#supNum.count" />
				<input type="hidden" id="" value="<s:property value=""/>"/>
				</td>
				<td><s:property value="supplierName"/></td>
				<td><s:property value="supplierNameSimple"/></td>
				<td>
					<s:if test='supAvgPoint!="null"|supAvgPoint!=""'>
						<a href="#" onclick="openNewWindow('viewSupplierJudgerScoreDetail_evaSupplier.action?supplierEvaluations.seId=<s:property value='supplierEvaluations.seId'/>&supplierEvaluateSup.sesId=<s:property value='sesId'/>&supplierEvaluateSup.supSumPoints=<s:property value='supSumPoints'/>')">
						<s:property value="supAvgPoint"/>
						</a>
					</s:if>
				</td>
				<td><s:property value="submitUserNum"/></td>
				<td><s:property value="sumUserNum"/></td>
			</tr>
		</s:iterator>
		</table>
		
	</form>
	</body>
</html>
