<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商考核评分</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script> 	
<script type="text/javascript">

var api = frameElement.api, W = api.opener;
</script>
</head>
 
<body>
<form id="evaDetail" name="" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="seuId" name="supplierEvaluateUser.seuId" value="<s:property value="supplierEvaluateUser.seuId"/>" />
<input type="hidden" id="status" name="supplierEvaluations.status" value="<s:property value="supplierEvaluations.status"/>" />
<input type="hidden" id="timeStart" name="" value="<s:date name="supplierEvaluations.timeStart" format="yyyy-MM-dd"/>" />
<input type="hidden" id="timeEnd" name="" value="<s:date name="supplierEvaluations.timeEnd" format="yyyy-MM-dd"/>" />
<input type="hidden" id="idPoints" name="" value="" />
<input type="hidden" id="taskId" name="" value="<s:property value="#request.taskId"/>" />
    <div class="Conter_main_conter">
	        <table width="100%" border="0" cellspacing="1" cellpadding="0" class="table_ys2">
	        	<tr>
	          		<td  colspan="6" class="Content_tab_style_04">
	          		<b>考核编号：</b><s:property value="supplierEvaluations.seCode"/>&nbsp;&nbsp;
	          		<b>考核名称：</b><s:property value="supplierEvaluations.seDescribe"/>&nbsp;&nbsp;
	          		<b>考核人：</b><s:property value="supplierEvaluateUser.userName"/>&nbsp;&nbsp;
	          		<b>考核日期起：</b><s:date name="supplierEvaluations.timeStart" format="yyyy-MM-dd"/>&nbsp;&nbsp;
	          		<b>考核日期止：</b><s:date name="supplierEvaluations.timeEnd" format="yyyy-MM-dd"/>&nbsp;&nbsp;
	          		</td>
	        	</tr>
	        	<tr align="center" class="Content_tab_style_04">
					<th width="5%" nowrap>序号</th>
					<th width="15%" nowrap>供应商名称</th>
					<th width="25%" nowrap>采购项目名称</th>
					<th width="10%" nowrap>考核得分</th>
					<th width="15%" nowrap>考核日期</th>
					<th width="20%" nowrap>操作</th>
				</tr>
				<s:iterator value="listValue" status="point">
						<tr id="<s:property value="eiId"/>" <s:if test="#point.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td>
							<s:property value="#point.count" />
							<input type="hidden" id="sesId<s:property value="#point.index" />" value="<s:property value="sesId"/>"/>
							<input type="hidden" id="supplierId" value="<s:property value="supplierId"/>"/>
						</td>
						<td align="left" title="点此链接可以查看供应商基本信息">
						<a href='##' onclick="createdetailwindow('供应商信息','viewSupplierSelectInfo_supplierBaseInfo.action?supplierInfo.supplierId=<s:property value="supplierId" />',1)">
						<s:property value="supplierName"/>
						</a>
						</td>
						<td align="left" title="点此链接可以查看采购单信息"><s:property value="projectName" escapeHtml="false"/></td>
						<td><div id="jdPoint<s:property value="#point.index" />"><s:if test="judgeSumPoint!=0"><s:property value="supPoints"/></s:if></div></td>
						<td><div id="jdDate<s:property value="#point.index" />"><s:date name="seDate" format="yyyy-MM-dd"/></div></td>
						<td>
							<button class="btn btn-info" type="button" class="btcommon1" id="evaPoint<s:property value="#point.index" />" <s:if test='status=="1"'>onclick="openNewWindow('viewSupplierEvaIndexScoreDetail_multiEvaluataion.action?tempSupEvaInfo=supIndex&supplierEvaluateResult.seId=<s:property value="supplierEvaluations.seId"/>&supplierEvaluateResult.sesId=<s:property value='sesId'/>&supplierEvaluateResult.seuId=<s:property value="seuId"/>&supplierEvaluateResult.seDate=<s:property value="seDate"/>')"</s:if><s:else>onclick="supEvaluationPoints(<s:property value="sesId"/>,<s:property value="#point.index" />,'save');"</s:else> >评  价</button>
							&nbsp;&nbsp;
							<button class="btn btn-info" type="button" class="btcommon1" <s:if test='status=="1"'>disabled</s:if> id="subPoint<s:property value="#point.index" />" onclick="submitJdPoint(<s:property value="#point.index" />,'submit');"  >提  交</button>
						</td>
					</tr>
				</s:iterator>
	        </table>
	        
	</div>
</form>
</body></html>