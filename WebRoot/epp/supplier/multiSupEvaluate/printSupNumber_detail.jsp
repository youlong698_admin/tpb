<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>打印合作供应商考核结果告知函</title>
	<script src="<%=path%>/common/script/context.js" type="text/javascript" ></script>
	 <script>
	var api = frameElement.api, W = api.opener;
	
	</script>
	<script language="javaScript">
		function getSysDate(){
			if(document.getElementById("date")){
				var str;
				var myDate = new Date();
				str=myDate.getFullYear()+"年";
				document.getElementById("date").innerHTML=str;
			}
			if(document.getElementById("date1")){
				var str;
				var myDate = new Date();
				str=myDate.getFullYear()+"年";
				document.getElementById("date1").innerHTML=str;
			}
			if(document.getElementById("dateTime")){
				var strDate;
				var myDate = new Date();
				strDate=myDate.getFullYear()+"年"+(myDate.getMonth()+1)+"月"+myDate.getDate()+"日";
				document.getElementById("dateTime").innerHTML=strDate;
			}
		}
		
	</script>
</head>
  
<body onload="getSysDate();"> 

<form id="showProjects" action="" method="post">
<input type="hidden" name="supplierInfo.supplierId" value="${supplierId }" />
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	          <tr>
		        <td class="Content_tab_style2" align="right" >
		        	<button type="button" class="btn btn-info" id="printDisplay" onclick="printDisplay.style.display='none';window.print();printDisplay.style.display='';"><i class="icon-white icon-plus-sign"></i> 打 印</button>
					
				</td>
			</tr>
	         <tr>
	           <td>
			<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			 <tr>
			    <td style="height:50;" align="center"  class="print_bid_title"><strong><font size="6" style="color:#FF0000">${systemCurrentDept }</font></strong></td>
			 </tr>
			 <tr>
			 <td><hr style="border:6;color:#FF0000" width="50%" size=6></td>
			 </tr>
			 <tr>
				<td style="height:10;" >&nbsp;</td>
			 </tr>
			 <tr>
				 <td align="center"><font size="4">合作供应商考核结果告知函</font><td/>
			 </tr>
			 <tr>
				<td style="height:10;" >&nbsp;</td>
			 </tr>
			 <tr>
				<td style="height:10;" >&nbsp;</td>
			 </tr>
			 <tr>
				<td align="center">
					<p style="font-size:13px;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;致:<strong><input id="" name="" readonly value="<s:property value='supplierInfo.supplierName'/>" 
	            	style="border-left:0px;border-top:0px;border-right:0px;border-bottom:1px solid #384B4A;width:260px"/></strong>
            		</p>
            	</td>
			 </tr>
			 <tr>
				<td style="height:10;" >&nbsp;</td>
			 </tr>
				
				<br>
			 <tr>
				<td align="center" style="font-size:13px;">
		  			${systemCurrentDept }<span id="date1" ></span >供应商考核工作已结束,<br/><br/>
					经过综合考核,贵公司在"卓越,优秀,良好,一般,有待提高"<br/><br/>
					五个供应商考核等级中核定为 
					<strong><input id="rank" name="" readonly value="<s:property value='supplierInfo.supplierLevel'/>" style="border-left:0px;border-top:0px;border-right:0px;border-bottom:1px solid #384B4A;width:28px"/>
					</strong>等级。<br/><br/>
	 			</td>
			<tr/>
			
			  <tr>
				<td style="height:10;" >&nbsp;</td>
			 </tr>

				<br>
			<tr>
	 			<td align="center" style="font-size:13px;">
		 			针对卓越等级的供应商,后续合作中我公司将本着加强合作,共创<br/><br/>
					未来的原则优先择选,同时在首付款比例,保函年限,商务审核效率<br/><br/>
					等方面予以一定的支持,并依据双方发展策略适时开展战略合作。<br/><br/>
					
				</td>
			</tr>

			 <tr>
				<td style="height:20;" >&nbsp;</td>
			 </tr>
			 
			  <tr>
				<td align="center">特此告知！</td>
			 </tr>
			 
			 <tr>
				<td style="height:10;" >&nbsp;</td>
			 </tr>
			 
		</table>
		        </td>
	        </tr>
	        <tr>
		        <td>
		<table width="40%" border="0" align="center" cellpadding="0" cellspacing="10" >
			 <tr> 
			    <td align="right">
					<span style="float:right; font-size:14px;"><strong>${systemCurrentDept }</strong></span><br/>
					&nbsp;&nbsp;
				</td>
			 </tr>
			 
			 <tr>
				<td align="right">
				 <strong> 日期：<span id="dateTime" style="float:right; font-size:14px;"></span></strong><br />
				</td>
			 </tr>
		</table>			
		        </td>
	         </tr>
	          <tr>
			 <td><hr style="border:6;color:#FF0000" width="50%" size=6></td>
			 </tr>
</table>

</form>

</body>
</html>
