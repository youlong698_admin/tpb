<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>考核人打分</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/supplier/multiEvaluateSupplier.js" type="text/javascript" ></script> 
<script type="text/javascript">

var api = frameElement.api, W = api.opener;
</script>
</head>
 
<body>
<form id="" name="evaScore" method="post" action="">
<input type="hidden" id="seId" name="supplierEvaluations.seId" value="<s:property value="supplierEvaluations.seId"/>" />
<input type="hidden" id="seuId" name="supplierEvaluateUser.seuId" value="<s:property value="supplierEvaluateUser.seuId"/>" />
<input type="hidden" id="sesId" name="supplierEvaluateResult.sesId" value="<s:property value="supplierEvaluateResult.sesId"/>" />
<input type="hidden" id="seDate" name="supplierEvaluateResult.seDate" value="<s:property value="supplierEvaluateResult.seDate"/>" />
<input type="hidden" id="evaIndexList" name="" value="<s:property value="listValue.size()"/>" />
<input type="hidden" id="index" name="index" value="<s:property value="#request.index"/>" />
    <div class="Conter_main_conter">
		<!-- 操作区域  begin-->
		<div class="right4" >
			<ul>
	        	<li><button class="btn btn-info" type="button" class="btcommon1" onclick="confirmSupEvaPoints();"  id="button">确  定</button></li>
			</ul>
		</div>
		<!-- 操作区域  end-->
	        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_ys2" id="evaIndex">
	        	<tr>
	          		<td  colspan="6" class="Content_tab_style_04">
	          		<b>供应商名称：</b><s:property value="supplierEvaluateSup.supplierName"/>&nbsp;&nbsp;
	          		<b>考核人：</b><s:property value="supplierEvaluateUser.userName"/>&nbsp;&nbsp;
	          		<b>考核日期：</b><s:date name="supplierEvaluateResult.seDate" format="yyyy-MM-dd"/>&nbsp;&nbsp;
	          		</td>
	        	</tr>
	        	<tr align="center" class="Content_tab_style_04">
					<th width="5%" nowrap>序号</th>
					<th width="10%" nowrap>指标类别</th>
					<th width="25%" nowrap>指标名称</th>
					<th width="20%" nowrap>指标说明</th>
					<th width="10%" nowrap>指标分值</th>
					<th width="10%" nowrap>考核分</th>
				</tr>
				<s:iterator value="listValue" status="supScore">
					<tr id="<s:property value="eiId"/>" <s:if test="#supScore.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
						<td>
						<s:property value="#supScore.count" />
						<input type="hidden" id="serId<s:property value="#supScore.index" />" name="supEvaResultList[<s:property value="#supScore.index" />].serId" value="<s:property value="serId"/>"/>
						</td>
						<td align="left"><s:property value="eikName"/></td>
						<td align="left"><s:property value="eiName"/></td>
						<td align="left"><s:property value="eiDescribe"/></td>
						<td><s:property value="adjustPoints"/></td>
						<td>
							<s:if test='status!="1"'>
								<input type="text" id="jdPoint<s:property value="#supScore.index" />" onkeyup="value=value.replace(/[^\d\.]/g,'');isOverBaseValue(this)" name="supEvaResultList[<s:property value="#supScore.index" />].judgePoint" value="<s:property value="judgePoint" />" class="Content_input_style1"/>
							</s:if>
							<s:else>
								<s:property value="judgePoint" />
							</s:else>
						</td>
					</tr>
				</s:iterator>
	        </table>
	</div>
</form>
</body></html>