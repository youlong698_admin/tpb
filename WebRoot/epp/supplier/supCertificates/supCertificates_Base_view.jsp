<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>供应商资质管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
var typePage="${typePage}";
var api,W;
$(function (){
    var now = new Date(); 
    var str = now.getFullYear()+"-"+((now.getMonth()+1)<10?"0":"")+(now.getMonth()+1)+"-"+(now.getDate()<10?"0":"")+now.getDate();
	
	if(typePage=="1"){
		api = frameElement.api, W = api.opener;
	}
		   
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "viewToSupplierCertificates_base_supCertificates.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            {
				className : "ellipsis",	
				data : "certificateName",
				width : "35%",
            	render: viewSupplierBase,
				orderable : false
			},
			{
				className : "ellipsis",	
				data : "certificateCode",
				width : "15%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{	
				className : "ellipsis",
				data : "timeBegain",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{	
				className : "ellipsis",
				data : "timeEnd",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				className : "ellipsis",	
				data : "certificateProfession",
				width : "15%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				className : "ellipsis",
				data : "remark",
				width : "10%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	$('td', row).eq(3).addClass(data.timeEnd<=str?"text-error":"");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			//$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.sciId";
		
		//组装查询参数
		
		param.certificateName = $("#certificateName").val();
		param.timeBegain = $("#timeBegain").val();
		param.supplierId = $("#supplierId").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	}
	
};
    //查看
    function viewSupplierBase(data, type, row,   meta){
       if(typePage=="1"){
         return "<a href=\"javascript:;\" onclick=\"createdetailwindow('查看资质信息','viewSupplierCertificatesDetail_base_supCertificates.action?supplierCertificateInfo.sciId=" + row.sciId + "',1)\" >"+data+"</a>"; 
       }else{
         return "<a href=\"javascript:;\" onclick=\"openWindow('查看资质信息','viewSupplierCertificatesDetail_base_supCertificates.action?supplierCertificateInfo.sciId=" + row.sciId + "',1)\" >"+data+"</a>"; 
       }      
    }
    
    function doQuery(){
     _table.draw();
    }

	//重置
	function doReset(){        
		document.forms[0].elements["certificateName"].value	=	"";
		document.forms[0].elements["timeBegain"].value		=	"";
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
						   </div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
						
						<input type="hidden" class="input-medium" id="supplierId" name="supplierInfo.supplierId" value="${supplierId}" />
						资质名称：
		            	<input type="text" placeholder="资质名称" class="input-medium" id="certificateName" name="supplierCertificateInfo.certificateName" size="14" value="<s:property value="supplierCertificateInfo.certificateName"/>" />
		         		   有效期起：
		            	<input type="text" placeholder="有效期起" name="supplierCertificateInfo.timeBegain" id="timeBegain" value="<s:date name="supplierCertificateInfo.timeBegain" format="yyyy-MM-dd" />" class="input-medium Wdate"
                               onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
                               
							  <button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
						      <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
						
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>资质名称</th>
									<th>资质证书编号</th>
									<th>有效期起</th>
									<th>有效期止</th>
									<th>资质专业</th>
									<th>备注</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>