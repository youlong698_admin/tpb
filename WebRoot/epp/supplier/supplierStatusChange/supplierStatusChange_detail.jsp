<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看供应商状态变更</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
   var api = frameElement.api, W = api.opener;	
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="" >

<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">供应商名称</td>
			    <td width="70%" class="Content_tab_style2">${supplierStatusChange.supplierName}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更前状态</td>
			    <td width="70%" class="Content_tab_style2">${supplierStatusChange.oldStatusCn}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更后状态</td>
			    <td width="70%" class="Content_tab_style2">${supplierStatusChange.newStatusCn}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更原因</td>
			    <td width="70%" class="Content_tab_style2">${supplierStatusChange.reason}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">操作人</td>
			    <td width="70%" class="Content_tab_style2">${supplierStatusChange.writerCn}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更日期</td>
			    <td width="70%" class="Content_tab_style2"><fmt:formatDate value="${supplierStatusChange.writeDate}" pattern="yyyy-MM-dd" /></td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">备注</td>
			    <td width="70%" class="Content_tab_style2">${supplierStatusChange.remark}</td>
			</tr>			
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">附件</td>
			    <td width="70%" class="Content_tab_style2"><c:out value="${supplierStatusChange.attachmentUrl}" escapeXml="false"/> </td>
			</tr>
		</table>
</div>
</div>
</form>
</body>
</html> 	