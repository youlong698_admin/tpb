<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>供应商状态变更</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script src="<%= path %>/common/script/required/requirePlan.js" type="text/javascript" ></script>
<script type="text/javascript">
	    //附件需要添加的信息
	var sessionId="<%=session.getId()%>";
	var attachmentType="SupplierStatusChange"; //当前是哪个类别功能上传的附件
	var path="<%= path %>" 
	var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
	var fileNameData=[];//已上传的文件名
	var fileTypeData=[];//已上传的文件的格式
	var attIdData=[];//已存入附件表的附件信息
	$(function (){
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		    showMsg('success','${message}');
		    W.doQuery();
		    api.close();
		  	}
		</c:if>
	    
	});
	
	</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="saveSupplierStatusChange_supplierStatusChange.action" >
<input name="supplierStatusChange.supplierId" type="hidden" id="supplierId" value="${supplierId }">
<input name="supplierStatusChange.comId" type="hidden" id="comId" value="${comId }">
<input name="supplierStatusChange.writer" type="hidden" id="writer" value="${writer }">
<input name="supplierStatusChange.writeDate" type="hidden" id="writeDate" value="<fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />">
<input name="supplierStatusChange.oldStatus" type="hidden" id="oldStatus" value="${oldStatus }">
<input name="supplierStatusChange.supplierName" type="hidden" id="supplierName" value="${supplierName }">
<input type="hidden" name="supplierStatusChange.attIds" id="attIds" />
<input type="hidden" name="supplierStatusChange.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="supplierStatusChange.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="supplierStatusChange.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="supplierStatusChange.attIdData" id="attIdData" value=""/>
<input type="hidden" name="returnVals" id="returnVals" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">供应商状态变更</td>
		    </tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">供应商名称</td>
			    <td width="70%" class="Content_tab_style2">${supplierName }</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更前状态</td>
			    <td width="70%" class="Content_tab_style2">${oldStatusCn }</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更后状态</td>
			    <td width="70%" class="Content_tab_style2">
			    <select id="newStatus" name="supplierStatusChange.newStatus" class="input" datatype="*" nullmsg="变更后状态不能为空！" >
	             		<option value = "">--请选择--</option>
	             		<c:forEach items="${statusMap}" var="map">
	             		    <option value="${map.key }">${map.value }</option>
	             		</c:forEach>
			     </select><font color="#ff0000">*</font>
			     <div class="info"><span class="Validform_checktip">变更后状态不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更原因</td>
			    <td width="70%" class="Content_tab_style2"><textarea datatype="*" nullmsg="变更原因不能为空！"  name="supplierStatusChange.reason" id="reason"  rows="2" class="Content_input_style2" ></textarea>
			    <font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">变更原因不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">操作人</td>
			    <td width="70%" class="Content_tab_style2">${writerCn }</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">变更日期</td>
			    <td width="70%" class="Content_tab_style2"><fmt:formatDate value="${now}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> </td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">备注</td>
			    <td width="70%" class="Content_tab_style2">
			    <textarea name="supplierStatusChange.remark" id="remark"  rows="2" class="Content_input_style2" ></textarea>
			    </td>
			</tr>
			
					<tr>
						<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
						<td class="Content_tab_style2" colspan="3">
							<!-- 附件存放 -->
							<div  id="fileDiv" class="panel"> 
							</div>
							<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
							<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
							
						</td>
					</tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;
			var oldStatus=$("#oldStatus").val();
			var newStatus=$("#newStatus").val();
			if(oldStatus==newStatus){
			    showMsg("alert","温馨提示：变更后的状态不能和变更前一样，请核查");
			    return false;
			}
		}
	});
})
</script>
</body>
</html> 	