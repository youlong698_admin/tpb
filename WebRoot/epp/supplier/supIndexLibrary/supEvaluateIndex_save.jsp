<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>供应商指标库信息增加</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
</head>
<body >
<form class="defaultForm" id="" name="" method="post" action="saveEvaluationIndex_supEvaIndex.action">
	<input type="hidden" name="evaluationIndex.eikId" value='<s:property value="evaluationIndexKind.eikId"/>' /> 
	<input type="hidden" name="evaluationIndex.eikName" value='<s:property value="parentEvaluationIndexKind.eikName"/>' /> 
	<input type="hidden" name="evaluationIndex.isUsable" value='0' />
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	<!-- 当前位置区域  begin-->
	<!-- 
	<div class="Conter_right1"><span class="Content_font2">系统管理 >> 供应商指标库管理 >> 供应商指标库信息增加</span></div>
	 -->
    <!-- 当前位置区域  end-->

    <div class="Conter_main_conter" style="width:60%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">供应商考核指标信息增加</td>
		    </tr>
		     <tr>
		    <td height="24" width="30%" align="center" class="Content_tab_style1">指标所属类别：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="parentEvaluationIndexKind.eikName"/>&nbsp;<font color="#FF0000">*</font></td>
		  </tr>
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1"><s:property value=""/>考核指标名称：</td>
		    <td width="70%" class="Content_tab_style2"><input  size="42" datatype="*" nullmsg="考核指标名称不能为空！" type="text" id="" name="evaluationIndex.eiName" value=""/>&nbsp;
		   		 <div class="info"><span class="Validform_checktip">考核指标名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		  </tr>
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1"><s:property value=""/>考核指标说明：</td>
		    <td width="70%" class="Content_tab_style2"><input size="42" datatype="*" nullmsg="考核指标说明不能为空！" type="text" id="" name="evaluationIndex.eiDescribe" value=""/>&nbsp;
		    	<div class="info"><span class="Validform_checktip">考核指标说明不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		  </tr>
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">考核参考分值：</td>
		    <td width="70%" class="Content_tab_style2"><input size="42" datatype="n |/^[0-9]+(.[0-9]{1})+(.[0-9]{2})?$/" nullmsg="考核参考分值不能为空！" errormsg="考核参考分值格式不正确！" type="text" id="referencePoints" name="evaluationIndex.referencePoints"  value=""/>&nbsp;
		    	<div class="info"><span class="Validform_checktip">考核参考分值格式不正确！！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		  </tr>
        <tr>
		    <td height="24" align="center" class="Content_tab_style1"><s:property value=""/>备注：</td>
		    <td width="70%" class="Content_tab_style2">
		    <textarea cols="28" rows="5" id="" name="evaluationIndex.remark"></textarea>
		    </td>
		  </tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"  ><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-cacel" type="button" id="btn-cacel" onclick="window.location.href='viewEvaluationIndex_supEvaIndex.action?evaluationIndexKind.eikId=<s:property value="evaluationIndexKind.eikId"/>'" ><i class="icon icon-repeat"></i>返回</button>
		</div>
</div>
</div>
</form>


<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	