<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>编辑供应商考核指标类别</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/supplier/supEvaluateIndex.js" type="text/javascript" ></script>
<script language="javaScript">
	
	function delet()
	{
		var eikId=document.getElementById("eikId").value;
		$.dialog.confirm("温馨提示：你确定要该指标类别信息！",function(){
		  var result = ajaxGeneral("deleteEvaluationIndexKind_supEvaIndex.action","eikId="+eikId,"text");
			   showMsg('success',''+result+'',function(){
			   		parent.MainFrame.location.href = "viewEvaluationIndex_supEvaIndex.action";
			   		parent.LeftFrame.location.href = "viewEvaluationIndexKindInitTree_supEvaIndex.action";											
   					});
		   	 	});
	}
</script>
</head>
<body >
<form class="defaultForm" id="" name="" method="post" action="updateEvaluationIndexKind_supEvaIndex.action" >
<input type="hidden" id="eikId" name="evaluationIndexKind.eikId" value='<s:property value="evaluationIndexKind.eikId"/>' /> 
<input type="hidden" name="evaluationIndexKind.parentId" value='<s:property value="evaluationIndexKind.parentId"/>' /> 
<input type="hidden" name="evaluationIndexKind.comId" value='<s:property value="evaluationIndexKind.comId"/>' /> 
<input type="hidden" name="evaluationIndexKind.levels" value='<s:property value="evaluationIndexKind.levels"/>' /> 
<input type="hidden" name="evaluationIndexKind.selflevCode" value='<s:property value="evaluationIndexKind.selflevCode"/>' />
<input type="hidden" name="evaluationIndexKind.isHaveChild" value='<s:property value="evaluationIndexKind.isHaveChild"/>' /> 
<input type="hidden" name="evaluationIndexKind.isUsable" value='<s:property value="evaluationIndexKind.isUsable"/>' />
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >

    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">供应商考核指标类别编辑</td>
		    </tr>
		    <tr>
		    <td  width="30%" align="center" class="Content_tab_style1">考核指标类别</td>
		    <td width="70%" class="Content_tab_style2">${peveName }&nbsp;<font color="#FF0000">*</font></td>
		  </tr>
		  <tr>
		    <td  align="center" class="Content_tab_style1">指标类别名称</td>
		    <td width="70%" class="Content_tab_style2">
		    <input size="42"   name="evaluationIndexKind.eikName" datatype="*" nullmsg="指标类别名称不能为空！" type="text" id=""  value="<s:property value="evaluationIndexKind.eikName"/>" />&nbsp;
		    	<div class="info"><span class="Validform_checktip">指标类别名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		    </td>
		  </tr>
		  
		  <tr>
		    <td  align="center" class="Content_tab_style1">排列序号</td>
		    <td width="70%" class="Content_tab_style2"><input size="42"  datatype="n" nullmsg="排列序号不能为空！" errormsg="排列序号必须是数字！"name="evaluationIndexKind.displaySort" type="text" id="" value="<s:property value="evaluationIndexKind.displaySort"/>" />&nbsp;
		    <div class="info"><span class="Validform_checktip">排列序号名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		    </td>
		  </tr>
		  
		  <tr>
		    <td  align="center" class="Content_tab_style1">备&nbsp;&nbsp;&nbsp;&nbsp;注</td>
		    <td width="70%" class="Content_tab_style2">
		    	<textarea cols="30" class="Content_input_style2"  rows="3" name="evaluationIndexKind.remark" id="remark"><s:property value="evaluationIndexKind.remark"/></textarea>
		    </td>
		  </tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"  ><i class="icon-white icon-ok-sign"></i>修改</button>
			<button type="button" class="btn btn-danger" id="btn-del" onclick="delet();"><i class="icon-white icon-trash"></i> 删除</button>
			<button class="btn btn-cacel" id="btn-cacel" type="button" onclick="javascript:window.location.href='viewEvaluationIndex_supEvaIndex.action?evaluationIndexKind.eikId=<s:property value="evaluationIndexKind.eikId"/>'" ><i class="icon icon-repeat"></i>返回</button>
			
		 </div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;
			
			supIndexKindHandle('updateKind');
			return false;	
		}
	});
})
</script>
</body>
</html> 	