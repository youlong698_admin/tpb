<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>显示供应商指标库信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "viewToEvaluationIndex_supEvaIndex.action?evaluationIndex.eikId=${evaluationIndexKind.eikId}&tempSupIndexInfo=<s:property value='tempSupIndexInfo'/>",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX,
            {
            	className : "ellipsis",
            	data: "eiName",
            	width : "25%",
            	render: viewGeneralManage,
            	orderable : false
            },
			{
				className : "ellipsis",
				data : "eiDescribe",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				data : "referencePoints",
				width : "10%"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.editItemInit(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	})
	//编辑指标类别
	$("#btn-editType").click(function(){
		
		GeneralManage.editTypeItem();
	})
	//增加指标类别
	$("#btn-addType").click(function(){
		
		GeneralManage.addTypeItem();
	})
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.eiId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 3:
				param.orderColumn = "de.referencePoints";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		
		//组装查询参数
		
		param.eiName = $("#eiName").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	// 新增
	addItemInit: function()  {
		var eikId = document.getElementById("eikId").value;
		if(eikId=="" || eikId==null)
		{
			showMsg("alert","温馨提示：请选择要增加的上级指标类别");
		}else
		{
		window.location.href="saveEvaluationIndexInit_supEvaIndex.action?evaluationIndexKind.eikId="+eikId;
        } 
	},
	//修改
	editItemInit: function(selectedItems)  {
	   var arrObj = selectedItems;
	   if(arrObj.length!=0){
			if(arrObj.length==1){
				var eiId = arrObj[0].eiId;
				//alert("update"+eiId);
				if(eiId!=''&&eiId!=null){
					window.location.href="updateEvaluationIndexInit_supEvaIndex.action?evaluationIndex.eiId="+eiId;
				}
			}else{
				showMsg("alert","温馨提示：只能选择一条信息！");
			}
		}else{
			showMsg("alert","温馨提示：请选择要修改的信息");
		}
	    	 
	},
	//删除
	deleteItem : function(selectedItems) {
		var arrObj = selectedItems;
		var eiId = "";
		if(arrObj!=null&&arrObj.length!=0){
			for(var i=0;i<arrObj.length;i++){
				if(arrObj.length==1){
					eiId = arrObj[0].eiId;
					break;
				}
				eiId += arrObj[i].eiId +",";
			}
			//alert(eiId);
			if(eiId!=''&&eiId!=null){
				//	document.forms[0].action="deleteEvaluationIndex_supEvaIndex.action?tempSupIndexInfo="+eiId;
				
				$.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
				   result = ajaxGeneral("deleteEvaluationIndex_supEvaIndex.action","tempSupIndexInfo="+eiId);
					   showMsg('success',''+result+'',function(){
					   		doQuery();							
		   					});
				   	 	});       				 
			}
		}else{
			showMsg("alert","温馨提示：请选择要删除的信息");
		}
	},		
	editTypeItem:function(){
		var eikId = document.getElementById("eikId").value;
		if(eikId=="" || eikId==null)
		{
			showMsg("alert","温馨提示：请选择要编辑的指标类别");
		}else
		{
			window.location.href="updateEvaluationIndexKindInit_supEvaIndex.action?evaluationIndexKind.eikId="+eikId;
		}
	},
	addTypeItem:function(){
		var eikId = document.getElementById("eikId").value;
		window.location.href="saveEvaluationIndexKindInit_supEvaIndex.action?evaluationIndexKind.eikId="+eikId;
	}
	
};
    //查看
    function viewGeneralManage(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看考核指标明细信息\",\"viewEvaluationIndexDetail_supEvaIndex.action?evaluationIndex.eiId=" + row.eiId + "\",2)' title='"+data+"'>"+data+"</a>"; 
              
    }
    
    function doQuery(){
     _table.draw();
    }

	//重置
	function doReset(){        
		document.forms[0].elements["eiName"].value	=	"";	
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button> 
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-primary" id="btn-addType"><i class="icon-white icon-plus-sign"></i> 增加指标类别</button>
			                <button type="button" class="btn btn-primary" id="btn-editType"><i class="icon-white icon-edit"></i> 编辑指标类别</button>
							<button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i> 新增</button>
							<button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
						   </div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
					<input type="hidden" id="eikId" name="evaluationIndexKind.eikId" value='<s:property value="evaluationIndexKind.eikId"/>' />
					<input type="hidden" id="parentId" name="evaluationIndexKind.parentId" value='<s:property value="evaluationIndexKind.eikId"/>' /> 
				
					指标名称：
	           			<input class="input-medium" name="" placeholder="指标名称" type="text"  id="eiName" size="14"   value="<s:property value='evaluationIndex.eiName'/>" />
	            		 
	            		  <button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
						<button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
									
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>指标名称</th>
									<th>指标说明</th>
									<th>参考分值</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>