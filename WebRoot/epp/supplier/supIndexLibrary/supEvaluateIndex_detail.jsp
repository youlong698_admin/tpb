<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>供应商考核指标明细信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script type="text/javascript">
		$(function (){
		
			var api = frameElement.api, W = api.opener;
		   
		});
	
	</script>
</head>
<body >
<form id="" name="" method="post" action="">

<div class="Conter_Container" >

    <div class="Conter_main_conter" style="width:60%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">供应商考核指标明细信息</td>
		    </tr>
		     <tr>
		    <td  width="30%" align="center" class="Content_tab_style1">指标所属类别：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="evaluationIndex.eikName"/></td>
		  </tr>
		  <tr>
		    <td  align="center" class="Content_tab_style1"><s:property value=""/>考核指标名称：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="evaluationIndex.eiName"/></td>
		  </tr>
		  <tr>
		    <td  align="center" class="Content_tab_style1"><s:property value=""/>考核指标说明：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="evaluationIndex.eiDescribe"/></td>
		  </tr>
		  <tr>
		    <td  align="center" class="Content_tab_style1">考核参考分值：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="evaluationIndex.referencePoints"/></td>
		  </tr>
        <tr>
		    <td  align="center" class="Content_tab_style1"><s:property value=""/>备注：</td>
		    <td width="70%" class="Content_tab_style2">
		    <s:property value="evaluationIndex.remark"/>
		    </td>
		  </tr>
		</table>
		
</div>
</div>
</form>
</body>
</html> 	