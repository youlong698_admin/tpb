<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>考核指标树</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		
		<link rel="stylesheet" href="<%=path %>/common/ace/assets/css/bootstrap.min.css" />
		
		<script src="<%=path %>/common/jQuery/jquery-1.8.3.min.js"></script>
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		<!--  <script src="<%=path %>/common/zTree/jquery.ztree.excheck.js"></script>-->
		<script type="text/javascript">
		 var setting = {
			view: {
				selectedMulti: false,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			async: {
				enable: true,
				dataType:"json",
				url: "viewEvaluationIndexKindTree_supEvaIndex.action",
				autoParam: ["id"]
			},
			callback: {
				//onCheck: onCheck  //选中事件
				onClick: zTreeOnClick  //点击事件
			}
		};

		
		function zTreeOnClick(e, treeId, treeNode) {
			
			//alert(treeNode.name+"--"+treeNode.id);
			parent.MainFrame.location.href = "viewEvaluationIndex_supEvaIndex.action?evaluationIndexKind.eikId="+treeNode.id+"&tempSupIndexInfo=${tempSupIndexInfo}";
			
		}
		
		function loadShow() {
			
			//alert(treeNode.name+"--"+treeNode.id);
			parent.MainFrame.location.href = "viewEvaluationIndex_supEvaIndex.action";
			
		}
		
		

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		
		/** 
		  * 刷新当前节点 
		  */
		 function refreshNode() { 
		  /*根据 treeId 获取 zTree 对象*/
		  var zTree = $.fn.zTree.getZTreeObj("treeDemo"), 
		  type = "refresh", 
		  /*获取 zTree 当前被选中的节点数据集合*/
		  nodes = zTree.getSelectedNodes(); 
		  /*强行异步加载父节点的子节点。[setting.async.enable = true 时有效]*/
		  zTree.reAsyncChildNodes(nodes[0], type); 
		 }
		 function getParentNode(){
			  var zTree = $.fn.zTree.getZTreeObj("treeDemo"), 
			  type = "refresh", 
			  nodes = zTree.getSelectedNodes(); 
			  /*根据 zTree 的唯一标识 tId 快速获取节点 JSON 数据对象*/
			  var parentNode = zTree.getNodeByTId(nodes[0].parentTId); 
			  return parentNode;		 
		 } 
		 /** 
		  * 刷新当前选择节点的父节点 
		  */
		 function refreshParentNode() { 
		  var zTree = $.fn.zTree.getZTreeObj("treeDemo"), 
		  type = "refresh", 
		  nodes = zTree.getSelectedNodes(); 
		  /*根据 zTree 的唯一标识 tId 快速获取节点 JSON 数据对象*/
		  var parentNode = zTree.getNodeByTId(nodes[0].parentTId); 
		  /*选中指定节点*/
		  zTree.selectNode(parentNode); 
		  zTree.reAsyncChildNodes(parentNode, type); 
		 }
		</script>
	</head>

	<body>
	
		<ul id="treeDemo" class="ztree"></ul>
		
	</body>
</html>