<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商产品明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
  	
</head>
 
<body>

<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
        	
        	<tr>
				<td width="20%" class="Content_tab_style1">产品名称：</td>
				<td width="30%" class="Content_tab_style2">
					<s:property value="supplierProductInfo.productName"/>
				</td>
				<td width="20%" class="Content_tab_style1">产品类别：</td>
				<td width="30%" class="Content_tab_style2">
					<s:property value="supplierProductInfo.productDictionaryName"/>
				</td>
			</tr>
        	
    		<tr>
    			<td class="Content_tab_style1">产品技术参数：</td>
				<td  class="Content_tab_style2">
					<s:property value="supplierProductInfo.productTechParameter"/>
				</td>
				<td  class="Content_tab_style1">规格型号：</td>
				<td  class="Content_tab_style2">
					<s:property value="supplierProductInfo.productKind"/>
				</td>
			</tr>

			<tr>
				<td class="Content_tab_style1">计量单位：</td>
				<td  class="Content_tab_style2">
					${supplierProductInfo.unit}
				</td>
				<td  class="Content_tab_style1">价格：</td>
				<td  class="Content_tab_style2">
				   <c:choose>
				      <c:when test="${supplierProductInfo.price!=0}">
					    ${supplierProductInfo.price}
				       </c:when>
				      <c:otherwise>
				                     面议
				      </c:otherwise>
				   </c:choose>
				</td>
			</tr>
			
			<tr>
			    <td  class="Content_tab_style1">产品销售占公司收入比例（%）：</td>
				<td  class="Content_tab_style2">
					${supplierProductInfo.salesPercent}
				</td>
				<td class="Content_tab_style1">产品销售情况：</td>
				<td  class="Content_tab_style2">
					${supplierProductInfo.productSales}
				</td>
			</tr>
			
			<tr>
				<td class="Content_tab_style1">产品图片：</td>
				<td  class="Content_tab_style2" colspan="3">
				   <c:if test="${fn:length(listImg)>0}">
				   <table border="0" cellspacing="0" cellpadding="1" id="imgTable">
						<!-- 图片存放区域 -->
							<tr class="imgTr">
							 <c:forEach items="${listImg}" var="img">
							  <td width="150px"><img width="150px" height="100px" src="/fileWeb/${img.img}"></td>
							 </c:forEach>
							</tr>
						
					</table>
					</c:if>
				</td>
			</tr>
			
			<tr>
				<td class="Content_tab_style1">产品详情：</td>
				<td  class="Content_tab_style2" colspan="3">
				   ${supplierProductInfo.description}
				</td>
			</tr>
			
        </table>
	</div>
</div>
</body></html>