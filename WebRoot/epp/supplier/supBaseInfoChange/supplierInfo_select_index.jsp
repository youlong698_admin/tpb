<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<title>供应商变更审核</title>
	    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	    <script type="text/javascript">
	    var api = frameElement.api, W = api.opener;
		    
	      function switchTab(url){
	         $('#iframe').attr('src',url);
	      }
	      function openWindowFrame(title,url,type){
		      createdetailwindow(title,url,type);	
		  }
		  function submitSupplierAudit(param){
		        var supplierId=$("#supplierId").val();
				$.dialog.confirm("温馨提示：您确定要提交审核吗？",function(){
					
					$(".buttonDiv").css("display","none");
					var result = ajaxGeneral("updateSupplierInfoChange_supplierBaseInfo.action","supplierInfo.supplierId="+supplierId+"&result="+param);
				        showMsg('success',''+result+'',function(){
				        api.reload(W);
				   		api.close();							
   					});
				},function(){},api)
      }
	    </script>
	</head>
	<body>
	 <input type="hidden" id="supplierId" value="${supplierInfo.supplierId }"/>
	 <div class="page-content">
         
         <!-- /.page-header -->

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">

                             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="switchTab('viewSupplierBaseInfoDetailChange_supplierBaseInfo.action?supplierInfo.supplierId=${supplierInfo.supplierId }');">基本信息</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewSupplierCertificatesChange_base_supCertificates.action?supplierId=${supplierInfo.supplierId }');">资质信息</a>
                                 </li>
                                 
                               </ul>
								<div class="tab-content">

                                 <div id="smsj">
                                     <iframe src="viewSupplierBaseInfoDetailChange_supplierBaseInfo.action?supplierInfo.supplierId=${supplierInfo.supplierId}" id="iframe" frameborder=0 scrolling="yes" width="100%" height="400px"></iframe>
                                 </div> 
                             </div>
                         </div>
		                 <div class="buttonDiv">
				    	    	<button class="btn btn-success" onclick="submitSupplierAudit('0');" type="button"><i class="icon-white icon-ok-sign"></i>通过</button>
								<button class="btn btn-danger" onclick="submitSupplierAudit('2');" type="button"><i class="icon-white icon-remove-sign"></i>退  回</button>
						</div>
                     </div>
                 </div>
                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>
     <!-- /.page-content -->
     
     </body>
</html>