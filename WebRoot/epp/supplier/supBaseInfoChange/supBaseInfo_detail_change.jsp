<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>供应商明细信息</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
</head>
 
<body>
<form id="" name="" method="post" action="saveSupplierBaseInfo_supplierBaseInfo.action">

<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
    		<tr>
				<td width="20%" class="Content_tab_style1">公司名称：</td>
				<td width="30%" >
				<c:if test="${not empty supplierInfoChange.supplierName}">
					<font color="red">${supplierInfoChange.supplierName }</font>(${supplierInfo.supplierName })
				</c:if>
				<c:if test="${empty supplierInfoChange.supplierName}">
				    ${supplierInfo.supplierName }
				</c:if>
				</td>
				<td rowspan="4" colspan="2">
			       <div style="width:220px; height:80px; overflow:hidden;">  
					<div id="imgDiv">
					   <c:choose>
					      <c:when test="${empty supplierInfo.logo}">
                            <img src="<%=path %>/images/no.png" width="220" height="80">
					      </c:when>
					      <c:otherwise>					      
                            <img src="/fileWeb/${supplierInfo.logo}" width="220" height="80">
					      </c:otherwise>
					   </c:choose> 
					</div> 
				  </div> 
			    </td>
			 </tr>
			 <tr>
			 <td width="15%" class="Content_tab_style1">
				<c:choose>
					<c:when test="${supplierInfo.registrationType=='01'}">
					    统一社会信用代码证号：  
					</c:when>
					<c:otherwise>
					     组织机构代码证编号：
					</c:otherwise>
				</c:choose>
				</td>
				<td  >
					${supplierInfo.orgCode}
				</td>
			</tr>
			<tr>
			
				<td width="15%" class="Content_tab_style1">用户名：</td>
				<td  >
					${supplierInfo.supplierLoginName}
					
				</td>
			</tr>
			<tr>
			    <td  class="Content_tab_style1">公司简称：</td>
				<td  >
				<c:if test="${not empty supplierInfoChange.supplierNameSimple}">
					<font color="red">${supplierInfoChange.supplierNameSimple }</font>(${supplierInfo.supplierNameSimple})
				</c:if>
				<c:if test="${empty supplierInfoChange.supplierNameSimple}">
				    ${supplierInfo.supplierNameSimple}
				</c:if>
				</td>
			</tr>
			<tr>
				<td width="20%" class="Content_tab_style1">成立日期：</td>
				<td width="30%" >
				<c:if test="${not empty supplierInfoChange.createDate}">
					<font color="red"><fmt:formatDate value="${supplierInfoChange.createDate}" pattern="yyyy-MM-dd"/></font>(<fmt:formatDate value="${supplierInfo.createDate}" pattern="yyyy-MM-dd"/>)
				</c:if>
				<c:if test="${empty supplierInfoChange.createDate}">
				    <fmt:formatDate value="${supplierInfo.createDate}" pattern="yyyy-MM-dd"/>
				</c:if>
				</td>				
				<td width="20%" class="Content_tab_style1">企业性质：</td>
				<td width="30%" >
					${supplierInfo.supplierTypeCn}
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">地区：</td>
				<td  >
					${supplierInfo.province} ${supplierInfo.city }
				</td>	
				<td  class="Content_tab_style1">所属行业：</td>
				<td  >
					${supplierInfo.industryOwnedCn}
					
				</td>
            </tr><tr>
				<td  class="Content_tab_style1">经营模式：</td>
				<td  >
					${supplierInfo.managementModelCn}
				</td>	
				<td  class="Content_tab_style1">公司网站：</td>
				<td  >
					${supplierInfo.companyWebsite}
					
				</td>
            </tr>
			<tr>
				<td  class="Content_tab_style1">企业法人：</td>
				<td  >
				<c:if test="${not empty supplierInfoChange.legalPerson}">
					<font color="red">${supplierInfoChange.legalPerson }</font>(${supplierInfo.legalPerson})
				</c:if>
				<c:if test="${empty supplierInfoChange.legalPerson}">
				   ${supplierInfo.legalPerson}
				</c:if>
				</td>
				<td  class="Content_tab_style1">注册资金：</td>
				<td  >
				<c:if test="${not empty supplierInfoChange.registerFunds}">
					<font color="red">${supplierInfoChange.registerFunds }</font>(${supplierInfo.registerFunds})
				</c:if>
				<c:if test="${empty supplierInfoChange.registerFunds}">
				  ${supplierInfo.registerFunds}
				</c:if>	
				</td>
            </tr>
			<tr>
				<td  class="Content_tab_style1">地址：</td>
				<td  >
				<c:if test="${not empty supplierInfoChange.supplierAddress}">
					<font color="red">${supplierInfoChange.supplierAddress }</font>(${supplierInfo.supplierAddress})
					&nbsp;
				</c:if>
				<c:if test="${empty supplierInfoChange.supplierAddress}">
				    ${supplierInfo.supplierAddress} 
				</c:if>
				</td>
				<td  class="Content_tab_style1">公司电话：</td>
				<td  >
				<c:if test="${not empty supplierInfoChange.supplierPhone}">
					<font color="red">${supplierInfoChange.supplierPhone }</font>(${supplierInfo.supplierPhone})
				</c:if>
				<c:if test="${empty supplierInfoChange.supplierPhone}">
				   ${supplierInfo.supplierPhone}
				</c:if>
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">公司传真：</td>
				<td  >
				<c:if test="${not empty supplierInfoChange.supplierFax}">
					<font color="red">${supplierInfoChange.supplierFax }</font>(${supplierInfo.supplierFax})
				</c:if>
				<c:if test="${not empty supplierInfoChange.supplierFax}">
				    ${supplierInfo.supplierFax}
				</c:if>
				</td>
				<td  class="Content_tab_style1">联系人：</td>
				<td  >
					${supplierInfo.contactPerson}
				</td>
			</tr>
			
			<tr>
			    <td  class="Content_tab_style1">联系人手机号：</td>
				<td  >
					${supplierInfo.mobilePhone}
				</td>
				<td  class="Content_tab_style1">联系人E-mail：</td>
				<td  >
					${supplierInfo.contactEmail}
				</td>				
			</tr>
			<tr>
				<td  class="Content_tab_style1">户名：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bankAccountName }
				</td>
				<td  class="Content_tab_style1">银行账号：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bankAccount }
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">开户银行：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bank }
				</td>
				<td  class="Content_tab_style1">税号：</td>
				<td  class="Content_tab_style2">
					${supplierInfo.bankAccount }
				</td>
			</tr>
			
			<tr>
				<td  class="Content_tab_style1">主营产品：</td>
				<td width="85%" colspan="3">
					${supplierInfo.prodKindNames}
				</td>
				
			</tr>
			<tr>
				<td  class="Content_tab_style1">经营范围：</td>
				<td colspan="3">
					${supplierInfo.scopeBusiness}
				</td>
				
			</tr>
			<tr>
				<td  class="Content_tab_style1">企业简介：</td>
				<td  colspan="3">
				   ${supplierInfo.introduce}
				</td>
			</tr>
        </table>
        
	</div>
</div>
</form>
</body></html>