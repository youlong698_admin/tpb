<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>历史价格曲线分析</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>

<script type="text/javascript">
   var api = frameElement.api, W = api.opener;	
</script>
</head>
<body >
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
        <c:choose>
           <c:when test="${xAxisData==''}">
              <div class="alert alert-block alert-info">
				价格库中没有相关价格数据!
			  </div>
           </c:when>
           <c:otherwise>
              <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	    <div id="main" style="width:800px;height:380px"></div>
	    <!-- ECharts单文件引入 -->
	    <script src="<%= path %>/common/echarts/echarts.min.js"></script>
	    <script type="text/javascript">
	        // 基于准备好的dom，初始化echarts图表
	        var myChart = echarts.init(document.getElementById('main')); 
	        
	        var option = {
			    title: {
			        text: '${materialName}价格曲线分析'
			    },
			    tooltip: {
			        trigger: 'axis'
			    },
			    legend: {
			        data:['价格']
			    },
			    toolbox: {
			        show: true,
			        feature: {
			            dataZoom: {
			                yAxisIndex: 'none'
			            },
			            dataView: {readOnly: false},
			            magicType: {type: ['line', 'bar']},
			            restore: {},
			            saveAsImage: {}
			        }
			    },
			    xAxis:  {
			        type: 'category',
			        boundaryGap: false,
			        data: [${xAxisData}]
			    },
			    yAxis: {
			        type: 'value',
			        axisLabel: {
			            formatter: '{value}元'
			        }
			    },
			    series: [
			        {
			            name:'价格',
			            type:'line',
			            data:[${yAxisData}],
			            markPoint: {
			                data: [
			                    {type: 'max', name: '最大值'},
			                    {type: 'min', name: '最小值'}
			                ]
			            },
			            markLine: {
			                data: [
			                    {type: 'average', name: '平均值'}
			                ]
			            }
			        }
			    ]
			};
	
	        // 为echarts对象加载数据 
	        myChart.setOption(option); 
	    </script>
           </c:otherwise>
        </c:choose>
    	    
</div>
</div>
</body>
</html> 	