<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>评委评标</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/evaluate/evaluateBids.js" type="text/javascript"></script>
<script type="text/javascript">
var api = frameElement.api, W = api.opener;
</script>
</head>
<body>
<form id="" name="updateBidJudge" method="post" action="">
<input type="hidden" name="tbjtId" id="tbjtId" value="${tenderBidJudgeType.tbjtId }"/>
   
	<table width="100%" id="evatable" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys1">		
              <tr class="Content_tab_style_04" valign="top">
                <th width="5%" nowrap >序号</th>
                <th width="15%" nowrap >评分项目</th>
                <th width="5%" nowrap >标准分值</th>
                <c:forEach items="${receivedBulletinList}" var="receivedBulletin" varStatus="isl">
                <th width="10%">
	                 <a href="##" onclick="createdetailwindow('供应商基本信息','viewSupplierSelectInfo_supplierBaseInfo.action?supplierInfo.supplierId=${receivedBulletin.supplierId}',1)">${receivedBulletin.supplierName}</a>&nbsp;<a href="#" title='复制第一个厂家的评分' onclick="copyValueToOther(this)"><c:if test="${isl.count!=1}">（复制）</c:if></a></th>
                </c:forEach>
              </tr>
              <c:forEach items="${tenderBidItemList}" var="tenderBidItem" varStatus="bts">
				<tr <c:choose><c:when test="${bts.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
					<td>
					 ${bts.count}
					</td>
					<td><a href="#" onclick="find(${tenderBidItem.itemId});">${tenderBidItem.itemName}</a> </td>
					<td>${tenderBidItem.points}</td>
					<c:forEach items="${receivedBulletinList}" var="receivedBulletin"  varStatus="isl">
	                	 <c:forEach items="${jtcList}" var="jsc">
						    <c:if test="${receivedBulletin.supplierId==jsc.supplierId&&tenderBidItem.itemId==jsc.itemId}">
		                	<td  align="right">
		                	${jsc.judgePoints }
		                	</td>
		                	</c:if>
	                	</c:forEach>
                	</c:forEach>
				</tr>
			  </c:forEach>
			<tr align="center" class='biaoge_01_b'>
                <td></td>
                <td>合计</td>
                <td >100</td>
				<c:forEach items="${receivedBulletinList}" var="receivedBulletin"  varStatus="isl">
                	<td width="10%"  align="right">${receivedBulletin.supPoints }</td>
              </c:forEach>
            </tr>
          <tr class='biaoge_01_b'>
                <td width="50%" colspan="${fn:length(receivedBulletinList)+3 }" align="left">
                		评委：  ${expertName}
                		评标日期：<fmt:formatDate value="${tenderBidJudgeType.tableSubmitTime}" pattern="yyyy-MM-dd"/>
                </td>
          </tr>
        </table>	
</form>
</body>
</html>