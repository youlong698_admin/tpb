<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>评委评标</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/evaluate/evaluateBids.js" type="text/javascript"></script>
<script language="javaScript">
var api = frameElement.api, W = api.opener;
//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		    showMsg('success','${message}');		       
		       W.window.location.reload();
		       api.close();
		  	}
		 </c:if>
</script>
</head>
<body >
<form id="" method="post" action="">
<input type="hidden" name="tbjtId" id="tbjtId" value="${tenderBidJudgeType.tbjtId }"/>
<input type="hidden" name="rcId" id="rcId" value="${rcId }"/>
  
	<table width="100%" id="evatable" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys1">		
              <tr class="Content_tab_style_04" valign="top">
                <th width="3%" nowrap >序号</th>
                <th width="18%" nowrap >评分项目</th>
                <th width="5%" nowrap >标准分值</th>
                <c:forEach items="${receivedBulletinList}" var="receivedBulletin" varStatus="isl">
                <th width="10%">
	                <input type="hidden" name="receivedBulletinList[${isl.index }].supplierId" value="${receivedBulletin.supplierId}" />
		            <input type="hidden" name="receivedBulletinList[${isl.index }].supplierName" value="${receivedBulletin.supplierName}" />
	                 <a href="##" onclick="createdetailwindow('供应商基本信息','viewSupplierSelectInfo_supplierBaseInfo.action?supplierInfo.supplierId=${receivedBulletin.supplierId}',1)">${receivedBulletin.supplierName}</a>&nbsp;<a href="#" title='复制第一个厂家的评分' onclick="copyValueToOther(this)"><c:if test="${isl.count!=1}">（复制）</c:if></a></th>
                </c:forEach>
              </tr>
              <c:forEach items="${tenderBidItemList}" var="tenderBidItem" varStatus="bts">
				<tr <c:choose><c:when test="${bts.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
					<td>
					 ${bts.count}
					 <c:choose>
					  <c:when test="${tenderBidItem.itemType=='0'}">
						<input type="hidden" name="judgeTechScoreList[${bts.index}].itemId" id="itemId" value="${tenderBidItem.itemId}" />
					  </c:when>
					  <c:otherwise>
						<input type="hidden"name="judgeBusScoreList[${bts.index}].itemId" id="itemId" value="${tenderBidItem.itemId}" />
					 </c:otherwise>
					 </c:choose>
					</td>
					<td><a href="#" onclick="find(${tenderBidItem.itemId});">${tenderBidItem.itemName}</a> </td>
					<td>${tenderBidItem.points}</td>
					<c:forEach items="${receivedBulletinList}" var="receivedBulletin"  varStatus="isl">
	                	<td>
	                	<input type="text" id="jd${bts.index}${isl.index}" onkeyup="isOverBaseValue(this);"  name="receivedBulletinList[${isl.index}].judgePoints" value="" />
	                	</td>
                	</c:forEach>
				</tr>
			  </c:forEach>
			<tr align="center" class='biaoge_01_b'>
                <td></td>
                <td>合计</td>
                <td >100</td>
				<c:forEach items="${receivedBulletinList}" var="receivedBulletin"  varStatus="isl">
                	<td width="10%"><input type="text" id="sup${isl.index}" name="receivedBulletinList[${isl.index}].supPoints" value=""/></td>
              </c:forEach>
            </tr>
          <tr class='biaoge_01_b'>
                <td width="50%" colspan="${fn:length(receivedBulletinList)+3 }" align="left">
                		评委：  ${expertName}
                		评标日期：<fmt:formatDate value="${date}" pattern="yyyy-MM-dd"/>
                </td>
          </tr>
          <tr>
          		<td   id="operateid" align="center" colspan="${fn:length(receivedBulletinList)+3 }" >
					
					<button class="btn btn-success" id="btn-save" type="button"  onclick="bidJudgeHandle('add',0);"><i class="icon-white icon-ok-sign"></i>保存</button>
					<button class="btn btn-danger" id="btn-submit" type="button"  onclick="bidJudgeHandle('add',1);"><i class="icon-white icon-leaf"></i>提交</button>
					<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
			   </td>
        	</tr>
        </table>	
</form>
</body>
</html>