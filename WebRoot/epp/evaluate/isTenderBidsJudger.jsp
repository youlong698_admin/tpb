<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<title>评委评标</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script type="text/javascript">
  function doQiandao(tbjId){
       var result=ajaxGeneral("signTenderBidJudge_evaluate.action","tbjId="+tbjId,"text");
		showMsg('success',''+result+'',function(){
				  window.location.href=window.location.href;						
		});
  }
  function doEvaluate(url){	
	createdetailwindow("专家评标",url,1);	    
  }
</script>
<body >
               
				<div style="margin:10px 10px 20px 10px">
					<div class="alert alert-block alert-success">
									您今天将要参加的评标项目如下:
								</div>
				<table style="width: 100%" class="table_ys1">		
		              <tr class='Content_tab_style_04'>
						<th width="5%"nowrap>序号</th>
						<th width="15%" nowrap>项目编号</th>
						<th width="25%" nowrap>项目名称</th>
						<th width="10%" nowrap>开标时间</th>
						<th width="10%" nowrap>评委角色</th>
						<th width="10%" nowrap>签到</th>
						<th width="10%" nowrap>签到时间</th>
						<th width="15%" nowrap>操作</th>
					  </tr>
					<c:forEach items="${bidJudgeList}" varStatus="sta" var="tenderBidJudge">
					   <tr align="center" id="tr${tenderBidJudge.tbjId }" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
							<td>
								${sta.index+1 }
							</td>
							<td>
							   ${tenderBidJudge.bidCode}
							</td>
							<td>
								${tenderBidJudge.buyRemark}
							</td>
							<td>
								<fmt:formatDate value="${tenderBidJudge.openDate}" pattern="yyyy-MM-dd HH:mm" />
							</td>
							<td>
								<c:forEach items="${judgeType}" var="map">
										<c:if test="${tenderBidJudge.expertRole == map.key}">${map.value }</c:if>
								</c:forEach>
							</td>
							<td>
							    <c:if test="${empty tenderBidJudge.expertSign}">
								<img src="<%=basePath %>/images/qiandao.png" style="cursor: pointer;" title="点击签到" onclick="doQiandao(${tenderBidJudge.tbjId})"/>
								</c:if>								
							    <c:if test="${not empty tenderBidJudge.expertSign}">
							              已签到
							    </c:if>
							</td>
							<td>
								<fmt:formatDate value="${tenderBidJudge.expertSignDate}" pattern="yyyy-MM-dd HH:mm" />
							</td>
							<td>
							   <!-- 0 技术  1 商务 -->
							   <c:if test="${not empty tenderBidJudge.expertSign&&tenderBidJudge.openStatus=='02'}">
							     <c:forEach items="${tenderBidJudge.bidJudgeTypes}" var="judgeType">
							        <c:if test="${judgeType.tableName=='1'}">
							            <c:choose>
							                <c:when test="${judgeType.tableStatu=='0'}">
							                 <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('updateTechOrBusJudgerBidsInit_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>商务评标</a><span class="green">（已保存）</span></div>
							                </c:when>
							                <c:when test="${judgeType.tableStatu=='1'}">
							                 <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('viewTechOrBusJudgerBidsDetail_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>商务评标</a><span class="red">（已提交）</span></div>
							                </c:when>
							                <c:otherwise>
							                 <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('saveTechOrBusJudgerBidsInit_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>商务评标</a>（未评标）</div>
							                </c:otherwise>
							           </c:choose>
							          </c:if>
							            <c:if test="${judgeType.tableName=='0'}">
							            <c:choose>
							                <c:when test="${judgeType.tableStatu=='0'}">
							                 <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('updateTechOrBusJudgerBidsInit_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>技术评标</a> <span class="green">（已保存）</span></div>
							                </c:when>
							                <c:when test="${judgeType.tableStatu=='1'}">
							                 <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('viewTechOrBusJudgerBidsDetail_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>技术评标</a><span class="red">（已提交）</span></div>
							                </c:when>
							                <c:otherwise>
							                  <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('saveTechOrBusJudgerBidsInit_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>技术评标</a>（未评标）</div>
							                </c:otherwise>
							           </c:choose>
							           </c:if>
							         </c:forEach>
							   </c:if>
							</td>
						</tr>
					</c:forEach>	
				</table>
			</div>
			
</body>
</html>

