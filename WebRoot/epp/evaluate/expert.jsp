<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Contetn-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path %>/common/script/context.js" type="text/javascript"></script>
		<title>查看专家信息</title>
		<script type="text/javascript">
		  
		</script>
	</head>

	<body>
		<form method="post" action="">
			<div class="Conter_Container">
				<div class="Conter_main_conter">
					<table class="table_ys1">
						<tr>
							<td width="15%" class="Content_tab_style1">
								姓名：
							</td>
							<td width="35%">
								${expert.expertName}
							</td>

							<td width="15%" class="Content_tab_style1">
								登录名：
							</td>
							<td width="35%">
								${expert.expertLoginName}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								性别：
							</td>
							<td width="35%">
								<c:if test="${expert.expertSex=='0'}">男</c:if>
								<c:if test="${expert.expertSex=='1'}">女</c:if>
							</td>
							<td width="15%" class="Content_tab_style1">
								身份证号：
							</td>
							<td width="35%">
								${expert.identify}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								系统内外标志：
							</td>
							<td width="35%">
								<c:if test="${expert.inOut=='0'}">系统内</c:if>
								<c:if test="${expert.inOut=='1'}">系统外</c:if>
							</td>
							<td width="15%" class="Content_tab_style1">
								工作部门：
							</td>
							<td width="35%">
								${expert.companyName}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								工作单位：
							</td>
							<td width="35%">
								${expert.department}
							</td>
							<td width="15%" class="Content_tab_style1">
								学历：
							</td>
							<td width="35%">
							${expert.degreeCn}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								所学专业：
							</td>
							<td width="35%">
								${expert.profession}
							</td>
							<td width="15%" class="Content_tab_style1">
								从事专业时间（年）：
							</td>
							<td width="35%">
								${expert.professionTime}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								评标专业：
							</td>
							<td width="35%">
								${expert.expertMajor}
							</td>
							<td width="15%" class="Content_tab_style1">
								专家类型：
							</td>
							<td width="35%">
								<c:if test="${expert.expertType=='0'}">正式</c:if>
								<c:if test="${expert.expertType=='1'}">临时</c:if>
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								职称：
							</td>
							<td width="35%">
								${expert.titleTechCn}
							</td>
							<td width="15%" class="Content_tab_style1">
								职务：
							</td>
							<td width="35%">
								${expert.titleHeadship}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								固定电话：
							</td>
							<td width="35%">
								${expert.phoneNumber}
							</td>
							<td width="15%" class="Content_tab_style1">
								移动电话：
							</td>
							<td width="35%">
								${expert.mobilNumber}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								传真：
							</td>
							<td width="35%">
								${expert.faxNumber}
							</td>
							<td width="15%" class="Content_tab_style1">
								通信地址：
							</td>
							<td width="35%">
								${expert.address}
							</td>
						</tr>
						<tr>
							<td width="15%" class="Content_tab_style1">
								邮箱地址：
							</td>
							<td width="35%">
								${expert.email}
							</td>
							<td width="15%" class="Content_tab_style1">
								当前状态：
							</td>
							<td width="35%">
								${expert.stutusCn}
							</td>
						</tr>
					</table>	
				</div>

			</div>

		</form>

	</body>
</html>
