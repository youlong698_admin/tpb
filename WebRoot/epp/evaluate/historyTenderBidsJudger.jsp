<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<title>历史项目评标</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script type="text/javascript">
 
  function doEvaluate(url){	
	createdetailwindow("专家评标",url,1);	    
  }
  function doQuery(){
    document.form1.action="viewHistoryTenderBidsJudger_evaluate.action";
    document.form1.submit();
  }
  //重置
  function doReset(){        
	document.form1.bidCode.value	=	"";	
	document.form1.buyRemark.value		=	"";
  }
</script>
<body >
               <form action="" name="form1" method="post">
				<div style="margin:10px 10px 20px 10px">
					<div class="alert ">
							项目编号：
						<input class="input-medium" type="text" name="bidCode" placeholder="项目编号" id="bidCode" value="" size="15" />
						项目名称：
						<input class="input-medium" type="text" name="buyRemark" placeholder="项目名称" id="buyRemark" size="15" value=""  />
					 	
						
						<button type="button" class="btn btn-info" id="btn-advanced-search" onclick="doQuery();" ><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					</div>
				<table style="width: 100%" class="table_ys1">		
		              <tr class='Content_tab_style_04'>
						<th width="5%"nowrap>序号</th>
						<th width="15%" nowrap>项目编号</th>
						<th width="25%" nowrap>项目名称</th>
						<th width="10%" nowrap>开标时间</th>
						<th width="10%" nowrap>评委角色</th>
						<th width="10%" nowrap>签到</th>
						<th width="10%" nowrap>签到时间</th>
						<th width="15%" nowrap>操作</th>
					  </tr>
					<c:forEach items="${bidJudgeList}" varStatus="sta" var="tenderBidJudge">
					   <tr align="center" id="tr${tenderBidJudge.tbjId }" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
							<td>
								${sta.index+1 }
							</td>
							<td>
							   ${tenderBidJudge.bidCode}
							</td>
							<td>
								${tenderBidJudge.buyRemark}
							</td>
							<td>
								<fmt:formatDate value="${tenderBidJudge.openDate}" pattern="yyyy-MM-dd HH:mm" />
							</td>
							<td>
								<c:forEach items="${judgeType}" var="map">
										<c:if test="${tenderBidJudge.expertRole == map.key}">${map.value }</c:if>
								</c:forEach>
							</td>
							<td>
							    <c:if test="${empty tenderBidJudge.expertSign}">
								 未签到
								</c:if>								
							    <c:if test="${not empty tenderBidJudge.expertSign}">
							             已签到
							    </c:if>
							</td>
							<td>
								<fmt:formatDate value="${tenderBidJudge.expertSignDate}" pattern="yyyy-MM-dd HH:mm" />
							</td>
							<td>
							   <c:if test="${not empty tenderBidJudge.expertSign}">
							     <c:forEach items="${tenderBidJudge.bidJudgeTypes}" var="judgeType">
							          <c:if test="${judgeType.tableName=='1'}">
							            <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('viewTechOrBusJudgerBidsDetail_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>商务评标</a></div>
							            
							          </c:if>
							          <c:if test="${judgeType.tableName=='0'}">
							             <div><a href="javascript:;" style="cursor: pointer;" onclick="doEvaluate('viewTechOrBusJudgerBidsDetail_evaluate.action?tbjtId=${judgeType.tbjtId }')"><img src="<%=basePath %>/images/leftico03.png"/>技术评标</a></div>
							           </c:if>
							         </c:forEach>
							   </c:if>
							   <c:if test="${empty tenderBidJudge.expertSign}">
							           无评标数据
								</c:if>
							</td>
						</tr>
					</c:forEach>	
				</table>
			</div>
			</form>
</body>
</html>

