<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>增加专家信息</title>
<meta http-equiv="Contetn-Type" content="text/html; charset=UTF-8" />
<script src="<%=path %>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

   	<script type="text/javascript">
   	var api = frameElement.api, W = api.opener;
   	var comId="${comId}";
   		 $(function (){
			 //返回信息
			   <c:if test="${message!=null}">
			   window.onload=function(){ 
			  	  showMsg('success','${message}');
		            W.doQuery();
		            api.close();
		  	      }
			    </c:if>
			    
			    $("#btn-danger").click(function(){
			    	api.close();
			    })
		});
   		
　		function checkPageValue( obj){
			DwrService.ifExitCodeInTable("Expert", "expertLoginName,isUsable", obj.value+",0" , set_view);	
		}
		function set_view(count){
			if( count >0){
				document.getElementById("infoDsp").innerHTML="登录名已存在,请重新输入!";
				document.getElementById("ename").focus();
				$('#submitButton').hide();
			}else{
				document.getElementById("infoDsp").innerHTML="";
				$('#submitButton').show();
			}
		}
		
　　	</script>
   	
   	<script type="text/javascript">
   		
    //选择评标专业
	function selectMajor(){		
		var ul =document.getElementById("expertMajorId").value;		
	     createdetailwindow_choose("选择评标专业","viewMajorWindow_expert.action?ul="+ul,4);
	}
	//选择评标专业
	function valueMajor(){
		var winObj=document.getElementById('returnVals').value;
		
		var expertMajorIds = "";
		var expertMajors = "";
		if(winObj!=null&&winObj!=''){
			var proKindArr = winObj.split(",");
			for(var i=0;i<(proKindArr.length-1);i++){
				var proKinds = proKindArr[i].split(":");
				expertMajorIds += proKinds[0]+",";
				expertMajors += proKinds[1]+",";
			}
			var expertMajorIds = expertMajorIds.substring(0, expertMajorIds.length-1);
			var expertMajors = expertMajors.substring(0, expertMajors.length-1);
			
			$("#expertMajorId").val(expertMajorIds);
			$("#expertMajor").val(expertMajors);
		}
	}
    	
			
   	</script>
    
	</head>
<body>
	
<form class="defaultForm" method="post" id="arrange" action="saveExpert_expert.action" enctype="">
<input type="hidden" name="expertMajorId" id="expertMajorId"/>

<input type="hidden" name="returnVals" id="returnVals" value=""/>
<div class="Conter_Container">
<div class="Conter_main_conter">
<table class="table_ys1" style="margin-top:20px;">
	<tr>
		<td width="15%" class="Content_tab_style1">姓名：</td>
		<td width="35%" >
        	<input type="text" name="expert.expertName" value="" datatype="*" nullmsg="请输入您要添加的姓名！" />&nbsp;<font color="#ff0000">*</font>
			<div class="info"><span class="Validform_checktip">请输入您要添加的姓名！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		</td>
		<td width="15%" class="Content_tab_style1" >登录名</td>
		<td width="35%" >
			<input type="text" id="ename" name="expert.expertLoginName" value=""  datatype="*" nullmsg="请输入登录名！"  onblur="checkPageValue(this);" />&nbsp;<font color="#ff0000">*</font><div id="infoDsp"></div>
			<div class="info"><span class="Validform_checktip">请输入登录名！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		</td>
	</tr>
    <tr>
		<td width="15%" class="Content_tab_style1">性别：</td>
		<td width="35%" >
			<input type="radio" name="expert.expertSex" value="0" checked="checked"/>男
			<input type="radio" name="expert.expertSex" value="1"/>女
		</td>
		<td width="15%" class="Content_tab_style1">身份证号：</td>
		<td width="35%" >
        	<input type="text" name="expert.identify" value="" />
		</td>
	</tr>
    <tr>
		
		<td width="15%" class="Content_tab_style1">系统内外标志：</td>
		<td width="35%" >
			<input type="radio" name="expert.inOut" value="0" checked="checked"/>系统内
			<input type="radio" name="expert.inOut" value="1"/>系统外
		</td>
		<td width="15%" class="Content_tab_style1">工作部门：</td>
		<td width="35%" >
			<input type="text" name="expert.companyName" value="" />
		</td>
	</tr>
    <tr>
		<td width="15%" class="Content_tab_style1">工作单位：</td>
		<td width="35%" >
			<input type="text" name="expert.department" value="" />
		</td>
		<td width="15%" class="Content_tab_style1">学历：</td>
		<td width="35%" >
		    <select name="expert.degree">
		       <c:forEach items="${degreeMap}"  var="map">
		          <option value="${map.key }">${map.value }</option>
		       </c:forEach>
		    </select>
		</td>
	</tr>
    <tr>
		
		<td width="15%" class="Content_tab_style1">所学专业：</td>
		<td width="35%" >
			<input type="text" name="expert.profession" value="" />
		</td>
		<td width="15%" class="Content_tab_style1">从事专业时间（年）：</td>
		<td width="35%" >
        	<input type="text" name="expert.professionTime"  value="" />
		</td>
	</tr>
    <tr>
		
		<td width="15%" class="Content_tab_style1">评标专业：</td>
		<td width="35%" >
			<input type="text" readonly="readonly" name="expert.expertMajor" datatype="*" nullmsg="评标专业不能为空！" id="expertMajor" />
			<img src="<%=basePath %>/images/select.gif" title="选择评标专业" onclick="selectMajor();"/>
			<div class="info"><span class="Validform_checktip">评标专业不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		</td>
		<td width="15%" class="Content_tab_style1">专家类型：</td>
		<td width="35%" >
			<input type="radio" name="expert.expertType" value="0" checked="checked"/>正式
			<input type="radio" name="expert.expertType" value="1"/>临时
		</td>
	</tr>
    <tr>
		
		<td width="15%" class="Content_tab_style1">职称：</td>
		<td width="35%" >
		    <select name="expert.titleTech">
		       <c:forEach items="${titleTechMap}"  var="map">
		          <option value="${map.key }">${map.value }</option>
		       </c:forEach>
		    </select>
		</td>
		<td width="15%" class="Content_tab_style1">职务：</td>
		<td width="35%" >
        	<input type="text" name="expert.titleHeadship" value="" />
		</td>
	</tr>
    <tr>
		
		<td width="15%" class="Content_tab_style1">固定电话：</td>
		<td width="35%" >
			<input type="text" name="expert.phoneNumber" value="" />
		</td>
		<td width="15%" class="Content_tab_style1">移动电话：</td>
		<td width="35%" >
        	<input type="text" name="expert.mobilNumber" datatype="/^\s*$/ |m" errormsg="请按照手机号码格式填写！" value="" />
			<div class="info"><span class="Validform_checktip">请按照手机号码格式填写！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		</td>
	</tr>
    <tr>
		
		<td width="15%" class="Content_tab_style1">传真：</td>
		<td width="35%" >
			<input type="text" name="expert.faxNumber" value="" />
		</td>
		<td width="15%" class="Content_tab_style1">通信地址：</td>
		<td width="35%" >
        	<input type="text" name="expert.address" value="" />
		</td>
	</tr>
     <tr>
		
		<td width="15%" class="Content_tab_style1">邮箱地址：</td>
		<td width="35%" >
			<input type="text" name="expert.email" datatype="/^\s*$/ |e" errormsg="请按照邮箱格式填写！" value="" />
			<div class="info"><span class="Validform_checktip">请按照邮箱格式填写！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
		</td>
		<td width="15%" class="Content_tab_style1">当前状态：</td>
		<td width="35%" >
			<input type="radio" name="expert.status" value="0" checked="checked"/>有效
			<input type="radio" name="expert.status" value="1"/>无效
		</td>
	</tr>
     
</table>
			
	<div class="buttonDiv">
		<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
		
	</div>

</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html>
