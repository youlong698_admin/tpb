<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>专家基本信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			debugger;
			$.ajax({
		            type: "POST",
		            url: "findExpert_expert.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX,
            {	
            	className : "ellipsis",
            	data: "expertName",
            	render: function(data,type, row, meta) {
                   return "<a onClick=\"createdetailwindow('查看专家信息','viewExpertDetail_expert.action?expert.expertId=" + row.expertId + "',1)\" title="+data+">"+data+"</a>"; 
                },
                orderable : false		
            },
			{
				data : "expertSex",
				render: CONSTANT.DATA_TABLES.RENDER.SEX,
				width : "10%",
				orderable : false
			},
			{
				data : "inOut",
				render: CONSTANT.DATA_TABLES.RENDER.INOUT,
				width : "10%"
			},
			{
				data : "expertType",
				render: CONSTANT.DATA_TABLES.RENDER.EXPERTTYPE,
				width : "10%"
			},
			{
				className : "ellipsis",
				data : "expertMajor",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "20%"
			},
			{
				data : "stutusCn",
				width : "10%"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	//给当前行某列加样式
        	$('td', row).eq(6).addClass(data.stutusCn=="有效"?"text-success":"text-error");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.updateItem(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	});
	
	//导出excel
	$("#btn-daochu").click(function(){
		GeneralManage.daochuItems();
	});
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//默认进入的排序
		 param.orderColumn="ex.expertId";
		//组装排序参数
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 3:
				param.orderColumn = "ex.inOut";
				break;
			case 4:
				param.orderColumn = "ex.expertType";
				break;
			case 5:
				param.orderColumn = "ex.expertMajor";
				break;
			case 6:
				param.orderColumn = "ex.stutusCn";
				break;
			default:
				param.orderColumn = "ex.inOut";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		param.expertName = $("#expertName").val();
		param.status = $("#status").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	addItemInit:function(){
		createdetailwindow('新增专家信息','saveExpertInit_expert.action',1);
	},
	updateItem:function(selectedItems){
		var v = selectedItems;
		if(v.length==0){
				showMsg("alert","温馨提示：请选择信息！");
			}else if(v.length > 1){
				showMsg("alert","温馨提示：只能选择一条信息修改！");
			}else{
				var ids = "";
				for(var i=0;i<v.length;i++){
					if(i == (v.length-1)){
						ids += v[i].expertId;
					}else{
						ids += v[i].expertId + ",";
					}
				}if(ids!=null){
					createdetailwindow('修改专家信息','updateExpertInit_expert.action?expert.expertId='+ids,1);
				}
			}
	},
	deleteItem : function(selectedItems) {
		var v = selectedItems;
		if(v.length==0){
				showMsg("alert","温馨提示：请选择信息！");
		}else if(v!=null){
			var ids = "";
			for(var i=0;i<v.length;i++){
				if(i == (v.length-1)){
					ids += v[i].expertId;
				}else{
					ids += v[i].expertId + ",";
				}
			}
			
			 var action = "deleteExpert_expert.actionn";
			$.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
			   result = ajaxGeneral("deleteExpert_expert.action","ids="+ids);
				   showMsg('success',''+result+'',function(){
				   		doQuery();							
	   					});
			   	 	});
						 
		}
	},
	//导出excel
	daochuItems:function(){
		  var expertName=$("#expertName").val();
		  var status=$("#status").val();		  
		  window.location.href = "exportExpertExcel_expert.action?expert.expertName="+expertName+"&expert.status="+status;
	},
};
	function doQuery(){
     _table.draw();
    }
	//重置
	function doReset(){        
		document.forms[0].elements["expertName"].value	=	"";	
		document.forms[0].elements["status"].value		=	"";
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i> 新增</button>
							<button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
							<button type="button" class="btn btn-info" id="btn-daochu"><i class="icon-white icon-share"></i> 导出Excel</button>
						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
						
			 			姓名：
						<input class="input-medium" type="text" placeholder="姓名"  id="expertName" name="expert.expertName" value=""/>
						
						专家状态：
						<select id="status" class="input-small">
							<option value="">--请选择--</option>
							<option value="0">有效</option>
							<option value="1">无效</option>
						</select>
				
						<button type="button" class="btn btn-info" id="btn-advanced-search"><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
				
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>姓名</th>
									<th>性别</th>
									<th>系统内/外</th>
									<th>专家类型</th>
									<th>评标专业</th>
									<th>状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>