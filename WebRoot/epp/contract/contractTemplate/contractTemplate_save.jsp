<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>新增合同模板</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		    showMsg('success','${message}');
		    W.doQuery();
		    api.close();
		  	}
		</c:if>
	    
	});
	
	</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="saveContractTemplate_contractTemplate.action"  enctype="multipart/form-data">
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">新增合同模板</td>
		    </tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">模板名称：</td>
			    <td width="70%" class="Content_tab_style2">
			      <input  datatype="*" nullmsg="模板名称不能为空！" name="contractTemplate.templateMame" type="text" id="templateMame" value=""><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">模板名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			     </td>			
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">模板路径：</td>
			    <td width="70%" class="Content_tab_style2"><input type="file" name="file" value=""/><font color="#ff0000">*</font></td>
			</tr>
			<tr>
			    <td height="24" class="Content_tab_style1" colspan="2" style="text-align: left;">
			    <br/>
			    &nbsp;&nbsp;模版建立说明：<br/>
				&nbsp;&nbsp;1.  模板中涉及需要从系统读取数据的信息，务必用<font color="red">{XXX}</font>代替。例如：<font color="red">{合同编号}、{合同名称}</font>等。<br/>
				&nbsp;&nbsp;2.	可以从系统中涉及的读取信息为：<font color="red">{合同编号}、{合同名称}、{甲方名称}、{甲方地址}、{甲方联系人}、{甲方联系人手机}、{甲方电话}、{甲方传真}、{乙方名称}、{乙方地址}、{乙方联系人}、{乙方联系人 手机}、{乙方电话}、{乙方传真}、{合同金额}、{货币类型}、{银行账号}、{银行户名}、{开户行}、{税号}</font><br/>
				&nbsp;&nbsp;3.	合同物资明细读取信息为：<font color="red">{合同物资明细}</font>
				</td>
			</tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	