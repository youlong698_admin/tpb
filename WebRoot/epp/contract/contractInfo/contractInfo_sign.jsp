<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>合同签订</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>

<script type="text/javascript">
	//附件需要添加的信息
	var sessionId="<%=session.getId()%>";
	var attachmentType="ContractInfo"; //当前是哪个类别功能上传的附件
	var path="<%= path %>" 
	//返回信息
	var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
	var fileNameData=[];//已上传的文件名
	var fileTypeData=[];//已上传的文件的格式
	var attIdData=[];//已存入附件表的附件信息

	var api = frameElement.api, W = api.opener;
	$(function (){
		//返回信息
	   <c:if test="{message!=null}">
		  window.onload=function(){ 
		    showMsg('success','{message}');
		    W.doQuery();
		    api.close();
		  	}
		</c:if>
	    
	});	
	function save(){
			
	  if(fileNameData==""){
	    showMsg("alert","温馨提示：请上传合同扫描件！");
	    return false;
	  }
	  
	  //提交之前把选择的附件信息填充值
		$("#fileNameData").val(fileNameData);
		$("#uuIdData").val(uuIdData);
		$("#fileTypeData").val(fileTypeData);
		$("#attIdData").val(attIdData);
		
	  document.defaultForm.action="updateSignContractInfo_contractInfo.action";
	  document.defaultForm.submit();
	}
	</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="">
<input  name="contractInfo.ciId" type="hidden" id="ciwId" value="${contractInfo.ciId }">
<input type="hidden" name="contractInfo.attIds" id="attIds" />
<input type="hidden" name="contractInfo.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="contractInfo.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="contractInfo.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="contractInfo.attIdData" id="attIdData" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter">
               	<!-------------------- 合同签订信息--------------------------->      
    	<table class="table_ys1"  width="100%">		        	
            <tr>
			    <td width="15%" class="Content_tab_style1">项目名称：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input name="contractInfo.projectName" type="text" id="projectName" value="${contractInfo.projectName }" readonly>
			    </td>
			    <td width="15%" class="Content_tab_style1">项目编号：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input   type="text" id="contractInfo.bidCode" value="${contractInfo.bidCode }" readonly></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同编号：</td>
			    <td class="Content_tab_style2"><input name="contractInfo.contractCode" type="text" id="contractCode" value="${contractInfo.contractCode }" readonly></td>
			    <td class="Content_tab_style1">合同名称：</td>
			    <td class="Content_tab_style2"><input name="contractInfo.contractName" type="text" id="contractSignAddress" value="${contractInfo.contractName }" readonly></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同签订日期：</td>
			    <td class="Content_tab_style2">
			       <input class="Wdate" datatype="*" nullmsg="合同签订日期不能为空！" name="contractInfo.signDate" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});" type="text" id="signDate" value=""><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">合同签订日期不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">合同签订地点：</td>
			    <td class="Content_tab_style2">
			      <input  name="contractInfo.contractSignAddress"  type="text" id="contractSignAddress" value=""></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同生效日期：</td>
			    <td class="Content_tab_style2">
			       <input class="Wdate"  name="contractInfo.conValidDate" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-%d'});" type="text" id="conValidDate" value="">
			    </td>
			    <td class="Content_tab_style1">合同结束日期：</td>
			    <td class="Content_tab_style2">
			       <input class="Wdate" name="contractInfo.conFinishDate" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-{%d+1}'});" type="text" id="conFinishDate" value="">
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">质保开始日期：</td>
			    <td class="Content_tab_style2">
			       <input class="Wdate"   name="contractInfo.warrantyPeriodStartDate" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-{%d+1}'});" type="text" id="warrantyPeriodStartDate" value="">
			    </td>
			    <td class="Content_tab_style1">质保结束日期：</td>
			    <td class="Content_tab_style2">
			       <input class="Wdate"  name="contractInfo.warrantyPeriodStopDate" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-{%d+1}'});" type="text" id="warrantyPeriodStopDate" value="">
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">预计到货日期：</td>
			    <td class="Content_tab_style2"><input class="Wdate" name="contractInfo.requirementArrivalDate" readonly onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-{%d+1}'});" type="text" id="requirementArrivalDate" value=""></td>
			    <td class="Content_tab_style1">付款方式：</td>
			    <td class="Content_tab_style2"><input  name="contractInfo.paymentType" type="text" id="paymentType" value=""></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3"><textarea name="contractInfo.conRemark" style="width: 80%">${contractInfo.conRemark }</textarea></td>
			</tr>			
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span>
				<br/><font color="#FF0000"> * 电子版合同扫描件</font></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>签订合同</button>
			<button class="btn" id="btn-close" type="button" onclick="api.close();"><i class="icon-white icon-remove-sign"></i>关闭</button>			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;
			
			save();
			return false;	
		}
	});
})
</script>
</body>
</html> 	