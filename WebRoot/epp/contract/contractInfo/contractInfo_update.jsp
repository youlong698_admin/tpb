<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>修改合同信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>

<script type="text/javascript">
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="ContractInfo"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 

var uuIdData=${contractInfo.uuIdData};//已上传的文件的文件uuid，上传后的文件以uuId命名
var fileNameData=${contractInfo.fileNameData};//已上传的文件名
var fileTypeData=${contractInfo.fileTypeData};//已上传的文件的格式
var attIdData=${contractInfo.attIdData};//已存入附件表的附件信息

    var unitStr="";
    <c:forEach var="dictionary" items="${dictionaryList}">
        unitStr+="<option value=\"${dictionary.dictName }\">${dictionary.dictName }</option>";
    </c:forEach>
   function doView(){
      window.location.href="viewContractInfo_contractInfo.action";
   }   
   function doText(){
      var tempId=$("#contractTempId").val();
      var ciId=$("#ciId").val();
      window.location.href="fileAlertContract_contractInfo.action?tempId="+tempId+"&contractInfo.ciId="+ciId;      
   }
   var num = ${fn:length(cmList)},j=1;
   function doAdd(){
    if(j==0) j=1;	
	var tr=document.getElementById("listtable").insertRow(j);
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
	var cell3 = tr.insertCell();
	var cell4 = tr.insertCell();
	var cell5 = tr.insertCell();
	var cell6 = tr.insertCell();
	var cell7 = tr.insertCell();
	var cell8 = tr.insertCell();
	var cell9 = tr.insertCell();
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this,"+num+")'><i class=\"icon-white icon-trash\"></i></button>";
    cell1.style.textAlign="center";
	cell2.innerHTML="<input type='text' name='cmList["+num+"].materialCode' id='materialCode_"+num+"' value=''  style='width: 80%'><input type='hidden' name='cmList["+num+"].materialId' id='materialId_"+num+"' value=''>" ;
	cell2.style.textAlign="center";
	cell3.innerHTML="<input type='text' id='materialName_"+num+"' value='' name='cmList["+num+"].materialName' style='width: 80%'/>" ;
	cell3.style.textAlign="center";
	cell4.innerHTML="<input type='text' id='materialType_"+num+"' value='' name='cmList["+num+"].materialType' style='width: 80%'/>";
	cell4.style.textAlign="center";
    cell5.innerHTML="<select id='unit_"+num+"' name='cmList["+num+"].unit'  style='width: 80%'>"+unitStr+"</select>";
    cell5.style.textAlign="center";
    cell6.innerHTML="<input type='text' id='amount_"+num+"' name='cmList["+num+"].amount' value='' class='numPrice' onblur='validateNum(this);accountPrice("+num+");' style='width: 80%'>";
    cell6.style.textAlign="center";
	cell7.innerHTML="<input type='text' id='price_"+num+"' value='' name='cmList["+num+"].price' class='numPrice' onblur='validateNum(this);accountPrice("+num+");' style='width: 80%'/>" ;
	cell7.style.textAlign="center";
	cell8.innerHTML="<span id='priceSumStr_"+num+"'>0</span>" ;
	cell8.style.textAlign="right";
	cell9.innerHTML="<input name='rmdList["+num+"].remark' type='text' style='width: 80%'/>" ;
	cell9.style.textAlign="center";
	num++;
	j++;
}
	//删除一行
	function deleteRow(obj,num){
	    j--;
	    var priceSumStr=$("#priceSumStr_"+num).text();
	    if(priceSumStr!=""){
	        var totalPriceStr=$("#totalPriceStr").text();
	        totalPrice=FloatSub(totalPriceStr,priceSumStr);
		    document.getElementById("totalPriceStr").innerText =parseFloat(totalPrice).toFixed(2);
	    }
		$(obj).parent().parent().remove(); 
	    
	}
  //计算总价
	function accountPrice(num) 
	{
	    var priceSumStr=$("#priceSumStr_"+num).text();
		var amount,price,totalPrice=0;
        price=$("#price_"+num).val();
        if(price==""){
           return false;
        }
        amount=$("#amount_"+num).val();
        if(amount==""){
           return false;
        }
        //showMsg("alert","amount="+amount);
        //计算小计
        var  prices=eval(price*amount).toFixed(2)+"";
        document.getElementById("priceSumStr_"+num).innerText=prices;
            
       /*计算总合计*/
        var totalPriceStr=$("#totalPriceStr").text();
        totalPrice=FloatSub(totalPriceStr,priceSumStr);
		totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
	    document.getElementById("totalPriceStr").innerText =totalPrice;
	}
   //保存
	 function save(){
	     var indexArr=document.getElementsByName("rowIndex");
	     var index,flag=true;
	     if(indexArr.length==0){
	        showMsg("alert","温馨提示：请添加合同物资明细！");
	        return false;
         }
		 for(var i=1;i<indexArr.length;i++){
		  index=indexArr[i].value;
		   if($("#materialCode_"+index).val()==""){
	           showMsg("alert","温馨提示：第“"+(i+1)+"”行编码不能为空");
	           flag=false;
	           return false;
			}
		   if($("#materialName_"+index).val()==""){
	           showMsg("alert","温馨提示：第“"+(i+1)+"”行名称不能为空");
	           flag=false;
	           return false;
			}
			if($("#amount_"+index).val()==""){
	           showMsg("alert","温馨提示：第“"+(i+1)+"”行数量不能为空");
	           flag=false;
	           return false;
			}
			if($("#price_"+index).val()==""){
	           showMsg("alert","温馨提示：第“"+(i+1)+"”行单价不能为空");
	           flag=false;
	           return false;
			}
		 }
		 var contractMoney=$("#contractMoney").val();
		 var totalPriceStr=parseFloat($("#totalPriceStr").text());
		 if(FloatSub(totalPriceStr,contractMoney)!=0){
		   showMsg("alert","温馨提示：合同金额和合同物资的总金额不相等");
		   return;
		 } 
		 if(flag){
		 
		  //提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			
		    document.defaultForm.submit();
		 }
	 }
	function doBill(sign){
	   if(sign==0){
	     $("#billTr").hide();
	     $("#billUndertaker").removeAttr("datatype");
	     $("#billUndertaker").removeAttr("nullmsg");
	   }else{
	     $("#billTr").show();
	     $("#billUndertaker").attr("datatype","*");
	     $("#billUndertaker").attr("nullmsg","收货联系人不能为空！");
	   }
	}
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="updateContractInfo_contractInfo.action">
<input  name="contractInfo.comId" type="hidden" id="comId" value="${contractInfo.comId }">
<input  name="contractInfo.ciId" type="hidden" id="ciId" value="${contractInfo.ciId }">
<input  name="contractInfo.isUsable" type="hidden" id="isUsable" value="${contractInfo.isUsable }">
<input  name="contractInfo.writer" type="hidden" id="writer" value="${contractInfo.writer }">
<input  name="contractInfo.writeDate" type="hidden" id="writeDate" value="${contractInfo.writeDate }">
<input  name="contractInfo.runStatus" type="hidden" id="runStatus" value="${contractInfo.runStatus }">
<input  name="contractInfo.statusCn" type="hidden" id="isUsable" value="${contractInfo.statusCn }">
<input  name="contractInfo.deptId" type="hidden" id="isUsable" value="${contractInfo.deptId }">
<input  name="contractInfo.baId" type="hidden" id="baId" value="${contractInfo.baId }">
<input  name="contractInfo.contractAmount" type="hidden" id="contractAmount" value="${contractInfo.contractAmount }">
<input  name="contractInfo.realInputAmount" type="hidden" id="realInputAmount" value="${contractInfo.realInputAmount }">
<input  name="contractInfo.realSendAmount" type="hidden" id="realSendAmount" value="${contractInfo.realSendAmount }">
<input  name="contractInfo.contractAppFile" type="hidden" id="contractAppFile" value="${contractInfo.contractAppFile }">

<input type="hidden" name="contractInfo.fileNameData" id="fileNameData" value="${contractInfo.fileNameData }"/>
<input type="hidden" name="contractInfo.uuIdData" id="uuIdData" value="${contractInfo.uuIdData }"/>
<input type="hidden" name="contractInfo.fileTypeData" id="fileTypeData" value="${contractInfo.fileTypeData }"/>
<input type="hidden" name="contractInfo.attIdData" id="attIdData" value="${contractInfo.attIdData }"/>
<!-- 修改页面删除附件的Id -->
<input type="hidden" name="contractInfo.attIds" id="attIds" />

<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter">
   <div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
         
				<li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');"><b>合同基本信息</b></a></li>
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b>合同物资信息</b></a></li>
         </ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">      
    	<table class="table_ys1"  width="100%">		        	
            <tr>
			    <td width="15%" class="Content_tab_style1">项目名称：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input  datatype="*" nullmsg="项目名称不能为空！" name="contractInfo.projectName" type="text" id="projectName" value="${contractInfo.projectName }" <c:if test="${not empty contractInfo.rcId}">readonly</c:if>>
			       <div class="info"><span class="Validform_checktip">项目名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			       <input  name="contractInfo.rcId" type="hidden" id="rcId" value="${contractInfo.rcId }"></td>
			    <td width="15%" class="Content_tab_style1">项目编号：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input   type="text" name="contractInfo.bidCode"  id="bidCode" value="${contractInfo.bidCode }" <c:if test="${not empty contractInfo.rcId}">readonly</c:if>><font color="#ff0000">*</font></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同编号：</td>
			    <td class="Content_tab_style2">
			       <input name="contractInfo.contractCode" type="text" id="contractCode" readonly value="${contractInfo.contractCode }">
			    </td>
			    <td class="Content_tab_style1">合同名称：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="合同名称不能为空！" name="contractInfo.contractName" type="text" id="contractSignAddress" value="${contractInfo.contractName }"><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">合同名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同分类：</td>
			    <td class="Content_tab_style2">
				    <select name="contractInfo.contractType" id="contractType">
				      <c:forEach items="${contractTypeList}" var="contractClass">
				         <option value="${contractClass.content }" <c:if test="${contractClass.content==contractInfo.contractType }">selected</c:if>>${contractClass.content }</option>
				      </c:forEach>
				    </select>
			    </td>
			    <td class="Content_tab_style1">项目分类：</td>
			    <td class="Content_tab_style2">
			        <select name="contractInfo.projectType" id="projectType">
				      <c:forEach items="${projectTypeList}" var="contractClass">
				         <option value="${contractClass.content }" <c:if test="${contractClass.content==contractInfo.projectType }">selected</c:if>>${contractClass.content }</option>
				      </c:forEach>
				    </select>
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">甲方名称：</td>
			    <td class="Content_tab_style2">
			    <input  datatype="*" nullmsg="甲方名称不能为空！" name="contractInfo.contractPersonNameA" type="text" id="contractPersonNameA" value="${contractInfo.contractPersonNameA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">甲方名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">乙方名称：</td>
			    <td class="Content_tab_style2">
				    <input  datatype="*" nullmsg="乙方名称不能为空！" name="contractInfo.contractNameB" readonly type="text" id="supplierName" value="${contractInfo.contractNameB }">
				    <input  name="contractInfo.supplierId" type="hidden" id="supplierId" value="${contractInfo.supplierId }">
				    <c:if test="${empty contractInfo.rcId}"><img src="images/select.gif" onclick="createdetailwindow('选择供应商','viewSupplierBaseInfoCompany_oneSupplierSelect.action',3);" title="选择供应商"/></c:if><font color="#ff0000">*</font>			      
				    <div class="info"><span class="Validform_checktip">乙方名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方地址：</td>
			    <td class="Content_tab_style2">
			        <input  datatype="*" nullmsg="甲方地址不能为空！" name="contractInfo.contractAddressA" type="text" id="contractAddressA" value="${contractInfo.contractAddressA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">甲方地址不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">乙方地址：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="乙方地址不能为空！" name="contractInfo.contractAddressB" type="text" id="supplierAddress" value="${contractInfo.contractAddressB }"><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">乙方地址不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方联系人：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="甲方联系人不能为空！" name="contractInfo.contractUndertaker" type="text" id="contractUndertaker" value="${contractInfo.contractUndertaker }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">甲方联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">乙方联系人：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="*" nullmsg="乙方联系人不能为空！" name="contractInfo.contractPersonB" type="text" id="contactPerson" value="${contractInfo.contractPersonB }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">乙方联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">甲方联系人手机：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="m" nullmsg="甲方联系人手机不能为空！" name="contractInfo.contractMobileA" type="text" id="contractMobileA" value="${contractInfo.contractMobileA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">甲方联系人手机不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">乙方联系人手机：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="m" nullmsg="乙方联系人手机不能为空！" name="contractInfo.contractMobileB" type="text" id="contractMobileB" value="${contractInfo.contractMobileB }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">乙方联系人手机不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方电话：</td>
			    <td class="Content_tab_style2">
			    <input datatype="/^((0\d{2,3})-)(\d{7,8})?$/" errormsg="电话格式不正确！格式为：0311-68030978" nullmsg="甲方电话不能为空！" name="contractInfo.contractTelA" type="text" id="contractTelA" value="${contractInfo.contractTelA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">甲方电话不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">乙方电话：</td>
			    <td class="Content_tab_style2">
			    <input  datatype="/^((0\d{2,3})-)(\d{7,8})?$/" errormsg="电话格式不正确！格式为：0311-68030978" nullmsg="乙方电话不能为空！" name="contractInfo.contractTelB" type="text" id="supplierPhone" value="${contractInfo.contractTelB }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">乙方电话不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方传真：</td>
			    <td class="Content_tab_style2">
			    <input  datatype="*" nullmsg="甲方传真不能为空！" name="contractInfo.contractFaxA" type="text" id="contractFaxA" value="${contractInfo.contractFaxA }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">甲方传真不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">乙方传真：</td>
			    <td class="Content_tab_style2">
			    <input  datatype="*" nullmsg="乙方传真不能为空！" name="contractInfo.contractFaxB" type="text" id="supplierFax" value="${contractInfo.contractFaxB }"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">乙方传真不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>			
			<tr>
			    <td class="Content_tab_style1">银行账号：</td>
			    <td class="Content_tab_style2">
			    <input  name="contractInfo.bankAccount" type="text" id="bankAccount" value="${contractInfo.bankAccount }">
			     </td>
			    <td class="Content_tab_style1">银行户名：</td>
			    <td class="Content_tab_style2">
			    <input  name="contractInfo.bankAccountName" type="text" id="bankAccountName" value="${contractInfo.bankAccountName }">
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">开户银行：</td>
			    <td class="Content_tab_style2">
			    <input  name="contractInfo.bank" type="text" id="bank" value="${contractInfo.bank }">
			    </td>
			    <td class="Content_tab_style1">税号：</td>
			    <td class="Content_tab_style2">
			    <input  name="contractInfo.dutyParagraph" type="text" id="dutyParagraph" value="${contractInfo.dutyParagraph }">
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同金额：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="n |/^(0|[1-9][0-9]*)+(.[0-9]{1,2})?$/" nullmsg="合同金额不能为空！" errormsg="合同金额必须是数字" name="contractInfo.contractMoney" type="text" id="contractMoney" value="<fmt:formatNumber value="${contractInfo.contractMoney }" pattern="#00.00#"/>">
			       <div class="info"><span class="Validform_checktip">合同金额不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        <select name="contractInfo.conMoneyType" id="conMoneyType">
				      <c:forEach items="${moneyTypeList}" var="contractClass">
				         <option value="${contractClass.content }" <c:if test="${contractClass.content==contractInfo.conMoneyType }">selected</c:if>>${contractClass.content }</option>
				      </c:forEach>
				    </select>
				</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同模板：</td>
			    <td class="Content_tab_style2">
			        <select name="contractInfo.contractTempId" id="contractTempId">
				      <c:forEach items="${contractTemplates}" var="contractTemplate">
				         <option value="${contractTemplate.ctId }" <c:if test="${contractTemplate.ctId==contractInfo.contractTempId }">selected</c:if>>${contractTemplate.templateMame }</option>
				      </c:forEach>
				    </select>
				</td>
				<td class="Content_tab_style1">框架协议：</td>
			    <td class="Content_tab_style2">
			         <input type="radio" name="contractInfo.framework" id="framework" value="0" <c:if test="${contractInfo.framework==0}">checked</c:if> onclick="doBill(0)"/>是
			         <input type="radio" name="contractInfo.framework" id="framework" value="1" <c:if test="${contractInfo.framework==1}">checked</c:if> onclick="doBill(1)"/>否
				</td>
			</tr>
			<c:if test="${contractInfo.framework==1}">
			<tr id="billTr">
			    <td class="Content_tab_style1">收货联系人：</td>
			    <td class="Content_tab_style2">
			       <input  name="contractInfo.billUndertaker" type="text" id="billUndertaker" value="${contractInfo.billUndertaker}">
			       <input  name="contractInfo.billUndertakerId" type="hidden" id="billUndertakerId" value="${contractInfo.billUndertakerId}">
			       <img id="selectSupplierImg"  src="images/select.gif" onclick="createdetailwindow('选择收货联系人','viewDeptIndex_userTree.action',1);" title="选择收货联系人"/><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">收货联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">收货联系人手机：</td>
			    <td class="Content_tab_style2">
			       <input   name="contractInfo.billMobile" type="text" id="billMobile" value="${contractInfo.billMobile }">
			    </td>
			</tr>
			</c:if>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3"><textarea name="contractInfo.conRemark" style="width: 80%">${contractInfo.conRemark }</textarea></td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span>
				<br/><font color="#FF0000"> * 待领导审批的合同原件</font></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
		</table>
		 </div>
             	
      		<!--------------------合同物资明细--------------------------->
      		<div id="dTab2" class="HackBox">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_ys1" id="listtable">
			<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap><c:if test="${empty contractInfo.rcId}"><img src="images/add.gif" onclick="doAdd();" title="添加合同物资"/></c:if><c:if test="${not empty contractInfo.rcId}">序号</c:if></th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>单价</th>
				<th width="100px" nowrap>小计</th>
				<th width="100px" nowrap>备注</th>
			</tr>
			    <c:set var="totalPrice" value="0"/>
				<c:forEach items="${cmList}" var="contractMaterial" varStatus="status">
				    <c:set var="totalPrice" value="${totalPrice+contractMaterial.amount*contractMaterial.price }"/> 
					<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>
						   <c:if test="${empty contractInfo.rcId}"><button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this,${status.index+1})'><i class="icon-white icon-trash"></i></button></c:if> ${status.index+1}
						   <input type='hidden' name='rowIndex' value='${status.index}'/>
						</td>
						<td>
						    ${contractMaterial.materialCode}
						    <input type='hidden' value='${contractMaterial.materialId}' name='cmList[${status.index}].materialId' /> 
						    <input type='hidden' value='${contractMaterial.materialCode}' name='cmList[${status.index}].materialCode' /> 
						</td>
						<td>
						   ${contractMaterial.materialName}
						    <input type='hidden' value='${contractMaterial.materialName}' name='cmList[${status.index}].materialName' /> 
						</td>
						<td>
						   ${contractMaterial.materialType}
						    <input type='hidden' value='${contractMaterial.materialType}' name='cmList[${status.index}].materialType' /> 
						</td>
						<td>
						   ${contractMaterial.unit}
						    <input type='hidden' value='${contractMaterial.unit}' name='cmList[${status.index}].unit' /> 
					    </td>
						<td align="right">
					     ${contractMaterial.amount}
						    <input type='hidden' value='${contractMaterial.amount}' name='cmList[${status.index}].amount' /> 
						 </td>
					    <td align="right">
					       <fmt:formatNumber value="${contractMaterial.price}" pattern="#,##0.00"/>
						    <input type='hidden' value='${contractMaterial.price}' name='cmList[${status.index}].price' /> 
					    </td>
					    <td align="right">
					       <fmt:formatNumber value="${contractMaterial.price*contractMaterial.amount}" pattern="#00.00#"/>
					    </td>
						<td>
						    <input type='text' value='${contractMaterial.remark}' name='cmList[${status.index}].remark' />
						    <input type='hidden' value='${contractMaterial.cmId}' name='cmList[${status.index}].cmId' /> 
						    <input type='hidden' value='${contractMaterial.ciId}' name='cmList[${status.index}].ciId' /> 
						</td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总价：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalPriceStr"><fmt:formatNumber value="${totalPrice}" pattern="#00.00#"/><!-- 小计 --></span>
					</td>
					<td>&nbsp;</td>
				</tr>	
       	</table>
	</div>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-info" id="btn-view" type="button" onclick="doView()"><i class="icon-white icon-th-list"></i>查看合同列表</button>
			<button class="btn btn-warning" id="btn-text" type="button" onclick="doText()"><i class="icon-white icon-file"></i>生成合同文本</button>			
		</div>
</div>
</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			save();
			return false;	
		}
	});
})
</script>
</body>
</html> 	