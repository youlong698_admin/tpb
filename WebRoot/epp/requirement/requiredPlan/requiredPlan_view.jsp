<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>计划管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/required/requirePlan.js" type="text/javascript" ></script>
<script type="text/javascript">
var _table;
var requiredMaterialWorkflow="${requiredMaterialWorkflow}";
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	 _table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findRequiredPlan_requiredPlan.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX,
            {	
            	className : "ellipsis",
				orderable : false,
            	render: function(data,type, row, meta) {
					
					if(row.orderStateName=="流程审批中")
					{
					    var action="display_workflow.action?param=view&processId="+row.processId+"&orderId=";
						return "<img src=\"<%=path%>/images/msg_1.jpg\" title=\"流程催办\" onclick=\"openWorkFlowWindow('"+action+"',"+row.rmId+",'"+row.processId+"')\" style=\"cursor:pointer\">";
					}else
					{
						return "";
					}
                },         	
            	width : "30px"
            },
            {
				data : "purchaseDeptName",
				width : "10%"
			},
			{
            	data: "rmCode",
            	width : "15%"
            },
            {	
            	className : "ellipsis",
            	data: "rmName",
            	render: viewGeneralManage,
            	orderable : false		
            },
			{
				data : "buyType",
				render: CONSTANT.DATA_TABLES.RENDER.BUYTYPE,
				orderable : false,
				width : "7%"
			},
			{
				data : "writerCN",
				orderable : false,
				width : "8%"
			},
			{
				data : "deptName",
				width : "10%"
			},
			{
				className : "ellipsis",
				data : "writeDate",
				width : "10%"
			},
			{
				data : "orderStateName",
				orderable : false,
				render: viewWorkflowProcess,
				width : "10%"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	//给当前行某列加样式
        	$('td', row).eq(9).addClass(row.orderState!=""?"text-success":"text-error");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.updateItem(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	})
	//撤销确认流程
	$("#btn-processCancel").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.pCancelItem(arrItemId);
	})
	//提交确认流程
	$("#btn-processSubmit").click(function(){
  		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.pSubmitItem(arrItemId);
	});
	//提交正式计划
	$("#btn-Submit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.btnSubmitItem(arrItemId);
	});
	//导出excel
	$("#btn-daochu").click(function(){
		GeneralManage.daochuItems();
	});

	$("#btn-daili").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.dailiItem(arrItemId);
	});
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//默认进入的排序
		 param.orderColumn="de.rmCode";
		//组装排序参数
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 3:
				param.orderColumn = "de.rmCode";
				break;
			case 7:
				param.orderColumn = "de.deptId";
				break;
			case 8:
				param.orderColumn = "de.writeDate";
				break;
			default:
				param.orderColumn = "de.rmCode";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		
		param.rmCode = $("#rmCode").val();
		param.projectName = $("#projectName").val();;
		param.writeDateStart = $("#writeDateStart").val();
		param.writeDateEnd = $("#writeDateEnd").val();
		param.writer = $("#writer").val();
		param.rmType = $("#rmType").val();
		param.status = $("#status").val();
		param.deptName = $("#deptName").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	addItemInit:function(){
		window.location.href="saveRequiredPlanInit_requiredPlan.action";
	},
	updateItem:function(selectedItems){
		var v = selectedItems;
		if(v.length < 1){
      		showMsg("alert","温馨提示：请选择将要修改的信息！");
      	}else if(v.length > 1){
	    	showMsg("alert","温馨提示：只能选择一条信息修改！");
	    }else{
	    	var orderState = v[0].orderState;
	    	if(orderState == "0"){
    	    		showMsg("alert","温馨提示:此项目流程已结束,不能修改!");
    	    		return false;
    	    	}else if(orderState == "1"){
    	    		showMsg("alert","温馨提示:此项目流程正在运行,不能修改!");
    	    		return false;
    	    	}else{
    	    		location.href="updateRequiredMaterialInit_requiredPlan.action?requiredMaterial.rmId="+v[0].rmId;
    	    	}
	    }
	},
	deleteItem : function(selectedItems) {
		var v = selectedItems;
		if(v.length < 1){
      		showMsg("alert","温馨提示：请选择将要删除的信息！");
      	}else{  
    	        var f=0;g=0; 	    	
      	   		var ids = "";
      	   		var orderIds = "";
    			for(var i=0;i<v.length;i++){
    			var orderState = v[i].orderState;
				    if(orderState == "0")f=1;
				    else if(orderState == "1")g=1;
    				if(i == (v.length-1)){
    					ids += v[i].rmId;
    					orderIds = v[i].orderId;
    				}else{
    					ids += v[i].rmId + ",";
    					orderIds += v[i].orderId + ",";
    				}
    			}
    			if(f==1){
	    	    	showMsg("alert","温馨提示:此项目流程已结束,不能删除!");
	    	    	return false;
		    	 }else if(g==1){
	    	    	showMsg("alert","温馨提示:此项目流程正在运行,不能删除!");
	    	    	return false;
		    	 }
    			$.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
 				  var result = ajaxGeneral("deleteRequiredMaterial_requiredPlan.action","rmCode="+v[0].rmCode+"&ids="+ids);
 					   showMsg('success',''+result+'',function(){
 					   		doQuery();							
 		   					});
 				   	 	});
	    }
	},
 	//撤销确认流程
	pCancelItem:function(selectedItems){
		var v = selectedItems;
		if(v.length < 1){
      		showMsg("alert","温馨提示：请选择将要撤销的信息！");
      	}else if(v.length > 1){
	    	showMsg("alert","温馨提示：只能选择一条信息撤销！");
	    }else{
	    	var orderState = v[0].orderState;
	    	if(orderState == "0"){
    	    		showMsg("alert","温馨提示:此项目流程已结束,不能撤销!");
    	    		return false;
   	    	}else{
   	    		var action = "deleteCancelReqProcess_bidworkflow.action";
				var param = "requiredMaterial.rmId="+v[0].rmId;
				var result = ajaxGeneral(action,param,"text");
				if(parseInt(result)==0){
					showMsg("alert","温馨提示：流程撤销成功！",function(){
					 window.location.href= window.location.href;
					});
				}else if(parseInt(result)==1){
					showMsg("alert","温馨提示：流程编号不存在！");
					return ;
				}else if(parseInt(result)==2){
					showMsg("alert","温馨提示：没有提交流程，不能撤销！");
					return ;
				}else{
				    showMsg("alert","温馨提示：流程已经处理，不能撤回");
					return ;				
				}
    		}
	    }
	},
	//提交确认流程
	pSubmitItem:function(selectedItems){
  		var v = selectedItems;
  		if(v.length < 1){
	  		showMsg("alert","温馨提示：请选择将要提交的信息！");
	  	}else if(v.length > 1){
	  		showMsg("alert","温馨提示：请选择一条将要提交的信息的信息！");
	    }else{
	    	var rmId = v[0].rmId;
	    	var rmCode = v[0].rmCode;
	    	var processId = v[0].processId;
	    	var orderId = v[0].orderId;
	    	var orderState = v[0].orderState;
	    	var instanceUrl = v[0].instanceUrl;
	    	
	    	if(orderState == "0"){
	    		showMsg("alert","温馨提示:此项目流程已结束,请选择将要提交的信息!");
	    	}else if(orderState == "1"){
	    		showMsg("alert","温馨提示:此项目流程正在运行,请选择将要提交的信息!");
	    	}else{
	    		 var orderNo="${work_flow_type}"+rmId;
					createdetailwindow("提交计划审批","<%=path%>"+instanceUrl+"?processId="+processId+"&orderId="+orderId+"&orderNo="+orderNo,1);
			}
	    }
	},
	//提交为正式计划
	btnSubmitItem:function(selectedItems){
	  var v = selectedItems;
		if(v.length < 1){
      		showMsg("alert","温馨提示：请选择将要提交未正式计划的信息！");
      	}else{
      	        var f=0;
    	    	var ids = "";
      	   		var orderIds = "";
    			for(var i=0;i<v.length;i++){
    			var orderState = v[i].orderState;
				    if(orderState == "0") f=1;				   
    				if(i == (v.length-1)){
    					ids += v[i].rmId;
    					orderIds = v[i].orderId;
    				}else{
    					ids += v[i].rmId + ",";
    					orderIds += v[i].orderId + ",";
    				}
    			}
    			if(f==1){
	    	    	showMsg("alert","温馨提示:您选择的计划中存在正式计划,不能提交!");
	    	    	return false;
		    	 }
    			$.dialog.confirm("温馨提示：你确定要提交选中的信息！",function(){
 				  var result = ajaxGeneral("updateProcessRequiredMaterial_requiredPlan.action","ids="+ids);
 					   showMsg('success',''+result+'',function(){
 					   		doQuery();							
 		   					});
 				   	 	});
	    }
	},
	//导出excel
	daochuItems:function(){		
		document.forms[0].action="exportRequiredMaterialExcel_requiredPlan.action";
		document.forms[0].submit();
		
	}
};

    
    //查看
    function viewGeneralManage(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看计划\",\"viewRequiredPlanDetail_requiredPlan.action?requiredMaterial.rmId=" + row.rmId + "\",1)' title='"+data+"'>"+data+"</a>"; 
              
    }

	//查看流程审批信息
	
      function viewWorkflowProcess(data, type, row,   meta ){
        if(requiredMaterialWorkflow=="0"){
    	  if(row.orderState != ""){
	    		  var action="display_workflow.action?param=view&processId="+row.processId+"&orderId=";
	    		  return "<a href='#' onclick='openWorkFlowWindow(\""+action+"\","+row.rmId+",\""+row.processId+"\")'>"+data+"</a>";
    	  }else{
    		  return "等待提交";
    	  }
    	  }else{
    	    if(row.orderState=="01")  return "等待提交";
    	    else  if(row.orderState=="0")  return "已完成";
    	  }
    	  
      }
      
	//弹出一个新窗口
	function openWorkFlowWindow(action, rmId, processId){
	    rmId="${work_flow_type}"+rmId;
		$.ajax({
			url:"<%=path%>/findWfOrder_workflow.action",
			type:"POST",
			dataType: 'json',
			async:false, 
			data:{orderNo:rmId, processId:processId},
			success:function(msg){
				var rsb = msg;
				var rs = rsb.order;
				if(rs.length > 0){
					createdetailwindow("查看流程",action+rs[0].id,0);
				}else{
					return false;
				}
			},
			error:function(){
			}
		});
		
	}

	function doQuery(){
	     _table.draw();
	   }

	//重置
	function doReset(){        
		document.forms[0].elements["rmCode"].value	=	"";	
		document.forms[0].elements["rmName"].value	=	"";
		document.forms[0].elements["writeDateStart"].value		=	"";
		document.forms[0].elements["writeDateEnd"].value	=	"";
		document.forms[0].elements["writer"].value      = "";
		document.forms[0].elements["rmType"].value      = "";
		document.forms[0].elements["status"].value      = "";
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
										     <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i> 填报计划</button>
							<button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
							<c:if test="${requiredMaterialWorkflow=='0'}">
							<button type="button" class="btn btn-info" id="btn-processSubmit"><i class="icon-white icon-resize-small"></i> 提交流程</button>  <!--fa fa-chain-broken icon-resize-full -->
							<button type="button" class="btn btn-warning" id="btn-processCancel"><i class="icon-white icon-resize-full"></i> 撤销流程</button>
							</c:if>
							<c:if test="${requiredMaterialWorkflow=='1'}">
							<button type="button" class="btn btn-info" id="btn-Submit"><i class="icon-white icon-resize-small"></i> 提交为正式计划</button>  <!--fa fa-chain-broken icon-resize-full -->
							</c:if>
							<button type="button" class="btn btn-info" id="btn-daochu"><i class="icon-white icon-share"></i> 导出Excel</button>					
						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
						计划编号：
					<input  type="text" class="input-medium" placeholder="计划编号" id="rmCode" name="rmCode" size="15" value="" />
				计划名称：
					<input  type="text" class="input-medium" placeholder="计划名称" id="rmName" name="rmName"  size="15" value="" />
				填报日期从：
					<input type="text" class="input-medium Wdate" id="writeDateStart" size="15" name="writeDateStart" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" readonly  value=""/>
					至：<input type="text" class="input-medium Wdate" id="writeDateEnd" size="15" name="writeDateEnd" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" readonly value =""/>
				填报人：
					<input  class="input-medium" placeholder="填报人" type="text" id="writer" size="15" value=""/>
				计划类别：
					<select id="rmType" name="rmType" class="input-small">
					 		<option value="">全部</option>
							<option value="00">普通计划</option>
							<option value="01">加急计划</option>
					</select>
					
				流程状态：
					<select id="status" name="status" class="input-small">
					 		<option value="">全部</option>
							<option value="01">等待提交</option>
							<option value="1">流程中</option>
							<option value="0">已完成</option>
					</select>
					
				<button type="button" class="btn btn-info" id="btn-advanced-search"><i class="icon-white icon-search" ></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon icon-search" ></i> 重置</button>
			
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>操作</th>
									<th>采购组织</th>
									<th>计划编号</th>
									<th>计划名称</th>
									<th>采购类型</th>
									<th>填报人</th>
									<th>所在单位</th>
									<th>填报日期</th>
									<th>状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>