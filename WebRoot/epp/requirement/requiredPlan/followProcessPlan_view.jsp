<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>计划追踪</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" href="<%=path%>/style/timeline/style.css"
			type="text/css" media="all" />
		
		<script language="javaScript">
		var api = frameElement.api, W = api.opener;
		</script>
	</head>
	<body>
	                          	
        <div class="record">	     	
					<c:set var="data_tmp" value=""/>
					<dl class="recordList">	
						<dd>
						<p class="record_Time"></p>
						<div class="record_jd"></div>
						<div class="record_Info">
							<div class="record_arr">◆</div>
							<div class="record_Intro_curr">
								<p class="record_Intro_Title_curr"><span>当前节点：</span><a href="#">${purchaseRecordLog.operateContent } </a></p>							    
							</div>
							
						</div>
						<div class="clear"></div>
					</dd>
					<c:forEach items="${listValue}" var="purchaseRecordLog" > 
					<c:set var="data"><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="yyyy-MM-dd"/></c:set>
					    <c:if test="${data_tmp==''}">
						<dt>
							<p><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="MM-dd"/></p>
						</dt>						
					    <c:set var="data_tmp" value="${data}"/>
						</c:if>	
						 <c:if test="${data_tmp!=''&&data_tmp!=data}">	
						 <dt>
							<p><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="MM-dd"/></p>
						</dt>						
					    <c:set var="data_tmp" value="${data}"/>	
						 </c:if>		
						<dd>
						<p class="record_Time"><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="HH:mm:ss"/></p>
						<div class="record_jd"></div>
						<div class="record_Info">
							<div class="record_arr">◆</div>
							<div class="record_Intro">
								<p><span>流程节点:</span>${purchaseRecordLog.bidNode }</p>
								<p class="record_Intro_Title"><span>节点内容：</span><font color="#00848E">${purchaseRecordLog.operateContent }</font> </p>
							</div>
							
						</div>
						<div class="clear"></div>
					</dd>
					</c:forEach>								
				</dl>
			</div>


	</body>
</html>