<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>修改计划</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script src="<%= path %>/common/script/required/requirePlan.js" type="text/javascript" ></script>
<script language="javaScript">
//附件需要添加的信息
 var sessionId="<%=session.getId()%>";
 var attachmentType="RequiredMaterial"; //当前是哪个类别功能上传的附件
 var path="<%= path %>";
 var fileNameData=${requiredMaterial.fileNameData};
 var uuIdData=${requiredMaterial.uuIdData};
 var fileTypeData=${requiredMaterial.fileTypeData};
 var attIdData=${requiredMaterial.attIdData};
 

var num = <c:out value="${fn:length(listValue)}"/>;
function addRow(materialId,buyCode,buyName,materialType,unit,newPrice,amount,mnemonicCode){
	if(!materialId)var materialId="";
	if(!buyCode)var buyCode="";
	if(!buyName)var buyName="";
	if(!materialType)var materialType="";
	if(!unit)var unit="";
	if(!mnemonicCode)var mnemonicCode="";
	if(!newPrice)var newPrice="";
	if(newPrice == "0" || newPrice==""){newPrice = "0"};
	if(!amount)var amount="";
	
	tr=document.getElementById("listtable").insertRow();
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
	var cell3 = tr.insertCell();
	var cell4 = tr.insertCell();
	var cell5 = tr.insertCell();
	var cell6 = tr.insertCell();
	var cell7 = tr.insertCell();
	var cell8 = tr.insertCell();
	var cell9 = tr.insertCell();
	var cell10 = tr.insertCell();
	var cell11 = tr.insertCell();
	var cell12 = tr.insertCell();
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/><input type='hidden' name='rmdList["+num+"].materialId' id='materialId_"+num+"' value='"+materialId+"'>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this,"+num+")'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML="<input type='text' name='rmdList["+num+"].buyCode' id='buyCode_"+num+"' readonly value='"+buyCode+"' size='10' style='width: 95px' onclick='selectMaterial("+num+")'>" ;
	cell3.innerHTML="<span id='buyName_"+num+"'>"+buyName+"</span> <input type='hidden' id='buyName_s_"+num+"' value='"+buyName+"' name='rmdList["+num+"].buyName' />" ;
	cell4.innerHTML="<span id='materialType_"+num+"'>"+materialType+"</span><input type='hidden' id='materialType_s_"+num+"' value='"+materialType+"' name='rmdList["+num+"].materialType' />";
    cell5.innerHTML="<span id='unit_"+num+"'>"+unit+"</span><input type='hidden' id='unit_s_"+num+"' value='"+unit+"' name='rmdList["+num+"].unit' />";
    cell6.innerHTML="<input type='text' name='rmdList["+num+"].amount'  id='amount_"+num+"' value='"+amount+"'  style='width: 35px' onkeyup='validateNum(this);'>";
	if(newPrice!="0"){
		cell7.innerHTML="<div id='newPrice_"+num+"'>"+newPrice+"<img src='<%= path %>/images/chart_curve.png' style='cursor:pointer' onclick='viewHistoryPrice("+materialId+")'/></div> <input type='hidden' id='newPrice_s_"+num+"' value='"+newPrice+"' name='rmdList["+num+"].newPrice' />" ;
	 }else{
		cell7.innerHTML="<div id='newPrice_"+num+"'></div> <input type='hidden' id='newPrice_s_"+num+"' value='' name='rmdList["+num+"].newPrice' />" ;
	}
	cell8.innerHTML="<input type='text' name='rmdList["+num+"].estimatePrice' value='' id='estimatePrice_"+num+"' style='width: 50px' onkeyup='caculatePrice(this,"+num+");'>" ;
	cell9.innerHTML="<span id='estimateSumPrice_"+num+"'></span> <input type='hidden' id='estimateSumPrice_s_"+num+"' value='0' name='rmdList["+num+"].estimateSumPrice' />" ;
    cell10.innerHTML="<input type='text' name='rmdList["+num+"].deliverDate' id='deliverDate_"+num+"'  class=\"Wdate\" onchange =\"deliverDateFor('"+num+"')\" style='width:90px' "
		    					+"onclick=\"WdatePicker({ dateFmt:'yyyy-MM-dd' })\" readonly>";
	cell11.innerHTML="<input type='hidden' name='rmdList["+num+"].mnemonicCode' id='mnemonicCode_"+num+"' value='"+mnemonicCode+"'><input type='text' name='rmdList["+num+"].deliverPlace' id='deliverPlace_"+num+"'  size='15' style='width: 90px'>" ;
	cell12.innerHTML="<input name='rmdList["+num+"].remark' type='text' style='width: 90%'/>" ;
	num++;
}

function deleteRow(obj,num){
    var priceSumStr=$("#estimateSumPrice_s_"+num).val();
	    if(priceSumStr!=""){
	        var totalPriceStr=$("#totalBudget").val();
	        totalPrice=FloatSub(totalPriceStr,priceSumStr);
		    document.getElementById("totalBudget_s").innerText =parseFloat(totalPrice).toFixed(2);
		    $("#totalBudget").val(parseFloat(totalPrice).toFixed(2));
	    } 
	$(obj).parent().parent().remove();
}
var acd;
function selectMaterial(rowId){
	acd=rowId;
	var url="viewMaterialInfoIndex_requiredPlan.action";
	var pro="dialogWidth=900px;dialogHeight=700px;";	
	createdetailwindow("选择服务编码","viewMaterialInfoIndex_requiredPlan.action",1);
				
}
function valueMaterial(){
	var rowId=acd;	
	   var winObj=document.getElementById('returnVals').value;
		if(winObj){
			var returnVals = winObj.split('@');
			var rmSource = returnVals[0];
			for(var i=1;i<returnVals.length;i++){
				var materialId = returnVals[i].split("#")[0];
				var buyCode = returnVals[i].split("#")[1];
				var buyName = returnVals[i].split("#")[2];
				var materialType = returnVals[i].split("#")[3];
				var unit = returnVals[i].split("#")[4];
				var newPrice = returnVals[i].split("#")[5];
				if(newPrice == "0" || newPrice==""){newPrice = "0"};
				var mnemonicCode = returnVals[i].split("#")[6];
				var amount = returnVals[i].split("#")[7];
				
				if(amount=="" || typeof(amount)=="undefined") amount=0;
				
				if(i==1){
					$('#buyCode_'+rowId).val(buyCode);
					$('#buyName_'+rowId).html(buyName);
					$('#buyName_s_'+rowId).val(buyName);
					$('#materialType_'+rowId).html(materialType);
					$('#materialType_s_'+rowId).val(materialType);					
					$('#unit_'+rowId).html(unit);
					$('#unit_s_'+rowId).val(unit);
					if(newPrice!="0"){
					  $('#newPrice_'+rowId).html(newPrice+"<img src='<%= path %>/images/chart_curve.png' style='cursor:pointer' onclick='viewHistoryPrice("+materialId+")'/>");
					  $('#newPrice_s_'+rowId).val(newPrice);
					}else{
					  $('#newPrice_'+rowId).html("");
					  $('#newPrice_s_'+rowId).val("");
					}
					$('#mnemonicCode_'+rowId).html(mnemonicCode);
					$('#amount_'+rowId).val(amount);
					$('#estimatePrice_'+rowId).val("");
				}else{
					addRow(materialId,buyCode,buyName,materialType,unit,newPrice,amount,mnemonicCode);
				}
			}
		}	
     }     
     function selectBuyType(obj){
         var buyType=obj.value;
         var TpurchaseDeptId=$('#TpurchaseDeptId').val();
         var TownPurchaseDeptId=$('#TownPurchaseDeptId').val();
         if(buyType=="0"){
             $('#ownPurchaseDept').css("display","block");
             $('#purchaseDept').css("display","none");
             $('#purchaseDeptId').val(TownPurchaseDeptId);
         }else{
             $('#ownPurchaseDept').css("display","none");
             $('#purchaseDept').css("display","block");
             $('#purchaseDeptId').val(TpurchaseDeptId);
         }
     }
</script>
<style type="text/css">
	.input_ys1{text-align:center;}
</style>
</head>
<body >
<div class="container-fluid">
<form class="defaultForm" id="requiredPlan_save" method="post" action="saveRequiredPlan_requiredPlan.action" enctype="">
<input type="hidden" name="requiredMaterial.rmId" value="${requiredMaterial.rmId }"/>
<input type="hidden" name="requiredMaterial.rmCode" value="${requiredMaterial.rmCode}"/>
<input type="hidden" name="requiredMaterial.floatCode" value="${requiredMaterial.floatCode}"/>
<input type="hidden" name="requiredMaterial.comId" value="${requiredMaterial.comId}"/>

<input type="hidden" name="requiredMaterial.fileNameData" id="fileNameData" value="${requiredMaterial.fileNameData }"/>
<input type="hidden" name="requiredMaterial.uuIdData" id="uuIdData" value="${requiredMaterial.uuIdData }"/>
<input type="hidden" name="requiredMaterial.fileTypeData" id="fileTypeData" value="${requiredMaterial.fileTypeData }"/>
<input type="hidden" name="requiredMaterial.attIdData" id="attIdData" value="${requiredMaterial.attIdData }"/>
<input type="hidden" name="returnVals" id="returnVals" value=""/>
<!-- 修改页面删除附件的Id -->
<input type="hidden" name="requiredMaterial.attIds" id="attIds" />
<!-- 防止表单重复提交 -->
<s:token/>
 <!-- 当前位置区域  begin-->
	<!-- 当前位置区域  end-->
	<div  class="row-fluid">
			<table class="table_ys1" width="100%">
    				<tr>
		          		<td  colspan="4" class="Content_tab_style_05">修改计划</td>
		        	</tr>
		        	<tr>
		        	    
						<td width="15%" class="Content_tab_style1">计划编号：</td>
						<td width="85%" class="Content_tab_style2" colspan="3">
							<s:property value="requiredMaterial.rmCode"/>&nbsp;
						</td>
		        	</tr>
		    		<tr>
						<td width="15%" class="Content_tab_style1">计划名称：</td>
						<td width="35%" class="Content_tab_style2">
							<input  class="Content_input_style1" type="text" id='rmName' name="requiredMaterial.rmName" size="15" value ="${requiredMaterial.rmName }"  /><font color="#FF0000">*</font>
			 			</td>
			 			<td width="15%" class="Content_tab_style1">计划类型：</td>
						<td width="35%" class="Content_tab_style2">
							<select name="requiredMaterial.rmType" id="rmType">
							<c:forEach items="${requireType}" var="requireType">
							      <option value="${requireType.key }" <c:if test="${requireType.key==requiredMaterial.rmType }">selected="selected"</c:if>>${requireType.value }</option>
							</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
					   <td width="15%" class="Content_tab_style1">采购类型：</td>
						<td width="35%" class="Content_tab_style2">
						    <select name="requiredMaterial.buyType" id="buyType" onchange="selectBuyType(this);">
							<c:forEach items="${buyType}" var="buyType">
							      <option value="${buyType.key }" <c:if test="${buyType.key==requiredMaterial.buyType }">selected="selected"</c:if>>${buyType.value }</option>
							</c:forEach>
							</select>
						</td>						
						<td class="Content_tab_style1" nowrap>采购组织：</td>
						<td class="Content_tab_style2">
						    <c:choose>
						       <c:when test="${requiredMaterial.buyType=='0'}">
						            <span id="ownPurchaseDept">${ownPurchaseDeptName }</span>
									<span id="purchaseDept" style="display: none">${purchaseDeptName }</span>
						       </c:when>
						       <c:otherwise>
						            <span id="ownPurchaseDept"  style="display: none">${ownPurchaseDeptName }</span>
									<span id="purchaseDept">${purchaseDeptName }</span>
						       </c:otherwise>
						    </c:choose>
							<input type="hidden" name="requiredMaterial.purchaseDeptId" id="purchaseDeptId" value="${requiredMaterial.purchaseDeptId }"/>
							<input type="hidden" id="TpurchaseDeptId" value="${purchaseDeptId }"/>
							<input type="hidden" id="TownPurchaseDeptId" value="${ownPurchaseDeptId }"/>
						</td>	
					</tr>
					<tr>
					     <td width="15%"  class="Content_tab_style1">预算金额：</td>
						<td width="35%"  class="Content_tab_style2">
							<span id="totalBudget_s"><fmt:formatNumber value="${requiredMaterial.totalBudget }" pattern="##0.00"/></span>
							<input class="Content_input_style1"  type="hidden" id='totalBudget'  name="requiredMaterial.totalBudget" value="${requiredMaterial.totalBudget}"/>
						</td>
						<td class="Content_tab_style1" nowrap>填报日期：</td>
						<td class="Content_tab_style2">
							<fmt:formatDate value="${requiredMaterial.writeDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
							<input type="hidden" id="writeDate" name="requiredMaterial.writeDate" value="${requiredMaterial.writeDate }"/>
						</td>						
					</tr>
					<tr>
						  <td class="Content_tab_style1">填报人：</td>
						<td class="Content_tab_style2">
							${writerCn }
			 				<input type="hidden" id="writer" name="requiredMaterial.writer" value="${requiredMaterial.writer }"/>
						</td>	
					   <td class="Content_tab_style1">填报单位：</td>
						<td class="Content_tab_style2">
							${deptName }
							<input type="hidden" name="requiredMaterial.deptId" value="${requiredMaterial.deptId }"/>
						</td>
						<td class="Content_tab_style1" nowrap></td>
						<td class="Content_tab_style2">
						</td>						
					</tr>
					<tr>
						<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
						<td class="Content_tab_style2" colspan="3">
							<!-- 附件存放 -->
							<div  id="fileDiv" class="panel"> 
							
							</div>
							<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
							<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
						</td>
					</tr>
			</table>
			</div>  
		<div  class="row-fluid">
       	<table class="table_ys1" id="listtable">
		<tr>
         		<td  colspan="12" class="Content_tab_style_05">计划明细</td>
       	</tr>
		<tr align="center" class="Content_tab_style_04">
			<th width="2%" nowrap> 操作</th>
			<th width="8%" nowrap>编码<font color="#ff0000">*</font></th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>单位</th>
			<th width="55px" nowrap>数量<font color="#ff0000">*</font></th>
			<th width="80px" nowrap>上次采购价</th>
			<th width="80px" nowrap>估算单价</th>
			<th width="100px" nowrap>估算总价</th>
			<th width="10%" nowrap>交货时间</th>
			<th width="10%" nowrap>交货地点</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredMaterialDetail" varStatus="status">
			<tr  class="input_ys1">
				<td><input type="hidden" name="rowIndex" value="${status.index}"/>
				    <input type='hidden' name='rmdList[${status.index}].materialId' id='materialId_${status.index}'  value="${requiredMaterialDetail.materialId}"/>
				    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
				</td>
				<td>
				    <input type='text' id='buyCode_${status.index}' name='rmdList[${status.index}].buyCode' readonly value='${requiredMaterialDetail.buyCode}' size='10' style="width: 95px" onclick='selectMaterial(${status.index})' />
				</td>
				<td>
				   <span id='buyName_${status.index}'>${requiredMaterialDetail.buyName}</span> <input type='hidden' id='buyName_s_${status.index}' value='${requiredMaterialDetail.buyName}' name='rmdList[${status.index}].buyName' /> 
				</td>
				<td>
				   <span id='materialType_${status.index}'>${requiredMaterialDetail.materialType}</span> <input type='hidden' id='materialType_s_${status.index}' value='${requiredMaterialDetail.materialType}' name='rmdList[${status.index}].materialType' />
				</td>
				<td>
				   <span id='unit_${status.index}'>${requiredMaterialDetail.unit}</span> <input type='hidden' id='unit_s_${status.index}' value='${requiredMaterialDetail.unit}' name='rmdList[${status.index}].unit' />
			    </td>
			    <td>
			       <input type='text' name='rmdList[${status.index}].amount' style='width:35px;'  id='amount_${status.index}' value='${requiredMaterialDetail.amount}'  size='5' onkeyup="validateNum(this);caculatePriceByAmt(this,${status.index})" />
			    </td>
				<td>
				   <span id='newPrice_${status.index}'></span><input type='hidden' id='newPrice_s_${status.index}' value='${requiredMaterialDetail.newPrice}' name='rmdList[${status.index}].newPrice' />
				</td>
				<td><input type='text' name='rmdList[${status.index}].estimatePrice' style='width:50px;' id='estimatePrice_${status.index}' value='${requiredMaterialDetail.estimatePrice}' size='10' onkeyup="caculatePrice(this,${status.index});" /></td>
				<td><span id='estimateSumPrice_${status.index}'><fmt:formatNumber value="${requiredMaterialDetail.estimateSumPrice}" pattern="##0.00"/></span><input type='hidden' id='estimateSumPrice_s_${status.index}' value='${requiredMaterialDetail.estimateSumPrice}' name='rmdList[${status.index}].estimateSumPrice' /></td>
				<td>
					<input type='text' name='rmdList[${status.index}].deliverDate' id='deliverDate_${status.index}' class="Wdate" style='width:90px'
		                    onchange ="deliverDateFor('0')"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" readonly value='<fmt:formatDate value="${requiredMaterialDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />' />
		        </td>				
				<td>
					<input type='hidden' name='rmdList[${status.index}].mnemonicCode' id='mnemonicCode_0'  value="${requiredMaterialDetail.mnemonicCode}"/><input type='text' name='rmdList[${status.index}].deliverPlace' id='deliverPlace_${status.index}' value='${requiredMaterialDetail.deliverPlace}'  size='15'  style="width: 90px"/>
				</td>
				<td><input type="text" name='rmdList[${status.index}].remark'  value="${requiredMaterialDetail.remark }" style="width: 90%"/></td>
			</tr>
		</c:forEach>	
	</table>
    </div>
	<div class="buttonDiv">
		<button type="button" class="btn btn-info" id="btn-add" onclick="javascript:addRow();"><i class="icon-white icon-plus-sign"></i> 添加一行</button>
		<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-cacel" type="button" id="btn-cacel"  onclick="javascript:history.go(-1);" ><i class="icon icon-repeat"></i>返回</button>
	 </div>
	 
</form>
</div>
 <script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;
			save();				
			return false;	
		}
	});
})
</script>
</body>
</html>