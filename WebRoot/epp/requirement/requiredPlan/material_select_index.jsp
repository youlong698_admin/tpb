<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	 
        <script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
	    <script type="text/javascript">
	  	 var api = frameElement.api, W = api.opener;
	    
	      function switchTab(url){
	         $('#iframe').attr('src',url);
	      }
	    </script>
	
	</head>
	<body>
	 <div class="page-content">

         <!-- /.page-header -->

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">

                             <ul class="nav nav-tabs" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="switchTab('viewMaterialInfoIndex_materialTree.action');" >新需求计划</a>
                                 </li>
                                 <!--  
                                 <li>
                                     <a data-toggle="tab" onClick="switchTab('viewRequiredPlanPro_requiredPlan.action');" >历史需求计划</a>
                                 </li>
                                -->

                             </ul>

                             <div class="tab-content">
                                 <div id="smsj" >
                                     <iframe src="viewMaterialInfoIndex_materialTree.action"  id="iframe"  frameborder=0 scrolling="yes" width="100%"></iframe>
                                 </div>

                             </div>
                         </div>

                     </div>
                 </div>

                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>
    <script type="text/javascript">
     $(function (){	    
	        var height=window.screen.availHeight*0.85;
	        $('#iframe').height(height);
	    });
    </script>
</body>

</html>