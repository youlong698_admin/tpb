<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>计划信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script type="text/javascript">
	var api = frameElement.api, W = api.opener;
		
	</script>
</head>
<body >
<form id="cx" method="post" action="">
	
			<table class="table_ys1">
    				<tr>
		          		<td  colspan="4" class="Content_tab_style_05">计划信息</td>
		        	</tr>
		    		<tr>
						<td width="15%" class="Content_tab_style1">计划编号：</td>
						<td width="85%" class="Content_tab_style2" colspan="3">
							${requiredMaterial.rmCode }&nbsp;
						</td>
					</tr>
					<tr>
					    <td width="15%" class="Content_tab_style1">计划名称：</td>
						<td width="35%" class="Content_tab_style2">
							${requiredMaterial.rmName }
						</td>
						<td width="15%" class="Content_tab_style1">计划类型：</td>
						<td width="35%" class="Content_tab_style2">
							${requireTypeCn }
						</td>
					</tr>
					<tr>
						<td width="10%" class="Content_tab_style1" nowrap>估算总额：</td>
						<td width="30%" class="Content_tab_style2">
							<fmt:formatNumber value="${requiredMaterial.totalBudget}" pattern="#,##0.00"/>
						</td>
						<td class="Content_tab_style1" nowrap>填报日期：</td>
						<td class="Content_tab_style2">
							<fmt:formatDate value="${requiredMaterial.writeDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
						</td>
				    </tr>
					<tr>
						<td class="Content_tab_style1" nowrap>采购组织：</td>
						<td class="Content_tab_style2">
							${purchaseDeptName }
						</td>
						<td class="Content_tab_style1" nowrap>填报人：</td>
						<td class="Content_tab_style2">
							${writerCn }
						</td>
					</tr>
					<tr>
						<td class="Content_tab_style1" nowrap>填报单位：</td>
						<td class="Content_tab_style2">
							${deptName }
						</td>
						<td class="Content_tab_style1" nowrap></td>
						<td class="Content_tab_style2">
							
						</td>
					</tr>
					<tr>
						<td class="Content_tab_style1">附件列表：</td>
						<td class="Content_tab_style2" colspan="3">
							<c:out value="${requiredMaterial.attachmentUrl}" escapeXml="false"/>
						</td>
					</tr>
			</table>
		<table class="table_ys1">
		<tr>
         <td  colspan="12" class="Content_tab_style_05">计划明细</td>
       	</tr>
		<tr align="center" class="Content_tab_style_04">
			<th width="3%" nowrap>序号</th>
			<th width="8%" nowrap>编码 </th>
			<th width="18%" nowrap>名称 </th>
			<th width="18%" nowrap>规格型号 </th>
		    <th width="5%" nowrap>单位</th>
			<th width="5%" nowrap>数量</th>
			<th width="6%" nowrap>上次采购价</th>
			<th width="6%" nowrap>估算单价</th>
			<th width="6%" nowrap>估算总价</th>
			<th width="6%" nowrap>交货时间</th>
			<th width="5%" nowrap>交货地点</th>
			<th width="13%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredMaterialDetail" varStatus="status">
			<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>${requiredMaterialDetail.buyCode}</td>
				
				<td>${requiredMaterialDetail.buyName}</td>
				<td>${requiredMaterialDetail.materialType}</td>
				<td>${requiredMaterialDetail.unit}</td>
				<td align="right">${requiredMaterialDetail.amount}</td>
				<td align="right">${requiredMaterialDetail.newPrice}</td>
				<td align="right"><fmt:formatNumber value="${requiredMaterialDetail.estimatePrice}" pattern="##0.00"/></td>
				<td align="right"><fmt:formatNumber value="${requiredMaterialDetail.estimateSumPrice}" pattern="##0.00"/></td>
				<td><fmt:formatDate value="${requiredMaterialDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></td>
				<td>${requiredMaterialDetail.deliverPlace}</td>
				<td>${requiredMaterialDetail.remark }</td>
			</tr>
		</c:forEach>
	</table>
</form>
		
</body>
</html>