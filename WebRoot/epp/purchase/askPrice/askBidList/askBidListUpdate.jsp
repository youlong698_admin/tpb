<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>编制询价计划</title>
	<link href="<%=path%>/common/jQuery/chosen/1.1.0/chosen.min.css" rel="stylesheet"/>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
$(function(){
    <c:if test="${supplierType=='01'}">
	//移除供应商
	$('.search-choice').live('click',function(){
		var that=$(this);
		var supplierId=that.attr('supplier-id');
		var type=that.attr('type');
		var supplierIds=$("#supIds").val();
		var supTypes=$("#supTypes").val();
		$("#supIds").val(supplierIds.replace(","+supplierId+",",","));
		$("#supTypes").val(supTypes.replace(","+type+",",","));
		that.remove();
	});
	//此方法执行是如果编辑页面，需要加载显示供应商
	doReturn();
	</c:if>
})

function openSupplierWindow(){
    var ul =$('#supIds').val();
    var rcId =$('#rcId').val();
	 if(ul.length==0 || ul==","){
		 ul=-1;
	 }else{
	 	ul=ul.substring(1,ul.lastIndexOf(","));
	 }
	 createdetailwindow_choose("选择供应商","viewInviteSupplier_supplierSelect.action?ul="+ul+"&rcId="+rcId,1);
}
function doReturn(){
    var reValue=$('#returnVals').val();
    $("#supIds").val("");
    $("#supTypes").val("");
    $("#suppliersChosen").html("");
	var supIds=",",supTypes=",";
		if(reValue!=null&&reValue!=""){
			var mArr = reValue.split(",");
			for(var i=0;i<mArr.length;i++){
				if(mArr[i]!=""){
					nArr = mArr[i].split(":");
					supIds += nArr[0]+",";
					supTypes += nArr[2]+",";
					$('<li class="search-choice" supplier-id="'+nArr[0]+'" type="'+nArr[2]+'"><span>'+nArr[1]+'</span><a class="search-choice-close" ></a></li>')
							.appendTo('#suppliersChosen');
				}
			}
		}
		$("#supIds").val(supIds);
		$("#supTypes").val(supTypes);
} 
var num = "${fn:length(businessResponseItemsList)}";
function addRow(){
    var tr=document.getElementById("businessTable").insertRow();
	num++;
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
    var cell3 = tr.insertCell();
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this)'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML="<input type='text' name='briList["+num+"].responseItemName' id='responseItemName_"+num+"' value=''>" ;
	cell3.innerHTML="<input type='text' id='responseRequirements_"+num+"' value='' name='briList["+num+"].responseRequirements'/>" ;
	
	cell1.align="center";
}
	//删除一行
	function deleteRow(obj){
		$(obj).parent().parent().remove(); 
	}
</script>
  
</head>
 
<body>
<form class="defaultForm" id="save_AskList" name="save_AskList" method="post" action="updateAskBidList_askBidList.action">
<input type="hidden" name="supIds" id="supIds" value="${supplierIds }" />
<input type="hidden" name="supTypes" id="supTypes" value="${supplierTypes }" />
<input type="hidden" name="askBidList.rcId" id="rcId" value="${askBidList.rcId}" />
<input type="hidden" name="askBidList.ablId" id="ablId" value="${askBidList.ablId}" />
<input type="hidden" name="askBidList.writer" id="writer" value="${askBidList.writer }" />
<input type="hidden" name="askBidList.writeDate" id="writeDate" value="<fmt:formatDate value="${askBidList.writeDate}" type="date" pattern='yyyy-MM-dd HH:mm:ss'/>" />
<input type="hidden" name="askBidList.remark1" id="remark1" value="${askBidList.remark1 }" />
<input type="hidden" name="askBidList.remark2" id="remark2" value="${askBidList.remark2 }" />
<input type="hidden" name="askBidList.remark3" id="remark3" value="${askBidList.remark3 }" />
<input type="hidden" name="askBidList.remark4" id="remark4" value="${askBidList.remark4 }" />
<input type="hidden" id="returnVals" name="returnVals" value="${returnVals}"/>
<!-- 防止表单重复提交 -->
<s:token/>		

<div class="Conter_Container">
    <div class="row-fluid">
    	<table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">询价计划</td>
    	    </tr>
    	     <tr>
				<td width="15%" class="Content_tab_style1">项目负责人：</td>
				<td width="35%" class="Content_tab_style2">
					<input class="Content_input_style1" datatype="*" nullmsg="项目负责人不能为空！" type="text" id='responsibleUser' name="askBidList.responsibleUser" value="${askBidList.responsibleUser }"/><font color="#FF0000">*</font>
					<div class="info"><span class="Validform_checktip">项目负责人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">负责人手机号：</td>
				<td width="35%" class="Content_tab_style2">
				    <input class="Content_input_style1" datatype="m" nullmsg="负责人手机号不能为空！" placeholder="手机号用于接收项目的供应商报名及回标提醒信息" type="text" id='responsiblePhone' name="askBidList.responsiblePhone" value="${askBidList.responsiblePhone }"/><font color="#FF0000">*</font>
					<div class="info"><span class="Validform_checktip">负责人手机号不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">询价时间：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" name="askBidList.askDateStr" datatype="*" nullmsg="询价时间不能为空！" id="askBidList.askDate" class="Wdate" 
                        value="<fmt:formatDate value="${askBidList.askDate}" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm', minDate:'%y-%M-%d %H:%m',maxDate:'#F{$dp.$D(\'askBidList.returnDate\')}' })" /><font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">询价时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">报价截止时间：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" name="askBidList.returnDateStr" datatype="*" nullmsg="报价截止时间不能为空！" id="askBidList.returnDate" class="Wdate" 
                        value="<fmt:formatDate value="${askBidList.returnDate}" pattern="yyyy-MM-dd HH:mm" />" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm', minDate:'#F{$dp.$D(\'askBidList.askDate\')}',maxDate:'#F{$dp.$D(\'askBidList.decryptionDate\')}' })" /><font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">报价截止时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">报价解密时间：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" name="askBidList.decryptionDateStr" datatype="*" nullmsg="报价解密时间不能为空！" id="askBidList.decryptionDate" class="Wdate" 
                         value="<fmt:formatDate value="${askBidList.decryptionDate }" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'askBidList.returnDate\')}' })" /><font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">报价解密时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">报价类型：</td>
				<td width="35%" class="Content_tab_style2">
				    <select name="askBidList.priceType" id="priceType">
				       <c:forEach items="${priceTypeMap}" var="map">
				          <option value="${map.key }" <c:if test="${askBidList.priceType==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>	
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价列类型：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   <select name="askBidList.priceColumnType" id="priceColumnType">
				       <c:forEach items="${priceColumnTypeMap}" var="map">
				          <option value="${map.key }" <c:if test="${askBidList.priceColumnType==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>
				</td>
			</tr>			
			<tr>
				<td width="15%" class="Content_tab_style1">备注：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   <textarea name="askBidList.remark" id="askBidList.remark"  rows="2" class="Content_input_style2" >${askBidList.remark }</textarea>
				</td>
			</tr>
			<c:if test="${supplierType=='01'}">
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">询价供应商</td>
    	      </tr>
			  <tr>
				<td width="15%" class="Content_tab_style1">邀请供应商：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					
						<div class="chosen-container-multi">
									<ul class="chosen-choices" id="suppliersChosen">
									</ul>
								</div>
								<div id="chosen-img">
									<img src="<%=basePath%>/images/select.gif" title="选择供应商"
										onclick="openSupplierWindow();"/>
								</div>
				</td>
			  </tr>
			 </c:if>
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">商务响应项</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="businessTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>操作<img src="<%=basePath%>/images/add.gif" title="添加商务响应项"
										onclick="addRow();"/></th>
						<th width="30%" nowrap>响应项名称</th>
						<th width="60%" nowrap>响应项要求 </th>
					</tr>
					<c:choose>
					   <c:when test="${fn:length(businessResponseItemsList)==0}">
					           <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="0"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_0' name='briList[0].responseItemName'  value='付款方式'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_0' value='' name='briList[0].responseRequirements' /> 
								</td>
								</tr>
								<tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="1"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_1' name='briList[1].responseItemName'  value='交货时间'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_1' value='' name='briList[1].responseRequirements' /> 
								</td>
								</tr>
					            <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="2"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_2' name='briList[2].responseItemName'  value='发票要求'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_2' value='' name='briList[2].responseRequirements' /> 
								</td>
								</tr>
								<tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="3"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_3' name='briList[3].responseItemName'  value='收货地'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_3' value='' name='briList[3].responseRequirements' /> 
								</td>
								</tr>
					   </c:when>
					   <c:otherwise>
					     <c:forEach items="${businessResponseItemsList}" var="businessResponseItems" varStatus="status">
							 <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="${status.index}"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_${status.index}' name='briList[${status.index}].responseItemName'  value='${businessResponseItems.responseItemName}'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_${status.index}' value='${businessResponseItems.responseRequirements}' name='briList[${status.index}].responseRequirements' /> 
								</td>
								</tr>
						</c:forEach>
					   </c:otherwise>
					</c:choose>
					
    	            </table>
    	         </td>
    	      </tr>
			  		
        </table>
        
        <div class="buttonDiv">
		<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
     </div>  
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeCheck:function(curform){
			<c:if test="${supplierType=='01'}">
			var inviteSup = $("#supIds").val();
			if(inviteSup==','){
				showMsg("alert","温馨提示：邀请供应商不能为空！");
				return false;
			}else
			{
				return true;
			}
			</c:if>
			
		}
	});
})
</script>
</body>
</html>