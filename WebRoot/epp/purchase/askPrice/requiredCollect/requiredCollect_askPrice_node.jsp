<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>询价项目流程监控信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<link href="<%=path%>/style/bidMonitor.css" rel="stylesheet" type="text/css" />
<script language="javaScript">
  var rcId="${requiredCollect.rcId}";
  var divName="bidNode${requiredCollect.serviceStatus}";
  var serviceStatus=${requiredCollect.serviceStatus};
  $(function (){
  	<c:if test="${isBidAbnomal}">
     //消息提示
    if(serviceStatus==24){
  	    //获取应标响应数
  	     var count=ajaxGeneral("getAskReceiveBulletinForNode_askReceivedBulletin.action","rcId="+rcId,"text");
	     $("#bidNode22").append('<span class="XxtsNum">'+count+'家</span>');
  	     //debugger;
  	     //获取没有查看的问题解答个数
	 	count=ajaxGeneral("getBidCommunicationInfoForNode_bidCommunicationInfo.action","rcId="+rcId,"text");
	     $("#bidNode24").append('<span class="XxtsNum">'+count+'条</span>');
  	    //获取标前澄清数  	    
  	}else{
	    var msg=ajaxGeneral("messageBidMonitor_askBidList.action","rcId="+rcId,"text");
	    if(msg=="1"){
	       $("#"+divName).append('<span class="num">1</span>');
	     }
	 }
	</c:if>
  });
   function doClick(title,url,bidMonitorStyle){
      if(url.indexOf("?")!=-1) 
      url=url+"&rcId="+rcId;
      else
      url=url+"?rcId="+rcId;
      if(bidMonitorStyle=="overState"){
        createdetailwindow(title,url,0);
      }else if(bidMonitorStyle=="execState"){
        createdetailwindow(title,url,0);
      }else {
        //createdetailwindow(title,url,0);
      }
   }
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12" id="content">
				<div class="row-fluid" style="padding-top: 20px">
					<div class="span12" id="div-table-container">
				
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								
								<!--内容-->
								<div class="row">
								<div class="row header orange">
									<div class="col-md-8"><i class="ace-icon fa fa-file-o"></i>询价项目相关信息</div>
									<div class="col-md-2" align="right"><button class="btn btn-warning" onclick="location.href='viewRequiredCollectNode_purchaseAsk.action?requiredCollect.rcId=${requiredCollect.rcId}';" id="btn-danger"> <i class="icon-white icon-refresh"></i>刷新</button>
									<!-- <button class="btn btn-info" onclick="location.href='viewRequiredCollect_purchaseAsk.action';" id="btn-danger"><i class="icon-white icon-repeat"></i>返回</button>--></div> 
								</div>
								<div class="row">
									<div class="col-md-6">
										<dl>
											<dt>
												项目编号：
											</dt>
											<dd>
												<a href="javascript: createdetailwindow('采购数据','viewRequiredCollectDetail_purchaseAsk.action?requiredCollect.rcId=${requiredCollect.rcId}',1)">${requiredCollect.bidCode }</a>
											</dd>
										</dl>
									</div>
									<div class="col-md-6">
										<dl>
											<dt>
												项目名称：
											</dt>
											<dd>
												${requiredCollect.buyRemark }
											</dd>
										</dl>
									</div>
								</div>
									</div>
                                     
									<div class="row">
									  <div class="header blue">
			                               <i class="ace-icon fa fa-sitemap"></i>采购流程引导<span style="font-size: 13px">（<span style="background:#CCD4CD;" >&nbsp;&nbsp;&nbsp;&nbsp;</span><font color="#CCD4CD">灰色代表已办流程</font>，<span style="background:#66CD00;" >&nbsp;&nbsp;&nbsp;&nbsp;</span><font color="#66CD00">绿色代表在办流程</font>，<span style="background:#6CA6CD;" >&nbsp;&nbsp;&nbsp;&nbsp;</span><font color="#6CA6CD">蓝色代表未办流程</font>）</span>
		                              </div>
		                              </div>
		                              <div class="row">
										<div class="title">
											采购过程
										</div>
										<div class="cardlist">
											<c:forEach items="${bidProcessLogList }" var="bidProcessLog" varStatus="status">
											                                            <c:choose>
												<c:when test="${bidProcessLog.completeDate==null&&bidProcessLog.bplId!=null}">
													<c:set var="bidMonitorStyle" value="execState" />
												</c:when>
												<c:when test="${bidProcessLog.completeDate!=null}">
													<c:set var="bidMonitorStyle" value="overState" />
												</c:when>
												<c:otherwise>
													<c:set var="bidMonitorStyle" value="futureState" />
												</c:otherwise>
											</c:choose>
											<div class="card ${bidMonitorStyle }" id="bidNode${bidProcessLog.bidNode}"
												onclick="doClick('${bidProcessLog.nodeName}','${bidProcessLog.url}','${bidMonitorStyle }')"
												>
												<img src="${bidProcessLog.img}" />
												<div>
													<span class="bidNode">${ status.index + 1} ${bidProcessLog.nodeName}</span>
															<c:if test="${bidProcessLog.completeDate!=null}">
															<span>耗时${bidProcessLog.day}天(<fmt:formatDate
																	value="${bidProcessLog.completeDate}"
																	pattern="yyyy/MM/dd" />)
															</span>
															</c:if>
												</div>												
											</div>
											</c:forEach>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>