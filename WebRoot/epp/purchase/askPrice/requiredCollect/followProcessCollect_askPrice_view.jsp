<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>查看采购流程</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" href="<%=path%>/style/timeline/style.css"
			type="text/css" media="all" />
		
		<script language="javaScript">
		var api = frameElement.api, W = api.opener;
		function doClick(rcId){
		   api.close();
		   W.window.location.href="viewRequiredCollectNode_purchaseAsk.action?requiredCollect.rcId="+rcId;
		}
		</script>
	</head>
	<body>
	                          	
        <div class="record">	     	
					<c:set var="data_tmp" value=""/>
					<dl class="recordList">	
						<dd>
						<p class="record_Time"></p>
						<div class="record_jd"></div>
						<div class="record_Info">
							<div class="record_arr">◆</div>
							<div class="record_Intro_curr">
								<p><span>当前节点:</span>${requiredCollect.serviceStatusCn }</p>
								<c:if test="${not empty type}">
								<p class="record_Intro_Title_curr"><span>节点内容：</span><a href="#">${purchaseRecordLog.operateContent } </a></p>
							    </c:if>
							    <c:if test="${empty type}">
								<p class="record_Intro_Title_curr" onclick="doClick('${requiredCollect.rcId}');"><span>节点内容：</span><img src="images/icon_cancel.gif" title="点击进入流程引导界面" style="cursor:hand"/><a href="#">${purchaseRecordLog.operateContent } </a></p>
							    </c:if>
							</div>
							
						</div>
						<div class="clear"></div>
					</dd>
					<c:forEach items="${listValue}" var="purchaseRecordLog" > 
					<c:set var="data"><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="yyyy-MM-dd"/></c:set>
					    <c:if test="${data_tmp==''}">
						<dt>
							<p><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="MM-dd"/></p>
						</dt>						
					    <c:set var="data_tmp" value="${data}"/>
						</c:if>	
						 <c:if test="${data_tmp!=''&&data_tmp!=data}">	
						 <dt>
							<p><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="MM-dd"/></p>
						</dt>						
					    <c:set var="data_tmp" value="${data}"/>	
						 </c:if>		
						<dd>
						<p class="record_Time"><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="HH:mm:ss"/></p>
						<div class="record_jd"></div>
						<div class="record_Info">
							<div class="record_arr">◆</div>
							<div class="record_Intro">
								<p><span>流程节点:</span>${purchaseRecordLog.bidNode }</p>
								<p class="record_Intro_Title"><span>节点内容：</span><font color="#00848E">${purchaseRecordLog.operateContent }</font> </p>
							</div>
							
						</div>
						<div class="clear"></div>
					</dd>
					</c:forEach>								
				</dl>
			</div>


	</body>
</html>