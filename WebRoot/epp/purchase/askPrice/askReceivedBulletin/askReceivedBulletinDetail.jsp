<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>应标响应</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							<c:if test="${supplierType=='00'}">
							  <div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
									<tr>
						    	       <td colspan="4" class="Content_tab_style_td_head">公开报名查看</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="60%" nowrap>
												报名供应商
											</th>
											<th width="20%" nowrap>
												来源分类
											</th>
											<th width="15%" nowrap>
												报名日期
											</th>
										</tr>
										<c:forEach items="${inviteSupplierList}" var="inviteSupplier" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${inviteSupplier.supplierName}</td>	
												<td>
												   <c:choose>
												       <c:when test="${inviteSupplier.sourceCategory==1}">核心供应商</c:when>
												       <c:when test="${inviteSupplier.sourceCategory==1}">盘外供应商</c:when>
												       <c:otherwise>公共供应商库</c:otherwise>
												   </c:choose>
												</td>
												<td><fmt:formatDate value="${inviteSupplier.writeDate}" type="both" pattern="yyyy-MM-dd" /></td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
							</c:if>
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
									<tr>
						    	       <td colspan="5" class="Content_tab_style_td_head">应标查看</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="40%" nowrap>
												应标供应商
											</th>
											<th width="20%" nowrap>
												应标人
											</th>
											<th width="20%" nowrap>
												联系电话
											</th>
											<th width="15%" nowrap>
												应标日期
											</th>
										</tr>
										<c:forEach items="${askReceivedBulletinList}" var="askReceivedBulletin" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${askReceivedBulletin.supplierName}</td>												
												<td>${askReceivedBulletin.receivedName}</td>
												<td>${askReceivedBulletin.receivedTel}</td>
												<td><fmt:formatDate value="${askReceivedBulletin.receivedDate}" type="both" pattern="yyyy-MM-dd" /></td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>