<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>网上报价</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 
 function doView(bpId){
	   createdetailwindow("查看供应商报价","viewAskBidPriceResponeDetail_askBidPriceResponse.action?bidPrice.bpId="+bpId,1);
	}
  function parityPrice(rcId){
	   createdetailwindow("供应商比价","viewParityPrice_askBidPriceResponse.action?rcId="+rcId,1);
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row-fluid">
								<div class="span12">
								<c:if test="${isEcryptionTime}">
							     <div class="btn-toolbar">
							       <button type="button" class="btn btn-primary" id="btn-parity" onclick="parityPrice(${rcId})"><i class="icon-white icon-indent-left"></i>比价</button>
				                 </div>
				                 </c:if>
				                 <c:if test="${!isEcryptionTime}">
				                       <div class="alert alert-block alert-danger">
											未到报价解密时间，报价解密时间为：<fmt:formatDate value="${askBidList.decryptionDate}" pattern="yyyy-MM-dd HH:mm" />
										</div>
				                    </c:if>
                             </div>
                           </div>
							<div class="row">
							  <div class="span12" id="content">
								<div class="row">
									<div class="col-xs-12">
										<div class="alert alert-block alert-danger">
											注意：报价没有解密之前，价格采取保密机制。
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="6" class="Content_tab_style_td_head">历史报价信息</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="35%" nowrap>
												供应商
											</th>
											<th width="15%" nowrap>
												报价时间
											</th>
											<th width="25%" nowrap>
												总价
											</th>
											<th width="25%" nowrap>
												税率
											</th>
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${listValue}" var="bidPrice" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${bidPrice.supplierName}</td>
												<td><fmt:formatDate value="${bidPrice.writeDate}" type="both" pattern="yyyy-MM-dd HH:mm" /></td>
												   <c:choose>
												       <c:when test="${empty bidPrice.totalPrice}">
									                       <td>   *****</td><td>
									                       <span class="text-muted">价格保密</span></td> 
									                       <td>   *****</td><td>
												       </c:when>
													   <c:otherwise>  
													    <td><span class="text-muted"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></span></td> 
													    <td>${bidPrice.taxRate}</td> 
													    <td><button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidPrice.bpId})'><i class="icon-white icon-bullhorn"></i></button></td>		
											         </c:otherwise>
												   </c:choose>
												</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>