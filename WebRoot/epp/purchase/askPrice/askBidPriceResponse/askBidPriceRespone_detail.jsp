<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>供应商报价信息查看</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>		
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
    </script>
</head>
<body>
<form class="defaultForm" id="supInfo" name="" method="post" action="">
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable">			
    	    <tr>
    	       <td colspan="8" class="Content_tab_style_td_head" align="center">报价信息</td>
    	    </tr>
    	     <tr>
		    <td colspan="8" style="font-size:14px;"><font color="red"><b>1.本次报价为：“${askBidList.priceTypeCn }”<br></b></font></td>
		    </tr>
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="10%" nowrap>名称 </th>
				<th width="10%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>${askBidList.priceColumnTypeCn }</th>
				<th width="100px" nowrap>小计</th>
			</tr>
			<c:forEach items="${bpdList}" var="bidPriceDetail" varStatus="status">
				<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
					<td>
					   ${status.index+1}
					</td>
					<td>
					    ${bidPriceDetail.requiredCollectDetail.buyCode}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.buyName}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.materialType}
					</td>
					<td>
					   ${bidPriceDetail.requiredCollectDetail.unit}
				    </td>
					<td align="right">
				     ${bidPriceDetail.requiredCollectDetail.amount}
					 </td>
				    <td align="right">
				       <fmt:formatNumber value="${bidPriceDetail.price}" pattern="#,##0.00"/>
				    </td>
					<td align="right">
					   <fmt:formatNumber value="${bidPriceDetail.price*bidPriceDetail.requiredCollectDetail.amount}" pattern="#,##0.00"/>
					</td>
				</tr>
			</c:forEach>
				<tr>
					<td colspan="2" align="center">总价：</td>
					<td colspan="6">
					    <fmt:formatNumber value="${bidPrice.totalPrice }" pattern="#,##0.00"/>
					</td>
				</tr>		
				 <tr>
					<td colspan="2" align="center">税率（%）：</td>
					<td colspan="6">
						${bidPrice.taxRate }
					</td>
				</tr>				  		
        </table>
        <c:if test="${fn:length(bbrList)>0}">
        <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
    	    </tr>
			 <tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="10%" nowrap>序号</th>
				<th width="20%" nowrap>响应项名称</th>
				<th width="30%" nowrap>响应项要求 </th>
				<th width="40%" nowrap>我的响应 </th>
			</tr>
			<c:forEach items="${bbrList}" var="bidBusinessResponse" varStatus="status">
			 <tr>
				<td align="center">
				   ${status.index+1}
				</td>
				<td>
				    ${bidBusinessResponse.responseItemName}
				</td>
				<td>
				   ${bidBusinessResponse.responseRequirements}
				</td>
				<td>
				   ${bidBusinessResponse.myResponse}
				 </td>
				</tr>
		   </c:forEach>	  		
        </table>
        </c:if>   
     </div>  
	</div>
</form>
	</body>
</html>