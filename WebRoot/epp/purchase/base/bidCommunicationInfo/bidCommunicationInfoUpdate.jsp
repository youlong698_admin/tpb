<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>问题解答</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 var rcId=${rcId};
 $(function(){
    $("#btn-next").click(function(){
	   location.href="updateBidCommunicationInfoMonitor_bidCommunicationInfo.action?rcId="+rcId;
	});
	})
	function doView(bciId){
	   createdetailwindow("查看问题解答","viewBidCommunicationInfoDetail_bidCommunicationInfo.action?bidCommunicationInfo.bciId="+bciId,1);
	}
	function doUpdate(bciId){
	   createdetailwindow("回复问题解答","updateBidCommunicationInfoInit_bidCommunicationInfo.action?bidCommunicationInfo.bciId="+bciId,1);
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<c:if test="${isNext}">
						<div class="row-fluid">
							<div class="span12">
								<div class="btn-toolbar">
									<button type="button" class="btn btn-success" id="btn-next">
										<i class="icon-white icon-resize-small"></i> 执行下一步
									</button>
								</div>
							</div>
						</div>
					</c:if>
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="6" class="Content_tab_style_td_head">问题解答</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="20%" nowrap>
												供应商名称
											</th>
											<th width="20%" nowrap>
												问题内容
											</th>
											<th width="20%" nowrap>
												提问日期
											</th>
											<th width="20%" nowrap>
												回复人
											</th>
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${bidCommunicationInfoList}" var="bidCommunicationInfo" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${bidCommunicationInfo.questionerName}</td>	
												<td>${bidCommunicationInfo.questionContent}</td>	
												<td><fmt:formatDate value="${bidCommunicationInfo.questionDate}" type="both" pattern="yyyy-MM-dd" /></td>
												<td>${bidCommunicationInfo.answerName}</td>	
												<td>
												   <c:if test="${empty bidCommunicationInfo.answerName}">
												     <button class='btn btn-mini btn-primary' type="button"  onclick='doUpdate(${bidCommunicationInfo.bciId})'><i class="icon-white icon-hand-right"></i></button>
												   </c:if>
												   <c:if test="${not empty bidCommunicationInfo.answerName}">
												     <button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidCommunicationInfo.bciId})'><i class="icon-white icon-bullhorn"></i></button>
												   </c:if>
												</td>	
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>