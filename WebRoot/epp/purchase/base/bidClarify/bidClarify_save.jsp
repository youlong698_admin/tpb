<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>新增标前澄清</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%= path %>/common/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="<%= path %>/common/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%= path %>/common/kindeditor/lang/zh_CN.js"></script> 
   <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
 var api = frameElement.api, W = api.opener, cDG;
  <c:if test="${message!=null}">
		   window.onload=function(){ 
		   api.get("dialog").window.location.reload();
		   api.close();
		  }
  </c:if>
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="BidClarify"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
 var fileNameData=[];
 var uuIdData=[];
 var fileTypeData=[];
 var attIdData=[];
  			KindEditor.ready(function(K) {
				var editor1 = K.create('textarea[id="clarifyContent"]', {
					cssPath : '<%=path %>/common/kindeditor/plugins/code/prettify.css',
				    uploadJson : '<%=path %>/common/kindeditor/jsp/upload_json.jsp',
				    fileManagerJson : '<%=path %>/common/kindeditor/jsp/file_manager_json.jsp',
					width:'95%',
					height:'250px',
					allowFileManager : true,
					items : [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons'],					
					afterCreate : function() {
						var self = this;
						 $('#btn-save').click(function () {
							self.sync();
							$("#fileNameData").val(fileNameData);
						    $("#uuIdData").val(uuIdData);
							$("#fileTypeData").val(fileTypeData);
							$("#attIdData").val(attIdData);
						    var action = "saveBidClarify_bidClarify.action?status=1";
							document.forms[0].action = action;
							document.forms[0].submit();
	                    });
						 $('#sendButton').click(function () {
							self.sync();
							var clarifyContent = document.getElementById("clarifyContent").value;
							if(clarifyContent!=''){
								lhgdialog.confirm("温馨提示：您确定要正式发布吗？",function(){
								    $("#fileNameData").val(fileNameData);
								    $("#uuIdData").val(uuIdData);
								    $("#fileTypeData").val(fileTypeData);
								    $("#attIdData").val(attIdData);
								    var action = "saveBidClarify_bidClarify.action?status=0";
							        document.forms[0].action = action;
							        document.forms[0].submit();
								},function(){},api);
							}else{
								showMsg("alert","温馨提示：澄清内容不能为空！");
								return ;
							}
	                    });
					}
				});
			});
		
</script>
</head>
 
<body>
<form id="noticeEdit" class="defaultForm" name="bidClarifyEdit" method="post" action="">
<input type="hidden" name="bidClarify.bcId" value=""/>
<input type="hidden" name="bidClarify.rcId" value="${rcId}"/>
<input type="hidden" name="bidClarify.attIds" id="attIds"/>
<input type="hidden" name="bidClarify.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="bidClarify.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="bidClarify.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="bidClarify.attIdData" id="attIdData" value=""/>
<s:token/>			
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table width="98%" border="0" align="center"  class="table_ys2">
        	<tr>
          		<td  colspan="2" class="Content_tab_style_td_head">标前澄清</td>
        	</tr>
			<tr>
				<td class="Content_tab_style1">澄清标题：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1"  name="bidClarify.clarifyTitle" id="bidClarify.clarifyTitle" value="" />&nbsp;<font color="#ff0000">*</font>
				     <div class="info"><span class="Validform_checktip">澄清标题不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			   </td>
			</tr>
			<tr>
				<td class="Content_tab_style1">澄清内容：</td>
				<td class="Content_tab_style2"> 
					<textarea name="bidClarify.clarifyContent" id="clarifyContent"
						class="Content_input_style2" ></textarea>&nbsp;<font color="#ff0000">*</font>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
        </table>
        <div class="buttonDiv">
		<button class="btn btn-success"  type="button" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-success" type="button" id="sendButton" ><i class="icon-white icon-bullhorn"></i>发布</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
	</div>
</div>
</form>
</body></html>