<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>标前澄清</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 var rcId=${rcId};
 $(function(){
    $("#allcheck").bind("click", function () {
                $("[name = bcId]:checkbox").each(function () {
                    $(this).attr("checked", !$(this).attr("checked"));
                });
            });
    $("#btn-add").click(function(){
	   createdetailwindow("新增标前澄清","saveBidClarifyInit_bidClarify.action?rcId="+rcId,1);
	});
	$("#btn-edit").click(function(){
	  var bcId,count=0;
	  $("input[name='bcId']:checkbox:checked").each(function(){ 
        bcId=$(this).val();
        count++;
      });
       if(count < 1){
      		showMsg("alert","温馨提示：请选择将要修改的信息！");
      	}else if(count > 1){
	    	showMsg("alert","温馨提示：只能选择一条信息修改！");
	    }else{
	      var status=$("#status"+bcId).val(); 
	      if(status == "0"){
    	    	showMsg("alert","温馨提示:此信息已经正式对供应商发布,不能修改!");
    	    	return false;
    	   }else{
    	        createdetailwindow("编辑标前澄清","updateBidClarifyInit_bidClarify.action?bidClarify.bcId="+bcId,1);
    	   }
	   }
	});
	$("#btn-del").click(function(){
	   var ids="",count=0,bcId,falg=false;
	  $("input[name='bcId']:checkbox:checked").each(function(){ 
	    bcId=$(this).val();
        ids+=$(this).val();
        count++;
        var status=$("#status"+bcId).val(); 
	      if(status == "0") falg=true;
      });
      if(falg){
            showMsg("alert","温馨提示:此信息已经正式对供应商发布,不能修改!");
      }else if(count < 1){
      		showMsg("alert","温馨提示：请选择将要删除的信息！");
      }else{
	        $.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
		    var result = ajaxGeneral("deleteBidClarify_bidClarify.action","ids="+ids);
			   showMsg('success',''+result+'',function(){
			   		window.location.href=window.location.href;							
   					});
		   	 	});
	   }
	});
	})
	function doView(bcId){
	   createdetailwindow("查看标前澄清","viewBidClarifyDetail_bidClarify.action?bidClarify.bcId="+bcId,1);
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
				     <div class="btn-toolbar">
				                    <button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i>新增</button>
							        <button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							        <button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
					 </div>
                           </div>
                           </div>
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="6" class="Content_tab_style_td_head">标前澄清</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												<input type="checkbox" name="allcheck" id="allcheck">
											</th>
											<th width="5%" nowrap>
												序号
											</th>
											<th width="50%" nowrap>
												澄清标题
											</th>
											<th width="20%" nowrap>
												澄清日期
											</th>
											<th width="10%" nowrap>
												状态
											</th>
											<th width="10%" nowrap>
												供应商查看情况
											</th>
										</tr>
										<c:forEach items="${bidClarifyList}" var="bidClarify" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td><input type="checkbox" name="bcId" value="${bidClarify.bcId}"></td>
												<td>${status.index+1}</td>
												<td>${bidClarify.clarifyTitle}</td>	
												<td><fmt:formatDate value="${bidClarify.writeDate}" type="both" pattern="yyyy-MM-dd" /></td>
												<td><input type="hidden" id="status${bidClarify.bcId }" value="${bidClarify.status }"/>${bidClarify.statusCn}</td>	
												<td><button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidClarify.bcId})'><i class="icon-white icon-bullhorn"></i></button></td>	
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>