<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>标前澄清</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
			
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">

								<div class="alert alert-block alert-success">
								        <p>
                                            <strong>
												<i class="icon-ok fa fa-check"></i>
												状态：${bidClarify.statusCn }
												</strong>
                                        </p>
                                   </div>						 
								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
										<tr>
											<td colspan="4" class="Content_tab_style_td_head">
												标前澄清
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												澄清标题：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidClarify.clarifyTitle}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												澄清内容：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidClarify.clarifyContent}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												附件：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												<c:out value="${bidClarify.attachmentUrl}" escapeXml="false"/>
											</td>
										</tr>

									</table>
								</div>
								
								<c:if test="${bidClarify.status=='0'}">
								<br/>
							   <div class="col-xs-12">
							   <table  class="table_ys1" id="listtable">	
		                            <tr>
											<td colspan="5" class="Content_tab_style_td_head">
												供应商澄清响应
											</td>
										</tr>
									<tr id="tdNum" align="center" class="Content_tab_style_04">
										<th width="5%" nowrap>序号</th>
										<th width="50%" nowrap>供应商名称</th>
										<th width="10%" nowrap>查看人 </th>
										<th width="10%" nowrap>查看时间 </th>
										<th width="20%" nowrap>备注</th>
									</tr>
									<c:forEach items="${bidClarifyResponsesList}" var="bidClarifyResponses" varStatus="status">
										<tr  class="input_ys1">
											<td>${status.index+1}
											</td>
											<td>
											    ${bidClarifyResponses.supplierName}
											</td>
											<td>
											   ${bidClarifyResponses.viewr}
											</td>
											<td>
												<fmt:formatDate value="${bidClarifyResponses.viewDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
											</td>
											<td>${bidClarifyResponses.remark }</td>
										</tr>
									</c:forEach>		
								</table>
							   </div>
							</c:if>
							
							</div>
							
						</div>
					</div>
                    
				</div>
			</div>
		</div>
	</body>
</html>