<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>${title }</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
			
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">

								<div class="alert alert-block alert-success">
								        <p>
                                            <strong>
												<i class="icon-ok fa fa-check"></i>
												公示状态：${bidWinning.statusCn }
												</strong>
                                        </p>
                                   </div>						 
								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
										<tr>
											<td colspan="4" class="Content_tab_style_td_head">
												${title }
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												项目编号：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidWinning.bidCode}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												公示标题：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidWinning.winningTitle}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												公示内容：
											</td>
											<td width="35%" class="Content_tab_style2" colspan="3">
												${bidWinning.winningContent}
											</td>
										</tr>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</body>
</html>