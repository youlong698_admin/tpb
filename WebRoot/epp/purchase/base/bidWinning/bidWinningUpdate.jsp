<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>${title }</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%= path %>/common/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="<%= path %>/common/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%= path %>/common/kindeditor/lang/zh_CN.js"></script> 
<script language="javaScript">
 var api = frameElement.api, W = api.opener, cDG;
//附件需要添加的信息
  			KindEditor.ready(function(K) {
				var editor1 = K.create('textarea[id="winningContent"]', {
					cssPath : '<%=path %>/common/kindeditor/plugins/code/prettify.css',
				    uploadJson : '<%=path %>/common/kindeditor/jsp/upload_json.jsp',
				    fileManagerJson : '<%=path %>/common/kindeditor/jsp/file_manager_json.jsp',
					width:'95%',
					height:'350px',
					allowFileManager : true,
					items : [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons'],
					afterCreate : function() {
						var self = this;
						 $('#btn-save').click(function () {
							self.sync();
						    var action = "updateBidWinning_bidWinning.action?status=1";
							document.forms[0].action = action;
							document.forms[0].submit();
	                    });
						 $('#sendButton').click(function () {
							self.sync();
							var winningContent = document.getElementById("winningContent").value;
							if(winningContent!=''){
								lhgdialog.confirm("温馨提示：您确定要发布至门户吗？",function(){
								    var action = "updateBidWinning_bidWinning.action?status=0";
							        document.forms[0].action = action;
							        document.forms[0].submit();
								},function(){},api);
							}else{
								showMsg("alert","温馨提示：公告内容不能为空！");
								return ;
							}
	                    });
					}
				});
			});
		
</script>
</head>
 
<body>
<form id="noticeEdit" class="defaultForm" name="noticeEdit" method="post" action="">
<input type="hidden" name="bidWinning.bwId" value="${bidWinning.bwId}"/>
<input type="hidden" name="bidWinning.rcId" value="${bidWinning.rcId}"/>
<input type="hidden" name="bidWinning.status" value="${bidWinning.status}"/>
<input type="hidden" name="bidWinning.writer" value="${bidWinning.writer}"/>
<input type="hidden" name="bidWinning.publisher" value="${bidWinning.publisher}"/>
<input type="hidden" name="bidWinning.buyWay" value="${bidWinning.buyWay}"/>
<input type="hidden" name="bidWinning.writeDate" value="<fmt:formatDate value="${bidWinning.writeDate}" pattern="yyyy-MM-dd" />"/>
<input type="hidden" name="bidWinning.publishDate" value="<fmt:formatDate value="${bidWinning.publishDate}" pattern="yyyy-MM-dd" />"/>
<s:token/>			
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table width="98%" border="0" align="center"  class="table_ys2">
        	<tr>
          		<td  colspan="2" class="Content_tab_style_td_head">${title }</td>
        	</tr>
        	
        	<tr>
				<td class="Content_tab_style1">项目编号：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" readonly name="bidWinning.bidCode" id="bidWinning.bidCode" value="${bidWinning.bidCode}" />
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">公示标题：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1"  name="bidWinning.winningTitle" id="bidWinning.winningTitle" value="${bidWinning.winningTitle}" />&nbsp;<font color="#ff0000">*</font>
				     <div class="info"><span class="Validform_checktip">公示标题不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			   </td>
			</tr>
			<tr>
				<td class="Content_tab_style1">公示内容：</td>
				<td class="Content_tab_style2"> 
					<textarea name="bidWinning.winningContent" id="winningContent"
						class="Content_input_style2" >${bidWinning.winningContent}</textarea>&nbsp;<font color="#ff0000">*</font>
				</td>
			</tr>
        </table>
        <div class="buttonDiv">
		<button class="btn btn-success"  type="button" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-success" type="button" id="sendButton" ><i class="icon-white icon-bullhorn"></i>发布至门户</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
	</div>
</div>
</form>
</body></html>