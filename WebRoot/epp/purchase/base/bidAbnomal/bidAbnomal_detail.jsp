<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>采购业务终止页面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script language="javaScript">

var api = frameElement.api, W = api.opener;
 
</script>
</head>
<body >
<form class="defaultForm" id="formAbnomal" name=""  method="post" enctype="" action="">
			

<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1" align="center">
			<tr>
				<td width="15%" class="Content_tab_style1">标段编号：</td>
				<td width="35%" class="Content_tab_style2">
					${bidAbnomal.bidCode }
				</td>
				<td width="15%" class="Content_tab_style1">采购方式：</td>
				<td width="35%" class="Content_tab_style2">
					${bidAbnomal.buyWayCn }
				</td>
			</tr>	
    		<tr>
				<td class="Content_tab_style1">采购摘要：</td>
				<td class="Content_tab_style2" colspan="3">
					${bidAbnomal.buyRemark }
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">异常类别：</td>
				<td class="Content_tab_style2" colspan="3">
						${bidAbnomal.abnomalTypeCn }
				</td>
			</tr>
    		<tr>
				<td class="Content_tab_style1">终止原因：</td>
				<td class="Content_tab_style2" colspan="3">
					${bidAbnomal.abnomalReason }
				</td>
			</tr>

			<tr>
				<td  class="Content_tab_style1">附件：</td>
				<td class="Content_tab_style2" colspan="3">
					${attachment }
				</td>
			</tr>
			 
			<tr>
				<td class="Content_tab_style1">操作人：</td>
				<td class="Content_tab_style2">
					${bidAbnomal.writerCn }
				</td>
				<td class="Content_tab_style1">终止日期：</td>
				<td class="Content_tab_style2">
					<s:date name="bidAbnomal.writeDete" format="yyyy-MM-dd  HH:mm:ss"/>
				</td>
			</tr>
			
        </table>
	</div>
</div>
</form>
</body>
</html>