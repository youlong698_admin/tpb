<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>采购业务终止页面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="BidAbnomal"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
//返回信息
<c:if test="${bidAbnomal.baId==null}">
	var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
	var fileNameData=[];//已上传的文件名
	var fileTypeData=[];//已上传的文件的格式
	var attIdData=[];//已存入附件表的附件信息
</c:if>
<c:if test="${bidAbnomal.baId!=null}">
	var uuIdData=${bidAbnomal.uuIdData};//已上传的文件的文件uuid，上传后的文件以uuId命名
	var fileNameData=${bidAbnomal.fileNameData};//已上传的文件名
	var fileTypeData=${bidAbnomal.fileTypeData};//已上传的文件的格式
	var attIdData=${bidAbnomal.attIdData};//已存入附件表的附件信息
</c:if>
var api = frameElement.api, W = api.opener;
	function subButton()
	{
		//提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			
		var bmId=$("#bmId").val();
		
		$.dialog.confirm("温馨提示：你确定要采购业务终止么！",function(){
		     document.formAbnomal.action="saveBidAbnomal_bidAbnomal.action";
		     document.formAbnomal.submit();
	   	},function(){},api);			   	 	
	}
</script>
</head>
<body >
<form class="defaultForm" id="formAbnomal" name="formAbnomal"  method="post" action="">
<input type="hidden" name="bidAbnomal.attIds" id="attIds" />
<input type="hidden" name="bidAbnomal.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="bidAbnomal.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="bidAbnomal.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="bidAbnomal.attIdData" id="attIdData" value=""/>

<input type="hidden" name="rcId" id="rcId" value="${requiredCollect.rcId }"/>
<input type="hidden" name="bidAbnomal.baId" id="baId" value="${bidAbnomal.baId }"/>
<input type="hidden" name="bidAbnomal.status" id="status" value="${bidAbnomal.status }"/>
<input type="hidden" name="bidAbnomal.statusCn" id="statusCn" value="${bidAbnomal.statusCn }"/>
<input type="hidden" name="bidAbnomal.deptId" id="deptId" value="${bidAbnomal.deptId }"/>

<s:token/>			

<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1" align="center">
			<tr>
				<td width="15%" class="Content_tab_style1">标段编号：</td>
				<td width="35%" class="Content_tab_style2">
					${requiredCollect.bidCode }
					<input name="bidAbnomal.bidCode" type="hidden" id="bidCode" value="${requiredCollect.bidCode }" />
				</td>
				<td width="15%" class="Content_tab_style1">采购方式：</td>
				<td width="35%" class="Content_tab_style2">
					${requiredCollect.buyWayCn }
					<input name="bidAbnomal.buyWay" type="hidden" id="buyWay" value="${requiredCollect.buyWay}" />
				</td>
			</tr>	
    		<tr>
				<td class="Content_tab_style1">采购摘要：</td>
				<td class="Content_tab_style2" colspan="3">
					${requiredCollect.buyRemark }
					<input name="bidAbnomal.buyRemark" type="hidden" id="buyRemark" value="${requiredCollect.buyRemark }" />
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">异常类别：</td>
				<td class="Content_tab_style2" colspan="3">
					 	<select name="bidAbnomal.abnomalType" id="abnomalType" class="input-small" datatype="*" nullmsg="异常类别不能为空！">
					 		<option value='' >---请选择---</option>
					 		<c:forEach items='${abnomalList}' var='abList'>
							<c:if test='${abList.key ==bidAbnomal.abnomalType}'>
							<option value='${abList.key }' selected='selected'>${abList.value }</option>
							</c:if>
							<c:if test='${abList.key !=bidAbnomal.abnomalType}'>
							<option value='${abList.key }'>${abList.value }</option>
							</c:if>
							</c:forEach>
					 	</select>
					 	<div class="info"><span class="Validform_checktip">异常类别不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
    		<tr>
				<td class="Content_tab_style1">终止原因：</td>
				<td class="Content_tab_style2" colspan="3">
					<textarea name="bidAbnomal.abnomalReason" id="abnomalReason" datatype="*" nullmsg="终止原因不能为空！"cols="85%" rows="4" style="resize:none;width:85%"
				   		>${bidAbnomal.abnomalReason }</textarea>
				   		<div class="info"><span class="Validform_checktip">终止原因不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>

			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
			 
			<tr>
				<td class="Content_tab_style1">操作人：</td>
				<td class="Content_tab_style2">
					<input name="bidAbnomal.writerCn" type="text" id="writerCn" value="${bidAbnomal.writerCn }" readonly />
					<input name="bidAbnomal.writer" type="hidden" id="writer" value="${bidAbnomal.writer }" readonly />
					<input name="bidAbnomal.writerId" type="hidden" id="writerId" value="${bidAbnomal.writerId }" readonly />
				</td>
				<td class="Content_tab_style1">终止日期：</td>
				<td class="Content_tab_style2">
					<input name="bidAbnomal.writeDete" type="text" class="Wdate"  
                        id="writeDete" value="<fmt:formatDate  value="${bidAbnomal.writeDete}" type="both"/>" readonly/>
				</td>
			</tr>
			
        </table>
        <div class="buttonDiv">
				<button class="btn btn-success" id="btn-save" type="button"><i class="icon-white icon-ok-sign"></i>提交</button>
			    <button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			subButton();
			return false;	
		}
	});
})
</script>
</body>
</html>