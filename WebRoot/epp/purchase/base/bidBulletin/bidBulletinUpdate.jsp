<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>${title }</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%= path %>/common/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="<%= path %>/common/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%= path %>/common/kindeditor/lang/zh_CN.js"></script> 
   <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
var api = frameElement.api, W = api.opener, cDG;
 
var orderState="${requiredCollect.orderState}";
var rcId="${requiredCollect.rcId}";
var processId = "${requiredCollect.processId}";
var orderId = "${requiredCollect.orderId}";
var instanceUrl = "${requiredCollect.instanceUrl}";
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="BidBulletin"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
 var fileNameData=${bidBulletin.fileNameData};
 var uuIdData=${bidBulletin.uuIdData};
 var fileTypeData=${bidBulletin.fileTypeData};
 var attIdData=${bidBulletin.attIdData};
  			KindEditor.ready(function(K) {
				var editor1 = K.create('textarea[id="bulletinContent"]', {
					cssPath : '<%=path %>/common/kindeditor/plugins/code/prettify.css',
				    uploadJson : '<%=path %>/common/kindeditor/jsp/upload_json.jsp',
				    fileManagerJson : '<%=path %>/common/kindeditor/jsp/file_manager_json.jsp',
					width:'95%',
					height:'250px',
					allowFileManager : true,
					items : [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons'],
					afterCreate : function() {
						var self = this;
						 $('#btn-save').click(function () {
							self.sync();
							$("#fileNameData").val(fileNameData);
						    $("#uuIdData").val(uuIdData);
							$("#fileTypeData").val(fileTypeData);
							$("#attIdData").val(attIdData);
						    var action = "updateBidBulletin_bidBulletin.action?status=1";
							document.forms[0].action = action;
							document.forms[0].submit();
	                    });
						 $('#sendButton').click(function () {
							self.sync();
							var bulletinContent = document.getElementById("bulletinContent").value;
							if(bulletinContent!=''){
								lhgdialog.confirm("温馨提示：您确定要发布至门户吗？",function(){
								    $("#fileNameData").val(fileNameData);
								    $("#uuIdData").val(uuIdData);
								    $("#fileTypeData").val(fileTypeData);
								    $("#attIdData").val(attIdData);
								    var action = "updateBidBulletin_bidBulletin.action?status=0";
							        document.forms[0].action = action;
							        document.forms[0].submit();
								},function(){},api);
							}else{
								showMsg("alert","温馨提示：公告内容不能为空！");
								return ;
							}
	                    });
					}
				});
			});
	function processSubmit(){
  	    	if(orderState == "0"){
  	    		showMsg("alert","温馨提示:此项目流程已结束,请选择将要提交的信息!");
  	    	}else if(orderState == "1"){
  	    		showMsg("alert","温馨提示:此项目流程正在运行,请选择将要提交的信息!");
  	    	}else{
  	    	   var orderNo="${work_flow_type}"+rcId;
  	    	   createdetailwindow("采购项目流程申请","<%=path%>"+instanceUrl+"?processId="+processId+"&orderId="+orderId+"&orderNo="+orderNo);
  	    	       
  	    	}
	  	}
	  	
	  	//撤销流程审批
	function processCancel(){
	    	if(orderState == "0"){
   	    		$.dialog.alert("温馨提示:此项目流程已结束,不能撤销!");
   	    		return false;
   	    	}else{
   	    		var action = "deleteCancelPurchaseProcess_bidworkflow.action";
				var param = "requiredCollect.rcId="+rcId;
				var result = ajaxGeneral(action,param,"text");
				if(parseInt(result)==0){
					showMsg("alert","温馨提示：流程撤销成功！");
					window.location.reload(); 
				}else if(parseInt(result)==1){
					showMsg("alert","温馨提示：流程编号不存在！");
					return ;
				}else if(parseInt(result)==2){
					showMsg("alert","温馨提示：没有提交流程，不能撤销！");
					return ;
				}else if(parseInt(result)==3){
					showMsg("alert","温馨提示：流程已经处理，不能撤销！");
					return ;
				}else{
					return ;				
				}
    		}
	}
		//弹出一个新窗口
	function openWorkFlowWindow(action, rcId, processId){
		 rcId="${work_flow_type}"+rcId;
		 $.ajax({
			url:"<%=path%>/findWfOrder_workflow.action",
			type:"POST",
			dataType: 'json',
			async:false, 
			data:{orderNo:rcId, processId:processId},
			success:function(msg){
				var rsb = msg;
				var rs = rsb.order;
				if(rs.length > 0){
					createdetailwindow("查看流程",action+rs[0].id,0);
				}else{
					return;
				}
			},
			error:function(){
				
			}
		});
		
	}	
</script>
</head>
 
<body>
<form id="noticeEdit" class="defaultForm" name="noticeEdit" method="post" action="">
<input type="hidden" name="bidBulletin.bbId" value="${bidBulletin.bbId}"/>
<input type="hidden" name="bidBulletin.rcId" value="${bidBulletin.rcId}"/>
<input type="hidden" name="bidBulletin.status" value="${bidBulletin.status}"/>
<input type="hidden" name="bidBulletin.writer" value="${bidBulletin.writer}"/>
<input type="hidden" name="bidBulletin.publisher" value="${bidBulletin.publisher}"/>
<input type="hidden" name="bidBulletin.buyWay" value="${bidBulletin.buyWay}"/>
<input type="hidden" name="bidBulletin.writeDate" value="<fmt:formatDate value="${bidBulletin.writeDate}" pattern="yyyy-MM-dd HH:mm:ss" />"/>
<input type="hidden" name="bidBulletin.publishDate" value="<fmt:formatDate value="${bidBulletin.publishDate}" pattern="yyyy-MM-dd HH:mm:ss" />"/>
<input type="hidden" name="bidBulletin.attIds" id="attIds" />
<input type="hidden" name="bidBulletin.fileNameData" id="fileNameData" value="${bidBulletin.fileNameData}"/>
<input type="hidden" name="bidBulletin.uuIdData" id="uuIdData" value="${bidBulletin.uuIdData}"/>
<input type="hidden" name="bidBulletin.fileTypeData" id="fileTypeData" value="${bidBulletin.fileTypeData}"/>
<input type="hidden" name="bidBulletin.attIdData" id="attIdData" value="${bidBulletin.attIdData}"/>
<s:token/>			
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table width="98%" border="0" align="center"  class="table_ys2">
        	<tr>
          		<td  colspan="2" class="Content_tab_style_td_head">${title }</td>
        	</tr>
        	<tr>
				<td class="Content_tab_style1">项目编号：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" readonly name="bidBulletin.bidCode" id="bidBulletin.bidCode" value="${bidBulletin.bidCode}" />
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">公告标题：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1"  name="bidBulletin.bulletinTitle" id="bidBulletin.bulletinTitle" value="${bidBulletin.bulletinTitle}" />&nbsp;<font color="#ff0000">*</font>
			   </td>
			</tr>
			<tr>
				<td class="Content_tab_style1">公告内容：</td>
				<td class="Content_tab_style2"> 
					<textarea name="bidBulletin.bulletinContent" id="bulletinContent"
						class="Content_input_style2" >${bidBulletin.bulletinContent}</textarea>&nbsp;<font color="#ff0000">*</font>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
				</td>
			</tr>
        </table>
        <div class="buttonDiv">
		<c:if test="${requiredcollectWorkflow=='0'}"> <!-- 项目需要审批并且状态为等待提交时候 -->
             <c:if test="${requiredCollect.orderState==3||empty requiredCollect.orderState}">
                 <button class="btn btn-success"  type="button" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
                 <button type="button" class="btn btn-info" id="btn-processSubmit" onclick="processSubmit(${rcId})"><i class="icon-white icon-resize-small"></i> 提交流程</button>  <!--fa fa-chain-broken icon-resize-full -->
		         <button type="button" class="btn btn-warning" id="btn-processCancel" onclick="processCancel(${rcId})"><i class="icon-white icon-resize-full"></i> 撤销流程</button>
		     </c:if>		     
		     <c:if test="${requiredCollect.orderState=='0'}">		        
		         <button class="btn btn-success" type="button" id="sendButton" ><i class="icon-white icon-bullhorn"></i>发布至门户</button>
             </c:if>    
	    </c:if>
	    <c:if test="${requiredcollectWorkflow=='1'}"> 
		     <button class="btn btn-success"  type="button" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
		     <button class="btn btn-success" type="button" id="sendButton" ><i class="icon-white icon-bullhorn"></i>发布至门户</button>
		</c:if>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
	 <c:if test="${requiredcollectWorkflow=='0'}">
		<div class="row">
			<input type="hidden" id="rcId" value="${rcId }"/>
			 <div class="alert alert-block alert-success">
			        <p>
		                <strong>
						<i class="icon-ok fa fa-check"></i>
						当前状态：<c:choose>
						<c:when test="${not empty requiredCollect.orderState}">
						<a href="##" onclick="openWorkFlowWindow('display_workflow.action?param=view&processId=${requiredCollect.processId}&orderId=','${requiredCollect.rcId }','${requiredCollect.processId}')">${requiredCollect.orderStateName}</a>
						</c:when>
						<c:otherwise>等待提交</c:otherwise>
						</c:choose>
						</strong>
		              </p>
             </div>
         </div>
   </c:if>
	</div>
</div>
</form>
</body></html>