<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>${title }</title>		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
		<script type="text/javascript">
		    function changeBidBulletin(rcId,bbId){
		       window.location.href="changeBidBulletinInit_bidBulletin.action?rcId="+rcId+"&bidBulletin.bbId="+bbId;
		    }
		</script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">

								<div class="alert alert-block alert-success">
								        <p>
                                            <strong>
												<i class="icon-ok fa fa-check"></i>
												公告状态：${bidBulletin.statusCn }
												<c:if test="${isChange}">
												   <button class="btn btn-danger" type="button" id="chageButton" onclick="changeBidBulletin(${bidBulletin.rcId},${bidBulletin.bbId })"><i class="icon-white icon-bullhorn"></i>变更公告</button>
												</c:if>
											</strong>
                                        </p>
                                   </div>						 
								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
										<tr>
											<td colspan="2" class="Content_tab_style_td_head">
												${title }
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												项目编号：
											</td>
											<td width="35%" class="Content_tab_style2">
												${bidBulletin.bidCode}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												公告标题：
											</td>
											<td width="35%" class="Content_tab_style2">
												${bidBulletin.bulletinTitle}
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												公告内容：
											</td>
											<td width="35%" class="Content_tab_style2">
												<c:out value="${bidBulletin.bulletinContent}" escapeXml="false"/>
											</td>
										</tr>
										<tr>
											<td width="15%" class="Content_tab_style1">
												附件：
											</td>
											<td width="35%" class="Content_tab_style2">
												<c:out value="${bidBulletin.attachmentUrl}" escapeXml="false"/>
											</td>
										</tr>

									</table>
								</div>
							</div>
						</div>
	</body>
</html>