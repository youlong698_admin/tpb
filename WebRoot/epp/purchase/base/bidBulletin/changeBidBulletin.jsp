<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>${bidBulletin.bulletinTitle}</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%= path %>/common/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="<%= path %>/common/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%= path %>/common/kindeditor/lang/zh_CN.js"></script> 
   <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
 var api = frameElement.api, W = api.opener, cDG;
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="BidBulletin"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
 var fileNameData=${bidBulletin.fileNameData};
 var uuIdData=${bidBulletin.uuIdData};
 var fileTypeData=${bidBulletin.fileTypeData};
 var attIdData=${bidBulletin.attIdData};
 var editor1;
  			KindEditor.ready(function(K) {
				editor1 = K.create('textarea[id="bulletinContent"]', {
					cssPath : '<%=path %>/common/kindeditor/plugins/code/prettify.css',
				    uploadJson : '<%=path %>/common/kindeditor/jsp/upload_json.jsp',
				    fileManagerJson : '<%=path %>/common/kindeditor/jsp/file_manager_json.jsp',
					width:'95%',
					height:'250px',
					allowFileManager : true,
					afterCreate : function() {
						var self = this;
						 $('#sendButton').click(function () {
							self.sync();
							var bulletinContent = document.getElementById("bulletinContent").value;
							if(bulletinContent!=''){
								lhgdialog.confirm("温馨提示：您确定要发布至门户吗？",function(){
								    $("#fileNameData").val(fileNameData);
								    $("#uuIdData").val(uuIdData);
								    $("#fileTypeData").val(fileTypeData);
								    $("#attIdData").val(attIdData);
								    var action = "changeBidBulletin_bidBulletin.action";
							        document.forms[0].action = action;
							        document.forms[0].submit();
								},function(){},api);
							}else{
								showMsg("alert","温馨提示：公告内容不能为空！");
								return ;
							}
	                    });
					}
				});
			});
	function doCreate(rcId,buyWay){	
	   editor1.html("");
	   if(buyWay=="00"){
	      var openDate=$("#openDate").val();
	      var salesDate=$("#salesDate").val();
	      var saleeDate=$("#saleeDate").val();
	      var returnDate=$("#returnDate").val();
	      if(openDate==""){
	        showMsg("alert","温馨提示：开标日期不能为空！");
			return ;
	      }
	      if(salesDate==""){
	        showMsg("alert","温馨提示：招标文件领购开始日期不能为空！");
			return ;
	      }
	      if(saleeDate==""){
	        showMsg("alert","温馨提示：招标文件领购截止日期不能为空！");
			return ;
	      }
	      if(returnDate==""){
	        showMsg("alert","温馨提示：回标截止日期不能为空！");
			return ;
	      }
	      var params={rcId:rcId,openDate:openDate,salesDate:salesDate,saleeDate:saleeDate,returnDate:returnDate};
	      var content=ajaxGeneral("changeBidBulletinContent_bidBulletin.action",params,"html");
	      editor1.insertHtml(content);
	   }else if(buyWay=="01"){
	      var askDate=$("#askDate").val();
	      var decryptionDate=$("#decryptionDate").val();
	      var returnDate=$("#returnDate").val();
	      if(askDate==""){
	        showMsg("alert","温馨提示：询价时间不能为空！");
			return ;
	      }
	      if(returnDate==""){
	        showMsg("alert","温馨提示：报价截止时间不能为空！");
			return ;
	      }
	      if(decryptionDate==""){
	        showMsg("alert","温馨提示：报价解密时间不能为空！");
			return ;
	      }
	      var params={rcId:rcId,decryptionDate:decryptionDate,returnDate:returnDate,askDate:askDate};
	      var content=ajaxGeneral("changeBidBulletinContent_bidBulletin.action",params,"html");
	      editor1.insertHtml(content);
	   }else if(buyWay=="02"){
	      var biddingStartTime=$("#biddingStartTime").val();
	      var biddingEndTime=$("#biddingEndTime").val();
	      if(biddingStartTime==""){
	        showMsg("alert","温馨提示：竞价开始时间不能为空！");
			return ;
	      }
	      if(biddingEndTime==""){
	        showMsg("alert","温馨提示：竞价结束时间不能为空！");
			return ;
	      }	      
	      var params={rcId:rcId,biddingEndTime:biddingEndTime,biddingStartTime:biddingStartTime};
	      var content=ajaxGeneral("changeBidBulletinContent_bidBulletin.action",params,"html");
	      editor1.insertHtml(content);
	   }
	 
	}	
</script>
</head>
 
<body>
<form id="noticeEdit" class="defaultForm" name="noticeEdit" method="post" action="">
<input type="hidden" name="bidBulletin.rcId" value="${bidBulletin.rcId}"/>
<input type="hidden" name="bidBulletin.sysCompany" value="${bidBulletin.sysCompany}"/>
<input type="hidden" name="bidBulletin.comId" value="${bidBulletin.comId}"/>
<input type="hidden" name="bidBulletin.buyWay" value="${bidBulletin.buyWay}"/>
<input type="hidden" name="bidBulletin.attIds" id="attIds" />
<input type="hidden" name="bidBulletin.fileNameData" id="fileNameData" value="${bidBulletin.fileNameData}"/>
<input type="hidden" name="bidBulletin.uuIdData" id="uuIdData" value="${bidBulletin.uuIdData}"/>
<input type="hidden" name="bidBulletin.fileTypeData" id="fileTypeData" value="${bidBulletin.fileTypeData}"/>
<input type="hidden" name="bidBulletin.attIdData" id="attIdData" value="${bidBulletin.attIdData}"/>
<s:token/>			
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table width="98%" border="0" align="center"  class="table_ys2">
        	<tr>
          		<td  colspan="2" class="Content_tab_style_td_head">${title }</td>
        	</tr>
        	<tr>
				<td class="Content_tab_style1">项目编号：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" readonly name="bidBulletin.bidCode" id="bidBulletin.bidCode" value="${bidBulletin.bidCode}" />
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">公告标题：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1"  name="bidBulletin.bulletinTitle" id="bidBulletin.bulletinTitle" value="${bidBulletin.bulletinTitle}" />&nbsp;<font color="#ff0000">*</font>
				</td>
			</tr>
			<c:choose>
			<c:when test="${bidBulletin.buyWay=='00'}">
			 <tr>
				<td class="Content_tab_style1">开标日期：</td>
				<td class="Content_tab_style2">
					<input type="text" name="openDateStr" id="openDate" class="Wdate" 
                        value="<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'%y-%M-%d %H:%m' })" /><font color="#ff0000">*</font>
				</td>
			</tr>

			<tr>
			    <td class="Content_tab_style1">招标文件领购开始日期：</td>
				<td class="Content_tab_style2">
					<input type="text" name="salesDateStr" id="salesDate" class="Wdate" 
                        value="<fmt:formatDate value="${tenderBidList.salesDate}" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'%y-%M-%d %H:%m',maxDate:'#F{$dp.$D(\'saleeDate\')}' })" /><font color="#ff0000">*</font>
				</td>
			</tr>

			<tr>
				<td class="Content_tab_style1">招标文件领购截止日期：</td>
				<td class="Content_tab_style2">
					<input type="text" name="saleeDateStr" id="saleeDate" class="Wdate" 
                         value="<fmt:formatDate value="${tenderBidList.saleeDate }" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'salesDate\')}' })" /><font color="#ff0000">*</font>
				</td>
			</tr>

			<tr>
				<td class="Content_tab_style1">回标截止日期：</td>
				<td class="Content_tab_style2">
					<input type="text" name="returnDateStr" id="returnDate" class="Wdate" 
                         value="<fmt:formatDate value="${tenderBidList.returnDate }" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'salesDate\')}' })" /><font color="#ff0000">*</font>
				<a href="javascript:;" onclick="doCreate(${bidBulletin.rcId},'${bidBulletin.buyWay}');"><img src="<%=path %>/images/home_main_icon5.png"/>生成公告</a>
				</td>
			</tr>
			</c:when>
			<c:when test="${bidBulletin.buyWay=='01'}">
			 <tr>
			   <td class="Content_tab_style1">询价时间：</td>
			   <td class="Content_tab_style2">
					<input type="text" name="askDateStr" id="askDate" class="Wdate" 
                        value="<fmt:formatDate value="${askBidList.askDate}" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm', minDate:'%y-%M-%d %H:%m',maxDate:'#F{$dp.$D(\'returnDate\')}' })" /><font color="#ff0000">*</font>
			   </td>
			 </tr>
			 <tr>
			   <td class="Content_tab_style1">报价截止时间：</td>
			   <td class="Content_tab_style2">
					<input type="text" name="returnDateStr"  id="returnDate" class="Wdate" 
                        value="<fmt:formatDate value="${askBidList.returnDate}" pattern="yyyy-MM-dd HH:mm" />" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm', minDate:'%y-%M-%d %H:%m',maxDate:'#F{$dp.$D(\'decryptionDate\')}' })" /><font color="#ff0000">*</font>
			   </td>
			 </tr>
			 <tr>
			   <td class="Content_tab_style1">报价解密时间：</td>
			   <td class="Content_tab_style2">
					<input type="text" name="decryptionDateStr"  id="decryptionDate" class="Wdate" 
                         value="<fmt:formatDate value="${askBidList.returnDate}" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'returnDate\')}' })" /><font color="#ff0000">*</font>
			        <a href="javascript:;" onclick="doCreate(${bidBulletin.rcId},'${bidBulletin.buyWay}');"><img src="<%=path %>/images/home_main_icon5.png"/>生成公告</a>
			   </td>
			 </tr>
			</c:when>
			<c:otherwise>
			  <tr>
			    <td class="Content_tab_style1">竞价开始时间：</td>
				<td class="Content_tab_style2">
					<input type="text" name="biddingStartTimeStr" id="biddingStartTime" class="Wdate" 
                        value="<fmt:formatDate value="${biddingBidList.biddingStartTime}" pattern="yyyy-MM-dd HH:mm" />" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm', minDate:'%y-%M-%d %H:%m',maxDate:'#F{$dp.$D(\'biddingEndTime\')}' })" /><font color="#ff0000">*</font>
				</td>
			  </tr>
			  <tr>
			    <td class="Content_tab_style1">竞价结束时间：</td>
				<td class="Content_tab_style2">
					<input type="text" name="biddingEndTimeStr" id="biddingEndTime" class="Wdate" 
                        value="<fmt:formatDate value="${biddingBidList.biddingEndTime}" pattern="yyyy-MM-dd HH:mm" />"  onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'biddingStartTime\')}' })" /><font color="#ff0000">*</font>
				     <a href="javascript:;" onclick="doCreate(${bidBulletin.rcId},'${bidBulletin.buyWay}');"><img src="<%=path %>/images/home_main_icon5.png"/>生成公告</a>
			   </td>
			  </tr>
			  <tr>
			  
			  </tr>
			</c:otherwise>
			</c:choose>
			<tr>
				<td class="Content_tab_style1">公告内容：</td>
				<td class="Content_tab_style2"> 
					<textarea name="bidBulletin.bulletinContent" id="bulletinContent"
						class="Content_input_style2" ></textarea>&nbsp;<font color="#ff0000">*</font>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
        </table>
        <div class="buttonDiv">
		<button class="btn btn-success" type="button" id="sendButton" ><i class="icon-white icon-bullhorn"></i>发布至门户</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
	</div>
</div>
</form>
</body></html>