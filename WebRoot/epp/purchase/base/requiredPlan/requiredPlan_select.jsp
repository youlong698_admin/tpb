<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>选择需求计划信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/required/requirePlan.js" type="text/javascript" ></script>
<script type="text/javascript">

var api = frameElement.api, W = api.opener;
$(function (){

	$("#btn-danger").click(function(){
	    	api.close();
	});
	
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	var _table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findNotCollectedRequiredPlan_purchaseBase.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.SELECTPAGECHECKBOX,
            {
            	data: "rmCode",            	
            	width : "8%"
            },
			{
				data : "rmType",
				render: CONSTANT.DATA_TABLES.RENDER.RMTYPE,
				orderable : false,
				width : "7%"
			},
			{
				className : "ellipsis",
				data : "buyCode",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "13%"
			},
			{
				className : "ellipsis",
				data : "buyName",
				width : "20%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				className : "ellipsis",
				data : "materialType",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false,
				width : "10%"
			},
			{
				data : "unit",
				width : "5%",
				orderable : false
			},
			{
            	className : "numPrice",
				data : "amount",
				render: CONSTANT.DATA_TABLES.RENDER.NUMBER,
				width : "8%"
			},
			{
            	className : "numPrice",
				data : "yamount",
				render: CONSTANT.DATA_TABLES.RENDER.NUMBER,
				width : "8%"
			},
			{
            	className : "numPrice",
				data : "estimatePrice",
            	render: CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE,
				width : "7%",
				orderable : false
			},
			{
				data : "deliverDate",
				width : "7%"
			},
			{
				data : "deptName",
				width : "10%",
				orderable : false
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	//
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-save").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });	
		GeneralManage.saveItemInit(arrItemId);
	});
	
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//组装排序参数
		//默认进入的排序
		 param.orderColumn="de.rmCode";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 1:
				param.orderColumn = "de.rmCode";
				break;
			case 3:
				param.orderColumn = "de.buyCode";
				break;
			case 7:
				param.orderColumn = "de.amount";
				break;
			case 8:
				param.orderColumn = "de.yamount";
				break;
			case 10:
				param.orderColumn = "de.deliverDate";
				break;
			default:
				param.orderColumn = "de.rmCode";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		
		param.rmCode = $("#rmCode").val();
		param.rmName = $("#rmName").val();;
		param.buyCode = $("#buyCode").val();
		param.buyName = $("#buyName").val();
		param.rmType = $("#rmType").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	saveItemInit:function(selectedItems){
		var v = selectedItems;
		  if(v.length>0){
	    	var returnVals ="1@";
	    	var newAmount;
			for(var i=0;i<v.length;i++){
			    newAmount=FloatSub(v[i].amount,v[i].yamount);
				if(i == (v.length-1)){
					returnVals += v[i].buyCode+"#"+v[i].buyName+"#"+v[i].materialType+"#"+v[i].unit+"#"+newAmount+"#"+v[i].deliverDate+"#"+v[i].rmdId+"#"+v[i].mnemonicCode+"#"+v[i].materialId+"#"+v[i].estimatePrice;
				}else{
					returnVals += v[i].buyCode+"#"+v[i].buyName+"#"+v[i].materialType+"#"+v[i].unit+"#"+newAmount+"#"+v[i].deliverDate+"#"+v[i].rmdId+"#"+v[i].mnemonicCode+"#"+v[i].materialId+"#"+v[i].estimatePrice+"@";
				}
			}
			//父页面一定要有id 为 returnVals 的 input 和 doReturn()函数
			W.document.getElementById('returnVals').value=returnVals;
			W.doReturn();
			api.close();
		}else{
	    	showMsg("alert",'温馨提示：请选择计划信息！');
	    }
	}
};

	//重置
	function doReset(){        
		document.forms[0].elements["projectName"].value	=	"";	
		document.forms[0].elements["rmCode"].value	=	"";
		document.forms[0].elements["buyName"].value		=	"";
		document.forms[0].elements["rmType"].value	=	"";
		document.forms[0].elements["buyCode"].value	=	"";
		document.forms[0].elements["buyName"].value		=	"";
	}
	
--></script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
										     <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i> 确定</button>
							<button type="button" class="btn btn-danger" id=btn-danger><i class="icon-white icon-remove-sign"></i> 关闭</button>
						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well" >						
						计划名称：
							<input  type="text" class="input-medium" placeholder="计划名称" id="rmName" size="15" name="requiredMaterial.rmName"  value="" />
						计划编号：
							<input  type="text" class="input-medium" placeholder="计划编号" id="rmCode" size="15" name="requiredMaterial.rmCode"  value="" />
						计划类别：
						<select id="rmType" name="rmType" class="input-small">
						 		<option value="">全部</option>
								<option value="00">普通计划</option>
								<option value="01">加急计划</option>
						</select>
					           编码：
							<input class="input-medium" type="text" placeholder="编码" id="buyCode" name="requiredMaterialDetail.buyCode" value="" />
						名称：
							<input class="input-medium" type="text" placeholder="名称" id="buyName" name="requiredMaterialDetail.buyName" value="" />
						
						<button type="button" class="btn btn-info" id="btn-advanced-search" ><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
			
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="30px">
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>计划编号</th>
									<th>计划类型</th>
									<th>编码</th>
									<th>名称</th>
									<th>规格型号</th>
									<th>计量单位</th>
									<th>计划数量</th>
									<th>已采数量</th>
									<th>估算单价</th>
									<th>交货期</th>
									<th>填报部门</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>