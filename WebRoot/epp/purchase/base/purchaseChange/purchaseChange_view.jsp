<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>采购方式变更</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
<![endif]-->
<script type="text/javascript">
	var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findRequiredCollectedForPurchaseChange_purchaseBase.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            {
				data : "purchaseDeptName",
				width : "10%",
            	orderable : false	
			},
            {
            	data: "bidCode",            	
            	width : "15%",             	
            	render: viewGeneralManage
            },
            {	
            	className : "currentNode",
            	data: "serviceStatusCn",
				width : "10%",
				render: function(data,type, row, meta) {
                   return "<span title=\""+ data +"\">"+data+"</span>";
                },
            	orderable : false			
            },
            {	
            	className : "ellipsis",
            	data: "buyRemark",
            	render: viewGeneralManage,
				width : "16%",
            	orderable : false			
            },
			{
				data : "buyWay",
				width : "8%",
				render: CONSTANT.DATA_TABLES.RENDER.BUYWAY,
            	orderable : false	
			},
			{
				data : "supplierType",
				width : "13%",
				render: CONSTANT.DATA_TABLES.RENDER.SUPPLIERTYPE,
            	orderable : false	
			},
			{
				data : "writerCn",
				width : "10%",
				orderable : false
			},
			{
				className : "ellipsis",
				data : "writeDate",
				render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "10%"
			},
			{
				className : "ellipsis",
				data : "bidStatusCn",
				width : "10%",
            	orderable : false	
			},
			{
				className : "td-operation",
				data:null,
				orderable : false,
				render: function(data,type, row, meta) {
				   if((row.buyWay=='00'&&row.serviceStatus<27)||(row.buyWay=='01'&&row.serviceStatus<25)||(row.buyWay=='02'&&row.serviceStatus<25))
                   return "<a href=\"javascript:createdetailwindow('变更采购方式','purchaseChangeInit_purchaseBase.action?rcId="+row.rcId+"',1)\" class=\"a_look\"><i class=\"icon icon-edit\"></i> 变更</a>"; 
                   else 
                   return ""; 
                   
                },
				width : "100px"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
            $('td', row).eq(8).addClass("light-red");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });	
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
 	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//组装排序参数
		//默认进入的排序
		 param.orderColumn="de.rcId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 2:
				param.orderColumn = "de.bidCode";
				break;
			case 8:
				param.orderColumn = "de.writeDate";
				break;
			default:
				param.orderColumn = "de.rcId";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		
		param.bidCode = $("#bidCode").val();
		param.buyRemark = $("#buyRemark").val();
		param.rmCode = $("#rmCode").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	} 	
};
    //查看
    function viewGeneralManage(data, type, row,   meta){
       if(row.buyWay=="00"){
           return "<a href='viewRequiredCollectNode_purchaseTender.action?requiredCollect.rcId=" + row.rcId + "' title='"+data+"'>"+data+"</a>"; 
        }else if(row.buyWay=="01"){
           return "<a href='viewRequiredCollectNode_purchaseAsk.action?requiredCollect.rcId=" + row.rcId + "' title='"+data+"'>"+data+"</a>"; 
        }else if(row.buyWay=="02"){
           return "<a href='viewRequiredCollectNode_purchaseBidding.action?requiredCollect.rcId=" + row.rcId + "' title='"+data+"'>"+data+"</a>"; 
        }
    }
    function doQuery(){
     _table.draw();
    }
	//重置
	function doReset(){        
		document.forms[0].elements["bidCode"].value	=	"";	
		document.forms[0].elements["buyRemark"].value	=	"";
		document.forms[0].elements["rmCode"].value	=	"";
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="查询区域" id="toggle-advanced-search">
											<i class="icon-white icon-search"></i>
										</button>
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
										    <i class="icon-white icon-refresh"></i>
										</button>
									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid" style="display:none;" id="div-advanced-search">
					<form class="form-inline well">
						计划编号：
						<input class="input-medium" type="text" name="rmCode" placeholder="计划编号" id="rmCode" value="" size="15" />
						
						项目编号：
						<input class="input-medium" type="text" name="bidCode" placeholder="项目编号" id="bidCode" value="" size="15" />
						项目名称：
						<input class="input-medium" type="text" name="buyRemark" placeholder="项目名称" id="buyRemark" size="15" value=""  />
						
					 	<button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					 	
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>采购组织</th>
									<th>项目编号</th>
									<th>当前节点</th>
									<th>项目名称</th>
									<th>采购方式</th>
									<th>供应商选择方式</th>
									<th>立项人</th>
									<th>立项日期</th>
									<th>采购状态</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>