<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>采购方式变更保存页面</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
 <script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
var api = frameElement.api, W = api.opener;
//附件需要添加的信息
 var sessionId="<%=session.getId()%>";
 var attachmentType="RequiredCollect"; //当前是哪个类别功能上传的附件
 var path="<%= path %>";
 var fileNameData=${requiredCollect.fileNameData};
 var uuIdData=${requiredCollect.uuIdData};
 var fileTypeData=${requiredCollect.fileTypeData};
 var attIdData=${requiredCollect.attIdData};
 function doSave(){
   if($.trim($('#changeReason').val()) == ""){
		showMsg("alert","温馨提示：请输入变更原因！");
		return false;
	}
	//提交之前把选择的附件信息填充值
	$("#fileNameData").val(fileNameData);
	$("#uuIdData").val(uuIdData);
	$("#fileTypeData").val(fileTypeData);
	$("#attIdData").val(attIdData);
	
	$.dialog.confirm("温馨提示：你确定要变更采购方式么！",function(){
		document.form.action="savePurchaseChange_purchaseBase.action";
		document.form.submit();
	},function(){},api);
 }
</script>
</head>
<body >
<form id="cx" name="form" method="post" action="">
<input type="hidden" name="oldRcId" value="${requiredCollect.rcId}"/>
<input type="hidden" name="requiredCollect.attIds" id="attIds" />	
<input type="hidden" name="requiredCollect.fileNameData" id="fileNameData" value="${requiredCollect.fileNameData}"/>
<input type="hidden" name="requiredCollect.uuIdData" id="uuIdData" value="${requiredCollect.uuIdData}"/>
<input type="hidden" name="requiredCollect.fileTypeData" id="fileTypeData" value="${requiredCollect.fileTypeData}"/>
<input type="hidden" name="requiredCollect.attIdData" id="attIdData" value="${requiredCollect.attIdData}"/>
	<table align="center"  class="table_ys1">
		<tr align="center" class="Content_tab_style_05"><th colspan="4">采购方式变更</th></tr>
		<tr >
			 <td width="15%" class="Content_tab_style1">项目名称：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.buyRemark }
			 </td>
			<td width="15%" class="Content_tab_style1">项目编号：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.bidCode }				
			 </td>
		</tr>
		<tr>	 
			 <td width="15%" nowrap class="Content_tab_style1">采购方式：</td>
			 <td width="35%" align="left">
			 	${buyWayCn }	
			 </td>
			 <td width="15%" class="Content_tab_style1">供应商选择方式：</td>
			 <td width="35%" align="left">
			 	${supplierTypeCn }	
			 </td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>预算金额：</td>
			<td class="Content_tab_style2">
				${requiredCollect.totalBudget }
			</td>
			<td class="Content_tab_style1" nowrap>编制日期：</td>
			<td class="Content_tab_style2">
				<fmt:formatDate value="${requiredCollect.writeDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
			</td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>采购组织：</td>
			<td class="Content_tab_style2">
				${purchaseDeptName }
			</td>
			<td class="Content_tab_style1">负责人：</td>
			<td class="Content_tab_style2">
				${writerCn }
			</td>
		</tr>
   		<tr>
			<td class="Content_tab_style1">负责人单位：</td>
			<td class="Content_tab_style2">
				${deptName }
			</td>				
			 <td class="Content_tab_style1">变更后采购方式：</td>
			 <td align="left" width="10%" >
			    <select name="requiredCollect.buyWay">
				 	<c:forEach items="${purchaseType}" var="map">
				 	       <option value="${map.key}" <c:if test="${map.key==requiredCollect.buyWay}">selected</c:if>>${map.value }</option>
				 	</c:forEach> 
			 	</select>
	    	 </td>	
		</tr>
		<tr class='biaoge_01_b'>
			 <td class="Content_tab_style1" width="10%" >变更原因：</td>
			 <td align="left" width="30%" colspan="3">
				 	<textarea name="requiredCollect.changeReason" id="changeReason"  cols="85%" rows="4" style="resize:none;width:85%"
				   		></textarea>
			 </td>
		</tr>
		
		<tr>
			<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
			<td class="Content_tab_style2" colspan="3">
				<!-- 附件存放 -->
				<div  id="fileDiv" class="panel"> 
				
				</div>
				<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
				<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
				<input type="hidden" id="rmpId" name="rmpId" value="${requiredMS.rmpId }"/>&nbsp;
			</td>
		</tr>
	</table>
	<table align="center" class="table_ys1" id="listtable">	
		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap>序号</th>
			<th width="95px" nowrap>编码</th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredCollectDetail" varStatus="status">
			<tr  class="input_ys1">
				<td>${status.index+1}
				</td>
				<td>
				    ${requiredCollectDetail.buyCode}
				</td>
				<td>
				   ${requiredCollectDetail.buyName}
				</td>
				<td>
				   ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.unit}
			    </td>
			    <td align="right">
			       ${requiredCollectDetail.amount}
			    </td>
				<td>
					<fmt:formatDate value="${requiredCollectDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
				</td>
				<td>${requiredCollectDetail.remark }</td>
			</tr>
		</c:forEach>		
	</table>
	<div class="buttonDiv">
				<button class="btn btn-success" id="btn-save" type="button" onclick="doSave()"><i class="icon-white icon-ok-sign"></i>确认变更</button>
			    <button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
		</div>
</form>
</body>
</html>