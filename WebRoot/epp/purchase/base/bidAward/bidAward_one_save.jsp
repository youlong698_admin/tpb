<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>授标单个供应商</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 $(function (){
		//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		       api.get("dialog").window.location.href=api.get("dialog").window.location.href;
		       api.close();
		       }
		 </c:if>
	});
 function save(){
    		var supId = 0;
    		var radNames = document.getElementsByName("radSup");
    		for(var i=0;i<radNames.length;i++){
    			if(radNames[i].checked){
    				supId=radNames[i].value;
    			}
    		}
    		if(supId==0){
    			showMsg("alert","温馨提示：请选择推荐中标供应商！");
    		}else{
	    		var rcId  = document.getElementById("rcId").value;
	    		window.location.href="saveOneBidAward_bidAward.action?supplierId="+supId
  							+"&rcId="+rcId;
    			}
    }
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							
							<div class="row">
								<div class="col-xs-12">
								    <input type="hidden" id="rcId" name="rcId" value="${rcId }"/>
									<table width="100%" class="table_ys1">
									    <tr>
						    	          <td colspan="5" class="Content_tab_style_td_head">授标信息</td>
						    	        </tr>
										<tr class="Content_tab_style_04">
										    <th width="5%"  nowrap>选择</th>
											<th width="5%" nowrap>
												序号
											</th>
											<th width="40%" nowrap>
												供应商
											</th>
											<th width="40%" nowrap>
												总报价
											</th>
											<th width="10%" nowrap>
												税率
											</th>
										</tr>
										<c:forEach items="${listValue}" var="bidPrice" varStatus="status">
											<tr align="center" class="<c:if test="${not empty bidPrice.bidPrice }"> red</c:if>'">
												<td><input type="radio" id="rcdSup" name="radSup" value="${bidPrice.supplierId}"/></td>
					    	                    <td>${status.index+1}</td>
												<td>${bidPrice.supplierName}</td>
												<td align="right"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></td>
												<td>${bidPrice.taxRate}</td>
											</tr>
										</c:forEach>
									</table>
									<div class="buttonDiv">
										<button class="btn btn-success" id="btn-save" onclick="save();"><i class="icon-white icon-ok-sign"></i>授标</button>
										<button class="btn btn-danger"  type="button" onclick="api.close();"><i class="icon-white icon-remove-sign"></i>关闭</button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>