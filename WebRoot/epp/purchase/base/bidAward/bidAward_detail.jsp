<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>供应商授标信息查看</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
    </script>
</head>
<body>
<form class="defaultForm" id="supInfo" name="" method="post" action="">
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable">			
    	    <tr>
    	       <td colspan="8" class="Content_tab_style_td_head" align="center">授标信息</td>
    	    </tr>
				<tr>
					<td colspan="2" align="center">授标总价：</td>
					<td colspan="6">
					    <fmt:formatNumber value="${bidAward.bidPrice }" pattern="#,##0.00"/>
					</td>
				</tr>		
				 <tr>
					<td colspan="2" align="center">税率（%）：</td>
					<td colspan="6">
						${bidAward.taxRate }
					</td>
				</tr>
				<tr id="tdNum" align="center" class="Content_tab_style_04">
					<th width="5%" nowrap>序号</th>
					<th width="95px" nowrap>编码</th>
					<th width="15%" nowrap>名称 </th>
					<th width="25%" nowrap>规格型号 </th>
					<th width="10%" nowrap>计量单位</th>
					<th width="55px" nowrap>数量</th>
					<th width="55px" nowrap>授标价</th>
					<th width="100px" nowrap>小计</th>
				</tr>
				<c:forEach items="${badList}" var="bidAwardDetail" varStatus="status">
					<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>
						   ${status.index+1}
						</td>
						<td>
						    ${bidAwardDetail.requiredCollectDetail.buyCode}
						</td>
						<td>
						   ${bidAwardDetail.requiredCollectDetail.buyName}
						</td>
						<td>
						   ${bidAwardDetail.requiredCollectDetail.materialType}
						</td>
						<td>
						   ${bidAwardDetail.requiredCollectDetail.unit}
					    </td>
						<td align="right">
					     ${bidAwardDetail.awardAmount}
						 </td>
					    <td align="right">
					       <fmt:formatNumber value="${bidAwardDetail.price}" pattern="#,##0.00"/>
					    </td>
						<td align="right">
						   <fmt:formatNumber value="${bidAwardDetail.price*bidAwardDetail.awardAmount}" pattern="#,##0.00"/>
						</td>
					</tr>
				</c:forEach>
								  		
        </table>  
     </div>  
	</div>
</form>
	</body>
</html>