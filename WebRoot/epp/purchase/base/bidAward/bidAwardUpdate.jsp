<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>授标</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
  var api = frameElement.api, W = api.opener, cDG;
  var orderState="${requiredCollect.orderState}";
  var rcId="${requiredCollect.rcId}";
  var processId = "${requiredCollect.processId}";
  var orderId = "${requiredCollect.orderId}";
  var instanceUrl = "${requiredCollect.instanceUrl}";
  function recodSupplier(rcId){
	   createdetailwindow("授标单个供应商","saveOneInitBidAward_bidAward.action?rcId="+rcId,1);
	}
  function moreSupplier(rcId){
       createdetailwindow("授标多个供应商","saveMoreInitBidAward_bidAward.action?rcId="+rcId,1);
  }
  function btnNext(rcId){
       location.href="updateBidAwardMonitor_bidAward.action?rcId="+rcId;
  };
  function doView(baId,supplierName){
	   createdetailwindow("查看供应商授标信息","viewBidAwardDetail_bidAward.action?baId="+baId,1);
	}
  function processSubmit(){
  	    	if(orderState == "0"){
  	    		showMsg("alert","温馨提示:此项目流程已结束,请选择将要提交的信息!");
  	    	}else if(orderState == "1"){
  	    		showMsg("alert","温馨提示:此项目流程正在运行,请选择将要提交的信息!");
  	    	}else{
  	    	   var orderNo="${work_flow_type}"+rcId;
  	    	   createdetailwindow("采购结果流程申请","<%=path%>"+instanceUrl+"?processId="+processId+"&orderId="+orderId+"&orderNo="+orderNo);
  	    	       
  	    	}
	  	}
	  	
	  	//撤销流程审批
	function processCancel(){
	    	if(orderState == "0"){
   	    		$.dialog.alert("温馨提示:此项目流程已结束,不能撤销!");
   	    		return false;
   	    	}else{
   	    		var action = "deleteCancelPurchaseResultProcess_bidworkflow.action";
				var param = "requiredCollect.rcId="+rcId;
				var result = ajaxGeneral(action,param,"text");
				if(parseInt(result)==0){
					showMsg("alert","温馨提示：流程撤销成功！");
					window.location.reload(); 
				}else if(parseInt(result)==1){
					showMsg("alert","温馨提示：流程编号不存在！");
					return ;
				}else if(parseInt(result)==2){
					showMsg("alert","温馨提示：没有提交流程，不能撤销！");
					return ;
				}else if(parseInt(result)==3){
					showMsg("alert","温馨提示：流程已经处理，不能撤销！");
					return ;
				}else{
					return ;				
				}
    		}
	}
		//弹出一个新窗口
	function openWorkFlowWindow(action, rcId, processId){
		 rcId="${work_flow_type}"+rcId;
		 $.ajax({
			url:"<%=path%>/findWfOrder_workflow.action",
			type:"POST",
			dataType: 'json',
			async:false, 
			data:{orderNo:rcId, processId:processId},
			success:function(msg){
				var rsb = msg;
				var rs = rsb.order;
				if(rs.length > 0){
					createdetailwindow("查看流程",action+rs[0].id,0);
				}else{
					return;
				}
			},
			error:function(){
				
			}
		});
		
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row-fluid">
								<div class="span12">
							     <div class="btn-toolbar">
							           <c:if test="${bidpurchaseresultWorkflow!='0'}">
							              <button type="button" class="btn btn-info" id="btn-add" onclick="recodSupplier(${rcId})"><i class="icon-white icon-road"></i>授标单个供应商</button>
				                          <button type="button" class="btn btn-primary" id="btn-parity" onclick="moreSupplier(${rcId})"><i class="icon-white icon-indent-left"></i>授标多个供应商</button>
				                          <button type="button" class="btn btn-success" id="btn-next" onclick="btnNext(${rcId})"><i class="icon-white icon-resize-small"></i> 执行下一步</button>
				                       </c:if>
							           <c:if test="${bidpurchaseresultWorkflow=='0'}">
							              <c:if test="${requiredCollect.orderState==3||empty requiredCollect.orderState}">
				                          <button type="button" class="btn btn-info" id="btn-add" onclick="recodSupplier(${rcId})"><i class="icon-white icon-road"></i>授标单个供应商</button>
				                          <button type="button" class="btn btn-primary" id="btn-parity" onclick="moreSupplier(${rcId})"><i class="icon-white icon-indent-left"></i>授标多个供应商</button>
				                           <button type="button" class="btn btn-info" id="btn-processSubmit" onclick="processSubmit(${rcId})"><i class="icon-white icon-resize-small"></i> 提交流程</button>  <!--fa fa-chain-broken icon-resize-full -->
										  <button type="button" class="btn btn-warning" id="btn-processCancel" onclick="processCancel(${rcId})"><i class="icon-white icon-resize-full"></i> 撤销流程</button>
									     </c:if>
									     <c:if test="${requiredCollect.orderState=='0'}">
									      <button type="button" class="btn btn-success" id="btn-next" onclick="btnNext(${rcId})"><i class="icon-white icon-resize-small"></i> 执行下一步</button>
				                         </c:if>
				                       </c:if>
				                   </div>
                             </div>
                           </div>
							<c:if test="${bidpurchaseresultWorkflow=='0'}">
							<div class="row">
								<input type="hidden" id="rcId" value="${rcId }"/>
								 <div class="alert alert-block alert-success">
								        <p>
                                            <strong>
												<i class="icon-ok fa fa-check"></i>
												当前状态：<c:choose>
												<c:when test="${not empty requiredCollect.orderState}">
												<a href="##" onclick="openWorkFlowWindow('display_workflow.action?param=view&processId=${requiredCollect.processId}&orderId=','${requiredCollect.rcId }','${requiredCollect.processId}')">${requiredCollect.orderStateName}</a>
												</c:when>
												<c:otherwise>等待提交</c:otherwise>
												</c:choose>
												</strong>
                                        </p>
                                   </div>
                              </div>
                            </c:if>
							<div class="row">
								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	          <td colspan="5" class="Content_tab_style_td_head">授标信息</td>
						    	        </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="30%" nowrap>
												供应商
											</th>
											<th width="15%" nowrap>
												总报价
											</th>
											<th width="25%" nowrap>
												授标价
											</th>	
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${listValue}" var="bidPrice" varStatus="status">
											<tr align="center" class="<c:if test="${not empty bidPrice.bidPrice }">red</c:if>">
												<td>${status.index+1}</td>
												<td>${bidPrice.supplierName}</td>
												<td align="right"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></td>
												<td align="right"><fmt:formatNumber value="${bidPrice.bidPrice}" pattern="#00.00#"/></td>
												<td><c:if test="${not empty bidPrice.bidPrice }"><button class='btn btn-mini btn-primary' type="button"  onclick="doView(${bidPrice.baId},'${bidPrice.supplierName}')"><i class="icon-white icon-bullhorn"></i></button></c:if></td>												
												</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>