<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>授标多个供应商</title>
	<script src="<%=path%>/common/script/context.js" type="text/javascript" ></script>
	<script type="text/javascript">
	var api = frameElement.api, W = api.opener;
	$(function (){
		//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		       api.get("dialog").window.location.href=api.get("dialog").window.location.href;
		       api.close();
		       }
		 </c:if>
	    
	    
	});
    	
	function suppChange(obj,index){
		var rcId=document.getElementById("rcId").value;
		var rcdId=document.getElementById("rcdId"+index).value;
		var supplierId=obj.value;
		if(supplierId!=""){
		   var result = ajaxGeneral("getPriceDetailSupplier_bidAward.action","rcId="+rcId+"&rcdId="+rcdId+"&supplierId="+supplierId,"text");
		   document.getElementById("span"+index).innerHTML=result;//单价
		   document.getElementById("price"+index).value=result;//单价
		}
	}
	
	
	function save()
	{
	    var falg=true;
   		var supplierIds = document.getElementsByName("supplierId");
   		for(var i=0;i<supplierIds.length;i++){
   			if(supplierIds[i].value==""){
   			    falg=false;
   				showMsg("alert","温馨提示：请选择授标的供应商！");
   				break;
   			}
   		}
   		if(falg) document.form.submit();
	}
    </script>
</head>
 
<body >
<form method="post" name="form" action="saveMoreBidAward_bidAward.action">
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					
					<div class="row">
					<input type="hidden" id="rcId" name="rcId" value="${rcId }"/>
	
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys1">
						<tr class="Content_tab_style_04" >
					     	<th nowrap>序号</th>
					     	<th nowrap>编码 </th>
							<th nowrap>名称 </th>
							<th nowrap>规格型号 </th>
						    <th nowrap>计量单位</th>
					        <th nowrap>数量</th>
					        <th nowrap>授标供应商</th>
					        <th nowrap>授标价</th>
			       	    </tr>
			       	    <c:forEach items="${rcdList}" var="requiredCollectDetail" varStatus="status">
			       	    <tr>
					    	<td>${status.index+1}</td>
			    			<td>${requiredCollectDetail.buyCode}</td>
							<td>${requiredCollectDetail.buyName}</td>
							<td>${requiredCollectDetail.materialType}</td>
							<td>${requiredCollectDetail.unit}</td>
						    <td>${requiredCollectDetail.amount}</td>
			    			<td>
								<select name="supplierId" onclick="suppChange(this,${status.index+1})" id="select${status.index+1}">
								    <option value="">--请选择--</option>
									<c:forEach items="${invList}" var="inv">
									<option value="${inv.supplierId }">${inv.supplierName }</option>
									</c:forEach>
								</select>
							</td>
					    	<td><span id="span${status.index+1}"></span>
					    	<input type="hidden" name="rcdId" value="${requiredCollectDetail.rcdId }"  id="rcdId${status.index+1}"/>
					    	<input type="hidden" name="price" value=""  id="price${status.index+1}"/>
					    	<input type="hidden" name="amount" value="${requiredCollectDetail.amount }"  id="amount${status.index+1}"/></td>
					    	</tr>
			       	    </c:forEach>    	
						</table>
						<div class="buttonDiv">
							<button class="btn btn-success" type="button" id="btn-save" onclick="save();"><i class="icon-white icon-ok-sign"></i>授标</button>
							<button class="btn btn-danger"  type="button" onclick="api.close();"><i class="icon-white icon-remove-sign"></i>关闭</button>
						</div>
		     	
	</div>
	</div>
	</div>
	</div>
</form>
</body>
</html>
 
