<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>网上报价</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 
 function doView(baId,supplierName){
	   createdetailwindow("查看供应商授标信息","viewBidAwardDetail_bidAward.action?baId="+baId,1);
	}
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">

								<div class="col-xs-12">
									<div class="row">
								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	          <td colspan="5" class="Content_tab_style_td_head">授标信息</td>
						    	        </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="30%" nowrap>
												供应商
											</th>
											<th width="15%" nowrap>
												总报价
											</th>
											<th width="25%" nowrap>
												授标价
											</th>	
											<th width="10%" nowrap>
												操作
											</th>
										</tr>
										<c:forEach items="${listValue}" var="bidPrice" varStatus="status">
											<tr align="center" class="<c:if test="${not empty bidPrice.bidPrice }">red</c:if>">
												<td>${status.index+1}</td>
												<td>${bidPrice.supplierName}</td>
												<td align="right"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></td>
												<td align="right"><fmt:formatNumber value="${bidPrice.bidPrice}" pattern="#00.00#"/></td>
												<td><c:if test="${not empty bidPrice.bidPrice }"><button class='btn btn-mini btn-primary' type="button"  onclick="doView(${bidPrice.baId},'${bidPrice.supplierName}')"><i class="icon-white icon-bullhorn"></i></button></c:if></td>												
												</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>