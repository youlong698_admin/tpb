<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>新建磋商信息</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
       <c:if test="${message!=null}">
		   window.onload=function(){ 
		   api.get("dialog").window.location.reload();
		   api.close();
		  }
       </c:if>
       function save(){
         var supplierIds="",rcdIds="",briIds="";
         $("input[name='supplierId']:checkbox:checked").each(function(){  
           supplierIds+=$(this).val()  
         }); 
         if(supplierIds==""){
           showMsg("alert","温馨提示：请选择磋商范围！");
           return false;
         }
         $("input[name='rcdId']:checkbox:checked").each(function(){  
           rcdIds+=$(this).val()  
         });
         $("input[name='briId']:checkbox:checked").each(function(){  
           briIds+=$(this).val()  
         });
          if(rcdIds==""&&briIds==""){
           showMsg("alert","温馨提示：报价信息和商务信息必须选择其中一项进行磋商！");
           return false;
         }
         document.forms[0].action="saveNegotiate_bidNegotiate.action";
         document.forms[0].submit();
       }
    </script>
</head>
<body>
<form class="defaultForm" id="bidNegotiate" name="bidNegotiate" method="post" action="">
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable" style="margin-top: 30px">	    
            <tr>
				 <td width="30%" class="Content_tab_style1">截止回应时间：</td>
				<td width="70%" class="Content_tab_style2">
					<input type="text" name="negotiateReturnDateStr" datatype="*" nullmsg="截止回应时间不能为空！" id="negotiateReturnDate" class="Wdate" 
                        value="" onfocus="validateDate('negotiateReturnDate')" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm' })" /><font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">截止回应时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    	    </tr>        		
    	    <tr>
    	       <td colspan="2" class="Content_tab_style_td_head" align="center">磋商范围</td>
    	    </tr>
    	    <tr>
    	      <td colspan="2">
    	         <table class="table_ys2">
		    	     <tr id="tdNum" align="center" class="Content_tab_style_04">
		    	        <th width="30px" nowrap>选择</th>
						<th width="30px" nowrap>序号</th>
						<th nowrap>供应商</th>
					 </tr>
					 <c:forEach items="${isList}" var="inviteSupplier" varStatus="status">
						<tr  class="input_ys1">
						    <td>
							  <input type="checkbox" name="supplierId" id="supplierId" value="${inviteSupplier.supplierId}"/>
							</td>
							<td>
							   ${status.index+1}
							</td>
							<td>
							   ${inviteSupplier.supplierName}
							</td>
						</tr>
					</c:forEach>
				</table>
    	      </td>
    	    </tr>		
    	    <tr id="priceTitle">
    	       <td colspan="2" class="Content_tab_style_td_head" align="center">报价信息</td>
    	    </tr>
    	    <tr id="priceContent">
    	      <td colspan="2">
    	        <table class="table_ys2">
		    	     <tr id="tdNum" align="center" class="Content_tab_style_04">
		    	        <th width="30px" nowrap>选择</th>
						<th width="30px" nowrap>序号
							   <input type="hidden" name="indexArr"  id="indexArr" value="-1"/></th>
						<th width="95px" nowrap>编码</th>
						<th width="10%" nowrap>名称 </th>
						<th width="10%" nowrap>规格型号 </th>
						<th width="10%" nowrap>计量单位</th>
						<th width="55px" nowrap>数量</th>
						<th width="55px" nowrap>当前报价最低价</th>
						<th width="55px" nowrap>期望价格</th>
					</tr>
					<c:forEach items="${rcdList}" var="requiredCollectDetail" varStatus="status">
						<tr  class="input_ys1">
						     <td>
							  <input type="checkbox" name="rcdId" id="rcdId" value="${requiredCollectDetail.rcdId}"/>
							</td>
							<td>
							   ${status.index+1}
							</td>
							<td>
							    ${requiredCollectDetail.buyCode}
							</td>
							<td>
							   ${requiredCollectDetail.buyName}
							</td>
							<td>
							   ${requiredCollectDetail.materialType}
							</td>
							<td>
							   ${requiredCollectDetail.unit}
						    </td>
						    <td align="right">
						       ${requiredCollectDetail.amount}		     
							</td>
						    <td align="right">
						       ${requiredCollectDetail.lowestPrice}				       
							</td>
							<td>
							 <input type="text"  style="width:150px;background-color: yellow" name="expectPrice_${requiredCollectDetail.rcdId}"  id="expectPrice_${requiredCollectDetail.rcdId}" value=""
										onblur="validateNum(this);"/>
							<input type="hidden"  name="lowestPrice_${requiredCollectDetail.rcdId}"  id="lowestPrice_${requiredCollectDetail.rcdId}" value="${requiredCollectDetail.lowestPrice}"/>
							</td>
						</tr>
					</c:forEach>
    	        
    	        </table>
    	      </td>
    	    </tr>    	    
    	    <tr id="responseTitle">
    	       <td colspan="2" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
    	    </tr>
    	    <tr id="responseContent">
    	      <td colspan="2">
    	        <table align="center" class="table_ys2">
			 <tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="30px" nowrap>选择</th>
				<th width="10%" nowrap>序号</th>
				<th width="20%" nowrap>响应项名称</th>
				<th width="30%" nowrap>响应项要求 </th>
				<th nowrap>期望响应 </th>
			</tr>
			<c:forEach items="${briList}" var="businessResponseItems" varStatus="status">
			 <tr>
			     <td>
					<input type="checkbox" name="briId" id="briId" value="${businessResponseItems.briId}"/>
				</td>
				<td align="center">
				   ${status.index+1}
				</td>
				<td>
				    ${businessResponseItems.responseItemName}
				</td>
				<td>
				   ${businessResponseItems.responseRequirements}
				</td>
				<td>
				  <input type="text"  style="width:150px;background-color: yellow" id="expectResponse_${businessResponseItems.briId}" name="expectResponse_${businessResponseItems.briId }" value=""/>
				</td>
				</tr>
		   </c:forEach>	  		
        </table>
    	      </td>
    	    </tr>
							  		
        </table>
             
         <div class="buttonDiv">
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>发起磋商</button>	
				<button class="btn btn-danger"  type="button" onclick="api.close();"><i class="icon-white icon-remove-sign"></i>关闭</button>
		</div>
     </div>  
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			save();
			return false;	
		}
	});
})
</script>
	</body>
</html>