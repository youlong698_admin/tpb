<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>新增中标通知书</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
   <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script language="javaScript">
 var api = frameElement.api, W = api.opener, cDG;
  <c:if test="${message!=null}">
		   window.onload=function(){ 
		   api.get("dialog").window.location.reload();
		   api.close();
		  }
  </c:if>
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="BidResultNotice"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
 var fileNameData=[];
 var uuIdData=[];
 var fileTypeData=[];
 var attIdData=[];
  	$(function (){
  	   $('#btn-save').click(function () {
  	        var supplierId=$("#supplierId").val();
  	        if(supplierId==""){
	  	        showMsg("alert","温馨提示：请选择中标供应商！");
	  	        return false;
  	        }
			$("#fileNameData").val(fileNameData);
		    $("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
		    var action = "saveBidResultNotice_bidResultNotice.action?status=1";
			document.forms[0].action = action;
			document.forms[0].submit();
                 });
		 $('#sendButton').click(function () {
		        var supplierId=$("#supplierId").val();
	  	        if(supplierId==""){
	  	          showMsg("alert","温馨提示：请选择中标供应商！");
	  	          return false;
	  	        }
				lhgdialog.confirm("温馨提示：您确定要正式发布吗？",function(){
				    $("#fileNameData").val(fileNameData);
				    $("#uuIdData").val(uuIdData);
				    $("#fileTypeData").val(fileTypeData);
				    $("#attIdData").val(attIdData);
				    var action = "saveBidResultNotice_bidResultNotice.action?status=0";
			        document.forms[0].action = action;
			        document.forms[0].submit();
				},function(){},api);
         });
  	
  	});
  	function suppChange(obj){
		var str=obj.value;
		if(str!=""){
		   var arrStr=str.split(";");
		   document.getElementById("span").innerHTML=parseFloat(arrStr[1]);//单价
		   document.getElementById("bidPrice").value=parseFloat(arrStr[1]);//单价
		   document.getElementById("supplierId").value=arrStr[0];//供应商ID
		   document.getElementById("supplierName").value=arrStr[2];//供应商名称
		}else{
		   document.getElementById("span").innerHTML="";//单价
		   document.getElementById("bidPrice").value="";//单价
		   document.getElementById("supplierId").value="";//供应商ID
		   document.getElementById("supplierName").value="";//供应商名称
		}
	}
		
</script>
</head>
 
<body>
<form id="noticeEdit" class="defaultForm" name="bidResultNotice" method="post" action="">
<input type="hidden" name="bidResultNotice.brnId" value=""/>
<input type="hidden" name="bidResultNotice.rcId" value="${rcId}"/>
<input type="hidden" name="bidResultNotice.attIds" id="attIds"/>
<input type="hidden" name="bidResultNotice.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="bidResultNotice.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="bidResultNotice.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="bidResultNotice.attIdData" id="attIdData" value=""/>
<s:token/>			
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table width="98%" border="0" align="center"  class="table_ys2">
        	<tr>
          		<td  colspan="2" class="Content_tab_style_td_head">中标通知书</td>
        	</tr>
			<tr>
				<td class="Content_tab_style1">中标厂家：</td>
				<td class="Content_tab_style2">
					<select onclick="suppChange(this)">
					    <option value="">--请选择--</option>
						<c:forEach items="${baList}" var="bidAward">
						<option value="${bidAward.supplierId };${bidAward.bidPrice};${bidAward.supplierName }">${bidAward.supplierName }</option>
						</c:forEach>
					</select>
			   </td>
			</tr>
			<tr>
				<td class="Content_tab_style1">中标金额：</td>
				<td class="Content_tab_style2"> 
					<span id="span"></span>
					    	<input type="hidden" name="bidResultNotice.bidPrice" value=""  id="bidPrice"/>
					    	<input type="hidden" name="bidResultNotice.supplierId" value=""  id="supplierId"/>
					    	<input type="hidden" name="bidResultNotice.supplierName" value=""  id="supplierName"/>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
        </table>
        <div class="buttonDiv">
		<button class="btn btn-success"  type="button" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-success" type="button" id="sendButton" ><i class="icon-white icon-bullhorn"></i>发布</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
	</div>
</div>
</form>
</body></html>