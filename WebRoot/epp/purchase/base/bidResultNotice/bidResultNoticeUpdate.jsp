<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>中标通知书</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
 var rcId=${rcId};
 $(function(){
    $("#allcheck").bind("click", function () {
                $("[name = bcId]:checkbox").each(function () {
                    $(this).attr("checked", !$(this).attr("checked"));
                });
            });
    $("#btn-add").click(function(){
	   createdetailwindow("新增中标通知书","saveBidResultNoticeInit_bidResultNotice.action?rcId="+rcId,1);
	});
	$("#btn-edit").click(function(){
	  var brnId,count=0;
	  $("input[name='brnId']:checkbox:checked").each(function(){ 
        brnId=$(this).val();
        count++;
      });
       if(count < 1){
      		showMsg("alert","温馨提示：请选择将要修改的信息！");
      	}else if(count > 1){
	    	showMsg("alert","温馨提示：只能选择一条信息修改！");
	    }else{
	      var status=$("#status"+brnId).val(); 
	      if(status != ""){
    	    	showMsg("alert","温馨提示:此信息已经正式对供应商发布,不能修改!");
    	    	return false;
    	   }else{
    	        createdetailwindow("编辑中标通知书","updateBidResultNoticeInit_bidResultNotice.action?bidResultNotice.brnId="+brnId,1);
    	   }
	   }
	});
	$("#btn-del").click(function(){
	   var ids="",count=0,brnId;
	  $("input[name='brnId']:checkbox:checked").each(function(){ 
	    brnId=$(this).val();
        ids+=$(this).val();
        count++;
        var status=$("#status"+brnId).val(); 
	      if(status != ""){
    	    	showMsg("alert","温馨提示:此信息已经正式对供应商发布,不能修改!");
    	    	return false;
    	   }
      });
      if(count < 1){
      		showMsg("alert","温馨提示：请选择将要删除的信息！");
      	}else{
	        $.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
		    var result = ajaxGeneral("deleteBidResultNotice_bidResultNotice.action","ids="+ids);
			   showMsg('success',''+result+'',function(){
			   		window.location.href=window.location.href;							
   					});
		   	 	});
	   }
	});
    $("#btn-next").click(function(){
	   location.href="updateBidResultNoticeMonitor_bidResultNotice.action?rcId="+rcId;
	});
	})
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
				     <div class="btn-toolbar">
				                    <button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i>新增</button>
							        <button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							        <button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
							
				                    <button type="button" class="btn btn-success" id="btn-next"><i class="icon-white icon-resize-small"></i> 采购完成</button>
					 </div>
                           </div>
                           </div>
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="7" class="Content_tab_style_td_head">中标通知书</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												<input type="checkbox" name="allcheck" id="allcheck">
											</th>
											<th width="5%" nowrap>
												序号
											</th>
											<th width="30%" nowrap>
												供应商名称
											</th>
											<th width="20%" nowrap>
												中标金额
											</th>
											<th width="10%" nowrap>
												发布时间
											</th>
											<th width="10%" nowrap>
												附件下载
											</th>
											<th width="10%" nowrap>
												状态
											</th>
										</tr>
										<c:forEach items="${bidResultNoticeList}" var="bidResultNotice" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td><input type="checkbox" name="brnId" value="${bidResultNotice.brnId}"></td>
												<td>${status.index+1}</td>
												<td>${bidResultNotice.supplierName}</td>
												<td align="right"><fmt:formatNumber value="${bidResultNotice.bidPrice }" pattern="#00.00#"/></td>	
												<td><fmt:formatDate value="${bidResultNotice.publishDate}" type="both" pattern="yyyy-MM-dd" /></td>
												<td><c:out value="${bidResultNotice.attachmentUrl}" escapeXml="false"/></td>
												<td><input type="hidden" id="status${bidResultNotice.brnId }" value="${bidResultNotice.publisher }"/>
                                                  <c:choose>
                                                    <c:when test="${not empty bidResultNotice.publisher }">已发布</c:when>
                                                    <c:otherwise>未发布</c:otherwise>
                                                  </c:choose>
                                                </td>	
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>