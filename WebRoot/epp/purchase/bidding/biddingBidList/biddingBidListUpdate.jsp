<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>编制竞价方案</title>
	<link href="<%=path%>/common/jQuery/chosen/1.1.0/chosen.min.css" rel="stylesheet"/>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
$(function(){
    <c:if test="${supplierType=='01'}">
	//移除供应商
	$('.search-choice').live('click',function(){
		var that=$(this);
		var type=that.attr('type');
		var supplierIds=$("#supIds").val();
		var supTypes=$("#supTypes").val();
		$("#supIds").val(supplierIds.replace(","+supplierId+",",","));
		$("#supTypes").val(supTypes.replace(","+type+",",","));
		that.remove();
	});
	//此方法执行是如果编辑页面，需要加载显示供应商
	doReturn();
	</c:if>
})

function openSupplierWindow(){
    var ul =$('#supIds').val();
    var rcId =$('#rcId').val();
	 if(ul.length==0 || ul==","){
		 ul=-1;
	 }else{
	 	ul=ul.substring(1,ul.lastIndexOf(","));
	 }
	 createdetailwindow_choose("选择供应商","viewInviteSupplier_supplierSelect.action?ul="+ul+"&rcId="+rcId,1);
}
function doReturn(){
    var reValue=$('#returnVals').val();
    $("#supIds").val("");
    $("#supTypes").val("");
    $("#suppliersChosen").html("");
	var supIds=",",supTypes=",";
		if(reValue!=null&&reValue!=""){
			var mArr = reValue.split(",");
			for(var i=0;i<mArr.length;i++){
				if(mArr[i]!=""){
					nArr = mArr[i].split(":");
					supIds += nArr[0]+",";
					supTypes += nArr[2]+",";
					$('<li class="search-choice" supplier-id="'+nArr[0]+'" type="'+nArr[2]+'"><span>'+nArr[1]+'</span><a class="search-choice-close" ></a></li>')
							.appendTo('#suppliersChosen');
				}
			}
		}
		$("#supIds").val(supIds);
		$("#supTypes").val(supTypes);
} 
var num = "${fn:length(businessResponseItemsList)}",trialBidRoundNum="${fn:length(trialBidRounds)}",bidRoundNum="${fn:length(bidRounds)}";
//商务响应项table操作
function addRow(){
    var tr=document.getElementById("businessTable").insertRow();
    num++;    
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
    var cell3 = tr.insertCell();
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this)'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML="<input type='text' name='briList["+num+"].responseItemName' id='responseItemName_"+num+"' value=''>" ;
	cell3.innerHTML="<input type='text' id='responseRequirements_"+num+"' value='' name='briList["+num+"].responseRequirements'/>" ;
	
	cell1.align="center";
	}
	//删除一行
	function deleteRow(obj){
		$(obj).parent().parent().remove(); 
	}
//试竞价轮次table操作
function addTrialBidRoundRow(){
    var tr=document.getElementById("trialBidRoundTable").insertRow();
    trialBidRoundNum++;
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
    var cell3 = tr.insertCell();
    var cell4 = tr.insertCell();
	cell1.innerHTML=""
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteTrialBidRoundRow(this)'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML="第"+trialBidRoundNum+"轮<input type='hidden' name='trialLc' value='"+trialBidRoundNum+"'>" ;
	cell3.innerHTML="<input type='text'  value='' name='trialTimeLong' id='trialTimeLong'  onblur='checkInt2(this);'/>" ;
	cell4.innerHTML="<input type='text'  value='' name='trialPriceCount' id='trialPriceCount'  onblur='checkInt(this);'/>" ;
    
    cell1.align="center";
}
	//删除一行
	function deleteTrialBidRoundRow(obj){
		$(obj).parent().parent().remove(); 
		var targetTable=document.getElementById("trialBidRoundTable");
		for (var i = 0; i<targetTable.rows.length; i++) { 
	    if (i != 0)                  
	       targetTable.rows[i].cells[1].innerHTML = "第"+i+"轮<input type='hidden' name='trialLc' value='"+i+"'>";   
	       } 
		}
//正式竞价轮次table操作
function addBidRoundRow(){
    var tr=document.getElementById("bidRoundTable").insertRow();
    bidRoundNum++;
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
    var cell3 = tr.insertCell();
    var cell4 = tr.insertCell();
	cell1.innerHTML=""
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteBidRoundRow(this)'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML="第"+bidRoundNum+"轮<input type='hidden' name='lc' value='"+bidRoundNum+"'>" ;
	cell3.innerHTML="<input type='text'  value='' name='timeLong' id='timeLong'  onblur='checkInt2(this);'/>" ;
	cell4.innerHTML="<input type='text'  value='' name='priceCount' id='priceCount'  onblur='checkInt(this);'/>" ;
    
    cell1.align="center";
}
	//删除一行
	function deleteBidRoundRow(obj){
		$(obj).parent().parent().remove(); 
		var targetTable=document.getElementById("bidRoundTable");
		for (var i = 0; i<targetTable.rows.length; i++) { 
	    if (i != 0)                  
	       targetTable.rows[i].cells[1].innerHTML = "第"+i+"轮<input type='hidden' name='lc' value='"+i+"'>";   
	       } 
		}
	 //设置竞价管理员
   function selectUser(){
	   createdetailwindow_choose("设置竞价管理员","viewDeptIndex_userTree.action",1);
		
   }
   function valueEvaluateUser(){
	   
		var users=$("#returnValues").val();
		//alert(users)
		if(users!=""){
			var ur = users.split(",");
			for(var i=0;i<ur.length;i++){
				var u = ur[i].split(":");
				document.getElementById("bidAdminCn").value=u[1];
				document.getElementById("bidAdmin").value=u[0];	
			}
		}
	}
	function checkInt2(obj){
		var a = obj.value;
	  	if(a !="" && (isNaN(a) || a < 0 || a.indexOf(".")!=-1)){
	  		showMsg("alert","请输入正整数！");
	  		obj.focus();
	  		return false;
	  	}
	  	if(a==""){
	  		obj.innerText = "1";
	  	}
    }
    function checkInt(obj){
		var a = obj.value;
	  	if(a !="" && (isNaN(a) || a < 0 || a.indexOf(".")!=-1)){
	  		showMsg("alert","请输入正整数！");
	  		obj.focus();
	  		return false;
	  	}
	  	if(a==""){
	  		obj.innerText = "";
	  	}
    }
    function selectBiddingType(){
      var biddingType=$("#biddingType").val();
      if(biddingType=="0"){
         document.getElementById("pricePrincipleSelect").options[0].selected = true;
         document.getElementById("pricePrinciple").value="0";
      }else{
         document.getElementById("pricePrincipleSelect").options[1].selected = true;
         document.getElementById("pricePrinciple").value="1";
      }
    }
</script>
  
</head>
 
<body>
<form class="defaultForm" id="save_BiddingList" name="save_BiddingList" method="post" action="updateBiddingBidList_biddingBidList.action">
<input type="hidden" name="supIds" id="supIds" value="${supplierIds }" />
<input type="hidden" name="supTypes" id="supTypes" value="${supplierTypes }" />
<input type="hidden" name="biddingBidList.rcId" id="rcId" value="${biddingBidList.rcId}" />
<input type="hidden" name="biddingBidList.bblId" id="bblId" value="${biddingBidList.bblId}" />
<input type="hidden" name="biddingBidList.writer" id="writer" value="${biddingBidList.writer }" />
<input type="hidden" name="biddingBidList.writeDate" id="writeDate" value="<fmt:formatDate value="${biddingBidList.writeDate}" type="date" pattern='yyyy-MM-dd HH:mm:ss'/>" />
<input type="hidden" name="biddingBidList.remark1" id="remark1" value="${biddingBidList.remark1 }" />
<input type="hidden" name="biddingBidList.remark2" id="remark2" value="${biddingBidList.remark2 }" />
<input type="hidden" name="biddingBidList.remark3" id="remark3" value="${biddingBidList.remark3 }" />
<input type="hidden" name="biddingBidList.remark4" id="remark4" value="${biddingBidList.remark4 }" />
<input type="hidden" name="biddingBidList.pricePrinciple" id="pricePrinciple" value="${biddingBidList.pricePrinciple }"/>
<input type="hidden" id="returnVals" name="returnVals" value="${returnVals}"/>
<input type="hidden" name="returnValues" id="returnValues"/>
<!-- 防止表单重复提交 -->
<s:token/>		

<div class="Conter_Container">
    <div class="row-fluid">
    	<table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">竞价方案</td>
    	    </tr>
    	     <tr>
				<td width="15%" class="Content_tab_style1">项目负责人：</td>
				<td width="35%" class="Content_tab_style2">
					<input class="Content_input_style1" datatype="*" nullmsg="项目负责人不能为空！" type="text" id='responsibleUser' name="biddingBidList.responsibleUser" value="${biddingBidList.responsibleUser }"/><font color="#FF0000">*</font>
					<div class="info"><span class="Validform_checktip">项目负责人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">负责人手机号：</td>
				<td width="35%" class="Content_tab_style2">
				    <input class="Content_input_style1" datatype="m" nullmsg="负责人手机号不能为空！" placeholder="手机号用于接收项目的供应商报名及回标提醒信息" type="text" id='responsiblePhone' name="biddingBidList.responsiblePhone" value="${biddingBidList.responsiblePhone }"/><font color="#FF0000">*</font>
					<div class="info"><span class="Validform_checktip">负责人手机号不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">竞价方式：</td>
				<td width="35%" class="Content_tab_style2">
				    <select name="biddingBidList.biddingType" id="biddingType" onchange="selectBiddingType();">
				       <c:forEach items="${biddingTypeMap}" var="map">
				          <option value="${map.key }" <c:if test="${biddingBidList.biddingType==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>	
				</td>
				<td width="15%" class="Content_tab_style1">报价显示方式：</td>
				<td width="35%" class="Content_tab_style2">
				    <select  name="biddingBidList.priceMode" id="priceMode">
				       <c:forEach items="${priceModeMap}" var="map">
				          <option value="${map.key }" <c:if test="${biddingBidList.priceMode==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>	
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价原则：</td>
				<td width="35%" class="Content_tab_style2">
				    <select  id="pricePrincipleSelect" disabled="disabled">
				       <c:forEach items="${pricePrincipleMap}" var="map">
				          <option value="${map.key }" <c:if test="${biddingBidList.pricePrinciple==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>	
				</td>
				<td width="15%" class="Content_tab_style1">竞价原则：</td>
				<td width="35%" class="Content_tab_style2">
				    <select name="biddingBidList.biddingPrinciple" id="biddingPrinciple">
				       <c:forEach items="${biddingPrincipleMap}" var="map">
				          <option value="${map.key }" <c:if test="${biddingBidList.biddingPrinciple==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>	
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">竞价开始时间：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" name="biddingBidList.biddingStartTimeStr" datatype="*" nullmsg="竞价开始时间不能为空！" id="biddingBidList.biddingStartTime" class="Wdate" 
                        value="<fmt:formatDate value="${biddingBidList.biddingStartTime}" pattern="yyyy-MM-dd HH:mm" />" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm', minDate:'%y-%M-%d %H:%m',maxDate:'#F{$dp.$D(\'biddingBidList.biddingEndTime\')}' })" /><font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">竞价开始时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束时间：</td>
				<td width="35%" class="Content_tab_style2">
					<input type="text" name="biddingBidList.biddingEndTimeStr" datatype="*" nullmsg="竞价结束时间不能为空！" id="biddingBidList.biddingEndTime" class="Wdate" 
                        value="<fmt:formatDate value="${biddingBidList.biddingEndTime}" pattern="yyyy-MM-dd HH:mm" />" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'biddingBidList.biddingStartTime\')}' })" /><font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">竞价结束时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
               <td width="15%" class="Content_tab_style1">竞价管理员：</td>
				<td width="35%" class="Content_tab_style2">
					<input name="bidAdminCn" datatype="*" nullmsg="竞价管理员不能为空！"  errormsg="竞价管理员不能为空！" type="text" class="Content_input_style1"
						id="bidAdminCn" value="${biddingBidList.bidAdminCn }" readonly/><font color="#ff0000">*</font>
				    <div class="info"><span class="Validform_checktip">竞价管理员不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				    <input name="biddingBidList.bidAdmin" type="hidden" id="bidAdmin" value="${biddingBidList.bidAdmin }"/> 
				    <img src="<%=basePath %>/images/select.gif" title="选择开标管理员" onclick="selectUser()"  />
				</td>
				<td width="15%" class="Content_tab_style1">最高限价：</td>
				<td width="35%" class="Content_tab_style2">
					<input name="biddingBidList.hignPrice" datatype="n |/^(0|[1-9][0-9]*)+(.[0-9]{1,2})?$/" placeholder="竞价时限制供方输入的最高限价" ignore="ignore" errormsg="最高限价格式不正确，只能是数字！！" onkeyup="value=value.replace(/[^\d\.]/g,'');"  type="text"  value="${biddingBidList.hignPrice}"  class="Content_input_style1">
					<div class="info"><span class="Validform_checktip">最高限价格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
            </tr>
            <tr>
               <td width="15%" class="Content_tab_style1">最低限价：</td>
				<td width="35%" class="Content_tab_style2">
					<input name="biddingBidList.minPrice" type="text" datatype="n |/^(0|[1-9][0-9]*)+(.[0-9]{1,2})?$/"  placeholder="竞价时限制供方输入的最低限价" ignore="ignore" errormsg="最低限价格式不正确，只能是数字！！" onkeyup="value=value.replace(/[^\d\.]/g,'');"  class="Content_input_style1" value="${biddingBidList.minPrice}">
				    <div class="info"><span class="Validform_checktip">最低限价格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束延迟时间(分钟)：</td>
				<td width="35%" class="Content_tab_style2">
					<input name="biddingBidList.delayTime" type="text" datatype="n |/^[0-9]*[1-9][0-9]*$/" placeholder="为空则不延迟" ignore="ignore" errormsg="竞价结束延迟时间格式不正确，只能是数字！" onkeyup="value=value.replace(/[^\d\.]/g,'');" class="Content_input_style1" value="${biddingBidList.delayTime}">
				    <div class="info"><span class="Validform_checktip">竞价结束延迟时间格式不正确，只能是数字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>				
            </tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价类型：</td>
				<td width="35%" class="Content_tab_style2">
				    <select name="biddingBidList.priceType" id="priceType">
				       <c:forEach items="${priceTypeMap}" var="map">
				          <option value="${map.key }" <c:if test="${biddingBidList.priceType==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>	
				</td>
				<td width="15%" class="Content_tab_style1">报价列类型：</td>
				<td width="35%" class="Content_tab_style2">
				   <select name="biddingBidList.priceColumnType" id="priceColumnType">
				       <c:forEach items="${priceColumnTypeMap}" var="map">
				          <option value="${map.key }" <c:if test="${biddingBidList.priceColumnType==map.key }">selected</c:if>>${map.value }</option>
				       </c:forEach>
				    </select>
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">竞价管理员权限：</td>
				<td width="35%" class="Content_tab_style2">
				        <input type="checkbox" value="1" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'1')}">checked</c:if> name="biddingAdminRights"/>供应商报价
						<input type="checkbox" value="2" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'2')}">checked</c:if> name="biddingAdminRights"/>当前最低(高)报价
						<input type="checkbox" value="3" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'3')}">checked</c:if> name="biddingAdminRights"/>报价排序表
						<input type="checkbox" value="4" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'4')}">checked</c:if> name="biddingAdminRights"/>报价明细图
				</td>
				<td width="15%" class="Content_tab_style1">供应商权限：</td>
				<td width="35%" class="Content_tab_style2">
				       <input type="checkbox" value="2" <c:if test="${fn:contains(biddingBidList.supplierRights,'2')}">checked</c:if> name="supplierRights" />供应商报价
					   <input type="checkbox" value="3" <c:if test="${fn:contains(biddingBidList.supplierRights,'3')}">checked</c:if> name="supplierRights" />当前最低(高)报价
				</td>
			</tr>			
			<tr>
				<td width="15%" class="Content_tab_style1">备注：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   <textarea name="biddingBidList.remark" id="biddingBidList.remark"  rows="2" class="Content_input_style2" >${biddingBidList.remark }</textarea>
				</td>
			</tr>
			<tr>
    	       <td colspan="4" class="Content_tab_style_td_head">试竞价轮次设置</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="trialBidRoundTable" class="table_ys1" width="100%">
    	              <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>操作<img src="<%=basePath%>/images/add.gif" title="添加试竞价轮次"
										onclick="addTrialBidRoundRow();"/></th>
						<th width="20%" nowrap>轮次</th>
						<th width="20%" nowrap>时长（分钟） </th>
						<th width="50%" nowrap>限制供应商报价次数(<font color="red">如不限制，则不填写</font>) </th>
					  </tr>
				      <c:forEach var="biddingBidRound" items="${trialBidRounds}">
				         <tr>
				            <td align="center"><input type="hidden" name="rowIndex" value="${status.index}"/>
						        <button class='btn btn-mini btn-danger' type="button"  onclick='deleteTrialBidRoundRow(this)'><i class="icon-white icon-trash"></i></button>
						    </td>
				            <td>第${biddingBidRound.biddingRound }轮<input type="hidden" name="trialLc" value="${biddingBidRound.biddingRound }"/></td>
				            <td><input type="text" name="trialTimeLong" id="trialTimeLong" value="<fmt:formatNumber value="${biddingBidRound.timeLong/60 }" pattern="0"/>"   onblur="checkInt2(this);"/></td>
				            <td><input type="text" name="trialPriceCount" id="trialPriceCount" value="${biddingBidRound.priceCount }"   onblur="checkInt(this);"/></td>
				         </tr>
				     </c:forEach>
				    </table>
				  </td>
		      </tr>		
		      <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">正式竞价轮次设置</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="bidRoundTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>操作<img src="<%=basePath%>/images/add.gif" title="添加正式竞价轮次"
										onclick="addBidRoundRow();"/></th>
						<th width="20%" nowrap>轮次</th>
						<th width="20%" nowrap>时长（分钟） </th>
						<th width="50%" nowrap>限制供应商报价次数(<font color="red">如不限制，则不填写</font>) </th>
					  </tr>
				      <c:forEach var="biddingBidRound" items="${bidRounds}">
				         <tr>
				            <td align="center"><input type="hidden" name="rowIndex" value="${status.index}"/>
						        <button class='btn btn-mini btn-danger' type="button"  onclick='deleteBidRoundRow(this)'><i class="icon-white icon-trash"></i></button>
						    </td>
				            <td>第${biddingBidRound.biddingRound }轮<input type="hidden" name="lc" value="${biddingBidRound.biddingRound }"/></td>
				            <td><input type="text" name="timeLong" id="timeLong" value="<fmt:formatNumber value="${biddingBidRound.timeLong/60 }" pattern="0"/>"   onblur="checkInt2(this);"/></td>
				            <td><input type="text" name="priceCount" id="priceCount" value="${biddingBidRound.priceCount }"   onblur="checkInt(this);"/></td>
				        </tr>
				     </c:forEach>
				    </table>
				  </td>
		      </tr>	
			<c:if test="${supplierType=='01'}">
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">竞价参与供应商</td>
    	      </tr>
			  <tr>
				<td width="15%" class="Content_tab_style1">参与供应商：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					
						<div class="chosen-container-multi">
									<ul class="chosen-choices" id="suppliersChosen">
									</ul>
								</div>
								<div id="chosen-img">
									<img src="<%=basePath%>/images/select.gif" title="选择供应商"
										onclick="openSupplierWindow();"/>
								</div>
				</td>
			  </tr>
			 </c:if>
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">商务响应项</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="businessTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>操作<img src="<%=basePath%>/images/add.gif" title="添加商务响应项"
										onclick="addRow();"/></th>
						<th width="30%" nowrap>响应项名称</th>
						<th width="60%" nowrap>响应项要求 </th>
					</tr>
					<c:choose>
					   <c:when test="${fn:length(businessResponseItemsList)==0&&(empty biddingBidList.bblId)}">
					           <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="0"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_0' name='briList[0].responseItemName'  value='付款方式'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_0' value='' name='briList[0].responseRequirements' /> 
								</td>
								</tr>
								<tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="1"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_1' name='briList[1].responseItemName'  value='交货时间'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_1' value='' name='briList[1].responseRequirements' /> 
								</td>
								</tr>
					            <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="2"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_2' name='briList[2].responseItemName'  value='发票要求'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_2' value='' name='briList[2].responseRequirements' /> 
								</td>
								</tr>
								<tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="3"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td>
								    <input type='text' id='responseItemName_3' name='briList[3].responseItemName'  value='收货地'/>
								</td>
								<td>
								   <input type='text' id='responseRequirements_3' value='' name='briList[3].responseRequirements' /> 
								</td>
								</tr>
					   </c:when>
					   <c:otherwise>
						<c:forEach items="${businessResponseItemsList}" var="businessResponseItems" varStatus="status">
						 <tr  class="input_ys1">
							<td align="center"><input type="hidden" name="rowIndex" value="${status.index}"/>
							    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
							</td>
							<td>
							    <input type='text' id='responseItemName_${status.index}' name='briList[${status.index}].responseItemName'  value='${businessResponseItems.responseItemName}'/>
							</td>
							<td>
							   <input type='text' id='responseRequirements_${status.index}' value='${businessResponseItems.responseRequirements}' name='briList[${status.index}].responseRequirements' /> 
							</td>
							</tr>
					     </c:forEach>
					     </c:otherwise>
					     </c:choose>
    	            </table>
    	         </td>
    	      </tr>
			  		
        </table>
        
        <div class="buttonDiv">
		<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
     </div>  
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeCheck:function(curform){
		    selectBiddingType();
			<c:if test="${supplierType=='01'}">
			var inviteSup = $("#supIds").val();
			if(inviteSup==','){
				showMsg("alert","温馨提示：邀请供应商不能为空！");
				return false;
			}else
			{
				return true;
			}
			</c:if>
			
		}
	});
})
</script>
</body>
</html>