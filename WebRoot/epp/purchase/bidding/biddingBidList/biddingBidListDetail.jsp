<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>竞价方案查看</title>	
	<link href="<%=path%>/common/jQuery/chosen/1.1.0/chosen.min.css" rel="stylesheet"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>		
	<script type="text/javascript">
    var api = frameElement.api, W = api.opener, cDG;
    $(function(){
    <c:if test="${supplierType=='01'}">
	//此方法执行是如果编辑页面，需要加载显示供应商
	doReturn();
	</c:if>
})
function doReturn(){
    var reValue=$('#returnVals').val();
    $("#supIds").val("");
    $("#suppliersChosen").html("");
	var supIds=",";
		if(reValue!=null&&reValue!=""){
			var mArr = reValue.split(",");
			for(var i=0;i<mArr.length;i++){
				if(mArr[i]!=""){
					nArr = mArr[i].split(":");
					supIds += nArr[0]+",";
					$('<li class="search-choice" supplier-id="'+nArr[0]+'"><span>'+nArr[1]+'</span></li>')
							.appendTo('#suppliersChosen');
				}
			}
		}
		$("#supIds").val(supIds);
}
        </script>
</head>
<body>
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="supIds" id="supIds" value="${supplierIds }" />
		<input type="hidden" id="returnVals" name="returnVals" value="${returnVals}"/>
    	<table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">竞价方案</td>
    	    </tr> 
    	    <tr>
				<td width="15%" class="Content_tab_style1">项目负责人：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.responsibleUser }
				</td>
				<td width="15%" class="Content_tab_style1">负责人手机号：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.responsiblePhone }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">竞价方式：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.biddingTypeCn }
				</td>
				<td width="15%" class="Content_tab_style1">报价显示方式：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.priceModeCn }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价原则：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.pricePrincipleCn }
				</td>
				<td width="15%" class="Content_tab_style1">竞价原则：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.biddingPrincipleCn }
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">竞价开始时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${biddingBidList.biddingStartTime}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${biddingBidList.biddingEndTime}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>
            <tr>
				<td width="15%" class="Content_tab_style1">竞价管理员：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.bidAdminCn }
				</td>
				<td width="15%" class="Content_tab_style1">最高限价：</td>
				<td width="35%" class="Content_tab_style2">
				    ${biddingBidList.hignPrice}
				</td>
			</tr><tr>
				<td width="15%" class="Content_tab_style1">最低限价：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.minPrice}
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束延迟时间(分钟)：</td>
				<td width="35%" class="Content_tab_style2">
				    ${biddingBidList.delayTime}
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价类型：</td>
				<td width="35%" class="Content_tab_style2">
				    ${askBidList.priceTypeCn }
				</td>
				<td width="15%" class="Content_tab_style1">报价列类型：</td>
				<td width="35%" class="Content_tab_style2">
				   ${askBidList.priceColumnTypeCn }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">竞价管理员权限：</td>
				<td width="35%" class="Content_tab_style2">
				        <input type="checkbox" value="1" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'1')}">checked</c:if> name="biddingAdminRights" disabled="disabled"/>供应商报价
						<input type="checkbox" value="2" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'2')}">checked</c:if> name="biddingAdminRights" disabled="disabled"/>当前最低(高)报价
						<input type="checkbox" value="3" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'3')}">checked</c:if> name="biddingAdminRights" disabled="disabled"/>报价排序表
						<input type="checkbox" value="4" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'4')}">checked"</c:if> name="biddingAdminRights" disabled="disabled"/>报价明细图
				</td>
				<td width="15%" class="Content_tab_style1">供应商权限：</td>
				<td width="35%" class="Content_tab_style2">
				       <input type="checkbox" value="2" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'2')}">supplierRights"</c:if> name="supplierRights" disabled="disabled"/>供应商报价
					   <input type="checkbox" value="3" <c:if test="${fn:contains(biddingBidList.biddingAdminRights,'3')}">supplierRights"</c:if> name="supplierRights" disabled="disabled"/>当前最低(高)报价
				</td>
			</tr>				
			<tr>
				<td width="15%" class="Content_tab_style1">备注：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   ${askBidList.remark }
				</td>
			</tr>
			<tr>
    	       <td colspan="4" class="Content_tab_style_td_head">试竞价轮次设置</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="trialBidRoundTable" class="table_ys1" width="100%">
    	              <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>序号</th>
						<th width="20%" nowrap>轮次</th>
						<th width="20%" nowrap>时长（分钟） </th>
						<th width="50%" nowrap>限制供应商报价次数(<font color="red">如不限制，则不填写</font>) </th>
					  </tr>
				      <c:forEach var="biddingBidRound" items="${trialBidRounds}" varStatus="status">
				         <tr>
				            <td align="center">
				             ${status.index+1}
				             </td>
				            <td>第${biddingBidRound.biddingRound }轮</td>
				            <td>${biddingBidRound.timeLong/60 }</td>
				            <td>${biddingBidRound.priceCount }</td>
				         </tr>
				     </c:forEach>
				    </table>
				  </td>
		      </tr>		
		      <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">正式竞价轮次设置</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="bidRoundTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>序号</th>
						<th width="20%" nowrap>轮次</th>
						<th width="20%" nowrap>时长（分钟） </th>
						<th width="50%" nowrap>限制供应商报价次数(<font color="red">如不限制，则不填写</font>) </th>
					  </tr>
				      <c:forEach var="biddingBidRound" items="${bidRounds}" varStatus="status">
				         <tr>
				            <td align="center">
						        ${status.index+1}
						    </td>
				            <td>第${biddingBidRound.biddingRound }轮</td>
				            <td>${biddingBidRound.timeLong/60 }</td>
				            <td>${biddingBidRound.priceCount }</td>
				        </tr>
				     </c:forEach>
				    </table>
				  </td>
		      </tr>	
			<c:if test="${supplierType=='01'}">
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">竞价参与供应商</td>
    	      </tr>
			  <tr>
				<td width="15%" class="Content_tab_style1">竞价供应商：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					
						<div class="chosen-container-multi">
									<ul class="chosen-choices" id="suppliersChosen">
									</ul>
								</div>
				</td>
			  </tr>
			 </c:if>
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">商务响应项</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="businessTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>序号</th>
						<th width="30%" nowrap>响应项名称</th>
						<th width="60%" nowrap>响应项要求 </th>
					</tr>
					<c:forEach items="${businessResponseItemsList}" var="businessResponseItems" varStatus="status">
					 <tr>
						<td align="center">
						   ${status.index+1}
						</td>
						<td>
						    ${businessResponseItems.responseItemName}
						</td>
						<td>
						   ${businessResponseItems.responseRequirements}
						</td>
						</tr>
				</c:forEach>
    	            </table>
    	         </td>
    	      </tr>
			  		
        </table>
        	
     </div>  
	</div>
	</body>
</html>