<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>竞价信息</title>
	<link href="<%=path%>/style/default.css" rel="stylesheet" type="text/css" />
</head>
  
  <body>
    <c:choose>
       <c:when test="${num==0}">
         <table border="0" width="95%" align="center" cellpadding="0"
			cellspacing="0" class="table_ys1" style="vertical-align: top;">
			<thead>
				<tr class="Content_tab_style_04">
					<td nowrap  width="20%">
						消息发布时间
					</td>
					<td nowrap width="20%">
						发布人
					</td>
					<td nowrap width="60%">
						消息内容
					</td>
				</tr>
			</thead>
			<c:forEach items="${bciList}" var="bci">
			   <tr>
				<td nowrap><fmt:formatDate value="${bci.publishDate}" pattern="HH:mm" /></td>
				<td nowrap>${bci.publisher }</td>
				<td>${bci.info }</td>
			</tr>
			</c:forEach>
           </table>
       </c:when>
       <c:when test="${num==1}">
           <table border="0" width="95%" align="center" cellpadding="0"
			cellspacing="0" class="table_ys1" style="vertical-align: top;">
			<thead>
				<tr class="Content_tab_style_04">
					<td nowrap  width="40%">
						供应商名称
					</td>
					<td nowrap width="30%">
						当前最低价格
					</td>
					<td nowrap width="30%">
						报价时间
					</td>
					<th width="10%" nowrap>
						操作
					</th>
				</tr>
			</thead>
			<c:forEach items="${bpList}" var="bp">
			   <tr>
				<td nowrap>${bp.supplierName }</td>
				<td align="right"><fmt:formatNumber value="${bp.totalPrice }" pattern="#,##0.00"/></td>
				<td nowrap><fmt:formatDate value="${bp.writeDate}" pattern="HH:mm" /></td>
				<td><button class='myViewButton' type="button"  onclick='doView(${bp.bpId})'><i class="icon-white icon-bullhorn"></i>查看</button></td>		
			   </tr>
			</c:forEach>
           </table>
       </c:when>
       <c:when test="${num==2}">
	       <div align="center" style="padding-top: 5px;">${title }：
						<font size="6" color="red"><fmt:formatNumber value="${price }" pattern="#,##0.00"/></font>
		   </div>
       </c:when>
       <c:when test="${num==3}">
          <c:set var="data_tmp" value=""/>
           <table border="0" width="95%" align="center" cellpadding="0"
			cellspacing="0" class="table_ys1" style="vertical-align: top;">
			<thead>
				<tr class="Content_tab_style_04">
					<td nowrap  width="50%">
						供应商名称
					</td>
					<td nowrap width="50%">
						本轮次的最后报价
					</td>
				</tr>
			</thead>
			<c:forEach items="${bphList}" var="obj">
			   <c:set var="data" value="${obj[2]}"/>
			    <c:if test="${data_tmp==''}">
				     <tr>
  	                   <td colspan="3" class="Content_tab_style_td_head" align="center">第${data }轮</td>
  	                 </tr>						
			          <c:set var="data_tmp" value="${data}"/>
				</c:if>	
				 <c:if test="${data_tmp!=''&&data_tmp!=data}">	
				     <tr>
  	                  <td colspan="3" class="Content_tab_style_td_head" align="center">第${data }轮</td>
  	                 </tr>						
			        <c:set var="data_tmp" value="${data}"/>	
				 </c:if>
			   <tr>
				<td nowrap>${obj[1] }</td>
				<td align="right"><fmt:formatNumber value="${obj[0] }" pattern="#,##0.00"/></td>
			   </tr>
			</c:forEach>
			</table>
       </c:when>
       
       <c:otherwise>
           <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	    <div id="main" style="height:280px"></div>
	    <!-- ECharts单文件引入 -->
		<script src="<%= path %>/common/echarts/echarts.min.js"></script>
	    <script type="text/javascript">
	        // 基于准备好的dom，初始化echarts图表
	        var myChart = echarts.init(document.getElementById('main')); 
	        
	        var option = {
			    title : {
			        text: '供应商报价折线图'
			    },
			    tooltip : {
			        trigger: 'axis',
			        axisPointer:{
			            show: true,
			            type : 'cross',
			            lineStyle: {
			                type : 'dashed',
			                width : 1
			            }
			        },
			        formatter : function (params) {
			            return params.seriesName + ' : [ '
			                   + params.value[0] + ', ' 
			                   + params.value[1] + ' ]';
			        }
			    },
			    legend: {
			        data:[${supplierNames}]
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            dataView : {show: true, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type: 'value',
			            show:true,
			            name:'分钟'
			        }
			    ],
			    yAxis : [
			        {
			            type: 'value',
			            show:true,
			            name:'报价',
			            axisLine: {
			                lineStyle: {
			                    color: '#dc143c'
			                }
			            }
			        }
			    ],
			    series : [
			       ${data}
			    ]
			};
	
	        // 为echarts对象加载数据 
	        myChart.setOption(option); 
	    </script>
       </c:otherwise>
    </c:choose>
  </body>
</html>
