<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>竞价现场</title>
	<link href="<%=path%>/style/default.css" rel="stylesheet" type="text/css" />
	<script src="<%= path %>/common/jQuery/jquery-1.8.3.min.js" type="text/javascript" ></script>
	<script src="<%= path %>/common/lhgDialog/lhgdialog.js?skin=bootstrap2" type="text/javascript" ></script>
	<script src="<%= path %>/common/script/common.js" type="text/javascript" ></script>
	<link href="<%=path%>/style/bidding.css" rel="stylesheet" type="text/css" />
	<link href="<%=path%>/style/tab.css" rel="stylesheet" type="text/css" />
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
  <script language="javascript"> 
        var api = frameElement.api, W = api.opener, cDG;
	    var t1,t2;
	    var status,lcmc,lcmc_next,endTime,isDelayBid,isDelay,isTrialBid,temp_endTime,currBiddingRound;
	    var trialBiddingRound=${biddingBidList.trialBiddingRound},biddingRound=${biddingBidList.biddingRound};
	    var rcId=${biddingBidList.rcId};
	    var bblId=${biddingBidList.bblId};
	    var biddingType="${biddingBidList.biddingType}";
	    var str_time = "";
		$(function(){    
			    t1=window.setInterval(function (){
			    var result=ajaxGeneral("viewBiddingBidListStrtus_biddingBidScene.action","bblId="+bblId+"&rcId="+rcId,"json");
			    if(result.info=="error"){
			      //Error();
			    }
                status=result.status;
                isDelay = result.isDelay;
                isTrialBid = result.isTrialBid;
                currBiddingRound=result.currBiddingRound;
                bbrId=result.bbrId;
                //轮次名称
                if(isTrialBid==0){
                	lcmc="试竞价第"+currBiddingRound+"轮";
                	lcmc_next="试竞价第"+(Number(currBiddingRound)+1)+"轮";
                }else{
                	lcmc="第"+currBiddingRound+"轮";
                	lcmc_next="第"+(Number(currBiddingRound)+1)+"轮";
                }
                //根据状态显示不同的信息
                if(status==0){
                  var s = "<div>";
                      if(isTrialBid==0){
                      s+="<input type=\"button\" class=\"an06\" value="+lcmc+"开始"+" onClick=\"doZsjj("+bblId+",'"+rcId+"',0)\">"
                      }else{
                      s+="<input type=\"button\" class=\"an06\" value="+lcmc+"开始"+" onClick=\"doZsjj("+bblId+",'"+rcId+"',1)\">"
                      }
                      s+="<div class=\"clear\"></div><div>";
				      s+="		时长："+result.timeLong+"秒";
				      s+="</div><div id=\"djs\"></div></div>";
				      document.getElementById("lcxx").innerHTML = s;
               }else if(status==1||status==4){
               //debugger;
                  isDelayBid=result.isDelayBid;                
                  endTime=result.endTime;
                  temp_endTime = endTime;
                  if(endTime<=0){
			       	var delayTime=document.getElementById("delayTime").value; //延迟时间
			       	 //最后一轮的竞价轮次等于当前的竞拍轮次并且是否延迟竞价为1，是否需要延迟为0，是否试竞价为0		 
			       	if(result.isDelayBid==1&&isDelay==0&&biddingRound==currBiddingRound&&isTrialBid==1){
						 doYc(bbrId,rcId,delayTime);
			        }else if(result.isDelayBid==1&&isDelay==0&&trialBiddingRound==currBiddingRound&&isTrialBid==0){
						 doYc(bbrId,rcId,delayTime);
			        }else{
			             doStop(bbrId,bblId,rcId);
			        }
                 }
              if(endTime>0){  
                 //取出间隔时间的天、小时、分,java中时间换成毫秒是按照1000*60*60*24这样的换算方式转的
		       int_day=Math.floor(endTime/86400000);
		       endTime=endTime-int_day*86400000;
		       int_hour=Math.floor(endTime/3600000);
		       endTime=endTime-int_hour*3600000;
		       int_minute=Math.floor(endTime/60000);
		       endTime=endTime-int_minute*60000;
		       int_second=Math.floor(endTime/1000);
		       
		       if(int_hour<10)
		       int_hour="0"+int_hour;
		       if(int_minute<10)
		       int_minute="0"+int_minute;
		       if(int_second<10)
		       int_second="0"+int_second;
		       str_time=int_hour+":"+int_minute+":"+int_second;
		         var _t = "";
		         if(isDelayBid==0){
			       if(isTrialBid == 0){
			    	   _t = "试竞价";
				   }else{
					   _t = "正式竞价";
					}
                 document.getElementById("lcxx").innerHTML ="<div>"
                 +"<input type=\"button\" class=\"an06\" value=\""+_t+"延时竞价\" disabled>"
				 +"<div class=\"clear\"></div><div>"
				 +"		时长："+result.timeLong+"秒"
				 +"<div style=\"font-weight: bold; font-size: 16px; color: red;\">以下时间为延长时间</div>"
				 +"</div><div id=\"djs\">倒计时：<div id=\"timer\""
				 +"style=\"font-weight: bold; font-size: 28px; color: #ff8209;\">"+str_time+"</div></div>"
				  +"<div><input type=\"button\" class=\"an07\" value=\"暂停\" onClick=\"doBreak('"+bbrId+"');\" id=\"breakbutton\">"
				 +"<input type=\"button\" class=\"an05\" value=\"重新开始\" onClick=\"ReStart('"+bbrId+"');\" disabled ></div></div>";
                
                 }else{
                  document.getElementById("lcxx").innerHTML ="<div>"
                 +"<input type=\"button\" class=\"an06\" value=\""+lcmc+"\" disabled>"
				 +"<div class=\"clear\"></div><div>"
				 +"		时长："+result.timeLong+"秒"
				 +"</div><div id=\"djs\">倒计时：<div id=\"timer\""
				 +"style=\"font-weight: bold; font-size: 28px; color: #ff8209;\">"+str_time+"</div></div>"
				 +"<div><input type=\"button\" class=\"an07\" value=\"暂停\" onClick=\"doBreak('"+bbrId+"');\" id=\"breakbutton\">"
				 +"<input type=\"button\" class=\"an05\" value=\"重新开始\" onClick=\"ReStart('"+bbrId+"');\" disabled ></div></div>";
                 
                 }
                 }                 
                 }else if(status==3){
				 if(isTrialBid==0 && currBiddingRound ==trialBiddingRound){
					 document.getElementById("lcxx").innerHTML ="<div>"
	                      +"<input type=\"button\" class=\"an06\" value=\"试竞价活动已结束,进入正式竞价\" onClick=\"doZsjj("+bblId+","+rcId+",1)\">"
	        		      +"</div>";
       		         $("#leftTip").html("试竞价活动已结束");
				 }else if(isTrialBid==0 && currBiddingRound !=trialBiddingRound){
					 document.getElementById("lcxx").innerHTML ="<div>"
		                 +"<input type=\"button\"  class=\"an06\" value=\""+lcmc+"结束,进入"+lcmc_next+"\" onClick=\"doZsjj("+bblId+","+rcId+",0);\">"
						 +"<div class=\"clear\"></div><div id=\"djs\"></div></div>";
					 $("#leftTip").html(lcmc + "结束");
			     }else if(isTrialBid==1  && currBiddingRound ==biddingRound){
			    	 document.getElementById("lcxx").innerHTML ="<div>"
                         +"<input type=\"button\" class=\"an06\" value=\"竞价活动已结束\" disabled>";
        				 +"</div>";
        				 $("#leftTip").html("竞价活动已结束");
			     }else if(isTrialBid==1 && currBiddingRound !=biddingRound){
			    	 document.getElementById("lcxx").innerHTML ="<div>"
		                 +"<input type=\"button\" class=\"an06\" value=\""+lcmc+"已结束,进入"+lcmc_next+"\" onClick=\"doZsjj("+bblId+","+rcId+",1)\">"
						 +"<div class=\"clear\"></div><div id=\"djs\"></div></div>";
			    	 $("#leftTip").html(lcmc + "已结束");
				 }	
                  document.getElementById("xmxxdiv").style.display="none";
				  document.getElementById("jjtsxxdiv").style.display="block";
                  document.getElementById("supplierName").innerHTML =result.supplierName;
				  document.getElementById("price").innerHTML =result.price;
				  if(result.isBidEnd=="0"){
				     setTimeout(function(){clearInterval(t1)},2000);
	    	         setTimeout(function(){clearInterval(t2)},2000);	
				  }
                 }else if(status==2){             
                 
                 document.getElementById("lcxx").innerHTML ="<div>"
                 +"<input type=\"button\" class=\"an06\" value=\""+lcmc+"暂停\" disabled>"
				 +"<div class=\"clear\"></div><div>"
				 +"		时长："+result.timeLong+"秒"
				  +"</div>"
				 +"<div class=\"clear\"></div><div>"
				 +"		剩余时间："+result.surplusTime+"秒"
				  +"</div>"
				  +"<div><input type=\"button\" class=\"an07\" value=\"暂停\" onClick=\"doBreak("+bbrId+","+rcId+");\" disabled>"
				 +"<input type=\"button\" class=\"an05\" value=\"重新开始\" onClick=\"ReStart("+bbrId+","+rcId+");\"></div></div>";
                 }
             },1000);
             
		  t2=window.setInterval(BiddingMain,1000);
		});
		
		function BiddingMain(){
			   var num=0;
	    	   if(document.getElementById("num")) num = document.getElementById("num").value;
		       var result=ajaxGeneral("getBidMain_biddingBidScene.action","rcId="+rcId+"&num="+num+"&biddingType="+biddingType,"html");
		       $("#bidMain").html(result);

			}
	    function Error()
	    {
	    	clearInterval(t1);
	    	clearInterval(t2);
		   // showMsg("alert","网络异常，请核查");
	    }
	    
		function doYc(bbrId,rcId,delayTime){
		      var result=ajaxGeneral("updateBiddingBidListDelay_biddingBidScene.action","bbrId="+bbrId+"&rcId="+rcId+"&delayTime="+delayTime,"text");
			  if(result=="success"){
		         window.location.reload();
		      }		
		}
		
		function doZsjj(bblId,rcId,sign){
		      var result=ajaxGeneral("updateBiddingBidListStart_biddingBidScene.action","bblId="+bblId+"&rcId="+rcId+"&sign="+sign);
			  if(result=="success"){
		         window.location.reload();
		      }	
		}
		function doBreak(bbrId){
			var result=ajaxGeneral("updateBiddingBidListBreak_biddingBidScene.action","bbrId="+bbrId);
			
		}
		function ReStart(bbrId){
			 var result=ajaxGeneral("updateBiddingBidListReStart_biddingBidScene.action","bbrId="+bbrId);
			 if(result=="success"){
		         window.location.reload();
		      }	
		}
			
		function doStop(bbrId,bblId,rcId){
			 var result=ajaxGeneral("updateBiddingBidListStop_biddingBidScene.action","bbrId="+bbrId+"&bblId="+bblId+"&rcId="+rcId);			
	    	 if(result=="success"){
	    	    window.location.reload();
	    	    //setTimeout(function(){clearInterval(t1)},5000);
	    	    //setTimeout(function(){clearInterval(t2)},5000);		
	    	 }	   
		}
		function submitSsxx(){
		    var xxnr=document.bjform.ssxx.value;
			var xxdx=document.bjform.xxdx.value;
			var receive,receiver;
			if(xxnr==""){
				showMsg("alert","您还没有填写任何消息呢！");
				document.bjform.ssxx.focus();
				return;
			}else{
			   if(xxdx=="0"){
			     receive="0";
			     receiver="全部供应商";
			   }else{
			     receive=xxdx.split(",")[0];
			     receiver=xxdx.split(",")[1];
			   }
			}
			var result=ajaxGeneral("saveBiddingCommunication_biddingBidScene.action","rcId="+rcId+"&info="+xxnr+"&receive="+receive+"&receiver="+receiver);
			if(result=="success"){
				document.bjform.ssxx.value="";
				$("#kjhf_").find("option[text='']").attr("selected",true); 
			}
		}

	   function kjhf(obj){
		document.getElementById("ssxx").innerHTML = obj.value;
	   }
	   
	   function divDisplaySwitch(num){
   	    // var length=$("#ulId li").length;
   	     for(var i=0;i<5;i++){
   	       if($("#xxMain"+i).length>0){
			if(i==num){
				document.getElementById("xxMain"+i).className="hover";
				$("#num").val(num);
            }else{
				document.getElementById("xxMain"+i).className="";
			}
			}
		}
		var result=ajaxGeneral("getBidMain_biddingBidScene.action","rcId="+rcId+"&num="+num+"&biddingType="+biddingType,"html");
		$("#bidMain").html(result);
   	    } 
   	     function doView(bpId){
	        createdetailwindow("查看供应商报价","viewBiddingBidPriceResponeDetail_biddingBidScene.action?bidPrice.bpId="+bpId,1);
	     }
	     function parityPrice(rcId){
	        createdetailwindow("供应商比价","viewParityPrice_biddingBidScene.action?rcId="+rcId,1);
	     }
	     function doNext(){
	         location.href="updateBiddingBidSceneMonitor_biddingBidScene.action?rcId="+rcId;
	     }
		</script>
</head>
 
<body>
   <div id="jjdt">
    <input type="hidden" id="num" name="num" value="0"/>
	<input type="hidden" name="delayTime" id="delayTime" value="${biddingBidList.delayTime }"/>
	
	<form action="" method="post" name="bjform">
		<table width="100%">
		<tr><td  style="vertical-align: top">
		<div class="kuanhui" id="xmxxdiv">
			 <div class="palistxmxx">
<div class="palistxmxx-tel border-blue">
<h1 class="bg-blue">竞价项目信息</h1>
</div>
			
			<div class="dh1">竞价项目名称：${requiredCollect.bidCode }</div>
			<div class="dh2">竞价项目编号：${requiredCollect.buyRemark }</div>
			<div class="dh2">竞价方式：${biddingBidList.biddingTypeCn }</div>
			<div class="dh2">报价方式：${biddingBidList.priceModeCn}</div>
			<div class="dh2">报价原则：${biddingBidList.pricePrincipleCn }</div>
			<div class="dh2">竞价策略：${biddingBidList.biddingPrincipleCn }</div>
				</div> 
				<input type="hidden" id="time_end" name="time_end" />
				</div>
				
				
				<div class="kuanhui" id="jjtsxxdiv" style="display: none">
								<div class="palistxmxx">
									<div class="palistxmxx-tel border-blue">
										<h1 class="bg-blue">
											竞价结束提示信息
										</h1>
									</div>
									<div>
										<div
											style="BACKGROUND-IMAGE: url(<%=path %>/images/icon-03.png); BACKGROUND-REPEAT: no-repeat; FLOAT: left">
											<div style="padding-LEFT: 60px;">
												<div class="dh2">${requiredCollect.buyRemark }</div>
												<div class="dh2" style="color: #e00606" id="leftTip">
												
												 
												</div>
												<div class="dh2"><c:if test="${biddingBidList.biddingType=='0'}">最低</c:if><c:if test="${biddingBidList.biddingType=='0'}">最高</c:if>报价供应商：<span style="color:#ff0000" id="supplierName"></span></div>
												<div class="dh2">价格：<span style="color:#ff0000" id="price"></span></div>
											</div>
										</div>
									</div>
								</div>
							</div>
				
			<div class="kuanhui">
			<div id="ssjjxx" style="width:100%;">
                  <div id="Tab3">
						<div class="Menubox2">
							<ul id="ulId">
								<li id="xxMain0" class="hover" onClick="divDisplaySwitch(0);">
									实时消息
								</li>
								<c:if test="${fn:contains(biddingBidList.biddingAdminRights,'1')}">
								<li id="xxMain1" onClick="divDisplaySwitch(1);">
									供应商报价
								</li>
								</c:if>
								<c:if test="${fn:contains(biddingBidList.biddingAdminRights,'2')}">
								<li id="xxMain2" onClick="divDisplaySwitch(2);">
									${title }
								</li>
								</c:if>
								<c:if test="${fn:contains(biddingBidList.biddingAdminRights,'3')}">
								<li id="xxMain3" onClick="divDisplaySwitch(3);">
									报价排序表
								</li>
								</c:if>	
								<c:if test="${fn:contains(biddingBidList.biddingAdminRights,'4')}">
								<li id="xxMain4" onClick="divDisplaySwitch(4);">
									报价明细表
								</li>
								</c:if>							
							</ul>
						</div>
						<div id="bidMain" class="Contentbox">
						    <table border="0" width="95%" align="center" cellpadding="0"
								cellspacing="0" class="table_ys1" style="vertical-align: top;">
								<thead>
									<tr class="Content_tab_style_04">
										<td nowrap  width="20%">
											消息发布时间
										</td>
										<td nowrap width="20%">
											发布人
										</td>
										<td nowrap width="60%">
											消息内容
										</td>
									</tr>
								</thead>
								<c:forEach items="${bciList}" var="bci">
								   <tr>
									<td nowrap><fmt:formatDate value="${bci.publishDate}" pattern="HH:mm" /></td>
									<td nowrap>${bci.publisher }</td>
									<td>${bci.info }</td>
								</tr>
								</c:forEach>
					           </table>
						</div>
					</div>
			
			</div>
			</div>
		</td>
		<td style="vertical-align: top;width: 335px">
				<div id="lcjjxx" class="kuanhui620">
								<div class="palist">
									<div class="palist-tel border-blue">
										<h1 class="bg-blue">
											竞价轮次信息
										</h1>
									</div>
									<c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set> 
									<c:choose>
									<c:when test="${biddingBidList.biddingStartTime.time-nowDate>0}">
										<div class="pamai"  style="font-size: 15px" align="center">
												未到竞价开始时间
										</div>									
                                    </c:when>
                                    <c:otherwise>
										<div class="pamai" id="lcxx" >
												 
										</div>
									</c:otherwise>
									</c:choose>
								</div>
							</div>
				           <div class="kuanhui620"> 
				           <div class="palist">
                                <div class="palist-tel border-blue"><h1 class="bg-blue">实时消息沟通</h1>
                                </div>
                                                                       消息对象： <select name="xxdx" id="xxdx">
                                           <option value="0">全部供应商</option>
                                           <c:forEach items="${inviteSupplierList}" var="inviteSupplier">
                                              <option value="${inviteSupplier.supplierId },${inviteSupplier.supplierName }">${inviteSupplier.supplierName }</option>
                                           </c:forEach>
                                     </select> 
                                 <br/>
                                                                   快捷回复:<select onchange="kjhf(this);" style="font-size: 15px" id="kjhf_"> 
                                            <option></option>            
                                          <c:forEach items="${ssxxMap}" var="map">
                                         	<option value="${map.value }">${map.value }</option> 
                                          </c:forEach>                        
                                  </select>
                                  <br/>
						   消息内容： <textarea rows="6" id="ssxx"></textarea>
						    <div style="margin: 0 auto;text-align: center;"><input type="button" class="myButton"   value="发送" onclick="submitSsxx();"></div>
						   </div></div>
						   <div class="kuanhui620"> 
				           <div class="palist">
				            <div id="bjMain" class="BjContentbox">
						     <input type="button" class="an01" id="bj" name="bj"
														value="比价" onClick="parityPrice(${rcId});"/>
					         
					           <input type="button" class="an02" id="end" name="end"
														value="下一步" <c:if test="${biddingBidList.isBidEnd=='1'}"> disabled </c:if> onClick="doNext();"/>
					        
							</div>
							</div>
						</div>
				</td>
			
				</tr>
				</table>
				
	</form>
	</div>
  </body>
</html>
