<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>竞价现场</title>
	<link href="<%=path%>/style/default.css" rel="stylesheet" type="text/css" />
	<script src="<%= path %>/common/jQuery/jquery-1.8.3.min.js" type="text/javascript" ></script>
	<script src="<%= path %>/common/lhgDialog/lhgdialog.js?skin=bootstrap2" type="text/javascript" ></script>
	<script src="<%= path %>/common/script/common.js" type="text/javascript" ></script>
	<link href="<%=path%>/style/bidding.css" rel="stylesheet" type="text/css" />
	<link href="<%=path%>/style/tab.css" rel="stylesheet" type="text/css" />
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
  <script language="javascript"> 
        var api = frameElement.api, W = api.opener, cDG;
	    var rcId=${biddingBidList.rcId};
	    var biddingType="${biddingBidList.biddingType}";
	    function divDisplaySwitch(num){
   	     var length=$("#ulId li").length;
   	     for(var i=0;i<length;i++){
			if(i==num){
				document.getElementById("xxMain"+i).className="hover";
				$("#num").val(num);
            }else{
				document.getElementById("xxMain"+i).className="";
			}
		}
		var result=ajaxGeneral("getBidMain_biddingBidScene.action","rcId="+rcId+"&num="+num+"&biddingType="+biddingType,"html");
		$("#bidMain").html(result);
   	    } 
   	     function doView(bpId){
	        createdetailwindow("查看供应商报价","viewBiddingBidPriceResponeDetail_biddingBidScene.action?bidPrice.bpId="+bpId,1);
	     }
	     function parityPrice(rcId){
	        createdetailwindow("供应商比价","viewParityPrice_biddingBidScene.action?rcId="+rcId,1);
	     }
		</script>
</head>
 
<body>
   <div id="jjdt">
	<c:if test="${not empty info}">
		<div class="alert alert-block alert-danger">
	       <p>
	            <strong>
				<i class="icon-white icon-remove-sign"></i>
				${info }
				</strong>
	       </p>
	    </div>
	</c:if>
	
	<c:if test="${empty info}">
	<form action="" method="post" name="bjform">
		<table width="100%">
		<tr><td  style="vertical-align: top">
				<div class="kuanhui" id="jjtsxxdiv">
								<div class="palistxmxx">
									<div class="palistxmxx-tel border-blue">
										<h1 class="bg-blue">
											竞价结束提示信息
										</h1>
									</div>
									<div>
										<div
											style="BACKGROUND-IMAGE: url(<%=path %>/images/icon-03.png); BACKGROUND-REPEAT: no-repeat; FLOAT: left">
											<div style="padding-LEFT: 60px;">
												<div class="dh2">${requiredCollect.buyRemark }</div>
												<div class="dh2" style="color: #e00606" id="leftTip">
												
												 
												</div>
												<div class="dh2"><c:if test="${biddingBidList.biddingType=='0'}">最低</c:if><c:if test="${biddingBidList.biddingType=='1'}">最高</c:if>报价供应商：<span style="color:#ff0000" id="supplierName">${supplierName }</span></div>
												<div class="dh2">价格：<span style="color:#ff0000" id="price">${price }</span></div>
											</div>
										</div>
									</div>
								</div>
							</div>
				
			<div class="kuanhui">
			<div id="ssjjxx" style="width:100%;">
                  <div id="Tab3">
						<div class="Menubox2">
							<ul id="ulId">
								<li id="xxMain0" class="hover" onClick="divDisplaySwitch(0);">
									实时消息
								</li>
								<li id="xxMain1" onClick="divDisplaySwitch(1);">
									供应商报价
								</li>
								<li id="xxMain2" onClick="divDisplaySwitch(2);">
									${title }
								</li>
								<li id="xxMain3" onClick="divDisplaySwitch(3);">
									报价排序表
								</li>
								<li id="xxMain4" onClick="divDisplaySwitch(4);">
									报价明细表
								</li>						
							</ul>
						</div>
						<div id="bidMain" class="Contentbox">
						    <table border="0" width="95%" align="center" cellpadding="0"
								cellspacing="0" class="table_ys1" style="vertical-align: top;">
								<thead>
									<tr class="Content_tab_style_04">
										<td nowrap  width="20%">
											消息发布时间
										</td>
										<td nowrap width="20%">
											发布人
										</td>
										<td nowrap width="60%">
											消息内容
										</td>
									</tr>
								</thead>
								<c:forEach items="${bciList}" var="bci">
								   <tr>
									<td nowrap><fmt:formatDate value="${bci.publishDate}" pattern="HH:mm" /></td>
									<td nowrap>${bci.publisher }</td>
									<td>${bci.info }</td>
								</tr>
								</c:forEach>
					           </table>
						</div>
					</div>
			
			</div>
			</div>
		</td>
		</tr>
	</table>				
	</form>
	</c:if>
	</div>
  </body>
</html>
