<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>供应商比价</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>	
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
	<script type="text/javascript">
       var api = frameElement.api, W = api.opener, cDG;
    </script>
</head>
<body>
<form class="defaultForm" id="supInfo" name="" method="post" action="">
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
        <table align="center" class="table_ys1" id="listtable">			
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="35px" nowrap rowspan="2">序号</th>
				<th width="65px" nowrap rowspan="2">编码</th>
				<th width="100px" nowrap rowspan="2">名称 </th>
				<th width="100px" nowrap rowspan="2">规格型号 </th>
				<th width="35px" nowrap rowspan="2">计量单位</th>
				<th width="55px" nowrap rowspan="2">数量</th>
				<c:if test="${fn:length(bpIdStr)>0 }">
				<th nowrap colspan="${fn:length(bpIdStr) }">${biddingBidList.priceColumnTypeCn }</th>
				</c:if>
			</tr>
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<c:forEach items="${supplierStr}" var="supplierName" varStatus="status">		
				    <th width="100px" nowrap style="background-color:${colorStr[status.count-1]}">${supplierName }</th>
				</c:forEach>
			</tr>
			<c:forEach items="${bidPriceList}" var="list" varStatus="status">
				<tr onmouseover="style.backgroundColor='#FF9900'" onmouseout="style.backgroundColor='#FFFFFF'">
				    <td>
					   ${status.index+1}
					</td>
					<c:forEach var="value" items="${list}"  varStatus="statusList"> 
					    <c:if test="${statusList.count>5}">
				       <td style="background-color:${colorStr[statusList.count-6]}" align="right">${value }</td>
				        </c:if>
				       <c:if test="${statusList.count<=5}">
				       <td>${value }</td> 
				       </c:if>   
				    </c:forEach>
				</tr>
			</c:forEach>
			<tr style="height: 30px;">
				<td colspan="6" align="center" style="background-color: yellow">总价：</td>
				<c:forEach items="${totalPriceStr}" var="totalPrice" varStatus="status">
				    <td style="background-color:${colorStr[status.count-1]}"  align="right">
				      <fmt:formatNumber value="${totalPrice }" pattern="#,##0.00"/>
				    </td>
				</c:forEach>				
			</tr>		
			 <tr style="height: 30px;">
				<td colspan="6" align="center" style="background-color: yellow">税率（%）：</td>
				<c:forEach items="${taxRateStr}" var="taxRate" varStatus="status">
					<td style="background-color:${colorStr[status.count-1]}"  align="right">
						${taxRate }
					</td>
				</c:forEach>
			</tr>				  		
        </table>
        <c:if test="${fn:length(bbrfList)>0}">
        <table align="center" class="table_ys2">
			 <tr id="tdNum" align="center" class="Content_tab_style_04">
				<th width="35px" nowrap rowspan="2">序号</th>
				<th width="100px" nowrap rowspan="2">响应项名称</th>
				<th width="100px" nowrap rowspan="2">响应项要求 </th>
				<c:if test="${fn:length(bpIdStr)>0 }">
				<th  nowrap colspan="${fn:length(bpIdStr) }">我的响应</th>
				</c:if>
			</tr>
			<tr id="tdNum" align="center" class="Content_tab_style_04">
				<c:forEach items="${supplierStr}" var="supplierName" varStatus="status">
				    <th width="100px" nowrap style="background-color:${colorStr[status.count-1]}">${supplierName }</th>
				</c:forEach>
			</tr>
			<c:forEach items="${bbrfList}" var="list" varStatus="status">
				<tr onmouseover="style.backgroundColor='#FF9900'" onmouseout="style.backgroundColor='#FFFFFF'">
				    <td>
					   ${status.index+1}
					</td>
					<c:forEach var="value" items="${list}" varStatus="statusList"> 
					   <c:if test="${statusList.count>2}">
				       <td style="background-color:${colorStr[statusList.count-3]}">${value }</td> 
				       </c:if>
				       <c:if test="${statusList.count<=2}">
				       <td>${value }</td> 
				       </c:if> 
				    </c:forEach>
				</tr>
			</c:forEach>		
        </table>
        </c:if>   
     </div>  
	</div>
</form>
	</body>
</html>