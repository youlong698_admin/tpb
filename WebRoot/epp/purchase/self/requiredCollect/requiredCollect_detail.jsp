<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>自主采购明细信息</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">
	var api = frameElement.api, W = api.opener;
</script>
</head>
<body >
<form id="cx" method="post" action="">
	
	<table align="center"  class="table_ys1">
		<tr align="center" class="Content_tab_style_05"><th colspan="4">查看自主采购</th></tr>
		<tr >
			 <td width="15%" class="Content_tab_style1">项目名称：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.buyRemark }
			 </td>
			<td width="15%" class="Content_tab_style1">项目编号：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.bidCode }				
			 </td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>采购组织：</td>
			<td class="Content_tab_style2">
				${purchaseDeptName }
			</td>
			<td class="Content_tab_style1">立项人：</td>
			<td class="Content_tab_style2">
				${writerCn }
			</td>
		</tr>
   		<tr>
			<td class="Content_tab_style1" nowrap>立项日期：</td>
			<td class="Content_tab_style2">
				<fmt:formatDate value="${requiredCollect.writeDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
			</td>
			<td class="Content_tab_style1">立项单位：</td>
			<td class="Content_tab_style2">
				${deptName }
			</td>
			
		</tr>
		<tr>
			<td  class="Content_tab_style1">附件列表：</td>
			<td class="Content_tab_style2" colspan="3">
				<c:out value="${requiredMaterial.attachmentUrl}" escapeXml="false"/>				
			</td>
		</tr>
	</table>
	<table align="center" class="table_ys1" id="listtable">	
		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap>序号</th>
			<th width="95px" nowrap>编码</th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredCollectDetail" varStatus="status">
			<tr  class="input_ys1">
				<td>${status.index+1}
				</td>
				<td>
				    ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.buyName}
				</td>
				<td>
				   ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.unit}
			    </td>
			    <td align="right">
			       ${requiredCollectDetail.amount}
			    </td>
				<td>
					<fmt:formatDate value="${requiredCollectDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
				</td>
				<td>${requiredCollectDetail.remark }</td>
			</tr>
		</c:forEach>		
	</table>
</form>
</body>
</html>