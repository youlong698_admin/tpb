<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>价格评分规则</title>
	<script src="<%=path%>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">
$(function (){
       <c:if test="${message!=null}">
	      window.parent.showMsgFrameMsg('success','${message}',4,0);
	    </c:if>
	$('.ace').click(function(){
		if($(this).attr('checked')){
			var value = $(this).val();
			$('#up_01,#up_02,#up_03,#up_00').html("");
			if(value == "01"){//最低价满分
				$('#up_01').html("X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet' size='4' value='1'>");
			}else if(value == "02"){//平均价满分
				$('#up_02').html("X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet' size='4' value='1'>");
			}else if(value == "03"){//最高价满分
				$('#up_03').html("X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet' size='4' value='1'>");
			}else{//其他不适用以上三种格式的评分规则
				$('#up_00').html("X = 0 <input type='hidden' name='tenderBidPriceSet.upPoint' class='pointsSet' size='4' value='0' >");
			}
		}

	
	})
    $('.pointsSet').blur(function(){
		if(isNaN($(this).val()) || $.trim($(this).val()) == "0" || $.trim($(this).val()) == ""){
			window.parent.showMsgFrame("alert","温馨提示：请输入合法的系数！");
			$(this).val("1");
			$(this).focus();
		}else{
			$(this).val($.trim($(this).val()));
		}
    })
    
      $("#saveButton").click(function(){
          if(!$('.pointsSet')[0]){
				window.parent.showMsgFrame("alert","温馨提示：请先设置评分规则！");
				return false;
          }
          document.form.action = "updateTenderBidPrice_tenderBidPre.action";
		  document.form.submit(); 
      });

 });
</script>

</head>
 
<body  >
<form id="updateTenderBidPriceForm" name="form" action="" method="post">
<input type="hidden" name="tenderBidPriceSet.writer" value="${tenderBidPriceSet.writer }"/>
<input type="hidden" name="tenderBidPriceSet.writeDate" value="${tenderBidPriceSet.writeDate}" />
<input type="hidden" name="tenderBidPriceSet.rcId" value="${tenderBidPriceSet.rcId}" />
<input type="hidden" name="tenderBidPriceSet.tbpsId" value="${tenderBidPriceSet.tbpsId}" />
<!-- 防止表单重复提交 -->
<s:token/>
	
	<!-- 数据列表区域  begin-->
			<table class="table_ys2">
				<tr>
					<th  class="Content_tab_style_td_head" colspan="5">
						价格评分规则设置
					</th>
				</tr>
				<tr class="Content_tab_style_04">
					<th width="5%" nowrap>
						选择
					</th>
					<th width="10%" nowrap>
						评分规则
					</th>
					<th width="30%" nowrap>
						评分公式
					</th>
					<th width="15%">
						参数设置
					</th>
					<th width="25%" nowrap>
						备注
					</th>
				</tr>
				<tr>
					<td align="center">
						<input type="radio" class="ace" name="tenderBidPriceSet.brsType"
							value="01"
							<c:if test="${tenderBidPriceSet.brsType == '01' }">checked</c:if> />
						
					</td>
					<td>
						最低价满分
					</td>
					<td>
						<img src="<%=basePath%>/images/priceRule/minPrice.png" width="300"
							height="30" />
					</td>
					<td id="up_01">
						<c:if test="${tenderBidPriceSet.brsType == '01' }">
						X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet'
								size='4' value='<s:property value="tenderBidPriceSet.upPoint"/>' />
						</c:if>
					</td>
					<td align="left">
						&nbsp;
						<img src="<%=basePath%>/images/priceRule/minRemark.png"
							width="250" height="60" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="radio" class="ace" name="tenderBidPriceSet.brsType"
							value="02"
							<c:if test="${tenderBidPriceSet.brsType == '02' }">checked</c:if> />
						
					</td>
					<td>
						平均价满分
					</td>
					<td>
						<img src="<%=basePath%>/images/priceRule/middlePrice.png"
							width="300" height="45" />
					</td>
					<td id="up_02">
						<c:if test="${tenderBidPriceSet.brsType == '02' }">
						X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet'
								size='4' value='<s:property value="tenderBidPriceSet.upPoint"/>' />
						</c:if>
					</td>
					<td align="left">
						&nbsp;
						<img src="<%=basePath%>/images/priceRule/middleRemark.png"
							width="250" height="80" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="radio" class="ace" name="tenderBidPriceSet.brsType"
							value="03"
							<c:if test="${tenderBidPriceSet.brsType == '03' }">checked</c:if> />
						
					</td>
					<td>
						最高价满分
					</td>
					<td>
						<img src="<%=basePath%>/images/priceRule/maxPrice.png" width="300"
							height="35" />
					</td>
					<td id="up_03">
						<c:if test="${tenderBidPriceSet.brsType == '03' }">
						X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet'
								size='4' value='<s:property value="tenderBidPriceSet.upPoint"/>' />
						</c:if>
					</td>
					<td align="left">
						&nbsp;
						<img src="<%=basePath%>/images/priceRule/maxRemark.png"
							width="250" height="65" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<input type="radio" class="ace" name="tenderBidPriceSet.brsType"
							value="00"
							<c:if test="${tenderBidPriceSet.brsType == '00' }">checked</c:if> />
						
					</td>
					<td>
						其他
					</td>
					<td>
						其他
					</td>
					<td id="up_00">
						<c:if test="${tenderBidPriceSet.brsType == '00' }">
						X =<input type='text' name='tenderBidPriceSet.upPoint' class='pointsSet'
								value='<s:property value="tenderBidPriceSet.upPoint"/>' />
						</c:if>
					</td>
					<td align="left">
						&nbsp;
						<img src="<%=basePath%>/images/priceRule/otherRemark.png"
							width="250" height="65" />
					</td>
				</tr>
			</table>
			<!-- 数据列表区域  end-->
    
	<div class="buttonDiv">
		<button class="btn btn-success"  type="button" id="saveButton" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
	</div>
</form>
</body>
</html>
 
