<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>评标权重管理</title>
	<script src="<%=path%>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script type="text/javascript">
	  $(function(){
	       //返回信息
		   <c:if test="${message!=null}">
			   window.parent.showMsgFrameMsg('success','${message}',1,0);
		    </c:if>
	  })
	</script>
</head>
 
<body  >
<form class="defaultForm" id="updateTenderBidRateForm"  action="updateTenderBidRate_tenderBidPre.action" method="post">
	<input type="hidden" name="tenderBidRate.rcId" value="${tenderBidRate.rcId }"/>
	<input type="hidden" name="tenderBidRate.tbrId" value="${tenderBidRate.tbrId}"/>
<s:token/>
	
	<!-- 数据列表区域  begin-->
	<table class="table_ys2">
  			<tr>
				<th  class="Content_tab_style_td_head" colspan="4">设置标段评标权重</th>
			</tr>
  			<tr class="Content_tab_style_04" >
		        <th width="20%" nowrap>技术权重（%）</th>
		        <th width="20%" nowrap>商务权重（%）</th>
		        <th width="20%" nowrap>评标价权重（%）</th>
		        <th width="20%" nowrap>合计（%）</th>
       	    </tr>
			<tr align="center">
    			<td><input type="text" datatype="*,/^[0-9]*[.]?[0-9]*$/" nullmsg="技术权重不能为空！" errormsg="技术权重数字不合法！" class="input-medium" id="techRate" name="tenderBidRate.techRate" value="${tenderBidRate.techRate }" />
    				<div class="info"><span class="Validform_checktip">技术权重数字不合法！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    			<td><input type="text" datatype="*,/^[0-9]*[.]?[0-9]*$/" nullmsg="商务权重不能为空！" errormsg="商务权重数字不合法！"class="input-medium" id="businessRate" name="tenderBidRate.businessRate" value="${tenderBidRate.businessRate}" />
    				<div class="info"><span class="Validform_checktip">商务权重数字不合法！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    			<td><input type="text" datatype="*,/^[0-9]*[.]?[0-9]*$/" nullmsg="评标价权重不能为空！" errormsg="评标价权重数字不合法！"class="input-medium" id="priceRate" name="tenderBidRate.priceRate" value="${tenderBidRate.priceRate}" />
    				<div class="info"><span class="Validform_checktip">评标价权重数字不合法！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    			<td><span id="total">${tenderBidRate.totalRate }</span></td>
		    </tr>
    </table>
    <!-- 数据列表区域  end-->
    
	<div class="buttonDiv">
		<button class="btn btn-success" id="btn-save" type="button"><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			if (FloatAdd(FloatAdd($("#techRate").val(), $("#businessRate").val()), $("#priceRate").val())!= 100) {
				window.parent.showMsgFrame("alert","温馨提示：权重和必须为100%！");
				return false;
			}else{
				return true;
			}	
		}
	});
	
})
</script>
</body>
</html>
 
