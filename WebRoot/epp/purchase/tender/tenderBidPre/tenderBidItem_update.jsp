<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>技术评分项选择</title>
	<script src="<%=path%>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script src="<%=path%>/common/script/purchase/competeTalks/competeTalks.js" type="text/javascript" ></script> 
<script language="javascript">
var itemType1 = ${tenderBidItem.itemType};
$(function(){
//返回信息
	   <c:if test="${message!=null}">
	      window.parent.showMsgFrameMsg('success','${message}',3,itemType1);
	   </c:if>
	$('#ckAll').click(function(){
		if($('#ckAll').attr("checked")){
			$('.ck').attr("checked","checked");
		}else{
			$('.ck').removeAttr("checked");
		}
	})
	
	$('.points').keyup(function(){
		var points = $('.points');
		var sumPoints = 0;
		for(var i=0;i<points.length;i++){
			if(isNaN(points[i].value)){
				window.parent.showMsgFrame("alert","温馨提示：请输入合法的分值！");
				document.getElementById("pt"+(i+1)).value=0;
				return;
			}
			sumPoints += points[i].value*1;
		}
		$('#totalPoints').html("（总分："+sumPoints+"）");
	})

	//点击删除
	$('#deleteButton').click(function(){
		var checkBox = $('.ck');
		var ids = "";
		for(var i=0;i<checkBox.length;i++){
			if(checkBox[i].checked){
				ids += checkBox[i].value + ",";
			}
		}
		if(ids.indexOf(',')<=0){
			window.parent.showMsgFrame("alert","温馨提示：请选择要删除的评分项信息！");
			return;
		}else{
			document.forms[0].action = "deleteTenderBidItemSet_tenderBidPre.action?ids="+ids.substring(0, ids.length-1);
			document.forms[0].submit();
		}
	})
	//点击保存
	$('#btn-save').click(function(){
		var points = $('.points');
		var sumPoints = 0;
		for(var i=0;i<points.length;i++){
			if(isNaN(points[i].value)){
				window.parent.showMsgFrame("alert","温馨提示：请输入合法的分值！")
				return false;
			}
			sumPoints += points[i].value*1;
		}
		if(sumPoints != 100){
			window.parent.showMsgFrame("alert","温馨提示：总分必须为100！")
			return false;
		}
		document.forms[0].action = "updateTenderBidItemSet_tenderBidPre.action?closeMsg=1";
		document.forms[0].submit();
		
	});
	
	//评分导入
   $("#btn-itemImp").click(function(){
        var rcId = $("#rcId").val();
		var itemType = $("#itemType").val();
		var title,name,url,submitUrl;
		if(itemType==0){
			title="技术评分项信息导入";
			name="评分项信息导入模板.xls";
			url="/common/template/importTemplate/bidTecItems.xls";
		}
		if(itemType==1){
			title="商务评分项信息导入";
			name="评分项信息导入模板.xls";
			url="/common/template/importTemplate/bidBusItems.xls";
		}
		submitUrl="saveTenderBidItemsExcel_tenderBidPre.action?rcId="+rcId+"&itemType="+itemType;
		window.parent.$("#title").val(title);
		window.parent.$("#name").val(name);
		window.parent.$("#url").val(url);
		window.parent.$("#submitUrl").val(submitUrl);
        //createdetailwindow(title,"Template_ImportAction.action",2);
         window.parent.doImport(title);
	 });
});
function doSelect(rcId,itemType){
   window.parent.doSelectItem(rcId,itemType);
}
</script>
</head>
 
<body>
<form id="itemSetSave" action="" method="post">
	<input type="hidden" name="tenderBidItem.rcId" id="rcId" value="${tenderBidItem.rcId }"/>
	<input type="hidden" name="tenderBidItem.itemType" id="itemType" value="${tenderBidItem.itemType}"/>
<!-- 防止表单重复提交 -->
<s:token/>
	
	
	<!-- 数据列表区域  begin-->
			<table class="table_ys2">
				<tr>
					<th colspan="5"  class="Content_tab_style_td_head">
						<c:if test="${tenderBidItem.itemType == '0'}">技术评分设置</c:if>
						<c:if test="${tenderBidItem.itemType == '1'}">商务评分设置</c:if>
					</th>
				</tr>
				<tr id="th" class="Content_tab_style_04">
					<th width="4%" nowrap>
						<img src="images/select.gif" onclick="doSelect('${tenderBidItem.rcId}','${tenderBidItem.itemType }');" title="选择评分项"/><img src="images/delete.gif" id="deleteButton" title="删除所选评分项"/>
					</th>
					<th width="4%" nowrap>
						序号
					</th>
					<th width="20%" nowrap>
						评分项目
					</th>
					<th width="50%" nowrap>
						评分说明
					</th>
					<th width="20%" nowrap>
						评分值
						<span id="totalPoints"></span>
					</th>
				</tr>
				<c:forEach var="tenderBidItem" items="${listValue}"   varStatus="sta">
					<tr align="center">
						<td>
							<input type="checkbox" class="ck "
								value="${tenderBidItem.tbiId}" />
						</td>
						<td>
							${sta.index+1 }
							<input type="hidden"
								name="tbiList[${sta.index }].tbiId"
								value="${tenderBidItem.tbiId}" />
							<input type="hidden"
								name="tbiList[${sta.index }].rcId"
								value="${tenderBidItem.rcId}" />
							<input type="hidden"
								name="tbiList[${sta.index }].itemId"
								value="${tenderBidItem.itemId}" />
							<input type="hidden"
								name="tbiList[${sta.index }].itemType"
								value="${tenderBidItem.itemType}" />
							<input type="hidden"
								name="tbiList[${sta.index }].writer"
								value="${tenderBidItem.writer}" />
							<input type="hidden"
								name="tbiList[${sta.index }].writeDate"
								value="${tenderBidItem.writeDate}" />
						</td>
						<td>
							${tenderBidItem.itemName}
							<input type="hidden"
								name="tbiList[${sta.index }].itemName"
								value="${tenderBidItem.itemName}" />
						</td>
						<td align="left" title="${tenderBidItem.itemDetail}">
						   <c:choose>
							    <c:when test="${fn:length(tenderBidItem.itemDetail) > 35}">
									${fn:substring(tenderBidItem.itemDetail,0,35)}...
								</c:when>
								<c:otherwise>
									${tenderBidItem.itemDetail }
								</c:otherwise>
						   </c:choose>
						   <input type="hidden"
								name="tbiList[${sta.index }].itemDetail"
								value="${tenderBidItem.itemDetail}" />
						</td>
						<td>
							<input type="text" class="points"
								id="pt${sta.index+1 }"
								name="tbiList[${sta.index }].points"
								value="${tenderBidItem.points}" />
						</td>
					</tr>
				</c:forEach>
			</table>
			<!-- 数据列表区域  end-->
    
	<div class="buttonDiv">
			<button class="btn btn-success" type="button" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>			
			<button type="button" class="btn btn-info" id="btn-itemImp"><i class="icon-white icon-edit"></i> 导入评分Excel</button>
	</div>
	<!-- 操作区域  end-->
</form>
</body>
</html>
 
