<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>标前准备</title>		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
				<div class="row">

								<div class="container-fluid">
										<div class="accordion" id="accordion2">
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse"
														data-parent="#accordion2" href="#collapseOne"> 标段评标权重
													</a>
												</div>
												<div id="collapseOne" class="accordion-body collapse"
													style="height: 0px;">
													<div class="accordion-inner">
														<table width="100%" class="table_ys2">
															<tr class="Content_tab_style_04">
																<th width="25%" nowrap>
																	技术权重（%）
																</th>
																<th width="25%" nowrap>
																	商务权重（%）
																</th>
																<th width="25%" nowrap>
																	评标价权重（%）
																</th>
																<th width="25%" nowrap>
																	合计（%）
																</th>
															</tr>
															<tr class='biaoge_01_b'>
																<td>
																	${tenderBidRate.techRate }
																</td>
																<td>
																	${tenderBidRate.businessRate}
																</td>
																<td>
																	${tenderBidRate.priceRate}
																</td>
																<td>
																	${tenderBidRate.totalRate}
																</td>
															</tr>
														</table>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse"
														data-parent="#accordion2" href="#collapseTwo"> <c:if
															test="${fn:length(itemsOne)>0}"><i class="fa fa-star orange" title="已设置评分规则"></i></c:if> 商务评分规则 </a>
												</div>
												<div id="collapseTwo" class="accordion-body collapse">
													<div class="accordion-inner">
														<table width="90%" class="table_ys2">
															<tr class="Content_tab_style_04">
																<th width="2%" nowrap>
																	序号
																</th>
																<th width="10%" nowrap>
																	评分项目
																</th>
																<th width="30%" nowrap>
																	评分说明
																</th>
																<th width="5%" nowrap>
																	参考分值
																</th>
															</tr>
															<c:forEach items="${itemsOne}" var="tenderBidItem"
																varStatus="status">
																<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
																	<td>
																		${status.index+1 }
																	</td>
																	<td>
																		${tenderBidItem.itemName}
																	</td>
																	<td align="left">
																		${tenderBidItem.itemDetail}
																	</td>
																	<td>
																		${tenderBidItem.points}
																	</td>
																</tr>
															</c:forEach>
														</table>
													</div>
												</div>
											</div>											
											<div class="accordion-group">
											<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse"
														data-parent="#accordion2" href="#collapseThree"> <c:if
															test="${fn:length(itemsTwo)>0}"><i class="fa fa-star orange" title="已设置评分规则"></i></c:if>技术评分规则 </a>
												</div>
												<div id="collapseThree" class="accordion-body collapse">
													<div class="accordion-inner">
														<table width="90%" class="table_ys2">
															<tr align="center" class="Content_tab_style_04">
																<th width="2%" nowrap>
																	序号
																</th>
																<th width="10%" nowrap>
																	评分项目
																</th>
																<th width="30%" nowrap>
																	评分说明
																</th>
																<th width="5%" nowrap>
																	参考分值
																</th>
															</tr>
															<c:forEach items="${itemsTwo}" var="tenderBidItem"
																varStatus="status">
																<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
																	<td>
																		${status.index+1 }
																	</td>
																	<td>
																		${tenderBidItem.itemName}
																	</td>
																	<td align="left">
																		${tenderBidItem.itemDetail}
																	</td>
																	<td>
																		${tenderBidItem.points}
																	</td>
																</tr>
															</c:forEach>
														</table>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse"
														data-parent="#accordion2" href="#collapseFour"> <c:if
															test="${fn:length(bpss)>0}"><i class="fa fa-star orange" title="已设置评分规则"></i></c:if>价格评分规则 </a>
												</div>
												<div id="collapseFour" class="accordion-body collapse">
													<div class="accordion-inner">
														<table class="table_ys2">
															<tr class="Content_tab_style_04">
																<th width="10%" nowrap>
																	评分规则
																</th>
																<th width="30%" nowrap>
																	评分公式
																</th>
																<th width="10%">
																	扣分参数值
																</th>
																<th width="25%" nowrap>
																	备注
																</th>
															</tr>
															<c:choose>
															<c:when test="${tenderBidPriceSet.brsType == '01'}">
																<tr>
																	<td>
																		最低价满分
																	</td>
																	<td>
																		<img src="<%=basePath%>/images/priceRule/minPrice.png"
																			width="300" height="30" />
																	</td>
																	<td id="up_01">
					                                                    	X = ${tenderBidPriceSet.upPoint}
																	</td>
																	<td align="left">
																		&nbsp;
																		<img
																			src="<%=basePath%>/images/priceRule/minRemark.png"
																			width="250" height="60" />
																	</td>
																</tr>
															</c:when>
															<c:when test="${tenderBidPriceSet.brsType == '02'}">
																<tr>
																	<td>
																		平均价满分
																	</td>
																	<td>
																		<img
																			src="<%=basePath%>/images/priceRule/middlePrice.png"
																			width="300" height="45" />
																	</td>
																	<td id="up_02">
																		X =X = ${tenderBidPriceSet.upPoint}
																	</td>
																	<td align="left">
																		&nbsp;
																		<img
																			src="<%=basePath%>/images/priceRule/middleRemark.png"
																			width="250" height="80" />
																	</td>
																</tr>
															</c:when>
															<c:when test="${tenderBidPriceSet.brsType == '03'}">
																<tr>
																	<td>
																		最高价满分
																	</td>
																	<td>
																		<img src="<%=basePath%>/images/priceRule/maxPrice.png"
																			width="300" height="35" />
																	</td>
																	<td id="low_03">
																		X =X = ${tenderBidPriceSet.upPoint}
																	</td>
																	<td align="left">
																		&nbsp;
																		<img
																			src="<%=basePath%>/images/priceRule/maxRemark.png"
																			width="250" height="65" />
																	</td>
																</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td>
																		其他
																	</td>
																	<td>
																		其他
																	</td>
																	<td id="up_00">
																		X =X = ${tenderBidPriceSet.upPoint}
																	</td>
																	<td align="left">
																		&nbsp;
																		<img
																			src="<%=basePath%>/images/priceRule/otherRemark.png"
																			width="250" height="65" />
																	</td>
																</tr>
															</c:otherwise>
															</c:choose>
														</table>
													</div>
												</div>
											</div>
											<div class="accordion-group">
												<div class="accordion-heading">
													<a class="accordion-toggle" data-toggle="collapse"
														data-parent="#accordion2" href="#collapseFive"> <c:if
															test="${fn:length(listValue)>0}"><i class="fa fa-star orange" title="已抽取专家"></i></c:if>评标委员会 </a>
												</div>
												<div id="collapseFive" class="accordion-body collapse">
													<div class="accordion-inner">
														<table width="100%" class="table_ys2" id="tb02">
															<tr class="Content_tab_style_04">
																<th width="2%" nowrap>
																	序号
																</th>
																<th width="10%" nowrap>
																	评委姓名
																</th>
																<th width="15%" nowrap>
																	工作单位
																</th>
																<th width="15%" nowrap>
																	评标专业
																</th>
																<th width="10%" nowrap>
																	联系方式
																</th>
																<th width="15%" nowrap>
																	类别
																</th>
															</tr>
															<c:forEach items="${listValue}" varStatus="sta" var="tenderBidJudge">
																<tr align="center" id="tr${tenderBidJudge.tbjId }" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
																	<td>
																		${sta.index+1 }
																	</td>
																	<td>
																	   ${tenderBidJudge.expertName}
																	</td>
																	<td>
																		${tenderBidJudge.companyName}
																	</td>
																	<td>
																		${tenderBidJudge.expertMajor}
																	</td>
																	<td>
																		${tenderBidJudge.mobilNumber}
																	</td>
																	<td>
																		<c:forEach items="${judgeType}" var="map">
																				<c:if test="${tenderBidJudge.expertRole == map.key}"> ${map.value }</c:if>
																		</c:forEach>
																	</td>
																</tr>
															</c:forEach>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
	</body>
</html>