<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>评标权重</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
		<script type="text/javascript">
		var api = frameElement.api, W = api.opener;
		var rcId="${rcId}";
	      function switchTabTwo(url){
	         $('#iframe').attr('src',url);
	      }
	      function doSelectItem(rcId,itemType){
	          createdetailwindow("评分项信息","viewBidItemInit_tenderBidPre.action?rcId="+rcId+"&itemType="+itemType,1);
	      }
	      function doImport(title){
		     createdetailwindow(title,"Template_ImportAction.action",2);
		  }
		  function showMsgFrame(type,msg){
		     showMsg(type,msg);
		  }
		  function showMsgFrameMsg(type,msg,sign,type){
		     showMsg(type,msg);
		      if(sign==1){
		        $('#iframe').attr('src',"updateTenderBidRateInit_tenderBidPre.action?tenderBidRate.rcId="+rcId);
		      }else if(sign==3){
		      if(type==1){
		        $('#iframe').attr('src',"updateTenderBidItemInit_tenderBidPre.action?tenderBidItem.rcId="+rcId+"&tenderBidItem.itemType=1");
		      }else{
		        $('#iframe').attr('src',"updateTenderBidItemInit_tenderBidPre.action?tenderBidItem.rcId="+rcId+"&tenderBidItem.itemType=0");
		      }
		     }else if(sign==4){
		      $('#iframe').attr('src',"updateTenderBidPriceInit_tenderBidPre.action?tenderBidPriceSet.rcId="+rcId);
		     }
		  }		  
		  function doSelectJudge(rcId,ul){
	          createdetailwindow("抽取专家","viewExpertSelect_tenderBidJudge.action?rcId="+rcId+"&ul="+ul,1);
	      }		  
	      function doReturn(){
			    var reValue=$('#returnVals').val();
			    $("#expertIds").val("");
				var expertIds="";
					if(reValue!=null&&reValue!=""){
						var mArr = reValue.split(",");
						for(var i=0;i<mArr.length;i++){
							if(mArr[i]!=""){
								nArr = mArr[i].split(":");
								expertIds += ","+nArr[0];
							}
						}
					}
					$("#expertIds").val(expertIds);
					
                    ajaxGeneral("updateBidJudgeSave_tenderBidJudge.action","expertIds="+expertIds+"&rcId=${rcId}");
					$('#iframe').attr('src',"viewTenderBidJudge_tenderBidJudge.action?rcId=${rcId}");
		  }
		  function doNext(rcId){
		    location.href="updateTenderBidPre_tenderBidPre.action?rcId="+rcId;
		  }
		 </script>
	</head>
	<body>

	<input type="hidden" name="title" id="title" value=""/>
	<input type="hidden" name="name" id="name" value=""/>
	<input type="hidden" name="submitUrl" id="submitUrl" value=""/>
	<input type="hidden" name="url" id="url" value=""/>
	<input type="hidden" name="returnVals" id="returnVals" value=""/>
	<input type="hidden" name="expertIds" id="expertIds" value=""/>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<c:if test="${isNext}">
						<div class="row-fluid">
							<div class="span12">
								<div class="btn-toolbar">
									<button type="button" class="btn btn-success" id="btn-next"
										onclick="doNext(${rcId})">
										<i class="icon-white icon-resize-small"></i> 执行下一步
									</button>
								</div>
							</div>
						</div>
					</c:if>
					<div class="row">
					<div class="col-xs-12">
				                   <div class="alert alert-block alert-danger">
									温馨提示:开标前，请把以下信息进行完善，以方便电子开标
								</div>	
								</div>
                           </div>
					<div class="row">
						<div class="col-xs-12">
									<div class="tabbable">

                             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="switchTabTwo('updateTenderBidRateInit_tenderBidPre.action?tenderBidRate.rcId=${rcId}')">评标权重设置</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="switchTabTwo('updateTenderBidItemInit_tenderBidPre.action?tenderBidItem.rcId=${rcId}&tenderBidItem.itemType=1')">商务评分设置</a>
                                 </li>
                                 <li>
                                     <a data-toggle="tab" onClick="switchTabTwo('updateTenderBidItemInit_tenderBidPre.action?tenderBidItem.rcId=${rcId}&tenderBidItem.itemType=0')">技术评分设置</a>
                                 </li>
                                 <li>
                                     <a data-toggle="tab" onClick="switchTabTwo('updateTenderBidPriceInit_tenderBidPre.action?tenderBidPriceSet.rcId=${rcId}')">价格评分设置</a>
                                 </li>
                                 <li>
                                     <a data-toggle="tab" onClick="switchTabTwo('viewTenderBidJudge_tenderBidJudge.action?rcId=${rcId}')">组建评标专家</a>
                                 </li>
                                 
                               </ul>

                             <div class="tab-content">
                                 <div id="smsj" >
                                     <iframe src="updateTenderBidRateInit_tenderBidPre.action?tenderBidRate.rcId=${rcId}" 
                                     frameborder=0 scrolling="yes" width="100%" id="iframe" height="380"></iframe>
                                 </div>
                             </div>
                         </div>


								</div>
							</div>
						</div>
					</div>

				</div>
	</body>
</html>