<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="pragma" content="no-cache"> 
<meta http-equiv="cache-control" content="no-cache"> 
<meta http-equiv="expires" content="0"> 
<title>创建招标项目信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script src="<%= path %>/common/script/required/requirePlan.js" type="text/javascript" ></script>
<script language="javaScript">
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="RequiredCollect"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
var fileNameData=[];//已上传的文件名
var fileTypeData=[];//已上传的文件的格式
var attIdData=[];//已存入附件表的附件信息

var num = 1;
var map = {};
var totalBudget=0;
function addRow(materialId,buyCode,buyName,materialType,unit,amount,deliverDate,rmdId,mnemonicCode,estimatePrice){
	totalBudget=$("#totalBudget").val();
	tr=document.getElementById("listtable").insertRow();
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
	var cell3 = tr.insertCell();
	var cell4 = tr.insertCell();
	var cell5 = tr.insertCell();
	var cell6 = tr.insertCell();
	var cell7 = tr.insertCell();
	var cell8 = tr.insertCell();
	var cell9 = tr.insertCell();
	var cell10 = tr.insertCell();
	var estimateSumPrice=parseFloat(amount)*parseFloat(estimatePrice);
	estimateSumPrice=estimateSumPrice.toFixed(2);
	totalBudget=FloatAdd(totalBudget,estimateSumPrice);
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/><input type='hidden' name='rcdList["+num+"].rmdId' value='"+rmdId+"'/><input type='hidden' name='rcdList["+num+"].materialId' value='"+materialId+"'/>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this,"+rmdId+","+num+")'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML=buyCode+"<input name='rcdList["+num+"].buyCode' type='hidden' value='"+buyCode+"'/>" ;
	cell3.innerHTML=buyName+"<input name='rcdList["+num+"].buyName' type='hidden' value='"+buyName+"'/>" ;
	cell4.innerHTML=materialType+"<input name='rcdList["+num+"].materialType' type='hidden' value='"+materialType+"'/>" ;
	cell5.innerHTML=unit+"<input name='rcdList["+num+"].unit' type='hidden' value='"+unit+"'/>" ;
	cell6.innerHTML="<input type='text' name='rcdList["+num+"].amount'  id='amount_"+num+"' value='"+amount+"' size='5' style='width: 55px' onblur='checkAmount("+amount+",this);caculatePriceByAmt(this,"+num+")''>";
	cell7.innerHTML="<input type='text' name='rcdList["+num+"].estimatePrice' value='"+estimatePrice+"' readonly id='estimatePrice_"+num+"' style='width: 50px'>" ;
	cell8.innerHTML="<span id='estimateSumPrice_"+num+"'>"+estimateSumPrice+"</span> <input type='hidden' id='estimateSumPrice_s_"+num+"' value='"+estimateSumPrice+"' name='rcdList["+num+"].estimateSumPrice' />" ;
    cell9.innerHTML=deliverDate+"<input name='rcdList["+num+"].mnemonicCode' type='hidden' value='"+mnemonicCode+"'/><input name='rcdList["+num+"].deliverDate' type='hidden' value='"+deliverDate+"'/>" ;
	cell10.innerHTML="<input name='rcdList["+num+"].remark' type='text' style='width: 90%'/>" ;
	num++;
	$("#totalBudget").val(totalBudget);
	document.getElementById("totalBudget_s").innerText =parseFloat(totalBudget).toFixed(2);
}
	//删除一行
	function deleteRow(obj,rmdId,num){
	    var priceSumStr=$("#estimateSumPrice_s_"+num).val();
	    if(priceSumStr!=""){
	        var totalPriceStr=$("#totalBudget").val();
	        totalPrice=FloatSub(totalPriceStr,priceSumStr);
		    document.getElementById("totalBudget_s").innerText =parseFloat(totalPrice).toFixed(2);
		    $("#totalBudget").val(parseFloat(totalPrice).toFixed(2));
	    } 
	    delete map[rmdId];  
		$(obj).parent().parent().remove(); 
	}
	
	var acd;
	//选择采购对象
	function selectRequiredPlan(){
		var url="viewNotCollectedRequiredPlan_purchaseBase.action";		
		createdetailwindow("选择未立项的计划",url,3);
	}
   //选择采购对象
   function doReturn(){	
	   var winObj=document.getElementById('returnVals').value;
		if(winObj){
			var returnVals = winObj.split('@');
			var rmSource = returnVals[0];
			for(var i=1;i<returnVals.length;i++){
				var buyCode = returnVals[i].split("#")[0];
				var buyName = returnVals[i].split("#")[1];
				var materialType = returnVals[i].split("#")[2];
				var unit = returnVals[i].split("#")[3];
				var amount = returnVals[i].split("#")[4];
				var deliverDate = returnVals[i].split("#")[5];
				var rmdId = returnVals[i].split("#")[6];
				var mnemonicCode = returnVals[i].split("#")[7];
				var materialId = returnVals[i].split("#")[8];			    
				var estimatePrice = returnVals[i].split("#")[9];
			    
			    if(!map.hasOwnProperty(rmdId)){
			      map[rmdId]=rmdId;
				  addRow(materialId,buyCode,buyName,materialType,unit,amount,deliverDate,rmdId,mnemonicCode,estimatePrice);
				}
				
			}
		}	
     }
   function checkAmount(amount,obj){
	  var newAmount = obj.value;
	    if(newAmount==""){
	        showMsg("alert","采购数量不能为空!")
			obj.value=amount;
			return false;
	    }
		if(isNaN(newAmount)){
			showMsg("alert","请输入数字!");
			obj.value=amount;
			return false;
		}else{
			var index=newAmount.indexOf(".");
			if(index!=-1){
				var b = newAmount.charAt(index+3);
				if(b!=""){
					showMsg("alert","小数点要最多两位，请更正！");
			obj.value=amount;			
					return false;
				}  
			}
		 }
		 if(parseFloat(newAmount)>parseFloat(amount)){
	            showMsg("alert","本次采购数量不能大于剩余数量“"+amount+"”，请更正！");
			    obj.value=amount;			
				return false;
		 }
   }
   function save(){
		if (num>1) {
			document.form1.action="saveRequiredCollect_purchaseTender.action";
			document.form1.submit();
		}else{
			showMsg("alert","温馨提示：请添加采购物资明细！");
		}
   }
</script>
</head>
<body >
<div class="container-fluid">
<div class="row-fluid">
<div class="span12" id="content">
<form class="defaultForm" id="form1" name="form1" method="post" action="">
<input type="hidden" id="rcId" name="requiredCollect.rcId" value=""/>
<input type="hidden" id="bidCode" name="requiredCollect.bidCode" value=""/>
<input type="hidden" name="requiredCollect.floatCode" value=""/>

<input type="hidden" name="requiredCollect.attIds" id="attIds" />
<input type="hidden" name="requiredCollect.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="requiredCollect.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="requiredCollect.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="requiredCollect.attIdData" id="attIdData" value=""/>
<input type="hidden" name="returnVals" id="returnVals" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>	
	<table align="center" class="table_ys1" style="margin-top:5px;">
		<tr align="center" class="Content_tab_style_05"><th colspan="4">创建招标项目</th></tr>
		<tr >
			 <td width="15%" class="Content_tab_style1">项目名称：</td>
			 <td width="35%" align="left">
			 	<input type="text" id="buyRemark" datatype="*6-100" nullmsg="项目名称不能为空！" errormsg="项目名称必须在6-50字之内！" name="requiredCollect.buyRemark" value="" size="70"/>
			 	<font color="#FF0000">*</font>
			 	<div class="info"><span class="Validform_checktip">项目名称必须在6-50字之内！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				
			 </td>
			 <td width="15%" class="Content_tab_style1">招标方式：</td>
			 <td width="35%" align="left">
			 	<select id="supplierType" name="requiredCollect.supplierType" datatype="*"  nullmsg="招标方式不能为空！" class="input-small">
						  <option value="">--请选择--</option>
						  <c:forEach items="${supplierType}" var="supplierType">
						    <option value="${supplierType.key }">${supplierType.value }</option>
						  </c:forEach>
				</select>
				<div class="info"><span class="Validform_checktip">招标方式不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				
			 </td>
		</tr>		
		<tr>
			<td width="15%"  class="Content_tab_style1">预算金额：</td>
			<td width="35%"  class="Content_tab_style2">
				<span id="totalBudget_s"></span>
				<input class="Content_input_style1"  type="hidden" id='totalBudget'   name="requiredCollect.totalBudget" value="0"/>
			</td>
			<td width="15%"  class="Content_tab_style1" nowrap>立项日期：</td>
			<td width="35%" class="Content_tab_style2">
				<fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>&nbsp;
			</td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>采购组织：</td>
			<td class="Content_tab_style2">
				${purchaseDeptName }
				<input type="hidden" name="requiredCollect.purchaseDeptId" value="${purchaseDeptId }"/>
			</td>
			<td class="Content_tab_style1">立项人：</td>
			<td class="Content_tab_style2">
				${writerCn }
 				<input type="hidden" id="writer" name="requiredCollect.writer" value="${writer }"/>
			</td>
		</tr>
   		<tr>
			<td class="Content_tab_style1">立项单位：</td>
			<td class="Content_tab_style2">
				${deptName }
				<input type="hidden" name="requiredCollect.deptId" value="${deptId }"/>
			</td>
			<td class="Content_tab_style1" nowrap></td>
			<td class="Content_tab_style2">
			</td>			
		</tr>
		<tr>
			<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
			<td class="Content_tab_style2" colspan="3">
				<!-- 附件存放 -->
				<div  id="fileDiv" class="panel"> 
				</div>
				<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
				<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
				
			</td>
		</tr>
	</table>
	<table align="center" class="table_ys1" id="listtable">	
		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap >操作 <img src="images/select.gif" onclick="selectRequiredPlan();" title="选择需求计划"/></th>
			<th width="95px" nowrap>编码<font color="#ff0000">*</font></th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量<font color="#ff0000">*</font></th>
			<th width="85px" nowrap>估算单价</th>
			<th width="100px" nowrap>估算总价</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>		
	</table>
	
	<div class="buttonDiv">
		<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-cacel" type="button" id="btn-cacel"  onclick="javascript:history.go(-1);" ><i class="icon icon-repeat"></i>返回</button>
	 </div>	
</form>
</div>
</div>
</div>
<script type="text/javascript">
$(function(){
	var defaultForm=$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象
			save();
			return false;	
		}
	});
})
</script>	
</body>
</html>