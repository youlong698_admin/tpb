<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>招标文件查看</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>		
	<script type="text/javascript">
    var api = frameElement.api, W = api.opener, cDG;
    </script>
</head>
<body>
<div class="Conter_Container">
    <div class="row-fluid">
		<table width="100%" class="table_ys2">
			<tr>
				<td colspan="2" class="Content_tab_style_td_head">
					招标文件信息
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">
					上传人：
				</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidFile.writerCn}
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">
					上传时间：
				</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidFile.writeDate}" pattern="yyyy-MM-dd" />
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">
					招标文件附件：
				</td>
				<td width="35%" class="Content_tab_style2">
					<c:out value="${tenderBidFile.attachmentUrl}" escapeXml="false"/>
				</td>
			</tr>

		</table>
        	
     </div>  
	</div>
	</body>
</html>