<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>编制招标计划</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
    <!-- 上传组件引入js -->
	<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
	<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
	<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
	<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
	<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
	<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script type="text/javascript">
 var api = frameElement.api, W = api.opener, cDG;
//附件需要添加的信息
 var sessionId="<%=session.getId()%>";
 var attachmentType="TenderBidFile"; //当前是哪个类别功能上传的附件
 var path="<%= path %>" 
 var fileNameData=${tenderBidFile.fileNameData};
 var uuIdData=${tenderBidFile.uuIdData};
 var fileTypeData=${tenderBidFile.fileTypeData};
 var attIdData=${tenderBidFile.attIdData};
</script>
  
</head>
 
<body>
<form class="defaultForm" id="save_TenderBidFile" name="save_TenderBidFile" method="post" action="updateTenderBidFile_tenderBidFile.action">
<input type="hidden" name="tenderBidFile.attIds" id="attIds"/>
<input type="hidden" name="tenderBidFile.rcId" value="${tenderBidFile.rcId}"/>
<input type="hidden" name="tenderBidFile.tbfId" value="${tenderBidFile.tbfId}"/>
<input type="hidden" name="tenderBidFile.writer" value="${tenderBidFile.writer}"/>
<input type="hidden" name="tenderBidFile.writeDate" value="<fmt:formatDate value="${tenderBidFile.writeDate}" pattern="yyyy-MM-dd" />"/>
<input type="hidden" name="tenderBidFile.fileNameData" id="fileNameData" value="${tenderBidFile.fileNameData}"/>
<input type="hidden" name="tenderBidFile.uuIdData" id="uuIdData" value="${tenderBidFile.uuIdData}"/>
<input type="hidden" name="tenderBidFile.fileTypeData" id="fileTypeData" value="${tenderBidFile.fileTypeData}"/>
<input type="hidden" name="tenderBidFile.attIdData" id="attIdData" value="${tenderBidFile.attIdData}"/>
<!-- 防止表单重复提交 -->
<s:token/>		

<div class="Conter_Container">
    <div class="row-fluid">
    	<table align="center" class="table_ys2">
    	    <tr>
				<td colspan="2" class="Content_tab_style_td_head">
					招标文件信息
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">
					上传人：
				</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidFile.writerCn}
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">
					上传时间：
				</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidFile.writeDate}" pattern="yyyy-MM-dd" />
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>
			  		
        </table>
        
        <div class="buttonDiv">
		<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
     </div>  
	</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeCheck:function(curform){
		    $("#fileNameData").val(fileNameData);
		    $("#uuIdData").val(uuIdData);
		    $("#fileTypeData").val(fileTypeData);
		    $("#attIdData").val(attIdData);
			if(uuIdData==""){
				showMsg("alert","温馨提示：附件不能为空！");
				return false;
			}else
			{
				return true;
			}
			
		}
	});
})
</script>
</body>
</html>