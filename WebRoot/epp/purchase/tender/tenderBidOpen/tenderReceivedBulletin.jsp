<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>投标文件查看</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row" id="myID">
							</div>
							
							<div class="row">

								<div class="col-xs-12">
									<table width="100%" class="table_ys2">
									<tr>
						    	       <td colspan="5" class="Content_tab_style_td_head">供应商投标文件查看</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="40%" nowrap>
												供应商
											</th>
											<th width="55%" nowrap>
												投标文件
											</th>
										</tr>
										<c:forEach items="${tenderReceivedBulletinList}" var="tenderReceivedBulletin" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${tenderReceivedBulletin.supplierName}</td>	
												<td>
												<c:choose>
												  <c:when test="${requiredCollect.openStatus=='02'}">
												     <c:out value="${tenderReceivedBulletin.attachmentUrl}" escapeXml="false"/>
												  </c:when>
												  <c:otherwise> <span class="text-muted">未开标</span></c:otherwise>
												</c:choose>
					                           </td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>