<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>技术评分汇总表</title>
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
			<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
		<script language="javaScript">
	function doView(title,url){
      window.parent.doView(title,url);
    }
</script>
	</head>

	<body>
			<input type="hidden" id="rcId" value="${rcId }"/>
			<!-- 定义两个变量    评标总数    提交评标总数 -->
			<c:set var="judgerNum" value="${fn:length(judgeList)}"></c:set>
			<c:set var="submitjudgerNum" value="0"></c:set>
			<div class="alert alert-block alert-info">
				<p>
				 <i class="icon-user"></i>评委评标情况：
				 <c:forEach var="tenderBidJudgeType" items="${judgeList}">
				  <c:if test="${tenderBidJudgeType.tableStatu==1}">
				  <c:set var="submitjudgerNum" value="${submitjudgerNum+1}"></c:set>
				  <i class="icon-star"></i><a href="##" onclick="doView('技术评分查看','viewJudgeTechTab_tenderJudgeScore.action?tbjtId=${tenderBidJudgeType.tbjtId}&rcId=${rcId }')" >${tenderBidJudgeType.expertName }</a>
				  </c:if>
                  <c:if test="${tenderBidJudgeType.tableStatu==0}">
				  <i class="icon-star-empty"></i>${tenderBidJudgeType.expertName }
				  </c:if>
                  </c:forEach>
				</p>
			</div>
			
			<c:if test="${judgerNum==submitjudgerNum&&judgerNum!=0 }">
			<table width="100%" border="0" align="center" cellpadding="0"
				cellspacing="1">
				<tr class="Content_tab_style_td_head">
					<th>
						技术评分汇总表
					</th>
				</tr>
				<tr class='biaoge_01_b'>
					<td>
						<table width="100%" border="0" align="center" cellpadding="0"
							cellspacing="0" class="table_ys1">
							<tr class="Content_tab_style_04" valign="top">
								<th width="5%" nowrap>
									序号
								</th>
								<th width="20%" nowrap>
									评分项目
								</th>
								<th width="5%" nowrap>
									标准分值
								</th>
								<c:forEach items="${supplierStr}" var="supplierName" varStatus="status">		
				                  <th width="20%" nowrap style="background-color:${colorStr[status.count-1]}"> ${supplierName } </th>
				               </c:forEach>
							</tr>
							<c:forEach items="${tjtsList}" var="list" varStatus="status">
								<tr onmouseover="style.backgroundColor='#FF9900'" onmouseout="style.backgroundColor='#FFFFFF'">
								    <td>
									   ${status.index+1}
									</td>
									<c:forEach var="value" items="${list}"  varStatus="statusList"> 
									    <c:if test="${statusList.count>2}">
								        <td style="background-color:${colorStr[status.count-1]}" align="right"> ${value }</td>
								        </c:if>
								       <c:if test="${statusList.count<=2}">
								       <td>${value }</td> 
								       </c:if>   
								    </c:forEach>
								</tr>
							</c:forEach>
							<tr class="biaoge_01_b">
								<td colspan="2">
									合计：
								</td>
								<td>
									100
								</td>								
								<c:forEach items="${totalSumStr}" var="totalSum" varStatus="status">
								    <td style="background-color:${colorStr[status.count-1]}"  align="right">
								      ${totalSum }
								    </td>
								</c:forEach>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</c:if>
	</body>
</html>

