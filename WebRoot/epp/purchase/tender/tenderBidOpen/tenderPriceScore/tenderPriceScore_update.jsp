
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>报价评分汇总表</title>
	<script src="<%=path%>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<style>
	    .style1 {
			font-size: 14px;
			font-weight: bold;
		}
    </style>
    <script type="text/javascript">
     var api = frameElement.api, W = api.opener;
      function savePriceScore(sign){
     	$.dialog.confirm(" 温馨提示：确定保存吗? ",function (){
     		document.forms[0].action="updateTenderPriceScore_tenderPriceScore.action?sign="+sign;
     		document.forms[0].submit();
     	},function(){},api);
	  }
    </script>
</head>
 
<body>
<form action="" method="post" name="">
	<input type="hidden"  name="rcId" value="${rcId }"/>
<s:token/>
	<table width="100%" class="table_ys1">
		     
		     <tr class='biaoge_01_b'>
		     	<td>
			     	<table class="table_ys2">
						<tr class="Content_tab_style_04">
							<th width="10%" nowrap>
								评分规则
							</th>
							<th width="30%" nowrap>
								评分公式
							</th>
							<th width="10%">
								扣分参数值
							</th>
							<th width="25%" nowrap>
								备注
							</th>
						</tr>
						<c:choose>
						<c:when test="${tenderBidPriceSet.brsType == '01'}">
							<tr>
								<td>
									最低价满分
								</td>
								<td>
									<img src="<%=basePath%>/images/priceRule/minPrice.png"
										width="300" height="30" />
								</td>
								<td id="up_01">
                                                	X = ${tenderBidPriceSet.upPoint}
								</td>
								<td align="left">
									&nbsp;
									<img
										src="<%=basePath%>/images/priceRule/minRemark.png"
										width="250" height="60" />
								</td>
							</tr>
						</c:when>
						<c:when test="${tenderBidPriceSet.brsType == '02'}">
							<tr>
								<td>
									平均价满分
								</td>
								<td>
									<img
										src="<%=basePath%>/images/priceRule/middlePrice.png"
										width="300" height="45" />
								</td>
								<td id="up_02">
									X =X = ${tenderBidPriceSet.upPoint}
								</td>
								<td align="left">
									&nbsp;
									<img
										src="<%=basePath%>/images/priceRule/middleRemark.png"
										width="250" height="80" />
								</td>
							</tr>
						</c:when>
						<c:when test="${tenderBidPriceSet.brsType == '03'}">
							<tr>
								<td>
									最高价满分
								</td>
								<td>
									<img src="<%=basePath%>/images/priceRule/maxPrice.png"
										width="300" height="35" />
								</td>
								<td id="low_03">
									X =X = ${tenderBidPriceSet.upPoint}
								</td>
								<td align="left">
									&nbsp;
									<img
										src="<%=basePath%>/images/priceRule/maxRemark.png"
										width="250" height="65" />
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td>
									其他
								</td>
								<td>
									其他
								</td>
								<td id="up_00">
									X =X = ${tenderBidPriceSet.upPoint}
								</td>
								<td align="left">
									&nbsp;
									<img
										src="<%=basePath%>/images/priceRule/otherRemark.png"
										width="250" height="65" />
								</td>
							</tr>
						</c:otherwise>
						</c:choose>
					</table>
		     	</td>
       	    </tr>
       	    
       	    <tr class='biaoge_01_b'>
		     	<td>
		     		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys1">
			  			<tr class="Content_tab_style_04" >
					     	<th width="5%"  nowrap >序号</th>
					     	<th width="15%" nowrap>投标厂家</th>
					        <th width="10%" nowrap>报价</th>
					        <th width="10%" nowrap>平均价</th>
					        <th width="10%" nowrap>报价评分（百分制）</th>
			       	    </tr>
			       	    <c:forEach items="${tssList}" var="tss" varStatus="sta">
			       	    <tr class='biaoge_01_b'>
					    	<td>
					    		${sta.index+1}
					    		<input type="hidden" name="bopsObjList[${sta.index}].supplierId" value="${tss.supplierId}"/>
					    		<input type="hidden" name="bopsObjList[${sta.index}].rcId" value="${rcId}"/>
					    	</td>
			    			<td>${tss.supplierName}</td>
					    	<td>${tss.sumPrice}</td>
					    	<td>${tss.avgPrice}</td>
					    	<td>
					    		<input type="text" name="bopsObjList[${sta.index}].priceScore" value="${tss.priceScore}"/>
							</td>
					    </tr>
			       	    </c:forEach>
					     
    				</table>
		     	</td>
		     </tr>
		     <tr class='biaoge_01_b'>
		     	<td>
		     		<button type="button" class="btn btn-info"   onclick="savePriceScore(1);"><i class="icon-white icon-ok-sign"></i>保存</button>
		     		<button type="button" class="btn btn-danger"   onclick="savePriceScore(2);"><i class="icon-white icon-leaf"></i>提交</button>
		     	</td>
	     	</tr>
    </table>
	
</form>
</body>
</html>
 
