<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>开标状态</title>
<script src="<%=path%>/common/script/context.js"
	type="text/javascript"></script>
<script language="javascript">
     
    function openStatus(){
     var tblId="${tenderBidList.tblId}";
     var openPassword=$("#openPassword").val();
     var result=ajaxGeneral("updateTenderBidListOpenStatus_tenderBidOpen.action","tblId="+tblId+"&openPassword="+openPassword,"text");
		if(result=="success"){
		  window.location.href=window.location.href;
		}else{
		  $("#error").html("开标密码错误！！！");
		}
   }
</script>
</head>
<body>
   <form id="showProjects" action="" method="post">
			<input type="hidden" name="rcId" value="${rcId }" />
			<!-- 查询区域  begin-->
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12" id="content">
				        <div>
				               <c:if test="${tenderBidList.openStatus=='01'&&isOpenAdmin}">
				                                        开标密码:<input type="text" placeholder="输入开标密码"  class="Content_input_style1"  id="openPassword" name="openPassword" value=""/> 
				               <button type="button" class="btn btn-danger" id="btn-del" onClick="openStatus()"> 确定专家全部签到进行开标</button>
				               <div id="error" style="color: red"></div>
				               </c:if>
				               <c:if test="${tenderBidList.openStatus=='02'}">
				                    <span style="color: red;font-weight: bold;">正在开标........</span>
				               </c:if>
							
				        </div>
						<table width="100%" class="table_ys2" id="tb02">
							<tr class="Content_tab_style_td_head">
								<th colspan="8" align="left">
									专家签到情况汇总：
								</th>
							</tr>
							<tr class="Content_tab_style_04">
								<th width="2%" nowrap>
									序号
								</th>
								<th width="10%" nowrap>
									评委姓名
								</th>
								<th width="15%" nowrap>
									工作单位
								</th>
								<th width="15%" nowrap>
									评标专业
								</th>
								<th width="10%" nowrap>
									联系方式
								</th>
								<th width="15%" nowrap>
									类别
								</th>
								<th width="15%" nowrap>
									是否签到
								</th>
								<th width="15%" nowrap>
									签到时间
								</th>
							</tr>
							<c:forEach items="${listValue}" varStatus="sta" var="tenderBidJudge">
								<tr align="center" id="tr${tenderBidJudge.tbjId }" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
									<td>
										${sta.index+1 }
									</td>
									<td>
									   ${tenderBidJudge.expertName}
									</td>
									<td>
										${tenderBidJudge.companyName}
									</td>
									<td>
										${tenderBidJudge.expertMajor}
									</td>
									<td>
										${tenderBidJudge.mobilNumber}
									</td>
									<td>
										<c:forEach items="${judgeType}" var="map">
											<c:if test="${tenderBidJudge.expertRole == map.key}">${map.value }</c:if>
										</c:forEach>
									</td>
									<td style="color: red">
										<c:if test="${tenderBidJudge.expertSign=='0'}">已签到</c:if>
									</td>
									<td style="color: red">
										<fmt:formatDate value="${tenderBidJudge.expertSignDate}" pattern="HH:mm" />
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>


		</form>
	</body>
</html>

