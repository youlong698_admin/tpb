<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />	
<title>标中质询</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">
 var api = frameElement.api, W = api.opener, cDG;
  <c:if test="${message!=null}">
		   window.onload=function(){ 
		   api.get("dialog").switchTabTwo("viewBidCommunicationInfoTab_tenderBidCommunicationInfo.action?rcId=${rcId}");
		   api.close();
		  }
  </c:if>
  function doAll(){
     var checkboxs=document.getElementsByName("supplierId");
	 for (var i=0;i<checkboxs.length;i++) {
	  var e=checkboxs[i];
	  e.checked=!e.checked;
	 }
  }	
</script>
</head>
 
<body>
<form id="bidCommunicationInfoEdit" class="defaultForm" name="bidCommunicationInfoEdit" method="post" action="saveBidCommunicationInfo_tenderBidCommunicationInfo.action">
<input type="hidden" name="bidCommunicationInfo.rcId" value="${bidCommunicationInfo.rcId}"/>
<input type="hidden" name="bidCommunicationInfo.type" value="${bidCommunicationInfo.type}"/>
<input type="hidden" name="bidCommunicationInfo.identification" value="${bidCommunicationInfo.identification}"/>
<s:token/>			
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table width="98%" border="0" align="center"  class="table_ys2">
        	<tr>
          		<td  colspan="2" class="Content_tab_style_td_head">质询</td>
        	</tr>
			<tr>
				<td class="Content_tab_style1">质询对象：</td>
				<td class="Content_tab_style2"> 
				   <div> <input type="checkbox" name="all" id="all" onclick="doAll()"/>全部供应商
				    <input type="hidden" name="supplierId"value="0"/></div>
				  <c:forEach items="${list}" var="tenderReceivedBulletin">
				      <input type="checkbox" name="supplierId" value="${tenderReceivedBulletin.supplierId }"/>${tenderReceivedBulletin.supplierName }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  </c:forEach>	
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">质询内容：</td>
				<td class="Content_tab_style2"> 
					<textarea name="bidCommunicationInfo.questionContent" id="questionContent" datatype="*" nullmsg="质询内容不能为空！"
						class="Content_input_style2" ></textarea>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">质询内容不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">质询内容日期：</td>
				<td class="Content_tab_style2"> 
					<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />
				</td>
			</tr>
			</table>
        <div class="buttonDiv">
		<button class="btn btn-success"  id="btn-save"><i class="icon-white icon-ok-sign"></i>质询</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
	 </div>	
	</div>
</div>
</form>

<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body></html>