<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<meta http-equiv="Cache-Control" content="no-store"/>
		<meta http-equiv="Pragma" content="no-cache"/>
		<meta http-equiv="Expires" content="0"/>
		<title>综合评分汇总</title>
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	</head>
	<body>
     <c:if test="${message!=''}">
		<div class="alert alert-block alert-danger">
			<p>
			  ${message }
			</p>
		</div>
	</c:if>
	<c:if test="${message==''}">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">		
						     		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys1">
							  			<tr class="Content_tab_style_04" >
									     	<th width="5%"  nowrap >序号</th>
									     	<th width="20%" nowrap>供应商名称</th>
									        <th width="10%" nowrap>技术评分（${tenderBidRate.techRate }%）</th>
									        <th width="10%" nowrap>商务评分（${tenderBidRate.businessRate}%）</th>
									        <th width="10%" nowrap>评标价评分（${tenderBidRate.priceRate}%）</th>
									        <th width="10%" nowrap>综合评分</th>
									        <th width="10%" nowrap>报价</th>
							       	    </tr>
							       	    <c:forEach items="${compPointList}" var="com" varStatus="sta">
							       	    <tr class='biaoge_01_b'>
									    	<td>
									    	${sta.index+1}
									    	</td>
							    			<td>${com.supplierName}</td>
									    	<td>${com.techSumPoints}</td>
									    	<td>${com.busSumPoints}</td>
									    	<td>${com.priceScore}</td>
									    	<td>${com.compreSumPoints}</td>
									    	<td>${com.sumPrice}</td>
									    </tr>
							       	    </c:forEach>
									     	
									</table>
		     	
							</div>
						</div>
					</div>

				</div>
		</c:if>
	</body>
</html>