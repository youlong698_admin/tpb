<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>供应商报价</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
	<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
	<![endif]-->
<script type="text/javascript">
  function doView(bpId){
	   window.parent.doView("查看供应商报价","viewAskBidPriceResponeDetail_tenderBidOpen.action?bpId="+bpId);
	 }
	
  function parityPrice(rcId){
	 window.parent.doView("供应商比价","viewParityPrice_tenderBidOpen.action?rcId="+rcId);
	 }
</script>
  
</head>
 
<body>
<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row-fluid">
								<div class="span12">
							     <div class="btn-toolbar">
				                       <button type="button" class="btn btn-primary" id="btn-parity" onclick="parityPrice(${rcId})"><i class="icon-white icon-indent-left"></i>比价</button>
				                    </div>
                             </div>
                           </div>
							<c:set var="isEcryption" value="false"/>
							<div class="row">
							  <div class="span12" id="content">
								<div class="row">
									<div class="col-xs-12">
										<div class="alert alert-block alert-danger">
											注意：报价没有解密之前，价格采取保密机制。
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<table width="100%" class="table_ys1">
									    <tr>
						    	       <td colspan="7" class="Content_tab_style_td_head">供应商报价信息</td>
						    	    </tr>
										<tr class="Content_tab_style_04">
											<th width="5%" nowrap>
												序号
											</th>
											<th width="30%" nowrap>
												供应商
											</th>
											<th width="15%" nowrap>
												报价时间
											</th>
											<th width="20%" nowrap>
												总价
											</th>	
											<th width="5%" nowrap>
												税率
											</th>
											<th width="10%" nowrap>
												操作
											</th>										
											<th width="10%" nowrap>
												二次报价
											</th>
										</tr>
										<c:forEach items="${listValue}" var="bidPrice" varStatus="status">
											<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
												<td>${status.index+1}</td>
												<td>${bidPrice.supplierName}</td>
												<td><fmt:formatDate value="${bidPrice.writeDate}" type="both" pattern="yyyy-MM-dd HH:mm" /></td>
												<c:choose>
												       <c:when test="${empty bidPrice.totalPrice}">
									                       <td>  <c:set var="isEcryption" value="true"/> *****</td>
									                       <td>
									                       <span class="text-muted">价格保密</span></td> 
									                       <td>   *****</td>
												       </c:when>
													   <c:otherwise>  
													    <td><span class="text-muted"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></span></td> 
													    <td>${bidPrice.taxRate}</td> 
													    <td><button class='btn btn-mini btn-primary' type="button"  onclick='doView(${bidPrice.bpId})'><i class="icon-white icon-bullhorn"></i></button></td>		
											         </c:otherwise>
												   </c:choose>
												<td>
                                                   <c:choose>
												       <c:when test="${bidPrice.isResponseNegotiate=='2'}">
									                       
												       </c:when>
												       <c:when test="${bidPrice.isResponseNegotiate=='0'}">
									                                                                 已二次报价还未响应
												       </c:when>
													   <c:otherwise>  
													                      已二次报价
													   </c:otherwise>
												   </c:choose>
                                                </td>
												   
												</tr>
										</c:forEach>

									</table>
									<input type="hidden" id="isEcryption" value="${isEcryption }"/>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
</body>
</html>