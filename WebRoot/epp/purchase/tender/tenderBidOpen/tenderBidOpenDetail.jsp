<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>开标现场</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>		
	    <script type="text/javascript">
		
		var api = frameElement.api, W = api.opener;
		var priceStatus="${tenderBidList.priceStatus}";
        var rcId="${rcId}";
	   function switchTabTwo(url){
	         $('#iframe').attr('src',url);
	      }
	   function doView(title,url){
		     createdetailwindow(title,url,1);
		  }
	   function doNext(rcId){
		    location.href="updateTenderBidOpen_tenderBidOpen.action?rcId="+rcId;
		  }
</script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
		               <div class="openBid">
							欢迎光临“${requiredCollect.buyRemark }”开标现场
						</div>						
							<div class="row">

								<div class="row-fluid">
									<c:if test="${isOpen}">
										<div class="tabbable">

											<ul class="nav nav-tabs padding-12 tab-color-blue background-blue">
                                                <li class="active">
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderBidOPenStatus_tenderBidOpen.action?rcId=${rcId}');">开标状态</a>
												</li>
												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderBidPriceRespone_tenderBidOpen.action?rcId=${rcId}')">供应商报价</a>
												</li>
												
												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderReceiveBulletinTab_tenderReceivedBulletin.action?rcId=${rcId}');">供应商投标文件</a>
												</li>
                                                <li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewBidCommunicationInfoTab_tenderBidCommunicationInfo.action?rcId=${rcId}');">标中质询</a>
												</li>
												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderJudTechGatherTab_tenderJudgeScore.action?rcId=${rcId}');">技术评标</a>
												</li>

												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderJudBusGatherTab_tenderJudgeScore.action?rcId=${rcId}')">商务评标</a>
												</li>
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderPriceGatherTab_tenderPriceScore.action?rcId=${rcId}')">报价评分汇总</a>
												</li>												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('vewTenderCompreScore_tenderCompreScore.action?rcId=${rcId}')">综合评分汇总</a>
												</li>

											</ul>

											<div class="tab-content">
												<div id="smsj">
													<iframe
														src="viewTenderBidOPenStatus_tenderBidOpen.action?rcId=${rcId}"
														frameborder=0 scrolling="yes" width="100%" id="iframe"
														height="400px" ></iframe>
												</div>
											</div>
										</div>
									</c:if>
									<c:if test="${!isOpen}">
									<div class="alert alert-block alert-danger">
								        <p>
                                            <strong>
												<i class="icon-white icon-remove-sign"></i>
												未到开标时间,开标时间为:<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />
												</strong>
                                        </p>
                                   </div>
									</c:if>
								</div>
							</div>
						</div>
					</div>

				</div>
	</body>
</html>