<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>开标现场</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>		
	    <script type="text/javascript">
		
		var api = frameElement.api, W = api.opener;
		var priceStatus="${tenderBidList.priceStatus}";
        var rcId="${rcId}";
	   //解密报价
	   function decryptPrice(){  
		  if(priceStatus!="03"){
		     var result=ajaxGeneral("decryptPrice_tenderBidOpen.action","rcId="+rcId,"text");
		      showMsg('success',''+result+'',function(){
				  window.location.href=window.location.href;						
				});
		  }else{
		     showMsg("alert","温馨提示：报价已经提交！");
		     return false;
			}
       }
       //二次报价
        function twoPrice(){
             createdetailwindow("二次报价磋商信息","saveInitNegotiate_bidNegotiate.action?rcId="+rcId,1);
        }
       //提交报价
        function submitPrice(){
           $.dialog.confirm("温馨提示：报价一经提交将成为最终报价，确定要提交报价信息么？",function() {
		   			var result = ajaxGeneral( "saveSubmitTenderBidPrice_tenderBidOpen.action","rcId="+rcId,"text");
		   			        showMsg('success',''+result+'',function(){
					   		window.location.href = window.location.href ;					
		   				});
		   			},function(){},api);
        }
       //报价评分
        function otherPriceScore(){
          createdetailwindow("编辑报价评分","updateTenderPriceScoreInit_tenderPriceScore.action?rcId="+rcId,0);
        }
	   function switchTabTwo(url){
	         $('#iframe').attr('src',url);
	      }
	   function doView(title,url){
		     createdetailwindow(title,url,1);
		  }
	   function doNext(rcId){
		    location.href="updateTenderBidOpen_tenderBidOpen.action?rcId="+rcId;
		  }
</script>
	</head>
	<body>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
		               <div class="openBid">
							欢迎光临“${requiredCollect.buyRemark }”开标现场
						</div>						
					   <div class="row-fluid">
							<div class="span12">
								<div class="btn-toolbar">
									<c:choose>
									<c:when test="${tenderBidList.calibtationStatus=='02'&&tenderBidList.priceStatus=='04'}">
							    			<c:if test="${isNext}">
											<button type="button" class="btn btn-success" id="btn-next" onclick="doNext(${rcId})"><i class="icon-white icon-resize-small"></i> 执行下一步</button>
							                </c:if>
									   </c:when>
									   <c:otherwise>
											 <input type="hidden" id="priceStatus" value="${tenderBidList.priceStatus}"/>
											 <c:if test="${tenderBidList.priceStatus!='03'&&tenderBidList.openStatus=='02'}">
												<button type="button" class="btn btn-info" id="desButton" onclick="decryptPrice()">
													<i class="fa fa-unlock"></i> 解密报价
												</button>
												</c:if>
												<c:if test="${tenderBidList.priceStatus=='02'}">									
												<button type="button" class="btn btn-info" id="updateButton" onclick="twoPrice()">
													<i class="icon-white icon-edit"></i> 二次报价
												</button>
												<button type="button" class="btn btn-info" id="submitButton" onclick="submitPrice()">
													<i class="icon-white icon-resize-small"></i> 提交报价
												</button>
												</c:if>												
												<c:if test="${tenderBidList.priceStatus=='03'&&tenderBidList.priceScoreType=='00'}">
						                        <button type="button" class="btn btn-info" id="btn-price" onclick="otherPriceScore()">
													<i class="icon-white icon-edit"></i> 编辑报价评分(您设置的评分规则为其他，请输入评分)
												</button>
												</c:if>
									  </c:otherwise>
									</c:choose>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="row-fluid">
									<c:if test="${isOpen}">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-12 tab-color-blue background-blue">
                                                <li class="active">
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderBidOPenStatus_tenderBidOpen.action?rcId=${rcId}');">开标状态</a>
												</li>												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderBidPriceRespone_tenderBidOpen.action?rcId=${rcId}')">供应商报价</a>
												</li>												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderReceiveBulletinTab_tenderReceivedBulletin.action?rcId=${rcId}');">供应商投标文件</a>
												</li>
                                                <li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewBidCommunicationInfoTab_tenderBidCommunicationInfo.action?rcId=${rcId}');">标中质询</a>
												</li>
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderJudTechGatherTab_tenderJudgeScore.action?rcId=${rcId}');">技术评标</a>
												</li>												
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderJudBusGatherTab_tenderJudgeScore.action?rcId=${rcId}')">商务评标</a>
												</li>
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('viewTenderPriceGatherTab_tenderPriceScore.action?rcId=${rcId}')">报价评分汇总</a>
												</li>
												<li>
													<a data-toggle="tab"
														onClick="switchTabTwo('vewTenderCompreScore_tenderCompreScore.action?rcId=${rcId}')">综合评分汇总</a>
												</li>

											</ul>

											<div class="tab-content">
												<div id="smsj">
													<iframe
														src="viewTenderBidOPenStatus_tenderBidOpen.action?rcId=${rcId}"
														frameborder=0 scrolling="yes" width="100%" id="iframe"
														height="400px" ></iframe>
												</div>
											</div>
										</div>
									</c:if>
									<c:if test="${!isOpen}">
									<div class="alert alert-block alert-danger">
								        <p>
                                            <strong>
												<i class="icon-white icon-remove-sign"></i>
												未到开标时间,开标时间为:<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />
												</strong>
                                        </p>
                                   </div>
									</c:if>
								</div>
							</div>
						</div>
					</div>

				</div>
	</body>
</html>