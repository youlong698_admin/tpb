<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>组建评标委员会</title>
<script src="<%=path%>/common/script/context.js"
	type="text/javascript"></script>
<script language="javascript">
     
	//点击删除
	 function deleteTenderBidJudge(tbjId){
        var result=ajaxGeneral("updateTenderBidJudgeDelete_tenderBidJudge.action","tbjId="+tbjId,"text");
		if(result=="success"){
		  $("#tr"+tbjId).css('display','none'); 
		}
	}
	//更新评标类别
	 function doClick(tbjId,key){
		ajaxGeneral("updateTerderBidJudgeType_tenderBidJudge.action","tbjId="+tbjId+"&key="+key,"text");
	}
    function doSelect(rcId){
     var ul ="";
     window.parent.doSelectJudge(rcId,ul);
   }
</script>
</head>
<body>
   <form id="showProjects" action="" method="post">
			<input type="hidden" name="rcId" value="${rcId }" />
			<!-- 查询区域  begin-->
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12" id="content">
						<table width="100%" class="table_ys2" id="tb02">
							<tr class="Content_tab_style_td_head">
								<th colspan="7" align="left">
									已抽取的专家列表：
								</th>
							</tr>
							<tr class="Content_tab_style_04">
								<th width="4%" nowrap>
									<img src="images/select.gif" onclick="doSelect(${rcId});" title="抽取专家"/>
								</th>
								<th width="2%" nowrap>
									序号
								</th>
								<th width="10%" nowrap>
									评委姓名
								</th>
								<th width="15%" nowrap>
									工作单位
								</th>
								<th width="15%" nowrap>
									评标专业
								</th>
								<th width="10%" nowrap>
									联系方式
								</th>
								<th width="15%" nowrap>
									类别
								</th>
							</tr>
							<c:forEach items="${listValue}" varStatus="sta" var="tenderBidJudge">
								<tr align="center" id="tr${tenderBidJudge.tbjId }" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
									<td>
										<img src="images/delete.gif" style="cursor: pointer;" id="deleteButton" title="删除所选专家" onclick="deleteTenderBidJudge(${tenderBidJudge.tbjId })"/>
									</td>
									<td>
										${sta.index+1 }
									</td>
									<td>
									   ${tenderBidJudge.expertName}
									</td>
									<td>
										${tenderBidJudge.companyName}
									</td>
									<td>
										${tenderBidJudge.expertMajor}
									</td>
									<td>
										${tenderBidJudge.mobilNumber}
									</td>
									<td>
										<c:forEach items="${judgeType}" var="map">
											<input type="radio"
												<c:if test="${tenderBidJudge.expertRole == map.key}">checked</c:if>
												name="judgeType${tenderBidJudge.expertId }"
												value="${map.key }" onclick="doClick(${tenderBidJudge.tbjId },'${map.key }')"/>
											  ${map.value }
										</c:forEach>
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>


		</form>
	</body>
</html>

