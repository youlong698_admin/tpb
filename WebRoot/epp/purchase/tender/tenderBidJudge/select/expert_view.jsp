<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/common/context.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>专家信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script type='text/javascript' src='<%=path %>/common/script/select.js'></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var rcId=${rcId};
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.SELECT_DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findExpert_tenderBidJudge.action?rcId="+rcId,
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			$.dialog.alert("查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		                $.dialog.alert("查询失败");
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            {
           		className: "td-checkbox",
                 orderable: false,
                 width: "5%",
                 data: null,
                 render: function (data, type, row, meta) {
                     return '<input type="checkbox" onclick="change();" name="selectEpp" id="'+row.expertId+'" value="'+row.expertId+':'+row.expertName+'" >';
                 }
            },
            {	
            	className : "ellipsis",
            	data: "expertName",
                orderable : false		
            },
			{
				data : "expertSex",
				render: CONSTANT.DATA_TABLES.RENDER.SEX,
				width : "10%",
				orderable : false
			},
			{
				data : "inOut",
				render: CONSTANT.DATA_TABLES.RENDER.INOUT,
				width : "10%"
			},
			{
				data : "expertType",
				render: CONSTANT.DATA_TABLES.RENDER.EXPERTTYPE,
				width : "10%"
			},
			{
				className : "ellipsis",
				data : "expertMajor",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "20%"
			},
			{
				data : "stutusCn",
				width : "10%"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	divAll();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	// 
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	   
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
		change();
		
    });
    
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//默认进入的排序
		 param.orderColumn="ex.expertId";
		//组装排序参数
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 3:
				param.orderColumn = "ex.inOut";
				break;
			case 4:
				param.orderColumn = "ex.expertType";
				break;
			case 5:
				param.orderColumn = "ex.expertMajor";
				break;
			case 6:
				param.orderColumn = "ex.stutusCn";
				break;
			default:
				param.orderColumn = "ex.inOut";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		param.expertName = $("#expertName").val();
		param.status = $("#status").val();
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	}
	
};
    function doQuery(){
     _table.draw();
    }

</script>
</head>
<body >
<form >
<div class="container-fluid" >
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid" style="margin-top:7px;margin-bottom:5px;text-align:left;">
				    <input type="hidden" id="rcId" value="${rcId }"/>
					姓名：
						<input class="input-medium" type="text" placeholder="姓名"  id="expertName" name="expert.expertName" value=""/>
						
						专家状态：
						<select id="status" class="input-small">
							<option value="">--请选择--</option>
							<option value="0">有效</option>
							<option value="1">无效</option>
						</select>
				
						<button type="button" class="btn btn-info" id="btn-advanced-search"><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
				
				
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="90%">
							<thead>
								<tr>
									<th style="width:5%;" >
										<input type="checkbox" id="check_all" value="qb"   name="cb-check-all" onclick="selectedAll()"/>
									</th>
									<th>姓名</th>
									<th>性别</th>
									<th>系统内/外</th>
									<th>专家类型</th>
									<th>评标专业</th>
									<th>状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
</body>
</html>