<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<title></title>
		 <script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
	</head>
	<body >
	 <div class="page-content">

         <!-- /.page-header --> 

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">
							
                             <ul class="nav nav-tabs  padding-12 tab-color-blue background-blue" id="cggd_info">
                                 	<li class="active">
	                                     <a data-toggle="tab" onClick="window.parent.MainTop.location.href='viewExpert_tenderBidJudge.action?rcId=${rcId}'">专家库中的专家</a>
	                                 </li>
                            
                             </ul>

                         </div>

                     </div>
                 </div>

                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>

		
</body>
</html>