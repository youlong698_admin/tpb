<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>招标计划查看</title>	
	<link href="<%=path%>/common/jQuery/chosen/1.1.0/chosen.min.css" rel="stylesheet"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="<%=path%>/common/script/context.js"
		type="text/javascript"></script>		
	<script type="text/javascript">
    var api = frameElement.api, W = api.opener, cDG;
    $(function(){
    <c:if test="${supplierType=='01'}">
	//此方法执行是如果编辑页面，需要加载显示供应商
	doReturn();
	</c:if>
})
function doReturn(){
    var reValue=$('#returnVals').val();
    $("#supIds").val("");
    $("#suppliersChosen").html("");
	var supIds=",";
		if(reValue!=null&&reValue!=""){
			var mArr = reValue.split(",");
			for(var i=0;i<mArr.length;i++){
				if(mArr[i]!=""){
					nArr = mArr[i].split(":");
					supIds += nArr[0]+",";
					$('<li class="search-choice" supplier-id="'+nArr[0]+'"><span>'+nArr[1]+'</span></li>')
							.appendTo('#suppliersChosen');
				}
			}
		}
		$("#supIds").val(supIds);
}
function doClick(type){
  if(type==1){
	  $("#password").css("display","none");
	  $("#passwordCn").css("display","block");
  }else{
	  $("#password").css("display","block");
	  $("#passwordCn").css("display","none");
  }
}
        </script>
</head>
<body>
<div class="Conter_Container">
    <div class="row-fluid">
		<input type="hidden" name="supIds" id="supIds" value="${supplierIds }" />
		<input type="hidden" id="returnVals" name="returnVals" value="${returnVals}"/>
    	<table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">招标计划</td>
    	    </tr> 
    	    <tr>
				<td width="15%" class="Content_tab_style1">项目负责人：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.responsibleUser }
				</td>
				<td width="15%" class="Content_tab_style1">负责人手机号：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.responsiblePhone }
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">开标日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">招标文件领购开始日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.salesDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">招标文件领购截止日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.saleeDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">回标截止日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.returnDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">标书费（元）：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.tenderMoney }
				</td>
				<td width="15%" class="Content_tab_style1">保证金（元）：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.bondMoney }
				</td>
			</tr>
			<tr>
			    <td width="15%" class="Content_tab_style1">报价类型：</td>
				<td width="35%" class="Content_tab_style2">
				    ${tenderBidList.priceTypeCn }
				</td>
				<td width="15%" class="Content_tab_style1">报价列类型：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   ${tenderBidList.priceColumnTypeCn }
				</td>
			</tr>
			<tr>
			    <td width="15%" class="Content_tab_style1">最小报价单位数：</td>
				<td width="35%" class="Content_tab_style2">
				   ${tenderBidList.minBidAmount }
				</td>
				<td width="15%" class="Content_tab_style1">开标管理员：</td>
				<td width="35%" class="Content_tab_style2">
				   ${tenderBidList.bidOpenAdminCn } <c:if test="${flag}"><div id="password" onclick="doClick(1)" style="font-size: 14px;color: red;cursor: pointer;">查看开标密码</div><div id="passwordCn" style="display: none;font-size: 14px;color: red;cursor: pointer;" onclick="doClick(2)" title="点击隐藏">${tenderBidList.bidOpenPassword }</div></c:if>
				</td>
			</tr>			
			<tr>
				<td width="15%" class="Content_tab_style1">备注：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				   ${tenderBidList.remark }
				</td>
			</tr>
			<c:if test="${supplierType=='01'}">
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">询价供应商</td>
    	      </tr>
			  <tr>
				<td width="15%" class="Content_tab_style1">邀请供应商：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
					
						<div class="chosen-container-multi">
									<ul class="chosen-choices" id="suppliersChosen">
									</ul>
								</div>
				</td>
			  </tr>
			 </c:if>
			  <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">商务响应项</td>
    	      </tr>
    	      <tr>
    	         <td colspan="4">
    	            <table id="businessTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>序号</th>
						<th width="30%" nowrap>响应项名称</th>
						<th width="60%" nowrap>响应项要求 </th>
					</tr>
					<c:forEach items="${businessResponseItemsList}" var="businessResponseItems" varStatus="status">
					 <tr>
						<td align="center">
						   ${status.index+1}
						</td>
						<td>
						    ${businessResponseItems.responseItemName}
						</td>
						<td>
						   ${businessResponseItems.responseRequirements}
						</td>
						</tr>
				</c:forEach>
    	            </table>
    	         </td>
    	      </tr>
			  		
        </table>
        	
     </div>  
	</div>
	</body>
</html>