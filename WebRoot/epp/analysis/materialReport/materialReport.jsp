<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>采购品成交统计</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findMaterialReport_purchaseChart.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			showMsg("error","查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               //showMsg("error","查询失败");
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
             {
				data : "buyCode",
				width : "10%",
            	orderable : false	
			},{
            	className : "ellipsis",
            	data: "buyName",    
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,        	
            	width : "20%",
            	orderable : false
            },
            {
            	className : "ellipsis",
            	data: "materialType",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "30%",
            	orderable : false
            },
			{
				className : "numPrice",
				data : "totalNumber",
				width : "10%",
				render:CONSTANT.DATA_TABLES.RENDER.NUMBER,
            	orderable : false			
			},
			{
				className : "numPrice",
				data : "amountMoney",
				width : "10%",
				render:CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE,
            	orderable : false			
			},
            {	
            	className : "textCenter",
				width : "10%",
				render: function(data,type, row, meta) {
                   return "<a href=\"javascript:void(0);\" onclick=\"createdetailwindow('历史价格分析','viewHistoryPrice_priceLibrary.action?buyCode="+row.buyCode+"',1)\">查看</a>";
                },
            	orderable : false			
            }
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			//$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	//导出excel
	$("#btn-daochu").click(function(){
		GeneralManage.daochuItems();
	});
	
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="";		
		//组装查询参数
		
		param.buyName = $("#buyName").val();
		param.materialType = $("#materialType").val();
		param.mkCode = $("#mkCode").val();
		param.writeDateStart = $("#writeDateStart").val();
		param.writeDateEnd = $("#writeDateEnd").val();
		
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	//导出excel
	daochuItems:function(){
		document.forms[0].action="exportMaterialReportExcel_purchaseChart.action";
		document.forms[0].submit();
		
	}
};
    
    function doQuery(){
     _table.draw();
    }

	//重置
	function doReset(){        
		document.forms[0].elements["buyName"].value	=	"";	
		document.forms[0].elements["materialType"].value	=	"";
		document.forms[0].elements["mkCode"].value	=	"";	
		document.forms[0].elements["writeDateStart"].value	=	"";	
		document.forms[0].elements["writeDateEnd"].value	=	"";
	}
</script>
</head>
<body >
<div class="container-fluid" >
		<div class="row-fluid">
			<div class="span12" id="content">
				
				<div class="row-fluid" id="div-advanced-search">
					<form class="form-inline well">
					 物料名称：
					 <input class="input-medium" placeholder="物料名称"type="text" id="buyName" name="buyName" value="" size="15" />
					&nbsp;&nbsp;规格型号：
					 <input class="input-medium" placeholder="规格型号"type="text" id="materialType" name="materialType" value="" />
					&nbsp;&nbsp;统计周期：
					<input type="text" class="input-medium Wdate" id="writeDateStart" name="writeDateStart" style="width: 80px" value="" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })"/>
					至：<input type="text" class="input-medium Wdate" id="writeDateEnd" name="writeDateEnd" style="width: 80px" value ="" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })"/>
			 		&nbsp;&nbsp;物料分类：
					 <input class="input-medium" placeholder="物料分类" type="text" id="mkCode" name="mkCode" value="" readonly="readonly"/>					 
			 		 &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-info" id="btn-advanced-search" ><i class="icon-white icon-search"></i> 查询</button>
				      <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
						<button type="button" class="btn btn-info" id="btn-daochu"><i class="icon-white icon-share"></i> 导出Excel</button>					
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>物料编码</th>
									<th>物料名称</th>
									<th>规格型号</th>
									<th>中标项目数量</th>
									<th>中标总金额(元)</th>
									<th>历史成交价格</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>