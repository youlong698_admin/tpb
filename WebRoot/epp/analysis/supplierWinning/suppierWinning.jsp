<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>供应商采购成交统计</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<style type="text/css">
html,body{
    height:100%
}
</style>
<script type="text/javascript">
   function doQuery(){
     var buyWay=$("#buyWay").val();
     var writeDateStart=$("#writeDateStart").val();
     var writeDateEnd=$("#writeDateEnd").val();
     window.location.href="viewSupplierWinning_purchaseChart.action?buyWay="+buyWay+"&writeDateStart="+writeDateStart+"&writeDateEnd="+writeDateEnd;
   }

</script>
</head>
<body><div class="page-content" style="width:100%;height:100%">

         <!-- /.page-header --> 

         <div class="row" style="width:100%;height:100%">
             <div class="col-xs-12" style="width:100%;height:100%">
             <div class="row-fluid"  id="div-advanced-search">
					<form class="form-inline well">
                                                               采购方式：
							<select name="buyWay" id="buyWay" class="input-small">
								<option value=''>
									--请选择--
								</option>
								<c:forEach items="${purchaseWayMap}" var="item">
									<option value="${item.key }" <c:if test="${buyWay==item.key }">selected</c:if>>
										${item.value }
									</option>
								</c:forEach>
							</select>
							统计周期：
						<input type="text" class="input-medium Wdate" id="writeDateStart" name="writeDateStart" style="width: 80px" value="${writeDateStart }" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })"/>
					至：<input type="text" class="input-medium Wdate" id="writeDateEnd" name="writeDateEnd"  style="width: 80px"  value ="${writeDateEnd }" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })"/>                         
						<button type="button" class="btn btn-info" id="btn-advanced-search"  onclick="doQuery();"><i class="icon-white icon-search"></i> 查询</button>
						
                 </form>
                 </div>
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row" align="center" style="width:95%;height:85%">
			        <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
				    <div id="main" style="width:100%;height:100%"></div>
				    <!-- ECharts单文件引入 -->
				    <script src="<%= path %>/common/echarts/echarts.min.js"></script>
				    <script type="text/javascript">
				        // 基于准备好的dom，初始化echarts图表
				        var myChart = echarts.init(document.getElementById('main')); 

						var myColor=['red','orange','yellow','green','cyan','blue','purple','#C1232B','#B5C334','#FCCE10','#E87C25','#27727B'];
						var xAxisData=[${xAxisData}];
						
						
						var option = {
						    title: {
						        text: '前${count}家供应商中标图形分析'
						    },
						    tooltip: {
						        trigger: 'axis'
						    },
						    toolbox: {
						        feature: {
						            dataView: {
						                show: true,
						                readOnly: false
						            },
						            restore: {
						                show: true
						            },
						            saveAsImage: {
						                show: true
						            }
						        }
						    },
						    grid: {
						        containLabel: true
						    },
						    legend: {
						        data: ['中标项目','中标合计']
						    },
						    xAxis: [{
						        type: 'category',
						        axisTick: {
						            alignWithLabel: true
						        },
						        axisLabel: {  
								   interval: 0,  
                                     formatter:function(value)  
                                     {  
                                         debugger  
                                         var ret = "";//拼接加\n返回的类目项  
                                         var maxLength = 5;//每项显示文字个数  
                                         var valLength = value.length;//X轴类目项的文字个数  
                                         var rowN = Math.ceil(valLength / maxLength); //类目项需要换行的行数  
                                         if (rowN > 1)//如果类目项的文字大于3,  
                                         {  
                                             for (var i = 0; i < rowN; i++) {  
                                                 var temp = "";//每次截取的字符串  
                                                 var start = i * maxLength;//开始截取的位置  
                                                 var end = start + maxLength;//结束截取的位置  
                                                 //这里也可以加一个是否是最后一行的判断，但是不加也没有影响，那就不加吧  
                                                 temp = value.substring(start, end) + "\n";  
                                                 ret += temp; //凭借最终的字符串  
                                             }  
                                             return ret;  
                                         }  
                                         else {  
                                             return value;  
                                         }  
                                     }  
                                 },
						        data: [${xAxis}]
						    }],
						    yAxis: [{
						        type: 'value',
						        name: '中标项目',
						        position: 'right',
						        axisLabel: {
						            formatter: '{value}'
						        }
						    }, {
						        type: 'value',
						        name: '中标合计',
						        position: 'left'
						    }],
						    series: [{
						        name: '中标项目',
						        type: 'line',
						        stack: '总量',
						            label: {
						                normal: {
						                    show: true,
						                    position: 'top',
						                }
						            },
						        lineStyle: {
						                normal: {
						                    width: 3,
						                    shadowColor: 'rgba(0,0,0,0.4)',
						                    shadowBlur: 10,
						                    shadowOffsetY: 10
						                }
						            },
						        data: [${totalNumber}]
						    }, {
						        name: '中标合计',
						        type: 'bar',
						        yAxisIndex: 1,
						        stack: '总量',
						        itemStyle: {
						                normal: {
						                    color: function(params) {
						                        var num=myColor.length;
						                        return myColor[params.dataIndex%num]
						                    },label: {
						                        show: true,
						                        position: 'top',
						                        formatter: '{c}'
						                    }
						                }
						            },
						        data: [${amountMoney}]
						    }]
						};


 

				
				        // 为echarts对象加载数据 
				        myChart.setOption(option); 
				    </script>
			    	    
			</div>
			</div>
</div>
</div>
</body>
</html> 	