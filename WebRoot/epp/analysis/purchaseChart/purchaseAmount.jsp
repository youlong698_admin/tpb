<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>采购金额统计分析</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<style type="text/css">
html,body{
    height:100%
}
</style>
<script type="text/javascript">
   function doQuery(){
     var buyWay=$("#buyWay").val();
     var year=$("#year").val();
     var type=$("#type").val();
     if(type==0||type==1||type==2){
        if(year==""){
            showMsg("alert","请选择统计周期");
            return;
        }
     }
     window.location.href="viewPurchaseAmount_purchaseChart.action?buyWay="+buyWay+"&year="+year+"&type="+type;
   }

</script>
</head>
<body><div class="page-content" style="width:100%;height:100%">

         <!-- /.page-header --> 

         <div class="row" style="width:100%;height:100%">
             <div class="col-xs-12" style="width:100%;height:100%">
             <div class="row-fluid"  id="div-advanced-search">
					<form class="form-inline well">
                                                               采购方式：
							<select name="buyWay" id="buyWay" class="input-small">
								<option value=''>
									--请选择--
								</option>
								<c:forEach items="${purchaseWayMap}" var="item">
									<option value="${item.key }" <c:if test="${buyWay==item.key }">selected</c:if>>
										${item.value }
									</option>
								</c:forEach>
							</select>
							统计周期：
						<input type="text" class="input-medium Wdate" name="year" id="year" style="width: 80px" value="${year }" onclick="WdatePicker({dateFmt:'yyyy'})"/>
	                                                    统计方式：
	                      <select name="type" id="type" class="input-small">
	                         <option value="0" <c:if test="${type==0 }">selected</c:if>>月度</option>
	                         <option value="1" <c:if test="${type==1 }">selected</c:if>>季度</option>
	                         <option value="2" <c:if test="${type==2 }">selected</c:if>>半年度</option>
	                         <option value="3" <c:if test="${type==3 }">selected</c:if>>年度</option>
	                      </select>                               
						<button type="button" class="btn btn-info" id="btn-advanced-search"  onclick="doQuery();"><i class="icon-white icon-search"></i> 查询</button>
						
                 </form>
                 </div>
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row" align="center" style="width:95%;height:85%">
			        <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
				    <div id="main" style="width:100%;height:100%"></div>
				    <!-- ECharts单文件引入 -->
				    <script src="<%= path %>/common/echarts/echarts.min.js"></script>
				    <script type="text/javascript">
				        // 基于准备好的dom，初始化echarts图表
				        var myChart = echarts.init(document.getElementById('main')); 

						var myColor=['red','orange','yellow','green','cyan','blue','purple','#C1232B','#B5C334','#FCCE10','#E87C25','#27727B'];
						var xAxisData=[${xAxisData}];
						
						
						var option = {
						    title: [{
						        text: '采购金额统计',
						        x: '25%',
						        textAlign: 'center'
						    },{
						        text: '项目数量统计',
						        x: '25%',
						        y: '53%',
						        textAlign: 'center'
						    },
						    {
						        text: '采购金额统计',
						         x: '75%',
						        textAlign: 'center'
						    }, {
						        text: '项目数量统计',
						        x: '75%',
						        y: '53%',
						        textAlign: 'center'
						    }],
						    toolbox:{
						        left:20,
						        feature:{
						            saveAsImage: {name:'采购金额成交统计'},
						            dataView: {},
						            magicType: {
						                type:['line','bar']
						            },
						            // brush: {},
						        }
						    },
						    tooltip : {
								 trigger:'item'
							},
						    grid: [
						        {
						            show: false,
						            left: 10,
						            top: '12%',
						            containLabel: true,
						            width: '65%',
						            height:'40%'
						        },
						        {
						            show: false,
						            left: '50%',
						            top: '12%',
						            containLabel: true,
						            width: '35%',
						            height:'35%'
						        }, 
						        {
						            show: false,
						            left: 10,
						            top: '60%',
						            containLabel: true,
						            width: '65%',
						            height:'40%'
						        },
						        {
						            show: false,
						            left: '50%',
						            top: '60%',
						            containLabel: true,
						            width: '35%',
						            height:'35%'
						        },
						    ],
						    xAxis: [
						        {
						            gridIndex:0,
						            axisTick:{
						                alignWithLabel: true
						            },
						            data:xAxisData
						        },
						         {
						            gridIndex:2,
						            axisTick:{
						                alignWithLabel: true
						            },
						            data:xAxisData
						        },
						    ],
						    yAxis: [
						        {
						            gridIndex:0,
						            name:"金额(万)",
						        },
						        {
						            gridIndex:2,
						            name:"数量",
						        },
						    ],
						   
						    series: [
						        {
						            type: 'bar',
						            name:'金额',
						            xAxisIndex:0,
						            yAxisIndex:0,
						            itemStyle: {
						                normal: {
						                    color: function(params) {
						                        var num=myColor.length;
						                        return myColor[params.dataIndex%num]
						                    },label: {
						                        show: true,
						                        position: 'top',
						                        formatter: '{b}\n{c}'
						                    }
						                }
						            },
						            data:[${amountMoney}]
						        },
						        {
						            type: 'bar',
						            xAxisIndex:1,
						            yAxisIndex:1,
						            name:'数量',
						            itemStyle: {
						                normal: {
						                   color: function(params) {
						                        var num=myColor.length;
						                        return myColor[params.dataIndex%num]
						                    },label: {
						                        show: true,
						                        position: 'top',
						                        formatter: '{b}\n{c}'
						                    }
						                }
						            },
						            data:[${totalNumber}]
						        },
						        {
                                    name:'金额(万)',
							        type: 'pie',
							        center: ['85%', '28%'],
							        radius: ['25%', '39%'],
							        color: myColor,
						            tooltip : {
								       formatter: "{a} <br/>{b} : {c} ({d}%)"
								   },
							        data:[${pieAmountMoney}]
							    },
						        {
                                    name:'数量',
							        type: 'pie',
							        center: ['85%', '80%'],
							        radius: ['25%', '39%'],
							        color: myColor,
						            tooltip : {
								       formatter: "{a} <br/>{b} : {c} ({d}%)"
								   },
							        data:[${pieTotalNumber}]
						    },
						    ]
						};


 

				
				        // 为echarts对象加载数据 
				        myChart.setOption(option); 
				    </script>
			    	    
			</div>
			</div>
</div>
</div>
</body>
</html> 	