<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>项目明细信息</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script language="javaScript">
	var api = frameElement.api, W = api.opener;
	 function doView(baId){
	   window.parent.openWindowFrame("采购项目信息","viewBidAwardDetail_bidAward.action?baId="+baId,1);
	}
</script>
</head>
<body >
<form id="cx" method="post" action="">
	
	<table align="center"  class="table_ys1">
		<tr><td colspan="4" class="Content_tab_style_td_head">采购项目信息</td></tr>
		<tr >
			 <td width="15%" class="Content_tab_style1">项目名称：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.buyRemark }
			 </td>
			<td width="15%" class="Content_tab_style1">项目编号：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.bidCode }				
			 </td>
		</tr>
		<tr>	 
			 <td width="15%" nowrap class="Content_tab_style1">采购方式：</td>
			 <td width="35%" align="left">
			 	${buyWayCn }	
			 </td>
			 <td width="15%" class="Content_tab_style1">供应商选择方式：</td>
			 <td width="35%" align="left">
			 	${supplierTypeCn }	
			 </td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>预算金额：</td>
			<td class="Content_tab_style2">
				${requiredCollect.totalBudget }
			</td>
			<td class="Content_tab_style1" nowrap>编制日期：</td>
			<td class="Content_tab_style2">
				<fmt:formatDate value="${requiredCollect.writeDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> 
			</td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>采购组织：</td>
			<td class="Content_tab_style2">
				${purchaseDeptName }
			</td>
			<td class="Content_tab_style1">负责人：</td>
			<td class="Content_tab_style2">
				${writerCn }
			</td>
		</tr>
   		<tr>
			<td class="Content_tab_style1">负责人单位：</td>
			<td class="Content_tab_style2">
				${deptName }
			</td>			
			<td class="Content_tab_style1" nowrap></td>
			<td class="Content_tab_style2"> 
			</td>
		</tr>
	</table>
	<table align="center" class="table_ys1" id="listtable">	
		<tr><td colspan="8" class="Content_tab_style_td_head">采购物资信息</td></tr>		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap>序号</th>
			<th width="95px" nowrap>编码</th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${rcdList}" var="requiredCollectDetail" varStatus="status">
			<tr  class="input_ys1">
				<td>${status.index+1}
				</td>
				<td>
				    ${requiredCollectDetail.buyCode}
				</td>
				<td>
				   ${requiredCollectDetail.buyName}
				</td>
				<td>
				   ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.unit}
			    </td>
			    <td align="right">
			       ${requiredCollectDetail.amount}
			    </td>
				<td>
					<fmt:formatDate value="${requiredCollectDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
				</td>
				<td>${requiredCollectDetail.remark }</td>
			</tr>
		</c:forEach>		
	</table>
	<c:choose>
	   <c:when test="${requiredCollect.buyWay=='00'}">
	       <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">招标计划</td>
    	    </tr> 
    		<tr>
				<td width="15%" class="Content_tab_style1">开标日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">招标文件领购开始日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.salesDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">招标文件领购截止日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.saleeDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">回标截止日期：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${tenderBidList.returnDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">标书费（元）：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.tenderMoney }
				</td>
				<td width="15%" class="Content_tab_style1">保证金（元）：</td>
				<td width="35%" class="Content_tab_style2">
					${tenderBidList.bondMoney }
				</td>
			</tr>
			<tr>
			    <td width="15%" class="Content_tab_style1">最小报价单位数：</td>
				<td width="35%" class="Content_tab_style2">
				   ${tenderBidList.minBidAmount }
				</td>
				<td width="15%" class="Content_tab_style1">开标管理员：</td>
				<td width="35%" class="Content_tab_style2">
				   ${tenderBidList.bidOpenAdminCn }
				</td>
			</tr>		
			</table>
	   </c:when>
	   <c:when test="${requiredCollect.buyWay=='01'}">
	      <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">询价计划</td>
    	    </tr> 
    	   <tr>
				<td width="15%" class="Content_tab_style1">询价时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${askBidList.askDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">报价截止时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${askBidList.returnDate}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>

			<tr>
				<td width="15%" class="Content_tab_style1">报价解密时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${askBidList.decryptionDate }" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1"></td>
				<td width="35%" class="Content_tab_style2"></td>
		    </tr>
			</table>
	   </c:when>
	   <c:otherwise>
	        <table align="center" class="table_ys2">
    	    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">竞价方案</td>
    	    </tr> 
    	    <tr>
				<td width="15%" class="Content_tab_style1">竞价方式：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.biddingTypeCn }
				</td>
				<td width="15%" class="Content_tab_style1">报价显示方式：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.priceModeCn }
				</td>
			</tr>
			<tr>
				<td width="15%" class="Content_tab_style1">报价原则：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.pricePrincipleCn }
				</td>
				<td width="15%" class="Content_tab_style1">竞价原则：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.biddingPrincipleCn }
				</td>
			</tr>
    		<tr>
				<td width="15%" class="Content_tab_style1">竞价开始时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${biddingBidList.biddingStartTime}" pattern="yyyy-MM-dd HH:mm" />
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束时间：</td>
				<td width="35%" class="Content_tab_style2">
					<fmt:formatDate value="${biddingBidList.biddingEndTime}" pattern="yyyy-MM-dd HH:mm" />
				</td>
			</tr>
            <tr>
				<td width="15%" class="Content_tab_style1">竞价管理员：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.bidAdminCn }
				</td>
				<td width="15%" class="Content_tab_style1">最高限价：</td>
				<td width="35%" class="Content_tab_style2">
				    ${biddingBidList.hignPrice}
				</td>
			</tr><tr>
				<td width="15%" class="Content_tab_style1">最低限价：</td>
				<td width="35%" class="Content_tab_style2">
					${biddingBidList.minPrice}
				</td>
				<td width="15%" class="Content_tab_style1">竞价结束延迟时间(分钟)：</td>
				<td width="35%" class="Content_tab_style2">
				    ${biddingBidList.delayTime}
				</td>
			</tr>
			</table>
	   </c:otherwise>
	</c:choose>
	<table width="100%" class="table_ys1">
	    <tr>
  	          <td colspan="5" class="Content_tab_style_td_head">授标信息</td>
  	        </tr>
		<tr class="Content_tab_style_04">
			<th width="5%" nowrap>
				序号
			</th>
			<th width="30%" nowrap>
				供应商
			</th>
			<th width="15%" nowrap>
				总报价
			</th>
			<th width="25%" nowrap>
				授标价
			</th>	
			<th width="10%" nowrap>
				操作
			</th>
		</tr>
		<c:forEach items="${list}" var="bidPrice" varStatus="status">
			<tr align="center" class="<c:if test="${not empty bidPrice.bidPrice }">red</c:if>">
				<td>${status.index+1}</td>
				<td>${bidPrice.supplierName}</td>
				<td align="right"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></td>
				<td align="right"><fmt:formatNumber value="${bidPrice.bidPrice}" pattern="#00.00#"/></td>
				<td><c:if test="${not empty bidPrice.bidPrice }"><button class='btn btn-mini btn-primary' type="button"  onclick="doView(${bidPrice.baId})"><i class="icon-white icon-bullhorn"></i></button></c:if></td>												
				</tr>
		</c:forEach>

	</table>
</form>
</body>
</html>