<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>采购详情页面</title>
		<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
		<script type="text/javascript">		
		  var api = frameElement.api, W = api.opener;
	      function switchTab(url){
	         $('#iframe').attr('src',url);
	      }
	      function openWindowFrame(title,url,type){
		      createdetailwindow(title,url,type);	
		  }
	    </script>	
	</head>
	<body>
	 <div class="page-content">

         <!-- /.page-header --> 

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">

                             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                <li class="active">
                                     <a data-toggle="tab" onClick="switchTab('viewPurchaseResultInfo_purchaseReport.action?requiredCollect.rcId=${requiredCollect.rcId}');">采购基础数据</a>
                                 </li>
								<li>
                                     <a data-toggle="tab" onClick="switchTab('viewPurchaseNode_purchaseReport.action?requiredCollect.rcId=${requiredCollect.rcId}');">采购整体过程</a>
                                 </li>
                                
                             </ul>

                             <div class="tab-content">
							
                                 <div id="smsj">
                                 	 <iframe src="viewPurchaseResultInfo_purchaseReport.action?requiredCollect.rcId=${requiredCollect.rcId}" id="iframe" frameborder=0 scrolling="yes" width="100%" height="620px"></iframe>
                                 </div> 
                             </div>
                         </div>

                     </div>
                 </div>

                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>

		
</body>
</html>