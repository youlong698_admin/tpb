<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>异常采购结项目查询分析</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "fildAbnomalBidList_purchaseReport.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			showMsg("error","查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               showMsg("error","查询失败");
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            {
            	className : "ellipsis",
            	data: "bidCode",            	
            	width : "6%",
            	render: function(data,type, row, meta) {
                   return "<a href='#' onclick='createdetailwindow(\"采购异常信息\",\"viewBidAbnomalDetail_bidAbnomal.action?rcId=" + row.rcId + "\",2)' title="+data+">"+data+"</a>"; 
                }
            },
            {
            	className : "ellipsis",
            	data: "buyRemark",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
            	orderable : false
            },
			{
				className : "ellipsis",
				data : "buyWayCn",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false,
				width : "6%"
			},
			{
				className : "ellipsis",
				data : "abnomalTypeCn",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "4%",
				orderable : false
			},
			{
				data : "writerCn",
				width : "5%",
				orderable : false
				
			},
			{
				className : "ellipsis",
				data : "abnomalReason",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "8%",
				orderable : false
			},
			{
				className : "ellipsis",
				data : "writeDete",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "7%"
				
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	//给当前行某列加样式
        	$('td', row).eq(11).addClass("text-right");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			//$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	//导出excel
	$("#btn-daochu").click(function(){
		GeneralManage.daochuItems();
	});
	
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.bidCode";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 0:
				param.orderColumn = "de.bidCode";
				break;
			case 6:
				param.orderColumn = "de.writeDete";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		
		//组装查询参数
		
		param.bidCode = $("#bidCode").val();
		param.buyRemark = $("#buyRemark").val();
		param.bidAbnomal = $("#bidAbnomal").val();
		param.buyWay = $("#buyWay").val();
		param.writeDateStart = $("#writeDateStart").val();
		param.writeDateEnd = $("#writeDateEnd").val();
		
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	//导出excel
	daochuItems:function(){
		document.forms[0].action="exportBidAbnomalExcel_purchaseReport.action";
		document.forms[0].submit();
		
	}
};
    
    function doQuery(){
     _table.draw();
    }

	//重置
	function doReset(){        
		document.forms[0].elements["bidCode"].value	=	"";	
		document.forms[0].elements["buyRemark"].value	=	"";
		document.forms[0].elements["bidAbnomal"].value	=	"";
		document.forms[0].elements["buyWay"].value	=	"";	
		document.forms[0].elements["writeDateStart"].value	=	"";	
		document.forms[0].elements["writeDateEnd"].value	=	"";
	}
</script>
</head>
<body >
<div class="container-fluid" >
		<div class="row-fluid">
			<div class="span12" id="content">
				
				<div class="row-fluid" id="div-advanced-search">
					<form class="form-inline well">
					项目编号：
					 <input class="input-medium" placeholder="项目编号"type="text" id="bidCode" name="bidCode" value="" size="15" />
					&nbsp;&nbsp;项目名称：
					 <input class="input-medium" placeholder="项目名称"type="text" id="buyRemark" name="buyRemark" value="" />
					
					&nbsp;&nbsp;采购方式：
					<select name="buyWay" id="buyWay" class="input-small">
						<option value=''>--请选择--</option>
						<c:forEach items='${purchaseWayMap}' var='item'>
							<option value='${item.key }'>${item.value }</option>
						</c:forEach>
				 	</select>
					
					异常类型：
					<select id="bidAbnomal" name="bidAbnomal"  class="input-small">
					 		<option value="">全部</option>
							<c:forEach items='${bidAbnomal}' var='item'>
							<option value='${item.key }'>${item.value }</option>
						</c:forEach>
					</select>
					&nbsp;&nbsp;异常日期：
					<input type="text" class="input-medium Wdate" id="writeDateStart" name="writeDateStart" size="15" value="" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })"/>
					至：<input type="text" class="input-medium Wdate" id="writeDateEnd" name="writeDateEnd" size="15" value ="" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })"/>
			 		 
			 		 &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-info" id="btn-advanced-search" ><i class="icon-white icon-search"></i> 查询</button>
				      <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
						<button type="button" class="btn btn-info" id="btn-daochu"><i class="icon-white icon-share"></i> 导出Excel</button>					
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>项目编号</th>
									<th>项目名称</th>
									<th>采购方式</th>
									<th>异常类型</th>
									<th>操作人</th>
									<th>原因</th>
									<th>操作时间</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>