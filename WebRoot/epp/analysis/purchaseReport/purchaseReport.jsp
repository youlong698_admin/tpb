<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>采购概况报表</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
<![endif]-->
<script type="text/javascript">
var _table;
$(function (){
	
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findPurchaseReport_purchaseReport.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            {
				data : "purchaseDeptName",
				width : "14%",
				render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
            {
            	data: "bidCode",  
				render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,          	
            	width : "15%"      
            },
            {	
            	className : "ellipsis",
            	data: "buyRemark",     	
            	width : "15%", 
            	orderable : false,
            	render: function(data,type, row, meta) {
                   return "<a href='javascript:void(0);' onclick='createdetailwindow(\"采购项目信息\",\"viewPurchaseReportTab_purchaseReport.action?requiredCollect.rcId=" + row.rcId + "\",3)' title="+data+">"+data+"</a>"; 
                }		
            },
			{
				data : "buyWayCn",
				width : "10%",
				render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS
			},
			{
				data : "supplierName",
				width : "15%",
				render: function(data,type, row, meta) {
                   return "<a href='javascript:void(0);' onclick='createdetailwindow(\"查看供应商明细\",\"viewSupplierSelectInfo_supplierBaseInfo.action?supplierInfo.supplierId="+row.supplierId+"\",1)' title="+data+">"+data+"</a>"; 
                },
			},
			{
            	className : "numPrice",
				data : "bidPrice",
				width : "10%",
				render:CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE
			},
			{
				data : "writerCn",
				width : "10%",
				orderable : false
			},
			{
				className : "ellipsis",
				data : "baDate",
				render:CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				width : "10%"
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
            $('td', row).eq(9).addClass("light-red");
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象
 	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};
		//组装排序参数
		//默认进入的排序
		 param.orderColumn="de.rcId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 3:
				param.orderColumn = "de.bidCode";
				break;
			case 7:
				param.orderColumn = "de.writeDate";
				break;
			default:
				param.orderColumn = "de.rcId";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		//组装查询参数
		
		param.bidCode = $("#bidCode").val();
		param.buyRemark = $("#buyRemark").val();
		param.purchaseWay = $("#purchaseWay").val();
		param.supplierName = $("#supplierName").val();
		param.dateStart = $("#dateStart").val();
		param.writeDateEnd = $("#writeDateEnd").val();
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	}
 	
};
    function doQuery(){
     _table.draw();
    }
	//重置
	function doReset(){        
		document.forms[0].elements["bidCode"].value	=	"";	
		document.forms[0].elements["buyRemark"].value	=	"";
		document.forms[0].elements["purchaseWay"].value	=	"";
		document.forms[0].elements["supplierName"].value	=	"";
		document.forms[0].elements["dateStart"].value	=	"";
		document.forms[0].elements["dateEnd"].value	=	"";
	}
	//导出EXCEL
	function doExport(){
	   var bidCode=$("#bidCode").val();
	   var buyRemark=$("#buyRemark").val();
	   var purchaseWay=$("#purchaseWay").val();
	   var supplierName=$("#supplierName").val();
	   var dateStart=$("#dateStart").val();
	   var dateEnd=$("#dateEnd").val();
	   window.location.href="exportPurchaseReportExcel_purchaseReport.action?bidCode="+bidCode+"&buyRemark="+buyRemark+"&purchaseWay="+purchaseWay+"&supplierName="+supplierName+"&dateStart="+dateStart+"&dateEnd="+dateEnd;
	}
</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid" id="div-advanced-search">
					<form class="form-inline well">						
						项目编号：
						<input class="input-medium" type="text" name="bidCode" placeholder="项目编号" id="bidCode" value="" size="15" />
						项目名称：
						<input class="input-medium" type="text" name="buyRemark" placeholder="项目名称" id="buyRemark" size="15" value=""  />
						采购方式：
						<select name="purchaseWay" id="purchaseWay" class="input-small">
						   <option value="">全部</option>
						   <c:forEach items="${purchaseWayMap}" var="map">
						      <option value="${map.key }">${map.value }</option>
						   </c:forEach>
						</select>
						供应商名称：						
						<input class="input-medium" type="text" name="supplierName" placeholder="供应商名称" id="supplierName" size="15" value=""  />
						<br/>
						时间周期：
						<input type="text" class="input-medium Wdate" id="dateStart" name="dateStart" placeholder="起始时间" size="15" value="" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"/>
						至：<input type="text" class="input-medium Wdate" id="dateEnd" name="dateEnd" placeholder="终止时间" size="15" value ="" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'dateStart\')}'})"/>
					 	<button type="button" class="btn btn-info" id="btn-advanced-search" onclick="doQuery();"><i class="icon-white icon-search"></i> 查询</button>
					    <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
					    <button type="button" class="btn btn-info" id="btn-daochu" onclick="doExport();"><i class="icon-white icon-share"></i> 导出Excel</button>		
					 	
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>采购组织</th>
									<th>项目编号</th>
									<th>项目名称</th>
									<th>采购方式</th>
									<th>中标供应商</th>
									<th>中标金额</th>
									<th>采购负责人</th>
									<th>授标日期</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>