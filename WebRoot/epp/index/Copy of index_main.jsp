<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>通用管理系统</title>
		<%-- 页面清缓存BGN --%>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
        <link rel="stylesheet" type="text/css" href="<%= path %>/style/index.css" />
	</head>

	<body>
  <div id="container">
   <div class="main_content">
		
     <div class="mian_div1">
		<div class="listtitle"><a href="javascript:window.parent.addTabs({id:'9998',title:'更多待办',close: true,url: 'viewWaitForDeal_viewWaitForDeal.action'});" class="more1"><img src="<%= path %>/images/more02.png" /></a><img src="<%= path %>/images/d07.png" /> 待办事项(${wfdCount })</div>
		<table class="table_ys2 overHidden" style="table-layout: fixed;">
            <thead>
                <tr class="Content_tab_style_04">
                    <th width="5%">序号</th>
                    <th width="15%">事项类型</th>
                    <th width="35%">事项名称</th>
                    <th width="10%">流程环节</th>
                    <th width="10%" >上一步处理人</th>
                    <th width="10%">发送人</th>
                    <th width="15%">发送时间</th>
                </tr>
            </thead>
            <tbody>
                <s:iterator value="#request.wfdList" status="wfd">
                <s:if test="#wfd.Count <= 5">
				<tr <s:if test="#wfd.Even">class='biaoge_01_a'</s:if><s:else>class='biaoge_01_b'</s:else>>
				<td align="center"><s:property value="#wfd.count"/></td>
				<td align="center" title="<s:property value="processName"/>">
				<s:property value="processName"/>
				</td>
				<td align="left" title="<s:property value="wfName"/>">
				<a href="#" onclick="createdetailwindow('<s:property value="processName"/>','<%=basePath %><s:property value="actionUrl"/>',3)">
				<s:if test="taskType==1">流程退回：</s:if><s:property value="wfName"/>
				</a>
				</td>
				<td align="center"><s:property value="taskName"/></td>
				<td align="center"><s:property value="lastUpdatorCn"/></td>
				<td align="center"><s:property value="creatorCn"/></td>
				<td align="center"><s:property value="orderCreateTime"/></td>
				</tr>
				</s:if>
				</s:iterator>
            </tbody>
        </table>
		</div>
		<div style="width:2%;float:left;">&nbsp;</div>
		<div class="mian_div2">
			 <div class="listtitle"><img src="<%= path %>/images/ico03.png" />  采购统计</div>    
		      <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
	    <div id="main" style="height:300px;"></div>
	    <c:if test="${not empty data}">
	    <!-- ECharts单文件引入 -->
	    <script src="<%= path %>/common/echarts/echarts.min.js"></script>
	    <script type="text/javascript">
	        // 基于准备好的dom，初始化echarts图表
	        var myChart = echarts.init(document.getElementById('main')); 
	        
	        var option = {
			    tooltip : {
			        trigger: 'item',
			        formatter: "{a} <br/>{b} : {c} ({d}%)"
			    },
			    gird:{
			       x:0,
			       y:0,
			       x2:0,
			       y2:0			       
			    },
			    series : [
			        {
			            name: '采购金额',
			            type: 'pie',
			            radius : '75%',
			            center: ['50%', '60%'],
			            data:[
			                ${data}
			            ],
			            itemStyle: {
			                emphasis: {
			                    shadowBlur: 10,
			                    shadowOffsetX: 0,
			                    shadowColor: 'rgba(0, 0, 0, 0.5)'
			                }
			            }
			        }
			    ]
			};
	
	        // 为echarts对象加载数据 
	        myChart.setOption(option); 
	    </script>
	    </c:if>
		</div>
   </div>
	<div id="hd">
    </div>
    <div id="bd">
    	<div id="main">
            <ul class="nav-list ue-clear">
            	<li class="nav-item desk">
                	<a href="viewIndexInfo_IndexEpp.action">
                        <p class="icon"></p>
                        <p class="title">我的桌面</p>
                    </a>
                </li>
                <li class="nav-item dosthings">
                	<a href="javascript:;" onclick="window.parent.addTabs({id:'9998',title:'更多待办',close: true,url: 'viewWaitForDeal_viewWaitForDeal.action'});">
                        <p class="icon"></p>
                        <p class="title">待办事项</p>
                    </a>
                </li>
                <li class="nav-item mail">
                	<a href="javascript:;" onclick="goAllMessage();">
                        <p class="icon"></p>
                        <p class="title">业务提醒</p>
                    </a>
                </li>
                <li class="nav-item notice">
                	<a href="javascript:;" onclick="goAllNotice();">
                        <p class="icon"></p>
                        <p class="title">公告通知</p>
                    </a>
                </li>
                
                <li class="nav-item logs">
                	<a href="javascript:;" onclick="window.parent.addTabs({id:'9999',title:'更多已办',close: true,url: 'viewAlreadyDone_viewWaitForDeal.action'});">
                        <p class="icon"></p>
                        <p class="title">已办事项</p>
                    </a>
                </li>
                 <li class="nav-item news">
                	<a href="javascript:;" onclick="window.parent.addTabs({id:'51',title:'供应商查询',close: true,url: 'viewSupplierBaseInfo_supplierBaseInfo.action'});">
                        <p class="icon"></p>
                        <p class="title">供应商查询</p>
                    </a>
                </li>
                <!--  
                <li class="nav-item tjjl">
                	<a href="javascript:;" onclick="window.parent.addTabs({id:'9980',title:'推广查询',close: true,url: 'viewUserstjjl_usersReferra.action'});">
                        <p class="icon"></p>
                        <p class="title">推广查询</p>
                    </a>
                </li>
                -->
            </ul>
            </div>
        </div>
    </div>
    <script type="text/javascript">
       
    function goAllNotice(){
    	var addurl = "viewNotice_IndexEpp.action";
  		createdetailwindow("公告", addurl, 1);
    }
     function goAllMessage(){
    	var addurl = "viewMessages_IndexEpp.action";
  		createdetailwindow("消息", addurl, 1, 2);
    }
    </script>
</body>

</html>