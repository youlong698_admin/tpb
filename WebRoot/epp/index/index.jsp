<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@ include file="/common/context.jsp"%>
<%
  SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L); 
 %>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		 <title><%=systemConfiguration.getSystemCurrentDept() %>电子采购信息管理平台</title>
        <%-- 页面清缓存BGN --%>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />

	
		<!-- basic styles -->
		<link href="<%=basePath %>/common/ace/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=basePath %>/common/font-awesome-4.7.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<!--  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />  -->

		<!-- ace styles -->
		<link rel="stylesheet" href="<%=basePath %>/common/ace/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=basePath %>/common/ace/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="<%=basePath %>/common/ace/assets/css/ace-skins.min.css" />
		 
		
		
		<!-- ace settings handler -->

		<script src="<%=basePath %>/common/ace/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
		<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
		<![endif]-->
		<style type="text/css">
		.lefttop{background:url(<%=basePath %>/images/lefttop.gif) repeat-x;height:40px;line-height:40px;}
		.lefttop strong{color:#fff;font-size:14px;margin-left: 10px;float:left;}
		.lefttop span{margin-left:8px; margin-top:10px;margin-right:8px; background:url(<%=basePath %>/images/leftico.png) no-repeat; width:20px; height:21px;float:left;}
		
		</style>
	</head>

	<body>
		<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">							
					<img src="<%=basePath %>/images/logo.png" />
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
								<span class="badge badge-important" id="noticeCount">0</span>
							</a>

							<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header" id="noticeTip">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									0条公告
								</li>
								
								<li class="dropdown-content" id="noticeContent">
									<ul class="dropdown-menu dropdown-navbar navbar-pink" >
										<!-- ajax加载 -->
									</ul>
								</li>
								
								<li>
									<a href="javascript:goAllNotice();" id="noticeFooter">
										查看全部
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<span class="badge badge-success" id="messageCount">0</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header" id="messageTip">
									<i class="ace-icon fa fa-envelope-o"></i>
									0条消息
								</li>

								<li id="messageContent">
									<ul class="dropdown-menu dropdown-navbar navbar-pink" >
										<!-- ajax加载 -->
									</ul>
								</li>

								

								<li>
									<a href="javascript:goAllMessage();" id="messageFooter">
										查看所有消息
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<%=basePath %>/common/ace/assets/avatars/avatar2.png" alt="Jason's Photo" />
								<span class="user-info">
									<small>${ur.userChinesename }</small>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="javascript:createdetailwindow_choose('修改密码','updatePwdInit_IndexEpp.action',4)" style="cursor:pointer;">
										<i class="ace-icon fa fa-user"></i>
										 修改密码
									</a>
								</li>
								
							    <li>
							        <a onclick="javascript:createdetailwindow_choose('代码生成','viewToProductCode_sysSet.action',1);" style="cursor:pointer;">
							              <i class="ace-icon fa fa-cogs"></i> 
							                                     代码生成
							          </a>
							    </li>						
								<li>
									<a href="javascript:clearLocalstorage()" style="cursor:pointer;">
										<i class="ace-icon fa fa-cog"></i>
										 清除浏览记录
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="javascript:logout()">
										<i class="ace-icon fa fa-power-off"></i>
										 注销
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="lefttop sidebar-shortcuts-large" id="sidebar-shortcuts-large"> <span></span><strong>功能菜单</strong></div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<li class="active">
							<a  href="javascript:addTabs({id:'home',title:'首页',close: false,url: 'viewIndexInfo_IndexEpp.action'});">
								<i class="menu-icon fa fa-home"></i>
								<span class="menu-text"> 首页 </span>
							</a>
						</li>
						
						<c:forEach  var="authority" items="${authorityList}">
						<c:if test="${fn:length(authority.subAuthorityList) > 0}">
						<li>
							<a href="javascript:void(0);" class='dropdown-toggle'>
								<i class="${ authority.menuImg}"></i>
								<span class="menu-text"> ${authority.menuName } </span>
								
								<b class="arrow fa fa-angle-down"></b>
							</a>
							<ul  class="submenu" >
								<c:forEach var="subAuthorityList" items="${authority.subAuthorityList}">
								<li > 
										<c:if test="${subAuthorityList.subAuthorityList==null}">
												<a href="javascript:addTabs({id:'${subAuthorityList.menuId }',title:'${subAuthorityList.menuName }',close: true,url: '<%=basePath %>${subAuthorityList.pathCode }'});">
												<i class="menu-icon fa fa-caret-right"></i>
													<span class="menu-text"> ${subAuthorityList.menuName } </span>
													
												</a>
										</c:if>
										<c:if test="${fn:length(subAuthorityList.subAuthorityList) > 0}">
										
												<a class="dropdown-toggle"  href="javascript:void(0);" >
												<i class="menu-icon fa fa-caret-right"></i>
													<span class="menu-text"> ${subAuthorityList.menuName } </span>
													<b class="arrow icon-angle-down"></b>
													</a>
												<ul class="submenu" >
											<c:forEach var="subSubAuthorityList" items="${subAuthorityList.subAuthorityList}">
												<c:if test="${fn:length(authority.subAuthorityList) > 0}">
													 
													
														<li > 
															<a href="javascript:addTabs({id:'${subSubAuthorityList.menuId }',title:'${subSubAuthorityList.menuName }',close: true,url: '<%=basePath %>${subSubAuthorityList.pathCode }'});">
															<i class="menu-icon fa fa-caret-right"></i>
															<span class="menu-text"> ${subSubAuthorityList.menuName } </span>
															
															</a>
														</li>
												</c:if>
											</c:forEach>
											</ul>
											</c:if>
										</li>
									
							</c:forEach>
							</ul>
						</li>
						</c:if>
						</c:forEach>
						
					</ul>

					<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
						 <i class="ace-icon fa fa-arrow-left" data-icon1="ace-icon fa fa-arrow-left" data-icon2="ace-icon fa fa-arrow-right" style="color:#000;"></i> 
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>

				<div class="main-content">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12" style="width: 100%;padding-left:2px;padding-right: 2px;" id="tabs">
	                            <ul class="nav nav-tabs" role="tablist">
	                                <!-- <li class="active"><a href="#Index" role="tab" data-toggle="tab">首页</a></li> -->
	                            </ul>
	                            <div class="tab-content">
	                                <div role="tabpanel" class="tab-pane active" id="Index">
	                                </div>
	                            </div>
	                        </div>
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="ace-icon fa fa-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; 选择皮肤</span>
						</div>

						<div>
							<input type="checkbox"  id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"> 固定导航条</label>
						</div>

						<div>
							<input type="checkbox"  id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"> 固定滑动条</label>
						</div>

						<div>
							<input type="checkbox"  id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl">切换到左边</label>
						</div>

						<div>
							<input type="checkbox"  id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								切换窄屏
								<b></b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<script src='<%=basePath %>/common/jQuery/jquery-1.8.3.min.js'></script>
		<script src="<%=basePath %>/common/jQuery/storage/jquery.storageapi.min.js"></script>		
		<script src="<%=basePath %>/common/ace/assets/js/bootstrap.min.js"></script>
		<script src="<%=basePath %>/common/ace/assets/js/ace-elements.min.js"></script>
		<script src="<%=basePath %>/common/ace/assets/js/ace.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>/common/ace/assets/js/bootstrap-tab.js"></script>
	    <script src="<%=basePath%>/common/lhgDialog/lhgdialog.js?skin=bootstrap2" type="text/javascript"></script>
         <script src="<%=basePath%>/common/script/common.js" type="text/javascript"></script>
         <script src="<%=basePath%>/common/jQuery/jquery.cookie.js" type="text/javascript"></script>
     
		<!-- inline scripts related to this page -->
		<script>
		jQuery(function($) {
		
	    	var height=$(window).height();
	    	height=height>920?height:920;
	    	$('#sidebar').height(height-90);
	    	
			//$( "#tabs" ).tabs();
			addTabs({id:'home',title:'首页',close: false,url: 'viewIndexInfo_IndexEpp.action'});
			$('.theme-poptit .close').click(function(){
	    		$('.theme-popover-mask').fadeOut(100);
	    		$('.theme-popover').slideUp(200);
	    	});
	    	$('#closeBtn').click(function(){
	    		$('.theme-popover-mask').fadeOut(100);
	    		$('.theme-popover').slideUp(200);
	    	});
	    	//$('#ace-settings-sidebar').click();
	    	//$('#sidebar').addClass('compact');
			$('#sidebar li').addClass('hover').filter('.open').removeClass('open').find('> .submenu').css('display', 'none');
			
			
		});
		
		</script>

		
		
		<script type="text/javascript">
		

	  	function logout(){
	  		$.dialog.confirm("温馨提示：您确定要注销系统么？", function() {
	  			location.href="<%=basePath %>/exit_verify.action";
	  		});
  		}
		function clearLocalstorage(){
				var storage=$.localStorage;
				if(!storage)
					storage=$.cookieStorage;
				storage.removeAll();
				showMsg("alert", "浏览器缓存清除成功!");
		}

	$(document).ready(function(){
	
		jQuery.ajax({
    		url:"viewNoticeList_IndexEpp.action",
    		type:"POST",
    		dataType:"JSON",
    		async: false,
    		success:function(data){
    		
    				var noticeList = data.list;
    				var noticeCount =data.count;
    				
    				//加载公告条数
    				if(noticeCount>99){
    					$("#noticeCount").html("99+");
    				}else{
    					$("#noticeCount").html(noticeCount);
    				}
    				
    				//加载公告提示
    				var noticeTip = "";
    				noticeTip += "<i class='ace-icon fa fa-bell icon-animated-bell'></i> ";
    				noticeTip += noticeCount+"条公告信息";
    				$("#noticeTip").html(noticeTip);
    				
    				//加载公告条目
    				var noticeContent = "";
    				var nots="";
    				if(noticeList.length > 0){
    					if(noticeList.length<=7)
    					{
    						
	    					for(var i=0;i<noticeList.length;i++){
	    						if(noticeList[i].title.length>=15)
	    						{
	    							nots=noticeList[i].title.substring(0,14)+"...";
	    						}else
	    						{
	    							nots=noticeList[i].title;
	    						}
	    						noticeContent +="<li><a href='javascript:goNotice(&quot;"+noticeList[i].noticeId+"&quot;)' ";
	    						noticeContent +="style='word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;'>";
	    						noticeContent +="<i class='btn btn-xs btn-primary fa fa-user'></i>";
	    						noticeContent +="&nbsp;"+nots + "</a></li>";
	        				}
        				}else
        				{
        					for(var i=0;i<7;i++){
        						if(noticeList[i].title.length>=15)
	    						{
	    							nots=noticeList[i].title.substring(0,14)+"...";
	    						}else
	    						{
	    							nots=noticeList[i].title;
	    						}
	    						noticeContent +="<li><a href='javascript:goNotice(&quot;"+noticeList[i].noticeId+"&quot;)' ";
	    						noticeContent +="style='word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;'>";
	    						noticeContent +="<i class='btn btn-xs btn-primary fa fa-user'></i>";
	    						noticeContent +="&nbsp;"+nots + "</a></li>";
	        				}
        				}
    				}
    				$("#noticeContent").after(noticeContent);
    				
    		}
    	});
		
		
		//加载消息
		$.ajax({
    		url:"viewMessageList_IndexEpp.action",
    		type:"POST",
    		dataType:"JSON",
    		async: false,
    		success:function(data){
    			
    				var messageList = data.list;
    				var messageCount =data.count;
    				//加载消息条目（有限）
    				var messageContent = "";
    				var icon="";
    				var url="";
    				var message="";
    				if(messageList.length > 0){
    					if(messageList.length<=7)
    					{
	    					for(var i=0;i<messageList.length;i++){
	    						if(messageList[i].status=="1")  //已读消息
	    						{
	    							icon="<i class='fa fa-check-square' style='color:#69aa46'></i>";
	    							url="javascript:void(0);";
	    						}else
	    						{
	    							icon="<i class='fa fa-minus-square' style='color:#d2322d;'></i>";
	    							url="javascript:goMessage(&quot;"+messageList[i].smlId+"&quot;);";
	    						}
	    						
	    						if(messageList[i].messageContent.length>=17)
	    						{
	    							message=messageList[i].messageContent.substring(0,16)+"...";
	    						}else
	    						{
	    							message=messageList[i].messageContent;
	    						}
	    						messageContent +="<li class='messTwo'><a href='"+url+"' ";
	    						messageContent +="style='word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;' title='"+messageList[i].messageContent+"'>";
	    						messageContent +=icon;
	    						messageContent +="&nbsp;"+message + "</a></li>";
	        				}
        				}else
        				{
        					for(var i=0;i<7;i++){
	    						if(messageList[i].status=="1")  //已读消息
	    						{
	    							icon="<i class='fa fa-check-square' style='color:#69aa46'></i>";
	    							url="javascript:void(0);";
	    						}else
	    						{
	    							icon="<i class='fa fa-minus-square' style='color:#d2322d;'></i>";
	    							url="javascript:goMessage(&quot;"+messageList[i].smlId+"&quot;);";
	    						}
	    						if(messageList[i].messageContent.length>=17)
	    						{
	    							message=messageList[i].messageContent.substring(0,16)+"...";
	    						}else
	    						{
	    							message=messageList[i].messageContent;
	    						}
	    						messageContent +="<li class='messTwo'><a href='"+url+"' ";
	    						messageContent +="style='word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;' title='"+messageList[i].messageContent+"'>";
	    						messageContent +=icon;
	    						messageContent +="&nbsp;"+messageList[i].messageContent + "</a></li>";
	        				}
        				}
    				}
    				
    				//加载消息条数
    				if(messageCount>99){
    					$("#messageCount").html("99+");
    				}else{
    					$("#messageCount").html(messageCount);
    				}
    				//加载消息tip提示
    				var messageTip = "";
					messageTip += "<i class='ace-icon fa fa-envelope-o'></i>  ";
					messageTip += messageCount+"条未读消息";
    				$("#messageTip").html(messageTip);
    				
    				$("#messageContent").after(messageContent);
    				
    		}
    	});
		
	});

    function goAllNotice(){
    	var addurl = "viewNotice_IndexEpp.action";
  		createdetailwindow("公告", addurl, 1);
    }

    function goNotice(id){
    	var addurl = "viewNoticesDetail_notices.action?notices.noticeId="+id;
		createdetailwindow("通知公告详情", addurl, 2);
    }
    
    function goAllMessage(){
    	var addurl = "viewMessages_IndexEpp.action";
  		createdetailwindow("消息", addurl, 1, 2);
    }
    
    function goMessage(id){
    	 ajaxGeneral("updateMessage_IndexEpp.action","messageId="+id,"text");
		   
		 //重新加载消息
		$.ajax({
    		url:"viewMessageList_IndexEpp.action",
    		type:"POST",
    		dataType:"JSON",
    		async: false,
    		success:function(data){
    			
    				var messageList = data.list;
    				var messageCount =data.count;
    				
    				//清空原有数据
    				$(".messTwo").empty();
    				//加载消息条目（有限）
    				var messageContent = "";
    				var icon="";
    				var url="";
    				var message="";
    				if(messageList.length > 0){
    					if(messageList.length<=7)
    					{
	    					for(var i=0;i<messageList.length;i++){
	    						if(messageList[i].status=="1")  //已读消息
	    						{
	    							icon="<i class='fa fa-check-square' style='color:#69aa46'></i>";
	    							url="javascript:void(0);";
	    						}else
	    						{
	    							icon="<i class='fa fa-minus-square' style='color:#d2322d;'></i>";
	    							url="javascript:goMessage(&quot;"+messageList[i].smlId+"&quot;);";
	    						}
	    						if(messageList[i].messageContent.length>=17)
	    						{
	    							message=messageList[i].messageContent.substring(0,16)+"...";
	    						}else
	    						{
	    							message=messageList[i].messageContent;
	    						}
	    						messageContent +="<li class='messTwo'><a href='"+url+"' ";
	    						messageContent +="style='word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;' title='"+messageList[i].messageContent+"'>";
	    						messageContent +=icon;
	    						messageContent +="&nbsp;"+messageList[i].messageContent + "</a></li>";
	        				}
        				}else
        				{
        					for(var i=0;i<7;i++){
	    						if(messageList[i].status=="1")  //已读消息
	    						{
	    							icon="<i class='fa fa-check-square' style='color:#69aa46'></i>";
	    							url="javascript:void(0);";
	    						}else
	    						{
	    							icon="<i class='fa fa-minus-square' style='color:#d2322d;'></i>";
	    							url="javascript:goMessage(&quot;"+messageList[i].smlId+"&quot;);";
	    						}
	    						if(messageList[i].messageContent.length>=17)
	    						{
	    							message=messageList[i].messageContent.substring(0,16)+"...";
	    						}else
	    						{
	    							message=messageList[i].messageContent;
	    						}
	    						messageContent +="<li class='messTwo'><a href='"+url+"' ";
	    						messageContent +="style='word-break:keep-all;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;' title='"+messageList[i].messageContent+"'>";
	    						messageContent +=icon;
	    						messageContent +="&nbsp;"+messageList[i].messageContent + "</a></li>";
	        				}
        				}
    				}
    				//加载消息条数
    				if(messageCount>99){
    					$("#messageCount").html("99+");
    				}else{
    					$("#messageCount").html(messageCount);
    				}
    				//加载消息tip提示
    				var messageTip = "";
					messageTip += "<i class='ace-icon fa fa-envelope-o'></i>  ";
					messageTip += messageCount+"条消息";
    				$("#messageTip").html(messageTip);
    				
    				$("#messageContent").after(messageContent);
    				
    		}
    	});							
		
    }
    
	</script>
</body>
</html>

