<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>通用管理系统</title>
		<%-- 页面清缓存BGN --%>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
		<link rel="stylesheet" type="text/css"
			href="<%=path%>/style/index.css" />
		<link href="<%=path%>/common/ace/assets/css/bootstrap.min.css"
			rel="stylesheet" type="text/css" />
		<script src="<%=path%>/style/indexCss/iconfont.js"></script>
	    <!-- ECharts单文件引入 -->
	    <script src="<%= path %>/common/echarts/echarts.min.js"></script>
		<style type="text/css">
		.icon {
		   width: 1em; height: 1em;
		   vertical-align: -0.15em;
		   fill: currentColor;
		   overflow: hidden;
		}
		</style>
	</head>
	<body>
		<div>
			<div class="content">
				<div class="container-fluid">
				    <div class="row">
				      <div class="col-xs-3 col-lg-3">
				           <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-info">
                                         <svg class="icon" aria-hidden="true">
					                        <use xlink:href="#icon-shenpi"></use>
					                    </svg>
					                </span>
                                    <div class="mini-stat-info text-center text-primary">
                                        <a href="javascript:window.parent.addTabs({id:'9998',title:'待办审批',close: true,url: 'viewWaitForDeal_viewWaitForDeal.action'});"><span class="counter text-dark">${wfdCount }</span></a>
                                                                                                            待办审批
                                    </div>
                           </div>         
				      </div>				      				     
				      <div class="col-xs-3 col-lg-3">
				           <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-success">
                                       <svg class="icon" aria-hidden="true">
					                        <use xlink:href="#icon-user-approval"></use>
					                    </svg>
                                    </span>
                                    <div class="mini-stat-info text-center text-primary">
                                        <a href="javascript:window.parent.addTabs({id:'9999',title:'已办审批',close: true,url: 'viewAlreadyDone_viewWaitForDeal.action'});"><span class="counter text-dark">${wadCount }</span></a>
                                                                                                         已办审批
                                    </div>
                           </div>         
				      </div>
				      <div class="col-xs-3 col-lg-3">
				           <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-warning">
                                       <svg class="icon" aria-hidden="true">
					                        <use xlink:href="#icon-xiaoxitixing"></use>
					                    </svg>
                                    </span>
                                    <div class="mini-stat-info text-center text-primary">
                                        <a href="javascript:goAllMessage();"><span class="counter text-dark">${messageCount }</span></a>
                                                                                                           业务提醒
                                    </div>
                           </div>         
				      </div>
				      <div class="col-xs-3 col-lg-3">
				           <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-pink">
                                       <svg class="icon" aria-hidden="true">
					                        <use xlink:href="#icon-icon-cart"></use>
					                    </svg>
                                    </span>
                                    <div class="mini-stat-info text-center text-primary">
                                        <span class="counter text-dark">${reqCount }</span>
                                                                                                          在途采购
                                    </div>
                           </div>         
				      </div>
				    </div>
					<div class="row">
						<div class="col-xs-3 col-lg-3">
							<div class="card card-purple">
								<div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">
											<span class="mini-stat-icon">
											<svg class="icon" aria-hidden="true">
						                        <use xlink:href="#icon-caigou-tianchong"></use>
						                    </svg>
						                    </span>
										</div>
										<h5 class="text-dark2">
											本年度采购项目量
										</h5>
										<h3 class="mt-2 text-purple">
											${yPurchaseCount }
										</h3>
									</div>
								</div>								
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-purple">${mPurchaseCount }</span></h5>
                                        </div>
                                </div>
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-pink">
								<div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-qiyeyanfatouruzongjine"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											本年度采购总额
										</h5>
										<h3 class="mt-2 text-pink">
											${yPurchaseMoneySum }
										</h3>
									</div>
								</div>								
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-pink">${mPurchaseMoneySum }</span></h5>
                                        </div>
                                </div>
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-red">
								<div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-save"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											本年度节资总额
										</h5>
										<h3 class="mt-2 text-red">
											${yEconomyMoneySum }
										</h3>
									</div>
								</div>								
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-red">${mEconomyMoneySum }</span></h5>
                                        </div>
                                </div>
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-primary">
							    <div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">											
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-yizhongzhi"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											年度终止采购量
										</h5>
										<h3 class="mt-2 text-primary">
											${yBidAbnomalCount }
										</h3>
									</div>
								</div>								
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-primary">${mBidAbnomalCount }</span></h5>
                                        </div>
                                </div>
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-danger">							    
							    <div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">											
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-hetongbeian"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											本年度合同总额
										</h5>
										<h3 class="mt-2 text-danger">
											${yContractSum }
										</h3>
									</div>
								</div>								
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-danger">${mContractSum }</span></h5>
                                        </div>
                                </div>
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-info">							    
							    <div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">											
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-dingdan"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											本年度订单总额
										</h5>
										<h3 class="mt-2 text-info">
											${yOrderSum }
										</h3>
									</div>
								</div>								
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-info">${mOrderSum }</span></h5>
                                        </div>
                                </div>
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-warning">					    
							    <div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-pinzhigongyingshang"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											年度成交供应商数
										</h5>
										<h3 class="mt-2 text-warning">
											${mBidSuppCount }
										</h3>
									</div>
								</div>
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-warning">${yBidSuppCount }</span></h5>
                                        </div>
                                </div>
								
							</div>
						</div>
						<div class="col-xs-3 col-lg-3">
							<div class="card card-cmyk">					    
							    <div class="card-heading"></div>
								<div class="card-body">
									<div>
										<div class="float-right">
											<span class="mini-stat-icon">
											   <svg class="icon" aria-hidden="true">
						                          <use xlink:href="#icon-icon03"></use>
						                       </svg>
						                     </span>
										</div>
										<h5 class="text-dark2">
											年度新增供应商数
										</h5>
										<h3 class="mt-2 text-cmyk">
											${ySuppCount }
										</h3>
									</div>
								</div>
								<div class="tiles-progress">
                                        <div>
                                            <h5>本月统计量 <span class="pull-right mt-2 text-cmyk">${mSuppCount }</span></h5>
                                        </div>
                                </div>
								
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-xs-8 col-lg-8">
						   <div class="chart-card">	
						   <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
								  <div id="main1" style="width:100%;height:260px"></div>
								   <script type="text/javascript">
				        // 基于准备好的dom，初始化echarts图表
				        var myChart = echarts.init(document.getElementById('main1')); 

						var myColor=['red','orange','yellow','green','cyan','blue','purple','#C1232B','#B5C334','#FCCE10','#E87C25','#27727B'];
						var xAxisData=[${xAxisData}];
						
						
						var option = {
						    title: [{
						        text: '采购金额统计',
						        x: '45%',
						        textAlign: 'center'
						    }],
						    toolbox:{
						        left:20,
						        feature:{
						            saveAsImage: {name:'采购金额成交统计'},
						            dataView: {},
						            magicType: {
						                type:['line','bar']
						            },
						            // brush: {},
						        }
						    },
						    tooltip : {
								 trigger:'item'
							},
						    grid: [
						        {
						            show: false,
						            left: 10,
						            top: '25%',
						            containLabel: true,
						            width: '95%',
						            height:'70%'
						        }
						    ],
						    xAxis: [
						        {
						            gridIndex:0,
						            axisTick:{
						                alignWithLabel: true
						            },
						            data:xAxisData
						        }
						    ],
						    yAxis: [
						        {
						            gridIndex:0,
						            name:"金额(万)",
						        }
						    ],
						   
						    series: [
						        {
						            type: 'bar',
						            name:'金额',
						            xAxisIndex:0,
						            yAxisIndex:0,
						            itemStyle: {
						                normal: {
						                    color: function(params) {
						                        var num=myColor.length;
						                        return myColor[params.dataIndex%num]
						                    },label: {
						                        show: true,
						                        position: 'top',
						                        formatter: '{b}\n{c}'
						                    }
						                }
						            },
						            data:[${amountMoney}]
						        }
						    ]
						};


 

				
				        // 为echarts对象加载数据 
				        myChart.setOption(option); 
				    </script>
						   </div>
						</div>
						<div class="col-xs-4 col-lg-4">
						   <div class="chart-card">	
							   <div id="main" style="width:100%;height:250px;"></div>
							    <c:if test="${not empty data}">
							    <script type="text/javascript">
							        // 基于准备好的dom，初始化echarts图表
							        var myChart = echarts.init(document.getElementById('main')); 
							        
							        var option1 = {
								        title: [{
									        text: '采购方式对比分析',
									        x: 'center'
									    }],
									    tooltip : {
									        trigger: 'item',
									        formatter: "{a} <br/>{b} : {c} ({d}%)"
									    },
									    series : [
									        {
									            name: '采购金额',
									            type: 'pie',
									            radius : '60%',
									            center: ['50%', '50%'],
									            data:[
									                ${data}
									            ],
									            itemStyle: {
									                emphasis: {
									                    shadowBlur: 10,
									                    shadowOffsetX: 0,
									                    shadowColor: 'rgba(0, 0, 0, 0.5)'
									                }
									            }
									        }
									    ]
									};
							
							        // 为echarts对象加载数据 
							        myChart.setOption(option1); 
							    </script>
							    </c:if>	
						   </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="<%=path%>/common/script/index.js" type="text/javascript"></script>
	</body>
</html>