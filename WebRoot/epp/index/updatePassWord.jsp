<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>修改密码</title>
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
    <script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<script type="text/javascript">
	   var api = frameElement.api, W = api.opener;	   
	    <c:if test="${message!=null}">
			  showMsg("alert","${message}");
	    </c:if>
	</script>
  </head>
  
 <body style="text-align: center;" >
    <form  class="defaultForm" method="post" action="updatePassWord_IndexEpp.action" style="margin:15px;margin-top:30px;" id="updatePassWord">
    	<table align="center" class="table_ys1"> 
    		<tr>
    			<td width="25%" class="Content_tab_style1" align="right">原始密码：</td>
    			<td width="75%" class="Content_tab_style2">
    				<input datatype="*" nullmsg="请输入原始密码！" type="password" id="PWD"  name="oldPassWord" style="height:30px;">&nbsp;<font color="red">*</font>
    				<div class="info"><span class="Validform_checktip">请输入原始密码！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    		</tr>
    		<tr>
    			<td width="25%"  class="Content_tab_style1" align="right">新密码：</td>
    			<td width="75%" class="Content_tab_style2">
    				<input type="password" id="newPWD" name="newPassWord" datatype="*6-15" nullmsg="请输入新密码！" errormsg="密码范围在6-15位之间！" style="height:30px;">&nbsp;<font color="red">*</font>
    				<div class="info"><span class="Validform_checktip">密码范围在6-15位之间！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    		</tr>
    		<tr>
    			<td width="25%" class="Content_tab_style1" align="right">确认密码：</td>
    			<td width="75%" class="Content_tab_style2">
    				<input type="password" id="confirmPWD" name="userPassword" datatype="*" recheck="newPassWord" nullmsg="请输入确认密码！"  errormsg="您两次输入的密码不一致!" style="height:30px;">&nbsp;<font color="red">*</font>
    				<div class="info"><span class="Validform_checktip">您两次输入的密码不一致!</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
    		</tr>
    	</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
		<button class="btn btn-danger" type="button"   onclick="javascript:api.close();" ><i class="icon-white icon-remove-sign"></i>关闭</button>
			
		</div>
    </form>
    <script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
  </body>
</html>
