<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@ page import="com.ced.sip.common.UserRightInfoUtil" %>
<%@ include file="/common/context.jsp"%>
<%
  SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L); 
 %>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
        <title><%=systemConfiguration.getSystemCurrentDept() %>电子采购信息管理平台</title>
        <%-- 页面清缓存BGN --%>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		
		<%-- 页面清缓存 --%>
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="0" />
			
		
        <link rel="stylesheet" href="<%=basePath %>/style/default.css" />
		
		<!-- basic styles -->
		<link href="<%=basePath %>/common/ace/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=basePath %>/common/font-awesome-4.7.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<!--  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />  -->

		<!-- ace styles -->
		<link rel="stylesheet" href="<%=basePath %>/common/ace/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=basePath %>/common/ace/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="<%=basePath %>/common/ace/assets/css/ace-skins.min.css" />
		 
		
		<!-- ace settings handler -->

		<script src="<%=basePath %>/common/ace/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="<%=basePath %>/common/ace/assets/js/html5shiv.js"></script>
		<script src="<%=basePath %>/common/ace/assets/js/respond.js"></script>
		<![endif]-->
		<style type="text/css">
		.lefttop{background:url(<%=basePath %>/images/lefttop.gif) repeat-x;height:40px;line-height:40px;}
		.lefttop strong{color:#fff;font-size:14px;margin-left: 10px;float:left;}
		.lefttop span{margin-left:8px; margin-top:10px;margin-right:8px; background:url(<%=basePath %>/images/leftico.png) no-repeat; width:20px; height:21px;float:left;}
		
		</style>
	</head>

	<body>
		<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
							<img src="<%=basePath %>/images/logo.png" />
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
							<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<%=basePath %>/common/ace/assets/avatars/avatar2.png" alt="Jason's Photo" />
								<span class="user-info">
									<small><%=UserRightInfoUtil.getExpertName( request) %></small>
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="javascript:createdetailwindow_choose('修改密码','updateExpertPwdInit_evaluate.action',4)" style="cursor:pointer;">
										<i class="ace-icon fa fa-user"></i>
										 修改密码
									</a>
								</li>
								<li>
									<a href="javascript:clearLocalstorage()">
										<i class="ace-icon fa fa-cog"></i>
										清除浏览记录
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="javascript:logout()">
										<i class="ace-icon fa fa-power-off"></i>
										 注销
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>

		<div class="main-container" id="main-container">
			
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					</script>

					<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="lefttop"> <span></span><strong>功能菜单</strong></div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
						<li class="active">
							<a  href="javascript:addTabs({id:'home',title:'专家信息',close: false,url: '<%=basePath %>/viewExpertDetail_evaluate.action'});">
								<i class="menu-icon fa fa-home"></i>
								<span class="menu-text"> 专家信息 </span>
							</a>
						</li>
						
						<li>
							<a  href="javascript:addTabs({id:'id1',title:'正在评标的项目',close: false,url: '<%=basePath %>/viewIsTenderBidsJudger_evaluate.action'});">
								<i class="menu-icon fa  fa-building"></i>
								<span class="menu-text"> 正在评标的项目 </span>
							</a>
						</li>
						
						<li>
							<a  href="javascript:addTabs({id:'id2',title:'历史评标的项目',close: false,url: '<%=basePath %>/viewHistoryTenderBidsJudger_evaluate.action'});">
								<i class="menu-icon fa fa-list-alt"></i>
								<span class="menu-text"> 历史评标的项目 </span>
							</a>
						</li>
						
					</ul>

					<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
						 <i class="ace-icon fa fa-arrow-left" data-icon1="ace-icon fa fa-arrow-left" data-icon2="ace-icon fa fa-arrow-right" style="color:#000;"></i> 
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>

				<div class="main-content">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12" style="width: 100%;padding-left:2px;padding-right: 2px;" id="tabs">
	                            <ul class="nav nav-tabs" role="tablist">
	                                <!-- <li class="active"><a href="#Index" role="tab" data-toggle="tab">首页</a></li> -->
	                            </ul>
	                            <div class="tab-content">
	                                <div role="tabpanel" class="tab-pane active" id="Index">
	                                </div>
	                            </div>
	                        </div>
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="ace-icon fa fa-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; 选择皮肤</span>
						</div>

						<div>
							<input type="checkbox"  id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"> 固定导航条</label>
						</div>

						<div>
							<input type="checkbox"  id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								切换窄屏
								<b></b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><script src='<%=basePath %>/common/jQuery/jquery-1.8.3.min.js'></script>
		<script src="<%=basePath %>/common/jQuery/storage/jquery.storageapi.min.js"></script>		
		<script src="<%=basePath %>/common/ace/assets/js/bootstrap.min.js"></script>
		<script src="<%=basePath %>/common/ace/assets/js/ace-elements.min.js"></script>
		<script src="<%=basePath %>/common/ace/assets/js/ace.min.js"></script>
		<script type="text/javascript" src="<%=basePath %>/common/ace/assets/js/bootstrap-tab.js"></script>
	    <script src="<%=basePath%>/common/lhgDialog/lhgdialog.js?skin=bootstrap2" type="text/javascript"></script>
        <script src="<%=basePath%>/common/script/common.js" type="text/javascript"></script>     
		<!-- inline scripts related to this page -->
		
		<script type="text/javascript">
		jQuery(function($) {
		
	    	var height=$(window).height();
	    	height=height>768?height:768;
	    	$('#sidebar').height(height-90);
	    	
			//$( "#tabs" ).tabs();
			addTabs({id:'home',title:'专家信息',close: false,url: '<%=basePath %>/viewExpertDetail_evaluate.action'});
	    	
	    	});
	  	function logout(){
	  		$.dialog.confirm("温馨提示：您确定要注销系统么？", function() {
	  			location.href="<%=basePath %>/exit_verify.action";
	  		});
  		}
		function clearLocalstorage(){
				var storage=$.localStorage;
				if(!storage)
					storage=$.cookieStorage;
				storage.removeAll();
				showMsg("alert", "浏览器缓存清除成功!");
			}

	</script>
</body>
</html>

