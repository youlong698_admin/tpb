<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>新增资金计划</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
 <!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<script src="<%= path %>/common/script/required/requirePlan.js" type="text/javascript" ></script>
<script language="javaScript">
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="CapitalPlan"; //当前是哪个类别功能上传的附件
var path="<%= path %>" 
var uuIdData=[];//已上传的文件的文件uuid，上传后的文件以uuId命名
var fileNameData=[];//已上传的文件名
var fileTypeData=[];//已上传的文件的格式
var attIdData=[];//已存入附件表的附件信息

	function doClick(type){
	    if(type==1){
	      $("#selectProjectImg").show();
	      $("#selectSupplierImg").hide();
	      $("#projectContractName").attr("readonly","readonly");
	      $("#bidCode").attr("readonly","readonly");
	      $("#td1").text("项目名称：");
	      $("#td2").text("项目名称");
	      $("#td3").text("项目编号：");
	    }else if(type==2){	  
	      $("#selectProjectImg").show();
	      $("#selectSupplierImg").hide();
	      $("#projectContractName").attr("readonly","readonly");
	      $("#bidCode").attr("readonly","readonly");
	      $("#td1").text("合同名称：");
	      $("#td2").text("合同名称");
	      $("#td3").text("合同编号：");
	    }else if(type==3){    
	      $("#selectProjectImg").hide();
	      $("#selectSupplierImg").show();
	      $("#projectContractName").removeAttr("readonly");
	      $("#bidCode").removeAttr("readonly");
	      $("#td1").text("项目名称：");
	      $("#td2").text("项目名称");
	      $("#td3").text("项目编号：");
	    }
	 }
	 function selectProject(){
	   var type=$("input[name='capitalPlan.type']:checked").val() 
	   if(type==1){
	    createdetailwindow('选择项目','viewRequiredCollectedForContractAndOrder_purchaseBase.action?sign=3',3);
	   }else{
	    createdetailwindow('选择合同','viewContractForOrder_contractInfo.action?sign=3',3);
	   }
	 }
	</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="saveCapitalPlan_capitalPlan.action" >
<input type="hidden" name="capitalPlan.attIds" id="attIds" />
<input type="hidden" name="capitalPlan.fileNameData" id="fileNameData" value=""/>
<input type="hidden" name="capitalPlan.uuIdData" id="uuIdData" value=""/>
<input type="hidden" name="capitalPlan.fileTypeData" id="fileTypeData" value=""/>
<input type="hidden" name="capitalPlan.attIdData" id="attIdData" value=""/>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
            <tr>
    	        <td colspan="4" style="color: red" align="left">*资金计划除来自项目或者合同(为框架协议)生成外，还可新建新的资金计划</td>
    	    </tr> 
    	    <tr>
    	         <td width="15%" class="Content_tab_style1">资金计划来源：</td>
    	          <td width="35%" class="Content_tab_style2" colspan="3">
    	             <label class="radio_label">
				        <input class="radio_radio" type="radio" name="capitalPlan.type" id="type" <c:if test="${type==1||type==0 }">checked <c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:if> value="1" onclick="doClick(1)">
				        <span class="radio_radioInput"></span>从采购项目获取
				    </label>
				     <label class="radio_label">
				        <input class="radio_radio" type="radio" name="capitalPlan.type" id="type" <c:if test="${type==2 }">checked <c:set var="td1" value="合同名称"/> <c:set var="td2" value="合同编号"/></c:if> value="2" onclick="doClick(2)">
				        <span class="radio_radioInput"></span>从合同(框架协议)获取
				    </label>
				     <label class="radio_label">
				        <input class="radio_radio" type="radio" name="capitalPlan.type" id="type" <c:if test="${type==3 }">checked <c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:if> value="3" onclick="doClick(3)">
				        <span class="radio_radioInput"></span>新建资金计划
				    </label>
    	          </td>
    	    </tr>
           <tr>
			    <td width="15%" class="Content_tab_style1" id="td1">${td1 }：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input  datatype="*" nullmsg="${td1 }不能为空！" name="capitalPlan.projectContractName" readonly type="text" id="projectContractName" value="${capitalPlan.projectContractName }">
			       <img src="images/select.gif" id="selectProjectImg" onclick="selectProject()"/><font color="#ff0000">*</font>			      
			       <div class="info"><span class="Validform_checktip" id="td2">${td1 }不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			       <input  name="capitalPlan.baCiId" type="hidden" id="baCiId" value="${capitalPlan.baCiId }"></td>
			    <td width="15%" class="Content_tab_style1" id="td3">${td2 }：</td>
			    <td width="35%" class="Content_tab_style2">
			       <input   type="text" name="capitalPlan.bidCode" id="bidCode" value="${capitalPlan.bidCode }" readonly><font color="#ff0000">*</font>
			    </td>
			</tr>
            <tr>
                <td class="Content_tab_style1">申请单位：</td>
			    <td class="Content_tab_style2">
			        <select name="capitalPlan.applicantDeptId">
			          <c:choose>
			             <c:when test="${type==0||type==3}">
			                <c:forEach var="dept" items="${applicantDeptList}">
				              <option value="${dept.deptId }">${dept.deptName }</option>
				            </c:forEach>
			             </c:when>
			             <c:otherwise>
			                <c:forEach var="dept" items="${applicantDeptList}">
				              <option value="${dept.depId }">${dept.deptName }</option>
				            </c:forEach>
			             </c:otherwise>
			          </c:choose>
			           
			        </select>
			    </td>
			    <td class="Content_tab_style1">申请付款金额：</td>
			    <td class="Content_tab_style2">
			       <input  datatype="n |/^(0|[1-9][0-9]*)+(.[0-9]{1,2})?$/" nullmsg="申请付款金额不能为空！" errormsg="申请付款金额必须是数字" name="capitalPlan.thisPaymentAmount" type="text" id="thisPaymentAmount" value="<fmt:formatNumber value="${capitalPlan.thisPaymentAmount }" pattern="#00.00#"/>"><font color="#ff0000">*</font>
			       <div class="info"><span class="Validform_checktip">申请付款金额不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    
			</tr>
            <tr>
			    <td class="Content_tab_style1">供应商名称：</td>
			    <td class="Content_tab_style2">
				    <input  datatype="*" nullmsg="供应商名称不能为空！" name="capitalPlan.supplierName" readonly type="text" id="supplierName" value="${capitalPlan.supplierName }">
				    <input  name="capitalPlan.supplierId" type="hidden" id="supplierId" value="${capitalPlan.supplierId }">
				    <c:if test="${empty capitalPlan.baCiId}"><img id="selectSupplierImg" style="display:none" src="images/select.gif" onclick="createdetailwindow('选择供应商','viewSupplierBaseInfoCompany_oneSupplierSelect.action',3);" title="选择供应商"/></c:if><font color="#ff0000">*</font>			      
			        <div class="info"><span class="Validform_checktip">供应商名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				 </td>
				 <td class="Content_tab_style1">联系人：</td>
			     <td class="Content_tab_style2"><input  name="capitalPlan.supplierPersonB" type="text" id="supplierPersonB" value="${capitalPlan.supplierPersonB }"><font color="#ff0000">*</font></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">联系人手机：</td>
			    <td class="Content_tab_style2"><input  name="capitalPlan.supplierMobileB" type="text" id="supplierMobileB" value="${capitalPlan.supplierMobileB }"><font color="#ff0000">*</font></td>
			    <td class="Content_tab_style1">银行账号：</td>
			    <td class="Content_tab_style2"><input  name="capitalPlan.bankAccount" type="text" id="bankAccount" value="${capitalPlan.bankAccount }"></td>
			</tr>
            <tr>
			     <td class="Content_tab_style1">银行户名：</td>
			    <td class="Content_tab_style2"><input   name="capitalPlan.bankAccountName" type="text" id="bankAccountName" value="${capitalPlan.bankAccountName }"></td>
			    <td class="Content_tab_style1">开户银行：</td>
			    <td class="Content_tab_style2"><input   name="capitalPlan.bank" type="text" id="bank" value="${capitalPlan.bank }"></td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">税号：</td>
			    <td class="Content_tab_style2">
			    <input  name="capitalPlan.dutyParagraph" type="text" id="dutyParagraph" value="${capitalPlan.dutyParagraph }">
			    </td>
			    <td class="Content_tab_style1"></td>
			    <td class="Content_tab_style2">
			    </td>
			</tr>
			<tr>
			     <td class="Content_tab_style1">申请事由：</td>
			    <td class="Content_tab_style2" colspan="3">
			       <textarea rows="capitalPlan.applicationReson" cols="" datatype="s0-1000 |/^\s*$/ " errormsg="申请事由限500个字！" style="width: 90%" rows="5" class="Content_input_style2" ></textarea>
					<div class="info"><span class="Validform_checktip">申请事由限500个字！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>    
			<tr>
				<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
				<td class="Content_tab_style2" colspan="3">
					<!-- 附件存放 -->
					<div  id="fileDiv" class="panel"> 
					</div>
					<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
					<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					
				</td>
			</tr>        
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-info" id="btn-view" type="button" onclick="doView()"><i class="icon-white icon-th-list"></i>查看资金计划列表</button>
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			 //提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);	
		}
	});
})
</script>
</body>
</html> 	