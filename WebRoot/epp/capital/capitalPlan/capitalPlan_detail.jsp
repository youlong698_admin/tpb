<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看资金计划</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script language="javascript" src="<%= path %>/common/Lodop6.2/LodopFuncs.js"></script>
<object  id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> 
<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0></embed>
</object>
<style type="text/css" id="printStyle">
.table_ys1 {
	border-collapse: collapse;
	border: none;
	margin-top: 5px;
	width: 100%;
}
.table_ys1 tr {	
	height: 30px;
}
.table_ys1 td {
	border: 1px solid #ccc;
	font-family: "宋体";
	font-size: 12px;
}
.Content_tab_style1 {
	background: #f2f2f2;
	height: 28px;
	font-size: 12px;
	font-weight:bold;
	color: #000;
	line-height: 28px;
	nowrap: nowrap;
	text-align: right;
	min-width: 15%;
}

.Content_tab_style2 {
	background-color: #fff;
	height: 28px;
	font-size: 12px;
	color: #000;
	line-height: 28px;
	text-align: left;
}

.Content_tab_style_04 {
	height: 30px;
	background: #f2f2f2;
	font-size: 13px;
	font-weight: bold;
	line-height: 30px;
	padding-top: 5px;
	padding-bottom: 5px;
	text-align: center;
}

.Content_tab_style_04 th {
	text-align: center;
	border: 1px solid #ccc;
}
.biaoge_01_a{background-color:#f8f8f8;}
#biaoge_01_a a{ color:#2925da}
.biaoge_01_b{background-color:#ffffff;}
#biaoge_01_b a{ color:#000;}
</style>
<script type="text/javascript">
   var api = frameElement.api, W = api.opener;
   function print(){
     var LODOP = getLodop();
     var strBodyStyle="<style>"+document.getElementById("printStyle").innerHTML+"</style>";
	 var strFormHtml=strBodyStyle+"<body>"+$(".Conter_main_conter").html()+"</body>";
	 LODOP.PRINT_INIT("资金计划审批单");
     LODOP.SET_PRINT_PAGESIZE(1, 0, 0, "A4");//定义纸张，方向
     LODOP.ADD_PRINT_HTM("5mm",34,"RightMargin:0.9cm","BottomMargin:9mm", strFormHtml);//定义打印的内容与位置
     LODOP.PREVIEW();   
   }
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="" >
<c:set var="td1" value=""/>
<c:set var="td2" value=""/>
<c:choose>
<c:when test="${capitalPlan.type==1||capitalPlan.type==3 }"><c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:when>
<c:otherwise><c:set var="td1" value="合同名称"/> <c:set var="td2" value="合同编号"/></c:otherwise>
</c:choose>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
        <div style="float:left;font-size: 20px;font-weight: 700;width: 100%" align="center"><img src="/fileWeb/${sysCompany.logo}" width="220" height="80" style="vertical-align:middle;"/>${sysCompany.compName }资金计划审批单</div> 
       <input type="hidden" id="cpId" value="${capitalPlan.cpId}"/>
    	<!-- 基本信息  begin-->
    	<table class="table_ys1" style="margin-top: 5px">
            <tr>
			    <td class="Content_tab_style1">申请单编号：</td>
			    <td class="Content_tab_style2" colspan="3">${capitalPlan.payCode}</td>
			</tr>
            <tr>
			    <td width="20%" class="Content_tab_style1">${td1 }：</td>
			    <td width="30%" class="Content_tab_style2">${capitalPlan.projectContractName}</td>
			    <td width="20%" class="Content_tab_style1">${td2 }：</td>
			    <td width="30%" class="Content_tab_style2">${capitalPlan.bidCode}</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">申请单位：</td>
			    <td class="Content_tab_style2">${capitalPlan.applicantDeptName}</td>
			    <td class="Content_tab_style1">申请付款金额：</td>
			    <td class="Content_tab_style2"><fmt:formatNumber value="${capitalPlan.thisPaymentAmount}" pattern="#,##0.00"/></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">供应商名称：</td>
			    <td class="Content_tab_style2">${capitalPlan.supplierName}</td>
			   <td class="Content_tab_style1">联系人：</td>
			    <td class="Content_tab_style2">${capitalPlan.supplierPersonB}</td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">联系人手机：</td>
			    <td class="Content_tab_style2">${capitalPlan.supplierMobileB}</td>
			    <td class="Content_tab_style1">银行账号：</td>
			    <td class="Content_tab_style2">${capitalPlan.bankAccount}</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">银行户名：</td>
			    <td class="Content_tab_style2">${capitalPlan.bankAccountName}</td>
			    <td  class="Content_tab_style1">开户银行：</td>
			    <td class="Content_tab_style2">${capitalPlan.bank}</td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">税号：</td>
			    <td class="Content_tab_style2">${capitalPlan.dutyParagraph}</td>
			    <td  class="Content_tab_style1"></td>
			    <td class="Content_tab_style2"></td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">编制人：</td>
			    <td class="Content_tab_style2">${capitalPlan.writerCN}</td>
			    <td class="Content_tab_style1">编制日期：</td>
			    <td class="Content_tab_style2"><fmt:formatDate value="${capitalPlan.writeDate}" pattern="yyyy-MM-dd" /></td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">申请事由：</td>
			    <td class="Content_tab_style2" colspan="3">${capitalPlan.applicantReson}</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">附件列表：</td>
				<td class="Content_tab_style2" colspan="3">
					<c:out value="${capitalPlan.attachmentUrl}" escapeXml="false"/>
				</td>
			</tr>
		</table>
		<table  align="center" border="0" cellpadding="0" class="table_ys1"
			cellspacing="0" style="margin-top: 0px" width="98%">
			    <tr class="Content_tab_style_04">
	  				<th width="5%" nowrap>序号</th>
	  				<th width="10%" nowrap>审批环节</th>
					<th width="10%" nowrap>审批人</th>
					<th width="15%" nowrap>审批时间</th>
					<th width="10%" nowrap>审批结果 </th>
					<th width="25%" nowrap>审批意见</th>
	       	    </tr>
	       	    <c:forEach items="${hytList}" var="hy" varStatus="status">
				<tr <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>
					<c:out escapeXml="false" value="${hy.displayName}" />&nbsp;
				</td>
				<td>
					${hy.operatorCn}
				</td>
				<td>
					${hy.finishTime}
				</td>
				<td>
					<c:choose><c:when test="${hy.performType=='1'}">通过</c:when><c:when test="${hy.performType=='-1'}">不通过</c:when><c:otherwise></c:otherwise></c:choose>&nbsp;&nbsp;
				</td>
				<td align="left">${hy.variable}</td>
			  </tr>
			  </c:forEach>
		</table>
</div>
</div>

<c:if test="${capitalPlan.runStatus=='0'}">		
<div class="buttonDiv">
	<button class="btn" id="btn-print" type="button" onclick="print()"><i class="icon icon-print"></i>打印</button> 
</div>
</c:if>
</form>
</body>
</html> 	