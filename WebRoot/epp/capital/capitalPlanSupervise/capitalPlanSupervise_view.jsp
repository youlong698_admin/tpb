<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>资金计划监督</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
var capitalPlanWorkflow="${contractWorkflow}";
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: "findCapitalPlanSupervise_capitalPlan.action",
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			showMsg("error","查询失败。错误码："+result.errorCode);
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
           {
            	className : "ellipsis",
            	data: "payCode",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "10%"
             }
             ,{
            	className : "ellipsis",
            	data: "applicantDeptName",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "10%"
             }
            ,{
            	className : "ellipsis",
            	data: "projectContractName",            	
            	render: viewCapitalPlanDetail,
            	width : "18%",
				orderable : false
             }
            ,{
            	className : "numPrice",
            	data: "thisPaymentAmount",
            	render: CONSTANT.DATA_TABLES.RENDER.NUMBER_PRICE,
            	width : "10%"
             }
            ,{
            	className : "ellipsis",
            	data: "supplierName",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "15%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "supplierPersonB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "9%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "supplierMobileB",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "9%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "writerCN",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%",
				orderable : false
             }
            ,{
            	className : "ellipsis",
            	data: "writeDate",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%"
             }
            ,{
            	className : "ellipsis",
            	data: "statusCn",
            	render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
            	width : "8%"
             }
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
	
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.cpId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
            case 0:
				param.orderColumn = "de.payCode";
				break;
            case 2:
				param.orderColumn = "de.thisPaymentAmount";
				break;
            case 7:
				param.orderColumn = "de.writeDate";
				break;
            case 8:
				param.orderColumn = "de.statusCn";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		
		//组装查询参数  ***按需修改****
		param.payCode = $("#payCode").val();
		param.projectContractName = $("#projectContractName").val();
		param.supplierName = $("#supplierName").val();
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	}	  				 
		
   };
   
    //查看
    function viewCapitalPlanDetail(data, type, row,   meta){
       return "<a href='#' onClick='createdetailwindow(\"查看资金计划明细\",\"viewCapitalPlanDetail_capitalPlan.action?capitalPlan.cpId=" + row.cpId + "\",1)' title='"+data+"'>"+data+"</a>"; 
              
    }
    
    function doQuery(){
     _table.draw();
    }
    
    //查看流程审批信息
	
      function viewWorkflowProcess(data, type, row,   meta ){
        if(capitalPlanWorkflow=="0"){
    	  if(row.orderState != ""){
	    		  var action="display_workflow.action?param=view&processId="+row.processId+"&orderId=";
	    		  return "<a href='#' onclick='openWorkFlowWindow(\""+action+"\","+row.cpId+",\""+row.processId+"\")'>"+data+"</a>";
    	  }else{
    		  return "起草状态";
    	  }
    	  }else{
    	    if(row.orderState=="01")  return "起草状态";
    	    else  if(row.orderState=="0")  return "已完成";
    	  }
    	  
      }
      
	//弹出一个新窗口
	function openWorkFlowWindow(action, cpId, processId){
	    ciId="${work_flow_type}"+cpId;
		$.ajax({
			url:"<%=path%>/findWfOrder_workflow.action",
			type:"POST",
			dataType: 'json',
			async:false, 
			data:{orderNo:ciId, processId:processId},
			success:function(msg){
				var rsb = msg;
				var rs = rsb.order;
				if(rs.length > 0){
					createdetailwindow("查看流程",action+rs[0].id,0);
				}else{
					return false;
				}
			},
			error:function(){
			}
		});
		
	}

</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid" id="div-advanced-search">
					<form class="form-inline well">
					<input type="hidden" id="returnValues" name="returnValues"/>
					  <!--***按需修改****-->
						申请单编号：
						<input class="input-medium" placeholder="申请单编号" type="text" id="payCode" name="payCode" value="" />
						项目名称：
						<input class="input-medium" placeholder="项目名称" type="text" id="projectContractName" name="projectContractName" value="" />
						供应商名称：
						<input class="input-medium" placeholder="供应商名称" type="text" id="supplierName" name="supplierName" value="" />
										 
								  <button type="button" class="btn btn-info" id="btn-advanced-search"  ><i class="icon-white icon-search"></i> 查询</button>
							      <button type="button" class="btn btn-cacel" id="btn-advanced-cacel" onclick="doReset();"><i class="icon-info-sign"></i> 重置</button>
							
					</form>
				</div>
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>						
						            <th>申请单编号</th>
						            <th>申请单位</th>						
						            <th>项目名称</th>
						            <th>申请金额</th>
						            <th>供应商名称</th>
						            <th>联系人</th>
						            <th>手机</th>
						            <th>编制人</th>
						            <th>编制日期</th>
						            <th>状态</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>