<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>查看接口日志明细</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		
	   
	});
	
	</script>
</head>
<body >
<form id="bidItemUpdate" name="" method="post" action="" >

<div class="Conter_Container" >
	
    <!-- 当前位置区域  end-->
	 <div class="Conter_main_conter" style="width:90%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">接口日志明细</td>
		    </tr>
		    <tr>
		  	<td class="Content_tab_style1">系统名称</td>
			<td class="Content_tab_style2">
				${interfaceLog.systemName }
			</td>
		  </tr>
		     <tr>
		    <td width="30%" align="center" class="Content_tab_style1">接口名称</td>
		    <td width="70%" class="Content_tab_style2">${interfaceLog.interfaceName }</td>
		  </tr>
		  <tr>
		    <td align="center" class="Content_tab_style1">发送消息</td>
		    <td width="70%" class="Content_tab_style2">${interfaceLog.sendContent }</td>
		  </tr>
		  <tr>
		    <td align="center" class="Content_tab_style1">回送消息</td>
		   
		  	<td width="70%" class="Content_tab_style2">${interfaceLog.receiveContent }</td>
		  </tr>
		  <tr> 
		    <td align="center" class="Content_tab_style1">接口调用时间</td>
		    <td width="70%" class="Content_tab_style2">${interfaceLog.writeDate }</td>
		  </tr>
		  <tr> 
		    <td align="center" class="Content_tab_style1">调用人</td>
		    <td width="70%" class="Content_tab_style2">${interfaceLog.writer }</td>
		  </tr>
		  
		</table>
		
</div>
</div>
</form>
</body>
</html> 	