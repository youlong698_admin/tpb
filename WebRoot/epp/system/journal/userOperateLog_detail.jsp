<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>查看用户操作日志明细</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		
	    
	});
	
	</script>
</head>
<body >
<form id="bidItemUpdate" name="" method="post" action="" >

<div class="Conter_Container" >
	
    <!-- 当前位置区域  end-->
	 <div class="Conter_main_conter" style="width:90%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
            <tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">用户操作日志明细</td>
		   </tr>
		   <tr>
		  	<td width="30%" class="Content_tab_style1">action</td>
			<td width="70%" class="Content_tab_style2">${userOperateLog.action }</td>
		  </tr>
		     <tr>
		    <td width="30%" align="center" class="Content_tab_style1">方法名</td>
		    <td width="70%" class="Content_tab_style2">${userOperateLog.method }</td>
		  </tr>
		  <tr>
		    <td width="30%" align="center" class="Content_tab_style1">参数</td>
		    <td width="70%" class="Content_tab_style2 wrapEnglish">${userOperateLog.param }</td>
		  </tr>
		  <tr>
		    <td width="30%" align="center" class="Content_tab_style1">执行结果</td>
		   
		  	<td width="70%" class="Content_tab_style2">${userOperateLog.result }</td>
		  </tr>
		  <tr> 
		    <td width="30%" align="center" class="Content_tab_style1">操作时间</td>
		    <td width="70%" class="Content_tab_style2">${userOperateLog.operDate }</td>
		  </tr>
		  <tr> 
		    <td width="30%" align="center" class="Content_tab_style1">IP地址</td>
		    <td width="70%" class="Content_tab_style2">${userOperateLog.ipAddress }</td>
		  </tr>
		  <tr> 
		    <td width="30%" align="center" class="Content_tab_style1">登录名</td>
		    <td width="70%" class="Content_tab_style2">${userOperateLog.writerEn }</td>
		  </tr>
		  <tr> 
		    <td width="30%" align="center" class="Content_tab_style1">用户名</td>
		    <td width="70%" class="Content_tab_style2">${userOperateLog.writer }</td>
		  </tr>
		  
		</table>
		
</div>
</div>
</form>
</body>
</html> 	