<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>查看错误日志明细</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		
	});
	
	</script>
</head>
<body >
<form id="bidItemUpdate" name="" method="post" action="" >

<div class="Conter_Container" >
	
    <!-- 当前位置区域  end-->
	 <div class="Conter_main_conter" style="width:90%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
            <tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">日志日志明细</td>
		   </tr>
		   <tr>
		  	<td class="Content_tab_style1">错误CODE</td>
			<td class="Content_tab_style2">${errorLog.hashcode }</td>
		  </tr>
		     <tr>
		    <td width="30%" align="center" class="Content_tab_style1">用户名</td>
		    <td width="70%" class="Content_tab_style2">${errorLog.writerEn }</td>
		  </tr>
		  <tr>
		    <td align="center" class="Content_tab_style1">用户姓名</td>
		    <td width="70%" class="Content_tab_style2">${errorLog.writer }</td>
		  </tr>
		  <tr>
		    <td align="center" class="Content_tab_style1">IP地址</td>
		   
		  	<td width="70%" class="Content_tab_style2">${errorLog.ipAddress }</td>
		  </tr>
		  <tr> 
		    <td align="center" class="Content_tab_style1">错误URL</td>
		    <td width="70%" class="Content_tab_style2">${errorLog.errorUrl }</td>
		  </tr>
		  <tr> 
		    <td align="center" class="Content_tab_style1">错误日期</td>
		    <td width="70%" class="Content_tab_style2">${errorLog.errorDate }</td>
		  </tr>
		  <tr> 
		    <td align="center" class="Content_tab_style1">错误信息</td>
		    <td width="70%" class="Content_tab_style2">${errorLog.errorInfo }</td>
		  </tr>
		  
		</table>
		
</div>
</div>
</form>
</body>
</html> 	