<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>新增资质文件类别</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		    showMsg('success','${message}');
		    W.doQuery();
		    api.close();
		  	}
		</c:if>
	    
	});
	
	</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="updateQualityCategory_qualityCategory.action" >
<input name="qualityCategory.qcId" type="hidden" id="qcId" value="${qualityCategory.qcId}">
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">修改资质文件类别</td>
		    </tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">编码</td>
			    <td width="70%" class="Content_tab_style2"><input  datatype="*" nullmsg="编码不能为空！" name="qualityCategory.code" type="text" id="code" value="${qualityCategory.code}"><font color="#ff0000">*</font></td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">资质名称</td>
			    <td width="70%" class="Content_tab_style2"><input  datatype="*" nullmsg="资质名称不能为空！" name="qualityCategory.qualityName" type="text" id="qualityName" value="${qualityCategory.qualityName}"><font color="#ff0000">*</font></td>
			</tr>
			 <tr>
			    <td width="30%" align="center" class="Content_tab_style1">是否有效</td>
			    <td width="70%" class="Content_tab_style2">
			     <input type="radio" name="qualityCategory.isUsable" id="isUsable" value="0" <c:if test="${qualityCategory.isUsable=='0' }">checked</c:if>/>有效&nbsp;&nbsp;
				 <input type="radio" name="qualityCategory.isUsable" id="isUsable" value="1" <c:if test="${qualityCategory.isUsable=='1' }">checked</c:if>/>无效<br />
			    </td>
			</tr>
			 <tr>
			    <td width="30%" align="center" class="Content_tab_style1">备注</td>
			    <td width="70%" class="Content_tab_style2"><textarea   name="qualityCategory.remark" id="remark" cols="28" class="Content_input_style2">${qualityCategory.remark}</textarea></td>
			</tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	