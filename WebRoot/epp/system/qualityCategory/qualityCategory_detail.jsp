<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>查看资质文件类别</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<script type="text/javascript">
   var api = frameElement.api, W = api.opener;	
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="" >

<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">资质文件类别明细</td>
		    </tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">编码</td>
			    <td width="70%" class="Content_tab_style2">${qualityCategory.code}</td>
			</tr>
            <tr>
			    <td height="24" width="30%" align="center" class="Content_tab_style1">资质名称</td>
			    <td width="70%" class="Content_tab_style2">${qualityCategory.qualityName}</td>
			</tr>
			 <tr>
			    <td width="30%" align="center" class="Content_tab_style1">是否有效</td>
			    <td width="70%" class="Content_tab_style2">
			     <c:if test="${qualityCategory.isUsable=='0' }">有效</c:if>
			     <c:if test="${qualityCategory.isUsable=='1' }">无效</c:if>
			    </td>
			</tr>
			 <tr>
			    <td width="30%" align="center" class="Content_tab_style1">备注</td>
			    <td width="70%" class="Content_tab_style2">${qualityCategory.remark}</td>
			</tr>
		</table>
</div>
</div>
</form>
</body>
</html> 	