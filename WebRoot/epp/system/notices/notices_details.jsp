<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<title>公告信息查看</title>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
	
	    
	});
	
	</script>
</head>

<body >
<form id="ff" method="post" action="saveNotices_notices.action">
<div class="Conter_Container" >
    <div class="Conter_main_conter"  >
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">公告信息</td>
		    </tr>
			<tr>
				<td width="20%" height="100" align="center" class="Content_tab_style1">详细内容：</td>
				<td width="80%" class="Content_tab_style2" style="height:100px">
				     <c:out value="${notices.content}" escapeXml="false"/>
				</td> 
			</tr>
			<tr>
				<td width="20%" height="24" align="center" class="Content_tab_style1">附&nbsp;&nbsp;&nbsp;&nbsp;件：</td>
				<td width="80%" class="Content_tab_style2">
							<c:out value="${notices.attachmentUrl}" escapeXml="false"/>
	            </td> 
			</tr>
			<tr>
				<td width="20%" height="24" align="center" class="Content_tab_style1">关键字：</td>
				<td width="80%" class="Content_tab_style2">${notices.keyword }&nbsp;</td> 
			</tr>
		    <tr>
				<td width="20%" height="24" align="center" class="Content_tab_style1">备&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
				<td width="80%" class="Content_tab_style2">${notices.remark}&nbsp;</td> 
			</tr>
			<tr>
				<td width="20%" height="24" align="center" class="Content_tab_style1">发布者：</td>
				<td width="80%" class="Content_tab_style2">${notices.publisher}&nbsp;</td> 
			</tr>
			<tr>
				<td width="20%"  align="center" class="Content_tab_style1">发布时间：</td>
				<td width="80%" class="Content_tab_style2">
					<fmt:formatDate value="${notices.publishDate}" pattern="yyyy-MM-dd"/>
				</td> 
			</tr>
	
	</table> 
	
</div>
</div>
</form>
</body>
</html> 	