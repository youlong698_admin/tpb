<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>显示公告信息</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>

<link rel="stylesheet" href="<%=path %>/common/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="<%=path %>/common/kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="<%=path %>/common/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%=path %>/common/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="<%=path %>/common/kindeditor/plugins/code/prettify.js"></script>
<!-- 上传组件引入js -->
<link href="<%= path %>/common/swfupload/process.css" rel="stylesheet" type="text/css"/>
<script src="<%= path %>/common/swfupload/swfupload.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/swfupload.queue.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/fileprogress.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/handlers.js" type="text/javascript"></script>
<script src="<%= path %>/common/swfupload/file.js" type="text/javascript"></script>
<style>
	form {
		margin: 0;
	}
	textarea {
		display: block;
	}
</style>
</head>
<body>

<script language="javaScript">
//附件需要添加的信息
var sessionId="<%=session.getId()%>";
var attachmentType="Notices"; //当前是哪个类别功能上传的附件
var path="<%= path %>";
 var fileNameData=${notices.fileNameData};
 var uuIdData=${notices.uuIdData};
 var fileTypeData=${notices.fileTypeData};
 var attIdData=${notices.attIdData};
var editor1; 
			KindEditor.ready(function(K) {
			editor1 = K.create('textarea[name="notices.content"]', {
				cssPath : '<%=path %>/common/kindeditor/plugins/code/prettify.css',
			    uploadJson : '<%=path %>/common/kindeditor/jsp/upload_json.jsp',
			    fileManagerJson : '<%=path %>/common/kindeditor/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
				}
			});
			prettyPrint();
		});
		
		function doSave(){
		   document.getElementById("contents").value=editor1.html(); 
		   document.form1.action="updateNotices_notices.action";
		   document.form1.submit();
		}
</script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
	   window.onload=function(){ 
		    showMsg('success','${message}');
		       W.doQuery();
		       api.close();
		  	}
	    </c:if>
	    
	});
	
	</script>
<form class="defaultForm" id="ff" name="form1" method="post" action="">
<input type="hidden" name="notices.publisher"  value="${notices.publisher}" />
<input type="hidden" name="notices.publishDate" value="${notices.publishDate}" />
<input type="hidden"   name="notices.noticeId" value="${notices.noticeId}" />
<input type="hidden"   name="notices.comId" value="${notices.comId}" />

<input type="hidden" name="notices.fileNameData" id="fileNameData" value="${notices.fileNameData}"/>
<input type="hidden" name="notices.uuIdData" id="uuIdData" value="${notices.uuIdData}"/>
<input type="hidden" name="notices.fileTypeData" id="fileTypeData" value="${notices.fileTypeData}"/>
<input type="hidden" name="notices.attIdData" id="attIdData" value="${notices.attIdData}"/>

<!-- 修改页面删除附件的Id -->
<input type="hidden" name="notices.attIds" id="attIds" />
<!-- 防止表单重复提交 -->
<s:token/>

<div class="Conter_Container" >
    <div class="Conter_main_conter"  >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1" >
    			<tr>
          			<td  colspan="4" class="Content_tab_style_05">修改公告信息</td>
        		</tr>
        		<tr>
				
		            <td width="20%" height="24" align="center" class="Content_tab_style1" nowrap>标&nbsp;&nbsp;&nbsp;&nbsp;题</td>
		            <td  colspan="3" class="Content_tab_style2"><input size="32" datatype="*" nullmsg="标题不能为空！" name="notices.title" type="text" value="${notices.title}" /><font color="#ff0000">&nbsp;*</font>
		            	<div class="info"><span class="Validform_checktip">标题不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			 	 </td> 
	           </tr>
				
				<tr>
					<td width="20%" height="24" align="center" class="Content_tab_style1" nowrap>关键字</td>
					<td width="30%"  class="Content_tab_style2"><input  name="notices.keyword" type="text"   size="32"  id="name" value="${notices.keyword}" /><input  name="notices.isUsable" type="hidden"  id="name" value="${notices.isUsable}" /></td> 
					<td width="20%" height="24" align="center" class="Content_tab_style1" nowrap>备&nbsp;&nbsp;&nbsp;&nbsp;注</td>
					<td width="30%"  class="Content_tab_style2"><input  name="notices.remark" type="text"  size="40" id="name" value="${notices.remark}" /></td> 
				</tr>
				<tr>
					<td width="20%" height="24" align="center" class="Content_tab_style1"  nowrap>详细内容</td>
					<td width="80%" colspan="3" class="Content_tab_style2">
						<textarea name="notices.content" id="contents" style="width:100%;height:250px;visibility:hidden;" ><s:property value='notices.content'/></textarea></td> 
				</tr>
				<tr>
					<td  class="Content_tab_style1"><span id="spanButtonPlaceHolder" ></span></td>
					<td class="Content_tab_style2" colspan="5">
						<!-- 附件存放 -->
						<div  id="fileDiv" class="panel"> 
						
						</div>
						<input class="cancel" id="btnCancel" name="cancelImg" type="button" style="display: none" value="取消" onclick="swfu.cancelQueue();"  />
						<div id="fsUploadProgress" style="padding-left:200px;width: 200px;"></div>
					</td>
				</tr>
				<tr>
					<td width="20%" height="24" align="center" class="Content_tab_style1" nowrap>发布者</td>
					<td width="30%" class="Content_tab_style2">${notices.publisher}</td> 
					<td width="20%" height="24" align="center" class="Content_tab_style1" nowrap>发布时间</td>
					<td width="30%" class="Content_tab_style2"><fmt:formatDate value="${notices.publishDate}" pattern="yyyy-MM-dd"/>
						
					</td> 
				</tr>
				
	</table> 
	<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
		
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			
			//提交之前把选择的附件信息填充值
			$("#fileNameData").val(fileNameData);
			$("#uuIdData").val(uuIdData);
			$("#fileTypeData").val(fileTypeData);
			$("#attIdData").val(attIdData);
			doSave();
			return false;	
		}
	});
})
</script>
</body>
</html> 	