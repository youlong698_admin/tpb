<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"  src="<%= path %>/common/script/context.js" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script type="text/javascript">

	function doSubmit(type){   
		var action;
		var mjId=document.getElementById("mjId").value;
		var mjName=document.getElementById("mjName").value;
		var mjCode=document.getElementById("mjCode").value;
		if(mjName=="" || mjCode=="")
		{
			showMsg("alert","温馨提示：请完善表单信息！");
			return false ;
		}
		if(type=="save"){
			location.href="saveMajorInit_major.action?major.mjId="+mjId;
		}
		if(type=="update"){
	       $.dialog.confirm("温馨提示：确定要提交信息吗?",function(){
				  action="updateMajor_major.action";
				  document.forms[0].action=action;
					document.forms[0].submit();
			},function(){return false;});
		}
		if(type=="delete"){
			
			$.dialog.confirm("温馨提示：确定要删除选中的信息?",function(){
			    var mesg = ajaxGeneral("delMajor_major.action","major.mjId="+mjId,"text");
				 window.parent.LeftFrame.refreshParentNode() ;
		         window.parent.MainFrame.location.href = "updateMajorInit_major.action";
			},function(){return false;});
		}
		
    }
    
</script>
</head>

<body >
<form  method="post" action="" id="major_update" name="major_update"> 
<input name="major.mjId" id="mjId" type="hidden" value="${major.mjId}"/>  
<input name="major.parentId" type="hidden" value="${major.parentId}"/>
<input name="major.isHaveChild" type="hidden" value="${major.isHaveChild}"/>
<input name="major.selflevCode" type="hidden" value="${major.selflevCode}"/>
<input name="major.levels" type="hidden" value="${major.levels}"/>
<input name="major.writer" type="hidden" value="${major.writer}"/>
<input name="major.writeDate" type="hidden" value="${major.writeDate}"/>
<input name="oldCode" type="hidden" value="${major.mjCode}"/>
<input name="major.isUsable" type="hidden" value="${major.isUsable}"/>
<input name="major.comId" type="hidden" value="${major.comId}"/>
<div class="Conter_main_conter" style="width: 70%">
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">专业修改：</td>
		    </tr>
		    <tr>
			    <td width="30%" class="Content_tab_style1" >专业编码：</td>
			    <td width="70%" class="Content_tab_style2">
			    	<input type="text"  readonly="readonly" id="mjCode"  size="29" name="major.mjCode" value="${major.mjCode }"/>
			    </td>
		    </tr>
		    <tr>
			    <td width="15%" class="Content_tab_style1">专业名称：</td>
			    <td width="35%" class="Content_tab_style2">
			    <input type="text" size="29"   id="mjName" name="major.mjName" value="${major.mjName }"/><font color="#ff0000">*</font></td>
		    </tr>
		    
		   	<tr>
			    <td width="15%" class="Content_tab_style1">排列序号：</td>
			    <td width="35%" class="Content_tab_style2">
			   	 <input name="major.displaySort" size="29"  type="text" id="displaySort" value="${major.displaySort}" onkeyup="value=value.replace(/[^\d\.]/g,'');" />
			    </td>
		   	</tr>
		  	<tr >
	            <td colspan="2" id="operateid" align="center" >
	            	<button type="button" class="btn btn-info" id="btn-add" <c:if test="${major.isUsable == '1' }">disabled</c:if> onclick=" doSubmit('save');" ><i class="icon-white icon-plus-sign"></i> 增加下级</button>
					<button type="button" class="btn btn-info" id="btn-edit" onclick="doSubmit('update');"><i class="icon-white icon-edit"></i> 保存</button>
				    <button type="button" class="btn btn-danger" id="btn-del" onclick="doSubmit('delete');"><i class="icon-white icon-trash"></i> 删除</button>
				</td>
			</tr>
   		</table> 
</div>
</form>
</body>
</html>

