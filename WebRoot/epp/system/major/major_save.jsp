<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加专业</title>
<script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script type="text/javascript">
        var comId="${comId}";
	function checkPageValue( obj){
		var t=DwrService.ifExitCodeInTableComId("Major", "mjCode,isUsable", obj.value+",0",comId, set_view);	
	    if(t!=0){
		   showMsg("alert","专业编码已经存在，请重新输入") ;
		}
	}
	function set_view(count){
		if( count >0){
			document.getElementById("infoDsp").innerHTML="专业编码已存在,请重新输入!";
			document.getElementById("mjCode").focus();
			$('#submitButton').hide();
		}else{
			document.getElementById("infoDsp").innerHTML="";
			$('#submitButton').show();
		}
	}
	
	function doSubmit(){
			
			var mjName=document.getElementById("mjName").value;
			if(mjName=="" )
			{
				showMsg("alert","温馨提示：请完善表单信息！");
				return false ;
			}
				
				var parentCode = $('#parentCode').val();
				var newCode = $('#mjCode').val();
				if(parentCode!=""){
					if(newCode.indexOf(parentCode) != 0 || parentCode == newCode){
						showMsg("alert","新增类别编码请以父类别编码为前缀扩充！");
						return false;
					}
				}
				
				var action="saveMajor_major.action";
				
			    $.dialog.confirm("温馨提示：确定要提交信息吗?",function(){
				    document.forms[0].action=action;
					document.forms[0].submit();
				});
			
	}
	
	function saveInit(){	
	   window.location.href = "saveMajorInit_major.action";
	}
</script>
</head>

<body >
<form id="major_save" name="deptadds" method="post" action="" >
<input name="major.parentId" type="hidden" value="${major.mjId }" /> 
<input name="major.isHaveChild" type="hidden" value='1' />
<input name="major.isUsable" type="hidden" value='0' />
<input id="parentCode" type="hidden" value="${major.mjCode}"/>
<div class="Conter_main_conter" style="width: 70%">
<table class="table_ys1">
<tr align="center" >
    	<td  colspan="2" nowrap width="3%" class="Content_tab_style_05">专业下级增加</td>
    </tr>
  <tr>
    <td class="Content_tab_style1">上级专业名称：</td>
    <td width="75%"  class="Content_tab_style2">${major.mjName }</td>
  </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1">专业编码：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="major.majorCode"/>
    <input readonly="readonly" name="major.mjCode"  size="29" class="Content_input_style1"  type="hidden" id="mjCode" onblur="checkPageValue(this);" value="${major.majorCode }"/>&nbsp;<font color="#FF0000">*<div id="infoDsp"></div></font></td>
  </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1">专业名称：</td>
    <td width="75%" class="Content_tab_style2"><input name="major.mjName" size="29" class="Content_input_style1"  type="text" id="mjName"  value="" />&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font></td>
  </tr>
    
  <tr>
    <td height="25%" class="Content_tab_style1">排列序号：</td>
    <td width="75%"  class="Content_tab_style2"><input name="major.displaySort" size="29" class="Content_input_style1"   type="text" id="displaySort" value="" onkeyup="value=value.replace(/[^\d\.]/g,'');"/>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2" align="center" >						   
  		<button type="button" class="btn btn-info" id="btn-edit" onclick="doSubmit();"><i class="icon-white icon-edit"></i> 保存</button>
  		<button type="button" class="btn btn-info" id="btn-save" onclick="saveInit()"><i class="icon-white icon-plus-sign"></i> 增加一级专业</button>
	</td>
  </tr>
 
  
</table>

</div>
</form> 
</body>
</html>

