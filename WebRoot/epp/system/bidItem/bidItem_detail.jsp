<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>查看评委评标明细</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		
	});
	
	</script>
</head>
<body >
<form id="bidItemUpdate" name="" method="post" action="" >
<input type="hidden" name="bidItem.itemType" value="${bidItem.itemType }"/>
<input type="hidden" name="bidItem.isUsable" value="${bidItem.isUsable }"/>

<div class="Conter_Container" >
	<input type="hidden" id="" name="bidItem.biId" value="${bidItem.biId }" />
    <div class="Conter_main_conter" style="width:90%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_04"><s:if test='bidItem.itemType=="0"'>技术</s:if><s:else>商务</s:else>评分说明</td>
		    </tr>
		     <tr>
		    <td width="30%" align="center" class="Content_tab_style1">评分项目：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="bidItem.itemName"/></td>
		  </tr>
		  
		  
		  
		  <tr>
		    <td align="center" class="Content_tab_style1">标准分值：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="bidItem.points"/></td>
		  </tr>
		  <tr>
		    <td align="center" class="Content_tab_style1">评分说明：</td>
		    <td width="70%" class="Content_tab_style2">
		        <s:property value="bidItem.itemDetail"/>
		   </td>
		  </tr>
		  <tr> 
		    <td align="center" class="Content_tab_style1">备&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
		    <td width="70%" class="Content_tab_style2">
		    	<s:property value="bidItem.remark"/>
		    </td>
		  </tr>
		</table>
		
</div>
</div>
</form>
</body>
</html> 	