<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加部门</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script language="javaScript">
	javascript:window.history.forward(1);
	
	function  getStringLen(Str){     
    var i,len,code;     
    if(Str==null || Str == "")   
    	return   0;     
    len = Str.length;     
    for(i=0; i<Str.length; i++){       
	    code = Str.charCodeAt(i);     
	    if(code > 255) {
	    	len ++;
	    }     
    }     
    return   len;     
}     
	
function checkLen(value,length){ 
    var len = getStringLen(value);  
    document.getElementById("YesCou03").value = len; 
} 
</script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		
	});
	
	</script>
</head>
<body >
<form id="bidItemUpdate" name="" method="post" action="" >
<input type="hidden" name="bidItem.itemType" value="<s:property value="bidItem.itemType"/>"/>
<input type="hidden" name="bidItem.isUsable" value="<s:property value="bidItem.isUsable"/>"/>

<div class="Conter_Container" >
	<!-- 当前位置区域  begin-->
	<%--<div class="Conter_right1"><span class="Content_font2">系统管理 >> <s:if test='itemType=="0"'>技术</s:if><s:else>商务</s:else>评分项目设置 >> 新增<s:if test='itemType=="0"'>技术</s:if><s:else>商务</s:else>评分项目</span></div>--%>
    <!-- 当前位置区域  end-->
	<input type="hidden" id="" name="bidItem.biId" value="<s:property value="bidItem.biId"/>" />
    <div class="Conter_main_conter" style="width:60%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05"><s:if test='bidItem.itemType=="0"'>技术</s:if><s:else>商务</s:else>评分说明</td>
		    </tr>
		     <tr>
		    <td height="24" width="30%" align="center" class="Content_tab_style1">评分项目：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="bidItem.itemName"/></td>
		  </tr>
		  
		  
		  
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">标准分值：</td>
		    <td width="70%" class="Content_tab_style2"><s:property value="bidItem.points"/></td>
		  </tr>
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">评分说明：</td>
		    <!--<td width="70%" class="Content_tab_style2"><input  size="42"   name="bidItem.itemDetail" type="text" id="" value="<s:property value="bidItem.itemDetail"/>">&nbsp;<font color="#FF0000">*<div id="infoDsp"></div></font></td>
		  	-->
		  	<td width="70%" class="Content_tab_style2">
		  <s:property value="bidItem.itemDetail"/>
				
		  </td>
		  </tr>
		  <tr> 
		    <td height="24" align="center" class="Content_tab_style1">备&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
		    <td width="70%" class="Content_tab_style2">
		    	<s:property value="bidItem.remark"/>
		    </td>
		  </tr>
		</table>
</div>
</div>
</form>
</body>
</html> 	