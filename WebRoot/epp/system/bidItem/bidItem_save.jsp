<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>技术评分项目增加</title>

<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script src="<%= path %>/common/script/generalCheck.js" type="text/javascript" ></script>
<link href="<%=path %>/style/default.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="<%=path %>/common/jQuery/jquery-1.7.2.min.js"></script>
<script language="javaScript">
	
function  getStringLen(Str){     
    var i,len,code;     
    if(Str==null || Str == "")   
    	return   0;     
    len = Str.length;     
    for(i=0; i<Str.length; i++){       
	    code = Str.charCodeAt(i);     
	    if(code > 255) {
	    	len ++;
	    }     
    }     
    return   len;     
}  
function checkLen(length){ 
	var value = $("#bidLen").val();
    var len = getStringLen(value); 
  
    document.getElementById("YesCou03").value = len;  
}
</script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
	    window.onload=function(){ 
			  	  showMsg('success','${message}');
		            W.doQuery();
		            api.close();
		  	      }
	    </c:if>
	   
	});
	
	</script>
</head>
<body >
<form class="defaultForm" id="bidItemAdd" name="" method="post" action="saveBidItem_bidItems.action" >
<input type="hidden" id="bidType" name="bidItem.itemType" value="${bidItem.itemType }"/>
<input type="hidden" name="bidItem.isUsable" value="0"/>
<!-- 防止表单重复提交 -->
<s:token/>

<div class="Conter_Container" >
    <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05"><c:choose><c:when test="${bidItem.itemType=='0'}">技术</c:when><c:otherwise>商务</c:otherwise></c:choose>评分项目增加</td>
		    </tr>
		     <tr>
		    <td height="24" width="30%" align="center" class="Content_tab_style1">评分项目：</td>
		    <td width="70%" class="Content_tab_style2"><input  size="42" datatype="*" nullmsg="评分项目不能为空！"   name="bidItem.itemName" type="text" id="" value="">&nbsp;<font color="#FF0000">*</font>
		   		<div class="info"><span class="Validform_checktip">评分项目不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			 </td>
		  </tr>
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">标准分值：</td>
		    <td width="70%" class="Content_tab_style2"><input size="42"  datatype="n |/^[0-9]+(.[0-9]{1})+(.[0-9]{2})?$/" nullmsg="标准分值不能为空！" errormsg="标准分值格式不正确！"   name="bidItem.points" type="text" id=""  value="">&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font>
		   		<div class="info"><span class="Validform_checktip">标准分值格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		  </tr>
		  
		   <tr>
		    <td height="24" align="center" class="Content_tab_style1">评分说明：</td>
		    <td width="70%" class="Content_tab_style2">
		    <textarea name="bidItem.itemDetail" rows="5" id="bidLen" datatype="*0-1000 |/^\s*$/ " errormsg="评分说明限1000个字符！" cols="50%" onkeyup="checkLen(1000)"  class="Content_input_style2"></textarea>
				<font color="#FF0000">*</font>
				<font color="#FF0000">[限1000个汉字!包括标点和空格]</font>&nbsp;可输入字数：<input name=ModCou03 size=3 readonly value="1000" class="ModCou03"/>&nbsp;已输入字数：<input name=YesCou03 id="YesCou03" size=3 readonly value="0" class="YesCou03"/>
		   		<div class="info"><span class="Validform_checktip">评分说明限1000个字符！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			
		    </td>
		    
		  </tr>
		  <tr>
		    <td height="24" align="center"  cols="52%"  class="Content_tab_style1">备&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
		    <td width="70%" class="Content_tab_style2">
		    	<textarea cols="50%" rows="4" class=Content_input_style2 name="bidItem.remark" id=""></textarea>
		    </td>
		  </tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	