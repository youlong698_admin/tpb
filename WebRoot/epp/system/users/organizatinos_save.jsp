<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>增加群组成员</title>
    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
    <script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function (){
		
		    //移除供应商
		$('#user1Chosen .search-choice').live('click',function(){
			var that=$(this);
			var userId=that.attr('user-id');
			var userIds=$("#userIds").val();
			
			$("#userIds").val(userIds.replace(","+userId+",",","));
			that.remove();
		});
	});
	
		function doBack(){
			window.location.href='viewOrganizatinos_users.action?organizatinos.orgId=${organizatinos.orgId}';
		}
		
		//选择群组成员
	function lxr(){
		var ul =document.getElementById("userIds").value;
		
		if(ul.length==0 || ul==","){
			 ul=-1;
		 }else{
		 	ul=ul.substring(1,ul.lastIndexOf(","));
		 }
		createdetailwindow_choose("选择群组成员","viewDeptIndex_userTree.action?ul="+ul ,1);	    
	   
	}
	
	function valueEvaluateUser(){
	var users=$("#returnValues").val();
	$("#user1Chosen").html("");
	document.getElementById("userIds").value="";
	
		 	if(users!=""){
				var str=",";
				var std=",";
				var ur = users.split(",");
				for(var i=0;i<ur.length;i++){
					var u = ur[i].split(":");
					str += u[0]+",";
					std += u[1]+",";
						$('<li class="search-choice" user-id="'+u[0]+'"><span>'+u[1]+'</span><a class="search-choice-close" ></a></li>')
						.appendTo('#user1Chosen');
					}
					document.getElementById("userIds").value=str;
					
			}
	}
	</script>
  </head>
  
 <body>
<form class="defaultForm" name="form" id="saveOrg" method="post" action="saveOrganizatinos_users.action">
<input type="hidden"  name="orguser.orgId" value="<s:property value='organizatinos.orgId' />" />

<input type="hidden" id="returnValues" name="returnValues" />
<s:token/>
<div class="Conter_Container">
	<div class="Conter_main_conter">
	    	<!-- 基本信息  begin-->
	    	<table class="table_ys1">
	        	<tr>
	          		<td  colspan="2" class="Content_tab_style_05">群组成员基本信息</td>
	        	</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">群组成员:</td>
		    		<td width="75%" colspan="4" class="Content_tab_style2">
					<input  id="ur" type="hidden" size="80" name="orguser.userName"  readonly /><font color="#ff0000">&nbsp;*</font>
					<input  id="userIds" type="hidden" name="orguser.orgUserIds"  datatype="*" nullmsg="群组成员不能为空！"/>
					<div class="chosen-container-multi">
						<ul class="chosen-choices" id="user1Chosen">
						</ul>
					</div>
					<div id="chosen-img">
					&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择群组成员" onclick="lxr()"/>
						<div class="info"><span class="Validform_checktip">群组成员不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</div>
				</td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">群组名称:</td>
		    		<td width="75%" colspan="4" class="Content_tab_style2">
					<input  id="ur" type="text" size="80" name="organizatinos.orgName"  value="<s:property value='organizatinos.orgName'/>"  readonly /><font color="#ff0000">&nbsp;*</font>
					<input type="hidden" name="organizatinos.orgId" value=""/>
				</td>
				</tr>
		    </table>
      	
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-cacel" type="button" id="btn-cacel" onclick="doBack();"><i class="icon icon-repeat"></i>返回</button>
		</div>
	</div>
</div>
</form>    
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			
			var userIds=$("#userIds").val();
			
			$("#userIds").val(userIds.substring(1, userIds.lastIndexOf(",")))
			return true;
		}
	});
})
</script>
</body>
</html>
