<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>增加用户</title>
    <script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
    <script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
    <script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<%=path%>/common/multiple-select/css/multiple-select.css" />
    <script src="<%=path%>/common/multiple-select/js/multiple-select.js" type="text/javascript"></script>
	<script language="javascript">
	    var comId="${comId}";
		function checkPageValue( obj){
			DwrService.ifExitCodeInTable("Users", "userName,isUsable", obj.value+",0" , set_view );	
		}
		function set_view(count){
			if( count >0){
				document.getElementById("infoDsp").innerHTML="登录名已存在,请重新输入!";
				document.getElementById("userName").focus();
				$('#submitButton').hide();
			}else{
				document.getElementById("infoDsp").innerHTML="";
				$('#submitButton').show();
			}
		}
		function selectMaterialKind(){
		  var materialKindIds=$("#materialKindIds").val();
		  createdetailwindow("选择采购类别","viewMaterialKindWindow_materialKindSelect.action?materialKindIds="+materialKindIds,4);
		}
		
	function valueProKind(){
		
		var winObj = $('#materidKindReturnVals').val();
		var proKindIds = "";
		var proKindNames = "";
		if(winObj!=null&&winObj!=''){
			var proKindArr = winObj.split(",");
			for(var i=0;i<proKindArr.length;i++){
				if(proKindArr[i]!=""){
					var proKinds = proKindArr[i].split(":");
					proKindIds += proKinds[0]+",";
					proKindNames += proKinds[1]+",";
				}
			}
			var materialKindIds = proKindIds.substring(0, proKindIds.length-1);
			var maerialKindNames = proKindNames.substring(0, proKindNames.length-1);
			//reSetWindowValue('proKindIds',proKindIds,'proKindNames',proKindNames);
			$("#materialKindIds").val(materialKindIds);
			$("#maerialKindNames").val(maerialKindNames);
		}
	}
	</script>
  </head>
  
 <body>
<form class="defaultForm" name="form" method="post" action="saveUsers_users.action">
<input type="hidden" name="departments.depId" value="<s:property value="departments.depId" />" />
<input type="hidden" name="users.depId" value="<s:property value="departments.depId" />" />
<input type="hidden" id="materialKindIds" name="materialKindIds" value="" />
<input type="hidden" id="materidKindReturnVals" name="materidKindReturnVals" value=""/>
<s:token/>
<div class="Conter_Container">
	 
	    <div class="Conter_main_conter">
	    	<!-- 基本信息  begin-->
	    	<table class="table_ys1">
	        	<tr>
	          		<td  colspan="5" class="Content_tab_style_05">用户基本信息</td>
	        	</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">系统登录名：</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.userName" id="userName"  datatype="s4-18"  errormsg="系统登录名至少5个字符,最多18个字符！" nullmsg="系统登录名不能为空！" size="29" value="" onblur="checkPageValue(this);"   type="text" />&nbsp;<font color="#FF0000">*<div id="infoDsp"></div></font>
				    	<div class="info"><span class="Validform_checktip">系统登录名不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">用户姓名:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.userChinesename" datatype="*" nullmsg="用户姓名不能为空！" id=""  size="29" value=""  class="Content_input_style1" type="text" />&nbsp;<font color="#FF0000">*</font>
				    <div class="info"><span class="Validform_checktip">用户姓名不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">登录密码:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.userPassword" datatype="*6-15"  nullmsg="登录密码不能为空！" errormsg="密码长度只能在6-15位之间" id="userPassword1"  size="29" value="123456"  type="password" />&nbsp;<font color="#FF0000">*默认密码为“123456”</font>
				    <div class="info"><span class="Validform_checktip">登录密码不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					</td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">确认密码:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.confirmPassWord" datatype="*" recheck="users.userPassword"  nullmsg="确认密码不能为空！" errormsg="您两次输入的账号密码不一致！" id="userPassword2"  size="29" value="123456"   type="password" />&nbsp;<font color="#FF0000">*</font>
				    <div class="info"><span class="Validform_checktip">您两次输入的账号密码不一致！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
					 </td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">部门名称:</td>
				    <td width="75%" class="Content_tab_style2"><input name="" id=""  size="29" value="<s:property value="departments.deptName" />" readonly class="Content_input_style1" type="text" />&nbsp;</td>
				</tr>
				<tr>
				    <td height="25%" class="Content_tab_style1">岗位:</td>
				    <td width="75%" class="Content_tab_style2">
				    <select id="stations" multiple="multiple">
				        <c:forEach var="dictionary" items="${dictionaryList}">
				           <option value="${dictionary.dictName }">${dictionary.dictName }</option>
				        </c:forEach>
				    </select>
				    <input type="hidden"  name="users.station" id="station" value=""/>
				    </td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">联系方式-移动电话:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.phoneNum" id="" datatype="m" nullmsg="移动电话不能为空！" errormsg="移动电话格式不正确" placeholder="移动电话用于接收平台短信息" size="29" value=""  class="Content_input_style1" type="text" />&nbsp;<font color="#FF0000">*</font>
				    	<div class="info"><span class="Validform_checktip">移动电话格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				    </td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">联系方式-固定电话:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.telNum" id="" size="29" value=""  class="Content_input_style1" type="text" />&nbsp;
				    	
				    </td>
				</tr>				
	        	<tr>
				    <td height="25%" class="Content_tab_style1">Email:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.email" datatype="e" nullmsg="E-mail不能为空" errormsg="E-mail格式不正确！" placeholder="E-mail用于接收平台邮件" id="email"  value=""  class="Content_input_style1" type="text" />&nbsp;<font color="#FF0000">*</font>
				    <div class="info"><span class="Validform_checktip">E-mail不能为空或E-mail格式不正确！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>	
				    </td>
				</tr>
				<tr>
				    <td height="25%" class="Content_tab_style1">所管理的采购类别:</td>
				    <td width="75%" class="Content_tab_style2">
					    <textarea id="maerialKindNames" name="maerialKindNames" cols="70%" rows="5" style="width: 90%" readonly></textarea>
						&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择采购类别"  onclick="selectMaterialKind();"/>
					 </td>
				</tr>
				<tr>
				    <td height="25%" class="Content_tab_style1">所管理的采购组织:</td>
				    <td width="75%" class="Content_tab_style2">
				       <c:forEach items="${deptList}" var="dept">
				         <input type="checkbox" name="deptPurchaseId" value="${dept.depId }"/>${dept.deptName }&nbsp;&nbsp;&nbsp;
				       </c:forEach>
					</td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">用户状态:</td>
				    <td width="75%" class="Content_tab_style2">
				    <input name="users.isUsable" type="radio" checked value="0" />有效
				    <input name="users.isUsable" type="radio" value="1" />无效&nbsp;</td>
				</tr>
		    </table>
   <!-- 基本信息  end-->
    
   		
		<div class="buttonDiv">
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				<button class="btn btn-cacel" type="button" id="btn-danger" onclick="window.location.href='viewUsers_users.action?users.depId=<s:property value="users.depId" />'"><i class="icon icon-repeat"></i>返回</button>
			</div>
	</div>
</div>
</form>    
<script type="text/javascript">
$(function(){
    
	$('#stations').change(function() {
            //console.log($(this).val());
        }).multipleSelect({
        });
        
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
			}	
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;
			var stations=",";  
            $("#stations option:selected").each(function (i) {  
                stations += ($(this).val()+",");    
            }); 
            $("#station").val(stations);	            
            document.form.submit();	
			return false;	
		}
	});
})
</script>

</body>
</html>
