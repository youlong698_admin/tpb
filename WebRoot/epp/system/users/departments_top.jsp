<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<title>专家查询分析</title>
		<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	    <script type="text/javascript">
	      function switchTab(url){
	         $('#iframe').attr('src',url);
	      }
	    </script>
	
	</head>
	<body>
	 <div class="page-content">

         <!-- /.page-header --> 

         <div class="row">
             <div class="col-xs-12">
                 <!-- PAGE CONTENT BEGINS -->
                 <!--内容-->
                 <div class="row">
                     <div class="col-md-12">
                         <div class="tabbable">

                             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="cggd_info">
                                 <li class="active">
                                     <a data-toggle="tab" onClick="window.parent.LeftFrame.location.href ='viewDepartmentsInitTree_users.action?tempUser=${tempUser}'">机构树</a>
                                 </li>

                                 <li>
                                     <a data-toggle="tab" onClick="window.parent.LeftFrame.location.href='viewOrganizatinosInitTree_users.action?orgType=org'">群组树</a>
                                 </li>

                             </ul>

                             <div class="tab-content">

                                 <div id="smsj">
                                     <iframe src="viewDepartmentsInitTree_users.action?tempUser=${tempUser}" id="iframe" frameborder=0 scrolling="yes" width="100%" height="440px"></iframe>
                                 </div> 
                             </div>
                         </div>

                     </div>
                 </div>

                 <!-- PAGE CONTENT ENDS -->
             </div>
             <!-- /.col -->
         </div>
         <!-- /.row -->
     </div>
</body>
</html>