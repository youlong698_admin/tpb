<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <base href="<%=basePath%>"/>
      
    <title>查看用户</title>
    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	 <script type="text/javascript">
		$(function (){
		
			var api = frameElement.api, W = api.opener;
			
		   
		});
	
	</script>

  </head>
  
 <body>
<form id="userUpdate" method="post" action="">
<input type="hidden" name="departments.depId" value="<s:property value="departments.depId" />" />
<input type="hidden" name="users.userId" value="<s:property value="users.userId" />" />
<input type="hidden" name="users.depId" value="<s:property value="departments.depId" />" />
<input type="hidden" name="users.userPassword" value="<s:property value="users.userPassword" />" />
<input type="hidden" id="oldName" value="<s:property value="users.userName" />" />
<input type="hidden" id="users.isUsable"  value="<s:property value="users.isUsable" />" />

<div class="Conter_Container">
	<div class="Conter_main_conter">
	    	<!-- 基本信息  begin-->
	    	<table class="table_ys1">
	        	<tr>
	          		<td  colspan="5" class="Content_tab_style_05">用户基本信息</td>
	        	</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">系统登录名：</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.userName" /></td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">用户姓名:</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.userChinesename" /></td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">岗位:</td>
				    <td width="75%" class="Content_tab_style2">
				    <s:property value="users.station" />
				    </td>
				</tr>
	        	<%--<tr>
				    <td height="25%" class="Content_tab_style1">登录密码:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.userPassword" id="userPassword1"  size="29" value="<s:property value="users.userPassword" />"  class="Content_input_style1" type="password" />&nbsp;<font color="#FF0000">*</font></td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">确认密码:</td>
				    <td width="75%" class="Content_tab_style2"><input name="users.confirmPassWord" id="userPassword2"  size="29" value="<s:property value="users.userPassword" />"  class="Content_input_style1" type="password" />&nbsp;<font color="#FF0000">*</font></td>
				</tr>
	        	--%><tr>
				    <td height="25%" class="Content_tab_style1">部门名称:</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.depIdCn" />&nbsp;</td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">联系方式-移动电话:</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.phoneNum" /></td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">联系方式-固定电话:</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.telNum" /></td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">联系地址:</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.address" /></td>
				</tr>
	        	<tr>
				    <td height="25%" class="Content_tab_style1">Email:</td>
				    <td width="75%" class="Content_tab_style2"><s:property value="users.email" /></td>
				</tr>
		    </table>

<!-- 其它信息  end -->
   		
	</div>
</div>
</form>    




</body>
</html>
