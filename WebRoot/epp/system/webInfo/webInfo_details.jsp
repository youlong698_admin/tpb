<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<title>公告信息查看</title>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
	
	    
	});
	
	</script>
</head>

<body >
<div class="Conter_Container" >
    <div class="Conter_main_conter"  >
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">公告信息</td>
		    </tr>
			<tr>
				<td width="20%"  align="center" class="Content_tab_style1">标&nbsp;&nbsp;&nbsp;&nbsp;题：</td>
				<td width="80%" class="Content_tab_style2">${webInfo.title }</td> 
			</tr>
			<tr>
				<td  class="Content_tab_style1">关键字：</td>
				<td class="Content_tab_style2">${webInfo.keyword }</td> 
			</tr>
			<tr>
				<td  class="Content_tab_style1">阅读量：</td>
				<td class="Content_tab_style2">${webInfo.readCount} </td> 
			</tr>
			<tr>
				<td  class="Content_tab_style1">图片：</td>
				<td class="Content_tab_style2">
                       <c:if test="${not empty webInfo.logo}"><img src="/fileWeb/${webInfo.logo}" width="200px" height="100px"></c:if>
                     </td> 
			</tr>
			<tr>
				<td align="center" class="Content_tab_style1">详细内容：</td>
				<td><c:out value="${webInfo.content}" escapeXml="false"/></td> 
			</tr>
			<tr>
				<td  class="Content_tab_style1">附&nbsp;&nbsp;&nbsp;&nbsp;件：</td>
				<td class="Content_tab_style2"><c:out value="${webInfo.attachmentUrl}" escapeXml="false"/></td> 
			</tr>
	
	</table> 
	
</div>
</div>
</body>
</html> 	