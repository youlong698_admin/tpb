<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>数据字典树</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		
		<link rel="stylesheet" href="<%=path %>/common/ace/assets/css/bootstrap.min.css" />
		
		<script src="<%=path %>/common/jQuery/jquery-1.8.3.min.js"></script>
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		<!--  <script src="<%=path %>/common/zTree/jquery.ztree.excheck.js"></script>-->
		<script type="text/javascript">
		 var setting = {
			view: {
				selectedMulti: false,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			async: {
				enable: true,
				dataType:"json",
				url: "viewDictTree_dict.action",
				autoParam: ["id"]
			},
			callback: {
				//onCheck: onCheck  //选中事件
				onClick: zTreeOnClick  //点击事件
			}
		};

		
		function zTreeOnClick(e, treeId, treeNode) {
			
			parent.MainFrame.location.href = "viewTSysDict_dict.action?dict.parentDictId="+treeNode.id;
		}
		
		function loadShow(){
			
			parent.MainFrame.location.href = "viewTSysDict_dict.action";
		}
		
		

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		</script>
	</head>

	<body onload="loadShow()">
	
		<ul id="treeDemo" class="ztree"></ul>
		

		
		
	</body>
</html>