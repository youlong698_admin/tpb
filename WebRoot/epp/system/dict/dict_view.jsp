<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>字典表管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
<script src="<%=path%>/common/script/context_table.js" type="text/javascript"></script>
<script type="text/javascript">
var _table;
var parentDictId="${dict.parentDictId}";
$(function (){
	var $wrapper = $('#div-table-container');
	var $table = $('#table-general');
	url="";
	if(parentDictId==null||parentDictId==""){
	 url="findviewTSysDict_dict.action";
	}else{
	 url="findviewTSysDict_dict.action?dict.parentDictId="+parentDictId;
	}
	_table = $table.dataTable($.extend(true,{
	},CONSTANT.DATA_TABLES.DEFAULT_OPTION, {
		ajax : function(data, callback, settings) {//ajax配置为function,手动调用异步查询
			//手动控制遮罩
			$wrapper.spinModal();
			//封装请求参数
			var param = GeneralManage.getQueryCondition(data);
			$.ajax({
		            type: "POST",
		            url: url,
		            cache : false,	//禁用缓存
		            data: param,	//传入已封装的参数
		            dataType: "json",
		            success: function(result) {
		            	//setTimeout仅为测试遮罩效果
		            	setTimeout(function(){
		            		//异常判断与处理
		            		if (result.errorCode) {
		            			
		            			return;
							}
		            		
		            		//封装返回数据，这里仅修改属性名
		            		var returnData = {};
			            	returnData.draw = data.draw;//这里直接自行返回了draw计数器,应该由后台返回
			            	returnData.recordsTotal = result.total;
			            	returnData.recordsFiltered = result.total;//后台不实现过滤功能，每次查询均视作全部结果
			            	returnData.data = result.pageData;
			            	//关闭遮罩
			            	$wrapper.spinModal(false);
			            	//调用DataTables提供的callback方法，代表数据已封装完成并传回DataTables进行渲染
			            	//此时的数据需确保正确无误，异常判断应在执行此回调前自行处理完毕
			            	callback(returnData);
		            	},200);
		            },
		            error: function(XMLHttpRequest, textStatus, errorThrown) {
		               
		                $wrapper.spinModal(false);
		            }
		        });
		},
        columns: [
            CONSTANT.DATA_TABLES.COLUMN.CHECKBOX,
            {
            	data: "dictCode",
            	width : "15%",
            	orderable : false
            },
			{
				className : "ellipsis",
				data : "dictName",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			},
			{
				className : "ellipsis",
				data : "dictOrder",
				width : "10%"
			},
			{
				className : "ellipsis",
				data : "remark",
				width : "20%",
				render: CONSTANT.DATA_TABLES.RENDER.ELLIPSIS,
				orderable : false
			}
        ],
        "createdRow": function ( row, data, index ) {
        	//行渲染回调,在这里可以对该行dom元素进行任何操作
        	//给当前行加样式
        	if (data.role) {
        		$(row).addClass("info");
			}
        	
            
        },
        "drawCallback": function( settings ) {
        	//渲染完毕后的回调
        	//清空全选状态
			$(":checkbox[name='cb-check-all']",$wrapper).prop('checked', false);
        	//默认选中第一行
        	$("tbody tr",$table).eq(0).click();
        }
	})).api();//此处需调用api()方法,否则返回的是JQuery对象而不是DataTables的API对象

    
    
	
	
	$("#btn-advanced-search").click(function(){
		_table.draw();
	});
		
	$("#btn-add").click(function(){
		GeneralManage.addItemInit();
	});
	
	$("#btn-edit").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.editItemInit(arrItemId);
	});
	
	$("#btn-del").click(function(){
		var arrItemId = [];
        $("tbody :checkbox:checked",$table).each(function(i) {
        	var item = _table.row($(this).closest('tr')).data();
        	arrItemId.push(item);
        });
		GeneralManage.deleteItem(arrItemId);
	})
	
	
	//行点击事件
	$("tbody",$table).on("click","tr",function(event) {
		$(this).addClass("active").siblings().removeClass("active");
		//获取该行对应的数据
		//var item = _table.row($(this).closest('tr')).data();
		//GeneralManage.currentItem = item;
    });
	
	$table.colResizable();$table.on("change",":checkbox",function() {
		if ($(this).is("[name='cb-check-all']")) {
			//全选
			$(":checkbox",$table).prop("checked",$(this).prop("checked"));
		}else{
			//一般复选
			var checkbox = $("tbody :checkbox",$table);
			$(":checkbox[name='cb-check-all']",$table).prop('checked', checkbox.length == checkbox.filter(':checked').length);
		}
    }).on("click",".td-checkbox",function(event) {
    	//点击单元格即点击复选框
    	!$(event.target).is(":checkbox") && $(":checkbox",this).trigger("click");
    })
	
	$("#toggle-advanced-search").click(function(){
		$("i",this).toggleClass("icon-search icon-circle-arrow-up");
		$("#div-advanced-search").slideToggle("fast");
	});
	
	$("#btn-info-content-collapse").click(function(){
		$("i",this).toggleClass("fa-minus fa-plus");
		$("span",this).toggle();
	});
	
});
		

var GeneralManage = {
	currentItem : null,
	getQueryCondition : function(data) {
		var param = {};

		//组装排序参数 
		//默认进入的排序
		 param.orderColumn="de.dictId";
		if (data.order&&data.order.length&&data.order[0]) {
			switch (data.order[0].column) {
			case 3:
				param.orderColumn = "de.dictOrder";
				break;
			}
			param.orderDir = data.order[0].dir;
		}
		
		//组装查询参数
		
		//组装分页参数
		param.startIndex = data.start;
		param.pageSize = data.length;
		
		param.draw = data.draw;
		
		return param;
	},
	// 新增
	addItemInit: function()  {
	   if('<s:property value="dict.parentDictId"/>'==""){
	       showMsg("alert","温馨提示：请先选择左侧的字典信息");
	       return false;
	   }
	   createdetailwindow("新增数据字典","saveTSysDictInit_dict.action?dict.parentDictId=<s:property value="dict.parentDictId"/>",2);	    
	},
	//修改
	editItemInit: function(selectedItems)  {
	   var v = selectedItems;
	   if(v.length==0){
           showMsg("alert","温馨提示：请选择信息");
       }else if(v.length > 1){
   	      showMsg("alert","温馨提示：只能选择一条信息修改！");
       }else{
	  		 var ids = "";
			   for(var i=0;i<v.length;i++){
				   if(i == (v.length-1)){
					 ids += v[i].dictId;
				   }else{
					 ids += v[i].dictId + ",";
						}
			   }
		 // document.forms[0].action="updateTSysDictInit_dict.action?dict.dictId="+ids ;		  
		 // document.forms[0].submit();
		      createdetailwindow("修改数据字典","updateTSysDictInit_dict.action?dict.dictId="+ids ,2);	    
		    
      }
	    		
	    	 
	},
	//删除
	deleteItem : function(selectedItems) {
		var v = selectedItems;
		 if(v.length>0){
	    	 var ids = "";
			 for(var i=0;i<v.length;i++){
				 if(i == (v.length-1)){
					ids += v[i].dictId;
				 }else{
					ids	 += v[i].dictId + ",";
					  }
			  }
	    /*if(confirm("你确定要删除选中的信息！")){
	    	document.forms[0].action="deleteTSysDict_dict.action?dictId="+ids ; 
	   	 	document.forms[0].submit();
	    }else{
	    	return false;
	    }*/
	    	$.dialog.confirm("温馨提示：你确定要删除选中的信息！",function(){
				   result = ajaxGeneral("deleteTSysDict_dict.action","dictId="+ids);
					   showMsg('success',''+result+'',function(){
					   		doQuery();							
		   					});
				   	 	}); 
	    }else{
	    	showMsg("alert","温馨提示：请选择删除的信息！");
	    }   
			
    }     				 
		
};
    
    function doQuery(){
     _table.draw();
    }

</script>
</head>
<body >
<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12" id="content">
				<div class="row-fluid">
					<div class="span12">
						<div class="btn-toolbar">
							<div class="pull-right">
								<div class="input-append">
									<div class="btn-group">
										<button type="button" class="btn btn-warning" title="刷新" id="toggle-advanced-refresh" onclick="doQuery()">
											 <i class="icon-white icon-refresh"></i>
										</button>
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-info" id="btn-add"><i class="icon-white icon-plus-sign"></i> 新增</button>
							<button type="button" class="btn btn-info" id="btn-edit"><i class="icon-white icon-edit"></i> 修改</button>
							<button type="button" class="btn btn-danger" id="btn-del"><i class="icon-white icon-trash"></i> 删除</button>
						   </div>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12" id="div-table-container">
						<table class="table table-striped table-bordered table-hover table-condensed" id="table-general" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>
										<input type="checkbox"  name="cb-check-all"/>
									</th>
									<th>字典表编码</th>
									<th>字典表名称</th>
									<th>排列序号</th>
									<th>备注</th>
								</tr>
							</thead>
							<tbody>
							</tbody>							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>