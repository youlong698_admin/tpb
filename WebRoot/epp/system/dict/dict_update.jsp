<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加部门</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">

	javascript:window.history.forward(1);
	function doSubmit(){
	if( doValidate( document.forms[0] ) ) {
		var action="updateTSysDict_dict.action";
		document.forms[0].action=action;
	   if(confirm("确定要提交信息吗?")){
					document.forms[0].submit();
					return true;
				}
				else
				{	
					return;
				}
		}else {
			return false ;
		}
		
	} 
</script>
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
		  window.onload=function(){ 
		    showMsg('success','${message}');
		    W.doQuery();
		    api.close();
		  	}
		</c:if>
	    
	});
	
	</script>
</head>
<body >
<form class="defaultForm" id="deptadds" name="deptadds" method="post" action="updateTSysDict_dict.action" >
<input name="dict.dictId" type="hidden" value='${dict.dictId }' /> 
<input name="dict.parentDictId" type="hidden" value='${parentDict.dictId }' /> 
<input name="dict.parentDictCode" type="hidden" value='${parentDict.dictCode }' /> 
<input name="dict.dictLevel" type="hidden" value='${dict.dictLevel }' /> 
<input name="dict.isHaveChild" type="hidden" value='1' /> 
<input name="dict.isUsable" type="hidden" value='0' />

<div class="Conter_Container" >
	 <div class="Conter_main_conter" style="width:80%;margin:10px auto" >
     
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">字典表修改</td>
		    </tr>
		    <tr>
		    <td height="24" width="30%" align="center" class="Content_tab_style1">字典类型</td>
		    <td width="70%" class="Content_tab_style2">${parentDict.dictName}&nbsp;<font color="#FF0000">*</font></td>
		  </tr>
		  
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">字典编码</td>
		    <td width="70%" class="Content_tab_style2"><input size="42"  datatype="*" nullmsg="字典编码不能为空！" name="dict.dictCode" type="text" id="dict.dictCode" value="${dict.dictCode }">&nbsp;<font color="#FF0000">*<div id="infoDsp"></div></font>
		    	<div class="info"><span class="Validform_checktip">字典编码不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		  </tr>
		  
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">字典名称</td>
		    <td width="70%" class="Content_tab_style2"><input size="42"  datatype="*" nullmsg="字典名称不能为空！" name="dict.dictName" type="text" id="dict.dictName"  value="${dict.dictName }">&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font>
		    	<div class="info"><span class="Validform_checktip">字典名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		  </tr>
		  
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">排列序号</td>
		    <td width="70%" class="Content_tab_style2"><input size="42"   name="dict.dictOrder" type="text" id="dict.dictOrder" value="${dict.dictOrder }">&nbsp;</td>
		  </tr>
		  
		  <tr>
		    <td height="24" align="center" class="Content_tab_style1">备&nbsp;&nbsp;&nbsp;&nbsp;注</td>
		    <td width="70%" class="Content_tab_style2">
		    	<textarea cols="30" class="Content_input_style2"  rows="3" name="dict.remark" id="dict.remark">${dict.remark}</textarea>
		    </td>
		  </tr>
		</table>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	