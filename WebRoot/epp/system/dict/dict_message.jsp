<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script language="javascript">
	showMsg("alert","<%= request.getAttribute("message") %>");
	
	<c:if test="${departments.operType=='add'}">
		window.parent.LeftFrame.addNode("${departments.deptName}", "${departments.depId}" ) ;
		window.parent.MainFrame.location.href = "updateDepartmentsInit_dept.action?departments.depId=${departments.depId}";
	</c:if>
	
	<c:if test="${departments.operType=='update'}">
		window.parent.LeftFrame.location.href = "viewDepartmentsInitTree_dept.action" ;
		window.parent.MainFrame.location.href = "updateDepartmentsInit_dept.action?departments.depId=${departments.depId}";
	</c:if>
	
	<c:if test="${departments.operType=='delete'}">
		window.parent.LeftFrame.location.href = "viewDepartmentsInitTree_dept.action" ;
		window.parent.MainFrame.location.href = "updateDepartmentsInit_dept.action?departments.depId=1";
	</c:if>
	
</script>
	</head>
	<body >
	</body>
</html>


