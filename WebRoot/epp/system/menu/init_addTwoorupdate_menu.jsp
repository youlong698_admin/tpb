<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>新增菜单</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"  src="<%= path %>/common/script/context.js" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<style type="text/css">
.text{
	width:200px;
	line-height:25px;
}
.text_1{
	width:300px;
	line-height:25px;
}
</style>
</head>
<body >
<form id="cx" method="post" action="">
<div id="main">
	<table class="table_ys1">
		<tr>
			<td colspan="2" class="Content_tab_style_05"></td>
		</tr>
		  
		<tr>
			<td class="Content_tab_style1" width="25%">上级菜单名称：</td>
			<td class="Content_tab_style2" width="75%">
				<input type="text" id="txtPmenuName" name="txtPmenuName" class="text" readonly="readonly" value="${pmenu.menuName }" />
				<input type="hidden" id="txtPmenuCode" name="txtPmenuName" value="${pmenu.menuCode }" />
			</td>
		</tr>		
		<tr>
	    	<td class="Content_tab_style1"  >菜单编码：</td>
				<td class="Content_tab_style2"  >
					<input type="text" id="txtMenuCode" name="txtMenuCode" value=""/>
				</td>
	  	</tr>  
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单名称：</td>
				<td class="Content_tab_style2"  >
					<input type="text" id="txtMenuName" name="txtMenuName" class="text" value=""/>
					<input type="hidden" id="menuCode" name="menuCode" value=""/>
				</td>
	  	</tr>
		<tr>
	    	<td class="Content_tab_style1"  >是否为子节点</td>
			<td class="Content_tab_style2"  >
			
				<input type="radio" name="menuFolderFlag"  id="menuFolderFlag1" value="1" />是&nbsp;&nbsp;
				<input type="radio" name="menuFolderFlag" id="menuFolderFlag0" value="0" />否<br />
			</td>
	  	</tr> 
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单排序值：</td>
			<td class="Content_tab_style2"  >
				<input type="text" id="txtSortNo" name="txtSortNo" class="text" value=""/>
			</td>
	  	</tr>
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单路径:</td>
			<td class="Content_tab_style2"  >
				<input type="text" id="txtPathCode" name="txtPathCode" class="text_1" value=""/>
			</td>
	  	</tr>	
	  	<tr>
	    	<td class="Content_tab_style1" width="20%">菜单是否可用:</td>
			<td class="Content_tab_style2" width="80%">
				<input type="radio" name="isActive" id="isActive1" value="1" />可用&nbsp;&nbsp;
				<input type="radio" name="isActive" id="isActive0" value="0" />不可用<br />
			</td>
	  	</tr>
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单备注:</td>
			<td class="Content_tab_style2"  >
				<textarea name="remark" rows="3" id="remark" style="width:300px;border: solid 1px gray;"></textarea>
			</td>
	  	</tr>
	</table>
	
	<table align="center" >
   		<tr>
			<td align="center"  id="operateButton" >
			
				 <button type="button" class="btn btn-info" id="btnSubmit" ><i class="icon-white icon-plus-sign"></i>保存</button>
				<button type="button" class="btn btn-cacel" id="btn-edit"  onclick="javascript:history.go(-1);"><i class="icon icon-repeat"></i> 返回</button>
		 	
			</td>
		</tr>
    </table>
    
    
	</div>
</form>		
</body>
<script type="text/javascript">

		$("#menuFolderFlag0").attr("checked","checked");
		$("#isActive1").attr("checked","checked");
		$("#menuLevel0").attr("checked","checked");
	
$(function(){
	
	$("#btnTest").click(function(){
		$.ajax({
			url:"<%=path%>/findTest_menurole.action",
			type:"POST",
			success:function(result){
				showMsg("alert",result);
			},
			error:function(){
				showMsg("alert","eeeeee")
			}
		});
	});
	
	
	$("#btnSubmit").click(function(){
		var menuCode = $("#txtMenuCode").val();
		var pmenuCode = $("#txtPmenuCode").val();
		var menuName = $("#txtMenuName").val();
		var menuFolderFlag = $("input:radio[name='menuFolderFlag']:checked").val();
		var sortNo = $("#txtSortNo").val();
		var pathCode = $("#txtPathCode").val();
		var isActive = $("input:radio[name='isActive']:checked").val();
		var remark = $("#remark").val();
		var tmpCode = $("#menuCode").val();
		var data = {
			menuCode:menuCode,pmenuCode:pmenuCode,menuName:menuName,
			menuFolderFlag:menuFolderFlag,sortNo:sortNo,pathCode:pathCode,isActive:isActive,
			remark:remark,tmpCode:tmpCode
		};
		if(menuName == ""){
			showMsg("alert","温馨提示:菜单名称不能为空！");
			return false;
		}else if(pathCode == ""){
			showMsg("alert","温馨提示:菜单路径不能为空！");
			return false;
		}else{
			
			$.dialog.confirm("温馨提示：你确定要修改菜单信息！",function(){
				  var result = ajaxGeneral("saveMenuData_menurole.action?prm=add",data);
				  // showMsg('success',''+result+'',function(){
			   		if(result == "true"){
							 showMsg('success','温馨提示：菜单新增成功!',function(){
						   		parent.breakMe();
					   		});
					}else{
						showMsg("alert","温馨提示：菜单新增失败!");
					}							
   					//});
		   	 	});
		}
	});
});
</script>
</html>