<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>菜单管理-菜单树</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		
		<link rel="stylesheet" href="<%=path %>/common/ace/assets/css/bootstrap.min.css" />
		
		<script src="<%=path %>/common/jQuery/jquery-1.8.3.min.js"></script>
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		<!--  <script src="<%=path %>/common/zTree/jquery.ztree.excheck.js"></script>-->
		<script type="text/javascript">
		 var setting = {
			view: {
				selectedMulti: false,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			async: {
				enable: true,
				dataType:"json",
				url: "findMenuData_menurole.action",
				autoParam: ["id"]
			},
			callback: {
				//onCheck: onCheck  //选中事件
				onClick: zTreeOnClick  //点击事件
			}
		};

		
		function zTreeOnClick(e, treeId, treeNode) {
			
			//alert(treeNode.name+"--"+treeNode.id);
			
			MainFrame.location.href = "initSysMenu_menurole.action?pmenuCode="+treeNode.pid+"&menuCode="+treeNode.id;
			
		}
		
		

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		
		function breakMe(){//添加，修改，删除后调用刷新树
			document.forms[0].action = "viewMenuIndex_menurole.action";
			document.forms[0].submit();
		}
		</script>
	</head>

	<body>
	<form>
	<%-- 标题按钮栏BGN --%>
	<input type="hidden" name="pmenuCode" id="pmenuCode"/><%--供子页面调用 --%>
	<input type="hidden"  name="pmenuName" id="pmenuName"/><%--供子页面调用 --%>
		<div style="width:27%;float:left;">
		<ul id="treeDemo" class="ztree"></ul>
		</div>
		<div style="width:73%;height:100%;float:left;">
			 <iframe style="display: block;"  name="MainFrame" width="100%" height="550px" marginwidth="0" src="" frameborder="0" scrolling="no"></iframe>
		</div>
	</form>
		
		
	</body>
</html>