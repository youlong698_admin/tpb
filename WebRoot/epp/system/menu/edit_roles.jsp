<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>角色编辑</title>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="<%=path %>/style/default.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="<%=path %>/common/jQuery/jquery-1.7.2.min.js"></script>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>	
<script type="text/javascript">
	$(function (){
	
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
		   window.onload=function(){ 
			  	  showMsg('success','${message}');
		            W.doQuery();
		            api.close();
		  	      }
	    </c:if>
	   
	});
	
	</script>
<style type="text/css">
.text{
	width:200px;
	line-height:20px;
}
.text_1{
	width:200px;
	line-height:20px;
}
</style>
</head>
<body >
<form class="defaultForm" id="cx" method="post" action="<%=path %>/saveRoles_menurole.action">
<div id="main">
	<table  class="table_ys1" style="margin-top:30px;">
		<tr>
			<td class="Content_tab_style1" width="25%">角色编码：</td>
			<td class="Content_tab_style2" width="75%">
				<input type="text" id="txtRoleNo" datatype="*" nullmsg="角色编码不能为空！" name="roleNo" value="${role.roleNo }" />
				<input type="hidden" id="txtRoleId" name="roleId" value="${role.roleId }" />
				<div class="info"><span class="Validform_checktip">角色编码不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			</td>
		</tr>
		  
	  	<tr>
	    	<td class="Content_tab_style1"  >角色名称：</td>
				<td class="Content_tab_style2"  >
					<input type="text" id="txtRoleName" datatype="*" nullmsg="角色名称不能为空！"name="roleName" value="${role.roleName }"/>
					<div class="info"><span class="Validform_checktip">角色名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
	  	</tr>
	    <tr>
	    	<td class="Content_tab_style1"  >是否有效：</td>
			<td class="Content_tab_style2"  >
				<input type="radio" name="isActive" id="isActive1" value="1" />有效&nbsp;&nbsp;
				<input type="radio" name="isActive" id="isActive0" value="0" />无效<br />
			</td>
	  	</tr>  
	  	<tr>
	    	<td class="Content_tab_style1"  >角色描述：</td>
			<td class="Content_tab_style2"  >
				<textarea name="roleDesc" datatype="*" nullmsg="角色描述不能为空！" rows="3" id="txtRoleDesc" style="width:300px;border: solid 1px gray;">${role.roleDesc }</textarea>
				<div class="info"><span class="Validform_checktip">角色描述不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				
			</td>
	  	</tr>
	</table>
	
    <div class="buttonDiv">
		<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
		
	</div>
    
	</div>
</form>		

<script type="text/javascript">
var isupdate = "${roleId }";
if(isupdate != ""){
	$("#isActive${role.isActive}").attr("checked","checked");
}else{
	$("#txtWriter").val('${writer }');
	$("#txtCreateTime").val('${writerDate }');
	$("#isActive1").attr("checked","checked");
}

</script>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html>