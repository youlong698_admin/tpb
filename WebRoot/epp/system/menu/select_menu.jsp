<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>功能菜单维护</title>

		<link rel="stylesheet" href="<%=path %>/common/zTree/zTreeStyle.css" />
		<link rel="stylesheet" href="<%=path %>/common/zTree/zTree.css" />
		<script src="<%=path%>/common/script/context.js" type="text/javascript"></script>
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<%=path %>/common/ace/assets/css/bootstrap.min.css" />
		
		<script src="<%=path %>/common/zTree/jquery.ztree.core.js"></script>
		 <script src="<%=path %>/common/zTree/jquery.ztree.excheck.js"></script>
		<script type="text/javascript"><!--
		var api = frameElement.api, W = api.opener;
		var mar;
		var roleId = "${roleId }";
		
		//var jsonTree=ajaxGeneral("selectMenuData_menurole.action",{roleId:roleId});
		//alert(jsonTree);
		
		 var setting = {
			view: {
				selectedMulti: false,  //是否允许同时选中多个节点
				showIcon: true
				
			},
			data: {
				simpleData: {
                          enable: true,
                          idKey: "id",
                          pIdKey: "pid",
                          rootPId: 0
                  }
			},
			check: {
				 enable: true,
               	 chkStyle: "checkbox",
                 chkboxType: { "Y": "ps", "N": "ps" }
			},
			async: {
				enable: true,
				dataType:"json",
				url: "selectMenuData_menurole.action?roleId=${roleId}"
				
			},
			callback: {
				beforeCheck: beforeCheck
			}

		};

		
		function zTreeOnClick(e,treeId, treeNode) {
			var zTree1 = $.fn.zTree.getZTreeObj("treeDemo");
			
			if(treeNode.isParent==true)
			{
				var bl=treeNode.checked;
				for(var obj in treeNode.children){
				 var node = zTree1.getNodeByParam("id",treeNode.children[obj].id);
					node.checked = bl; 
					zTree1.updateNode(node);
				}
			}
		}
		function beforeCheck(treeId, treeNode) {
			return (treeNode.doCheck !== false);
		}
		


		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		
		$(function(){
			 //确定事件
	         $('#btn-save').on('click', function() {
	        	
	        	var checkCount;
				//获取所选择的数据
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				checkCount = zTree.getCheckedNodes(true),
				mar="";
				
				if(checkCount.length==0){
	      			
	      			alert("请选择所授权菜单"); 
	       			return false;
				}
				var res = "";
				for(var i=0;i<checkCount.length;i++)
				{
					
					//id是MenuCode   code是MenuId
					if(checkCount[i].id!="999999")
					{
						if(i == checkCount.length-1){
							res += checkCount[i].id +"@"+checkCount[i].code;
						}else{
							res += checkCount[i].id +"@"+checkCount[i].code + ",";
						}
					}
				}
				
				var result=ajaxGeneral("saveAuthorizeSysRole_menurole.action",{roleId:roleId,menus:res});
				if(result){
							showMsg("success","温馨提示:角色授权成功！");				
							api.close();
						}else{
							showMsg("error","温馨提示:角色授权失败！");
						}
				
			});
			
			//关闭事件
			$("#btn-danger").click(function(){
				api.close();
			})
		})
		--></script>
	</head>

	<body>
	
		<div  style="height: 490px;overflow: auto">
			<ul id="treeDemo" class="ztree"></ul>
		</div>
		 <div align="right">	
				<button type="button" class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i> 确定</button>
				<button type="button" class="btn btn-danger" id=btn-danger><i class="icon-white icon-remove-sign"></i> 关闭</button>
				
			</div>
		
		
	</body>
</html>