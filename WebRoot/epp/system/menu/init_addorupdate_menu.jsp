<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>修改菜单</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"  src="<%= path %>/common/script/context.js" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<style type="text/css">
.text{
	width:200px;
	line-height:25px;
}
.text_1{
	width:300px;
	line-height:25px;
}
</style>
</head>
<body >
<form id="cx" method="post" action="">
<div id="main">
	<table class="table_ys1">
		<tr>
			<td colspan="2" class="Content_tab_style_05"></td>
		</tr>
		  
		<tr>
			<td class="Content_tab_style1" width="25%">上级菜单名称：</td>
			<td class="Content_tab_style2" width="75%">
				<input type="text" id="txtPmenuName" name="txtPmenuName" class="text" readonly="readonly" value="${pmenu.menuName }" />
				<input type="hidden" id="txtPmenuCode" name="txtPmenuName" value="${pmenu.menuCode }" />
			</td>
		</tr>
		<tr>
	    	<td class="Content_tab_style1"  >菜单编码：</td>
				<td class="Content_tab_style2"  >
					<input type="text" id="txtMenuCode" name="txtMenuCode" value="${menu.menuCode }"/>
				</td>
	  	</tr>  
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单名称：</td>
				<td class="Content_tab_style2"  >
					<input type="text" id="txtMenuName" name="txtMenuName" class="text" value="${menu.menuName }"/>
					<input type="hidden" id="txtMenuId" name="txtMenuId" value="${menu.menuId }"/>
					<input type="hidden" id="menuCode" name="menuCode" value="${menuCode }"/>
				</td>
	  	</tr>
		<tr>
	    	<td class="Content_tab_style1"  >是否为子节点</td>
			<td class="Content_tab_style2"  >
			
				<input type="radio" name="menuFolderFlag"  id="menuFolderFlag1" value="1" />是&nbsp;&nbsp;
				<input type="radio" name="menuFolderFlag" id="menuFolderFlag0" value="0" />否<br />
			</td>
	  	</tr> 
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单排序值：</td>
			<td class="Content_tab_style2"  >
				<input type="text" id="txtSortNo" name="txtSortNo" class="text" value="${menu.sortNo }"/>
			</td>
	  	</tr>
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单路径:</td>
			<td class="Content_tab_style2"  >
				<input type="text" id="txtPathCode" name="txtPathCode" class="text_1" value="${menu.pathCode }"/>
			</td>
	  	</tr>	
	  	<tr>
	    	<td class="Content_tab_style1" width="20%">菜单是否可用:</td>
			<td class="Content_tab_style2" width="80%">
				<input type="radio" name="isActive" id="isActive1" value="1" />可用&nbsp;&nbsp;
				<input type="radio" name="isActive" id="isActive0" value="0" />不可用<br />
			</td>
	  	</tr>
	  	<tr>
	    	<td class="Content_tab_style1"  >菜单备注:</td>
			<td class="Content_tab_style2"  >
				<textarea name="remark" rows="3" id="remark" style="width:300px;border: solid 1px gray;">${menu.remark }</textarea>
			</td>
	  	</tr>
	</table>
	
	<table align="center" >
   		<tr>
			<td align="center"  id="operateButton" >
				<button type="button" class="btn btn-info" id="btn-add" onclick="javascript:window.location.href='initTwoSysMenu_menurole.action?menuCode=${menu.menuCode }'"><i class="icon-white icon-plus-sign"></i>增加下级</button>
				<button type="button" class="btn btn-info" id="btnSubmit" ><i class="icon-white icon-edit"></i> 修改</button>
				<button type="button" class="btn btn-danger" id="btn-del" onclick="delSysMenu();"><i class="icon-white icon-trash"></i> 删除</button>
				   	
			</td>
		</tr>
    </table>
    
    
	</div>
</form>		
</body>
<script type="text/javascript">
var isupdate = "${menu.menuCode }";
var flag="${menu.menuFolderFlag}";
var act="${menu.isActive}";
var leve="${menu.menuLevel}";
if(leve=="")
{
	leve="0";
}
	if(isupdate != ""){
		$("#menuFolderFlag"+flag).attr("checked","checked");
		$("#isActive"+act).attr("checked","checked");
		$("#menuLevel"+leve).attr("checked","checked");
	}else{
		$("#menuFolderFlag0").attr("checked","checked");
		$("#isActive1").attr("checked","checked");
		$("#menuLevel0").attr("checked","checked");
	}
	
	function delSysMenu(){//删除功能组件
		
		var id="${menu.menuCode }";
		var isChild = "${menu.menuFolderFlag }";
		
		if(isChild==1){
			
			 $.dialog.confirm("温馨提示：您真的要删除此菜单项吗？此操作不可恢复！",function(){
			  var result = ajaxGeneral("deleteMenu_menurole.action","menuCode="+id);
				   showMsg('success',''+result+'',function(){
				   		parent.breakMe();							
	   					});
	   					
			   	 });				
		}else{
			showMsg("alert","请先删除子节点！");
		}
		
	}
$(function(){
	
	$("#btnTest").click(function(){
		$.ajax({
			url:"<%=path%>/findTest_menurole.action",
			type:"POST",
			success:function(result){
				showMsg("alert",result);
			},
			error:function(){
				showMsg("alert","eeeeee")
			}
		});
	});
	
	
	$("#btnSubmit").click(function(){
		var menuId = $("#txtMenuId").val();
		var menuCode = $("#txtMenuCode").val();
		var pmenuCode = $("#txtPmenuCode").val();
		var menuName = $("#txtMenuName").val();
		var menuFolderFlag = $("input:radio[name='menuFolderFlag']:checked").val();
		var sortNo = $("#txtSortNo").val();
		var pathCode = $("#txtPathCode").val();
		var isActive = $("input:radio[name='isActive']:checked").val();
		var remark = $("#remark").val();
		var tmpCode = $("#menuCode").val();
		var menuImg="${menu.menuImg}";
		var data = {
			menuId:menuId,menuCode:menuCode,pmenuCode:pmenuCode,menuName:menuName,
			menuFolderFlag:menuFolderFlag,sortNo:sortNo,pathCode:pathCode,isActive:isActive,
			remark:remark,tmpCode:tmpCode,menuImg:menuImg
		};
		if(menuName == ""){
			showMsg("alert","温馨提示:菜单名称不能为空！");
			return false;
		}else if(pathCode == ""){
			showMsg("alert","温馨提示:菜单路径不能为空！");
			return false;
		}else{
			
			$.dialog.confirm("温馨提示：你确定要修改菜单信息！",function(){
				  var result = ajaxGeneral("saveMenuData_menurole.action?prm=update",data,"text");
					  // showMsg('success',''+result+'',function(){
				   		if(result == "true"){
 							//showMsg("alert","温馨提示：菜单修改成功!");
 							 showMsg('success','温馨提示：菜单修改成功!',function(){
						   		parent.breakMe();
					   		});
	 						
						}else{
							showMsg("alert","温馨提示：菜单修改失败!");
						}							
	   					//});
			   	 	});
		}
	});
});
</script>
</html>