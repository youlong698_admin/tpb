<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加部门</title>
<script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">
	function checkPageValue( obj){
		var t=DwrService.ifExitCodeInTableComId("MaterialKind", "mkCode,isUsable", obj.value+",0",comId , set_view );	
	    if(t!=0){
		   showMsg("alert","类别编码已经存在，请重新输入") ;
		}
	}
	function set_view(count){
		if( count >0){
			document.getElementById("infoDsp").innerHTML="类别编码已存在,请重新输入!";
			document.getElementById("mkCode").focus();
			$('#submitButton').hide();
		}else{
			document.getElementById("infoDsp").innerHTML="";
			$('#submitButton').show();
		}
	}
	
	function doSubmit(){
			
			var mkName=document.getElementById("mkName").value;
			if(mkName=="" )
			{
				showMsg("alert","温馨提示：请完善表单信息！");
				return false ;
			}
				
				var parentCode = $('#parentCode').val();
				var newCode = $('#mkCode').val();
				if(newCode.indexOf(parentCode) != 0 || parentCode == newCode){
					showMsg("alert","新增类别编码请以父类别编码为前缀扩充！");
					return false;
				}
				
				var userIds=$("#userIds").val();
			    $("#userIds").val(userIds.substring(1, userIds.lastIndexOf(",")));
			    
				var action="saveMaterialKind_material.action";
				
			    $.dialog.confirm("温馨提示：确定要提交信息吗?",function(){
				    document.forms[0].action=action;
					document.forms[0].submit();
				});
			
	}
	
	function saveInit(){	
	   window.location.href = "saveMaterialKindInit_material.action";
	}
</script>
</head>

<body >
<form id="materialKind_save" name="deptadds" method="post" action="" >
<input name="materialKind.parentId" type="hidden" value='<s:property value="materialKind.mkId"/>' /> 
<input name="materialKind.isHaveChild" type="hidden" value='1' />
<input name="materialKind.isUsable" type="hidden" value='0' />
<input id="parentCode" type="hidden" value='<s:property value="materialKind.mkCode"/>' />
<input type="hidden" id="returnValues" name="returnValues" /> <!-- 采购员 -->
<input  id="userIds" type="hidden" name="userIds"/>	
<div class="Conter_main_conter" style="width: 70%"> 
<table class="table_ys1">
<tr align="center" >
    	<td  colspan="2" nowrap width="3%" class="Content_tab_style_05">采购类别下级增加</td>
    </tr>
  <tr>
    <td class="Content_tab_style1">上级类别名称:</td>
    <td width="75%"  class="Content_tab_style2"><s:property value="materialKind.mkName"/></td>
  </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1">类别编码：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialKind.materCode"/>
    <input readonly="readonly" name="materialKind.mkCode"  size="29" class="Content_input_style1"  type="hidden" id="mkCode" onblur="checkPageValue(this);" value="<s:property value="materialKind.materCode"/>"/>&nbsp;<font color="#FF0000">*<div id="infoDsp"></div></font></td>
  </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1">类别名称：</td>
    <td width="75%" class="Content_tab_style2"><input name="materialKind.mkName" size="29" class="Content_input_style1"  type="text" id="mkName"  value="" />&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font></td>
  </tr>
    
  <tr>
    <td height="25%" class="Content_tab_style1">排列序号：</td>
    <td width="75%"  class="Content_tab_style2"><input name="materialKind.displaySort" size="29" class="Content_input_style1"   type="text" id="deptOrder" value="" onkeyup="value=value.replace(/[^\d\.]/g,'');"/>&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2" align="center" >						   
  		<button type="button" class="btn btn-info" id="btn-edit" onclick="doSubmit();"><i class="icon-white icon-edit"></i> 保存</button>
  		<button type="button" class="btn btn-info" id="btn-save" onclick="saveInit()"><i class="icon-white icon-plus-sign"></i> 增加一级分类</button>
	</td>
  </tr>
 
  
</table>

</div>
</form> 
</body>
</html>

