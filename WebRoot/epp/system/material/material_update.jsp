<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> 修改物料名称</title>
<script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">
    var comId="${comId}";
	function checkPageValue( obj){
		if( obj.value == document.getElementById("oldCode").value ){
			document.getElementById("infoDsp").innerHTML="";
			return false ;	
		}
		var t=DwrService.ifExitCodeInTableComId("MaterialList", "materialCode,isUsable", obj.value+",0",comId, set_view );	
		if(t!=0){
		   showMsg("alert","编码已经存在，请重新输入") ;
		}
	}
	function set_view(count){
		//showMsg("alert", " in view ! " + count ) ;
		if( count >0){
			document.getElementById("infoDsp").innerHTML="编码已存在,请重新输入!";
			document.getElementById("materialCode").focus();
			$('#submitButton').hide();
		}else{
			document.getElementById("infoDsp").innerHTML="";
			$('#submitButton').show();
		}
	}
	
</script>
</head>

<body >
<form class="defaultForm" id="materialList_save" name="materialList_save" method="post" action="updateMaterial_material.action" >
<input id="parentCode" type="hidden" value="${materialKind.mkCode}" />
<input id="oldCode" type="hidden" value="${materialList.materialCode}" />
<input name="materialList.materialId" type="hidden" value="${materialList.materialId}" />
<input name="materialList.mkKindId" type="hidden" value="${materialList.mkKindId}" />
<input name="materialList.isUsable" type="hidden" value="${materialList.isUsable}" />
<input name="materialList.mkKindName" type="hidden" value="${materialList.mkKindName}" />
<input name="materialList.comId" type="hidden" value="${materialList.comId}" />

     <div class="Conter_main_conter">
<table class="table_ys1">
<tr align="center" >
    	<td height="24" colspan="2" nowrap width="3%" class="Content_tab_style_05">物料修改</td>
    </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1"><span class="title"></span>编码：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialList.materialCode"/>
    <input  readonly="readonly"  name="materialList.materialCode" value="${materialList.materialCode}" size="29" class="Content_input_style1"  type="hidden" id="materialCode" />&nbsp;<font color="#FF0000">*<div id="infoDsp"></div></font></td>
  </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1"><span class="title"></span>名称：</td>
    <td width="75%" class="Content_tab_style2">
    	<input name="materialList.materialName" value="${materialList.materialName}" size="29" class="Content_input_style1"  type="text" id="materialName" datatype="*" nullmsg="名称不能为空！"  />&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font>
    	<div class="info"><span class="Validform_checktip">名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
    </td>
  </tr>
  <tr>
    <td height="25%" class="Content_tab_style1">规格型号：</td>
    <td width="75%" class="Content_tab_style2"><textarea name="materialList.materialType" class="Content_input_style2" onpropertychange='this.style.posHeight=this.scrollHeight' cols="72%" rows="4"  id="materialType" >${materialList.materialType}</textarea>&nbsp;</td>
  </tr>
  <tr>
    <td height="25%" class="Content_tab_style1">计量单位：</td>
    <td width="75%" class="Content_tab_style2">
      <select name="materialList.unit">
        <c:forEach var="dictionary" items="${dictionaryList}">
           <option value="${dictionary.dictName }" <c:if test="${materialList.unit==dictionary.dictName }">selected</c:if>>${dictionary.dictName }</option>
        </c:forEach>
      </select>
     </td>
  </tr>  
  <tr>
    <td height="25%" class="Content_tab_style1">备注：</td>
    <td width="75%" class="Content_tab_style2"><textarea name="materialList.remark" class="Content_input_style2"  onpropertychange='this.style.posHeight=this.scrollHeight' cols="72%" rows="4" id="remark">${materialList.remark}</textarea></td>
  </tr>
</table>
		<div class="buttonDiv">
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				<button class="btn btn-cacel" id="btn-cacel" type="button" onclick="javascript:window.location.href='viewMaterialList_material.action?mkKindId=${materialKind.mkId}';"><i class="icon icon-repeat"></i>返回</button>
			</div>
</div>
</form> 
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html>

