<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"  src="<%= path %>/common/script/context.js" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	function doSubmit(type){   
		var action;
		var mkId=document.getElementById("mkId").value;
		var mkName=document.getElementById("mkName").value;
		var mkCode=document.getElementById("mkCode").value;
		if(mkName=="" || mkCode=="")
		{
			showMsg("alert","温馨提示：请完善表单信息！");
			return false ;
		}
		if(type=="save"){
			location.href="saveMaterialKindInit_material.action?materialKind.mkId="+mkId;
		}
		if(type=="update"){
	        var userIds=$("#userIds").val();
		    $("#userIds").val(userIds.substring(1, userIds.lastIndexOf(",")));
		    
			    
			//if (doValidate('deptadds')){
			$.dialog.confirm("温馨提示：确定要提交信息吗?",function(){
				  action="updateMaterialKind_material.action";
				  document.forms[0].action=action;
				  document.forms[0].submit();
			},function(){return false;});
		}
		if(type=="delete"){
			
			$.dialog.confirm("温馨提示：确定要删除选中的信息?",function(){
				  var mesg = ajaxGeneral("delMaterialKind_material.action","materialKind.mkId="+mkId,"text");			 
				  window.parent.LeftFrame.refreshParentNode() ;
				  window.parent.MainFrame.location.href="updateMaterialKindInit_material.action";	
			},function(){return false;});
		}
		
    }
</script>
</head>

<body >
<form  method="post" action="" id="materialKind_update" name="materialKind_update"> 
<input name="materialKind.mkId" id="mkId" type="hidden" value="${materialKind.mkId}" />  
<input name="materialKind.parentId" type="hidden" value="${materialKind.parentId}" />
<input name="materialKind.isHaveChild" type="hidden" value="${materialKind.isHaveChild}" />
<input name="materialKind.selflevCode" type="hidden" value="${materialKind.selflevCode}" />
<input name="materialKind.levels" type="hidden" value="${materialKind.levels}" />
<input name="materialKind.writer" type="hidden" value="${materialKind.writer}" />
<input name="materialKind.writeDate" type="hidden" value="${materialKind.writeDate}" />
<input name="oldCode" type="hidden" value="${materialKind.mkCode}" />
<input name="materialKind.isUsable" type="hidden" value="${materialKind.isUsable}" />
<input name="materialKind.comId" type="hidden" value="${materialKind.comId}" />
<input type="hidden" id="returnValues" name="returnValues" value="${returnValues }"/> <!-- 采购员 -->
<input  id="userIds" type="hidden" name="userIds"/>	
<div class="Conter_main_conter" style="width: 70%">
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="4" class="Content_tab_style_05">采购类别修改</td>
		    </tr>
		    <tr>
			    <td width="30%" class="Content_tab_style1" >类别编码：</td>
			    <td width="70%" class="Content_tab_style2">
			    	<input type="text"  readonly="readonly" id="mkCode"  size="29" name="materialKind.mkCode" value="${materialKind.mkCode}"/>
			    </td>
		    </tr>
		    <tr>
			    <td width="15%" class="Content_tab_style1">类别名称：</td>
			    <td width="35%" class="Content_tab_style2">
			    <input type="text" size="29"   id="mkName" name="materialKind.mkName" value="${materialKind.mkName}"/><font color="#ff0000">*</font></td>
		    </tr>
		    
		   	<tr>
			    <td width="15%" class="Content_tab_style1">排列序号：${materialKind.levels }</td>
			    <td width="35%" class="Content_tab_style2">
			   	 <input name="materialKind.displaySort" size="29"  type="text" id="displaySort" value="${materialKind.displaySort}" onkeyup="value=value.replace(/[^\d\.]/g,'');" />
			    </td>
		   	</tr>
		  	<tr >
	            <td colspan="2" id="operateid" align="center" >
	            	<s:if test="materialKind.levels!=3">
	            	<button type="button" class="btn btn-info" id="btn-add" <s:if test="materialKind.isUsable == '1'">disabled</s:if> onclick=" doSubmit('save');" ><i class="icon-white icon-plus-sign"></i> 增加下级</button>
					</s:if>					
					<button type="button" class="btn btn-info" id="btn-edit" onclick="doSubmit('update');"><i class="icon-white icon-edit"></i> 保存</button>
					</td>
			</tr>
   		</table> 
</div>
</form>
</body>
</html>

