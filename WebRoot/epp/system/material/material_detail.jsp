<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> 查看物料名称</title>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script language="javaScript">	
	var api = frameElement.api, W = api.opener;	
</script>
</head>

<body >
<form id="materialList_save" name="materialList_save" method="post" action="" >
<input id="parentCode" type="hidden" value='<s:property value="materialKind.mkCode"/>' />
<input id="oldCode" type="hidden" value='<s:property value="materialList.materialCode"/>' />
<input name="materialList.materialId" type="hidden" value='<s:property value="materialList.materialId"/>' />
<input name="materialList.mkKindId" type="hidden" value='<s:property value="materialList.mkKindId"/>' />
<input name="materialList.isUsable" type="hidden" value='<s:property value="materialList.isUsable"/>' />

     <div class="Conter_main_conter">
<table class="table_ys1">
<tr align="center" >
    	<td  colspan="2" nowrap width="3%" class="Content_tab_style_05"><span class="title"></span>名称详细信息</td>
    </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1"><span class="title"></span>编码：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialList.materialCode"/></td>
  </tr>
  
  <tr>
    <td height="25%" class="Content_tab_style1"><span class="title"></span>名称：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialList.materialName"/></td>
  </tr>
  <tr>
    <td height="25%" class="Content_tab_style1">规格型号：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialList.materialType"/></td>
  </tr>
  <tr>
    <td height="25%" class="Content_tab_style1">计量单位：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialList.unit"/></td>
  </tr>
  <tr>
    <td height="25%" class="Content_tab_style1">最新采购单价：</td>
    <td width="75%" class="Content_tab_style2"><s:property value="materialList.price"/></td>
  </tr>
 
  
</table>

</div>
</form> 
</body>
</html>

