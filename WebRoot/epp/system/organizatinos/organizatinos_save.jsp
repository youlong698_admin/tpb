<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加群组</title>
<script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
 <script type="text/javascript">
        var comId="${comId}";
		$(function (){
		
			//返回信息
		   <c:if test="${message!=null}">
			   showMsg('success','${message}',function(){
			   		parent.LeftFrame.location.href="viewOrganizatinosInitTree_org.action";
				
		   		});
		    </c:if>
		    
		 //移除供应商
		$('#deptChosen .search-choice').live('click',function(){
			var that=$(this);
			var depId=that.attr('dept-id');
			var depIds=$("#deptId").val();
		
			$("#deptId").val(depIds.replace(","+depId+",",","));
			that.remove();
		});
	});
	function set_view(count){
		//showMsg("alert", " in view ! " + count ) ;
		if( count >0){
			document.getElementById("infoDsp").innerHTML="群组代码已存在,请重新输入!";
			document.getElementById("orgCode").focus();
			$('#submitButton').hide();
		}else{
			document.getElementById("infoDsp").innerHTML="";
			$('#submitButton').show();
		}
	}
	
	function checkorgname( obj){
		//showMsg("alert"," value : " + obj.value) ;
		DwrService.validOrgName(obj.value, set_views);	
	}
	function doSubmit(){
			if( doValidate( document.forms[0] ) ) {
				var action="saveOrganizatinos_org.action";
				document.forms[0].action=action;
				document.forms[0].submit();
				
				return true;
				
			}else {
				return false ;
			}
			
	}
//选择部门权限信息
function selectdept(){
	var deptId=document.getElementById("deptId").value;
	//var orgId=document.getElementById("orgId").value;
	 createdetailwindow_choose("选择部门","viewOrganizatinos_InitTree_org.action?orgId=${organizatinos.orgId}&deptId="+deptId,2);
	
}
function valuedept(){
	var users=$("#returnValues").val();
	 $("#deptChosen").html("");
	 document.getElementById("deptId").value="";
	 
	if(users!=null && users!=""){
		var str=",";
		var std=",";
		var ur = users.split(",");
		for(var i=0;i<ur.length;i++){
			var u = ur[i].split(":");
			str += u[0]+",";     
			std += u[1]+",";
			$('<li class="search-choice" dept-id="'+u[0]+'"><span>'+u[1]+'</span><a class="search-choice-close" ></a></li>')
			.appendTo('#deptChosen');
		}
		document.getElementById("deptId").value=str;	
	}
}
</script>
</head>

<body >
<form class="defaultForm" id="organizatinos_save" name="orgadds" method="post" action="saveOrganizatinos_org.action" >
<input name="organizatinos.parentOrgId" type="hidden" value='<s:property value="organizatinos.orgId"/>' /> 
<input name="organizatinos.isHaveChild" type="hidden" value='1' />
<input name="organizatinos.isUsable" type="hidden" value='0' />

<input type="hidden" id="deptId" name="deptId"/>
<input type="hidden" id="returnValues" name="returnValues" value="${returnValues }"/>

<!-- 防止表单重复提交 -->
<s:token/>


<div class="Conter_main_conter" style="width: 90%">
<table class="table_ys1">
<tr align="center" >
    	<td height="24" colspan="2" nowrap width="3%" class="Content_tab_style_05">群组管理下级增加</td>
    </tr>
  <tr>
    <td width="30%" class="Content_tab_style1">上级群组名称：</td>
    <td width="70%"  class="Content_tab_style2"><s:property value="organizatinos.orgName"/><input type="hidden" readonly   value="<s:property value="organizatinos.orgName"/>" /></td>
  </tr>
  
  <tr>
    <td  class="Content_tab_style1">群组名称：</td>
    <td  class="Content_tab_style2">
    	<input name="organizatinos.orgName" size="29" datatype="*" nullmsg="群组名称不能为空！"  class="Content_input_style1"  type="text" id="orgName"  value="" />&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font>
    	<div class="info"><span class="Validform_checktip">群组名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
	</td>
  </tr>
  <tr>
    <td  class="Content_tab_style1">排列序号：</td>
    <td   class="Content_tab_style2">
    	<input name="organizatinos.orgOrder" size="29" datatype="/^\s*$/ |n" errormsg="排列序号必须是数字类型！" class="Content_input_style1"   type="text" id="orgOrder" value="" />&nbsp;
    	<div class="info"><span class="Validform_checktip">排列序号必须是数字类型！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
	</td>
  </tr>
  <tr>
    <td class="Content_tab_style1">权限部门：</td>
    <td class="Content_tab_style2">
    <textarea  name="" style="display:none;" id="depeName" cols="24%" rows="4" ></textarea>
    <div class="chosen-container-multi">
		<ul class="chosen-choices" id="deptChosen">
		</ul>
	</div>
	<div id="chosen-img">
	&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择部门"  onclick="selectdept();"/>
   	</div>
    </td>
  </tr>
  <tr>
    <td  class="Content_tab_style1">群组描述：</td>
    <td   class="Content_tab_style2"><textarea name="organizatinos.orgDesc" class="Content_input_style2" cols="24%" rows="4"  id="orgDesc" ></textarea></td>
  </tr>
  <tr>
  	<td colspan="2" align="center" >
  		<button class="btn btn-info" id="submitButton"><i class="icon-white icon-plus-sign"></i>保存</button>
		<button type="button" class="btn btn-cacel" id="btn-edit"  onclick="goBack();"><i class="icon icon-repeat"></i> 返回</button>
		
  	</td>
  </tr>
 
  
</table>

</div>
</form> 
<script type="text/javascript">
	function goBack(){
		var num = <s:property value="organizatinos.orgId"/>;
		location.href = "updateOrganizatinosInit_org.action?organizatinos.orgId="+num;
		
	}
</script>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#submitButton", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			
			doSubmit();
			return false;
			
			
		}
	});
})
</script> 
</body>
</html>

