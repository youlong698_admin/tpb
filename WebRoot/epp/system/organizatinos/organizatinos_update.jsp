<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"  src="<%= path %>/common/script/context.js" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function (){
	   //移除部门
		$('#deptChosen .search-choice').live('click',function(){
			var that=$(this);
			var depId=that.attr('dept-id');
			var depIds=$("#deptId").val();
			
			$("#deptId").val(depIds.replace(","+depId+",",","));
			that.remove();
		});
	});
		function doSubmit(type){   
		var action;
		if(type=="save"){
			action="saveOrganizatinosInit_org.action";
			document.forms[0].action=action;
       		document.forms[0].submit();
		}
		if(type=="update"){
			
			$.dialog.confirm("温馨提示：确定要提交修改信息吗！",function(){
				  action="updateOrganizatinos_org.action";
				  document.forms[0].action=action;
			      document.forms[0].submit();
			     });
		}
		if(type=="delete"){
			$.dialog.confirm("温馨提示：你确定要删除选中的群组！",function(){
				 var mesg = ajaxGeneral("delOrganizatinos_org.action",$('#organizatinos_update').serialize(),"text");
				 window.parent.LeftFrame.refreshParentNode() ;
				 window.parent.MainFrame.location.href="updateOrganizatinosInit_org.action";
			    
			});
				
		}
		
    }
 //选择部门权限信息
function selectdept(){
	var deptId=document.getElementById("deptId").value;
	 createdetailwindow_choose("选择部门","viewOrganizatinos_InitTree_org.action?orgId=${organizatinos.orgId}&deptId="+deptId,2);
	
}
function valuedept(){
	var users=$("#returnValues").val();
	 $("#deptChosen").html("");
	 document.getElementById("deptId").value="";
	 
	if(users!=null && users!=""){
		var str=",";
		var std=",";
		var ur = users.split(",");
		for(var i=0;i<ur.length;i++){
			var u = ur[i].split(":");
			str += u[0]+",";     
			std += u[1]+",";
			$('<li class="search-choice" dept-id="'+u[0]+'"><span>'+u[1]+'</span><a class="search-choice-close" ></a></li>')
			.appendTo('#deptChosen');
		}
		document.getElementById("deptId").value=str;	
	}
}
</script>
</head>

<body onload="valuedept();">
<form  method="post" class="defaultForm" action="" id="organizatinos_update" name="orgadds"> 
<input name="organizatinos.orgId" type="hidden" id="orgId" value="${organizatinos.orgId}" />  
<input name="organizatinos.parentOrgId" type="hidden" value="${organizatinos.parentOrgId}"/>
<input name="organizatinos.isHaveChild" type="hidden" value="${organizatinos.isHaveChild}"/>
<input name="organizatinos.selflevCode" type="hidden" value="${organizatinos.selflevCode}"/>
<input name="organizatinos.orgLevel" type="hidden" value="${organizatinos.orgLevel}"/>
<input name="organizatinos.writeDate" type="hidden" value="${organizatinos.writeDate}"/>
<input name="organizatinos.writer" type="hidden" value="${organizatinos.writer}"/>
<input name="oldCode" type="hidden" value="${organizatinos.orgCode}"/>
<input name="organizatinos.isUsable" type="hidden" value="${organizatinos.isUsable}"/>
<input name="organizatinos.comId" type="hidden" value="${organizatinos.comId}"/>

<input type="hidden" id="deptId" name="deptId"/>
<input type="hidden" id="returnValues" name="returnValues" value="${returnValues }"/>
<!-- 防止表单重复提交 -->
<s:token/>

 <div class="Conter_main_conter" style="width: 90%">
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">群组管理修改</td>
		    </tr>
		   <tr>
			    <td  class="Content_tab_style1">群组名称：</td>
			    <td class="Content_tab_style2">
			    <input type="text" size="29" readonly style="border:none;" name="organizatinos.orgName" value="${organizatinos.orgName}"/><font color="#ff0000">*</font></td>
		    </tr>		    
		   	<tr>
			    <td  class="Content_tab_style1">排列序号：</td>
			    <td class="Content_tab_style2">
			   	 <input name="organizatinos.orgOrder" size="29" datatype="/^\s*$/ |n" errormsg="排列序号必须是数字类型！"  type="text" id="orgOrder" value="${organizatinos.orgOrder}" />
			    	<div class="info"><span class="Validform_checktip">排列序号必须是数字类型！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
		   	</tr>
		   	<tr>
			    <td  class="Content_tab_style1">权限部门：</td>
			    <td class="Content_tab_style2">
			    <textarea  name="" style="display:none;" id="depeName" cols="24%" rows="4" ></textarea>
			    <div class="chosen-container-multi">
					<ul class="chosen-choices" id="deptChosen">
					</ul>
				</div>
				<div id="chosen-img">
				&nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择部门"  onclick="selectdept();"/>
			   	</div>
			    </td>
		  	</tr>
		    <tr>
			    <td  class="Content_tab_style1">群组描述：</td>
			    <td class="Content_tab_style2">
			    <textarea  name="organizatinos.orgDesc" cols="24%" rows="4" style="resize: none" class="Content_input_style2" >${organizatinos.orgDesc}</textarea>
			    
			    </td>
		  	</tr>
		  	
		  	<tr >
	            <td colspan="2" id="operateid" align="center" >
					<button type="button" class="btn btn-info" id="btn-add" <c:if test="${organizatinos.isUsable == '1'}">disabled</c:if> onclick=" doSubmit('save');"><i class="icon-white icon-plus-sign"></i>增加下级</button>
					<button class="btn btn-info" id="btn-edit" <c:if test="${organizatinos.orgId == 1}">disabled</c:if> ><i class="icon-white icon-edit"></i> 修改</button>
					<button type="button" class="btn btn-danger" id="btn-del" <c:if test="${organizatinos.orgId == 1}">disabled</c:if> onclick="doSubmit('delete');"><i class="icon-white icon-trash"></i> 删除</button>
				   
				</td>
			</tr>
   		</table>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			
			doSubmit('update');
			
			return false;
			
			
		}
	});
})
</script>
</body>
</html>

