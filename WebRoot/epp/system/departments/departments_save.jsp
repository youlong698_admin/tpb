<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>增加部门</title>
<script type="text/javascript" src="<%=path %>/dwr/interface/DwrService.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/engine.js"></script>
<script type="text/javascript" src="<%=path %>/dwr/util.js"></script>
<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<script language="javaScript">
    var comId="${comId}";
	
	function checkdeptname( obj){	
		DwrService.ifExitCodeInTableComId("Departments", "deptName,isUsable", obj.value+",0",comId, set_views);	
	}
	function set_views(count){
		if( count >0){
			document.getElementById("infoDsps").innerHTML="部门名称已存在,请重新输入!";
			document.getElementById("deptName").focus();
		}else{
			document.getElementById("infoDsps").innerHTML="";
		}
	}
	function doSubmit(){
			var action="saveDepartments_dept.action";
			document.forms[0].action=action;
		   
			document.forms[0].submit();
			//parent.LeftFrame.location.href="viewDepartmentsInitTree_dept.action";
			//parent.MainFrame.location.href="updateDepartmentsInit_dept.action?departments.depId=1";
		  
			return true;
			
	}
</script>
</head>

<body >
<form class="defaultForm" id="departments_save" name="deptadds" method="post" action="saveDepartments_dept.action" >
<input name="departments.parentDeptId" type="hidden" value='${departments.depId }' /> 
<input name="departments.isHaveChild" type="hidden" value='1' />
<input name="departments.isUsable" type="hidden" value='0' />
<div class="Conter_main_conter" style="width: 90%">
<table class="table_ys1">
<tr align="center" >
    	<td height="24" colspan="2" class="Content_tab_style_05">部门管理下级增加</td>
    </tr>
  <tr>
    <td class="Content_tab_style1">上级部门名称：</td>
    <td width="70%"  class="Content_tab_style2">${departments.deptName }<input type="hidden" readonly   value="${departments.deptName}" /></td>
  </tr>
  
  <tr>
    <td  class="Content_tab_style1">部门名称：</td>
    <td  class="Content_tab_style2">
    	<input name="departments.deptName" size="29" datatype="*" nullmsg="部门名称不能为空！" class="Content_input_style1"  type="text" id="deptName" onblur="checkdeptname(this);" value="" />&nbsp;<font color="#FF0000">*<div id="infoDsps"></div></font>
   		 <div class="info"><span class="Validform_checktip">部门名称不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
	</td>
  </tr>
  <tr>
    <td  class="Content_tab_style1">排列序号：</td>
    <td   class="Content_tab_style2">
    	<input name="departments.deptOrder" size="29" datatype="/^\s*$/ |n" errormsg="排列序号必须是数字类型！" class="Content_input_style1"   type="text" id="deptOrder" value="" />&nbsp;
    	<div class="info"><span class="Validform_checktip">排列序号必须是数字类型！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
	</td>
  </tr>  
  <tr>
    <td  class="Content_tab_style1">是否为采购组织：</td>
    <td class="Content_tab_style2">
    <input name="departments.isPurchaseDept" type="radio" value="0"/>是&nbsp;&nbsp;<input name="departments.isPurchaseDept" type="radio" value="1" checked/>否&nbsp;
    </td>
  </tr>
    
  <tr id="purchaseTr">
    <td  class="Content_tab_style1">集采所属采购组织：</td>
    <td class="Content_tab_style2">
	    <select name="departments.purchaseDeptId">
	      <c:forEach var="dept" items="${list_purchase}">
	        <option value="${dept.depId }">${dept.deptName }</option>
	      </c:forEach>
	    </select>
    </td>
  </tr>
  <tr>
    <td  class="Content_tab_style1">自采所属采购组织：</td>
    <td class="Content_tab_style2">
	    <select name="departments.ownPurchaseDeptId">
	         <option value="0">本部门自采</option>
	      <c:forEach var="dept" items="${list_purchase}">
	        <option value="${dept.depId }">${dept.deptName }</option>
	      </c:forEach>
	    </select>
    </td>
  </tr>
  <tr>
    <td  class="Content_tab_style1">部门描述：</td>
    <td class="Content_tab_style2"><textarea name="departments.deptDesc" class="Content_input_style2" cols="24%" rows="4"  id="deptDesc" ></textarea></td>
  </tr>
  <tr>
  	<td colspan="2" align="center" >
  		<button class="btn btn-info" id="submitButton" ><i class="icon-white icon-plus-sign"></i>保存</button>
		<button type="button" class="btn btn-cacel" id="btn-edit"  onclick="goBack();"><i class="icon icon-repeat"></i> 返回</button>
					
  	</td>
  </tr>
 
  
</table>

</div>
</form> 
<script type="text/javascript">
	function goBack(){
		var num = <s:property value="departments.depId"/>;
		location.href = "updateDepartmentsInit_dept.action?departments.depId="+num;
		
	}
</script>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#submitButton", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			
			doSubmit();
			return false;
			
			
		}
	});
})
</script>  
</body>
</html>

