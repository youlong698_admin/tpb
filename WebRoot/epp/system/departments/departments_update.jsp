<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"  src="<%= path %>/common/script/context.js" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
 <script type="text/javascript">
	$(function (){	
		//返回信息
	   <c:if test="${message!=null}">
		   showMsg('success','${message}',function(){
		   		//showMsg("alert",);
	   		});
	    </c:if>	    
	});

	function doSubmit(type){   
		var action;
		if(type=="save"){
			 action="saveDepartmentsInit_dept.action";
			 document.forms[0].action=action;
       		 document.forms[0].submit();
		}
		if(type=="update"){
			
			$.dialog.confirm("温馨提示：确定要提交修改信息吗！",function(){
			      action="updateDepartments_dept.action";
				  document.forms[0].action=action;
	       		  document.forms[0].submit();
	       		  });
		}
		if(type=="delete"){
			$.dialog.confirm("温馨提示：你确定要删除选中的部门！",function(){
				 var mesg = ajaxGeneral("delDepartments_dept.action",$('#departments_update').serialize(),"text");
				 window.parent.LeftFrame.refreshParentNode() ;
		         window.parent.MainFrame.location.href = "updateDepartmentsInit_dept.action";
			  });  
		}
		
    }
        var selectType,selectTitle;
    	function selectUser(type){
    	 selectType=type;
    	 if(selectType=="depLeaderId"){
    	    selectTitle="选择部门领导";
    	 }else{
    	    selectTitle="选择分管领导";
    	 }
 		 createdetailwindow_choose(selectTitle,"viewDeptIndex_userTree.action?deType=user",1);
	    }
 		function valueEvaluateUser(){
 		var s=$("#returnValues").val();
 			if(s!=""){
				 	var ur = s.split(":");
				 	if(selectType=="depLeaderId"){
				 		document.getElementById("addOpTaskName").value =ur[1];
				 		document.getElementById("addOpTaskValue").value =ur[0];
				 	}else{
				 	    document.getElementById("addOpName").value =ur[1];
				 		document.getElementById("addOpValue").value =ur[0];
				 	}				 
			}
 
 		}
 		
	
</script>
</head>

<body >
<form class="defaultForm" method="post" action="" id="departments_update" name="deptadds"> 
<input name="departments.depId" type="hidden" value="${departments.depId }" />  
<input name="departments.parentDeptId" type="hidden" value="${departments.parentDeptId}"  />
<input name="departments.isHaveChild" type="hidden" value="${departments.isHaveChild}"  />
<input name="departments.selflevCode" type="hidden" value="${departments.selflevCode}"  />
<input name="departments.deptLevel" type="hidden" value="${departments.deptLevel}"  />
<input name="departments.writer" type="hidden" value="${departments.writer}"  />
<input name="departments.WriteDate" type="hidden" value="${departments.writeDate}"  />
<input name="oldCode" type="hidden" value="${departments.deptCode}"  />
<input name="departments.isUsable" type="hidden" value="${departments.isUsable}"  />
<input name="departments.comId" type="hidden" value="${departments.comId}"  />
<c:if test="${departments.parentDeptId==0}">  
<input name="departments.isPurchaseDept" type="hidden" value="${departments.isPurchaseDept}"  />
<input name="departments.purchaseDeptId" type="hidden" value="${departments.purchaseDeptId}"  />
</c:if>

<input type="hidden" id="returnValues" name="returnValues" value=""/>
<div class="Conter_main_conter" style="width: 90%">
    	<!-- 基本信息  begin-->
    	<table class="table_ys1">
        	<tr align="center" >
    			<td colspan="2" class="Content_tab_style_05">部门管理修改</td>
		    </tr>
		    <tr>
			    <td  class="Content_tab_style1">部门名称：</td>
			    <td  class="Content_tab_style2">
			    <input type="text" size="29" readonly style="border:none;" name="departments.deptName" value="${departments.deptName}"/><font color="#ff0000">*</font></td>
		    </tr>
		     <tr>
			    <td  class="Content_tab_style1">部门领导：</td>
			    <td  class="Content_tab_style2">
			    <input size="29"  type="text" id="addOpTaskName" name ="departments.depLeaderCn"  readonly="readonly" class="Wsearch2" value="${departments.depLeaderCn}"  />
			    <input type ="hidden" id ="addOpTaskValue" name ="departments.depLeaderId"  value="${departments.depLeaderId}" />
			    &nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择部门领导" onclick="selectUser('depLeaderId')"  />
			    </td>
		   	</tr>
		    <tr>
			    <td  class="Content_tab_style1">分管领导：</td>
			    <td  class="Content_tab_style2">
			    <input size="29"  type="text" id="addOpName" name ="departments.compLeaderCn"  readonly="readonly" class="Wsearch2" value="${departments.compLeaderCn}"  />
			    <input type ="hidden" id ="addOpValue" name ="departments.compLeaderId"  value="${departments.compLeaderId}" />		
			     &nbsp;&nbsp;<img src="<%=basePath %>/images/select.gif" title="选择分管领导" onclick="selectUser('compLeaderId')"  />
			    </td>
		   	</tr>
		   	<tr>
			    <td  class="Content_tab_style1">排列序号：</td>
			    <td  class="Content_tab_style2">
			   	 <input name="departments.deptOrder" size="29" datatype="/^\s*$/ |n" errormsg="排列序号必须是数字类型！"  type="text" id="deptOrder" value="${departments.deptOrder}" />
			    	<div class="info"><span class="Validform_checktip">排列序号必须是数字类型！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				
			    </td>
		   	</tr>	
		   	<c:if test="${departments.parentDeptId!=0}">  	  	  
			<tr>
			  <td height="25%" class="Content_tab_style1">是否为采购组织：</td>
			  <td width="75%"  class="Content_tab_style2">
			  <input name="departments.isPurchaseDept" type="radio" value="0" <c:if test="${departments.isPurchaseDept=='0' }"> checked </c:if>/>是&nbsp;&nbsp;
			  <input name="departments.isPurchaseDept" type="radio" value="1" <c:if test="${departments.isPurchaseDept=='1' }"> checked </c:if>/>否&nbsp;
			  </td>
			</tr>			  
			<tr id="purchaseTr">
			  <td height="25%" class="Content_tab_style1">集采所属采购组织：</td>
			  <td width="75%"  class="Content_tab_style2">
			   <select name="departments.purchaseDeptId">
			     <c:forEach var="dept" items="${list_purchase}">
			      <option value="${dept.depId }" <c:if test="${dept.depId==departments.purchaseDeptId }">selected="selected"</c:if>>${dept.deptName }</option>
			    </c:forEach>
			   </select>
			  </td>
			</tr>  
			<tr>
			  <td height="25%" class="Content_tab_style1">自采所属采购组织：</td>
			  <td width="75%"  class="Content_tab_style2">
			   <select name="departments.ownPurchaseDeptId">
			     <option value="${departments.depId }" <c:if test="${departments.depId==departments.ownPurchaseDeptId }">selected="selected"</c:if>>本部门自采</option>
			     <c:forEach var="dept" items="${list_purchase}">
			      <option value="${dept.depId }" <c:if test="${dept.depId==departments.ownPurchaseDeptId }">selected="selected"</c:if>>${dept.deptName }</option>
			    </c:forEach>
			   </select>
			  </td>
			</tr>
			</c:if>   
		    <tr>
			    <td  class="Content_tab_style1">部门描述：</td>
			    <td  class="Content_tab_style2">
			    <textarea  name="departments.deptDesc" cols="24%" rows="4" style="resize: none" class="Content_input_style2" >${departments.deptDesc}</textarea>
			    
			    </td>
		  	</tr>
		  	<tr >
	            <td colspan="2" id="operateid" align="center" >
					<button type="button" class="btn btn-info" id="btn-add" <s:if test="departments.isUsable == '1'">disabled</s:if> onclick=" doSubmit('save');"><i class="icon-white icon-plus-sign"></i> 增加下级</button>
					<button class="btn btn-info" id="btn-edit" <s:if test="departments.depId == 1">disabled</s:if> ><i class="icon-white icon-edit"></i> 修改</button>
					<button type="button" class="btn btn-danger" id="btn-del" <s:if test="departments.depId == 1">disabled</s:if> onclick="doSubmit('delete');"><i class="icon-white icon-trash"></i> 删除</button>
				</td>
			</tr>
   		</table> 
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-edit", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		},
		beforeSubmit:function(curform){
			
			doSubmit('update');
			
			return false;
			
			
		}
	});
})
</script>
</body>
</html>

