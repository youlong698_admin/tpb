<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
<title>系统配置表</title>
<script src="<%=path %>/common/script/context.js" type="text/javascript" ></script>
<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/common/switch/css/honeySwitch.css" />
<script src="<%=path%>/common/switch/js/honeySwitch.js" type="text/javascript"></script>
<script src="<%=path%>/common/jQuery/uploadPreview.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=path%>/common/jqueryCity/jquery.cityselect.js"></script> 
<script type="text/javascript">
   $(function(){
        <c:if test="${systemConfiguration.mailFalg=='0'}">
          honeySwitch.showOn("#mailFalg1");
        </c:if>   
		switchEvent("#mailFalg1",function(){
			$("#mailFalg").val(0);
		},function(){
			$("#mailFalg").val(1);
		});
		
        <c:if test="${systemConfiguration.messageFalg=='0'}">
          honeySwitch.showOn("#messageFalg1");
        </c:if>
		switchEvent("#messageFalg1",function(){
			$("#messageFalg").val(0);
		},function(){
			$("#messageFalg").val(1);
		});
		     
        <c:if test="${systemConfiguration.requiredMaterialWorkflow=='0'}">
          honeySwitch.showOn("#requiredMaterialWorkflow1");
        </c:if>
		switchEvent("#requiredMaterialWorkflow1",function(){
			$("#requiredMaterialWorkflow").val(0);
		},function(){
			$("#requiredMaterialWorkflow").val(1);
		});
		
		<c:if test="${systemConfiguration.requiredcollect00Workflow=='0'}">
          honeySwitch.showOn("#requiredcollect00Workflow1");
        </c:if>
		switchEvent("#requiredcollect00Workflow1",function(){
			$("#requiredcollect00Workflow").val(0);
		},function(){
			$("#requiredcollect00Workflow").val(1);
		});
		
		<c:if test="${systemConfiguration.bidpurchaseresult00Workflow=='0'}">
          honeySwitch.showOn("#bidpurchaseresult00Workflow1");
        </c:if>
		switchEvent("#bidpurchaseresult00Workflow1",function(){
			$("#bidpurchaseresult00Workflow").val(0);
		},function(){
			$("#bidpurchaseresult00Workflow").val(1);
		});
		
		<c:if test="${systemConfiguration.requiredcollect01Workflow=='0'}">
          honeySwitch.showOn("#requiredcollect01Workflow1");
        </c:if>
        switchEvent("#requiredcollect01Workflow1",function(){
			$("#requiredcollect01Workflow").val(0);
		},function(){
			$("#requiredcollect01Workflow").val(1);
		});
		
		<c:if test="${systemConfiguration.bidpurchaseresult01Workflow=='0'}">
          honeySwitch.showOn("#bidpurchaseresult01Workflow1");
        </c:if>
		switchEvent("#bidpurchaseresult01Workflow1",function(){
			$("#bidpurchaseresult01Workflow").val(0);
		},function(){
			$("#bidpurchaseresult01Workflow").val(1);
		});
		
		<c:if test="${systemConfiguration.requiredcollect02Workflow=='0'}">
          honeySwitch.showOn("#requiredcollect02Workflow1");
        </c:if>
		switchEvent("#requiredcollect02Workflow1",function(){
			$("#requiredcollect02Workflow").val(0);
		},function(){
			$("#requiredcollect02Workflow").val(1);
		});
		
		<c:if test="${systemConfiguration.bidpurchaseresult02Workflow=='0'}">
          honeySwitch.showOn("#bidpurchaseresult02Workflow1");
        </c:if>
		switchEvent("#bidpurchaseresult02Workflow1",function(){
			$("#bidpurchaseresult02Workflow").val(0);
		},function(){
			$("#bidpurchaseresult02Workflow").val(1);
		});
		
		<c:if test="${systemConfiguration.contractWorkflow=='0'}">
          honeySwitch.showOn("#contractWorkflow1");
        </c:if>
		switchEvent("#contractWorkflow1",function(){
			$("#contractWorkflow").val(0);
		},function(){
			$("#contractWorkflow").val(1);
		});
		
		<c:if test="${systemConfiguration.orderWorkflow=='0'}">
          honeySwitch.showOn("#orderWorkflow1");
        </c:if>
		switchEvent("#orderWorkflow1",function(){
			$("#orderWorkflow").val(0);
		},function(){
			$("#orderWorkflow").val(1);
		});
		
		<c:if test="${systemConfiguration.capitalPlanWorkflow=='0'}">
          honeySwitch.showOn("#capitalPlanWorkflow1");
        </c:if>
		switchEvent("#capitalPlanWorkflow1",function(){
			$("#capitalPlanWorkflow").val(0);
		},function(){
			$("#capitalPlanWorkflow").val(1);
		});
		
		 $("#logo").uploadPreview({ width: 220, height: 80, imgDiv: "#imgDiv"});
		 <c:if test="${empty sysCompany.province}">
		  $("#city").citySelect({prov:"北京",city:"东城区"}); 
		 </c:if>=
		 <c:if test"${not empty sysCompany.province}">		 
		  $("#city").citySelect({prov:"${sysCompany.province}",city:"${sysCompany.city}"});  
		 </c:if> 
	});
</script>
</head>
<body >
<form class="defaultForm" id="defaultForm" name="defaultForm" method="post" action="updateSystemConfiguration_systemConfiguration.action" ENCTYPE="multipart/form-data">
<input name="systemConfiguration.scId" type="hidden" id="scId" value="${systemConfiguration.scId}">
<input name="systemConfiguration.comId" type="hidden" id="comId" value="${systemConfiguration.comId}">
<input name="systemConfiguration.systemCurrentDept" type="hidden" id="systemCurrentDept" value="${sysCompany.compName }">
<input name="sysCompany.scId" type="hidden" id="sysCompany.scId" value="${sysCompany.scId}">
<input name="sysCompany.compName" type="hidden" id="sysCompany.compName" value="${sysCompany.compName}">
<input name="sysCompany.status" type="hidden" id="sysCompany.status" value="${sysCompany.status}">
<input name="sysCompany.createTime" type="hidden" id="sysCompany.createTime" value="${sysCompany.createTime}">
<input name="sysCompany.demoData" type="hidden" id="sysCompany.demoData" value="${sysCompany.demoData}">
<input name="sysCompany.loginName" type="hidden" id="sysCompany.loginName" value="${sysCompany.loginName}">
<input name="sysCompany.password" type="hidden" id="sysCompany.password" value="${sysCompany.password}">
<input type="hidden" id="" name="sysCompany.logo" value="${sysCompany.logo}" />
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container" >
	
    <div class="Conter_main_conter" style="width:90%;margin:10px auto" >
     <div class="sysCompany ">
		<div  class="hd"><h3><span>1. </span>采购单位基本信息</h3></div>
		<div class="bd">
    	<!-- 基本信息  begin-->
    	<table class="table_ys1 ">
            <tr>
			    <td width="20%"class="Content_tab_style1">采购单位名称</td>
			    <td width="30%" class="Content_tab_style2">${sysCompany.compName }</td>
			    <td rowspan="4" colspan="2">
			       <div style="width:220px; height:80px; overflow:hidden;">  
					<div id="imgDiv">
					   <c:choose>
					      <c:when test="${empty sysCompany.logo}">
                            <img src="<%=path %>/images/no.png" width="220" height="80"> 
					      </c:when>
					      <c:otherwise>					      
                            <img src="/fileWeb/${sysCompany.logo}" width="220" height="80">
					      </c:otherwise>
					    </c:choose> 
					</div>  
					</div>
					<br>  
					<input id="logo" name="file" type="file" value="上传公司logo"> <span style="color: red">图片宽度：220  高度 ：80</span>
			    </td>
			</tr>
			<tr>
				<td  class="Content_tab_style1"width="20%"  >
                   <c:choose>
					<c:when test="${supplierInfo.registrationType=='01'}">
					    统一社会信用代码证号：  
					</c:when>
					<c:otherwise>
					     组织机构代码证编号：
					</c:otherwise>
				</c:choose></td>
				<td  class="Content_tab_style2" width="30%" >
					<input type="text"  maxlength="20" id="orgCode" name="sysCompany.orgCode"  value="${sysCompany.orgCode }" readonly style="background-color: #ccc"/>&nbsp;<font color="#ff0000">*</font>
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">公司简称：</td>
				<td class="Content_tab_style2">
					<input type="text" class="Content_input_style1" id="" name="sysCompany.shortName" value="${sysCompany.shortName}" />&nbsp;
				</td>
    		 </tr>
			 <tr>
			    <td class="Content_tab_style1">公司电话：</td>
			    <td class="Content_tab_style2"><input  datatype="*" nullmsg="公司电话不能为空！" name="sysCompany.compPhone" type="text" id="compPhone" value="${sysCompany.compPhone}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">公司电话不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">公司传真：</td>
			    <td class="Content_tab_style2"><input   name="sysCompany.compFax" type="text" id="compFax" value="${sysCompany.compFax}"><font color="#ff0000">*</font></td>
			    <td width="20%"class="Content_tab_style1">联系人</td>
			    <td width="30%" class="Content_tab_style2"><input  datatype="*" nullmsg="联系人不能为空！" name="sysCompany.contact" type="text" id="contact" value="${sysCompany.contact}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">联系人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			</tr>			
			<tr>
			    <td class="Content_tab_style1">联系手机：</td>
			    <td class="Content_tab_style2"><input  datatype="*" nullmsg="联系人手机不能为空！" name="sysCompany.mobilePhone" type="text" id="mobilePhone" value="${sysCompany.mobilePhone}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">联系人手机不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			    <td class="Content_tab_style1">所在地区：</td>
			    <td class="Content_tab_style2">
			    <div id="city"> 
				      <select class="prov" name="sysCompany.province" style="width: 105px;"></select>  
				      <select class="city" name="sysCompany.city" style="width: 105px;" disabled="disabled"></select> 
				</div>          
                </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">所属行业：</td>
			    <td class="Content_tab_style2">
			    <select datatype="*" nullmsg="所属行业不能为空！" 
						   id			 =	"industryOwned"
	                       name          =  "sysCompany.industryOwned" >
	                   <c:forEach var="map" items="${industryOwnedMap}">
	                   <option value="${map.key }" <c:if test="${sysCompany.industryOwned==map.key }">selected</c:if>>${map.value }</option>
	                 </c:forEach>
	                 </select>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">所属行业不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			    <td class="Content_tab_style1">经营模式：</td>
			    <td class="Content_tab_style2">
			    <select datatype="*" nullmsg="经营模式不能为空！" 
						   id			 =	"managementModel"
	                       name          =  "sysCompany.managementModel" >
	                   <c:forEach var="map" items="${managementModelMap}">
	                   <option value="${map.key }" <c:if test="${sysCompany.managementModel==map.key }">selected</c:if>>${map.value }</option>
	                 </c:forEach>
	                 </select>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">经营模式不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">公司地址：</td>
			    <td class="Content_tab_style2"><input   name="sysCompany.companyAddress" type="text" id="companyAddress" value="${sysCompany.companyAddress}">
			    </td>
			    <td class="Content_tab_style1">公司网站：</td>
			    <td class="Content_tab_style2"><input  name="sysCompany.companyWebsite" type="text" id="companyWebsite" value="${sysCompany.companyWebsite}">
			    </td>
			</tr>
			<tr>
			 	<td  class="Content_tab_style1">成立日期：</td>
				<td  class="Content_tab_style2">
					<input type="text" id="" name="sysCompany.createDate" value="<fmt:formatDate value="${sysCompany.createDate}" pattern="yyyy-MM-dd"/>" class="Wdate" onclick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" />
				</td>
				<td  class="Content_tab_style1">注册资金：</td>
				<td  class="Content_tab_style2">
					<input type="text" datatype="*" nullmsg="注册资金不能为空！"  id="registerFunds" name="sysCompany.registerFunds" value="${sysCompany.registerFunds}" />&nbsp;
					<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">注册资金不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td  class="Content_tab_style1">经营范围：</td>
				<td  class="Content_tab_style2" colspan="3">
				   <textarea id="introduce" name="sysCompany.scopeBusiness" datatype="s0-1000|/^\s*$/ " errormsg="经营范围限1000个字符！" style="width: 90%" rows="5" class="Content_input_style2" >${sysCompany.scopeBusiness}</textarea>
					<div class="info"><span class="Validform_checktip">经营范围限500个字符！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>			
			<tr>
			    <td class="Content_tab_style1">公司简介：</td>
			    <td class="Content_tab_style2" colspan="3">
			        <textarea id="introduce" name="sysCompany.companyProfile" datatype="s0-1000|/^\s*$/ " errormsg="企业简介限1000个字符！" style="width: 90%" rows="3" class="Content_input_style2" >${sysCompany.companyProfile }</textarea>
					<div class="info"><span class="Validform_checktip">企业简介限500个字符！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			</table>
		</div>
	   </div>
       <div class="sysCompany ">
		<div  class="hd"><h3><span>2. </span>业务功能设置</h3></div>
		<div class="bd"> 
		<table class="table_ys1 ">
		   <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">a.基础设置</td>
    	   </tr>
		   <tr>
		        <td width="20%" class="Content_tab_style1">计划编号前缀：</td>
			    <td width="30%" class="Content_tab_style2"><input datatype="*" nullmsg="计划编号前缀不能为空！"  name="systemConfiguration.requiredMaterialPrefix" type="text" id="requiredMaterialPrefix" value="${systemConfiguration.requiredMaterialPrefix}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">计划编号前缀不能为空</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			    <td width="20%" class="Content_tab_style1">采购立项编号前缀：</td>
			    <td width="30%" class="Content_tab_style2"><input datatype="*" nullmsg="采购立项编号前缀不能为空！"  name="systemConfiguration.requiredCollectPrefix" type="text" id="requiredCollectPrefix" value="${systemConfiguration.requiredCollectPrefix}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">采购立项编号前缀不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			</tr>
			<tr>
		        <td width="20%" class="Content_tab_style1">合同编号前缀：</td>
			    <td width="30%" class="Content_tab_style2"><input datatype="*" nullmsg="合同编号前缀不能为空！"  name="systemConfiguration.contractPrefix" type="text" id="contractPrefix" value="${systemConfiguration.contractPrefix}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">合同编号前缀不能为空</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			    <td width="20%" class="Content_tab_style1">订单编号前缀：</td>
			    <td width="30%" class="Content_tab_style2"><input datatype="*" nullmsg="订单编号前缀不能为空！"  name="systemConfiguration.orderPrefix" type="text" id="orderPrefix" value="${systemConfiguration.orderPrefix}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">订单编号前缀不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			</tr>
			<tr>
		        <td width="20%" class="Content_tab_style1">资金计划编号前缀：</td>
			    <td width="30%" class="Content_tab_style2"><input datatype="*" nullmsg="资金计划编号前缀不能为空！"  name="systemConfiguration.capitalPlanPrefix" type="text" id="capitalPlanPrefix" value="${systemConfiguration.capitalPlanPrefix}"><font color="#ff0000">*</font>
			    <div class="info"><span class="Validform_checktip">资金计划编号前缀不能为空</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div></td>
			    <td width="20%" class="Content_tab_style1"></td>
			    <td width="30%" class="Content_tab_style2"></td>
			</tr>
			<tr>
			    <td width="20%" class="Content_tab_style1">邮件开关：</td>
			    <td width="30%" class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="mailFalg1"></span>
				    <input type="hidden" id="mailFalg" name="systemConfiguration.mailFalg" value="${systemConfiguration.mailFalg }">
			    </td>
			    <td width="20%"class="Content_tab_style1">短信开关：</td>
			    <td width="30%" class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="messageFalg1"></span>
				    <input type="hidden" id="messageFalg" name="systemConfiguration.messageFalg" value="${systemConfiguration.messageFalg }">
			    </td>
			</tr>			
		    <tr>
    	       <td colspan="4" class="Content_tab_style_td_head">b.审批设置</td>
    	    </tr>
            <tr>
                <td class="Content_tab_style1">需求计划审批开关：</td>
			    <td class="Content_tab_style2">
				    <span class="switch-off" themeColor="#6d9eeb" id="requiredMaterialWorkflow1"></span>
				    <input type="hidden" id="requiredMaterialWorkflow" name="systemConfiguration.requiredMaterialWorkflow" value="${systemConfiguration.requiredMaterialWorkflow }">
			    </td>			    
                <td class="Content_tab_style1"></td>
			    <td class="Content_tab_style2">
			    </td>
			</tr>	 
    	    <tr>
    	        <td class="Content_tab_style1">招标立项项目审批开关：</td>
			    <td class="Content_tab_style2">			    
				    <span class="switch-off" themeColor="#6d9eeb" id="requiredcollect00Workflow1"></span>
				    <input type="hidden" id="requiredcollect00Workflow" name="systemConfiguration.requiredcollect00Workflow" value="${systemConfiguration.requiredcollect00Workflow }">
			    </td>
				<td class="Content_tab_style1">招标授标审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="bidpurchaseresult00Workflow1"></span>
				    <input type="hidden" id="bidpurchaseresult00Workflow" name="systemConfiguration.bidpurchaseresult00Workflow" value="${systemConfiguration.bidpurchaseresult00Workflow }">
			   </td>
			</tr>  
    	    <tr>
				<td class="Content_tab_style1">询价立项项目审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="requiredcollect01Workflow1"></span>
				    <input type="hidden" id="requiredcollect01Workflow" name="systemConfiguration.requiredcollect01Workflow" value="${systemConfiguration.requiredcollect01Workflow }">
				</td>
			    <td class="Content_tab_style1">询价授标审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="bidpurchaseresult01Workflow1"></span>
				    <input type="hidden" id="bidpurchaseresult01Workflow" name="systemConfiguration.bidpurchaseresult01Workflow" value="${systemConfiguration.bidpurchaseresult01Workflow }">
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">竞价立项项目审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="requiredcollect02Workflow1"></span>
				    <input type="hidden" id="requiredcollect02Workflow" name="systemConfiguration.requiredcollect02Workflow" value="${systemConfiguration.requiredcollect02Workflow }">
				</td>
				<td class="Content_tab_style1">竞价授标审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="bidpurchaseresult02Workflow1"></span>
				    <input type="hidden" id="bidpurchaseresult02Workflow" name="systemConfiguration.bidpurchaseresult02Workflow" value="${systemConfiguration.bidpurchaseresult02Workflow }">
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="contractWorkflow1"></span>
				    <input type="hidden" id="contractWorkflow" name="systemConfiguration.contractWorkflow" value="${systemConfiguration.contractWorkflow }">
				</td>
				<td class="Content_tab_style1">订单审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="orderWorkflow1"></span>
				    <input type="hidden" id="orderWorkflow" name="systemConfiguration.orderWorkflow" value="${systemConfiguration.orderWorkflow }">
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">资金计划审批开关：</td>
			    <td class="Content_tab_style2">
			        <span class="switch-off" themeColor="#6d9eeb" id="capitalPlanWorkflow1"></span>
				    <input type="hidden" id="capitalPlanWorkflow" name="systemConfiguration.capitalPlanWorkflow" value="${systemConfiguration.capitalPlanWorkflow }">
				</td>
				<td class="Content_tab_style1"></td>
			    <td class="Content_tab_style2">
			</tr>
			
			</table>
		</div>
	</div>
		<div class="buttonDiv">
			<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
			
		</div>
</div>
</div>
</form>
<script type="text/javascript">
$(function (){	
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body>
</html> 	