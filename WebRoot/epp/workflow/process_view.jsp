<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/common/context.jsp"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
	<head>
		<title>流程状态</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<meta http-equiv="Cache-Control" content="no-store"/>
		<meta http-equiv="Pragma" content="no-cache"/>
		<meta http-equiv="Expires" content="0"/>
	    <script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
		<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
		<link rel="stylesheet" href="${ctx}/style/css/style.css" type="text/css" media="all" />
		<link rel="stylesheet" href="${ctx}/style/css/snaker.css" type="text/css" media="all" />
		<script src="${ctx}/style/js/raphael-min.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/jquery-ui-1.8.4.custom/js/jquery.min.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/jquery-ui-1.8.4.custom/js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/snaker/snaker.designer.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/snaker/snaker.model.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/snaker/snaker.editors.js" type="text/javascript"></script>

<script type="text/javascript">

		var api = frameElement.api, W = api.opener;
		var workFlow=${workFlow};
	function display(process, active) {
		/** view*/
		$('#snakerflow').snakerflow($.extend(true,{
			basePath : "${ctx}/style/js/snaker/",
            ctxPath : "${ctx}",
            orderId : "${order.id}",
			restore : process,
			editable : false
			},active
		));
	}
	function doWorkFlowReminders(orderId,taskName){
	  var param = { 
            orderId:orderId, 
            taskName:taskName 
        }; 

	 $.ajax({
			type:"POST",
			async:false,
			url:"<%=path%>/saveWorkFlowReminders_bidworkflow.action",
			data:param,
			dataType:"text",
			error: function(){
				showMsg("alert",'流程催办出现错误！');
				return false;
			},
			success: function(data){
				 if(data=="1"){
				   showMsg("alert",'流程催办成功！');
				 }
			}
		});
		var tipDIV = document.getElementById("tipDIV");
            if (tipDIV) {
                document.body.removeChild(tipDIV);
            }
	}
</script>
</head>
	<body style="background-color:white;">
		<table class="table_ys1" align="center" border="0" cellpadding="0"
			cellspacing="0" style="margin-top: 0px" width="98%">
			<tr>
				<td align="left" class="td_list_2" colspan="2" style="font-weight: bold">
					流程名称：<font color="red">${order.processName }</font>&nbsp;&nbsp;
				</td>
				<td align="left" class="td_list_2" colspan="2" style="font-weight: bold">
					流程编号：<font color="red">${order.orderNo }</font>&nbsp;&nbsp;
				</td>
				<td align="left" class="td_list_2" colspan="2" style="font-weight: bold">
					流程创建时间：<font color="red">${order.createTime }</font>
				</td>
			</tr>
			<tr class="Content_tab_style_04">
				<td align=center width=20% nowrap>
					任务名称
				</td>
				<td align=center width=16% nowrap>
					任务创建时间
				</td>
				<td align=center width=16% nowrap>
					任务完成时间
				</td>
				<td align=center width=16% nowrap>
					任务处理人
				</td>
				<td align=center width=16% nowrap>
					审批结果
				</td>
				<td align=center width=16% nowrap>
					审批意见
				</td>
			</tr>
			<s:iterator value="#request.tasks" status="">
				<tr>
					<td class="td_list_2" align=left nowrap>
						<s:property value="displayName" escapeHtml="false"/>&nbsp;
					</td>
					<td class="td_list_2" align=left nowrap>
						<s:property value="createTime" />&nbsp;
					</td>
					<td class="td_list_2" align=left nowrap>
						<s:property value="finishTime" />&nbsp;
					</td>
					<td class="td_list_2" align=left nowrap>
						<s:property value="operatorCn" />
						&nbsp;
					</td>
					<td class="td_list_2" align=left nowrap>
						<s:if test="performType==1">同意</s:if><s:elseif test="performType==-1">不同意</s:elseif>&nbsp;&nbsp;
					</td>
					<td class="td_list_2" align=left><s:property value="variable" />&nbsp;</td>
				</tr>
			</s:iterator>
		</table>
		<c:if test="${workFlow}">
		<div style="font-weight: bold;color: red">温馨提示：如需催办，请点击红色流程节点处，弹出窗口的<img src="<%=path%>/images/clock.png"/>图片进行催办操作。</div>
		</c:if>
		<table class="properties_all" align="center" border="0" cellpadding="0" cellspacing="0" style="margin-top: 0px;background-image: url('${ctx}/style/js/snaker/images/flowdesignbg.png" height="98%">
			<tr><td>
			<div id="snakerflow" style="margin-top:10px; margin-left:10px; width:98%;height:98%;">
			</div></td></tr>			
		</table>
		<script type="text/javascript">
		$.ajax({
				type:'GET',
				url:"<%=path%>/json_workflow.action?r="+Math.random(),
				data:"orderId=${order.id}",
		        dataType: 'json',
				async: false,
				globle:false,
				error: function(){
					showMsg("alert",'数据处理错误！');
					return false;
				},
				success: function(data){
					var json = data;
					display(json.process, json.active);
				}
			});
			
		</script>
	</body>
</html>
