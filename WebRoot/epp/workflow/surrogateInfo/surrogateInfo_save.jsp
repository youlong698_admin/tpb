<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>委托代理设置</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
   <script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
    <script src="<%=path%>/common/script/workFlow/workFlow.js" type="text/javascript"></script>
    <script type="text/javascript">
	var api = frameElement.api, W = api.opener;
	$(function (){
	
		//返回信息
	   <c:if test="${message!=null}">
	   window.onload=function(){ 
		    showMsg('success','${message}');
		       W.doQuery();
		       api.close();
		  	}
	    </c:if>
	    
	    
	});
	
	//设置委托代理人
   function selectSurrogateUser(type){
	   typeUser=type;
		 createdetailwindow_choose("设置委托人","viewDeptIndex_userTree.action",1);
			
   }
   //设置流程交办人
   function selectInFormUser(type){
	   typeUser=type;
	   createdetailwindow_choose("设置代理人","viewDeptIndex_userTree.action",1);
		
   }
   function valueEvaluateUser(){
	   
		var users=$("#returnValues").val();
		//alert(users)
		if(users!=""){
			var str="";
			var std="";
			var ur = users.split(",");
			for(var i=0;i<ur.length;i++){
				var u = ur[i].split(":");
				str += u[0]+",";     
				std += u[1]+",";
				if(typeUser=="operator")
				{				
					document.getElementById("operator1").value=u[2];
					document.getElementById("operator").value=u[1];						
				}else
				{
					document.getElementById("surrogate1").value=u[2];
					document.getElementById("surrogate").value=u[1];	
				}
			}
			
		}
	}
	</script>
</head>
 
<body>
<form class="defaultForm" id="" name=""  method="post" action="saveSurrogateInfo_workflow.action">
<input type="hidden" name="returnValues" id="returnValues"/>
<input name="sgType"  type="hidden" value="0"/>

<c:if test="${empty adminType}">
 <input name="surrogate.operator"  type="hidden" value="${operator }"/>
</c:if>
<!-- 防止表单重复提交 -->
<s:token/>
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
        	<tr>
          		<td  colspan="4" class="Content_tab_style_05">委托代理信息设置</td>
        	</tr>	
        	<c:if test="${not empty adminType}">
			<tr>
				<td width="40%" class="Content_tab_style1">
						委托人：
					</td>
				<td width="60%" class="Content_tab_style2">
					<input name="" id="operator" datatype="*" nullmsg="委托人不能为空！" type="text" class="Content_input_style1" onclick="selectSurrogateUser('operator')" value="" readonly/>&nbsp;<font color="#ff0000">*</font>
					<input name="surrogate.operator" type="hidden" id="operator1" value="" />
					<div class="info"><span class="Validform_checktip">委托人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>				
			</c:if>
    		<tr>				
				<td width="40%" class="Content_tab_style1">代理人：</td>
				<td width="60%" class="Content_tab_style2">
					<input name="" id="surrogate" datatype="*" nullmsg="代理人不能为空！" type="text" class="Content_input_style1" onclick="selectSurrogateUser('surrogate')" value="" readonly/>&nbsp;<font color="#ff0000">*</font>
					<input name="surrogate.surrogate" type="hidden" id="surrogate1" value="" />
					<div class="info"><span class="Validform_checktip">代理人不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
    		
			<tr>
				<td class="Content_tab_style1">开始时间：</td>
				<td class="Content_tab_style2">
					<input name="surrogate.sdate" datatype="*" nullmsg="开始时间不能为空！" type="text" class="Wdate" onClick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" 
                        id="startDate" value="<fmt:formatDate value="${cuurDate}" 
                              pattern="yyyy-MM-dd" />" readonly/>&nbsp;<font color="#ff0000">*</font>
                        <div class="info"><span class="Validform_checktip">开始时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			<tr>
				<td class="Content_tab_style1">结束时间：</td>
				<td class="Content_tab_style2">
					<input name="surrogate.edate" datatype="*" nullmsg="结束时间不能为空！" type="text" class="Wdate" onClick="WdatePicker({ dateFmt:'yyyy-MM-dd' })" readonly
                     onchange="validateBidsDate();" id="endDate" value="" />&nbsp;<font color="#ff0000">*</font>
                     <div class="info"><span class="Validform_checktip">结束时间不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				</td>
			</tr>
			
        </table>
        <div class="buttonDiv">
				<button class="btn btn-success" id="btn-save" ><i class="icon-white icon-ok-sign"></i>保存</button>
				<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>

			</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body></html>