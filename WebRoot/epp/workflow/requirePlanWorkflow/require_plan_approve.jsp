<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>领导计划审批</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="<%= basePath %>/common/script/context.js" type="text/javascript" ></script>	 
<script src="<%= basePath %>/common/script/workFlow/workFlow.js" type="text/javascript" ></script>	    
<script language="javaScript">
var api = frameElement.api, W = api.opener;

</script>

</head>
<body >
<form id="cx" method="post" action="saveInitiatorToApprove_bidworkflow.action">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
	<div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
                <li id="Tab1"  class="active">
                    <a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');">基本信息</a>
                </li>
                <li id="Tab2" >
                    <a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b class="spxx">审批记录（<font color="red">${fn:length(hytList) }</font>）</b></a>
                </li>
               
			</ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
            	<div id="dTab1" class="HackBox" style="display:block">
            		<table class="table_ys1">
    				<tr>
		          		<td  colspan="4" class="Content_tab_style_05">计划信息</td>
		        	</tr>
		    		<tr>
						<td width="15%" class="Content_tab_style1">计划编号：</td>
						<td width="85%" class="Content_tab_style2" colspan="3">
							${requiredMaterial.rmCode }&nbsp;
						</td>
					</tr>
					<tr>
					    <td width="15%" class="Content_tab_style1">计划名称：</td>
						<td width="35%" class="Content_tab_style2">
							${requiredMaterial.rmName }
						</td>
						<td width="15%" class="Content_tab_style1">计划类型：</td>
						<td width="35%" class="Content_tab_style2">
							${requireTypeCn }
						</td>
					</tr>
					<tr>
						<td width="10%" class="Content_tab_style1" nowrap>估算总额：</td>
						<td width="30%" class="Content_tab_style2">
							<fmt:formatNumber value="${requiredMaterial.totalBudget}" pattern="##0.00"/>
						</td>
						<td class="Content_tab_style1" nowrap>采购组织：</td>
						<td class="Content_tab_style2">
							${purchaseDeptName }
						</td>
				    </tr>
					<tr>
						<td class="Content_tab_style1" nowrap>申请人：</td>
						<td class="Content_tab_style2">
							${creator }
						</td>
						<td class="Content_tab_style1" nowrap>申请部门：</td>
						<td class="Content_tab_style2">
							${deptName }
						</td>
					</tr>
					<tr>
						<td class="Content_tab_style1" nowrap>申请日期：</td>
						<td class="Content_tab_style2">
							${createTime}
						</td>
						<td class="Content_tab_style1" nowrap></td>
						<td class="Content_tab_style2">
							
						</td>
					</tr>
					<tr>
						<td class="Content_tab_style1">附件列表：</td>
						<td class="Content_tab_style2" colspan="3">
							<c:out value="${requiredMaterial.attachmentUrl}" escapeXml="false"/>
						</td>
					</tr>
			</table>
		<table class="table_ys1">
		<tr>
         <td  colspan="12" class="Content_tab_style_05">计划明细</td>
       	</tr>
		<tr align="center" class="Content_tab_style_04">
			<th width="3%" nowrap>序号</th>
			<th width="8%" nowrap>编码 </th>
			<th width="18%" nowrap>名称 </th>
			<th width="18%" nowrap>规格型号 </th>
		    <th width="5%" nowrap>单位</th>
			<th width="5%" nowrap>数量</th>
			<th width="6%" nowrap>上次采购价</th>
			<th width="6%" nowrap>估算单价</th>
			<th width="6%" nowrap>估算总价</th>
			<th width="6%" nowrap>交货时间</th>
			<th width="5%" nowrap>交货地点</th>
			<th width="13%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredMaterialDetail" varStatus="status">
			<tr align="center" <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>${requiredMaterialDetail.buyCode}</td>
				
				<td>${requiredMaterialDetail.buyName}</td>
				<td>${requiredMaterialDetail.materialType}</td>
				<td>${requiredMaterialDetail.unit}</td>
				<td align="right">${requiredMaterialDetail.amount}</td>
				<td align="right">${requiredMaterialDetail.newPrice}</td>
				<td align="right"><fmt:formatNumber value="${requiredMaterialDetail.estimatePrice}" pattern="##0.00"/></td>
				<td align="right"><fmt:formatNumber value="${requiredMaterialDetail.estimateSumPrice}" pattern="##0.00"/></td>
				<td><fmt:formatDate value="${requiredMaterialDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></td>
				<td>${requiredMaterialDetail.deliverPlace}</td>
				<td>${requiredMaterialDetail.remark }</td>
			</tr>
		</c:forEach>
	</table>
        <table width="80%" class="table_ys1">
	  	 	<tr>
	    	<td class="Content_tab_style1" width="15%">审批意见：</td>
			<td class="Content_tab_style2" width="85%" colspan="3">
				<c:if test="${detail=='detail'}">
					${signContent }
				</c:if>
				<c:if test="${detail!='detail'}">
					<textarea name="signContent" rows="3" id="signContent"  style="width:90%;border: solid 1px gray;" onkeyup="checkLen(500)" ></textarea><br/>
					<font color="#FF0000">*</font>
					<font color="#FF0000">[限500个汉字!]</font>&nbsp;可输入字数：<input name=ModCou03 size=3 readonly value="500" class="ModCou03"/>&nbsp;已输入字数：<input name=YesCou03 id="YesCou03" size=3 readonly value="0" class="YesCou03"/>
				</c:if>
			</td>
	  	</tr>
	  	<tr>
				<td width="15%" class="Content_tab_style1">审批人：</td>
				<td width="35%" class="Content_tab_style2">${taskActors }
				</td>
				<td width="15%" class="Content_tab_style1">审批时间：</td>
				<td width="35%" class="Content_tab_style2">${task.createTime }
				</td>
			</tr>
	  	<tr>
				<td width="15%" class="Content_tab_style1">审批结果：</td>
				<td width="35%" class="Content_tab_style2" colspan="3">
				<c:if test="${detail=='detail'}">
					<c:choose>
					  <c:when test="${result=='1'}">通过</c:when>
					  <c:when test="${result=='-1'}">不通过</c:when>
					  <c:otherwise></c:otherwise>
					</c:choose>
					</c:if>
				</td>
			</tr>
			
	</table>
        <div class="buttonDiv">	
			<c:if test="${detail!='detail'}">
				<button class="btn btn-success" onclick="submitWorkFlow('1');" type="button"><i class="icon-white icon-ok-sign"></i>通  过</button>
				<button class="btn btn-danger" onclick="submitWorkFlow('-1');" type="button"><i class="icon-white icon-remove-sign"></i>退  回</button>
				<input type="hidden" id="result" name="result" value="" />
			</c:if>
				
		</div>
       </div>
             	
      		
 	<!-------------------- 审批记录--------------------------->
    <div id="dTab2" class="HackBox">
	   <table width="80%" class="table_ys1">
	  		<tr>
          		<td  colspan="6" class="Content_tab_style_05" style="text-align:center"><b>审批记录</b></td>
        	</tr>
	  			<tr class="Content_tab_style_04" >
	  				<th width="5%" nowrap>序号</th>
	  				<th width="10%" nowrap>审批环节</th>
					<th width="10%" nowrap>审批人</th>
					<th width="10%" nowrap>审批时间</th>
					<th width="10%" nowrap>审批结果 </th>
					<th width="30%" nowrap>审批意见</th>
	       	    </tr>
	       	    <c:forEach items="${hytList}" var="hy" varStatus="status">
				<tr <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>
					<c:out escapeXml="false" value="${hy.displayName}" />&nbsp;
				</td>
				<td>
					${hy.operatorCn}&nbsp;
				</td>
				<td>
					${hy.finishTime}&nbsp;
				</td>
				<td>
					<c:choose><c:when test="${hy.performType=='1'}">通过</c:when><c:when test="${hy.performType=='-1'}">不通过</c:when><c:otherwise></c:otherwise></c:choose>&nbsp;&nbsp;
				</td>
				<td align="left">${hy.variable}</td>
			 </tr>
			  </c:forEach>
    	</table>		
		</div>		
	</div>
</div>
</form>		
</body>
</html>