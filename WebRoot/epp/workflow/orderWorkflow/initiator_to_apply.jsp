<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
	<title>合同审批单 </title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%= path %>/common/script/workFlow/workFlow.js" type="text/javascript" ></script>
	<script type="text/javascript">
		
		var api = frameElement.api, W = api.opener;
		
	</script>
</head>
 
<body>
<c:set var="td1" value=""/>
<c:set var="td2" value=""/>
<c:choose>
<c:when test="${orderInfo.type==1||orderInfo.type==3 }"><c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:when>
<c:otherwise><c:set var="td1" value="合同名称"/> <c:set var="td2" value="合同编号"/></c:otherwise>
</c:choose>
<form id="supInfo" name="" method="post" action="saveInitiatorToApply_bidworkflow.action">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
		<div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
         
				<li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');"><b>基本信息</b></a></li>
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b class="spxx">审批记录（<font color="red">${fn:length(hytList) }</font>）</b></a></li>
            </ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">
    	<table class="table_ys1"  width="100%">	
    	    <tr>
			    <td width="15%" class="Content_tab_style1" id="td1">${td1 }：</td>
			    <td width="35%" class="Content_tab_style2">
			      ${orderInfo.projectContractName }
			    </td>
			    <td width="15%" class="Content_tab_style1" id="td3">${td2 }：</td>
			    <td width="35%" class="Content_tab_style2">
			       ${orderInfo.bidCode }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">订单编号：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderCode }
			    </td>
			    <td class="Content_tab_style1">订单名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderName }
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">采购单位名称：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderPersonNameA }
			    </td>
			    <td class="Content_tab_style1">供应商名称：</td>
			    <td class="Content_tab_style2">
				    <a href="javascript:void()" onClick="createdetailwindow('查看供应商明细','viewSupplierSelectInfo_supplierBaseInfo.action?supplierInfo.supplierId=${orderInfo.supplierId}',1)">${orderInfo.orderNameB }</a>
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">收货地址：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.orderAddressA }
			    </td>
			    <td class="Content_tab_style1">供应商地址：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderAddressB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">收货联系人：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderUndertaker }
			    </td>
			    <td class="Content_tab_style1">供应商联系人：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderPersonB }
			    </td>
			</tr>			
			<tr>
			    <td class="Content_tab_style1">收货联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderMobileA }
			    </td>
			    <td class="Content_tab_style1">供应商联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderMobileB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位电话：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderTelA }
			    </td>
			    <td class="Content_tab_style1">供应商电话：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderTelB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">采购单位传真：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderFaxA }
			    </td>
			    <td class="Content_tab_style1">供应商传真：</td>
			    <td class="Content_tab_style2">
			       ${orderInfo.orderFaxB }
			    </td>
			    </tr>
            <tr>
			    <td class="Content_tab_style1">订单金额：</td>
			    <td class="Content_tab_style2">
			       <fmt:formatNumber value="${orderInfo.orderMoney }" pattern="#,##0.00"/>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        ${orderInfo.conMoneyType}
				</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3">${orderInfo.conRemark }</td>
			</tr>
		</table>
		<table class="table_ys1">
		<tr>
         <td  colspan="10" class="Content_tab_style_05">订单物资明细</td>
       	</tr>
		<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>单价</th>
				<th width="100px" nowrap>小计</th>
				<th width="100px" nowrap>备注</th>
			</tr>
			    <c:set var="totalPrice" value="0"/>
					  <c:forEach items="${omList}" var="orderMaterial" varStatus="status">
						    <c:set var="totalPrice" value="${totalPrice+orderMaterial.amount*orderMaterial.price }"/> 
							<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
								<td>
								  ${status.index+1}
								</td>
								<td>
								    ${orderMaterial.materialCode}								    
								</td>
								<td>
								   ${orderMaterial.materialName}
								</td>
								<td>
								   ${orderMaterial.materialType}
								</td>
								<td>
								   ${orderMaterial.unit}
							    </td>
								<td align="right">
							     ${orderMaterial.amount}
								 </td>
							    <td align="right">
							       <fmt:formatNumber value="${orderMaterial.price}" pattern="#,##0.00"/>
							    </td>
							    <td align="right">
							       <fmt:formatNumber value="${orderMaterial.price*orderMaterial.amount}" pattern="#,##0.00"/>
							    </td>
								<td>
								    ${orderMaterial.remark}
								</td>
							</tr>
						</c:forEach>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总价：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalPriceStr"><fmt:formatNumber value="${totalPrice}" pattern="#,##0.00"/><!-- 小计 --></span>
					</td>
					<td>&nbsp;</td>
				</tr>	
	</table>
	<table class="table_ys1">
    	   <c:if test="${detail!='detail'}">
			<tr>
		    	<td class="Content_tab_style1" width="15%">申请人意见：</td>
				<td  width="85%" colspan="5">
					<textarea name="signContent" rows="3" id="signContent"  style="width:90%;border: solid 1px gray;" ></textarea>
				</td>
		  	</tr>
		  	</c:if>
	</table>
	<div class="buttonDiv">
    			<c:if test="${detail!='detail'}">
				<button class="btn btn-success" id="" onclick="initApplySubmit()" type="button"><i class="icon-white icon-ok-sign"></i>提交</button>
				<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
				</c:if>
				
			</div>
   </div>
 	<!-------------------- 审批记录--------------------------->
    <div id="dTab2" class="HackBox">
	   <table width="80%" class="table_ys1">
	  		<tr>
          		<td  colspan="6" class="Content_tab_style_05" style="text-align:center"><b>审批记录</b></td>
        	</tr>
	  			<tr class="Content_tab_style_04" >
	  				<th width="5%" nowrap>序号</th>
	  				<th width="10%" nowrap>审批环节</th>
					<th width="10%" nowrap>审批人</th>
					<th width="10%" nowrap>审批时间</th>
					<th width="10%" nowrap>审批结果 </th>
					<th width="30%" nowrap>审批意见</th>
	       	    </tr>
	       	    <c:forEach items="${hytList}" var="hy" varStatus="status">
				<tr <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>
					<c:out escapeXml="false" value="${hy.displayName}" />&nbsp;
				</td>
				<td>
					${hy.operatorCn}
				</td>
				<td>
					${hy.finishTime}
				</td>
				<td>
					<c:choose><c:when test="${hy.performType=='1'}">通过</c:when><c:when test="${hy.performType=='-1'}">不通过</c:when><c:otherwise></c:otherwise></c:choose>&nbsp;&nbsp;
				</td>
				<td align="left">${hy.variable}</td>
			 </tr>
			  </c:forEach>
    	</table>		
		</div>	
	</div>
</div>
</form>
</body></html>