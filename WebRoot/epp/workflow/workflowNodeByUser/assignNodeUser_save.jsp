<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
	<title>流程节点分配处理人</title>
	<script type="text/javascript">
	$(function (){
	   
		var api = frameElement.api, W = api.opener;
		//返回信息
	   <c:if test="${message!=null}">
	   window.onload=function(){ 
		    showMsg('success','${message}');
		       W.doQuery();
		       api.close();
		  	}
	    </c:if>
	    
	});
	
	</script>
	<script language="javaScript">
	var api = frameElement.api, W = api.opener;
		//更新节点用户保存校验
		function updateNodeUser(){
			var userIndexs = document.getElementsByName("userIndex");
			var nodeList = document.getElementById("nodeList").value;
			var nodeInfo = "";
			var userIndex="";
			if(userIndexs!=null){
				for(var j=0;j<userIndexs.length;j++){
					     userIndex=userIndexs[j].value;
						 var userType=$("#userType"+userIndex).val();
						 if(userType=="10"){
							nodeInfo += document.getElementById("wpnId"+userIndex).value+":"+document.getElementById("nodeUserId"+userIndex).value+":"+document.getElementById("nodeUser"+userIndex).value+":"+document.getElementById("userOriginUser"+userIndex).value+":"+userType+"@";
						 }else if(userType=="11"){
							nodeInfo += document.getElementById("wpnId"+userIndex).value+":"+document.getElementById("nodeDeptId"+userIndex).value+":"+document.getElementById("nodeDept"+userIndex).value+":"+document.getElementById("userOriginDept"+userIndex).value+":"+userType+"@";
						 }else if(userType=="12"){
							nodeInfo += document.getElementById("wpnId"+userIndex).value+":"+document.getElementById("nodeScId"+userIndex).value+":"+document.getElementById("nodeSc"+userIndex).value+":"+document.getElementById("userOriginSc"+userIndex).value+":"+userType+"@";
						 }
				}		 
				//showMsg("alert",nodeUNObj.length+"--"+num+"=="+nodeInfo);
				document.getElementById("nodeInfo").value = nodeInfo;
				document.forms[0].submit();
			}else{
				return false;
			}
		}
    function selectUserType(value,index){
       if(value==10){
          $("#user"+index).show();
          $("#dep"+index).hide();
          $("#selflevCode"+index).hide();
          $("#station"+index).hide();
       }else if(value==11){
          $("#user"+index).hide();
          $("#dep"+index).show();
          $("#selflevCode"+index).hide();
          $("#station"+index).hide();
       }else if(value==12){
          $("#user"+index).hide();
          $("#dep"+index).hide();
          $("#selflevCode"+index).show();
          $("#station"+index).hide();
       }else if(value==13){
          $("#user"+index).hide();
          $("#dep"+index).hide();
          $("#selflevCode"+index).hide();
          $("#station"+index).show();
       }
    }
	var index_0;
		//选择用户
   	function selectUser(index){
   	    index_0=index;
		createdetailwindow_choose("指定用户","viewDeptIndex_userTree.action",1);		
	}
	function valueEvaluateUser(){
		
	    var index=index_0;
		var users=$("#returnValues").val();
		//alert(users)
		if(users!=""){
			var ur = users.split(",");
			for(var i=0;i<ur.length;i++){
				var u = ur[i].split(":");
				
				document.getElementById("nodeUN"+index).value=u[1];
				document.getElementById("nodeUserId"+index).value=u[0];
				document.getElementById("nodeUser"+index).value=u[2];	
			
			}
			
		}
	}
	//选择组织机构
   	function selectDep(index){
   	    index_0=index;
		createdetailwindow_choose("指定部门","viewDeptSelect_deptSelect.action",5);		
	}
	function valueEvaluateDept(){
		
	    var index=index_0;
		var users=$("#returnValues").val();
		//alert(users)
		if(users!=""){
			var ur = users.split(",");
			var u = ur[0].split(":");				
			document.getElementById("nodeDN"+index).value=u[1];
			document.getElementById("nodeDeptId"+index).value=u[0];
			document.getElementById("nodeDept"+index).value=u[1];	
			
		}
	}
	function doSet(wpnId){
	   createdetailwindow("多岗位设置","saveStationInit_workflow.action?wpnId="+wpnId,6);
	}
	</script>
</head>
<body >
<form method="post" action="updateAssignUserToNode_workflow.action">
<input type="hidden" id="processId" name="processId" value="${process.id }" />
<input type="hidden" id="nodeInfo" name="nodeInfo" value='' />
<input type="hidden" id="nodeList" name="" value="${fn:length(listValue)}" />
<input type="hidden" id="returnValues" name="returnValues" value='' />
	<div class="Conter_main_conter">
	 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys2">
	  		<tr>
          		<td  colspan="6" class="Content_tab_style_04" style="text-align:center"><b>【${process.displayName}】节点处理人设置</b></td>
        	</tr>
	  			<tr class="Content_tab_style_04" >
	  				<th width="5%" nowrap>序号</th>
			        <th width="10%" nowrap>节点标识</th>
			        <th width="15%" nowrap>节点名称</th>
			        <th width="10%" nowrap>处理人标识</th>
			        <th width="10%" nowrap>处理人类型</th>
			        <th width="30%" nowrap>处理人来源</th>
	       	    </tr>
	       	    <c:forEach items="${listValue}" var="node" varStatus="wfpn">
			    <tr class="biaoge_01_b" >
			     	<td>
			     	${wfpn.index+1 }
			     	<input type="hidden" id="wpnId${wfpn.index }" value="${node.wpnId}" />
			     	</td>
			    	<td>${node.nodeCode}</td>
			    	<td>${node.nodeName}</td>
			    	<td>${node.nodeOperator}</td>			    	
			    	<td>
			    	   <c:if test="${node.remark!='1'}">
			    	      <input type="hidden" name="userIndex" value="${wfpn.index }"/>
			    	      <select id="userType${wfpn.index }" name="userType" onchange="selectUserType(this.value,${wfpn.index })" style="width: 130px;">
			    	        <option value="10" <c:if test="${node.userType==10}">selected</c:if>>指定用户</option>
			    	        <option value="11" <c:if test="${node.userType==11}">selected</c:if>>指定部门</option>
			    	        <option value="12" <c:if test="${node.userType==12}">selected</c:if>>指定组织机构级别</option>
			    	        <option value="13" <c:if test="${node.userType==13}">selected</c:if>>不同岗位管理不同组织</option>
			    	      </select>
			    	   </c:if>
			    	</td>
			    	<td>
			    	<c:if test="${node.remark!='1'}">
			    	    <div id="user${wfpn.index }" <c:if test="${node.userType==0 ||node.userType==10 }"> style="display:block" </c:if> <c:if test="${node.userType!=10 }"> style="display:none" </c:if>>
				    		用户：<input type="text" id="nodeUN${wfpn.index }" name="nodeUN" value="${node.nodeUserNameCn}" onclick="selectUser(${wfpn.index })" readonly/>
				    		<input type="hidden" id="nodeUser${wfpn.index }" name="nodeUser" value='${node.nodeUserName}' />
				    		<input type="hidden" id="nodeUserId${wfpn.index }" name="nodeUserId" value='${node.nodeUserId}' />
				    		<input type="hidden" id="userOriginUser${wfpn.index }" name="userOriginUser" value='${node.userOrigin}' />
			    		</div>
			    		<div id="dep${wfpn.index }" <c:if test="${node.userType==11 }"> style="display:block" </c:if> <c:if test="${node.userType!=11 }"> style="display:none" </c:if>>
				    		部门：<input type="text" id="nodeDN${wfpn.index }" name="nodeDN" value="${node.nodeUserName}" onclick="selectDep(${wfpn.index })" readonly style="width: 80px;"/>
				    		<input type="hidden" id="nodeDept${wfpn.index }" name="nodeDept" value='${node.nodeUserName}' />
				    		<input type="hidden" id="nodeDeptId${wfpn.index }" name="nodeDeptId" value='${node.nodeUserId}' />
				    		岗位：
				    		<select id="userOriginDept${wfpn.index }" name="userOriginDept" style="width: 120px;">
						       <option value="">请选择</option>
				    		   <c:forEach var="dictionary" items="${dictionaryList}">
						           <option value="${dictionary.dictName }" <c:if test="${node.userOrigin==dictionary.dictName}">selected</c:if>>${dictionary.dictName }</option>
						        </c:forEach>
						   </select>
			    		</div>
			    		<div id="selflevCode${wfpn.index }" <c:if test="${node.userType==12 }"> style="display:block" </c:if> <c:if test="${node.userType!=12 }"> style="display:none" </c:if>>
				    		组织机构级别：<select id="nodeScId${wfpn.index }" name="nodeScId" style="width: 100px;">
				    		   <option value="">请选择</option>
				    		   <option value="1" <c:if test="${node.nodeUserId==1}">selected</c:if>>第1层级</option>
				    		   <option value="2" <c:if test="${node.nodeUserId==2}">selected</c:if>>第2层级</option>
				    		   <option value="3" <c:if test="${node.nodeUserId==3}">selected</c:if>>第3层级</option>
				    		   <option value="4" <c:if test="${node.nodeUserId==4}">selected</c:if>>第4层级</option>
				    		   <option value="5" <c:if test="${node.nodeUserId==5}">selected</c:if>>第5层级</option>
				    		</select>
				    		<input type="hidden" id="nodeSc${wfpn.index }" name="nodeSc" value='${node.nodeUserName}' />
				    		岗位：
				    		<select id="userOriginSc${wfpn.index }" name="userOriginSc" style="width: 120px;">
						        <option value="">请选择</option>
				    		   <c:forEach var="dictionary" items="${dictionaryList}">
						           <option value="${dictionary.dictName }" <c:if test="${node.userOrigin==dictionary.dictName}">selected</c:if>>${dictionary.dictName }</option>
						        </c:forEach>
						   </select>
			    		</div>
			    		<div id="station${wfpn.index }" <c:if test="${node.userType==13 }"> style="display:block" </c:if> <c:if test="${node.userType!=13 }"> style="display:none" </c:if>>
				    		<button class="btn" id="btn-station" type="button" onclick="doSet(${node.wpnId});"><i class="icon icon-user"></i>设置</button> 
			    		</div>
			    	</c:if>
			    	</td>
			    </tr>
		      </c:forEach>
    	</table>
    	<div class="buttonDiv">
			<button class="btn btn-success" type="button" id="btn-save" onclick="updateNodeUser();"><i class="icon-white icon-ok-sign"></i>保存</button>
			<button class="btn btn-cacel" id="btn-cacel" type="reset"><i class="icon icon-repeat"></i>重置</button> 
		</div>
	</div>
</form>
		
</body>
</html>
