<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<title>流程岗位权限</title>
	<script type="text/javascript">
	var api = frameElement.api, W = api.opener;
	var path="<%=path %>";
	var stationStr="<option value=\"\">请选择</option>";
    <c:forEach var="dictionary" items="${dictionaryList}">
        stationStr+="<option value=\"${dictionary.dictName }\">${dictionary.dictName }</option>";
    </c:forEach>
	
	var index_0;
	  //选择组织机构
   	function selectDep(index){
   	    index_0=index;
		createdetailwindow_choose("指定部门","viewDeptSelect_deptSelect.action",5);		
	}
	function valueEvaluateDept(){
		var deptId="",deptName="";
		var index=index_0;
		var users=$("#returnValues").val();
		//alert(users)
		if(users!=""){
		    var ur = users.split(",");
			for(var i=0;i<ur.length-1;i++){
				var u = ur[i].split(":");
				if(i==0){
					deptId+=","+u[0];
					deptName=u[1];
				}else{
					deptId+=","+u[0];
					deptName+=","+u[1];
				}
			}
            deptId+=",";
			document.getElementById("deptId_"+index).value=deptId;
			document.getElementById("deptName_"+index).value=deptName;	
			
		}
	}
	
	var num = "${fn:length(list)}";
function addRow(){
    var tr=document.getElementById("stationTable").insertRow();
	num++;
    tr.className="input_ys1";
    var cell1 = tr.insertCell(); 
    var cell2 = tr.insertCell();
    var cell3 = tr.insertCell();
	cell1.innerHTML="<input type='hidden' name='rowIndex' value='"+num+"'/>"
		    +"<button class='btn btn-mini btn-danger' type='button' onclick='deleteRow(this)'><i class=\"icon-white icon-trash\"></i></button>";
	cell2.innerHTML="<select id='station_"+num+"' name='wpnList["+num+"].station' style='width: 120px'>"+stationStr+"</select>" ;
	cell3.innerHTML="<textarea id='deptName_"+num+"' name='wpnList["+num+"].deptName' readonly='readonly'></textarea><img src='"+path+"/images/select.gif' title='选择部门'  onclick='selectDep("+num+");'/><input type='hidden' name='wpnList["+num+"].deptId' id='deptId_"+num+"' value=''/><input type='hidden' name='wpnList["+num+"].wpsrId' id='wpsrId_"+num+"' value='0'/>" ;
	
	cell1.align="center";
	cell2.align="center";
	cell3.align="center";
}
	//删除一行
	function deleteRow(obj){
		$(obj).parent().parent().remove(); 
	}
	
	//保存更改
	function updateWfProcessStationRights(){
	   var station,deptName,rowIndex,flag=false;
	   $("input[name='rowIndex']").each(function(j,item){
	      rowIndex=item.value;
	      station=$("#station_"+rowIndex).value;
	      deptName=$("#deptName_"+rowIndex).value;
	      if(station==""){
	         showMsg("alert","温馨提示：岗位不能为空！");
	         flag=true;
	         return;
	      }
	      if(station==""){
	         showMsg("alert","温馨提示：管理流程发起人部门不能为空！");
	         flag=true;
	         return;
	      }
       });
       
       if(!flag){
          document.form1.submit();
       }
	}
	
	</script>
</head>  
  <body>
    <form method="post" name="form1" action="saveStation_workflow.action">
         <input type="hidden" id="wpnId" name="wpnId" value="${wpnId }" />
         <input type="hidden" id="returnValues" name="returnValues" value='' />
             <table id="stationTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="10%" nowrap>操作<img src="<%=basePath%>/images/add.gif" title="添加岗位管理部门"
										onclick="addRow();"/></th>
						<th width="30%" nowrap>岗位</th>
						<th width="60%" nowrap>管理流程发起人部门 </th>
					</tr>
					<c:choose>
					   <c:when test="${fn:length(list)==0}">
					           <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="0"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td align="center">
								    <select id="station_0" name="wpnList[0].station" style="width: 160px;">
								        <option value="">请选择</option>
						    		       <c:forEach var="dictionary" items="${dictionaryList}">
								              <option value="${dictionary.dictName }">${dictionary.dictName }</option>
								           </c:forEach>
								   </select>
								</td>
								<td align="center">
								   <textarea id='deptName_0' name='wpnList[0].deptName' readonly='readonly'></textarea><img src="<%=basePath %>/images/select.gif" title="选择部门"  onclick="selectDep(0);"/> 
								   <input type='hidden' id='wpsrId_0' value='0' name='wpnList[0].wpsrId' /> 
								   <input type='hidden' id='deptId_0' value='' name='wpnList[0].deptId' /> 
								</td>
								</tr>
					   </c:when>
					   <c:otherwise>
					     <c:forEach items="${list}" var="wfProcessStationRights" varStatus="status">
							 <tr  class="input_ys1">
								<td align="center"><input type="hidden" name="rowIndex" value="${status.index}"/>
								    <button class='btn btn-mini btn-danger' type="button"  onclick='deleteRow(this)'><i class="icon-white icon-trash"></i></button>
								</td>
								<td align="center">
								    <select id="station_${status.index}" name="wpnList[${status.index}].station" style="width: 160px;">
								        <option value="">请选择</option>
						    		       <c:forEach var="dictionary" items="${dictionaryList}">
								              <option value="${dictionary.dictName }" <c:if test="${wfProcessStationRights.station==dictionary.dictName}">selected</c:if>>${dictionary.dictName }</option>
								           </c:forEach>
								   </select>
								</td>
								<td align="center">
								   <textarea id='deptName_${status.index}'  name='wpnList[${status.index}].deptName' readonly='readonly'>${wfProcessStationRights.deptName }</textarea><img src="<%=basePath %>/images/select.gif" title="选择部门"  onclick="selectDep(${status.index});"/> 
								   <input type='hidden' id='deptId_${status.index}' name='wpnList[${status.index}].deptId' value="${wfProcessStationRights.deptId }"/> 
								   <input type='hidden' id='wpsrId_${status.index}' name='wpnList[${status.index}].wpsrId' value="${wfProcessStationRights.wpsrId }"/> 
								</td>
								</tr>
						</c:forEach>
					   </c:otherwise>
					</c:choose>
					
    	            </table>
    	            <div class="buttonDiv">
						<button class="btn btn-success" type="button" id="btn-save" onclick="updateWfProcessStationRights();"><i class="icon-white icon-ok-sign"></i>保存</button>
						<button class="btn btn-cacel" id="btn-cacel" type="reset"><i class="icon icon-repeat"></i>重置</button> 
					</div>
    	    </form>
  </body>
</html>
