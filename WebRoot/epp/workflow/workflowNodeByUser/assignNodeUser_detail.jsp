<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<title>流程节点分配处理人</title>		
	<script language="javaScript">
	   
	var api = frameElement.api, W = api.opener;
		//返回列表页
		function goBackProcessView(){
			window.location.href="viewProcess_workflow.action";
		}
		function doView(wpnId){
	        createdetailwindow("多岗位查看","viewStationDetail_workflow.action?wpnId="+wpnId,6);
	    }
	</script>
</head>
<body >
<form method="post" action="">
	<div class="Conter_main_conter">
	 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_ys2">
	  		<tr>
          		<td  colspan="6" class="Content_tab_style_04" style="text-align:center"><b>【${process.displayName }】节点处理人设置</b></td>
        	</tr>
	  			<tr class="Content_tab_style_04" >
	  				<th width="5%" nowrap>序号</th>
			        <th width="10%" nowrap>节点标识</th>
			        <th width="15%" nowrap>节点名称</th>
			        <th width="10%" nowrap>处理人标识</th>
			        <th width="10%" nowrap>处理人类型</th>
			        <th width="30%" nowrap>处理人来源</th>
	       	    </tr>
	       	    <c:forEach items="${listValue}" var="node" varStatus="wfpn">
			    <tr class="biaoge_01_b" >
			     	<td>
			     	${wfpn.index+1 }
			     	</td>
			    	<td>${node.nodeCode}</td>
			    	<td>${node.nodeName}</td>
			    	<td>${node.nodeOperator}</td>
			    	<c:choose>
			    	   <c:when test="${node.userType==10}">
			    	    <td>指定用户</td>
			    	    <td>${node.nodeUserNameCn}</td>
			    	   </c:when>
			    	   <c:when test="${node.userType==11}">
			    	    <td>指定部门</td>
			    	    <td>部门：${node.nodeUserName}&nbsp;&nbsp;&nbsp;
			    	                      岗位：${node.userOrigin}</td>
			    	   </c:when>
			    	   <c:when test="${node.userType==12}">
			    	    <td>指定组织机构级别：</td>
			    	    <td>组织机构级别：${node.nodeUserId}&nbsp;&nbsp;&nbsp;
			    	                      岗位：${node.userOrigin}</td>
			    	   </c:when>
			    	   <c:when test="${node.userType==13}">
			    	    <td colspan="2"><button class="btn" id="btn-station" type="button" onclick="doView(${node.wpnId});"><i class="icon icon-user"></i>不同岗位管理不同组织查看</button></td>
			    	   </c:when>
			    	   <c:otherwise>
			    	      <td></td>
			    	      <td></td>
			    	   </c:otherwise>
			    	</c:choose>
			    </tr>
		      </c:forEach>
    	</table>
	</div>
</form>
		
</body>
</html>

