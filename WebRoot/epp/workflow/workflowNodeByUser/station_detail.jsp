<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<title>流程岗位权限</title>
	<script type="text/javascript">
	var api = frameElement.api, W = api.opener;
	
	</script>
</head>  
  <body>
    <form method="post" name="form1" action="saveStation_workflow.action">
              <table id="stationTable" class="table_ys1" width="100%">
    	            <tr id="tdNum" align="center" class="Content_tab_style_04">
						<th width="30%" nowrap>岗位</th>
						<th width="70%" nowrap>管理流程发起人部门 </th>
					</tr>
					<c:forEach items="${list}" var="wfProcessStationRights" varStatus="status">
							 <tr  class="input_ys1">
								<td align="center">
								    ${wfProcessStationRights.station}
								</td>
								<td>
								   ${wfProcessStationRights.deptName }
								</td>
								</tr>
						</c:forEach>
    	            </table>
    	    </form>
  </body>
</html>
