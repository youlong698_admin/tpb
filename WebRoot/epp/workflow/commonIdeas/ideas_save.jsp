<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<title>常用审批意见</title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
    <script src="<%=path%>/common/script/context_from.js" type="text/javascript"></script>
    <script src="<%=path%>/common/script/generalCheck.js" type="text/javascript"></script>
   <script src="<%=path%>/common/script/workFlow/workFlow.js" type="text/javascript"></script>
    <script language="javaScript">
    	$(function (){
	
			var api = frameElement.api, W = api.opener;
			//返回信息
		   <c:if test="${message!=null}">
		   window.onload=function(){ 
			  	  showMsg('success','${message}');
		            W.doQuery();
		            api.close();
		  	      }
		    </c:if>
		    
		   
		});
    </script>
</head>
 
<body>
<form id="" class="defaultForm" name=""  method="post" action="saveWfCommonIdeas_workflow.action">
<div class="Conter_Container">
    <div class="Conter_main_conter">
    	<table class="table_ys1">
        	<tr>
          		<td  colspan="4" class="Content_tab_style_05">添加常用审批意见</td>
        	</tr>	
    		<tr>
				<td class="Content_tab_style1" width="40%">审批意见：</td>
				<td class="Content_tab_style2" width="60%">
					<textarea name="wfCommonIdeas.ciName" rows="3" style="width:70%" datatype="*" nullmsg="审批意见不能为空！"></textarea>&nbsp;<font color="#ff0000">*</font>
					<div class="info"><span class="Validform_checktip">审批意见不能为空！</span><span class="dec"><s class="dec1">&#9670;</s><s class="dec2">&#9670;</s></span></div>
				
				</td>
			</tr>
        </table>
        <div class="buttonDiv">
				<button class="btn btn-success" id="btn-save"><i class="icon-white icon-ok-sign"></i>保存</button>
				<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>

			</div>
	</div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$(".defaultForm").Validform({
		btnSubmit:"#btn-save", 
		tiptype:function(msg,o,cssctl){
			if(!o.obj.is("form")){//验证表单元素时o.obj为该表单元素，全部验证通过提交表单时o.obj为该表单对象;
				var objtip=o.obj.parents("td").find(".Validform_checktip");
				cssctl(objtip,o.type);
				objtip.text(msg);
				
				var infoObj=o.obj.parents("td").find(".info");
				if(o.type==2){
					infoObj.fadeOut(200);
				}else{
					if(infoObj.is(":visible")){return;}
					var left=o.obj.offset().left,
						top=o.obj.offset().top;
	
					infoObj.css({
						left:left+10,
						top:top-45
					}).show().animate({
						top:top-35	
					},200);
				}
				
			}	
		}
	});
})
</script>
</body></html>