<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
		<title>流程展现</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<meta http-equiv="Cache-Control" content="no-store"/>
		<meta http-equiv="Pragma" content="no-cache"/>
		<meta http-equiv="Expires" content="0"/>
		<link rel="stylesheet" href="${ctx}/style/css/snaker.css" type="text/css" media="all" />
		<script src="${ctx}/style/js/raphael-min.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/jquery-ui-1.8.4.custom/js/jquery.min.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/jquery-ui-1.8.4.custom/js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/snaker/snaker.designer.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/snaker/snaker.model.js" type="text/javascript"></script>
		<script src="${ctx}/style/js/snaker/snaker.editors.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
				var json="${process }";
				var model;
				if(json) {
					//json.replace(new RegExp("@@","gm"), "\"")
					model=eval("(" + json + ")");
				} else {
					model="";
				}
				$('#snakerflow').snakerflow({
					basePath : "${ctx}/style/js/snaker/",
                    ctxPath : "${ctx}",
					restore : model,
                    formPath : "forms/",
					tools : {
						save : {
							onclick : function(data) {
								saveModel(data);
							}
						}
					}
				});
			});
			
			function saveModel(data) {
				$.ajax({
					type:'POST',
					url:"processDeploy_workflow.action",
					data:"model=" + data + "&id=${processId}",
					async: false,
					globle:false,
					error: function(){
						showMsg("alert",'数据处理错误！');
						return false;
					},
					success: function(data){
						if(data == 'true') {
							window.location.href = "viewProcess_workflow.action";
						} else {
							showMsg("alert",'数据处理错误！');
						}
					}
				});
			}
		</script>					
</head>
<body style="background-color:white;background-image: url('${ctx}/style/js/snaker/images/flowdesignbg.png')">
<div id="toolbox">
<div id="toolbox_handle">工具集</div>
<div class="node" id="save"><img src="${ctx}/style/js/snaker/images/save.gif" />&nbsp;&nbsp;保存</div>
<div>
<hr />
</div>
<div class="node selectable" id="pointer">
    <img src="${ctx}/style/js/snaker/images/select16.gif" />&nbsp;&nbsp;Select
</div>
<div class="node selectable" id="path">
    <img src="${ctx}/style/js/snaker/images/16/flow_sequence.png" />&nbsp;&nbsp;transition
</div>
<div>
<hr/>
</div>
<div class="node state" id="start" type="start"><img
	src="${ctx}/style/js/snaker/images/16/start_event_empty.png" />&nbsp;&nbsp;开始</div>
<div class="node state" id="end" type="end"><img
	src="${ctx}/style/js/snaker/images/16/end_event_terminate.png" />&nbsp;&nbsp;结束</div>
<div class="node state" id="task" type="task"><img
	src="${ctx}/style/js/snaker/images/16/task_empty.png" />&nbsp;&nbsp;任务</div>
<div class="node state" id="task" type="custom"><img
	src="${ctx}/style/js/snaker/images/16/task_empty.png" />&nbsp;&nbsp;自定义</div>
<div class="node state" id="task" type="subprocess"><img
	src="${ctx}/style/js/snaker/images/16/task_flow.png" />&nbsp;&nbsp;子流程</div>
<div class="node state" id="fork" type="decision"><img
	src="${ctx}/style/js/snaker/images/16/gateway_exclusive.png" />&nbsp;&nbsp;决策</div>
<div class="node state" id="fork" type="fork"><img
	src="${ctx}/style/js/snaker/images/16/gateway_parallel.png" />&nbsp;&nbsp;分支</div>
<div class="node state" id="join" type="join"><img
	src="${ctx}/style/js/snaker/images/16/gateway_parallel.png" />&nbsp;&nbsp;结合</div>
</div>

<div id="properties">
<div id="properties_handle">属性</div>
<table class="properties_all" cellpadding="0" cellspacing="0">
</table>
<div>&nbsp;</div>
</div>

<div id="snakerflow" style=""></div>
</body>
</html>
