<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>流程处理页面</title>
		<script src="<%=path%>/common/script/context.js"
			type="text/javascript"></script>
	    <script type="text/javascript">
	    $(function (){	
		  var api = frameElement.api, W = api.opener;
		  <c:if test="${empty error}">
			  //返回信息
			  window.onload=function(){ 
			    if(api.get("Wdialog")){
					api.reload(api.get("dialog"));
				 }else{
					 W.location.reload();//刷新父页面
				}
				 api.close();
		      }
	      </c:if>
	    
	});
	    </script>
	</head>
        <c:if test="${not empty error}">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" id="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="alert alert-block alert-danger">
								${message }
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<body>
	</body>
</html>
