<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
	<title>合同审批单 </title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%= path %>/common/script/workFlow/workFlow.js" type="text/javascript" ></script>
	<script type="text/javascript">
		
		var api = frameElement.api, W = api.opener;
		
	</script>
</head>
 
<body>
<form id="supInfo" name="" method="post" action="saveInitiatorToApply_bidworkflow.action">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
		<div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
         
				<li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab1');"><b>基本信息</b></a></li>
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTab('TabPage1','Tab2');"><b class="spxx">审批记录（<font color="red">${fn:length(hytList) }</font>）</b></a></li>
            </ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">
    	<table class="table_ys1"  width="100%">		        	
            <tr>
			    <td width="15%" class="Content_tab_style1">项目名称：</td>
			    <td width="35%" class="Content_tab_style2">
			       ${contractInfo.projectName }</td>
			    <td width="15%" class="Content_tab_style1">项目编号：</td>
			    <td width="35%" class="Content_tab_style2">
			       ${contractInfo.bidCode }</td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同编号：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractCode }
			    </td>
			    <td class="Content_tab_style1">合同名称：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractName }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同分类：</td>
			    <td class="Content_tab_style2">
				    ${contractInfo.contractType }
			    </td>
			    <td class="Content_tab_style1">项目分类：</td>
			    <td class="Content_tab_style2">
			      ${contractInfo.projectType }
			    </td>
			</tr>
           <tr>
			    <td class="Content_tab_style1">甲方名称：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractPersonNameA }
			    </td>
			    <td class="Content_tab_style1">乙方名称：</td>
			    <td class="Content_tab_style2">
				    <a href="javascript:void()" onClick="createdetailwindow('查看供应商明细','viewSupplierSelectInfo_supplierBaseInfo.action?supplierInfo.supplierId=${contractInfo.supplierId}',1)">${contractInfo.contractNameB }</a>
				 </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方地址：</td>
			    <td class="Content_tab_style2">
			        ${contractInfo.contractAddressA }
			    </td>
			    <td class="Content_tab_style1">乙方地址：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractAddressB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方联系人：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractUndertaker }
			    </td>
			    <td class="Content_tab_style1">乙方联系人：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractPersonB }
			    </td>
			</tr>
			 <tr>
			    <td class="Content_tab_style1">甲方联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractMobileA }
			    </td>
			    <td class="Content_tab_style1">乙方联系人手机：</td>
			    <td class="Content_tab_style2">
			       ${contractInfo.contractMobileB }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方电话：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractTelA }
			    </td>
			    <td class="Content_tab_style1">乙方电话：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractTelB }
			     </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">甲方传真：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractFaxA }
			    </td>
			    <td class="Content_tab_style1">乙方传真：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.contractFaxB }
			    </td>
			</tr>
			<tr>
			    <td class="Content_tab_style1">银行账号：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.bankAccount }
			    </td>
			    <td class="Content_tab_style1">银行户名：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.bankAccountName }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">开户银行：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.bank }
			    </td>
			    <td class="Content_tab_style1">税号：</td>
			    <td class="Content_tab_style2">
			    ${contractInfo.dutyParagraph }
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">合同金额：</td>
			    <td class="Content_tab_style2">
			       <fmt:formatNumber value="${contractInfo.contractMoney }" pattern="#,##0.00"/>
			   </td>
			    <td class="Content_tab_style1">货币类型：</td>
			    <td class="Content_tab_style2">
			        ${contractInfo.conMoneyType}
				</td>
			</tr>
            
            <tr>
			    <td class="Content_tab_style1">合同审批原件：</td>
			    <td class="Content_tab_style2" colspan="3">
			      <c:if test="${not empty appFile}">
                          <c:out value="${appFile}" escapeXml="false"/>
			      </c:if>
			    </td>
			</tr>
            <tr>
			    <td class="Content_tab_style1">备注：</td>
			    <td class="Content_tab_style2" colspan="3">${contractInfo.conRemark }</td>
			</tr>
		</table>
		<table class="table_ys1">
		<tr>
         <td  colspan="10" class="Content_tab_style_05">合同物资明细</td>
       	</tr>
		<tr align="center" class="Content_tab_style_04">
				<th width="5%" nowrap>序号</th>
				<th width="95px" nowrap>编码</th>
				<th width="15%" nowrap>名称 </th>
				<th width="25%" nowrap>规格型号 </th>
				<th width="10%" nowrap>计量单位</th>
				<th width="55px" nowrap>数量</th>
				<th width="55px" nowrap>单价</th>
				<th width="100px" nowrap>小计</th>
				<th width="100px" nowrap>备注</th>
			</tr>
				<c:set var="totalPrice" value="0"/>
				<c:forEach items="${cmList}" var="contractMaterial" varStatus="status">
				<c:set var="totalPrice" value="${totalPrice+contractMaterial.amount*contractMaterial.price }"/> 
					<tr  <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
						<td>
						   ${status.index+1}
						</td>
						<td>
						    ${contractMaterial.materialCode}
						</td>
						<td>
						   ${contractMaterial.materialName} 
						</td>
						<td>
						   ${contractMaterial.materialType} 
						</td>
						<td>
						   ${contractMaterial.unit}
					    </td>
						<td align="right">
					     ${contractMaterial.amount} 
						 </td>
					    <td align="right">
					       <fmt:formatNumber value="${contractMaterial.price}" pattern="#,##0.00"/>
					    </td>
					    <td align="right">
					       <fmt:formatNumber value="${contractMaterial.price*contractMaterial.amount}" pattern="#,##0.00"/>
					    </td>
						<td>
						${contractMaterial.remark}
						</td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="7" align="right" style="font-weight: bold;color: red">总价：</td>
					<td style="font-weight: bold;color: red" align="right">
						<span id="totalPriceStr"><fmt:formatNumber value="${totalPrice}" pattern="#,##0.00"/><!-- 小计 --></span>
					</td>
					<td>&nbsp;</td>
				</tr>	
	</table>
	<table class="table_ys1">
    	   <c:if test="${detail!='detail'}">
			<tr>
		    	<td class="Content_tab_style1" width="15%">申请人意见：</td>
				<td  width="85%" colspan="5">
					<textarea name="signContent" rows="3" id="signContent"  style="width:90%;border: solid 1px gray;" ></textarea>
				</td>
		  	</tr>
		  	</c:if>
	</table>
	<div class="buttonDiv">
    			<c:if test="${detail!='detail'}">
				<button class="btn btn-success" id="" onclick="initApplySubmit()" type="button"><i class="icon-white icon-ok-sign"></i>提交</button>
				<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
				</c:if>
				
			</div>
   </div>
 	<!-------------------- 审批记录--------------------------->
    <div id="dTab2" class="HackBox">
	   <table width="80%" class="table_ys1">
	  		<tr>
          		<td  colspan="6" class="Content_tab_style_05" style="text-align:center"><b>审批记录</b></td>
        	</tr>
	  			<tr class="Content_tab_style_04" >
	  				<th width="5%" nowrap>序号</th>
	  				<th width="10%" nowrap>审批环节</th>
					<th width="10%" nowrap>审批人</th>
					<th width="10%" nowrap>审批时间</th>
					<th width="10%" nowrap>审批结果 </th>
					<th width="30%" nowrap>审批意见</th>
	       	    </tr>
	       	    <c:forEach items="${hytList}" var="hy" varStatus="status">
				<tr <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>
					<c:out escapeXml="false" value="${hy.displayName}" />&nbsp;
				</td>
				<td>
					${hy.operatorCn}
				</td>
				<td>
					${hy.finishTime}
				</td>
				<td>
					<c:choose><c:when test="${hy.performType=='1'}">通过</c:when><c:when test="${hy.performType=='-1'}">不通过</c:when><c:otherwise></c:otherwise></c:choose>&nbsp;&nbsp;
				</td>
				<td align="left">${hy.variable}</td>
			 </tr>
			  </c:forEach>
    	</table>		
		</div>	
	</div>
</div>
</form>
</body></html>