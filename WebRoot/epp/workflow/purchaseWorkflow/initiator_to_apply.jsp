<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
	<title>项目立项审批单 </title>
	<script src="<%= path %>/common/script/context.js" type="text/javascript" ></script>
	<script src="<%= path %>/common/script/workFlow/workFlow.js" type="text/javascript" ></script>
	<script type="text/javascript">
		
		var api = frameElement.api, W = api.opener;
		
	</script>
<style>
	form {margin: 0;}
	textarea {display: block;}
</style>
</head>
 
<body>
<form id="supInfo" name="" method="post" action="saveInitiatorToApply_bidworkflow.action">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
		<div class="tabbable" align="center">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" class="TabBarLevel1" id="TabPage1">
         
				<li id="Tab1"  class="active"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab1','');"><b>基本信息</b></a></li>
		        <c:choose>
				    <c:when test="${requiredCollect.buyWay=='00'}">
				       <li id="Tab3"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewTenderBidListDetail_tenderBidList.action?rcId=${requiredCollect.rcId}');"><b>招标计划</b></a></li>
				       <li id="Tab4"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewTenderBidFileDetail_tenderBidFile.action?rcId=${requiredCollect.rcId}');"><b>招标文件</b></a></li>
				       <li id="Tab5"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewBidBulletinBidDetail_bidBulletin.action?rcId=${requiredCollect.rcId}');"><b>招标公告</b></a></li>
		            </c:when>
		            <c:when test="${requiredCollect.buyWay=='01'}">
				       <li id="Tab3"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewAskBidListDetail_askBidList.action?rcId=${requiredCollect.rcId}');"><b>询价计划</b></a></li>
				       <li id="Tab4"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewBidBulletinBidDetail_bidBulletin.action?rcId=${requiredCollect.rcId}');"><b>询价公告</b></a></li>
		            </c:when>
		            <c:otherwise>
		               <li id="Tab3"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewBiddingBidListDetail_biddingBidList.action?rcId=${requiredCollect.rcId}');"><b>竞价方案</b></a></li>
				       <li id="Tab4"><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab3','viewBidBulletinBidDetail_bidBulletin.action?rcId=${requiredCollect.rcId}');"><b>竞价公告</b></a></li>
		            </c:otherwise>
				</c:choose>
		        
		        <li id="Tab2" ><a data-toggle="tab" onFocus="blur()" onclick="javascript:switchTabUrl('Tab2','');"><b class="spxx">审批记录（<font color="red">${fn:length(hytList) }</font>）</b></a></li>
            </ul>
            
            <div id="cnt">
               	<!-------------------- 基本信息--------------------------->
       <div id="dTab1" class="HackBox" style="display:block">
    	<table align="center"  class="table_ys1">
		<tr align="center" class="Content_tab_style_05"><th colspan="4">${buyWayCn }项目</th></tr>
		<tr >
			 <td width="15%" class="Content_tab_style1">项目名称：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.buyRemark }
			 </td>
			<td width="15%" class="Content_tab_style1">项目编号：</td>
			 <td width="35%" align="left">
			 	${requiredCollect.bidCode }				
			 </td>
		</tr>
		<tr>	 
			 <td width="15%" nowrap class="Content_tab_style1">采购方式：</td>
			 <td width="35%" align="left">
			 	${buyWayCn }	
			 </td>
			 <td width="15%" class="Content_tab_style1">招标方式：</td>
			 <td width="35%" align="left">
			 	${supplierTypeCn }	
			 </td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>预算金额：</td>
			<td class="Content_tab_style2">
				<fmt:formatNumber value="${requiredCollect.totalBudget }" pattern="##0.00"/>
			</td>
			<td class="Content_tab_style1" nowrap>采购组织：</td>
			<td class="Content_tab_style2">
				${purchaseDeptName }
			</td>
		</tr>
		<tr>
			<td class="Content_tab_style1" nowrap>申请人：</td>
			<td class="Content_tab_style2">
				${creator }
			</td>
			<td class="Content_tab_style1" nowrap>申请部门：</td>
			<td class="Content_tab_style2">
				${deptName }
			</td>
		</tr>
		<tr>
			
			<td class="Content_tab_style1" nowrap>申请日期：</td>
			<td class="Content_tab_style2">
				${createTime}
			</td>		
			<td class="Content_tab_style1" nowrap></td>
			<td class="Content_tab_style2"> 
			</td>
		</tr>
		<tr>
			<td  class="Content_tab_style1">附件列表：</td>
			<td class="Content_tab_style2" colspan="3">
				<c:out value="${requiredCollect.attachmentUrl}" escapeXml="false"/>				
			</td>
		</tr>
	</table>
	<table align="center" class="table_ys1" id="listtable">	
		
		<tr id="tdNum" align="center" class="Content_tab_style_04">
			<th width="5%" nowrap>序号</th>
			<th width="95px" nowrap>编码</th>
			<th width="10%" nowrap>名称 </th>
			<th width="10%" nowrap>规格型号 </th>
			<th width="10%" nowrap>计量单位</th>
			<th width="55px" nowrap>数量</th>
			<th width="100px" nowrap>交货时间</th>
			<th width="20%" nowrap>备注</th>
		</tr>
		<c:forEach items="${listValue}" var="requiredCollectDetail" varStatus="status">
			<tr  class="input_ys1">
				<td>${status.index+1}
				</td>
				<td>
				    ${requiredCollectDetail.buyCode}
				</td>
				<td>
				   ${requiredCollectDetail.buyName}
				</td>
				<td>
				   ${requiredCollectDetail.materialType}
				</td>
				<td>
				   ${requiredCollectDetail.unit}
			    </td>
			    <td align="right">
			       ${requiredCollectDetail.amount}
			    </td>
				<td>
					<fmt:formatDate value="${requiredCollectDetail.deliverDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />
				</td>
				<td>${requiredCollectDetail.remark }</td>
			</tr>
		</c:forEach>		
	</table>
	<table class="table_ys1">
    	   <c:if test="${detail!='detail'}">
			<tr>
		    	<td class="Content_tab_style1" width="15%">申请人意见：</td>
				<td  width="85%" colspan="5">
					<textarea name="signContent" rows="3" id="signContent"  style="width:90%;border: solid 1px gray;" ></textarea>
				</td>
		  	</tr>
		  	</c:if>
	</table>
	<div class="buttonDiv">
    			<c:if test="${detail!='detail'}">
				<button class="btn btn-success" id="" onclick="initApplySubmit()" type="button"><i class="icon-white icon-ok-sign"></i>提交</button>
				<button class="btn btn-cacel" id="btn-cacel" type="reset" ><i class="icon-info-sign"></i>重置</button>
				</c:if>
				
			</div>
   </div>
 	<!-------------------- 审批记录--------------------------->
    <div id="dTab2" class="HackBox">
	   <table width="80%" class="table_ys1">
	  		<tr>
          		<td  colspan="6" class="Content_tab_style_05" style="text-align:center"><b>审批记录</b></td>
        	</tr>
	  			<tr class="Content_tab_style_04" >
	  				<th width="5%" nowrap>序号</th>
	  				<th width="10%" nowrap>审批环节</th>
					<th width="10%" nowrap>审批人</th>
					<th width="10%" nowrap>审批时间</th>
					<th width="10%" nowrap>审批结果 </th>
					<th width="30%" nowrap>审批意见</th>
	       	    </tr>
	       	    <c:forEach items="${hytList}" var="hy" varStatus="status">
				<tr <c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
				<td>${status.index+1}</td>
				<td>
					<c:out escapeXml="false" value="${hy.displayName}" />&nbsp;
				</td>
				<td>
					${hy.operatorCn}
				</td>
				<td>
					${hy.finishTime}
				</td>
				<td>
					<c:choose><c:when test="${hy.performType=='1'}">通过</c:when><c:when test="${hy.performType=='-1'}">不通过</c:when><c:otherwise></c:otherwise></c:choose>&nbsp;&nbsp;
				</td>
				<td align="left">${hy.variable}</td>
			 </tr>
			  </c:forEach>
    	</table>		
	</div>	
	<!------------------------信息详情------------------------------ -->
    <div id="dTab3" class="HackBox">
       <iframe src=""
					frameborder=0 scrolling="yes" width="100%" id="iframe"
					height="400px" ></iframe>
    </div>
	</div>
</div>
</form>
</body></html>