function Query(){	
	startIndex=0;
    $("#xmForm").submit();
}
function ajaxGeneral(action,param,type){
	var result = "";
	$.ajax({
		type:"POST",
		async:false,
		url:action,
		data:param,
		dataType:type,
		success:function(msg){
			result = msg;
		},
		error:function(error){
			result = error;
		}
	});
	return result;
}
function DateFormat(date,mask)
{
    var d ="";
    if(mask==1) d=date.substring(5,16);
    else if(mask==2) d=date.substring(5,10);
    else if(mask==3) d=date.substring(0,10);
    return d;
};
//js 数字计算防止误差  两数相加
function FloatAdd(arg1,arg2){   
	   var r1,r2,m;   
	   try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}   
	   try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}   
	   m=Math.pow(20,Math.max(r1,r2))   
	   return (arg1*m+arg2*m)/m   
	 } 
//js 数字计算防止误差  两数相减
function FloatSub(arg1,arg2){   
	var r1,r2,m,n;   
	try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}   
	try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}   
	m=Math.pow(10,Math.max(r1,r2));     
	n=(r1>=r2)?r1:r2;   
	return ((arg1*m-arg2*m)/m).toFixed(n);   
	} 
//数字校验
	function validateNum(obj){
	   if(isNaN(obj.value)){
	      document.getElementById(obj.id).value="";
	      return ;
	   }
	}