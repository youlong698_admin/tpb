var pageSize = Math.ceil((window.screen.height-138)/112); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
var bidderType=0,publishType="",bidInfo="";


$(function(){
    //发布时间
    $("#publicDate-mui-scroll .mui-table-view-cell").click(function () {
        var val = $(this).attr("val");
        $("#publishType").val(val);
        Query();
    });
});
   mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   mui('body').on('tap','a',function(){document.location.href=this.href;});
   
   var bidNameValueHtml = document.getElementById("bidNameValue");
    /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		bidderType=$("#bidderType").val();
		publishType=$("#publishType").val();
		bidInfo=$("#bidInfo").val();
		var resultData=ajaxGeneral("purchaseQuery_webMobile.action",{ pageSize: pageSize, startIndex: startIndex,  bidderType: bidderType, publishType: publishType, bidInfo: bidInfo },"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#xmQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell mui-media xmxx-item';
					var buyWayCn="";
					if(data[i].buyWay=='00') buyWayCn="招标";
					else if(data[i].buyWay=='01') buyWayCn="询价";
					else if(data[i].buyWay=='02') buyWayCn="竞价";
					
					li.setAttribute("bbId",data[i].bbId);
					
					var html = '<a href="javascript:;">';
					html+= '<div class="mui-media-body">';
					html+= '<div class="purchase-hd">';
					html+= '<strong><span>'+data[i].bulletinTitle+'</span></strong>';
					html+= '</div>';
					html+= '<ul class="purchase-tag">';
					html+= '<li class="purchase-type purchase-type-'+data[i].buyWay+'">';
					html+= buyWayCn;
					html+= '</li>';
					html+= '</ul>';
					html+= '<div class="purchase-fd row">';
					html+= '<div class="col purchase-meta">';
					html+= '<span>'+data[i].returnDate+'截止</span><span>&nbsp;&nbsp;</span>';
					html+= '</div>';
					html+= '</div>';
					html+= '</div>';
					html+= '</a>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
				//长按跳转至审批详情
			    mui('body').off('tap', '.xmxx-item');
			    mui('body').on('tap', '.xmxx-item', function () {
			        var bbId = $(this).attr("bbId");
			        doXmApplication(bbId);
			    });
			    mui('body').off('doubletap', '.xmxx-item');
			    mui('body').on('doubletap', '.xmxx-item', function () {
			        var bbId = $(this).attr("bbId");
			        doXmApplication(bbId);
			    });
	     }
	  //选择招标类型		  
	  mui('body').on('tap', '.mui-popover-action li>a', function() {
		var a = this,
			parent;
		//根据点击按钮，反推当前是哪个actionsheet
		for (parent = a.parentNode; parent != document.body; parent = parent.parentNode) {
			if (parent.classList.contains('mui-popover-action')) {
				break;
			}
		}
		//关闭actionsheet
		mui('#' + parent.id).popover('toggle'); 
		var val = $(this).attr("val");
		$("#bidderType").val(val);
		Query();
	});
	//
	mui('#area-mui-scroll').scroll();
	mui('#hangye-mui-scroll').scroll();
	mui('body').on('shown', '.mui-popover', function(e) {
			//console.log('shown', e.detail.id);//detail为当前popover元素
		});
	mui('body').on('hidden', '.mui-popover', function(e) {
		//console.log('hidden', e.detail.id);//detail为当前popover元素
	});
	
	//项目报名页面
	function doXmApplication(bbId){		
		window.location.href="viewBidBulletinContentMobile_webMobile.action?id="+bbId;
	}
	