var $loginBtn = $('.login_btn');
$(function(){

 // 切换表单类型
  $('.form-type').on('click', 'li > a', function(event) {
    event.preventDefault();
    formType = $(this).data('formtype');
    $(this).closest('.form-type').find('a').removeClass('active');
    $(this).addClass('active');

    if (formType == 'second') {
      $("#loginType").val(2);
    } else{
      $("#loginType").val(1);
    }
  });
   
   $('#password').on('keypress',function(event){
        if(event.keyCode == "13"){
            $loginBtn.trigger('click');
        }
  });		    
   
})


function doCheck(){
	var username = $("#username").val();
	var password   = $("#password").val();
	var loginType   = $("#loginType").val();
	if(username=="") {
		mui.toast('用户名不能为空！');
		return false;
	}
	
	if(password=="") {
		mui.toast('密码不能为空！');
		return false;
	} 
	
	// 点击登录
   $.ajax({
       url: 'verifyLogin_webMobile.action',
       type:"post",
       data:{
         "loginType": loginType,
         "username":username,
         "password":password
       },
       dataType: 'json',
       success: function (result) {
           if(result&&result.success)
           {
               loginSuccess(result);
           }
           else
           {
               if(result&&result.errcode)
               {
                   loginFail(result);
               }
               else
               {
                    mui.toast("系统错误，请稍后重试");
               }
           }
       },
       error: function () {
            mui.toast('系统异常，请稍后重试');
       }
   });
}
 function loginSuccess(result)
    {
	     window.document.location.replace("about_webMobile.action");
    }

    function loginFail(result)
    {
         switch (result.errcode) {
            case 1:
                 mui.toast("该用户名已禁用");
                break;
            case 2:
                 mui.toast("用户名与密码不匹配，请重新输入");
                break;
            }
    }