var pageSize = Math.ceil((window.screen.height-44)/100); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
var wfdName="",wfType="";
$(function(){
	$("#btnQuery").click(function () {
		$("#searchBarTitle").css("display","none");
		$("#searchQuery").css("display","block");
	});
	$("#cacel").click(function () {
		$("#searchBarTitle").css("display","block")
		$("#searchQuery").css("display","none");
	});
	//行业
    $("#spMenu-mui-scroll .mui-table-view-cell").click(function () {
        var val = $(this).attr("val");
        $("#wfType").val(val);
        Query();
    });
	
});
mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		wfdName=$("#wfdName").val();
		wfType=$("#wfType").val();
		var resultData=ajaxGeneral("findWaitForDeal_viewWaitForDeal.action",{ pageSize: pageSize, startIndex: startIndex, wfdName: wfdName, wfType: wfType},"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#spxxQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell mui-media spxx-item';
					var processName=data[i].processName,str="";
					if(processName=='计划审批') str="<span class=\"mui-badge sp jhsp\">计划</span>";
					else if(processName=='采购立项审批') str="<span class=\"mui-badge sp lxsp\">立项</span>";
					else if(processName=='采购结果审批') str="<span class=\"mui-badge sp jgsp\">结果</span>";
					else if(processName=='合同审批') str="<span class=\"mui-badge sp htsp\">合同</span>";
					else if(processName=='订单审批') str="<span class=\"mui-badge sp ddsp\">订单</span>";
					else if(processName=='供应商考核') str="<span class=\"mui-badge sp pjsp\">评价</span>";
					else if(processName=='资金计划审批') str="<span class=\"mui-badge sp zzsp\">资金</span>";
					
					
					li.setAttribute("processname",processName);
			        li.setAttribute("actionurl", data[i].actionUrl);
					
					var html = '<a href="javascript:;">';
					html+= '<div class="mui-media-object mui-pull-left">';
					html+= str;
					html+= '</div>';
					html+= '<div class="mui-media-body">';
					html+= '<div class="mui-table">';
					html+= '<div class="mui-table-cell">';
					html+= '<div class="spTitle mui-ellipsis">';
					html+= '<strong><span>'+data[i].wfName+'</span></strong>';
					html+= '</div>';
					html+= '<ul class="spmx">';
					html+= '<li>申请人:'+data[i].creatorCn+'</li>';
					//html+= '<li>流程环节：'+data[i].taskName+'</li>';
					//html+= '<li>上一步处理人:'+data[i].lastUpdatorCn+'</li>';
					html+= '<li>申请时间:'+DateFormat(data[i].orderCreateTime,1)+'</li>';
					html+= '</ul>';
					html+= '</div>';
					html+= '</div>';
					html+= '</div>';
					html+= '</a>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
				//长按跳转至审批详情
			    mui('body').off('tap', '.spxx-item');
			    mui('body').on('tap', '.spxx-item', function () {
			        var processName = $(this).attr("processname");
			        var actionUrl = $(this).attr("actionurl");
			        doWorkFlow(processName,actionUrl);
			    });
			    mui('body').off('doubletap', '.spxx-item');
			    mui('body').on('doubletap', '.spxx-item', function () {
			        var processName = $(this).attr("processname");
			        var actionUrl = $(this).attr("actionurl");
			        doWorkFlow(processName,actionUrl);
			    });
	     }
	
		//跳转至审批页面
	    function doWorkFlow(processName,actionUrl){
	 	   if(processName=="供应商考核"){
	 		   mui.alert("供应商考核流程请登录电脑进行操作", "提示");
	 	   }else{
	 		   window.location.href=path+actionUrl+"&from=0";
	 	   }
	    }
	