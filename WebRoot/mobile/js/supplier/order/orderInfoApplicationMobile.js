var pageSize = Math.ceil((window.screen.height-44)/100); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
var orderName="";
$(function(){
	$("#btnQuery").click(function () {
		$("#searchBarTitle").css("display","none");
		$("#searchQuery").css("display","block");
	});
	$("#cacel").click(function () {
		$("#searchBarTitle").css("display","block")
		$("#searchQuery").css("display","none");
	});
	
});
mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		orderName=$("#orderName").val();
		var resultData=ajaxGeneral("findOrderInfoApplication_orderInfoSupplier.action?sgin=1",{ pageSize: pageSize, startIndex: startIndex,orderColumn:"de.oiId",orderDir:"desc", orderName: orderName},"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#xmxxQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell mui-media xmxx-item';
					
					li.setAttribute("oiId", data[i].oiId);
					
					var html = '<a href="javascript:;">';
					html+= '<div class="left">';
					html+= '<div class="mui-table">';
					html+= '<div class="mui-table-cell">';
					html+= '<div class="xmTitle mui-ellipsis">';
					html+= '<strong><span>'+data[i].orderName+'</span></strong>';
					html+= '</div>';
					html+= '<ul class="xmmx">';
					html+= '<li>订单编号:'+data[i].orderCode+'</li>';
					html+= '<li>乙方名称:'+data[i].orderNameB+'</li>';
					html+= '</ul>';
					html+= '</div>';
					html+= '</div>';
					html+= '</div>';
					html+= '<div class="right">';
					html+= '<span class="orderMoneyDiv">';
					html+= '￥'+data[i].orderMoney;
					html+= '</span>';
					html+= '</div>';
					html+= '</a>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
			//长按跳转至项目详情
		    mui('body').off('tap', '.xmxx-item');
		    mui('body').on('tap', '.xmxx-item', function () {
		        var oiId = $(this).attr("oiId");
		        doUrl(oiId);
		    });
		    mui('body').off('doubletap', '.xmxx-item');
		    mui('body').on('doubletap', '.xmxx-item', function () {
		    	var oiId = $(this).attr("oiId");
		    	doUrl(oiId);
		    });
				
	     }
	
	 //跳转至项目页面
       function doUrl(oiId){
    	   window.location.href="saveConfirmOrderInfoInit_orderInfoSupplier.action?orderInfo.oiId="+oiId+"&from=0";
       }
	