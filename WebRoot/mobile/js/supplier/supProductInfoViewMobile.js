var pageSize = Math.ceil((window.screen.height-44)/100); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		var resultData=ajaxGeneral("viewToSupplierProductInfoMobile_supplierProductInfoSupplier.action",{ pageSize: pageSize, startIndex: startIndex,orderColumn:"de.spiId",orderDir:"desc"},"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#supplierProductInfoQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell mui-media pixx-item  mui-col-xs-6';
					
					li.setAttribute("spiId", data[i].spiId);
					
					var html = '<a href="javascript:;">';
					html+= '<img class="mui-media-object" src="/fileWeb/'+data[i].img+'">';
					html+= '<div class="mui-media-body">'+data[i].productName+'</div>';
					if(data[i].price==null||data[i].price==0){
						html+= '<div class="money">面议</div>';
					}else{
					    html+= '<div class="money">￥:'+data[i].price+'</div>';
					}
					html+= '</a>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
			//长按跳转至项目详情
		    mui('body').off('tap', '.pixx-item');
		    mui('body').on('tap', '.pixx-item', function () {
		        var spiId = $(this).attr("spiId");
		        doUrl(spiId);
		    });
		    mui('body').off('doubletap', '.pixx-item');
		    mui('body').on('doubletap', '.pixx-item', function () {
		    	var spiId = $(this).attr("spiId");
		    	doUrl(spiId);
		    });
				
	     }
	
	 //跳转至项目页面
       function doUrl(spiId){
    	   window.location.href="viewSupplierProductInfoDetail_supplierProductInfoSupplier.action?supplierProductInfo.spiId="+spiId+"&from=0";
       }
	