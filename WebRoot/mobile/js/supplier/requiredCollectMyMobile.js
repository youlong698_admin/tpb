var pageSize = Math.ceil((window.screen.height-44)/100); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
var buyRemark="",buyWay="";
$(function(){
	$("#btnQuery").click(function () {
		$("#searchBarTitle").css("display","none");
		$("#searchQuery").css("display","block");
	});
	$("#cacel").click(function () {
		$("#searchBarTitle").css("display","block")
		$("#searchQuery").css("display","none");
	});
	//行业
    $("#xmMenu-mui-scroll .mui-table-view-cell").click(function () {
        var val = $(this).attr("val");
        $("#buyWay").val(val);
        Query();
    });
	
});
mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		buyRemark=$("#buyRemark").val();
		buyWay=$("#buyWay").val();
		var resultData=ajaxGeneral("findRequiredCollectMy_requiredCollectSupplier.action",{ pageSize: pageSize, startIndex: startIndex,orderColumn:"de.rcId",orderDir:"desc", buyRemark: buyRemark, buyWay: buyWay},"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#xmxxQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell mui-media xmxx-item';
					var buyWay=data[i].buyWay,str="";
					if(buyWay=='00') str="<span class=\"mui-badge xm zb\">招标</span>";
					else if(buyWay=='01') str="<span class=\"mui-badge xm xj\">询价</span>";
					else if(buyWay=='02') str="<span class=\"mui-badge xm jj\">竞价</span>";
					

			        li.setAttribute("rcId", data[i].rcId);
					
					var html = '<a href="javascript:;">';
					html+= '<div class="mui-media-object mui-pull-left">';
					html+= str;
					html+= '</div>';
					html+= '<div class="mui-media-body">';
					html+= '<div class="mui-table">';
					html+= '<div class="mui-table-cell">';
					html+= '<div class="xmTitle mui-ellipsis">';
					html+= '<strong><span>'+data[i].buyRemark+'</span></strong>';
					html+= '</div>';
					html+= '<ul class="xmmx">';
					html+= '<li>项目编号:'+data[i].bidCode+'</li>';
					html+= '</ul>';
					html+= '</div>';
					html+= '</div>';
					html+= '</div>';
					html+= '</a>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
			//长按跳转至项目详情
		    mui('body').off('tap', '.xmxx-item');
		    mui('body').on('tap', '.xmxx-item', function () {
		        var rcId = $(this).attr("rcId");
		        doUrl(rcId);
		    });
		    mui('body').off('doubletap', '.xmxx-item');
		    mui('body').on('doubletap', '.xmxx-item', function () {
		    	var rcId = $(this).attr("rcId");
		    	doUrl(rcId);
		    });
				
	     }
	
	 //跳转至项目页面
       function doUrl(rcId){
    	   window.location.href="viewRequiredCollectDetailMobile_requiredCollectSupplier.action?requiredCollect.rcId="+rcId;
       }
	