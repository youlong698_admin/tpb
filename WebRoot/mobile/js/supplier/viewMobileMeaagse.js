var pageSize = Math.ceil((window.screen.height-44)/80); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		var resultData=ajaxGeneral("findMessages_supplierMain.action",{ pageSize: pageSize, startIndex: startIndex},"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#messageQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = 'mui-table-view-cell mui-media';					

			        li.setAttribute("smlId", data[i].smlId);
					
					var html = '';
					if(data[i].status != "1"){
						html+= '<div class="mui-slider-left mui-disabled">';
						html+= '<a class="mui-btn mui-btn-red" smlId="'+data[i].smlId+'">未读</a>';
						html+= '</div>';	
						html+='<div class="mui-slider-handle text-negative text" id="message'+data[i].smlId+'">';				
					}else{
						html+='<a href="javascript:;">';
						html+='<div class="text">';
					}
					html+= '<div class="mui-ellipsis mui-pull-left" style="width:75%">';
					html+= data[i].messageContent;
					html+= '</div>';
					html+= '<div class="mui-pull-right" style="width:25%;text-align:right">';
					html+= DateFormat(data[i].sendDate,1);
					html+= '</div>';
					html+= '</div>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
				 //向左滑动，未读变已读
			    mui('body').off('slideright', '.mui-table-view-cell');
			    mui('body').on('slideright', '.mui-table-view-cell', function (event) {
			        var smlId = $(this).attr("smlId");
			        ajaxGeneral("updateMessage_supplierMain.action","messageId="+smlId,"text");
			        mui.swipeoutClose(this);
			        $("#message"+smlId).removeClass("mui-slider-handle text-negative");
			        $(this).find(".mui-slider-left").remove();
			    });
				
	     }
	