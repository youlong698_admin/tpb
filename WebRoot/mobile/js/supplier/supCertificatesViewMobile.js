var pageSize = Math.ceil((window.screen.height-44)/100); //每页取多少条
var startIndex = 0; //起始数
var pageCur = 0; //当前页数
var sumcount = 0; //总记录数
mui.init({
	 //下来刷新
		pullRefresh: {
				container: '#pullrefresh',
				up: {
		            callback: pullupRefresh,
		            contentinit: '上拉显示更多',
		            contentdown: '上拉显示更多',
		            contentrefresh: '正在加载...',
		            contentnomore: '没有更多数据了'
		        }
			}
	});
   /**
	* 上拉加载具体业务，在尾部加载新内容
	*/
	function pullupRefresh() {
	    setTimeout("ajaxLoaddata()", 200);
	}
	if (mui.os.plus) {
	    mui.plusReady(function () {
	        setTimeout(function () {
	            mui('#pullrefresh').pullRefresh().pullupLoading();
	        }, 1000);
	
	    });
	} else {
	    mui.ready(function () {
	        mui('#pullrefresh').pullRefresh().pullupLoading();
	    });
	}
	var count=0;
	//异步加载数据
	function ajaxLoaddata() {
		pageCur = startIndex < sumcount ? ++pageCur : pageCur;
		startIndex=pageSize * pageCur;
		var resultData=ajaxGeneral("viewToSupplierCertificates_supCertificatesSupplier.action",{ pageSize: pageSize, startIndex: startIndex,orderColumn:"de.sciId",orderDir:"desc"},"json");
		var data = resultData.pageData;
		sumcount = resultData.total;
	    mui('#pullrefresh').pullRefresh().endPullupToRefresh((startIndex >= sumcount)); //参数为true代表没有更多数据了。
				var table = document.body.querySelector('#supCertificatesQuery');
				for (var i =0; i <data.length;i++) {
					var li = document.createElement('li');
					li.className = '';
					
					
					var html = '<a href="javascript:;">';
					html+= '<div class="mui-card">';
					html+='<div class="mui-card-header">';
					html+=data[i].certificateName;
					html+= '</div>';
					html+='<div class="mui-card-header">';
					html+='<div class="mui-card-content-inner">';
					html+='<p><label>资质证书编号:</label><span>'+data[i].certificateCode+'</span></p>';
					html+='<p><label>资质专业:</label><span>'+data[i].certificateProfession+'</span></p>';
					html+='<p><label>有效期起:</label><span>'+data[i].timeBegain+'</span></p>';
					html+='<p><label>有效期止:</label><span>'+data[i].timeEnd+'</span></p>';
					html+= '</div>';			
					html+= '</div>';
					html+='<div class="mui-card-footer" style="float:left">';
					html+=''+data[i].attachmentUrl+'';
					html+= '</div>';			
					html+= '</div>';
					html+= '</a>';
					li.innerHTML=html;
					table.appendChild(li);
	        }
				
	     }
	