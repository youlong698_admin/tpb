<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>采购项目</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<style>
.drop-down-select {
	height: 300px;
	width: 98%;
}
</style>
<script type="text/javascript">
    var path="<%=path%>";
</script>
	</head>
	<body>
	<form id="xmForm" name="form" method="post" target="_self" action="xmSearch_webMobile.action">
	<c:set var="bidName" value="询价"></c:set>
		<c:choose>
			<c:when test="${bidderType==0 }"><c:set var="bidName" value="全部"></c:set></c:when>
			<c:when test="${bidderType==1 }"><c:set var="bidName" value="招标"></c:set></c:when>
			<c:when test="${bidderType==2 }"><c:set var="bidName" value="询价"></c:set></c:when>
			<c:when test="${bidderType==3 }"><c:set var="bidName" value="竞价"></c:set></c:when>
		</c:choose>		
         <input type="hidden" id="bidderType" name="bidderType" value="${bidderType }" />
         <input type="hidden" id="publishType" name="publishType" value="${publishType }" />
		<div class="mui-inner-wrap container page-search-detail">
			<header class="mui-bar  mui-bar-nav bar top-search">
			<div class="searchbar">
				<a href="#searchType">
					<div class="searchType">
						<span  id="bidNameValue">${bidName }</span><span class="mui-icon mui-icon-arrowdown icon"></span>
					</div> 
				</a>
				<div>
					<span></span><span></span>
				</div>
				<div class="search-input">
					<span class="mui-icon icon mui-icon-search  search-icon"></span>
					<input placeholder="搜索 ${bidName }" id="bidInfo" name="bidInfo" value="${bidInfo }" type="text">
				</div>
				<button class="btn btn-link search-btn" onclick="Query()" type="button">
					<span>搜索</span>
				</button>
			</div>
			</header>
			<nav class="mui-bar mui-bar-tab">
				<a id="defaultTab" class="mui-tab-item" href="index_webMobile.action">
					<span class="mui-icon iconfont icon-ziyuan"></span>
					<span class="mui-tab-label">首页</span>
				</a>
				<a id="xmTab" class="mui-tab-item  mui-active" href="xmSearch_webMobile.action">
					<span class="mui-icon iconfont icon-xiangmu"></span>
					<span class="mui-tab-label">采购项目</span>
				</a>
				<a id="abountTab" class="mui-tab-item" href="about_webMobile.action">
					<span class="mui-icon iconfont icon-wode"></span> <span class="mui-tab-label">我的</span>
				</a>
		    </nav>
			<div class="mui-content">
				<div id="slider" class="mui-slider mui-fullscreen">
					<div id="sliderSegmentedControl"
						class="mui-scroll-wrapper mui-slider-indicator mui-segmented-control mui-segmented-control-inverted">
						<div class="mui-scroll" style="width: 100%">
							<a class="mui-control-item" href="#publicDateMiddlePopover"> 发布时间 <span
								class="mui-icon icon iconfont icon-icon"></span> </a>
						</div>
					</div>
					<div class="mui-slider-group">
						<!--下拉刷新容器-->
						<div id="pullrefresh" class="mui-scroll-wrapper purchase-view">
							<div class="mui-scroll">
								<!--数据列表-->
								<ul class="mui-table-view" id="xmQuery">
									
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="searchType"
			class="mui-popover mui-popover-action mui-popover-bottom">
			<ul class="mui-table-view">
				<li class="mui-table-view-cell">
					<a href="#" val="0">全部</a>
				</li>
				<li class="mui-table-view-cell">
					<a href="#" val="1">招标</a>
				</li>
				<li class="mui-table-view-cell">
					<a href="#" val="2">询价</a>
				</li>
				<li class="mui-table-view-cell">
					<a href="#" val="3">竞价</a>
				</li>
			</ul>
			<ul class="mui-table-view">
				<li class="mui-table-view-cell">
					<a href="#forward"><b>取消</b>
					</a>
				</li>
			</ul>
		</div>
		<!--右上角弹出菜单-->
		<div id="publicDateMiddlePopover" class="mui-popover drop-down-select">
			<div class="mui-popover-arrow"></div>
			<div class="mui-scroll-wrapper" id="publicDate-mui-scroll">
				<div class="mui-scroll">
					<ul class="mui-table-view">
					    <li class="mui-table-view-cell <c:if test="${publishType==0}">curList</c:if>"  val="0">
							全部
						</li>
					    <li class="mui-table-view-cell <c:if test="${publishType==1}">curList</c:if>"  val="1">
							近三天
						</li>
						<li class="mui-table-view-cell <c:if test="${publishType==2}">curList</c:if>"  val="2">
							近一周
						</li>
						<li class="mui-table-view-cell <c:if test="${publishType==3}">curList</c:if>"  val="3">
							近一个月
						</li>
					</ul>
				</div>
			</div>

		</div>
		</form>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		<script src="<%=path%>/mobile/js/common.js"></script>
		<script src="<%=path%>/mobile/js/xmSearch.js"></script>
	</body>
</html>
