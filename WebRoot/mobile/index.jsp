<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="com.ced.sip.system.entity.SystemConfiguration"%>
<%@page import="com.ced.sip.common.BaseDataInfosUtil"%>
<%@ include file="/common/context.jsp"%>
<%SystemConfiguration systemConfiguration=BaseDataInfosUtil.convertSystemConfiguration(1L); %>
<!DOCTYPE html>
<html>
	<head lang="en">
        <title><%=systemConfiguration.getSystemCurrentDept() %>-招标采购网</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/swiper-3.4.2.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<style>
.swiper-container {
	width: 100%;
}

.swiper-container img {
	width: 100%;
	height: auto;
}
</style>
	</head>
	<body>
		<div class="mui-inner-wrap container">
			<header class="mui-bar mui-bar-nav index-bar-nav">
			<span class="logo"><img
					src="<%=path%>/mobile/images/logo.png" />
			</span>
			<div class="search-input">
				<a href="xmSearch_webMobile.action">
					<div class="search-input-txt">
						<span class="mui-icon mui-icon-search icon"></span>
						<span>找询价/招标/竞价</span>
					</div> </a>
			</div>
			</header>
			<nav class="mui-bar mui-bar-tab">
				<a id="defaultTab" class="mui-tab-item mui-active" href="index_webMobile.action">
					<span class="mui-icon iconfont icon-ziyuan"></span>
					<span class="mui-tab-label">首页</span>
				</a>
				<a id="xmTab" class="mui-tab-item" href="xmSearch_webMobile.action">
					<span class="mui-icon iconfont icon-xiangmu"></span>
					<span class="mui-tab-label">采购项目</span>
				</a>
				<a id="abountTab" class="mui-tab-item" href="about_webMobile.action">
					<span class="mui-icon iconfont icon-wode"></span> <span class="mui-tab-label">我的</span>
				</a>
		    </nav>
			<div class="mui-content index">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<!-- Slides -->
						<div class="swiper-slide">
							<a href="#"> <img src="<%=path%>/mobile/images/banner1.jpg">
							</a>
						</div>
						<div class="swiper-slide">
							<a href="#"> <img src="<%=path%>/mobile/images/banner2.jpg">
							</a>
						</div>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			
			<div class="purchase-view" id="purchase-view">
				<div class="purchase-title">
					<div class="tit">
						<span class="mui-icon icon iconfont icon-caigouxuqiu"></span><span>
							采购需求</span>
					</div>
					<div class="meta">
						<a href="xmSearch_webMobile.action?bidderType=1"><span class="purchase-location"><span>招标</span>
						</span></a>
						<a href="xmSearch_webMobile.action?bidderType=2"><span class="purchase-industry"><span>询价</span>
						</span></a>
						<a href="xmSearch_webMobile.action?bidderType=3"><span class="purchase-industry"><span>竞价</span>
						</span>
						</a>
					</div>
				</div>
				<ul class="mui-table-view">
                    <c:forEach items="${bbList}" var="bb">
					<li class="mui-table-view-cell mui-media">
						<a href="viewBidBulletinContentMobile_webMobile.action?id=${bb.bbId }">
							<div class="mui-media-body">
								<div class="purchase-hd">
									<strong><span>${bb.bulletinTitle }</span> </strong>
								</div>
								<ul class="purchase-tag">
									<c:choose>
					                    <c:when test="${bb.buyWay=='00'}">
					                        <li class="purchase-type purchase-type-00">
												招标
											 </li>
					                    </c:when>
					                    <c:when test="${bb.buyWay=='01'}">
					                         <li class="purchase-type purchase-type-01">
												询价
											 </li>
					                     </c:when>
					                    <c:otherwise>
					                         <li class="purchase-type purchase-type-02">
												竞价
											 </li>
					                    </c:otherwise>
					                 </c:choose>									
								</ul>
								<div class="purchase-fd row">
									<div class="col purchase-meta">
										<span><fmt:formatDate value="${bb.returnDate }" pattern="yyyy-MM-dd" />截止</span><span>&nbsp;&nbsp;</span>
									</div>
								</div>
								</div>
						 </a>
					</li>
					</c:forEach>
				</ul>
				<div class="mui-table-view-cell mui-media" style="text-align: center;"><a class="link" href="xmSearch_webMobile.action" type="link">点击查看更多..</a></div>
			</div>
		</div>
		</div>
		<div style="display: none"><script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1262509326'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s13.cnzz.com/z_stat.php%3Fid%3D1262509326%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));</script></div>
		<script src="<%=path%>/mobile/js/swiper-3.4.2.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.lazyload.js"></script>
		<script src="<%=path%>/mobile/js/mui.lazyload.img.js"></script>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/common/script/numberCount.js"></script>
		<script>
	mui.init();
    var lazyLoadApi = mui('#purchase-view .mui-table-view').imageLazyload({//.rich_media_content指定lazyload实施区域的div class
        autoDestroy: false,
        placeholder: '<%=path%>/mobile/images/no.gif' //在js里指定临时占位图片，需自行准备并指定正确的本地路径或远程路径
			});
    mui('body').on('tap','a',function(){document.location.href=this.href;});
	//轮播
	var mySwiper = new Swiper('.swiper-container', {
		loop : true,
		pagination : '.swiper-pagination',
		paginationClickable : true,
		lazyLoading : true,
		autoplay : 3000
	});
</script>
	</body>
</html>
