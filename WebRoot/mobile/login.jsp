<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>登陆页面</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<link rel="apple-touch-icon-precomposed" sizes="36x36" href="<%=path%>/mobile/images/app-36.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="48x48" href="<%=path%>/mobile/images/app-48.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<%=path%>/mobile/images/app-72.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="96x96" href="<%=path%>/mobile/images/app-96.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<%=path%>/mobile/images/app-144.png"/>
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/login.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
	</head>
	<body>	    
	   <div class="content">
        <div class="login_bg">
	        <section class="banner">
	        <h1 class="logo"><img src="<%=path%>/mobile/images/logo.png" alt=""/></h1>
	        <ul class="form-type">
	          <li>
	            <a data-formtype="first" class="active" href="javascript:;">采购单位<i></i></a>
	          </li>
	          <li>
	            <a data-formtype="second" href="javascript:;">供应商<i></i></a>
	          </li>
	        </ul>
	      </section>
		<form action="" name="loginForm" method="post">
            <div class="userName">
                <input type="hidden" name="loginType" id="loginType" value="1"/>
                <input type="text" name="username" id="username" placeholder="请输入用户名"/>
            </div>
            <div class="passWord">
                <input type="password" name="password" id="password" placeholder="请输入密码"/>
            </div>
            <button class="login_btn" type="button" onclick="doCheck();">登&nbsp;&nbsp;录</button>
        </form>
        <p class="copyright">Powered by 河南云企采信息科技有限公司</p>
    </div>
    </div>
    <script type="text/javascript" src="<%=path%>/mobile/js/jquery.min.js"></script>
	<script type="text/javascript" src="<%=path%>/mobile/js/mui.min.js"></script>
	<script type="text/javascript" src="<%=path%>/mobile/js/login.js"></script>
	</body>
</html>