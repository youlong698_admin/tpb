<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>${bidBulletin.bulletinTitle }</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />		
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />			
		<link rel="stylesheet" href="<%=path%>/mobile/css/bidding.css" />
	</head>
	<body>
	   <c:set var="url" value=""/>
		<div class="mui-content bidding-detail">
		    <div class="mui-content-inner">
		    <div class="mui-content-padded rec-hd">
            <div class="rec-tlt row">
                <strong class="col">
                   <c:choose>
                      <c:when test="${bidBulletin.buyWay=='00'}">
                         <span class="top-icon-00">招标</span>
                         <c:set var="url" value="saveTenderBidListApplication_tenderBidListSupplier.action"/>
                      </c:when>
                      <c:when test="${bidBulletin.buyWay=='01'}">
                         <span class="top-icon-01">询价</span>
                         <c:set var="url" value="saveAskBidListApplication_askBidListSupplier.action"/>
                      </c:when>
                      <c:otherwise>
                         <span class="top-icon-02">竞价</span>
                         <c:set var="url" value="saveBiddingBidListApplication_biddingBidListSupplier.action"/>
                      </c:otherwise>
                   </c:choose>${bidBulletin.bulletinTitle }</strong>
            </div>
            <ul class="rec-time">
                <li>发布日期: <fmt:formatDate value="${bidBulletin.publishDate }" pattern="yyyy-MM-dd" /></li>
                <li>截止日期: <fmt:formatDate value="${bidBulletin.returnDate }" pattern="yyyy-MM-dd" /></li>
            </ul>
        </div>
        <section class="mui-card card-block card-fold">
            <div class="mui-card-header"><span>公告详情</span></div>
            <div class="mui-card-content mui-card-content-inner">
                <ul class="image-text card-fold-list" data-height="220" style="overflow: hidden; height: auto;">
                    <li>
                          ${bidBulletin.bulletinContent}
                    </li>
                </ul>
            </div>
          <div class="mui-card-footer">
          <strong>附件下载：</strong>
          <c:choose>
            <c:when test="${empty bidBulletin.attachmentUrl}">无附件下载</c:when>
            <c:otherwise><c:out value="${bidBulletin.attachmentUrl}" escapeXml="false"/></c:otherwise>
          </c:choose>           
        </div>
        </section>
     </div>
     <c:if test="${empty eppPerson}">
     <div class="footer xmbm">
             <c:choose>
		       <c:when test="${empty supplierInfo}">
		            <c:if test="${isApplication}">
		             <button type="button" class="mui-btn mui-btn-warning bm-btn-primary mui-col-xs-12" onclick="window.open('userLogin_webMobile.action?from=xxbm&id=${bidBulletin.bbId}');">立即报名</button>	
			        </c:if>
			        <c:if test="${!isApplication}">
			           <div class="bmalert"><span class="text-negative">对不起，报名时间已经结束！</span></div>
			        </c:if>
			   </c:when>
		       <c:otherwise>
		          <c:if test="${isInviteSupplier==0&&requiredCollect.supplierType=='00'}">
				        <c:if test="${isApplication}">
		                   <button type="button" class="mui-btn mui-btn-warning bm-btn-primary mui-col-xs-12" onclick="doBmSubmit();">立即报名</button>
		                 </c:if>
			        	<c:if test="${!isApplication}">
			               <div class="bmalert"><span class="text-negative">对不起，报名时间已经结束！</span></div>
			            </c:if>
			      </c:if>
		          <c:if test="${isInviteSupplier==0&&requiredCollect.supplierType=='01'}">
		                 <div class="bmalert"><span class="text-negative">对不起，本项目为定向邀请，您不能报名参加！</span></div>
		          </c:if>
		          <c:if test="${isInviteSupplier==1}">
		                 <div class="bmalert"><span class="text-primary">恭喜您，你已报名参加本项目的采购！</span></div>
		          </c:if>
		       </c:otherwise>
		     </c:choose> 
		        <input type="hidden" name="bbId" id="bbId" value="${bidBulletin.bbId }"/>
		        <input type="hidden" name="rcId" id="rcId" value="${requiredCollect.rcId }"/>
		        <input type="hidden" name="url" id="url" value="${url}"/>
       </div>
       </c:if>           
    </div>     
    <script src="<%=path%>/mobile/js/jquery.min.js"></script>
    <script src="<%=path%>/mobile/js/mui.min.js"></script>
    <script src="<%=path%>/mobile/js/common.js"></script>
		<script type="text/javascript">
		    function doBmSubmit(){
		       var rcId=$("#rcId").val();
		       var url=$("#url").val();
		       var bbId=$("#bbId").val();
		       var result=ajaxGeneral(url,{rcId: rcId, from: "0"},"text");
		       if(result=="success"){
		           mui.alert('恭喜您，供应商商报名成功', '提示信息', function() {
					 window.location.href="viewBidBulletinContentMobile_webMobile.action?id="+bbId;
				});
		       }else{
		          mui.toast('对不起，系统出现异常，请联系客服人员');
		       }
		     }
		</script>	
	</body>
</html>
