<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>我</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
	</head>
	<body>
	    <nav class="mui-bar mui-bar-tab">
				<a id="defaultTab" class="mui-tab-item" href="index_webMobile.action">
					<span class="mui-icon iconfont icon-ziyuan"></span>
					<span class="mui-tab-label">首页</span>
				</a>
				<a id="xmTab" class="mui-tab-item" href="xmSearch_webMobile.action">
					<span class="mui-icon iconfont icon-xiangmu"></span>
					<span class="mui-tab-label">采购项目</span>
				</a>
				<a id="abountTab" class="mui-tab-item mui-active" href="about_webMobile.action">
					<span class="mui-icon iconfont icon-wode"></span> <span class="mui-tab-label">我的</span>
				</a>
		    </nav>
		<div class="mui-inner-wrap container page-supplier-index">
		    <section class="head">
			   <c:choose>
			      <c:when test="${usesType==99}">
					<div class="login">
						 <!--  <a href="reg_webMobile.action"
							class="btn btn-primary btn-outlined" >
							<span>免费注册</span>
							</a>-->
						<a href="userLogin_webMobile.action"
							class="btn btn-negative btn-outlined" >
							<span>登录</span>
						</a>
					</div>
			    </c:when>
			    <c:when test="${usesType==0}">
			        <div class="profile">
						<a href="#" class="avatar-unverify">
							<div class="avatar-wrap">
								<div class="clearfix">
									<img
										src="<%=path %>/mobile/images/noPhoto.png"
										alt="">
									<p class="name">
										<span>${userNameCn }</span>
									</p>
								</div>
								<p class="verify-tips">
										恭喜您，您将享受以下服务
								</p>
								 <div class="verify-desc">
									<p>
										<span>采购全流程管控</span>
									</p>
									<p>
										<span>便捷的消息提醒</span>
									</p>
									<p>
										<span>海量供应商资源</span>
									</p>
									<p>
										<span>核心供应商管控</span>
									</p>
								</div>
							</div> 
						</a>
					</div>
			    </c:when>
			    <c:otherwise>
			       <c:choose>
				      <c:when test="${isRegister=='0'}">
						<div class="profile">
							<a href="#" class="avatar-unverify">
								<div class="avatar-wrap">
									<div class="clearfix">
										<img
											src="<%=path %>/mobile/images/noPhoto.png"
											alt="">
										<p class="name">
											<span>${supplierName }</span>
										</p>
									</div>
									<p class="verify-tips">
										您未完成审核，不能享受以下服务
									</p>
									 <div class="verify-desc">
										<p>
											<span>采购项目报名</span>
										</p>
										<p>
										   <span>便捷的消息提醒</span>
									    </p>
										<p>
											<span>精准采购需求推送</span>
										</p>
										<p>
											<span>企业橱窗展示</span>
										</p>
									</div>
								</div> 
							</a>
						</div>
					 </c:when>
					 <c:otherwise>
					     <div class="profile">
							<a href="#" class="avatar-unverify">
								<div class="avatar-wrap">
									<div class="clearfix">
										<img
											src="<%=path %>/mobile/images/noPhoto.png"
											alt="">
										<p class="name">
											<span>${supplierName }</span>
										</p>
									</div>
									<p class="verify-tips">
										您已通过审核，可以享受以下服务
									</p>
									 <div class="verify-desc">
										<p>
											<span>采购项目报名</span>
										</p>
										<p>
										   <span>便捷的消息提醒</span>
									    </p>
										<p>
											<span>精准采购需求推送</span>
										</p>
										<p>
											<span>企业橱窗展示</span>
										</p>
									</div>
								</div> 
							</a>
						</div>
					 </c:otherwise>
					 </c:choose>
					</c:otherwise>
			   </c:choose>
			</section>
			<div class="index">
			<c:choose>
			      <c:when test="${usesType==99}">
			       <div class="row import-menu" style="margin-top: 10px;">
					<div class="col">
						<a href="xmSearch_webMobile.action?bidderType=4"> <span
							class="mui-icon icon iconfont icon-daibaoming"><i class="badge" id="xmcount">${xmcount }</i></span>
							<p>
								项目报名
							</p>
						</a>
					</div>
					<div class="col">
						<a href="userLogin_webMobile.action" > <span
							class="mui-icon icon iconfont icon-wodexiangmu"></span>
							<p>
								我的项目
							</p> 
						</a>
					</div>
					<div class="col">
						<a href="userLogin_webMobile.action" > <span
							class="mui-icon icon iconfont icon-hetongdaiqueren"></span>
							<p>
								合同确认
							</p> 
						</a>
					</div>
					<div class="col">
						<a href="userLogin_webMobile.action" > <span
							class="mui-icon icon iconfont icon-querendingdan"></span>
							<p>
								订单确认
							</p> 
						</a>
					</div>
				   </div>
			  </c:when>
			      <c:when test="${usesType==1}">
			       <div class="row import-menu" style="margin-top: 10px;">
					<div class="col">
						<a href="xmSearch_webMobile.action?bidderType=4"> <span
							class="mui-icon icon iconfont icon-daibaoming"><i class="badge" id="xmcount">${xmcount }</i></span>
							<p>
								项目报名
							</p>
						</a>
					</div>
					<div class="col">
						<a href="viewRequiredCollectMy_requiredCollectSupplier.action?from=0"> <span
							class="mui-icon icon iconfont icon-wodexiangmu"></span>
							<p>
								我的项目
							</p> 
						</a>
					</div>
					<div class="col">
						<a href="viewContractInfoApplication_contractInfoSupplier.action?from=0"> <span
							class="mui-icon icon iconfont icon-hetongdaiqueren"></span>
							<p>
								合同确认
							</p> 
						</a>
					</div>
					<div class="col">
						<a href="viewOrderInfoApplication_orderInfoSupplier.action?from=0"> <span
							class="mui-icon icon iconfont icon-querendingdan"></span>
							<p>
								订单确认
							</p> 
						</a>
					</div>
				   </div>
			  </c:when>
			  <c:otherwise>
			     <div class="row import-menu border-bottom" style="margin-top: 10px;">
			        <div class="col">
						<a href="viewRequiredCollect_purchaseTender.action?from=0"> <span
							class="mui-icon icon iconfont icon-xiangmu1"></span>
							<p>
								发布的招标
							</p>
						</a>
					</div>
					<div class="col">
						<a href="viewRequiredCollect_purchaseAsk.action?from=0"> <span
							class="mui-icon icon iconfont icon-icon-inquiry"></span>
							<p>
								发布的询价
							</p> 
						</a>
					</div>
					<div class="col">
						<a href="viewRequiredCollect_purchaseBidding.action?from=0"> <span
							class="mui-icon icon iconfont icon-jingjiagonggao"></span>
							<p>
								发布的竞价
							</p> 
						</a>
					</div>
					</div>
					<div class="row import-menu">
						<div class="col">
							<a href="viewContractInfo_contractInfo.action?from=0"> <span
								class="mui-icon icon iconfont icon-hetong"></span>
								<p>
									我的合同
								</p> 
							</a>
						</div>
						
						<div class="col">
							<a href="viewOrderInfo_orderInfo.action?from=0"> <span
								class="mui-icon icon iconfont icon-dingdan"></span>
								<p>
									我的订单
								</p> 
							</a>
						</div>
					</div>
			  </c:otherwise>
			</c:choose>
			<ul class="mui-table-view abount" style="margin-top: 10px;">
				<c:choose>
				      <c:when test="${usesType==99}">
						<li class="mui-table-view-cell menu01">
							<a class="mui-navigate-right" href="userLogin_webMobile.action" >
							<span class="mui-icon icon iconfont icon-wodeziliao"></span>企业资料 </a>
						</li>
						<li class="mui-table-view-cell menu02">
							<a class="mui-navigate-right" href="userLogin_webMobile.action" >
							<span class="mui-icon icon iconfont icon-wodexiaoxi"></span><span class="mui-badge mui-badge-danger" id="messageCount">0</span>我的消息 </a>
						</li>
						<li class="mui-table-view-cell menu03">
							<a class="mui-navigate-right" href="userLogin_webMobile.action" >
							<span class="mui-icon icon iconfont icon-gerenzhongxin-xuanzhong"></span>个人中心</a>
						</li>
					</c:when>
				      <c:when test="${usesType==1}">
						<li class="mui-table-view-cell menu01">
							<a class="mui-navigate-right"  href="viewSupplierBaseInfoDetail_supplierBaseInfoSupplier.action?from=0">
							<span class="mui-icon icon iconfont icon-wodeziliao"></span>企业资料 </a>
						</li>
						<li class="mui-table-view-cell menu02">
							<a class="mui-navigate-right" href="viewMessagesMobile_supplierMain.action">
							<span class="mui-icon icon iconfont icon-wodexiaoxi"></span><span class="mui-badge mui-badge-danger" id="messageCount">0</span>我的消息 </a>
						</li>
						<li class="mui-table-view-cell menu03">
							<a class="mui-navigate-right" href="viewSuppplierPhoneMobile_supplierBaseInfoSupplier.action">
							<span class="mui-icon icon iconfont icon-gerenzhongxin-xuanzhong"></span>个人中心</a>
						</li>
					</c:when>
					<c:otherwise>
						<li class="mui-table-view-cell menu02">
							<a class="mui-navigate-right" href="viewMobileMessages_IndexEpp.action">
							<span class="mui-icon icon iconfont icon-wodexiaoxi"></span><span class="mui-badge mui-badge-danger" id="messageCount">0</span>我的消息 </a>
						</li>
						<li class="mui-table-view-cell menu03">
							<a class="mui-navigate-right" href="viewMobileWaitForDeal_viewWaitForDeal.action">
                            <span  class="mui-icon icon iconfont icon-daiban"></span><span class="mui-badge mui-badge-purple" id="waitCount">0</span>我的待办 </a>
						</li>
						<li class="mui-table-view-cell menu04">
							<a class="mui-navigate-right" href="viewMobileDoneForDeal_viewWaitForDeal.action">
							<span  class="mui-icon icon iconfont icon-yiban"></span>我的已办</a>
						</li>
						<li class="mui-table-view-cell menu05">
							<a class="mui-navigate-right" href="userMobile_users.action">
							<span class="mui-icon icon iconfont icon-gerenzhongxin-xuanzhong"></span>个人中心</a>
						</li>
					</c:otherwise>
				</c:choose>	
			</ul>
			<ul class="mui-table-view abount" style="margin-top: 10px;">
				<li class="mui-table-view-cell menu06">
					<a class="mui-navigate-right">
					<span class="mui-icon icon iconfont icon-xinshoubangzhu"></span>新手帮助 </a>
				</li>
			 <c:choose>
			    <c:when test="${usesType==0}">
				<li class="mui-table-view-cell menu07">
					<a class="mui-navigate-right">
					<span class="mui-icon icon iconfont icon-xiugaimima"></span>修改密码 </a>
				</li>
				</c:when>
				<c:when test="${usesType==1}">
				<li class="mui-table-view-cell menu07">
					<a class="mui-navigate-right">
					<span class="mui-icon icon iconfont icon-xiugaimima"></span>修改密码 </a>
				</li>
				</c:when>
				<c:otherwise>
				<li class="mui-table-view-cell menu07">
					<a class="mui-navigate-right">
					<span class="mui-icon icon iconfont icon-xiugaimima"></span>修改密码 </a>
				</li>
				</c:otherwise>
				</c:choose>
			</ul>
			<c:if test="${usesType==0||usesType==1}">
			<ul class="mui-table-view abount" style="margin-top: 15px;">
				<li class="mui-table-view-cell menu08">
					<a href="exit_verify.action?from=0" ><span class="mui-icon icon iconfont icon-tuichudenglu"></span>退出登录</a>
				</li>
			</ul>
			</c:if>
			</div>
		</div>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		<script type="text/javascript">
		   
		   <c:if test="${usesType==0}">		   
		   $(function (){
			    //加载消息提醒条数
			   $.ajax({
	    		url:"viewCountMessage_IndexEpp.action",
	    		type:"POST",
	    		dataType:"JSON",
	    		async: false,
	    		success:function(data){
	    			var messageCount =data.count;
	    			$("#messageCount").html(messageCount);
	    		}
    		   });
    		  //加载我的审批提醒条数
			  $.ajax({
	    		url:"viewCountWaitForDeal_IndexEpp.action",
	    		type:"POST",
	    		dataType:"JSON",
	    		async: false,
	    		success:function(data){
	    			var waitCount =data.count;
	    			$("#waitCount").html(waitCount);
	    		}
	    		});
    		})		
		   </c:if>
		   <c:if test="${usesType==1}">
		    $(function (){
			    //加载消息提醒条数
			   $.ajax({
	    		url:"viewCountMessage_supplierMain.action",
	    		type:"POST",
	    		dataType:"JSON",
	    		async: false,
	    		success:function(data){
	    			var messageCount =data.count;
	    			$("#messageCount").html(messageCount);
	    		}
    		   });
    		  })
		   </c:if>
		   mui.init();
		   mui('body').on('tap','a',function(){document.location.href=this.href;});
		</script>
	</body>
</html>
