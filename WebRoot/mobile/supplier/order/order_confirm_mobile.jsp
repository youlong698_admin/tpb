<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>订单详情查看</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<style type="text/css">
		   .mui-control-content {
				height: 100%;
				padding: 10px;
			}
		</style>
</head>
<body>
<form id="form" name="form" method="post" action="saveConfirmOrderInfo_orderInfoSupplier.action">
<c:set var="td1" value=""/>
<c:set var="td2" value=""/>
<c:choose>
<c:when test="${orderInfo.type==1||orderInfo.type==3 }"><c:set var="td1" value="项目名称"/> <c:set var="td2" value="项目编号"/></c:when>
<c:otherwise><c:set var="td1" value="合同名称"/> <c:set var="td2" value="合同编号"/></c:otherwise>
</c:choose>
<!-- 防止表单重复提交 -->
<s:token/>
	<div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">订单详情页面</h1> 
			</header>
			<div class="mui-content">
			    <div style="padding: 10px 10px;">
					<div id="segmentedControl" class="mui-segmented-control">
						<a class="mui-control-item mui-active" href="#item1">基本信息</a>
						<a class="mui-control-item" href="#item2">订单物资信息</a>
					</div>
				</div>
				<div id="item1" class="mui-control-content mui-active">
						<div>
						  <div class="mui-input-group spdetail">
						        <p>
			    				    <label>${td1 }:</label>
									<span>${orderInfo.projectContractName }</span>
								</p>
								<p>
								     <label>${td2 }:</label>
									 <span>${orderInfo.bidCode }</span>
								</p>
								<p>
									<label>订单编号:</label>
									<span>${orderInfo.orderCode }</span>
								</p>
								<p>
									<label>订单名称:</label>
									<span>${orderInfo.orderName }</span>
								</p>
								<p>
								    <label>采购单位名称:</label>
					                <span>${orderInfo.orderPersonNameA }</span>
					            </p>
								<p>			
									 <label>供应商名称:</label>
									 <span>${orderInfo.orderNameB }</span>
								</p>
								<p>			
									 <label>收货地址:</label>
									 <span>${orderInfo.orderAddressA}</span>
								</p>
								<p>			
									 <label>供应商地址:</label>
									 <span>${orderInfo.orderAddressB }</span>
								</p>
								<p>			
									 <label>收货联系人:</label>
									 <span>${orderInfo.orderUndertaker }</span>
								</p>
								<p>			
									 <label>供应商联系人:</label>
									 <span>${orderInfo.orderPersonB }</span>
								</p>
								<p>			
									 <label>收货联系人手机:</label>
									 <span>${orderInfo.orderMobileA }</span>
								</p>
								<p>			
									 <label>供应商联系人手机:</label>
									 <span>${orderInfo.orderMobileB }</span>
								</p>
								<p>			
									 <label>采购单位电话:</label>
									 <span>${orderInfo.orderTelA }</span>
								</p>
								<p>			
									 <label>供应商电话:</label>
									 <span>${orderInfo.orderTelB }</span>
								</p>
								<p>			
									 <label>采购单位传真:</label>
									 <span>${orderInfo.orderFaxA }</span>
								</p>
								<p>			
									 <label>供应商传真:</label>
									 <span>${orderInfo.orderFaxB }</span>
								</p>
								<p>			
									 <label>订单金额:</label>
									 <span><fmt:formatNumber value="${orderInfo.orderMoney }" pattern="#,##0.00"/></span>
								</p>
								<p>			
									 <label>货币类型:</label>
									 <span> ${orderInfo.conMoneyType}</span>
								</p>
								<p>			
									 <label>备注:</label>
									 <span>${orderInfo.conRemark }</span>
								</p>
							</div>
							
						</div>
			    </div>
			    <div id="item2" class="mui-control-content">
					<div class="mui-input-group">
					   <ul>
					       <c:set var="totalPrice" value="0"/>
							<c:forEach items="${omList}" var="orderMaterial" varStatus="status">
							<c:set var="totalPrice" value="${totalPrice+orderMaterial.amount*orderMaterial.price }"/> 
					        <li style="border-bottom:1px solid #c8c7cc">
					            <div class="mui-table">
					                <div class="mui-table-cell mui-col-xs-8">
					                    <h6 class="mui-ellipsis text-positive">${orderMaterial.materialName}</h6>
					                    <p class="mui-h6 mui-ellipsis">规格型号：${orderMaterial.materialType}</p>
					                </div>
					                <div class="mui-table-cell mui-col-xs-4 mui-text-right">
					                    <p>
					                    <span class="mui-h6 text-primary">
					                                                           ￥ <fmt:formatNumber value="${orderMaterial.price}" pattern="#,##0.00"/>
					                    </span>
					                    </p>
					                    <p>
					                    <span class="mui-h6">${orderMaterial.amount} </span>
					                    <span class="mui-h6">${orderMaterial.unit}</span></p>
					                </div>
					            </div>
					        </li>
					        </c:forEach>	
					        <li style="border-bottom:1px solid #c8c7cc;height: 40px;">
					            <div class="mui-table" style="padding: 10px;">
					                <div class="mui-table-cell mui-col-xs-4">
					                    <p class="mui-h5 mui-ellipsis">总价：</p>
					                </div>
					                <div class="mui-table-cell mui-col-xs-8 mui-text-right">
					                    <p>
					                    <span class="mui-h5 text-negative">
					                                                           ￥ <fmt:formatNumber value="${totalPrice}" pattern="#,##0.00"/>
					                    </span>
					                    </p>								                
					                </div>
					            </div>
					        </li>							        
					    </ul> 
					</div>
			    </div>
			    <div class="footer xmbm">
		     <div class="mui-input-row" style="background-color: #ccc">
				<label>确认说明</label>
				<input type="text" name="confirmRemark" id="confirmRemark" placeholder="请输入确认说明">
			 </div>
			 <input type="hidden" id="oiId" name="oiId" value="${orderInfo.oiId }"/>
			 <input type="hidden" id="supplierId" name="supplierId" value="${orderInfo.supplierId }"/>
			 <input type="hidden" id="status" name="status" value=""/>
			 <input type="hidden" id="from" name="from" value="0"/>
			<button class="mui-btn mui-btn-primary bm-btn mui-col-xs-12" id="btn-sum" type="button" onclick="submitConfirm('1');">确认无误</button>				
		</div>
		    	
	</div>
</form>	
 <script src="<%=path%>/mobile/js/jquery.min.js"></script>	
 <script src="<%=path%>/mobile/js/mui.min.js"></script>	
 <script type="text/javascript">
      function submitConfirm(sign){
      $("#status").val(sign);
      document.form.submit();
   }
 </script>
</body>
</html>