<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>报价提示页面</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />
</head>
<body>
	     <header class="mui-bar mui-bar-nav">
			<h1 class="mui-title">报价结果</h1> 
		</header>
       <div class="mui-content">
           <div class="success" style="margin-bottom: 15px">
                 <div style="margin-bottom: 15px">
					<span class="mui-icon icon mui-icon-checkmarkempty"></span>
				 </div>
				 <p class="mui-h2 text-success">${message }</p>
           </div>
           
		   <div class="mui-button-row">
				<button type="button" class="mui-btn mui-btn-primary mui-col-xs-12" onclick="window.location.href='viewAskBidPriceResponseMonitor_askBidPriceResponseSupplier.action?rcId=${rcId}&from=0'">返回我的报价记录</button>
		   </div>
       </div>
	</body>
</html>
