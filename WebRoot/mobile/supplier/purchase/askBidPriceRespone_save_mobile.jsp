<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>项目报价</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />		
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />				
		<link rel="stylesheet" href="<%=path%>/mobile/css/supplier.css" />
	</head>
<body>
  <div class="mui-inner-wrap container">
   <header class="mui-bar mui-bar-nav">	
			<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
			<h1 class="mui-title">项目报价</h1>
	</header>
	<div class="mui-content bidding-detail">
	    <div class="mui-content-inner" style="margin-bottom: 50px;">
	        <div class="mui-input-group" style="margin: 10px">
				<form class="defaultForm" id="supInfo" name="" method="post" action="">
				<input type="hidden" id="from" name="from" value="0"/>
				<input type="hidden" id="publicKey" value="${publicKey }"/>
				<input type="hidden" id="returnDate" value="<fmt:formatDate value="${askBidList.returnDate}" pattern="yyyy-MM-dd HH:mm" />"/>
				<div class="Conter_Container">
				    <div class="row-fluid">
						<input type="hidden" name="rcId" id="rcId" value="${rcId }" />
				        <table align="center" class="table_ys1" id="listtable">			
				    	    <tr>
				    	       <td colspan="8" class="Content_tab_style_td_head" align="center">报价信息</td>
				    	    </tr>
				    	     <tr>
						    <td colspan="8" style="font-size:14px;"><font color="red"><b>1.报价列不能有空值，如果此物料不供应，请填写0<br>
																					2.本次报价为：“${askBidList.priceTypeCn }”<br>
																					3.报价截止日期前，可以多次报价，系统会以最后一次报价为准。</b></font></td>
						    </tr>
							<tr id="tdNum" align="center" class="Content_tab_style_04">
								<th width="5%" nowrap>序号
									   <input type="hidden" name="indexArr"  id="indexArr" value="-1"/></th>
								<th width="20%" nowrap>名称 </th>
								<th width="10%" nowrap>规格<br/>型号 </th>
								<th width="10%" nowrap>计量<br/>单位</th>
								<th width="10%" nowrap>数量</th>
								<th width="20%" nowrap>${askBidList.priceColumnTypeCn }</th>
								<th width="15%" nowrap>小计</th>
							</tr>
							<c:forEach items="${rcdlist}" var="requiredCollectDetail" varStatus="status">
								<tr  class="input_ys1">
									<td>
									   ${status.index+1}
									   <input type="hidden" name="indexArr"  id="indexArr" value="${status.index}"/>
									</td>
									<td>
									   ${requiredCollectDetail.buyName}
									</td>
									<td>
									   ${requiredCollectDetail.materialType}
									</td>
									<td>
									   ${requiredCollectDetail.unit}
								    </td>
								    <td align="right">
								       ${requiredCollectDetail.amount}				       
									   <input type="hidden" name="bpdList[${status.index}].amount"  id="amount_${status.index}" value="${requiredCollectDetail.amount}"/>
								    </td>
									<td>
									 <input type="text" class="eppInput" style="width:100%;background-color: yellow;padding:0" id="price_${status.index}" value=""
												onblur="validateNum(this);"/>
									 <input type="hidden" name="bpdList[${status.index}].encryPrice"  id="encryPrice_${status.index}" value=""/>
									 <input type="hidden" name="bpdList[${status.index}].supplierId"  id="supplierId_${status.index}" value="${supplierId }"/>
									 <input type="hidden" name="bpdList[${status.index}].rcdId"  id='rcdId_${status.index}' value='${requiredCollectDetail.rcdId}'/>
									</td>
									<td>
									   <span id="priceSumStr_${status.index}"><!-- 小计 -->&nbsp;</span>
									</td>
								</tr>
							</c:forEach>
								<tr>
									<td colspan="2" align="center">总价：</td>
									<td colspan="6">
									    <input type="hidden" name="bidPrice.encryTotalPrice"  id="encryTotalPrice" value=""/>
									    <input type="hidden" name="bidPrice.rcId"  id="rcId" value="${rcId }"/>
									    <input type="hidden" name="bidPrice.supplierId"  id="supplierId" value="${supplierId }"/>
										<span id="totalPriceStr"><!-- 小计 -->&nbsp;</span>
									</td>
								</tr>		
								 <tr>
									<td colspan="2" align="center">税率（%）：</td>
									<td colspan="6">
										<input type='text' class="eppInput" name='bidPrice.taxRate' style='width:150px;background-color: yellow;padding:0'  id='taxRate' value=''  onblur="validateNum(this);" />
									</td>
								</tr>				  		
				        </table>
				        <c:if test="${fn:length(briList)>0}">
				        <table align="center" class="table_ys1">
				    	    <tr>
				    	       <td colspan="4" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
				    	    </tr>
							 <tr id="tdNum" align="center" class="Content_tab_style_04">
								<th width="10%" nowrap>序号</th>
								<th width="20%" nowrap>响应项名称</th>
								<th width="30%" nowrap>响应项要求 </th>
								<th width="40%" nowrap>我的响应 </th>
							</tr>
							<c:forEach items="${briList}" var="businessResponseItems" varStatus="status">
							 <tr>
								<td align="center">
								   ${status.index+1}
								</td>
								<td>
								    ${businessResponseItems.responseItemName}
								</td>
								<td>
								   ${businessResponseItems.responseRequirements}
								</td>
								<td>
								<input type="text" class="eppInput" style="width:100%;background-color: yellow;padding:0" id="myResponse_${status.index}" name="bbrList[${status.index}].myResponse" value=""/>
								<input type="hidden" id="responseRequirements_${status.index}" name="bbrList[${status.index}].responseRequirements" value="${businessResponseItems.responseRequirements }"/>
								<input type="hidden" id="responseItemName_${status.index}" name="bbrList[${status.index}].responseItemName" value="${businessResponseItems.responseItemName }"/>
								<input type="hidden" id="briId_${status.index}" name="bbrList[${status.index}].briId" value="${businessResponseItems.briId }"/>
								<input type="hidden" id="supplierId_${status.index}" name="bbrList[${status.index}].supplierId" value="${supplierId }"/>
								<input type="hidden" id="rcId_${status.index}" name="bbrList[${status.index}].rcId" value="${rcId }"/>
								</td>
								</tr>
						   </c:forEach>	  		
				        </table>
				        </c:if>        
				        
				     </div>  
					</div>
				</form>
			</div>
		</div>
		<div class="footer xmbm">
				<button class="mui-btn mui-btn-primary bm-btn mui-col-xs-5" id="btn-sum" type="button" onclick="accountPrice();">计算总价</button>
				<button class="mui-btn mui-btn-royal bm-btn mui-col-xs-5" id="btn-save" type="button" onclick="save();">提交报价信息</button>				
		</div>
	</div>
</div>   
    <script src="<%=path%>/mobile/js/jquery.min.js"></script>
	<script src="<%=path%>/mobile/js/mui.min.js"></script>
	<script src="<%=path%>/mobile/js/common.js"></script>
	<script type="text/javascript">
	   //计算总价
		function accountPrice() 
		{
		    var indexArr=document.getElementsByName("indexArr");
			var amount,price,index,totalPrice=0;
				 for(var i=1;i<indexArr.length;i++){
				    index=indexArr[i].value;
			        price=$("#price_"+index).val();
			        if(price==""){
			            mui.toast("报价列有空值");
			           return false;
			        }
			        amount=$("#amount_"+index).val();
			        //showMsg("alert","amount="+amount);
			        //计算小计
			        var  prices=eval(price*amount).toFixed(2)+"";
			        document.getElementById("priceSumStr_"+index).innerHTML=prices;
		             
			       /*计算总合计*/
					totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
				 }
				 document.getElementById("totalPriceStr").innerHTML =totalPrice;
		}
		//执行保存
		function save(){
		  //首先判断截标日期
		  var date = new Date(Date.parse(document.getElementById("returnDate").value.replace(/\-/g, "\/")));
			if(date != "" && date != undefined && date != null){
			var sDate = date;
		   	var eDate = new Date();
			if(sDate < eDate){
	     		 mui.toast("温馨提示：很遗憾，已经超过截标时间，不能上传！！");
	     		return false;
	  			}
			}
		  //报价加密
		  var indexArr=document.getElementsByName("indexArr");
		  var publicKey=$("#publicKey").val();
		  var encryPrice="";
			var amount,price,index,jsonData,totalPrice=0;
				 for(var i=1;i<indexArr.length;i++){
				    index=indexArr[i].value;
			        price=$("#price_"+index).val();
			        if(price==""){
			            mui.toast("报价列有空值");
			           return false;
			        }
			        amount=$("#amount_"+index).val();
			        //showMsg("alert","amount="+amount);
			        //计算小计
			        var  prices=eval(price*amount).toFixed(2)+"";
			        document.getElementById("priceSumStr_"+index).innerHTML=prices;
		             
			       /*计算总合计*/
					totalPrice=FloatAdd(totalPrice,prices).toFixed(2);
					
					jsonData={"publicKey":publicKey,"prices":price};
					encryPrice=ajaxGeneral("encryPrice_askBidPriceResponseSupplier.action",jsonData,"json");
					//加密每行报价数据
					$("#encryPrice_"+index).val(encryPrice.data);
				 }
				 document.getElementById("totalPriceStr").innerHTML =totalPrice;
				 
				 jsonData={"publicKey":publicKey,"prices":totalPrice};
				 encryPrice=ajaxGeneral("encryPrice_askBidPriceResponseSupplier.action",jsonData,"json");
				  //加密总价数据
				 $("#encryTotalPrice").val(encryPrice.data);
				if(encryPrice.data!=""){				
				   document.forms[0].action="saveAskBidPriceRespone_askBidPriceResponseSupplier.action";
				   document.forms[0].submit();
			    }else{			    
	     		  mui.toast("温馨提示：报价加密失败，请联系客服人员！！");
			    }
		
		}
	</script>
	</body>
</html>