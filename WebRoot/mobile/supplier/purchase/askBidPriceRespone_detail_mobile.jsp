<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>我的报价明细</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />		
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />				
		<link rel="stylesheet" href="<%=path%>/mobile/css/supplier.css" />
	</head>
	<body>
	  <div class="mui-inner-wrap container">
	   <header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">我的报价明细</h1>
		</header>
		<div class="mui-content bidding-detail">
		    <div class="mui-content-inner">
		        <div class="mui-input-group" style="margin: 10px">
							<table align="center" class="table_ys1" id="listtable">			
					    	    <tr>
					    	       <td colspan="8" class="Content_tab_style_td_head" align="center">报价信息</td>
					    	    </tr>
					    	     <tr>
							    <td colspan="8" style="font-size:14px;"><font color="red"><b>1.本次报价为：“${askBidList.priceTypeCn }”<br></b></font></td>
							    </tr>
								<tr id="tdNum" align="center" class="Content_tab_style_04">
									<th width="5%" nowrap>序号
										   <input type="hidden" name="indexArr"  id="indexArr" value="-1"/></th>
									<th width="20%" nowrap>名称 </th>
									<th width="10%" nowrap>规格<br/>型号 </th>
									<th width="10%" nowrap>计量<br/>单位</th>
									<th width="10%" nowrap>数量</th>
									<th width="10%" nowrap>${askBidList.priceColumnTypeCn }</th>
									<th width="20%" nowrap>小计</th>
								</tr>
								<c:forEach items="${bpdhList}" var="bidPriceDetailHistory" varStatus="status">
									<tr  class="input_ys1">
										<td>
										   ${status.index+1}
										</td>
										<td>
										   ${bidPriceDetailHistory.requiredCollectDetail.buyName}
										</td>
										<td>
										   ${bidPriceDetailHistory.requiredCollectDetail.materialType}
										</td>
										<td>
										   ${bidPriceDetailHistory.requiredCollectDetail.unit}
									    </td>
									    <td align="right">
									     ${bidPriceDetailHistory.requiredCollectDetail.amount}
										 </td>
									    <td align="right">
									       <fmt:formatNumber value="${bidPriceDetailHistory.price}" pattern="#,##0.00"/>
									    </td>
										<td>
										   <fmt:formatNumber value="${bidPriceDetailHistory.price*bidPriceDetailHistory.requiredCollectDetail.amount}" pattern="#,##0.00"/>
										</td>
									</tr>
								</c:forEach>
									<tr>
										<td colspan="2" align="center">总价：</td>
										<td colspan="6">
										    <fmt:formatNumber value="${bidPriceHistory.totalPrice }" pattern="#,##0.00"/>
										</td>
									</tr>		
									 <tr>
										<td colspan="2" align="center">税率（%）：</td>
										<td colspan="6">
											${bidPriceHistory.taxRate }
										</td>
									</tr>				  		
					        </table>
					        <c:if test="${fn:length(bbrhList)>0}">
					        <table align="center" class="table_ys1">
					    	    <tr>
					    	       <td colspan="4" class="Content_tab_style_td_head" align="center">商务响应项信息</td>
					    	    </tr>
								 <tr id="tdNum" align="center" class="Content_tab_style_04">
									<th width="10%" nowrap>序号</th>
									<th width="20%" nowrap>响应项名称</th>
									<th width="30%" nowrap>响应项要求 </th>
									<th width="40%" nowrap>我的响应 </th>
								</tr>
								<c:forEach items="${bbrhList}" var="bidBusinessResponseHistory" varStatus="status">
								 <tr>
									<td align="center">
									   ${status.index+1}
									</td>
									<td>
									    ${bidBusinessResponseHistory.responseItemName}
									</td>
									<td>
									   ${bidBusinessResponseHistory.responseRequirements}
									</td>
									<td>
									   ${bidBusinessResponseHistory.myResponse}
									 </td>
									</tr>
							   </c:forEach>	  		
					        </table>
					        </c:if> 
						</div>
		     </div>
           
    </div>   
    </div>      
    <script src="<%=path%>/mobile/js/jquery.min.js"></script>
	<script src="<%=path%>/mobile/js/mui.min.js"></script>
	</body>
</html>
