<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>我的报价记录</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />		
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />				
		<link rel="stylesheet" href="<%=path%>/mobile/css/supplier.css" />
	</head>
	<body>
	  <div class="mui-inner-wrap container">
	   <header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">我的报价记录</h1>
		</header>
		<div class="mui-content bidding-detail">
		    <div class="mui-content-inner">
		        <section class="mui-card card-block card-fold">
		            <div class="mui-card-content mui-card-content-inner">
		                <ul class="image-text card-fold-list" data-height="220" style="overflow: hidden; height: auto;background-color: #f2dede;color: #b94a48;">
		                    <li>
		                                                  注意：<br/>
							   1、询价方没有解密报价之前，价格采取保密机制。<br/>
							   2、询价解密之前可以多次报价，系统以最后一次的报价为准。<br/>
							   3、为防止短信骚扰，每个项目报价之后只有一次通知项目负责人的机会，请确认当次报价为最终报价的时候，才进行短信邮件告知项目负责人。
			                </li>
		                </ul>
		            </div>
		        </section>
		        <div class="mui-input-group" style="margin: 10px">
							<table width="100%" class="table_ys1">
								<tr>
									<td colspan="4" class="Content_tab_style_td_head">
										历史报价信息
									</td>
								</tr>
								<tr class="Content_tab_style_04">
									<th width="5%" nowrap>
										序号
									</th>
									<th width="45%" nowrap>
										报价时间
									</th>
									<th width="30%" nowrap>
										总价
									</th>
									<th width="20%" nowrap>
										操作
									</th>
								</tr>
								<c:forEach items="${listValue}" var="bidPriceHistory"
									varStatus="status">
									<tr align="center"
										<c:choose><c:when test="${status.index%2==0 }">class='biaoge_01_a'</c:when><c:otherwise>class='biaoge_01_b'</c:otherwise></c:choose>>
										<td>
											${status.index+1}
										</td>
										<td>
											<fmt:formatDate value="${bidPriceHistory.writeDate}"
												type="both" pattern="yyyy-MM-dd HH:mm" />
										</td>
										<c:choose>
											<c:when test="${empty bidPriceHistory.totalPrice}">
												<td>
													*****
												</td>
												<td>
													<span class="text-muted">价格保密</span>
												</td>
											</c:when>
											<c:otherwise>
												<td>
													<span class="text-muted">${bidPriceHistory.totalPrice}</span>
												</td>
												<td>
													<button class='mui-btn mui-btn-primary'
														type="button" onclick='doView(${bidPriceHistory.bphId})'>
														查看
													</button>
												</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>

							</table>
						</div>
		     </div>
           <div class="footer xmbm">
                <c:if test="${isReturnTime}">
		             <button type="button" class="mui-btn bm-btn-primary mui-col-xs-5" onclick="doBjSubmit();">立即报价</button>
		             <button type="button" class="mui-btn mui-btn-primary bm-btn mui-col-xs-5" id="btn-sms" onclick="doSms(${rcId})" <c:if test="${isSms=='0' }">disabled="disabled"  title="短信已告知" </c:if> >短信邮件告知</button>
				 </c:if>          
		        <input type="hidden" name="rcId" id="rcId" value="${rcId }"/>
           </div>        
    </div>   
    </div>  
    <script src="<%=path%>/mobile/js/jquery.min.js"></script>
	<script src="<%=path%>/mobile/js/mui.min.js"></script>
    <script src="<%=path%>/mobile/js/common.js"></script>
		<script type="text/javascript">
		    function doBjSubmit(){
		       var rcId=$("#rcId").val();
		       window.location.href="saveInitAskBidPriceRespone_askBidPriceResponseSupplier.action?rcId="+rcId+"&from=0";
		     }
		     function doSms(rcId){
		        var result=ajaxGeneral("saveSmsAskBidPriceRespone_askBidPriceResponseSupplier.action","rcId="+rcId,"text");
			    if(result=="1"){
			      $('#btn-sms').attr("disabled","disabled");
			      mui.toast("短信邮件已告知！");	      
			    }else{
			      mui.toast("短信邮件告知失败，请联系客服人员！");
			    }	
		    }
		    function doView(bphId){
	            window.location.href="viewAskBidPriceResponeDetail_askBidPriceResponseSupplier.action?bidPriceHistory.bphId="+bphId+"&from=0";
	        }
		</script>	
	</body>
</html>
