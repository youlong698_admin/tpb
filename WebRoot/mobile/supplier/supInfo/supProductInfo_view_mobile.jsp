<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>产品信息</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/supplier.css" />
	</head>
	<body>
	<div id="offCanvasWrapper" class="mui-off-canvas-wrap mui-draggable mui-scalable"> 
  <!--菜单部分-->
			<aside id="offCanvasSide" class="mui-off-canvas-right">
				<div id="offCanvasSideScroll" class="mui-scroll-wrapper">
					<div class="mui-scroll">
						<div class="title" style="margin-bottom: 25px;">企业档案</div>
						<ul class="mui-table-view mui-table-view-chevron mui-table-view-inverted">
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right" href="viewSupplierBaseInfoDetail_supplierBaseInfoSupplier.action?from=0">
									供应商基本信息
								</a>
							</li>
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right" href="viewSupplierProductInfo_supplierProductInfoSupplier.action?from=0">
									供应商产品信息
								</a>
							</li>
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right" href="viewSupplierCertificates_supCertificatesSupplier.action?from=0">
									供应商资质信息
								</a>
							</li>

						</ul>
					</div>
				</div>
			</aside>
		<div class="mui-inner-wrap container">
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				 <a class="mui-icon mui-icon-bars mui-pull-right" href="#offCanvasSide" type="link"></a>
				<h1 class="mui-title">产品信息</h1>
		    </header>
			<div id="offCanvasContentScroll" class="mui-content mui-scroll-wrapper">
			       <div id="slider" class="mui-slider mui-fullscreen">
						<div class="mui-slider-group">
							<!--下拉刷新容器-->
							<div id="pullrefresh" class="mui-scroll-wrapper">
								<div class="mui-scroll">
									<!--数据列表-->
									 <ul class="mui-table-view mui-grid-view" id="supplierProductInfoQuery">

									</ul>
								</div> 
							</div>
						 </div>	   
					</div>
			</div>
			<div class="mui-off-canvas-backdrop"></div>
		</div>
		</div>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		<script src="<%=path%>/mobile/js/common.js"></script>
		<script src="<%=path%>/mobile/js/supplier/supProductInfoViewMobile.js"></script>	
		<script type="text/javascript">
		   //侧滑容器父节点
    var offCanvasWrapper = mui('#offCanvasWrapper');
    //主界面容器
    var offCanvasInner = offCanvasWrapper[0].querySelector('.mui-inner-wrap');
    //菜单容器
    var offCanvasSide = document.getElementById("offCanvasSide");
    if (!mui.os.android) {
        var spans = document.querySelectorAll('.android-only');
        for (var i = 0, len = spans.length; i < len; i++) {
            spans[i].style.display = "none";
        }
    }	
    //移动效果是否为整体移动
    var moveTogether = false;
    //侧滑容器的class列表，增加.mui-slide-in即可实现菜单移动、主界面不动的效果；
    var classList = offCanvasWrapper[0].classList;

    /* //主界面‘显示侧滑菜单’按钮的点击事件
    document.getElementById('offCanvasShow').addEventListener('tap', function() {
    offCanvasWrapper.offCanvas('show');
    });
    //菜单界面，‘关闭侧滑菜单’按钮的点击事件
    document.getElementById('offCanvasHide').addEventListener('tap', function() {
    offCanvasWrapper.offCanvas('close');
    });*/
    //主界面和侧滑菜单界面均支持区域滚动；
    mui('#offCanvasSideScroll').scroll();
    mui('#offCanvasContentScroll').scroll();
	mui.ready(function(){
		mui('.mui-scroll-wrapper').scroll({deceleration: 0.0005});
	});
	
    //实现ios平台原生侧滑关闭页面；
    if (mui.os.plus && mui.os.ios) {
        mui.plusReady(function () { //5+ iOS暂时无法屏蔽popGesture时传递touch事件，故该demo直接屏蔽popGesture功能
            plus.webview.currentWebview().setStyle({
                'popGesture': 'none'
            });
        });
    }
	
    //左侧滑菜单的链接事件
    mui('body').on('tap', '#offCanvasSide a', function () {
       document.location.href = this.href;
        
    });
    //头部栏的超链接事件
    mui('body').on('tap', 'header a', function () {
        document.location.href = this.href;
    });
</script>

	</body>
</html>
