<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>个人中心</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
	</head>
	<body>
		<div class="mui-inner-wrap container">
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">个人中心</h1>
		    </header>
			<div class="mui-content">
						<ul class="mui-table-view">
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right">姓名<span class="mui-pull-right grxx">${users.userChinesename }</span></a>
							</li>
							<li class="mui-table-view-cell">
								<a>用户名<span class="mui-pull-right">${users.userName }</span></a>
							</li>							
							<li class="mui-table-view-cell">
								<a>部门<span class="mui-pull-right grxx">${users.deptName }</span></a>
							</li>						
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right">岗位<span class="mui-pull-right grxx">${users.station }</span></a>
							</li>
						</ul>
						<ul class="mui-table-view" style="margin-top: 15px;">
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right">手机<span class="mui-pull-right grxx">${users.phoneNum}</span></a>
							</li>
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right">邮箱<span class="mui-pull-right grxx">${users.email}</span></a>
							</li>							
							<li class="mui-table-view-cell">
								<a class="mui-navigate-right">电话<span class="mui-pull-right grxx">${users.telNum}</span></a>
							</li>	
						</ul>
					</div>
			
		</div>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		
	</body>
</html>
