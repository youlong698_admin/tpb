<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>我的已办</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
	    <style>
.drop-down-select {
	height: 350px;
	width: 180px;
}
</style>
<script type="text/javascript">
    var path="<%=path%>";
</script>
	</head>
	<body>
	<form id="xmForm" name="form" method="post" target="_self" action="viewMobileDoneForDeal_viewWaitForDeal.action">
		    <input type="hidden" id="wfType" name="wfType" value="${wfType }" />
		    <div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<div id="searchBarTitle">
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<a class="mui-btn mui-btn-link mui-pull-right" href="#menuMiddlePopover" id="menuSelect"><span class="mui-icon iconfont icon-list"></span></a>
				<a class="mui-btn mui-btn-link mui-pull-right" href="#" id="btnQuery"><span class="mui-icon iconfont icon-search"></span></a>
				<h1 class="mui-title">我的已办</h1> 
				</div>
				 <div class="searchbar spxxSearchbar"  style="display:none" id="searchQuery">
					<div class="mui-input-row mui-search" style="padding-right: 30px;clear: none">
					   <input type="search" class="mui-input-clear" placeholder="" name="wfdName" id="wfdName"  value="${wfdName }">
					</div>
					<button class="btn btn-link search-btn btnCacel" id="cacel" type="button">
						<span>取消</span>
					</button>
				</div>
		    </header>
				<div class="mui-content">
					<div id="slider" class="mui-slider mui-fullscreen">
						<div class="mui-slider-group">
							<!--下拉刷新容器-->
							<div id="pullrefresh" class="mui-scroll-wrapper spxx-view">
								<div class="mui-scroll">
									<!--数据列表-->
									<ul class="mui-table-view" id="spxxQuery">

									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="menuMiddlePopover" class="mui-popover drop-down-select spMenuMiddlePopover">
			<div class="mui-popover-arrow"></div>
			<div class="mui-scroll-wrapper" id="spMenu-mui-scroll">
				<div class="mui-scroll">
					<ul class="mui-table-view">					
					    <li class="mui-table-view-cell <c:if test="${wfType==''}">curList</c:if>"  val="">
							<div class="mui-pull-left"><span class="mui-badge sp allsp">全部</span></div><div class="mui-popover-name">全部审批</div>
						</li>
					    <li class="mui-table-view-cell <c:if test="${wfType=='RequiredPlan'}">curList</c:if>"  val="RequiredPlan">
							<div class="mui-pull-left"><span class="mui-badge sp jhsp">计划</span></div><div class="mui-popover-name">计划审批</div>
						</li>
					    <li class="mui-table-view-cell <c:if test="${wfType=='RequiredCollect'}">curList</c:if>"  val="RequiredCollect">
							<div class="mui-pull-left"><span class="mui-badge sp lxsp">立项</span></div><div class="mui-popover-name">采购立项审批</div>
						</li>
						<li class="mui-table-view-cell <c:if test="${wfType=='BidPurchaseResult'}">curList</c:if>"  val="BidPurchaseResult">
							<div class="mui-pull-left"><span class="mui-badge sp jgsp">结果</span></div><div class="mui-popover-name">采购结果审批</div>
						</li>
						<li class="mui-table-view-cell <c:if test="${wfType=='Contract'}">curList</c:if>"  val="Contract">
							<div class="mui-pull-left"><span class="mui-badge sp htsp">合同</span></div><div class="mui-popover-name">合同审批</div>
						</li>
						<li class="mui-table-view-cell <c:if test="${wfType=='Order'}">curList</c:if>"  val="Order">
							<div class="mui-pull-left"><span class="mui-badge sp ddsp">订单</span></div><div class="mui-popover-name">订单审批</div>
						</li>
						<li class="mui-table-view-cell <c:if test="${wfType=='CapitalPlan'}">curList</c:if>"  val="CapitalPlan">
							<div class="mui-pull-left"><span class="mui-badge sp zzsp">资金</span></div><div class="mui-popover-name">资金计划审批</div>
						</li>
						<li class="mui-table-view-cell <c:if test="${wfType=='SupplierEvaluate'}">curList</c:if>"  val="SupplierEvaluate">
							<div class="mui-pull-left"><span class="mui-badge sp pjsp">评价</span></div><div class="mui-popover-name">评价审批</div>
						</li>
					</ul>
				</div>
			</div>

		</div>
		</form>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		<script src="<%=path%>/mobile/js/common.js"></script>
		<script src="<%=path%>/mobile/js/epp/viewMobileDoneForDeal.js"></script>		
	</body>
</html>
