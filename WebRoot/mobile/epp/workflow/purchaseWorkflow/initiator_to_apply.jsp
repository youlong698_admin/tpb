<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>项目立项申请</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/js/timeline/timeline.css" />
</head>
<body>
<form id="supInfo" name="" method="post" action="">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
		   <div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">项目立项申请</h1> 
			</header>
			<div class="mui-content">
		    	<div class="mui-input-group spdetail">
						<p>
	    				    <label>项目编号：</label>
							<span>${requiredCollect.bidCode }</span>
						</p>
						<p>
						     <label>项目名称:</label>
							 <span>${requiredCollect.buyRemark }</span>
						</p>
						<p>
							<label>采购方式:</label>
							<span>${buyWayCn }</span>
						</p>
						<p>
							<label>招标方式:</label>
							<span>${supplierTypeCn }</span>
						</p>
						<p>
							<label>预算金额:</label>
							<span>${requiredCollect.totalBudget }</span>
						</p>
						<p>
							<label>采购组织:</label>
							<span>${purchaseDeptName }</span>
						</p>
						<p>
						    <label>申请人：</label>
			                <span>${creator }</span>
				        </p>
						<p>
						    <label>申请部门：</label>
			                <span>${deptName }</span>
			            </p>
						<p>			
							 <label>申请日期：</label>
							 <span>${createTime}</span>
						</p>
						<c:if test="${not empty requiredMaterial.attachmentUrl}">
							<p>
								<label>附件列表:</label>
								<span><c:out value="${requiredMaterial.attachmentUrl}" escapeXml="false"/></span>
							</p>
						</c:if>
					</div>
					
					<div class="mui-input-group" style="margin-bottom: 5px">
		    				<div class="mui-table-view-cell mui-collapse">
		    				   <a class="mui-navigate-right" href="#">采购明细</a>
		    				     <div class="mui-collapse-content">
		    				        <ul>
								       <c:forEach items="${listValue}" var="requiredCollectDetail" varStatus="status">
								        <li style="border-bottom:1px solid #c8c7cc">
								            <div class="mui-table">
								                <div class="mui-table-cell mui-col-xs-10">
								                    <h6 class="mui-ellipsis text-positive">${requiredCollectDetail.buyName}</h6>
								                    <p class="mui-h6 mui-ellipsis">规格型号：${requiredCollectDetail.materialType}</p>
								                </div>
								                <div class="mui-table-cell mui-col-xs-2 mui-text-right">
								                    <span class="mui-h5 text-primary">${requiredCollectDetail.amount}</span><span class="mui-h6">${requiredCollectDetail.unit}</span>
								                </div>
								            </div>
								        </li>
								        </c:forEach>								        
								    </ul> 
		    				     </div>
		    				</div>
		    		</div>
					<div class="mui-input-group" style="margin-bottom: 5px">
						<div class="mui-table-view-cell mui-collapse">
							<a class="mui-navigate-right" href="#">历史审批记录</a>
							<div class="mui-collapse-content">
							     <div class="mui-timeline">
            
						            <div class="mui-timeline-item start">
						                <div class="mui-timeline-item-label-icon mui-bg-success start">
						                    <i class="mui-icon icon iconfont icon-kaishi"></i>
						                </div>
						                <div class="mui-timeline-item-inner">
						                    &nbsp;
						                </div>
						            </div>
						            <c:forEach items="${hytList}" var="hy" varStatus="status">
							            <div class="mui-timeline-item middle">
							                <div class="mui-timeline-item-label-icon mui-bg-info middle">
							                    <i class="mui-iconfont icon iconfont icon-gerenzhongxin"></i>
							                </div>
							                <div class="mui-timeline-item-inner">
							                    <div class="content">
								                        <h5 class="mui-ellipsis text-dim">${hy.operatorCn}(<c:choose><c:when test="${hy.performType=='1'}"><span class="text-success">通过</span></c:when><c:when test="${hy.performType=='-1'}"><span class="text-negative">退回</span></c:when><c:otherwise></c:otherwise></c:choose>)</h5>
								                        <p class="mui-h6 mui-ellipsis">${hy.finishTime}</p>
								                        <p class="mui-h6 mui-ellipsis">${hy.variable}</p>
							                    </div>
							                </div>
							            </div>
						             </c:forEach>
									<c:if test="${orderState==0}">
										<div class="mui-timeline-item end">
							                <div class="mui-timeline-item-label-icon mui-bg-danger end">
							                    <i class="mui-iconfont icon iconfont icon-weibiaoti517"></i>
							                </div>
											<div class="mui-timeline-item-inner">
							                       &nbsp;                           
							                </div>
							            </div>
						            </c:if>
						        </div>
							</div>
						</div>
					</div>
					<c:if test="${detail!='detail'}">
					<div>
						<div class="mui-input-row">
							<textarea name="signContent" id="signContent" rows="2" placeholder="申请人意见"></textarea>
						</div>
					</div>
                    </c:if>
					
				</div>
				<c:if test="${detail!='detail'}">
					<div class="footer">
							<button type="button" class="mui-btn mui-btn-primary mui-col-xs-5" onclick="initApplySubmitMobile();">提交</button>&nbsp;&nbsp;
							<button class="mui-btn mui-btn-danger mui-col-xs-5" type="reset">重置</button>
							<input type="hidden" id="from" name="from" value="0" />
					</div>	
				</c:if>
	</div>
</form>	
 <script src="<%=path%>/mobile/js/jquery.min.js"></script>	
 <script src="<%=path%>/mobile/js/mui.min.js"></script>	
 <script src="<%= path %>/mobile/js/epp/workflow/workFlow.js" type="text/javascript" ></script>
</body>
</html>