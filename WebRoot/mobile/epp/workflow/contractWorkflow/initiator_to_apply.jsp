<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>合同申请</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/js/timeline/timeline.css" />
</head>
<body>
<form id="supInfo" name="" method="post" action="">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
		   <div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">合同申请</h1> 
			</header>
			<div class="mui-content">
		    	<div class="mui-input-group spdetail">
						<p>
	    				    <label>项目编号:</label>
							<span>${contractInfo.bidCode }</span>
						</p>
						<p>
						     <label>项目名称:</label>
							 <span>${contractInfo.projectName }</span>
						</p>
						<p>
							<label>合同编号:</label>
							<span>${contractInfo.contractCode }</span>
						</p>
						<p>
							<label>合同名称:</label>
							<span>${contractInfo.contractName }</span>
						</p>
						<p>
							<label>合同分类:</label>
							<span>${contractInfo.contractType }</span>
						</p>
						<p>
							<label>项目分类:</label>
							<span>${contractInfo.projectType }</span>
						</p>
						<p>
							<label>乙方名称:</label>
							<span>${contractInfo.contractNameB }</span>
						</p>
						<p>
							<label>乙方联系人:</label>
							<span>${contractInfo.contractPersonB }</span>
						</p>
						<p>
							<label>乙方联系人手机:</label>
							<span>${contractInfo.contractMobileB }</span>
						</p>
						<p>
							<label>乙方电话:</label>
							<span>${contractInfo.contractTelB }</span>
						</p>
						<p>
							<label>合同金额:</label>
							<span><fmt:formatNumber value="${contractInfo.contractMoney }" pattern="#,##0.00"/>&nbsp;&nbsp;${contractInfo.conMoneyType}</span>
						</p>
						<p>
							<label>合同审批原件:</label>
							<span> 
							   <c:if test="${not empty appFile}">
			                          <c:out value="${appFile}" escapeXml="false"/>
						      </c:if>
                          </span>
						</p>
						<c:if test="${not empty contractInfo.conRemark}">
							<p>
								<label>备注:</label>
								<span>${contractInfo.conRemark }</span>
							</p>
						</c:if>
					</div>
					
					<div class="mui-input-group" style="margin-bottom: 5px">
		    				<div class="mui-table-view-cell mui-collapse">
		    				   <a class="mui-navigate-right" href="#">合同明细</a>
		    				     <div class="mui-collapse-content">
		    				        <ul>
								       <c:set var="totalPrice" value="0"/>
										<c:forEach items="${cmList}" var="contractMaterial" varStatus="status">
										<c:set var="totalPrice" value="${totalPrice+contractMaterial.amount*contractMaterial.price }"/> 
								        <li style="border-bottom:1px solid #c8c7cc">
								            <div class="mui-table">
								                <div class="mui-table-cell mui-col-xs-8">
								                    <h6 class="mui-ellipsis text-positive">${contractMaterial.materialName}</h6>
								                    <p class="mui-h6 mui-ellipsis">规格型号：${contractMaterial.materialType}</p>
								                </div>
								                <div class="mui-table-cell mui-col-xs-4 mui-text-right">
								                    <p>
								                    <span class="mui-h6 text-primary">
								                                                           ￥ <fmt:formatNumber value="${contractMaterial.price}" pattern="#,##0.00"/>
								                    </span>
								                    </p>
								                    <p>
								                    <span class="mui-h6">${contractMaterial.amount} </span>
								                    <span class="mui-h6">${contractMaterial.unit}</span></p>
								                </div>
								            </div>
								        </li>
								        </c:forEach>	
								        <li style="border-bottom:1px solid #c8c7cc;height: 40px;">
								            <div class="mui-table" style="padding: 10px;">
								                <div class="mui-table-cell mui-col-xs-4">
								                    <p class="mui-h5 mui-ellipsis">总价：</p>
								                </div>
								                <div class="mui-table-cell mui-col-xs-8 mui-text-right">
								                    <p>
								                    <span class="mui-h5 text-negative">
								                                                           ￥ <fmt:formatNumber value="${totalPrice}" pattern="#,##0.00"/>
								                    </span>
								                    </p>								                
								                </div>
								            </div>
								        </li>							        
								    </ul> 
		    				     </div>
		    				</div>
		    		</div>
					<div class="mui-input-group" style="margin-bottom: 5px">
						<div class="mui-table-view-cell mui-collapse">
							<a class="mui-navigate-right" href="#">历史审批记录</a>
							<div class="mui-collapse-content">
							     <div class="mui-timeline">
            
						            <div class="mui-timeline-item start">
						                <div class="mui-timeline-item-label-icon mui-bg-success start">
						                    <i class="mui-icon icon iconfont icon-kaishi"></i>
						                </div>
						                <div class="mui-timeline-item-inner">
						                    &nbsp;
						                </div>
						            </div>
						            <c:forEach items="${hytList}" var="hy" varStatus="status">
							            <div class="mui-timeline-item middle">
							                <div class="mui-timeline-item-label-icon mui-bg-info middle">
							                    <i class="mui-iconfont icon iconfont icon-gerenzhongxin"></i>
							                </div>
							                <div class="mui-timeline-item-inner">
							                    <div class="content">
								                        <h5 class="mui-ellipsis text-dim">${hy.operatorCn}(<c:choose><c:when test="${hy.performType=='1'}"><span class="text-success">通过</span></c:when><c:when test="${hy.performType=='-1'}"><span class="text-negative">退回</span></c:when><c:otherwise></c:otherwise></c:choose>)</h5>
								                        <p class="mui-h6 mui-ellipsis">${hy.finishTime}</p>
								                        <p class="mui-h6 mui-ellipsis">${hy.variable}</p>
							                    </div>
							                </div>
							            </div>
						             </c:forEach>
									<c:if test="${orderState==0}">
										<div class="mui-timeline-item end">
							                <div class="mui-timeline-item-label-icon mui-bg-danger end">
							                    <i class="mui-iconfont icon iconfont icon-weibiaoti517"></i>
							                </div>
											<div class="mui-timeline-item-inner">
							                       &nbsp;                           
							                </div>
							            </div>
						            </c:if>
						        </div>
							</div>
						</div>
					</div>
					<c:if test="${detail!='detail'}">
					<div>
						<div class="mui-input-row">
							<textarea name="signContent" id="signContent" rows="2" placeholder="申请人意见"></textarea>
						</div>
					</div>
                    </c:if>
					
				</div>
				<c:if test="${detail!='detail'}">
					<div class="footer">
							<button type="button" class="mui-btn mui-btn-primary mui-col-xs-5" onclick="initApplySubmitMobile();">提交</button>&nbsp;&nbsp;
							<button class="mui-btn mui-btn-danger mui-col-xs-5" type="reset">重置</button>
							<input type="hidden" id="from" name="from" value="0" />
					</div>	
				</c:if>
	</div>
</form>	
 <script src="<%=path%>/mobile/js/jquery.min.js"></script>	
 <script src="<%=path%>/mobile/js/mui.min.js"></script>	
 <script src="<%= path %>/mobile/js/epp/workflow/workFlow.js" type="text/javascript" ></script>
</body>
</html>