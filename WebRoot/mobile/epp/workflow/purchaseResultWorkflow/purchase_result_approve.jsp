<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>采购结果审批</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/js/timeline/timeline.css" />
</head>
<body>
<form id="cx" method="post" action="">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
	<div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">采购结果审批</h1> 
			</header>
			<div class="mui-content">
		    	<div class="mui-input-group spdetail">
						<p>
	    				    <label>项目编号:</label>
							<span>${requiredCollect.bidCode}</span>
						</p>
						<p>
						     <label>项目名称:</label>
							 <span>${requiredCollect.buyRemark }</span>
						</p>
						<p>
							<label>采购方式:</label>
							<span>${buyWayCn }</span>
						</p>
						<p>
							<label>招标方式:</label>
							<span>${supplierTypeCn}</span>
						</p>
						<p>
							<label>采购组织:</label>
							<span>${purchaseDeptName }</span>
						</p>
						<p>
						    <label>申请人：</label>
			                <span>${creator }</span>
				        </p>
						<p>
						    <label>申请部门：</label>
			                <span>${deptName }</span>
			            </p>
						<p>			
							 <label>申请日期：</label>
							 <span>${createTime}</span>
						</p>
					</div>
					
					<div class="mui-input-group" style="margin-bottom: 5px">
		    				<div class="mui-table-view-cell mui-collapse">
		    				   <a class="mui-navigate-right" href="#">授标信息</a>
		    				     <div class="mui-collapse-content">
		    				        	<table width="100%" class="table_ys1">
										<tr class="Content_tab_style_04">
											<th width="60%" nowrap>
												供应商
											</th>
											<th width="30%" nowrap>
												总报价
											</th>
											<th width="30%" nowrap>
												授标价
											</th>	
										</tr>
										<c:forEach items="${list}" var="bidPrice" varStatus="status">
											<tr align="center" class="<c:if test="${not empty bidPrice.bidPrice }">text-primary</c:if>">
												<td>${bidPrice.supplierName}</td>
												<td align="right"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></td>
												<td align="right"><fmt:formatNumber value="${bidPrice.bidPrice}" pattern="#00.00#"/></td>
												</tr>
										</c:forEach>
								
									</table>
		    				     </div>
		    				</div>
		    		</div>
					<div class="mui-input-group" style="margin-bottom: 5px">
						<div class="mui-table-view-cell mui-collapse">
							<a class="mui-navigate-right" href="#">历史审批记录</a>
							<div class="mui-collapse-content">
							     <div class="mui-timeline">
            
						            <div class="mui-timeline-item start">
						                <div class="mui-timeline-item-label-icon mui-bg-success start">
						                    <i class="mui-icon icon iconfont icon-kaishi"></i>
						                </div>
						                <div class="mui-timeline-item-inner">
						                    &nbsp;
						                </div>
						            </div>
						            <c:forEach items="${hytList}" var="hy" varStatus="status">
							            <div class="mui-timeline-item middle">
							                <div class="mui-timeline-item-label-icon mui-bg-info middle">
							                    <i class="mui-iconfont icon iconfont icon-gerenzhongxin"></i>
							                </div>
							                <div class="mui-timeline-item-inner">
							                    <div class="content">
								                        <h5 class="mui-ellipsis text-dim">${hy.operatorCn}(<c:choose><c:when test="${hy.performType=='1'}"><span class="text-success">通过</span></c:when><c:when test="${hy.performType=='-1'}"><span class="text-negative">退回</span></c:when><c:otherwise></c:otherwise></c:choose>)</h5>
								                        <p class="mui-h6 mui-ellipsis">${hy.finishTime}</p>
								                        <p class="mui-h6 mui-ellipsis">${hy.variable}</p>
							                    </div>
							                </div>
							            </div>
						             </c:forEach>
									<c:if test="${orderState==0}">
										<div class="mui-timeline-item end">
							                <div class="mui-timeline-item-label-icon mui-bg-danger end">
							                    <i class="mui-iconfont icon iconfont icon-weibiaoti517"></i>
							                </div>
											<div class="mui-timeline-item-inner">
							                       &nbsp;                           
							                </div>
							            </div>
						            </c:if>
						        </div>
							</div>
						</div>
					</div>
					<c:if test="${detail!='detail'}">
					<div>
						<div class="mui-input-row">
							<textarea name="signContent" id="signContent" rows="2" placeholder="审批意见"></textarea>
						</div>
					</div>
                    </c:if>
					
				</div>
				<c:if test="${detail!='detail'}">
					<div class="footer">
							<button type="button" class="mui-btn mui-btn-primary mui-col-xs-5" onclick="submitWorkFlowMobile('1');">通过</button>&nbsp;&nbsp;
							<button type="button" class="mui-btn mui-btn-danger mui-col-xs-5" onclick="submitWorkFlowMobile('-1');">退回</button>
							<input type="hidden" id="result" name="result" value="" />
							<input type="hidden" id="from" name="from" value="0" />
					</div>	
				</c:if>
	</div>
</form>	
 <script src="<%=path%>/mobile/js/jquery.min.js"></script>	
 <script src="<%=path%>/mobile/js/mui.min.js"></script>	
 <script src="<%= path %>/mobile/js/epp/workflow/workFlow.js" type="text/javascript" ></script>
</body>
</html>