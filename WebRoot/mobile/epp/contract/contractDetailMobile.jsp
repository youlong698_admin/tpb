<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>合同详情查看</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<style type="text/css">
		   .mui-control-content {
				height: 100%;
				padding: 10px;
			}
			img{
			  width: 18px;
			  height: 18px;
			}
		</style>
</head>
<body>
<form id="supInfo" name="" method="post" action="">
<!-- 防止表单重复提交 -->
<s:token/>
	<div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">合同详情页面</h1> 
			</header>
			<div class="mui-content">
			    <div style="padding: 10px 10px;">
					<div id="segmentedControl" class="mui-segmented-control">
						<a class="mui-control-item mui-active" href="#item1">基本信息</a>
						<a class="mui-control-item" href="#item2">合同物资信息</a>
						<a class="mui-control-item" href="#item3">合同签订信息</a>
					</div>
				</div>
				<div id="item1" class="mui-control-content mui-active">
						<div>
						  <div class="mui-input-group spdetail">
								<p>
			    				    <label>项目名称:</label>
									<span>${contractInfo.projectName }</span>
								</p>
								<p>
								     <label>项目编号:</label>
									 <span> ${contractInfo.bidCode }</span>
								</p>
								<p>
									<label>合同编号:</label>
									<span>${contractInfo.contractCode }</span>
								</p>
								<p>
									<label>合同名称:</label>
									<span>${contractInfo.contractName }</span>
								</p>
								<p>
									<label>合同分类:</label>
									<span>${contractInfo.contractType }</span>
								</p>
								<p>
								    <label>项目分类:</label>
					                <span>${contractInfo.projectType }</span>
						        </p>
								<p>
								    <label>甲方名称:</label>
					                <span>${contractInfo.contractPersonNameA }</span>
					            </p>
								<p>			
									 <label>乙方名称:</label>
									 <span>${contractInfo.contractNameB }</span>
								</p>
								<p>			
									 <label>甲方地址:</label>
									 <span>${contractInfo.contractAddressA }</span>
								</p>
								<p>			
									 <label>乙方地址:</label>
									 <span>${contractInfo.contractAddressB }</span>
								</p>
								<p>			
									 <label>甲方联系人:</label>
									 <span>${contractInfo.contractUndertaker }</span>
								</p>
								<p>			
									 <label>乙方联系人:</label>
									 <span>${contractInfo.contractPersonB }</span>
								</p>
								<p>			
									 <label>甲方联系人手机:</label>
									 <span>${contractInfo.contractMobileA }</span>
								</p>
								<p>			
									 <label>乙方联系人手机:</label>
									 <span>${contractInfo.contractMobileB }</span>
								</p>
								<p>			
									 <label>甲方电话:</label>
									 <span>${contractInfo.contractTelA }</span>
								</p>
								<p>			
									 <label>乙方电话:</label>
									 <span>${contractInfo.contractTelB }</span>
								</p>
								<p>			
									 <label>甲方传真:</label>
									 <span>${contractInfo.contractFaxA }</span>
								</p>
								<p>			
									 <label>乙方传真:</label>
									 <span>${contractInfo.contractFaxB }</span>
								</p>
								<p>			
									 <label>合同金额:</label>
									 <span><fmt:formatNumber value="${contractInfo.contractMoney }" pattern="#,##0.00"/></span>
								</p>
								<p>			
									 <label>货币类型:</label>
									 <span> ${contractInfo.conMoneyType}</span>
								</p>
								<p>			
									 <label>框架协议:</label>
									 <span><c:choose>
									         <c:when test="${contractInfo.framework==0}">是</c:when>
									         <c:otherwise>否</c:otherwise>
									      </c:choose>
									 </span>
								</p>
								<p>			
									 <label>合同审批原件:</label>
									 <span><c:if test="${not empty appFile}">
						                          <c:out value="${appFile}" escapeXml="false"/>
									      </c:if>
									 </span>
								</p>
								<c:if test="${contractInfo.framework==1}">
								<p>			
									 <label>收货联系人:</label>
									 <span> ${contractInfo.billUndertaker }</span>
								</p>
								<p>			
									 <label>联系人手机:</label>
									 <span> ${contractInfo.billMobile}</span>
								</p>
								</c:if>
								<p>			
									 <label>备注:</label>
									 <span>${contractInfo.conRemark }</span>
								</p>
							</div>
							
						</div>
			    </div>
			    <div id="item2" class="mui-control-content">
					<div class="mui-input-group">
					   <ul>
					       <c:set var="totalPrice" value="0"/>
							<c:forEach items="${cmList}" var="contractMaterial" varStatus="status">
							<c:set var="totalPrice" value="${totalPrice+contractMaterial.amount*contractMaterial.price }"/> 
					        <li style="border-bottom:1px solid #c8c7cc">
					            <div class="mui-table">
					                <div class="mui-table-cell mui-col-xs-8">
					                    <h6 class="mui-ellipsis text-positive">${contractMaterial.materialName}</h6>
					                    <p class="mui-h6 mui-ellipsis">规格型号：${contractMaterial.materialType}</p>
					                </div>
					                <div class="mui-table-cell mui-col-xs-4 mui-text-right">
					                    <p>
					                    <span class="mui-h6 text-primary">
					                                                           ￥ <fmt:formatNumber value="${contractMaterial.price}" pattern="#,##0.00"/>
					                    </span>
					                    </p>
					                    <p>
					                    <span class="mui-h6">${contractMaterial.amount} </span>
					                    <span class="mui-h6">${contractMaterial.unit}</span></p>
					                </div>
					            </div>
					        </li>
					        </c:forEach>	
					        <li style="border-bottom:1px solid #c8c7cc;height: 40px;">
					            <div class="mui-table" style="padding: 10px;">
					                <div class="mui-table-cell mui-col-xs-4">
					                    <p class="mui-h5 mui-ellipsis">总价：</p>
					                </div>
					                <div class="mui-table-cell mui-col-xs-8 mui-text-right">
					                    <p>
					                    <span class="mui-h5 text-negative">
					                                                           ￥ <fmt:formatNumber value="${totalPrice}" pattern="#,##0.00"/>
					                    </span>
					                    </p>								                
					                </div>
					            </div>
					        </li>							        
					    </ul> 
					</div>
			    </div>
			    <div id="item3" class="mui-control-content">
					<div>
					   <div class="mui-input-group spdetail">
							<p>
		    				    <label>合同签订日期:</label>
								<span><fmt:formatDate value="${contractInfo.signDate }" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></span>
							</p>
							<p>
		    				    <label>合同签订地点:</label>
								<span>${contractInfo.contractSignAddress }</span>
							</p>
							<p>
		    				    <label>合同生效日期:</label>
								<span><fmt:formatDate value="${contractInfo.conValidDate }" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></span>
							</p>
							<p>
		    				    <label>合同结束日期:</label>
								<span><fmt:formatDate value="${contractInfo.conFinishDate }" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></span>
							</p>
							<p>
		    				    <label>质保开始日期:</label>
								<span><fmt:formatDate value="${contractInfo.warrantyPeriodStartDate }" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></span>
							</p>
							<p>
		    				    <label>质保结束日期:</label>
								<span><fmt:formatDate value="${contractInfo.warrantyPeriodStopDate }" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></span>
							</p>
							<p>
		    				    <label>预计到货日期:</label>
								<span><fmt:formatDate value="${contractInfo.requirementArrivalDate }" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></span>
							</p>
							<p>
		    				    <label>付款方式:</label>
								<span>${contractInfo.paymentType}</span>
							</p>
							<p>
		    				    <label>合同扫描件:</label>
								<span><c:if test="${not empty scanFile}">
				                     <c:out value="${scanFile}" escapeXml="false"/>
							      </c:if>
							    </span>
							</p>
						</div>
					</div>
				</div>
		    </div>
		    	
	</div>
</form>	
 <script src="<%=path%>/mobile/js/jquery.min.js"></script>	
 <script src="<%=path%>/mobile/js/mui.min.js"></script>	
</body>
</html>