<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>项目详情查看</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/common.css" />
		<link rel="stylesheet" href="<%=path%>/style/timeline/styleMobile.css"
			type="text/css" media="all" />	
		<style type="text/css">
		   .mui-control-content {
				height: 100%;
				padding: 10px;
			}
		</style>
</head>
<body>
<form id="supInfo" name="" method="post" action="">
<%@include file="/epp/workflow/include_workflow.jsp"%>
<!-- 防止表单重复提交 -->
<s:token/>
	<div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<h1 class="mui-title">项目详情页面</h1> 
			</header>
			<div class="mui-content">
			    <div style="padding: 10px 10px;">
					<div id="segmentedControl" class="mui-segmented-control">
						<a class="mui-control-item mui-active" href="#item1">基本信息</a>
						<a class="mui-control-item" href="#item2">进度信息</a>
					</div>
				</div>
				<div id="item1" class="mui-control-content mui-active">
						<div>
						  <div class="mui-input-group spdetail">
								<p>
			    				    <label>项目编号:</label>
									<span>${requiredCollect.bidCode }	</span>
								</p>
								<p>
								     <label>项目名称:</label>
									 <span>${requiredCollect.buyRemark }</span>
								</p>
								<p>
									<label>采购方式:</label>
									<span>${buyWayCn }</span>
								</p>
								<p>
									<label>供应商选择方式:</label>
									<span>${supplierTypeCn }</span>
								</p>
								<p>
									<label>预算金额:</label>
									<span>${requiredCollect.totalBudget }</span>
								</p>
								<p>
								    <label>编制日期:</label>
					                <span><fmt:formatDate value="${requiredCollect.writeDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /> </span>
						        </p>
								<p>
								    <label>采购组织:</label>
					                <span>${purchaseDeptName }</span>
					            </p>
								<p>			
									 <label>负责人:</label>
									 <span>${writerCn }</span>
								</p>
								<p>			
									 <label>负责人单位:</label>
									 <span>${deptName }</span>
								</p>
							</div>
							<div class="mui-input-group" style="margin-bottom: 5px">
				    				<div class="mui-table-view-cell mui-collapse">
				    				   <a class="mui-navigate-right" href="#">采购明细</a>
				    				     <div class="mui-collapse-content">
				    				        <ul>
										       <c:forEach items="${rcdList}" var="requiredCollectDetail" varStatus="status">
										        <li style="border-bottom:1px solid #c8c7cc">
										            <div class="mui-table">
										                <div class="mui-table-cell mui-col-xs-10">
										                    <h6 class="mui-ellipsis text-positive">${requiredCollectDetail.buyName}</h6>
										                    <p class="mui-h6 mui-ellipsis">规格型号：${requiredCollectDetail.materialType}</p>
										                </div>
										                <div class="mui-table-cell mui-col-xs-2 mui-text-right">
										                    <span class="mui-h5 text-primary">${requiredCollectDetail.amount}</span><span class="mui-h6">${requiredCollectDetail.unit}</span>
										                </div>
										            </div>
										        </li>
										        </c:forEach>								        
										    </ul> 
				    				     </div>
				    				</div>
				    		</div>				    		
							<div class="mui-input-group" style="margin-bottom: 5px">
					    		<c:choose>
		                          <c:when test="${requiredCollect.buyWay=='00'}">
		                              <div class="mui-table-view-cell mui-collapse">
					    				   <a class="mui-navigate-right" href="#">招标计划</a>
					    				     <div class="mui-collapse-content">
					    				        <p>			
													 <label>开标日期:</label>
													 <span><fmt:formatDate value="${tenderBidList.openDate}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>招标文件领购开始日期:</label>
													 <span><fmt:formatDate value="${tenderBidList.salesDate}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>招标文件领购截止日期:</label>
													 <span><fmt:formatDate value="${tenderBidList.saleeDate}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>回标截止日期:</label>
													 <span><fmt:formatDate value="${tenderBidList.returnDate }" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>标书费（元）:</label>
													 <span>${tenderBidList.tenderMoney }</span>
												</p>
												<p>			
													 <label>保证金（元）:</label>
													 <span>${tenderBidList.bondMoney }</span>
												</p>
												<p>			
													 <label>最小报价单位数:</label>
													 <span>${tenderBidList.minBidAmount }</span>
												</p>
												<p>			
													 <label>开标管理员:</label>
													 <span>${tenderBidList.bidOpenAdminCn }</span>
												</p>
					    				     </div>
					    			   </div>
		                          </c:when>
		                          <c:when test="${requiredCollect.buyWay=='01'}">
		                             <div class="mui-table-view-cell mui-collapse">
					    				   <a class="mui-navigate-right" href="#">询价计划</a>
					    				     <div class="mui-collapse-content">
					    				         <p>			
													 <label>询价时间:</label>
													 <span><fmt:formatDate value="${askBidList.askDate}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>报价截止时间:</label>
													 <span><fmt:formatDate value="${askBidList.returnDate}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>报价解密时间:</label>
													 <span><fmt:formatDate value="${askBidList.decryptionDate }" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
					    				     </div>
					    			 </div>
		                          </c:when>
		                          <c:otherwise>
		                             <div class="mui-table-view-cell mui-collapse">
					    				   <a class="mui-navigate-right" href="#">竞价方案</a>
					    				     <div class="mui-collapse-content">
					    				         <p>			
													 <label>竞价方式:</label>
													 <span>${biddingBidList.biddingTypeCn }</span>
												</p>
												<p>			
													 <label>报价显示方式:</label>
													 <span>${biddingBidList.priceModeCn }</span>
												</p>
												<p>			
													 <label>报价原则:</label>
													 <span>${biddingBidList.pricePrincipleCn }</span>
												</p>
												<p>			
													 <label>报价原则:</label>
													 <span>${biddingBidList.pricePrincipleCn }</span>
												</p>
												<p>			
													 <label>竞价原则:</label>
													 <span>${biddingBidList.biddingPrincipleCn }</span>
												</p>
												<p>			
													 <label>竞价开始时间:</label>
													 <span><fmt:formatDate value="${biddingBidList.biddingStartTime}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>竞价结束时间:</label>
													 <span><fmt:formatDate value="${biddingBidList.biddingEndTime}" pattern="yyyy-MM-dd HH:mm" /></span>
												</p>
												<p>			
													 <label>竞价管理员:</label>
													 <span>${biddingBidList.bidAdminCn }</span>
												</p>
												<p>			
													 <label>竞价管理员:</label>
													 <span>${biddingBidList.bidAdminCn }</span>
												</p>
												<p>			
													 <label>最高限价:</label>
													 <span> ${biddingBidList.hignPrice}</span>
												</p>
												<p>			
													 <label>最低限价:</label>
													 <span>${biddingBidList.minPrice}</span>
												</p>
												<p>			
													 <label>竞价结束延迟时间(分钟):</label>
													 <span>${biddingBidList.delayTime}</span>
												</p>
					    				     </div>
					    			 </div>
		                          </c:otherwise>
		                        </c:choose>
	                       </div>
				    		<div class="mui-input-group" style="margin-bottom: 35px">
				    				<div class="mui-table-view-cell mui-collapse">
				    				   <a class="mui-navigate-right" href="#">授标信息</a>
				    				     <div class="mui-collapse-content">
				    				        <table width="100%" class="table_ys1">
												<tr class="Content_tab_style_04">
													<th width="60%" nowrap>
														供应商
													</th>
													<th width="30%" nowrap>
														总报价
													</th>
													<th width="30%" nowrap>
														授标价
													</th>	
												</tr>
												<c:forEach items="${list}" var="bidPrice" varStatus="status">
													<tr align="center" class="<c:if test="${not empty bidPrice.bidPrice }">text-primary</c:if>">
														<td>${bidPrice.supplierName}</td>
														<td align="right"><fmt:formatNumber value="${bidPrice.totalPrice}" pattern="#00.00#"/></td>
														<td align="right"><fmt:formatNumber value="${bidPrice.bidPrice}" pattern="#00.00#"/></td>
														</tr>
												</c:forEach>
										
											</table> 
				    				     </div>
				    				</div>
				    		</div>
						</div>
			    </div>
			    <div id="item2" class="mui-control-content">
						<div>
						   <div class="record">	     	
							<c:set var="data_tmp" value=""/>
							<dl class="recordList">	
							<c:forEach items="${purchaseRecordList}" var="purchaseRecordLog" > 
							<c:set var="data"><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="yyyy-MM-dd"/></c:set>
							    <c:if test="${data_tmp==''}">
								<dt>
									<p><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="MM-dd"/></p>
								</dt>						
							    <c:set var="data_tmp" value="${data}"/>
								</c:if>	
								 <c:if test="${data_tmp!=''&&data_tmp!=data}">	
								 <dt>
									<p><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="MM-dd"/></p>
								</dt>						
							    <c:set var="data_tmp" value="${data}"/>	
								 </c:if>		
								<dd>
								<p class="record_Time"><fmt:formatDate value="${purchaseRecordLog.operateDate}" pattern="HH:mm:ss"/></p>
								<div class="record_jd"></div>
								<div class="record_Info">
									<div class="record_arr">◆</div>
									<div class="record_Intro">
										<p>${purchaseRecordLog.bidNode }</p>
										<p class="record_Intro_Title"><font color="#00848E">${purchaseRecordLog.operateContent }</font> </p>
									</div>
									
								</div>
								<div class="clear"></div>
							</dd>
							</c:forEach>								
						</dl>
					</div>
						</div>
			    </div>
		    </div>
		    	
	</div>
</form>	
 <script src="<%=path%>/mobile/js/jquery.min.js"></script>	
 <script src="<%=path%>/mobile/js/mui.min.js"></script>	
</body>
</html>