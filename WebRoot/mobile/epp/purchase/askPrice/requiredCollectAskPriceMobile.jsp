<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/context.jsp"%>
<!DOCTYPE html>
<html>
	<head lang="en">
		<title>发布的询价</title>
		<meta name="keywords"
			content="" />
		<meta name="description"
			content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
		<link rel="apple-touch-icon-precomposed" sizes="36x36"
			href="<%=path%>/mobile/images/app-36.png" />
		<link rel="apple-touch-icon-precomposed" sizes="48x48"
			href="<%=path%>/mobile/images/app-48.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="<%=path%>/mobile/images/app-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="96x96"
			href="<%=path%>/mobile/images/app-96.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="<%=path%>/mobile/images/app-144.png" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/mui.min.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/font-mobile/iconfont.css" />
		<link rel="stylesheet" href="<%=path%>/mobile/css/base.css" />	
		<link rel="stylesheet" href="<%=path%>/mobile/css/bidding.css" />
	    <style>
.drop-down-select {
	height: 220px;
	width: 180px;
}
</style>
<script type="text/javascript">
    var path="<%=path%>";
</script>
	</head>
	<body>
	<form id="xmForm" name="form" method="post" target="_self" action="viewRequiredCollect_purchaseAsk.action?from=0">
		    <div class="mui-inner-wrap container page-search-detail">
		    <!--页面标题栏开始-->
			<header class="mui-bar mui-bar-nav">	
				<div id="searchBarTitle">
				<a class="mui-action-back mui-icon mui-icon-left-nav mui-pull-left"></a>
				<a class="mui-btn mui-btn-link mui-pull-right" href="#" id="btnQuery"><span class="mui-icon iconfont icon-search"></span></a>
				<h1 class="mui-title">发布的询价</h1> 
				</div>
				 <div class="searchbar xmxxSearchbar"  style="display:none" id="searchQuery">
					<div class="mui-input-row mui-search" style="padding-right: 30px;clear: none">
					   <input type="search" class="mui-input-clear" placeholder="" name="buyRemark" id="buyRemark"  value="${buyRemark }">
					</div>
					<button class="btn btn-link search-btn btnCacel" id="cacel" type="button">
						<span>取消</span>
					</button>
				</div>
		    </header>
				<div class="mui-content">
					<div id="slider" class="mui-slider mui-fullscreen">
						<div class="mui-slider-group">
							<!--下拉刷新容器-->
							<div id="pullrefresh" class="mui-scroll-wrapper xmxx-view">
								<div class="mui-scroll">
									<!--数据列表-->
									<ul class="mui-table-view" id="xmxxQuery">

									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</form>
		<script src="<%=path%>/mobile/js/jquery.min.js"></script>
		<script src="<%=path%>/mobile/js/mui.min.js"></script>
		<script src="<%=path%>/mobile/js/common.js"></script>
		<script src="<%=path%>/mobile/js/epp/purchase/askPrice/requiredCollectAskPriceMobile.js"></script>		
	</body>
</html>
